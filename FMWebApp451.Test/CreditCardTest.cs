﻿using System;
using System.Web.Mvc;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using FM2015.Models;
using FM2015.Controllers;
using FMWebApp451.Respositories;
using FM2015.ViewModels;
using FMWebApp451.ViewModels.Stripe;
using FMWebApp451.Services.StripeServices;
using FMWebApp451.Services.PayWhirlServices;

namespace FMWebApp451.Test
{
    public class CreditCardTest
    {
        public enum cardInfoType
        {
            good, failsName, failsCardNumber, failsExpMonth, failsExpYear, failsCVC, failsId
        }

        public static CCUpdateController CCUpdateControllerFactory()
        {
            TestCreditCard creditCardService = new TestCreditCard();
            TestCustomerProvider customerProvider = new TestCustomerProvider();
            return new CCUpdateController(creditCardService, customerProvider);
        }

        public static CreditCard CreditCardFactory()
        {
            TestCustomerProvider customerProvider = new TestCustomerProvider();
            TestAddressProvider addressProvider = new TestAddressProvider();
            TestPayWhirlService payWhirlService = new TestPayWhirlService();
            TestStripeService stripeService = new TestStripeService();
            TestCERProvider CERProvider = new TestCERProvider();
            return new CreditCard(customerProvider, addressProvider, payWhirlService, stripeService, CERProvider);
        }

        public static CreditCardViewModel InitCCVM(cardInfoType type)
        {
            CreditCardViewModel ccvm = new CreditCardViewModel();
            //initialize a "good" test card
            ccvm.CreditCardViewModelKey = 1;
            ccvm.Name = "Rodger Mohme";
            ccvm.CardNumber = "1234 5678 9876 5432";
            ccvm.StripeCustomerID = "tst_cus_1234";
            ccvm.ExpirationMonth = "01";
            ccvm.ExpirationYear = "2016";
            ccvm.Cvc = "123";

            switch (type)
            {
                case cardInfoType.good:
                    break;

                case cardInfoType.failsId:
                    ccvm.CreditCardViewModelKey = 0;
                    break;

                case cardInfoType.failsName:
                    ccvm.Name = string.Empty;
                    break;

                case cardInfoType.failsCardNumber:
                    ccvm.CardNumber = string.Empty;
                    break;

                case cardInfoType.failsCVC:
                    ccvm.Cvc = string.Empty;
                    break;

                case cardInfoType.failsExpMonth:
                    ccvm.ExpirationMonth = string.Empty;
                    break;

                case cardInfoType.failsExpYear:
                    ccvm.ExpirationYear = string.Empty;
                    break;

                default:
                    break;
            }

            return ccvm;
            
 
        }

        [Fact]
        public void Index_DoesExist()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();

            // Act
            ViewResult result = controller.Index(0) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model
        }

        [Fact]
        public void Index_IdZeroReturnsViewBagErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();

            // Act
            ViewResult result = controller.Index(0) as ViewResult;

            // Assert
            Assert.Equal("Name: Unknown", result.ViewBag.Name.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void Index_IdZReturnsCCVM()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            int id = 1;

            // Act
            ViewResult result = controller.Index(id) as ViewResult;
            CreditCardViewModel ccvm = (CreditCardViewModel)result.Model;

            // Assert
            Assert.NotNull(ccvm);
            Assert.Equal(id, ccvm.CreditCardViewModelKey); // add additional checks on the Model
        }

        [Fact]
        public void Index_Post_EmptyCardNumberReturnsErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.CardNumber = string.Empty;

            // Act
            ViewResult result = controller.Index(ccvm) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Card Number is required", result.ViewBag.ErrorMsg.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void Index_Post_ShortCardNumberReturnsErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.CardNumber = "1234";

            // Act
            ViewResult result = controller.Index(ccvm) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Card is less than 15 characters.  Please correct, then try again.", result.ViewBag.ErrorMsg.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void Index_Post_EmptyNameReturnsErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.Name = string.Empty;

            // Act
            ViewResult result = controller.Index(ccvm) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Name is required", result.ViewBag.ErrorMsg.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void Index_Post_EmptyExpMonthReturnsErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.ExpirationMonth = string.Empty;

            // Act
            ViewResult result = controller.Index(ccvm) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Expiration Month is required", result.ViewBag.ErrorMsg.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void Index_Post_ExpMonthLT1ReturnsErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.ExpirationMonth = "0";

            // Act
            ViewResult result = controller.Index(ccvm) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Expiration month must be a number from 1 to 12 (for instance, 6)", result.ViewBag.ErrorMsg.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void Index_Post_ExpMonthGT12ReturnsErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.ExpirationMonth = "13";

            // Act
            ViewResult result = controller.Index(ccvm) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Expiration month must be a number from 1 to 12 (for instance, 6)", result.ViewBag.ErrorMsg.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void Index_Post_ExpMonthNANReturnsErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.ExpirationMonth = "cat";

            // Act
            ViewResult result = controller.Index(ccvm) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Expiration month must be a number from 1 to 12 (for instance, 5)", result.ViewBag.ErrorMsg.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void Index_Post_EmptyExpYearReturnsErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.ExpirationYear = string.Empty;

            // Act
            ViewResult result = controller.Index(ccvm) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Expiration Year is required", result.ViewBag.ErrorMsg.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void Index_Post_ExpYearNANReturnsErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.ExpirationYear = "dog";

            // Act
            ViewResult result = controller.Index(ccvm) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Expiration year must be a number (for instance, 2016)", result.ViewBag.ErrorMsg.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void Index_Post_CvcEmptyReturnsErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.Cvc = string.Empty;

            // Act
            ViewResult result = controller.Index(ccvm) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("CVS is required", result.ViewBag.ErrorMsg.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void Index_Post_CvcNANReturnsErrMsg()
        {
            //Arrange
            CCUpdateController controller = CCUpdateControllerFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.Cvc = "foo1";

            // Act
            ViewResult result = controller.Index(ccvm) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal("CVS must be all numbers (for instance, 123)", result.ViewBag.ErrorMsg.ToString()); // add additional checks on the Model
        }

        [Fact]
        public void AddNewCard_idZeroReturnsErrMsg()
        {
            //Arrange
            CreditCard ccProvider = CreditCardFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.CreditCardViewModelKey = 0;
            string resultMsg = @"Customer information is unavailable. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";

            // Act
            string result = ccProvider.AddNewCard(ccvm);

            // Assert
            Assert.Equal(resultMsg, result); // add additional checks on the Model
        }

        [Fact]
        public void AddNewCard_idLTZeroReturnsErrMsg()
        {
            //Arrange
            CreditCard ccProvider = CreditCardFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            ccvm.CreditCardViewModelKey = -1;
            string resultMsg = @"Customer information is unavailable. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";

            // Act
            string result = ccProvider.AddNewCard(ccvm);

            // Assert
            Assert.Equal(resultMsg, result); // add additional checks on the Model
        }

        [Fact]
        public void AddNewCard_BadNameReturnsErrMsg()
        {
            //Arrange
            CreditCard ccProvider = CreditCardFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.failsName);
            string resultMsg = @"Credit Card was declined. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";

            // Act
            string result = ccProvider.AddNewCard(ccvm);

            // Assert
            Assert.Equal(resultMsg, result); // add additional checks on the Model
        }

        [Fact]
        public void AddNewCard_BadCardNoReturnsErrMsg()
        {
            //Arrange
            CreditCard ccProvider = CreditCardFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.failsCardNumber);
            string resultMsg = @"Credit Card was declined. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";

            // Act
            string result = ccProvider.AddNewCard(ccvm);

            // Assert
            Assert.Equal(resultMsg, result); // add additional checks on the Model
        }

        [Fact]
        public void AddNewCard_BadMonthReturnsErrMsg()
        {
            //Arrange
            CreditCard ccProvider = CreditCardFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.failsExpMonth);
            string resultMsg = @"Credit Card was declined. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";

            // Act
            string result = ccProvider.AddNewCard(ccvm);

            // Assert
            Assert.Equal(resultMsg, result); // add additional checks on the Model
        }

        [Fact]
        public void AddNewCard_BadYearReturnsErrMsg()
        {
            //Arrange
            CreditCard ccProvider = CreditCardFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.failsExpYear);
            string resultMsg = @"Credit Card was declined. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";

            // Act
            string result = ccProvider.AddNewCard(ccvm);

            // Assert
            Assert.Equal(resultMsg, result); // add additional checks on the Model
        }

        [Fact]
        public void AddNewCard_BadCvcReturnsErrMsg()
        {
            //Arrange
            CreditCard ccProvider = CreditCardFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.failsCVC);
            string resultMsg = @"Credit Card was declined. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";

            // Act
            string result = ccProvider.AddNewCard(ccvm);

            // Assert
            Assert.Equal(resultMsg, result); // add additional checks on the Model
        }

        [Fact]
        public void AddNewCard_GoodCardReturnsNoErr()
        {
            //Arrange
            CreditCard ccProvider = CreditCardFactory();
            CreditCardViewModel ccvm = InitCCVM(cardInfoType.good);
            string resultMsg = @"Credit Card Update Successfully!";

            // Act
            string result = ccProvider.AddNewCard(ccvm);

            // Assert
            Assert.Equal(resultMsg, result); // add additional checks on the Model
        }
    }
}
