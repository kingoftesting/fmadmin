﻿using System;
using System.Web.Mvc;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using FM2015.Models;
using FM2015.Controllers;
using FMWebApp451.Respositories;
using FM2015.ViewModels;

namespace FMWebApp451.Test
{
    public class DSRByEmpTest
    {
        public static DSRByEmpController DSRByEmpTestControllerFactory()
        {
            TestOrderProvider orderProvider = new TestOrderProvider();
            TestOrderDetailProvider orderDetailProvider = new TestOrderDetailProvider();
            TestProductProvider productProvider = new TestProductProvider();
            TestProductCostProvider productCostProvider = new TestProductCostProvider();
            TestTransactionProvider transactionProvider = new TestTransactionProvider();
            TestSalesDetailsProvider salesDetailProvider = new TestSalesDetailsProvider();
            TestMultiPayRevenueByOrder mpRevenueByOrderProvider = new TestMultiPayRevenueByOrder();
            TestMultiPayTransactionsByDateProvider mpTransactionsByDateProvider = new TestMultiPayTransactionsByDateProvider();
            return new DSRByEmpController(orderProvider, orderDetailProvider, productProvider, transactionProvider, salesDetailProvider,
                mpRevenueByOrderProvider, mpTransactionsByDateProvider);
        }

        [Fact]
        public void GetMonthYear_NullsReturnTodayMonthYear()
        {
            //Arrange
            DSRByEmpController controller = DSRByEmpTestControllerFactory();
            int theMonth = 0;
            int theYear = 0;

            // Act
            string result = controller.GetMonthYear(null, null, out theMonth, out theYear);

            // Assert
            Assert.Equal(string.Empty, result);
            Assert.Equal(DateTime.Today.Year, theYear);
            Assert.Equal(DateTime.Today.Month, theMonth);
        }

        [Fact]
        public void GetMonthYear_NonIntsReturnTodayMonthYear()
        {
            //Arrange
            DSRByEmpController controller = DSRByEmpTestControllerFactory();
            int theMonth = 0;
            int theYear = 0;

            // Act
            string result = controller.GetMonthYear("foo", "bar", out theMonth, out theYear);

            // Assert
            Assert.Equal(string.Empty, result);
            Assert.Equal(DateTime.Today.Year, theYear);
            Assert.Equal(DateTime.Today.Month, theMonth);
        }

        [Fact]
        public void GetMonthYear_MonthYearReturnMonthYear()
        {
            //Arrange
            DSRByEmpController controller = DSRByEmpTestControllerFactory();
            int theMonth = 0;
            int theYear = 0;

            // Act
            string result = controller.GetMonthYear("11", "2015", out theMonth, out theYear);

            // Assert
            Assert.Equal(string.Empty, result);
            Assert.Equal(2015, theYear);
            Assert.Equal(11, theMonth);
        }

        [Fact]
        public void Index_DoesExist()
        {
            //Arrange
            DSRByEmpController controller = DSRByEmpTestControllerFactory();

            // Act
            ViewResult result = controller.Index(null, null) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model
        }
    }
}
