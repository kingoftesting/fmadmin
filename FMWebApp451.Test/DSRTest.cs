﻿using System;
using System.Web.Mvc;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using FM2015.Models;
using FM2015.Controllers;
using FMWebApp451.Respositories;
using FM2015.ViewModels;

namespace FMWebApp451.Test
{

    public class DSRTest
    {
        public static DSRController DSRControllerFactory()
        {
            TestOrderProvider orderProvider = new TestOrderProvider();
            TestOrderDetailProvider orderDetailProvider = new TestOrderDetailProvider();
            TestProductProvider productProvider = new TestProductProvider();
            TestProductCostProvider productCostProvider = new TestProductCostProvider();
            TestTransactionProvider transactionProvider = new TestTransactionProvider();
            TestSalesDetailsProvider salesDetailProvider = new TestSalesDetailsProvider();
            TestSubscriptionProvider subscriptionProvider = new TestSubscriptionProvider();
            TestSubscriptionPlanProvider subscriptionPlanProvider = new TestSubscriptionPlanProvider();
            TestMultiPayRevenueByOrder mpRevenueByOrderProvider = new TestMultiPayRevenueByOrder();
            TestMultiPayTransactionsByDateProvider mpTransactionsByDateProvider = new TestMultiPayTransactionsByDateProvider();
            return new DSRController(orderProvider, orderDetailProvider, productProvider, transactionProvider, salesDetailProvider, subscriptionProvider,
                subscriptionPlanProvider, mpRevenueByOrderProvider, mpTransactionsByDateProvider);
        }

        [Fact]
        public void Index_DoesExist()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();

            // Act
            ViewResult result = controller.Index(null) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model

        }

        [Fact]
        public void DSRController_IndexParamParser_EmptyStrReturnsToday()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            string startStr = string.Empty;
            string endStr = startStr;
            string resultStartStr = DateTime.Today.ToString();
            string resultEndStr = DateTime.Today.AddDays(1).AddTicks(-1).ToString();

            // Act
            string result = controller.IndexParamParser(null, out startStr, out endStr);

            // Assert
            Assert.Equal(string.Empty, result);
            Assert.Equal(resultStartStr, startStr);
            Assert.Equal(resultEndStr, endStr);
        }

        [Fact]
        public void DSRController_IndexParamParser_yStrReturnsYesterday()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            string startStr = string.Empty;
            string endStr = startStr;
            string resultStartStr = DateTime.Today.AddDays(-1).ToString();
            string resultEndStr = DateTime.Today.AddTicks(-1).ToString();

            // Act
            string result = controller.IndexParamParser("y", out startStr, out endStr);

            // Assert
            Assert.Equal(string.Empty, result);
            Assert.Equal(resultStartStr, startStr);
            Assert.Equal(resultEndStr, endStr);
        }

        [Fact]
        public void DSRController_IndexParamParser_ySpaceStrReturnsError()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            string startStr = string.Empty;
            string endStr = startStr;

            // Act
            string result = controller.IndexParamParser("y ", out startStr, out endStr);

            // Assert
            Assert.False(string.IsNullOrEmpty(result));
        }

        [Fact]
        public void DSRController_IndexParamParser_YStrReturnsYesterday()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            string startStr = string.Empty;
            string endStr = startStr;
            string resultStartStr = DateTime.Today.AddDays(-1).ToString();
            string resultEndStr = DateTime.Today.AddTicks(-1).ToString();

            // Act
            string result = controller.IndexParamParser("Y", out startStr, out endStr);

            // Assert
            Assert.Equal(string.Empty, result);
            Assert.Equal(resultStartStr, startStr);
            Assert.Equal(resultEndStr, endStr);
        }

        [Fact]
        public void DSRController_IndexParamParser_wStrReturns7Days()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime tempDate = DateTime.Today.AddDays(-7);
            DateTime tempDate1 = DateTime.Today.AddTicks(-1);
            string startStr = string.Empty;
            string endStr = startStr;

            // Act
            string result = controller.IndexParamParser("w", out startStr, out endStr);

            // Assert
            Assert.Equal(string.Empty, result);
            Assert.Equal(tempDate.ToString(), startStr);
            Assert.Equal(tempDate1.ToString(), endStr);
        }

        [Fact]
        public void DSRController_IndexParamParser_mStrReturnsCurrentMonth()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime tempDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime tempDate1 = tempDate.AddMonths(1).AddTicks(-1); //should be the end of the month
            string startStr = string.Empty;
            string endStr = startStr;

            // Act
            string result = controller.IndexParamParser("m", out startStr, out endStr);

            // Assert
            Assert.Equal(string.Empty, result);
            Assert.Equal(tempDate.ToString(), startStr);
            Assert.Equal(tempDate1.ToString(), endStr);
        }

        [Fact]
        public void DSRController_IndexParamParser_SingleDateReturnsDateAndEndOfMonth()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Today;
            DateTime endDate = startDate.Date.AddDays(1).AddTicks(-1);
            string startStr = string.Empty;
            string endStr = startStr;

            // Act
            string result = controller.IndexParamParser(startDate.ToString("d"), out startStr, out endStr);

            // Assert
            Assert.Equal(string.Empty, result);
            Assert.Equal(startDate.ToString(), startStr);
            Assert.Equal(endDate.ToString(), endStr);
        }

        [Fact]
        public void DSRController_IndexParamParser_DoubleDateReturnsSameDoubleDates()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Today;
            DateTime endDate = startDate;
            string paramStr = startDate.ToString("d") + " " + endDate.ToString("d");
            string startStr = string.Empty;
            string endStr = startStr;

            // Act
            string result = controller.IndexParamParser(paramStr, out startStr, out endStr);

            // Assert
            Assert.Equal(string.Empty, result);
            Assert.Equal(startDate.ToString(), startStr);
            Assert.Equal(startDate.AddDays(1).AddTicks(-1).ToString(), endStr);
        }

        [Fact]
        public void DSRController_IndexParamParser_ReverseDoubleDateReturnsError()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);
            string paramStr = endDate.ToString("d") + " " + startDate.ToString("d");
            string startStr = string.Empty;
            string endStr = startStr;

            // Act
            string result = controller.IndexParamParser(paramStr, out startStr, out endStr);

            // Assert
            Assert.False(string.IsNullOrEmpty(result));
        }

        [Fact]
        public void DSRController_GetSalesStats_OrderCountReturns4()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);

            // Act
            Sales result = controller.GetSalesStats(startDate, endDate);

            // Assert
            Assert.Equal(6, result.TotalOrders);
        }

        [Fact]
        public void DSRController_GetSalesStats_OrderDetailCountReturns4()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);

            // Act
            Sales result = controller.GetSalesStats(startDate, endDate);

            // Assert
            Assert.Equal(8, result.TotalUnits);
        }

        [Fact]
        public void DSRController_GetSalesStats_DeviceCountReturns3()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);

            // Act
            Sales result = controller.GetSalesStats(startDate, endDate);

            // Assert
            Assert.Equal(3, result.TotalDevices);
        }

        [Fact]
        public void DSRController_GetSalesStats_MPDeviceCountReturns1()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);

            // Act
            Sales result = controller.GetSalesStats(startDate, endDate);

            // Assert
            Assert.Equal(2, result.TotalMPDevices);
        }

        [Fact]
        public void DSRController_GetSalesStats_RefillsCountReturns1()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);

            // Act
            Sales result = controller.GetSalesStats(startDate, endDate);

            // Assert
            Assert.Equal(0, result.TotalRefills);
        }

        [Fact]
        public void DSRController_GetSalesStats_TransactionCountReturns1()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);

            // Act
            Sales result = controller.GetSalesStats(startDate, endDate);

            // Assert
            Assert.Equal(2, result.TotalMPDevices);
        }

        [Fact]
        public void DSRController_GetSalesStats_TransactionRefundReturnsCorrectAmount()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);

            // Act
            Sales result = controller.GetSalesStats(startDate, endDate);

            // Assert
            Assert.Equal(1.00M, result.TotalRefunds);
        }

        [Fact]
        public void DSRController_GetSalesStats_TransactionSAReturnsCorrectAmount()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);

            // Act
            Sales result = controller.GetSalesStats(startDate, endDate);

            // Assert
            Assert.Equal(10.00M, result.TotalOtherCS);
        }

        [Fact]
        public void DSRController_GetSalesStats_SalesDetailsReturnsList()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);

            // Act
            Sales result = controller.GetSalesStats(startDate, endDate);

            // Assert
            Assert.Equal(6, result.SalesDetailsList.Count);
        }

        [Fact]
        public void DSRController_GetSalesStats_SalesDetailsReturnsCorrectList()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);

            // Act
            Sales result = controller.GetSalesStats(startDate, endDate);

            // Assert
            Assert.Equal(131, result.SalesDetailsList[5].ProductID);
        }

        [Fact]
        public void DSRController_GetSalesStats_StatsReturnsCorrect()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(1);

            // Act
            Sales result = controller.GetSalesStats(startDate, endDate);

            // Assert
            Assert.Equal(startDate, result.StartDate);
            Assert.Equal(endDate, result.EndDate);
            Assert.Equal(134.46M, result.TotalCost);
            Assert.Equal(30.00M, result.TotalLineItemDiscounts);
            Assert.Equal(30.00M, result.tstTotalLineItemDiscounts);
            Assert.Equal(499.98M, result.Mp1PayRevenue);
            Assert.Equal(50.00M, result.MpDiscounts);
            Assert.Equal(112.48M, result.MpRevenue); //1st payment
            Assert.Equal(449.98M, result.MpNetRevenue);
            Assert.Equal(112.48M, result.MpCollected);
            Assert.Equal(0.00M, result.MpCancelled);
            Assert.Equal(0.00M, result.MpUnCollectable);
            Assert.Equal(0.00M, result.MpWrittenOff);
            Assert.Equal(337.47M, result.MpToBeCollected);
            Assert.Equal(566.93M, result.TotalGrossRevenue);
            Assert.Equal(566.92M, result.tstTotalGrossRevenue + result.TotalLineItemDiscounts);
            Assert.Equal(954.43M, result.TotalSinglePayGrossRevenue);
            Assert.Equal(22.50M, result.TotalDiscounts);
            Assert.Equal(861.93M, result.TotalNetRevenue);  //
            Assert.Equal(524.43M, result.TotalNetCash);
            Assert.Equal(389.97M, result.TotalMargin);
            Assert.Equal(22.50M, result.TotalSalesTax);
            Assert.Equal(4.99M, result.TotalShipping);
            Assert.Equal(550.92M, result.TotalCollectedWebSite);
            Assert.Equal(550.92M, result.TotalCollectedCC); //552.92?? sum of all transactions
            Assert.Equal(0M, result.LineItemDiscountReconcile);
            Assert.Equal(-0.01M, result.adjGrossRevRevenueReconcile);
            Assert.Equal(0.01M, result.GrossReconcile); //order 97388
            Assert.Equal(1, result.GrossMismatchList.Count); //order 97388
            Assert.Equal(97388, result.GrossMismatchList[0].OrderID); //order 97388
            Assert.Equal(2, result.mpTransactionList.Count);
            Assert.Equal(97390, result.mpTransactionList[0].OrderId);
            Assert.Equal(6, result.TransAmountsFromPriorMonths.Length);
            Assert.Equal(0M, result.TransAmountsFromPriorMonths[0]);
        }

        [Fact]
        public void DSRController_Index_StatsReturnsCorrect()
        {
            //Arrange
            DSRController controller = DSRControllerFactory();

            // Act
            ViewResult result = controller.Index(null) as ViewResult;
            SalesViewModel SalesVM = new SalesViewModel();
            SalesVM = (SalesViewModel)result.Model;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model
            Assert.Equal(499.98M, SalesVM.SalesPrimary.Mp1PayRevenue);

        }
    }
}
