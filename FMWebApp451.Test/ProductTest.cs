﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using FM2015.ViewModels;
using FM2015.Controllers;
using AdminCart;
using FMWebApp451.Respositories;
using System.Web.Routing;

namespace FMWebApp451.Test
{

    public class ProductTest
    {
        //https://msdn.microsoft.com/en-us/library/gg416511(VS.98).aspx

        [Fact]
        public void ProductProvider_SaveIDEqual1_Returns0()
        {
            //Arrange
            Product p = new Product();
            p.ProductID = 1;
            TestProductProvider testProvider = new TestProductProvider();

            // Act
            int result = testProvider.Save(p);

            // Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void ProductProvider_InitProducts_ReturnsList()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();

            // Act
            List<Product> resultList = TestProductProvider.InitProductList();

            // Assert
            Assert.Equal(11, resultList.Count);
            Assert.Equal(1, resultList[0].ProductID);
        }

        [Fact]
        public void ProductProvider_GetById0_ReturnsProductID0()
        {
            //Arrange
            int productId = 0;
            TestProductProvider testProvider = new TestProductProvider();

            // Act
            Product result = testProvider.GetById(productId);

            // Assert
            Assert.Equal(0, result.ProductID);
        }

        [Fact]
        public void ProductProvider_GetById1_ReturnsProductID1()
        {
            //Arrange
            int productId = 1;
            TestProductProvider testProvider = new TestProductProvider();

            // Act
            Product result = testProvider.GetById(productId);

            // Assert
            Assert.Equal(1, result.ProductID);
        }

        [Fact]
        public void ProductProvider_GetBySkuBadSku_ReturnsProductID0()
        {
            //Arrange
            string sku ="test";
            TestProductProvider testProvider = new TestProductProvider();

            // Act
            Product result = testProvider.GetBySku(sku);

            // Assert
            Assert.Equal(0, result.ProductID);
        }

        [Fact]
        public void ProductProvider_GetBySkuGoodSku_ReturnsProductID0()
        {
            //Arrange
            string sku = "FM-0001";
            TestProductProvider testProvider = new TestProductProvider();

            // Act
            Product result = testProvider.GetBySku(sku);

            // Assert
            Assert.Equal(1, result.ProductID);
        }

        [Fact]
        public void Index_DoesExist()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void Index_ReturnsGoodViewResult()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model

            List<Product> pList = (List<Product>)result.ViewData.Model;
            Assert.Equal(8, pList.Count);
            Assert.Equal(2, pList[0].ProductID);
            Assert.Equal(3, pList[1].ProductID);
            Assert.True(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Index");
        }

        [Fact]
        public void AdminIndex_DoesExist()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);

            // Act
            ViewResult result = controller.AdminIndex() as ViewResult;

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void AdminIndex_ReturnsGoodViewResult()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);

            // Act
            ViewResult result = controller.AdminIndex() as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model

            List<Product> pList = (List<Product>)result.ViewData.Model;
            Assert.Equal(11, pList.Count);
            Assert.Equal(1, pList[0].ProductID);
            Assert.Equal(2, pList[1].ProductID);
            Assert.Equal(3, pList[2].ProductID);
            Assert.True(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "AdminIndex");
        }

        [Fact]
        public void Details_DoesExist()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);
            int productId = 2;

            // Act
            ViewResult result = controller.Details(productId) as ViewResult;

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void Details_ValidIdReturnsGoodViewResult()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);
            int productId = 2;

            // Act
            ViewResult result = controller.Details(productId) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model

            Product product = (Product)result.ViewData.Model;
            Assert.Equal(2, product.ProductID);
            Assert.True(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Details");
        }

        [Fact]
        public void Details_Id0ReturnsId0()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);
            int productId = 0;

            // Act
            ViewResult result = controller.Details(productId) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model

            Product product = (Product)result.ViewData.Model;
            Assert.Equal(0, product.ProductID);
        }

        [Fact]
        public void Edit_DoesExist()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);
            int productId = 0;

            // Act
            ViewResult result = controller.Edit(productId) as ViewResult;

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void GenCategorySelectList_ReturnsValidSelectList()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);
            List<SelectListItem> selectedItemsList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "n/a", Value = "n/a" },
                new SelectListItem { Text = "device", Value = "device" },
                new SelectListItem { Text = "multipay", Value = "multipay" },
                new SelectListItem { Text = "club", Value = "club" },
                new SelectListItem { Text = "accessory", Value = "accessory" }
            };

            // Act
            List<SelectListItem> result = controller.GenCategorySelectList() as List<SelectListItem>;

            // Assert
            Assert.NotNull(result);
            Assert.Equal(selectedItemsList.Count(), result.Count());
            for (var i = 0; i < selectedItemsList.Count(); i++)
            {
                Assert.Equal(selectedItemsList.ElementAt(i).Value, result.ElementAt(i).Value);
                Assert.Equal(selectedItemsList.ElementAt(i).Text, result.ElementAt(i).Text);
            }
        }

        [Fact]
        public void Edit_ValidIdReturnsGoodViewResult()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);
            int productId = 2;

            List<SelectListItem> selectedItemsList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "n/a", Value = "n/a" },
                new SelectListItem { Text = "device", Value = "device" },
                new SelectListItem { Text = "multipay", Value = "multipay" },
                new SelectListItem { Text = "club", Value = "club" },
                new SelectListItem { Text = "accessory", Value = "accessory" }
            };

            // Act
            ViewResult viewResult = controller.Edit(productId) as ViewResult;
            ProductViewModel pvmResult = (ProductViewModel)viewResult.Model;

            // Assert
            Assert.NotNull(viewResult);
            Assert.NotNull(viewResult.Model); // add additional checks on the Model

            Assert.Equal(2, pvmResult.Product.ProductID);
            Assert.True(string.IsNullOrEmpty(viewResult.ViewName) || viewResult.ViewName == "Edit");

            Assert.NotNull(pvmResult.Categories);
            Assert.Equal(selectedItemsList.Count(), pvmResult.Categories.Count());
            for (var i = 0; i < selectedItemsList.Count(); i++)
            {
                Assert.Equal(selectedItemsList.ElementAt(i).Value, pvmResult.Categories.ElementAt(i).Value);
                Assert.Equal(selectedItemsList.ElementAt(i).Text, pvmResult.Categories.ElementAt(i).Text);
            }
        }

        [Fact]
        public void Edit_Post_IdZeroReturnsException()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);
            ProductViewModel pvm = new ProductViewModel();
            int productId = 0;

            // Act
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => controller.Edit(productId, pvm));

            // Assert
            //Assert.Equal("Value does not fall within the expected range.", ex.Message);
            Assert.NotEmpty(ex.Message);
        }

        [Fact]
        public void Edit_Post_IdNegReturnsException()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);
            ProductViewModel pvm = new ProductViewModel();
            int productId = -1;

            // Act
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => controller.Edit(productId, pvm));

            // Assert
            //Assert.Equal("Value does not fall within the expected range.", ex.Message);
            Assert.NotEmpty(ex.Message);
        }

        [Fact]
        public void Edit_Post_ValidIdandPVMReturnsRedirect()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);
            RouteCollection routes = new RouteCollection();
            ProductViewModel pvm = new ProductViewModel();
            int productId = 1;

            // Act
            RedirectToRouteResult result = controller.Edit(productId, pvm) as RedirectToRouteResult;

            // Assert
            Assert.NotNull(result.RouteValues);
            Assert.Equal("Details", result.RouteValues["action"]);
            Assert.Equal("Products", result.RouteValues["controller"]);
            Assert.Equal(1, result.RouteValues["id"]);
        }

        [Fact]
        public void Clone_DoesExist()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);
            int productId = 0;

            // Act
            RedirectToRouteResult result = controller.Clone(productId) as RedirectToRouteResult;

            // Assert
            Assert.Equal("Edit", result.RouteValues["action"]);
            Assert.Equal("Products", result.RouteValues["controller"]);
        }

        [Fact]
        public void Create_DoesExist()
        {
            //Arrange
            TestProductProvider testProvider = new TestProductProvider();
            TestProductCostProvider testCostProvider = new TestProductCostProvider();
            ProductsController controller = new ProductsController(testProvider, testCostProvider);

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model
        }
    }
}
