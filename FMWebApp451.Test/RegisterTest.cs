﻿using System;
using System.Web.Mvc;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using FM2015.Models;
using FM2015.Controllers;
using FMWebApp451.Respositories;
using FM2015.ViewModels;
using FMWebApp451.Controllers;

namespace FMWebApp451.Test
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    
    public class RegisterTest
    {
        public static RegistrationController RegistrationControllerFactory()
        {
            TestRegisterProvider registerProvider = new TestRegisterProvider();
            return new RegistrationController(registerProvider);
        }

        [Fact]
        public void Index_DoesExist()
        {
            ///Arrange
            RegistrationController controller = RegistrationControllerFactory();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model
        }

        
    }
}
