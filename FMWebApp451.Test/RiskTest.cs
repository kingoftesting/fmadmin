﻿using System;
using System.Collections.Generic;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using ShopifyAgent;
using FMWebApp451.Respositories;

namespace FMWebApp451.Test
{

    public class RiskTest
    {
        [Fact]
        public void GetLast7Dayl_Returns_List()
        {
            //Arrange
            TestRiskProvider testRiskProvider = new TestRiskProvider();

            // Act
            List<ShopOrderRisk> resultList = testRiskProvider.GetAll();

            // Assert
            Assert.Equal(4, resultList.Count);
        }

        [Fact]
        public void GetByShopifyId_Returns_List()
        {
            //Arrange
            TestRiskProvider testRiskProvider = new TestRiskProvider();

            // Act
            ShopOrderRisk result = testRiskProvider.GetByShopifyName(1);

            // Assert
            Assert.Equal(1, result.Order_id);
            Assert.Equal("investigate", result.Recommendation);
            Assert.Equal(0.50M, result.Score);
        }
    }
}
