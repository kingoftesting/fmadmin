﻿using System;
using System.Web;
using System.Web.Routing;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using Moq;
using System.Web.Mvc;

namespace FMWebApp451.Test
{

    public class RoutesTest
    {
        [Fact]
        public void DefaultView_ShouldReturnCorrectRouteDataWithValues()
        {
            //Arrange
            RouteCollection routes = new RouteCollection();
            FMWebApp451.RouteConfig.RegisterRoutes(routes);
            var httpContextMock = new Mock<HttpContextBase>();

            httpContextMock.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/");

            //Act
            RouteData routeData = routes.GetRouteData(httpContextMock.Object);

            //Assert
            Assert.NotNull(routeData);
            Assert.Equal("Home", routeData.Values["Controller"]);
            Assert.Equal("Index", routeData.Values["Action"]);
            Assert.Equal(UrlParameter.Optional, routeData.Values["id"]);
        }

        [Fact]
        public void Home_IndexView_ShouldReturnCorrectRouteDataWithValues()
        {
            //Arrange
            RouteCollection routes = new RouteCollection();
            FMWebApp451.RouteConfig.RegisterRoutes(routes);
            var httpContextMock = new Mock<HttpContextBase>();

            httpContextMock.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Home");

            //Act
            RouteData routeData = routes.GetRouteData(httpContextMock.Object);

            //Assert
            Assert.NotNull(routeData);
            Assert.Equal("Home", routeData.Values["Controller"]);
            Assert.Equal("Index", routeData.Values["Action"]);
            Assert.Equal(UrlParameter.Optional, routeData.Values["id"]);
        }

        [Fact]
        public void Home_AboutView_ShouldReturnCorrectRouteDataWithValues()
        {
            //Arrange
            RouteCollection routes = new RouteCollection();
            FMWebApp451.RouteConfig.RegisterRoutes(routes);
            var httpContextMock = new Mock<HttpContextBase>();

            httpContextMock.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Home/About");

            //Act
            RouteData routeData = routes.GetRouteData(httpContextMock.Object);

            //Assert
            Assert.NotNull(routeData);
            Assert.Equal("Home", routeData.Values["Controller"]);
            Assert.Equal("About", routeData.Values["Action"]);
            Assert.Equal(UrlParameter.Optional, routeData.Values["id"]);
        }

        [Fact]
        public void Home_ContactView_ShouldReturnCorrectRouteDataWithValues()
        {
            //Arrange
            RouteCollection routes = new RouteCollection();
            FMWebApp451.RouteConfig.RegisterRoutes(routes);
            var httpContextMock = new Mock<HttpContextBase>();

            httpContextMock.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Home/Contact");

            //Act
            RouteData routeData = routes.GetRouteData(httpContextMock.Object);

            //Assert
            Assert.NotNull(routeData);
            Assert.Equal("Home", routeData.Values["Controller"]);
            Assert.Equal("Contact", routeData.Values["Action"]);
            Assert.Equal(UrlParameter.Optional, routeData.Values["id"]);
        }

        [Fact]
        public void Products_DefaultView_ShouldReturnCorrectRouteDataWithValues()
        {
            //Arrange
            RouteCollection routes = new RouteCollection();
            FMWebApp451.RouteConfig.RegisterRoutes(routes);
            var httpContextMock = new Mock<HttpContextBase>();

            httpContextMock.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Products");

            //Act
            RouteData routeData = routes.GetRouteData(httpContextMock.Object);

            //Assert
            Assert.NotNull(routeData);
            Assert.Equal("Products", routeData.Values["Controller"]);
            Assert.Equal("Index", routeData.Values["Action"]);
            Assert.Equal(UrlParameter.Optional, routeData.Values["id"]);
        }

        [Fact]
        public void Products_IndexView_ShouldReturnCorrectRouteDataWithValues()
        {
            //Arrange
            RouteCollection routes = new RouteCollection();
            FMWebApp451.RouteConfig.RegisterRoutes(routes);
            var httpContextMock = new Mock<HttpContextBase>();

            httpContextMock.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Products/Index");

            //Act
            RouteData routeData = routes.GetRouteData(httpContextMock.Object);

            //Assert
            Assert.NotNull(routeData);
            Assert.Equal("Products", routeData.Values["Controller"]);
            Assert.Equal("Index", routeData.Values["Action"]);
            Assert.Equal(UrlParameter.Optional, routeData.Values["id"]);
        }

        [Fact]
        public void Products_AdminIndexView_ShouldReturnCorrectRouteDataWithValues()
        {
            //Arrange
            RouteCollection routes = new RouteCollection();
            FMWebApp451.RouteConfig.RegisterRoutes(routes);
            var httpContextMock = new Mock<HttpContextBase>();

            httpContextMock.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Products/AdminIndex");

            //Act
            RouteData routeData = routes.GetRouteData(httpContextMock.Object);

            //Assert
            Assert.NotNull(routeData);
            Assert.Equal("Products", routeData.Values["Controller"]);
            Assert.Equal("AdminIndex", routeData.Values["Action"]);
            Assert.Equal(UrlParameter.Optional, routeData.Values["id"]);
        }

        [Fact]
        public void Products_DetailsView_ShouldReturnCorrectRouteDataWithValues()
        {
            //Arrange
            RouteCollection routes = new RouteCollection();
            FMWebApp451.RouteConfig.RegisterRoutes(routes);
            var httpContextMock = new Mock<HttpContextBase>();

            httpContextMock.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Products/Details/1");

            //Act
            RouteData routeData = routes.GetRouteData(httpContextMock.Object);

            //Assert
            Assert.NotNull(routeData);
            Assert.Equal("Products", routeData.Values["Controller"]);
            Assert.Equal("Details", routeData.Values["Action"]);
            Assert.Equal("1", routeData.Values["id"]);
        }

        [Fact]
        public void Products_EditView_ShouldReturnCorrectRouteDataWithValues()
        {
            //Arrange
            RouteCollection routes = new RouteCollection();
            FMWebApp451.RouteConfig.RegisterRoutes(routes);
            var httpContextMock = new Mock<HttpContextBase>();

            httpContextMock.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Products/Edit/1");

            //Act
            RouteData routeData = routes.GetRouteData(httpContextMock.Object);

            //Assert
            Assert.NotNull(routeData);
            Assert.Equal("Products", routeData.Values["Controller"]);
            Assert.Equal("Edit", routeData.Values["Action"]);
            Assert.Equal("1", routeData.Values["id"]);
        }

        [Fact]
        public void Products_CreateView_ShouldReturnCorrectRouteDataWithValues()
        {
            //Arrange
            RouteCollection routes = new RouteCollection();
            FMWebApp451.RouteConfig.RegisterRoutes(routes);
            var httpContextMock = new Mock<HttpContextBase>();

            httpContextMock.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Products/Create/1");

            //Act
            RouteData routeData = routes.GetRouteData(httpContextMock.Object);

            //Assert
            Assert.NotNull(routeData);
            Assert.Equal("Products", routeData.Values["Controller"]);
            Assert.Equal("Create", routeData.Values["Action"]);
            Assert.Equal("1", routeData.Values["id"]);
        }

        [Fact]
        public void Products_CloneView_ShouldReturnCorrectRouteDataWithValues()
        {
            //Arrange
            RouteCollection routes = new RouteCollection();
            FMWebApp451.RouteConfig.RegisterRoutes(routes);
            var httpContextMock = new Mock<HttpContextBase>();

            httpContextMock.Setup(x => x.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Products/Clone/1");

            //Act
            RouteData routeData = routes.GetRouteData(httpContextMock.Object);

            //Assert
            Assert.NotNull(routeData);
            Assert.Equal("Products", routeData.Values["Controller"]);
            Assert.Equal("Clone", routeData.Values["Action"]);
            Assert.Equal("1", routeData.Values["id"]);
        }
    }
}
