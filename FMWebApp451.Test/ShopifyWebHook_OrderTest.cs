﻿using System;
using System.Collections.Generic;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using Newtonsoft.Json;


namespace FMWebApp451.Test
{

    public class ShopifyWebHook_OrderTest
    {

        [Fact]
        public void TestOrderData_Returns_ShopifyOrder()
        {
            //Arrange
            string webHookValue = testJSON_testOrder;

            // Act
            ShopifyAgent.ShopifyOrder result = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyOrder>(webHookValue);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(123456, result.Id);
            Assert.Equal("jon@doe.ca", result.Email);
            Assert.NotNull(result.LineItems);
            Assert.Equal(2, result.LineItems.Length);
            Assert.Equal(1225817025, result.LineItems[0].Id);
            Assert.Equal("FM-5010", result.LineItems[0].Sku);
            Assert.Equal(19, result.LineItems[1].Id);
            Assert.Equal("FM-5170", result.LineItems[1].Sku);
        }

        [Fact]
        public void RealOrderData_Returns_ShopifyOrder()
        {
            //Arrange
            string webHookValue = testJSON_order;

            // Act
            ShopifyAgent.ShopifyOrder result = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyOrder>(webHookValue);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(5050797517, result.Id);
            Assert.NotNull(result.LineItems);
            Assert.Equal(1, result.LineItems.Length);
            Assert.Equal(9021860301, result.LineItems[0].Id);
        }

        [Fact]
        public void RealOrderData_Transactions_ShopifyOrder()
        {
            //Arrange
            string webHookValue = testJSON_orderTransactions;

            // Act
            ShopifyAgent.ShopifyOrder result = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyOrder>(webHookValue);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(5070527629, result.Id);
            Assert.NotNull(result.LineItems);
            Assert.Equal(1, result.LineItems.Length);
            Assert.Equal(9058203789, result.LineItems[0].Id);
            Assert.Equal("•••• •••• •••• 8774", result.payment_details.Credit_card_number);
            Assert.NotNull(result.Refunds);
            Assert.Equal(5070527629, result.Refunds[0].Order_id);
            Assert.NotNull(result.Refunds[0].Refund_line_items);
            Assert.Equal(9058203789, result.Refunds[0].Refund_line_items[0].Line_item_id);
            Assert.Equal("FaceMaster Platinum Facial Toning System - RF", result.Refunds[0].Refund_line_items[0].Line_item.Title);
            Assert.NotNull(result.Refunds[0].Transactions);
            Assert.Equal(1, result.Refunds[0].Transactions.Length);
            Assert.Equal(5522489485, result.Refunds[0].Transactions[0].Id);
            Assert.Equal(199.99M, result.Refunds[0].Transactions[0].Amount);
            //Assert.Equal(199.99M, result.Refunds[0].Transactions[0].);
            //Assert.Equal("ch_19mO2XLfRlOEM9uuY7JSTKd8", result.Transactions[0].Charge.Id);
        }

        public static string testJSON_order = @"
{
  ""id"": 5050797517,
  ""email"": ""Jmrigotti@publishpath.net"",
  ""closed_at"": null,
  ""created_at"": ""2017-02-08T11:12:02-08:00"",
  ""updated_at"": ""2017-02-08T11:12:02-08:00"",
  ""number"": 8519,
  ""note"": """",
  ""token"": ""76743b1b309fb09a01dc85d97a87f7e5"",
  ""gateway"": ""cash"",
  ""test"": false,
  ""total_price"": ""62.49"",
  ""subtotal_price"": ""62.49"",
  ""total_weight"": 0,
  ""total_tax"": ""0.00"",
  ""taxes_included"": false,
  ""currency"": ""USD"",
  ""financial_status"": ""paid"",
  ""confirmed"": true,
  ""total_discounts"": ""0.00"",
  ""total_line_items_price"": ""62.49"",
  ""cart_token"": null,
  ""buyer_accepts_marketing"": false,
  ""name"": ""809519"",
  ""referring_site"": null,
  ""landing_site"": null,
  ""cancelled_at"": null,
  ""cancel_reason"": null,
  ""total_price_usd"": null,
  ""checkout_token"": null,
  ""reference"": null,
  ""user_id"": null,
  ""location_id"": null,
  ""source_identifier"": null,
  ""source_url"": null,
  ""processed_at"": ""2017-02-08T11:12:02-08:00"",
  ""device_id"": null,
  ""browser_ip"": null,
  ""landing_site_ref"": null,
  ""order_number"": 9519,
  ""discount_codes"": [],
  ""note_attributes"": [],
  ""payment_gateway_names"": [],
  ""processing_method"": """",
  ""checkout_id"": null,
  ""source_name"": ""273785"",
  ""fulfillment_status"": null,
  ""tax_lines"": [],
  ""tags"": ""ch_19l0rAINMTFgK3cee5XdYOSd"",
  ""contact_email"": ""Jmrigotti@publishpath.net"",
  ""order_status_url"": null,
  ""line_items"": [
    {
      ""id"": 9021860301,
      ""variant_id"": 4284190017,
      ""title"": ""FaceMaster System 4 Easy Payments"",
      ""quantity"": 1,
      ""price"": ""62.49"",
      ""grams"": 966,
      ""sku"": ""FM-5010"",
      ""variant_title"": ""FaceMaster System 4 Easy Payments"",
      ""vendor"": ""Paywhirl"",
      ""fulfillment_service"": ""manual"",
      ""product_id"": 1429202497,
      ""requires_shipping"": true,
      ""taxable"": true,
      ""gift_card"": false,
      ""name"": ""FaceMaster System 4 Easy Payments - FaceMaster System 4 Easy Payments"",
      ""variant_inventory_management"": null,
      ""properties"": [],
      ""product_exists"": true,
      ""fulfillable_quantity"": 1,
      ""total_discount"": ""0.00"",
      ""fulfillment_status"": null,
      ""tax_lines"": []
    }
  ],
  ""shipping_lines"": [],
  ""shipping_address"": {
    ""first_name"": ""Jan"",
    ""address1"": ""127 Liberty Way  "",
    ""phone"": ""3193296233"",
    ""city"": ""Waxahachie "",
    ""zip"": ""75167"",
    ""province"": ""Texas"",
    ""country"": ""United States"",
    ""last_name"": ""Rigotti"",
    ""address2"": null,
    ""company"": null,
    ""latitude"": null,
    ""longitude"": null,
    ""name"": ""Jan Rigotti"",
    ""country_code"": ""US"",
    ""province_code"": ""TX""
  },
  ""fulfillments"": [],
  ""refunds"": [],
  ""customer"": {
    ""id"": 5382272205,
    ""email"": ""Jmrigotti@publishpath.net"",
    ""accepts_marketing"": false,
    ""created_at"": ""2017-02-08T11:11:55-08:00"",
    ""updated_at"": ""2017-02-08T11:12:02-08:00"",
    ""first_name"": ""Jan"",
    ""last_name"": ""Rigotti"",
    ""orders_count"": 1,
    ""state"": ""enabled"",
    ""total_spent"": ""0.00"",
    ""last_order_id"": 5050797517,
    ""note"": null,
    ""verified_email"": true,
    ""multipass_identifier"": null,
    ""tax_exempt"": false,
    ""phone"": null,
    ""tags"": ""facemaster-system-4-easy-payments, Paywhirl"",
    ""last_order_name"": ""809519"",
    ""default_address"": {
      ""id"": 5384798157,
      ""first_name"": ""Jan"",
      ""last_name"": ""Rigotti"",
      ""company"": null,
      ""address1"": ""127 Liberty Way  "",
      ""address2"": null,
      ""city"": ""Waxahachie "",
      ""province"": ""Texas"",
      ""country"": ""United States"",
      ""zip"": ""75167"",
      ""phone"": ""3193296233"",
      ""name"": ""Jan Rigotti"",
      ""province_code"": ""TX"",


      ""country_code"": ""US"",
      ""country_name"": ""United States"",
      ""default"": true
    }
  }
}
  ";

        public static string testJSON_testOrder = @"
{
""id"": 123456,
  ""email"": ""jon@doe.ca"",
  ""closed_at"": null,
  ""created_at"": ""2017-02-11T15:59:25-08:00"",
  ""updated_at"": ""2017-02-11T15:59:25-08:00"",
  ""number"": 234,
  ""note"": null,
  ""token"": ""123456abcd"",
  ""gateway"": null,
  ""test"": true,
  ""total_price"": ""274.98"",
  ""subtotal_price"": ""264.98"",
  ""total_weight"": 0,
  ""total_tax"": ""0.00"",
  ""taxes_included"": false,
  ""currency"": ""USD"",
  ""financial_status"": ""voided"",
  ""confirmed"": false,
  ""total_discounts"": ""5.00"",
  ""total_line_items_price"": ""269.98"",
  ""cart_token"": null,
  ""buyer_accepts_marketing"": true,
  ""name"": ""#9999"",
  ""referring_site"": null,
  ""landing_site"": null,
  ""cancelled_at"": ""2017-02-11T15:59:25-08:00"",


  ""cancel_reason"": ""customer"",
  ""total_price_usd"": null,
  ""checkout_token"": null,
  ""reference"": null,
  ""user_id"": null,
  ""location_id"": null,
  ""source_identifier"": null,
  ""source_url"": null,
  ""processed_at"": null,
  ""device_id"": null,
  ""browser_ip"": null,
  ""landing_site_ref"": null,
  ""order_number"": 1234,
  ""discount_codes"": [],
  ""note_attributes"": [],
  ""payment_gateway_names"": [
    ""visa"",
    ""bogus""
  ],
  ""processing_method"": """",
  ""checkout_id"": null,
  ""source_name"": ""web"",
  ""fulfillment_status"": ""pending"",
  ""tax_lines"": [],
  ""tags"": """",
  ""contact_email"": ""jon@doe.ca"",
  ""order_status_url"": null,
  ""line_items"": [
    {
      ""id"": 1225817025,
      ""variant_id"": null,
      ""title"": ""FaceMaster Platinum Facial Toning System"",
      ""quantity"": 1,
      ""price"": ""249.99"",
      ""grams"": 966,
      ""sku"": ""FM-5010"",
      ""variant_title"": null,
      ""vendor"": null,
      ""fulfillment_service"": ""manual"",
      ""product_id"": 467391081,
      ""requires_shipping"": true,
      ""taxable"": true,
      ""gift_card"": false,
      ""name"": ""FaceMaster Platinum Facial Toning System"",
      ""variant_inventory_management"": null,
      ""properties"": [],
      ""product_exists"": true,
      ""fulfillable_quantity"": 1,
      ""total_discount"": ""0.00"",
      ""fulfillment_status"": null,
      ""tax_lines"": []
    },
    {
      ""id"": 19,
      ""variant_id"": null,
      ""title"": ""FaceMaster Organic Glyco-Peptide Eye Firming Serum"",
      ""quantity"": 1,
      ""price"": ""19.99"",
      ""grams"": 68,
      ""sku"": ""FM-5170"",
      ""variant_title"": null,
      ""vendor"": null,
      ""fulfillment_service"": ""manual"",
      ""product_id"": 9019475341,
      ""requires_shipping"": true,
      ""taxable"": true,
      ""gift_card"": false,
      ""name"": ""FaceMaster Organic Glyco-Peptide Eye Firming Serum"",
      ""variant_inventory_management"": null,
      ""properties"": [],
      ""product_exists"": true,
      ""fulfillable_quantity"": 1,
      ""total_discount"": ""5.00"",
      ""fulfillment_status"": null,
      ""tax_lines"": []
}
  ],
  ""shipping_lines"": [
    {
      ""id"": 1234567,
      ""title"": ""Generic Shipping"",
      ""price"": ""10.00"",
      ""code"": null,
      ""source"": ""shopify"",
      ""phone"": null,
      ""requested_fulfillment_service_id"": null,
      ""delivery_category"": null,
      ""carrier_identifier"": null,
      ""tax_lines"": []
    }
  ],
  ""billing_address"": {
    ""first_name"": ""Bob"",
    ""address1"": ""123 Billing Street"",
    ""phone"": ""555-555-BILL"",
    ""city"": ""Billtown"",
    ""zip"": ""K2P0B0"",
    ""province"": ""Kentucky"",
    ""country"": ""United States"",
    ""last_name"": ""Biller"",
    ""address2"": null,
    ""company"": ""My Company"",
    ""latitude"": null,
    ""longitude"": null,
    ""name"": ""Bob Biller"",
    ""country_code"": ""US"",
    ""province_code"": ""KY""
  },
  ""shipping_address"": {
    ""first_name"": ""Steve"",
    ""address1"": ""123 Shipping Street"",
    ""phone"": ""555-555-SHIP"",
    ""city"": ""Shippington"",
    ""zip"": ""K2P0S0"",
    ""province"": ""Kentucky"",
    ""country"": ""United States"",
    ""last_name"": ""Shipper"",
    ""address2"": null,
    ""company"": ""Shipping Company"",
    ""latitude"": null,
    ""longitude"": null,
    ""name"": ""Steve Shipper"",
    ""country_code"": ""US"",
    ""province_code"": ""KY""
  },
  ""fulfillments"": [],
  ""refunds"": [],
  ""customer"": {
    ""id"": 1234567,
    ""email"": ""john@test.com"",
    ""accepts_marketing"": false,
    ""created_at"": null,
    ""updated_at"": null,
    ""first_name"": ""John"",
    ""last_name"": ""Smith"",
    ""orders_count"": 0,
    ""state"": ""disabled"",
    ""total_spent"": ""0.00"",
    ""last_order_id"": null,
    ""note"": null,
    ""verified_email"": true,
    ""multipass_identifier"": null,
    ""tax_exempt"": false,
    ""phone"": null,
    ""tags"": """",
    ""last_order_name"": null,
    ""default_address"": {
      ""id"": 1234567,
      ""first_name"": null,
      ""last_name"": null,
      ""company"": null,
      ""address1"": ""123 Elm St."",
      ""address2"": null,
      ""city"": ""Ottawa"",
      ""province"": ""Ontario"",
      ""country"": ""Canada"",
      ""zip"": ""K2H7A8"",
      ""phone"": ""123-123-1234"",
      ""name"": """",
      ""province_code"": ""ON"",
      ""country_code"": ""CA"",
      ""country_name"": ""Canada"",
      ""default"": false
    }
  }
}
";

        public static string testJSON_orderTransactions = @"
{
  ""id"": 5070527629,
  ""email"": ""rdaniel024@sbcglobal.net"",
  ""closed_at"": null,
  ""created_at"": ""2017-02-12T06:09:27-08:00"",
  ""updated_at"": ""2017-02-12T09:51:22-08:00"",
  ""number"": 8649,
  ""note"": """",
  ""token"": ""17062aa9727a3fbb4c6dc8cc018ff294"",
  ""gateway"": ""shopify_payments"",
  ""test"": false,
  ""total_price"": ""199.99"",
  ""subtotal_price"": ""199.99"",
  ""total_weight"": 967,
  ""total_tax"": ""0.00"",
  ""taxes_included"": false,
  ""currency"": ""USD"",
  ""financial_status"": ""refunded"",
  ""confirmed"": true,
  ""total_discounts"": ""0.00"",
  ""total_line_items_price"": ""199.99"",
  ""cart_token"": ""cb0c31f4013faa31b45d8851f70659d6"",
  ""buyer_accepts_marketing"": true,
  ""name"": ""809649"",
  ""referring_site"": ""http://m.facebook.com"",
  ""landing_site"": ""/products/facemaster-platinum-facial-toning-system-rf?utm_source=facebook&utm_medium=paid social&utm_content=FacemasterVideo_prodthumbnail&utm_campaign=RF-FMcom Visitor LL1pct US_W_50plus"",
  ""cancelled_at"": ""2017-02-12T09:50:53-08:00"",
  ""cancel_reason"": ""other"",
  ""total_price_usd"": ""199.99"",
  ""checkout_token"": ""6d9a9b9a2d80aef84b33ea1775ec3a69"",
  ""reference"": null,
  ""user_id"": null,
  ""location_id"": null,
  ""source_identifier"": null,
  ""source_url"": null,
  ""processed_at"": ""2017-02-12T06:09:27-08:00"",
  ""device_id"": null,
  ""browser_ip"": ""99.189.100.25"",
  ""landing_site_ref"": null,
  ""order_number"": 9649,
  ""discount_codes"": [],
  ""note_attributes"": [],
  ""payment_gateway_names"": [
    ""shopify_payments""
  ],
  ""processing_method"": ""direct"",
  ""checkout_id"": 16012396173,
  ""source_name"": ""web"",
  ""fulfillment_status"": null,
  ""tax_lines"": [],
  ""tags"": """",
  ""contact_email"": ""rdaniel024@sbcglobal.net"",
  ""order_status_url"": ""https://checkout.shopify.com/8354419/checkouts/6d9a9b9a2d80aef84b33ea1775ec3a69/thank_you_token?key=7daea12f49b9972700802f1a783816a4"",
  ""line_items"": [
    {
      ""id"": 9058203789,
      ""variant_id"": 30023011213,
      ""title"": ""FaceMaster Platinum Facial Toning System - RF"",
      ""quantity"": 1,
      ""price"": ""199.99"",
      ""grams"": 966,
      ""sku"": ""FM-5010R"",
      ""variant_title"": """",
      ""vendor"": ""Facemaster"",
      ""fulfillment_service"": ""manual"",
      ""product_id"": 8752275661,
      ""requires_shipping"": true,
      ""taxable"": true,
      ""gift_card"": false,
      ""name"": ""FaceMaster Platinum Facial Toning System - RF"",
      ""variant_inventory_management"": ""shopify"",
      ""properties"": [],
      ""product_exists"": true,
      ""fulfillable_quantity"": 0,
      ""total_discount"": ""0.00"",
      ""fulfillment_status"": null,
      ""tax_lines"": [],
      ""origin_location"": {
        ""id"": 281545537,
        ""country_code"": ""US"",
        ""province_code"": ""CA"",
        ""name"": ""FaceMaster"",
        ""address1"": ""23961 Craftsman Road, Suite I"",
        ""address2"": """",
        ""city"": ""Calabasas"",
        ""zip"": ""91302""
      },
      ""destination_location"": {
        ""id"": 2919673485,
        ""country_code"": ""US"",
        ""province_code"": ""OK"",
        ""name"": ""Renee Daniel"",
        ""address1"": ""10404 N 117th E Ave"",
        ""address2"": """",
        ""city"": ""Owasso"",
        ""zip"": ""74055""
      }
    }
  ],
  ""shipping_lines"": [
    {
      ""id"": 4154389773,
      ""title"": ""Free Standard US Contiguous Shipping (Purchases Over $100)"",
      ""price"": ""0.00"",
      ""code"": ""Free Standard US Contiguous Shipping (Purchases Over $100)"",
      ""source"": ""shopify"",
      ""phone"": null,
      ""requested_fulfillment_service_id"": null,
      ""delivery_category"": null,
      ""carrier_identifier"": null,
      ""tax_lines"": []
    }
  ],
  ""billing_address"": {
    ""first_name"": ""Renee"",
    ""address1"": ""10404 N 117th E Ave"",
    ""phone"": ""(918) 261-1729"",
    ""city"": ""Owasso"",
    ""zip"": ""74055"",
    ""province"": ""Oklahoma"",
    ""country"": ""United States"",
    ""last_name"": ""Daniel"",
    ""address2"": """",
    ""company"": null,
    ""latitude"": 36.30275,
    ""longitude"": -95.843099,
    ""name"": ""Renee Daniel"",
    ""country_code"": ""US"",
    ""province_code"": ""OK""
  },
  ""shipping_address"": {
    ""first_name"": ""Renee"",
    ""address1"": ""10404 N 117th E Ave"",
    ""phone"": ""(918) 261-1729"",
    ""city"": ""Owasso"",
    ""zip"": ""74055"",
    ""province"": ""Oklahoma"",
    ""country"": ""United States"",
    ""last_name"": ""Daniel"",
    ""address2"": """",
    ""company"": null,
    ""latitude"": 36.30275,
    ""longitude"": -95.843099,
    ""name"": ""Renee Daniel"",
    ""country_code"": ""US"",
    ""province_code"": ""OK""
  },
  ""fulfillments"": [],
  ""client_details"": {
    ""browser_ip"": ""99.189.100.25"",
    ""accept_language"": ""en-us"",
    ""user_agent"": ""Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Mobile/14D27 [FBAN/FBIOS;FBAV/79.0.0.44.69;FBBV/49363082;FBRV/0;FBDV/iPhone7,1;FBMD/iPhone;FBSN/iOS;FBSV/10.2.1;FBSS/3;FBCR/AT&T;FBID/phone;FBLC/en_US;FBOP/5]"",
    ""session_hash"": ""9a4379cd11a8436f42d5f8f8020c7885"",
    ""browser_width"": 375,
    ""browser_height"": 667
  },
  ""refunds"": [
    {
      ""id"": 206721421,
      ""order_id"": 5070527629,
      ""created_at"": ""2017-02-12T09:51:21-08:00"",
      ""note"": null,
      ""restock"": false,
      ""user_id"": null,
      ""processed_at"": ""2017-02-12T09:51:21-08:00"",
      ""refund_line_items"": [
        {
          ""id"": 176448781,
          ""quantity"": 1,
          ""line_item_id"": 9058203789,
          ""subtotal"": 199.99,
          ""total_tax"": 0.0,
          ""line_item"": {
            ""id"": 9058203789,
            ""variant_id"": 30023011213,
            ""title"": ""FaceMaster Platinum Facial Toning System - RF"",
            ""quantity"": 1,
            ""price"": ""199.99"",
            ""grams"": 966,
            ""sku"": ""FM-5010R"",
            ""variant_title"": """",
            ""vendor"": ""Facemaster"",
            ""fulfillment_service"": ""manual"",
            ""product_id"": 8752275661,
            ""requires_shipping"": true,
            ""taxable"": true,
            ""gift_card"": false,
            ""name"": ""FaceMaster Platinum Facial Toning System - RF"",
            ""variant_inventory_management"": ""shopify"",
            ""properties"": [],
            ""product_exists"": true,
            ""fulfillable_quantity"": 0,
            ""total_discount"": ""0.00"",
            ""fulfillment_status"": null,
            ""tax_lines"": [],
            ""origin_location"": {
              ""id"": 281545537,
              ""country_code"": ""US"",
              ""province_code"": ""CA"",
              ""name"": ""FaceMaster"",
              ""address1"": ""23961 Craftsman Road, Suite I"",
              ""address2"": """",
              ""city"": ""Calabasas"",
              ""zip"": ""91302""
            },
            ""destination_location"": {
              ""id"": 2919673485,
              ""country_code"": ""US"",
              ""province_code"": ""OK"",
              ""name"": ""Renee Daniel"",
              ""address1"": ""10404 N 117th E Ave"",
              ""address2"": """",
              ""city"": ""Owasso"",
              ""zip"": ""74055""
            }
          }
        }
      ],
      ""transactions"": [
        {
          ""id"": 5522489485,
          ""order_id"": 5070527629,
          ""amount"": ""199.99"",
          ""kind"": ""refund"",
          ""gateway"": ""shopify_payments"",
          ""status"": ""success"",
          ""message"": ""Transaction approved"",
          ""created_at"": ""2017-02-12T09:51:21-08:00"",
          ""test"": false,
          ""authorization"": ""re_19mRVNLfRlOEM9uu66jhPduq"",
          ""currency"": ""USD"",
          ""location_id"": null,
          ""user_id"": null,
          ""parent_id"": 5521614477,
          ""device_id"": null,
          ""receipt"": {
            ""id"": ""re_19mRVNLfRlOEM9uu66jhPduq"",
            ""object"": ""refund"",
            ""amount"": 19999,
            ""balance_transaction"": {
              ""id"": ""txn_19mRVOLfRlOEM9uuVgOriJS9"",
              ""object"": ""balance_transaction"",
              ""amount"": -19999,
              ""available_on"": 1487116800,
              ""created"": 1486921881,
              ""currency"": ""usd"",
              ""description"": ""REFUND FOR CHARGE (FaceMaster)"",
              ""fee"": 0,
              ""fee_details"": [],
              ""net"": -19999,
              ""source"": ""ch_19mO2XLfRlOEM9uuY7JSTKd8"",
              ""sourced_transfers"": {
                ""object"": ""list"",
                ""data"": [],
                ""has_more"": false,
                ""total_count"": 0,
                ""url"": ""/v1/transfers?source_transaction=re_19mRVNLfRlOEM9uu66jhPduq""
              },
              ""status"": ""pending"",
              ""type"": ""refund""
            },
            ""charge"": {
              ""id"": ""ch_19mO2XLfRlOEM9uuY7JSTKd8"",
              ""object"": ""charge"",
              ""amount"": 19999,
              ""amount_refunded"": 19999,
              ""application"": ""ca_1vQrdCwnvOuC2Ypn5R9whwXkGxb4XJjx"",
              ""application_fee"": ""fee_19mO2dLfRlOEM9uuH8mSkAVP"",
              ""balance_transaction"": {
                ""id"": ""txn_19mO2dLfRlOEM9uuq7vEH0tB"",
                ""object"": ""balance_transaction"",
                ""amount"": 19999,
                ""available_on"": 1487116800,
                ""created"": 1486908561,
                ""currency"": ""usd"",
                ""description"": ""FaceMaster"",
                ""fee"": 550,
                ""fee_details"": [
                  {
                    ""amount"": 550,
                    ""application"": ""ca_1vQrdCwnvOuC2Ypn5R9whwXkGxb4XJjx"",
                    ""currency"": ""usd"",
                    ""description"": ""Shopify Payments application fee"",
                    ""type"": ""application_fee""
                  }
                ],
                ""net"": 19449,
                ""source"": ""ch_19mO2XLfRlOEM9uuY7JSTKd8"",
                ""sourced_transfers"": {
                  ""object"": ""list"",
                  ""data"": [],
                  ""has_more"": false,
                  ""total_count"": 0,
                  ""url"": ""/v1/transfers?source_transaction=ch_19mO2XLfRlOEM9uuY7JSTKd8""
                },
                ""status"": ""pending"",
                ""type"": ""charge""
              },
              ""captured"": true,
              ""created"": 1486908561,
              ""currency"": ""usd"",
              ""customer"": null,
              ""description"": ""FaceMaster"",
              ""destination"": null,
              ""dispute"": null,
              ""failure_code"": null,
              ""failure_message"": null,
              ""fraud_details"": {},
              ""invoice"": null,
              ""livemode"": true,
              ""metadata"": {
                ""transaction_fee_total_amount"": ""550"",
                ""payments_transaction_fee_id"": ""1257159309"",
                ""payments_charge_id"": ""1271104333"",
                ""order_transaction_id"": ""5521614477"",
                ""email"": ""rdaniel024@sbcglobal.net"",
                ""order_id"": ""c16012396173.1""
              },
              ""on_behalf_of"": null,
              ""order"": null,
              ""outcome"": {
                ""network_status"": ""approved_by_network"",
                ""reason"": null,
                ""seller_message"": ""Payment complete."",
                ""type"": ""authorized""
              },
              ""paid"": true,
              ""receipt_email"": null,
              ""receipt_number"": null,
              ""refunded"": true,
              ""refunds"": {
                ""object"": ""list"",
                ""data"": [
                  {
                    ""id"": ""re_19mRVNLfRlOEM9uu66jhPduq"",
                    ""object"": ""refund"",
                    ""amount"": 19999,
                    ""balance_transaction"": ""txn_19mRVOLfRlOEM9uuVgOriJS9"",
                    ""charge"": ""ch_19mO2XLfRlOEM9uuY7JSTKd8"",
                    ""created"": 1486921881,
                    ""currency"": ""usd"",
                    ""metadata"": {
                      ""transaction_fee_total_amount"": ""550"",
                      ""payments_transaction_fee_id"": ""1257370061"",
                      ""order_transaction_id"": ""5522489485"",
                      ""payments_refund_id"": ""45307085""
                    },
                    ""reason"": null,
                    ""receipt_number"": null,
                    ""status"": ""succeeded""
                  }
                ],
                ""has_more"": false,
                ""total_count"": 1,
                ""url"": ""/v1/charges/ch_19mO2XLfRlOEM9uuY7JSTKd8/refunds""
              },
              ""review"": null,
              ""shipping"": null,
              ""source"": {
                ""id"": ""card_19mO2XLfRlOEM9uuVy3M6Irk"",
                ""object"": ""card"",
                ""address_city"": ""Owasso"",
                ""address_country"": ""US"",
                ""address_line1"": ""10404 N 117th E Ave"",
                ""address_line1_check"": ""pass"",
                ""address_line2"": null,
                ""address_state"": ""OK"",
                ""address_zip"": ""74055"",
                ""address_zip_check"": ""pass"",
                ""brand"": ""Visa"",
                ""country"": ""US"",
                ""customer"": null,
                ""cvc_check"": ""pass"",
                ""dynamic_last4"": null,
                ""exp_month"": 8,
                ""exp_year"": 2018,
                ""fingerprint"": ""Ccy8A1OGtYxERnjz"",
                ""funding"": ""credit"",
                ""last4"": ""8774"",
                ""metadata"": {},
                ""name"": ""Kathy Daniel"",
                ""tokenization_method"": null
              },
              ""source_transfer"": null,
              ""statement_descriptor"": null,
              ""status"": ""succeeded"",
              ""transfer_group"": null
            },
            ""created"": 1486921881,
            ""currency"": ""usd"",
            ""metadata"": {
              ""transaction_fee_total_amount"": ""550"",
              ""payments_transaction_fee_id"": ""1257370061"",
              ""order_transaction_id"": ""5522489485"",
              ""payments_refund_id"": ""45307085""
            },
            ""reason"": null,
            ""receipt_number"": null,
            ""status"": ""succeeded""
          },
          ""error_code"": null,
          ""source_name"": ""1047464""
        }
      ],
      ""order_adjustments"": []
    }
  ],
  ""payment_details"": {
    ""credit_card_bin"": ""447995"",
    ""avs_result_code"": ""Y"",
    ""cvv_result_code"": ""M"",
    ""credit_card_number"": ""•••• •••• •••• 8774"",
    ""credit_card_company"": ""Visa""
  },
  ""customer"": {
    ""id"": 5406908237,
    ""email"": ""rdaniel024@sbcglobal.net"",
    ""accepts_marketing"": true,
    ""created_at"": ""2017-02-12T06:07:08-08:00"",
    ""updated_at"": ""2017-02-12T09:51:22-08:00"",
    ""first_name"": ""Renee"",
    ""last_name"": ""Daniel"",
    ""orders_count"": 1,
    ""state"": ""disabled"",
    ""total_spent"": ""0.00"",
    ""last_order_id"": 5070527629,
    ""note"": null,
    ""verified_email"": true,
    ""multipass_identifier"": null,
    ""tax_exempt"": false,
    ""phone"": null,
    ""tags"": """",
    ""last_order_name"": ""809649"",
    ""default_address"": {
      ""id"": 5410567949,
      ""first_name"": ""Renee"",
      ""last_name"": ""Daniel"",
      ""company"": null,
      ""address1"": ""10404 N 117th E Ave"",
      ""address2"": """",
      ""city"": ""Owasso"",
      ""province"": ""Oklahoma"",
      ""country"": ""United States"",
      ""zip"": ""74055"",
      ""phone"": ""(918) 261-1729"",
      ""name"": ""Renee Daniel"",
      ""province_code"": ""OK"",
      ""country_code"": ""US"",
      ""country_name"": ""United States"",
      ""default"": true
    }
  }
}

";

    }
}
