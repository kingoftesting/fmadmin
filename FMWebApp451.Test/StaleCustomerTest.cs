﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using FM2015.Models;
using FM2015.Controllers;
using AdminCart;


namespace FMWebApp451.Test
{
    public class StaleCustomerTest
    {
        

        [Fact]
        public void provider_GetMany_ReturnsList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();

            // Act
            List<OrdersWithDetail> result = provider.GetMany(DateTime.Now.AddDays(-1));

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void provider_GetMany_FutureDateReturnsException()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddDays(1);

            // Act
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => provider.GetMany(startDate));

            // Assert
            Assert.NotEmpty(ex.Message);
        }

        [Fact]
        public void provider_GetMany_Minus1yrReturnsList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddMonths(-12);

            // Act
            List<OrdersWithDetail> resultList = provider.GetMany(startDate);

            // Assert
            Assert.Equal(8, resultList.Count);
        }

        [Fact]
        public void provider_GetMany_FullDataReturnsList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddMonths(-12);
            OrdersWithDetail obj = new OrdersWithDetail
            {
                OrdersWithDetailsId = 4,
                OrderDate = DateTime.Now.AddMonths(-11),
                CustomerId = 4,
                //ProductId = 134,
                IsFMSystem = true,
                FirstName = "rodger",
                LastName = "mohme",
                Email = "rmohme@alumni.uci.edu"
            };

            // Act
            List<OrdersWithDetail> resultList = provider.GetMany(startDate);
            OrdersWithDetail result = (from od in resultList where od.CustomerId == 4 select od).FirstOrDefault();

            // Assert
            Assert.Equal(obj.OrdersWithDetailsId, result.OrdersWithDetailsId);
            Assert.Equal(obj.OrderDate, result.OrderDate);
            Assert.Equal(obj.CustomerId, result.CustomerId);
            //Assert.Equal(obj.ProductId, result.ProductId);
            Assert.Equal(obj.IsFMSystem, result.IsFMSystem);
            Assert.Equal(obj.FirstName, result.FirstName);
            Assert.Equal(obj.LastName, result.LastName);
            Assert.Equal(obj.Email, result.Email);
        }

        [Fact]
        public void provider_GetMany_FMSysOnlyReturnsFMSysOnlyList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddMonths(-15);
            

            // Act
            List<OrdersWithDetail> fullList = provider.GetMany(startDate);
            List<OrdersWithDetail> fmOnlyList = (from od in fullList where od.IsFMSystem == true select od).ToList();
            List<OrdersWithDetail> resultList = (from od in fmOnlyList where od.IsFMSystem == false select od).ToList();

            // Assert
            Assert.NotEqual(0, fmOnlyList.Count);
            Assert.Equal(0, resultList.Count);
        }

        [Fact]
        public void provider_GetMany_FMSysNoneReturnsFMSysNoneList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddMonths(-15);


            // Act
            List<OrdersWithDetail> fullList = provider.GetMany(startDate);
            List<OrdersWithDetail> fmOnlyList = (from od in fullList where od.IsFMSystem == false select od).ToList();
            List<OrdersWithDetail> resultList = (from od in fmOnlyList where od.IsFMSystem == true select od).ToList();

            // Assert
            Assert.NotEqual(0, fmOnlyList.Count);
            Assert.Equal(0, resultList.Count);
        }

        [Fact]
        public void StaleCustomers_FindDeviceOrders_TestData1ReturnsList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddMonths(-15);
            DateTime fmEndDate = startDate.AddMonths(3);

            // Act
            List<OrdersWithDetail> odList = provider.GetMany(startDate);
            List<OrdersWithDetail> targetList = StaleCustomers.FindDeviceOrders(odList, fmEndDate);

            // Assert
            Assert.Equal(3, targetList.Count);
            Assert.Equal(1, targetList[0].CustomerId);
        }

        [Fact]
        public void StaleCustomers_FindDeviceOrders_TestData2ReturnsList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddMonths(-14);
            DateTime fmEndDate = startDate.AddMonths(3);

            // Act
            List<OrdersWithDetail> odList = provider.GetMany(startDate);
            List<OrdersWithDetail> targetList = StaleCustomers.FindDeviceOrders(odList, fmEndDate);

            // Assert
            Assert.Equal(2, targetList.Count);
            Assert.Equal(2, targetList[0].CustomerId);
        }

        [Fact]
        public void StaleCustomers_FindNonRecentAccyOrders_TestData1ReturnsList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddMonths(-14);
            DateTime fmEndDate = startDate.AddMonths(0);
            DateTime noPurchaseDate = DateTime.Now.AddMonths(0);

            // Act
            List<OrdersWithDetail> odList = provider.GetMany(startDate);
            List<OrdersWithDetail> targetList = StaleCustomers.FindNonRecentAccyOrders(odList, fmEndDate, noPurchaseDate);

            // Assert
            Assert.Equal(6, targetList.Count);
            Assert.Equal(1, targetList[0].CustomerId);
        }

        [Fact]
        public void StaleCustomers_FindNonRecentAccyOrders_TestData2ReturnsList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddMonths(-14);
            DateTime fmEndDate = startDate.AddMonths(0);
            DateTime noPurchaseDate = DateTime.Now.AddMonths(-3);

            // Act
            List<OrdersWithDetail> odList = provider.GetMany(startDate);
            List<OrdersWithDetail> targetList = StaleCustomers.FindNonRecentAccyOrders(odList, fmEndDate, noPurchaseDate);

            // Assert
            Assert.Equal(5, targetList.Count);
            Assert.Equal(1, targetList[0].CustomerId);
        }

        [Fact]
        public void StaleCustomers_FindNonRecentAccyOrders_TestData3ReturnsList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddMonths(-14);
            DateTime fmEndDate = startDate.AddMonths(3);
            DateTime noPurchaseDate = DateTime.Now.AddMonths(-3);

            // Act
            List<OrdersWithDetail> odList = provider.GetMany(startDate);
            List<OrdersWithDetail> targetList = StaleCustomers.FindNonRecentAccyOrders(odList, fmEndDate, noPurchaseDate);

            // Assert
            Assert.Equal(4, targetList.Count);
            Assert.Equal(2, targetList[0].CustomerId);
        }

        [Fact]
        public void StaleCustomers_FindRecentAccyOrders_TestData1ReturnsList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddMonths(-15);
            DateTime noPurchaseDate = DateTime.Now.AddMonths(-3);

            // Act
            List<OrdersWithDetail> odList = provider.GetMany(startDate);
            List<OrdersWithDetail> targetList = StaleCustomers.FindRecentAccyOrders(odList, noPurchaseDate);

            // Assert
            Assert.Equal(1, targetList.Count);
            Assert.Equal(1, targetList[0].CustomerId);
        }

        [Fact]
        public void StaleCustomers_FindCommonInLists_TestData1ReturnsList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();
            DateTime startDate = DateTime.Now.AddMonths(-15);
            DateTime fmEndDate = startDate.AddMonths(3);
            DateTime noPurchaseDate = DateTime.Now.AddMonths(-8);
            List<OrdersWithDetail> odList = provider.GetMany(startDate);
            List<OrdersWithDetail> fmList = StaleCustomers.FindDeviceOrders(odList, fmEndDate);
            List<OrdersWithDetail> recentList = StaleCustomers.FindRecentAccyOrders(odList, noPurchaseDate);

            // Act
            List<OrdersWithDetail> targetList = StaleCustomers.FindCommonInLists(fmList, recentList);

            // Assert
            Assert.Equal(1, targetList.Count);
            Assert.Equal(1, targetList[0].CustomerId);
        }

        [Fact]
        public void StaleCustomers_FindNotCommonInLists_TestDataDiffCustReturnsList()
        {
            //Arrange
            List<OrdersWithDetail> list1 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 1, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            List<OrdersWithDetail> list2 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 2, OrderDate = DateTime.Now.AddMonths(-14), CustomerId = 2, IsFMSystem = false, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            // Act
            List<OrdersWithDetail> targetList = StaleCustomers.FindNotCommonInLists(list1, list2);

            // Assert
            Assert.Equal(1, targetList.Count);
            Assert.Equal(1, targetList[0].CustomerId);
        }

        [Fact]
        public void StaleCustomers_FindNotCommonInLists_TestDataSameCustReturnsNUllList()
        {
            //Arrange
            List<OrdersWithDetail> list1 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 1, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            List<OrdersWithDetail> list2 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 2, OrderDate = DateTime.Now.AddMonths(-14), CustomerId = 1, IsFMSystem = false, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            // Act
            Exception ex = Assert.Throws<ArgumentNullException>(() => StaleCustomers.FindNotCommonInLists(list1, list2));

            // Assert
            Assert.NotEmpty(ex.Message);
        }

        [Fact]
        public void StaleCustomers_RemoveDuplicates_TestData1CustReturns1Cust()
        {
            //Arrange
            List<OrdersWithDetail> list1 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 1, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            // Act
            List<OrdersWithDetail> targetList = StaleCustomers.RemoveDuplicates(list1);

            // Assert
            Assert.Equal(1, targetList.Count);
        }

        [Fact]
        public void StaleCustomers_RemoveDuplicates_TestData2SameCustReturns1Cust()
        {
            //Arrange
            List<OrdersWithDetail> list1 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 1, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-11), CustomerId = 1, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            // Act
            List<OrdersWithDetail> targetList = StaleCustomers.RemoveDuplicates(list1);

            // Assert
            Assert.Equal(1, targetList.Count);
        }

        [Fact]
        public void StaleCustomers_RemoveDuplicates_TestData2DiffCustReturns2Cust()
        {
            //Arrange
            List<OrdersWithDetail> list1 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 1, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-11), CustomerId = 2, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            // Act
            List<OrdersWithDetail> targetList = StaleCustomers.RemoveDuplicates(list1);

            // Assert
            Assert.Equal(2, targetList.Count);
        }

        [Fact]
        public void StaleCustomers_MergeLists_TestData1CustReturns1Cust()
        {
            //Arrange
            List<OrdersWithDetail> list1 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 1, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            List<OrdersWithDetail> list2 = new List<OrdersWithDetail>();

            // Act
            List<OrdersWithDetail> targetList = StaleCustomers.MergeLists(list1, list2);

            // Assert
            Assert.Equal(1, targetList.Count);
        }

        [Fact]
        public void StaleCustomers_MergeLists_TestData2DiffCustReturns2Cust()
        {
            //Arrange
            List<OrdersWithDetail> list1 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 1, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            List<OrdersWithDetail> list2 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 2, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            // Act
            List<OrdersWithDetail> targetList = StaleCustomers.MergeLists(list1, list2);

            // Assert
            Assert.Equal(2, targetList.Count);
        }
        [Fact]
        public void StaleCustomers_MergeLists_TestData2Same1difffCustReturns2Cust()
        {
            //Arrange
            List<OrdersWithDetail> list1 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 1, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 2, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            List<OrdersWithDetail> list2 = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 2, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 2, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
            };

            // Act
            List<OrdersWithDetail> targetList = StaleCustomers.MergeLists(list1, list2);

            // Assert
            Assert.Equal(2, targetList.Count);
        }

        [Fact]
        public void StaleCustomers_GetList_TestDataReturnsList()
        {
            //Arrange
            TestOrdersWithDetailProvider provider = new TestOrdersWithDetailProvider();

            // Act
            List<OrdersWithDetail> odList = provider.GetMany(DateTime.Now.AddMonths(Config.stale_startMonthsBack));
            List<OrdersWithDetail> targetList = StaleCustomers.GetList(odList);

            // Assert
            Assert.Equal(5, targetList.Count);
        }

        [Fact]
        public void DisplayStaleCustomers_DoesExist()
        {
            //Arrange
            TestOrdersWithDetailProvider testProvider = new TestOrdersWithDetailProvider();
            ReportController controller = new ReportController(testProvider);

            // Act
            ViewResult result = controller.DisplayStaleCustomers() as ViewResult;

            // Assert
            Assert.NotNull(result);
        }
    }
}
