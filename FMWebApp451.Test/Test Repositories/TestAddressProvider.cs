﻿using System;
using System.Collections.Generic;
using System.Linq;
using FMWebApp451.Interfaces;
using AdminCart;

namespace FM2015.Models
{
    public class TestAddressProvider : IAddress
    {
        public Address GetByCustomerId(int customerId, int type) // should use Enum Address.AddressType type
        {
            Address a = new Address();
            if (customerId > 0)
            {
                List<Address> aList = GetAll();
                a = aList[0];
                a.CustomerID = customerId;
            }           
            return a;
        }

        public List<Address> GetAll()
        {
            return InitAddressList();
        }

        public static List<Address> InitAddressList()
        {
            List<Address> cList = new List<Address>
            {
                new Address() {
                    AddressId = 1,
                    Street = "202 Nighthawk Dr.",
                    City = "Paso Robles",
                    State = "CA",
                    Zip = "93446",
                    Country = "US",
                },
            };

            return cList;
        }
    }
}