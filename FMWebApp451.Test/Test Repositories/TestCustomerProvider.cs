﻿using System;
using System.Collections.Generic;
using FMWebApp451.Interfaces;
using AdminCart;

namespace FM2015.Models
{
    public class TestCustomerProvider : ICustomer
    {
        public Customer GetById(int id)
        {
            Customer c = new Customer();
            if (id > 0)
            {
                List<Customer> cList = GetAll();
                c = cList[0];
                c.CustomerID = id;
            }
            
            return c;
        }

        public List<Customer> GetAll()
        {
            return InitCustomerList();
        }

        public bool Save(Customer customer)
        {
            return true;
        }

        public static List<Customer> InitCustomerList()
        {
            List<Customer> cList = new List<Customer>
            {
                new Customer() {
                    CustomerID = 152,
                    FirstName = "Rodger",
                    LastName = "Mohme",
                    Email = "rmohme@alumni.uci.edu",
                    Password = "",
                    Phone = "805-610-6826",
                    CreateDate = DateTime.Now,
                    LastUpdateDate = DateTime.Now,
                    LastLoginDate = DateTime.Now,
                    SiteID = 2,
                    ReferrerDomain = "",
                    InBoundQuery = "",
                    FaceMasterNewsletter = false,
                    SuzanneSomersNewsletter = false,
                    SuzanneSomersCustomerID = 0,
                    PartnerID = 0,
                },
            };

            return cList;
        }
    }
}