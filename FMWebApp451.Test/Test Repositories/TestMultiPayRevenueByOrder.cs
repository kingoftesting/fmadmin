﻿using System;
using System.Collections.Generic;
using FMWebApp451.Interfaces;
using FMWebApp451.ViewModels;

namespace FM2015.Models
{
    public class TestMultiPayRevenueByOrder : IMultiPayRevenueByOrder
    {
        public List<MultiPayRevenueByOrder> GetAll(DateTime startDate, DateTime endDate)
        {
            return InitMultiPayRevenueByOrderList();
        }

        public static List<MultiPayRevenueByOrder> InitMultiPayRevenueByOrderList()
        {
            List<MultiPayRevenueByOrder> mpList = new List<MultiPayRevenueByOrder>
            {
                //97390
                new MultiPayRevenueByOrder() { OrderId = 97390, TransactionAmount = 62.49M, RefundAmount = 0, OrderDate = DateTime.Now,
                    StartDate = DateTime.Now, EndDate = DateTime.Now.AddMonths(3), GrossRevenue = 249.99M,
                    PromoCodePercent = 0, PromoCodeDiscount = 0M, PromoCode = "", Revenue = 249.99M,
                    Payment = 62.49M, ttlPaymentCount = 4, PaymentsMade = 1, Collected = 62.49M, Cancelled = 0M, ToBeCollected = 187.50M,
                    UnCollectable = 0M, WrittenOff = 0M, Status = "active" },
                //97456
                new MultiPayRevenueByOrder() { OrderId = 97456, TransactionAmount = 49.99M, RefundAmount = 0, OrderDate = DateTime.Now,
                    StartDate = DateTime.Now, EndDate = DateTime.Now.AddMonths(3), GrossRevenue = 249.99M,
                    PromoCodePercent = 0.20M, PromoCodeDiscount = 50.00M, PromoCode = "Suzanne50", Revenue = 199.99M,
                    Payment = 49.99M, ttlPaymentCount = 4, PaymentsMade = 1, Collected = 49.99M, Cancelled = 0M, ToBeCollected = 149.97M,
                    UnCollectable = 0M, WrittenOff = 0M, Status = "active" },
            };

            return mpList;
        }
    }
}