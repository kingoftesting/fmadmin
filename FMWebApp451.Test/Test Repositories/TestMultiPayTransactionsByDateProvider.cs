﻿using System;
using System.Collections.Generic;
using FMWebApp451.Interfaces;
using FMWebApp451.ViewModels;

namespace FM2015.Models
{
    public class TestMultiPayTransactionsByDateProvider : IMultiPayTransactionsByDate
    {
        public List<MultiPayTransactionsByDate> GetAll(DateTime startDate, DateTime endDate)
        {
            return InitMultiPayTransactionsByDateList();
        }

        public static List<MultiPayTransactionsByDate> InitMultiPayTransactionsByDateList()
        {
            List<MultiPayTransactionsByDate> mpList = new List<MultiPayTransactionsByDate>
            {
                //97390
                new MultiPayTransactionsByDate() { Id = 1, OrderId = 97390, TransactionAmount = 62.49M, OrderDate = DateTime.Now },
                //97456
                new MultiPayTransactionsByDate() { Id = 2, OrderId = 97456, TransactionAmount = 49.99M, OrderDate = DateTime.Now },
            };

            return mpList;
        }
    }
}