﻿using System;
using System.Collections.Generic;
using FMWebApp451.Interfaces;
using AdminCart;

namespace FM2015.Models
{
    public class TestOrderDetailProvider : IOrderDetail
    {
        public List<OrderDetail> GetByDate(DateTime startDate, DateTime endDate)
        {
            return InitOrderDetailList();
        }

        public List<OrderDetail> GetAll()
        {
            return GetAll();
        }

        public List<OrderDetail> GetById(int Id)
        {
            return GetAll();
        }

        public bool Save(OrderDetail od)
        {
            return true;
        }

        public static List<OrderDetail> InitOrderDetailList()
        {
            List<OrderDetail> odList = new List<OrderDetail>
            {
                //97390
                new OrderDetail() { OrderID = 97390, ProductID = 131,  UnitPrice = 62.49M, Discount = 0, Quantity = 1, UnitCost = 38.00M },
                //97389
                new OrderDetail() { OrderID = 97389, ProductID = 14,  UnitPrice = 249.99M, Discount = 0, Quantity = 1, UnitCost = 38.00M },
                //97388
                new OrderDetail() { OrderID = 97388, ProductID = 124,  UnitPrice = 39.99M, Discount = 0.01M, Quantity = 1, UnitCost = 3.26M },
                //97386
                new OrderDetail() { OrderID = 97386, ProductID = 6,  UnitPrice = 19.99M, Discount = 0, Quantity = 1, UnitCost = 3.10M },
                //97430
                new OrderDetail() { OrderID = 97430, ProductID = 17,  UnitPrice = 35.99M, Discount = 0, Quantity = 2, UnitCost = 3.42M },
                new OrderDetail() { OrderID = 97430, ProductID = 24,  UnitPrice = 59.99M, Discount = 29.99M, Quantity = 1, UnitCost = 7.26M },
                //97456
                new OrderDetail() { OrderID = 97456, ProductID = 131,  UnitPrice = 62.49M, Discount = 0, Quantity = 1, UnitCost = 38.00M },
            };

            return odList;
        }
    }
}