﻿using System;
using System.Collections.Generic;
using AdminCart;
using FMWebApp451.Interfaces;

namespace FM2015.Models
{
    public class TestOrderProvider : IOrder
    {
        public List<Order> GetByDate (DateTime startDate, DateTime endDate)
        {
            return InitOrderList();
        }

        public List<Order> GetAll()
        {
            return InitOrderList();
        }

        public bool Save(AdminCart.Order order)
        {
            return true;
        }

        public static List<Order> InitOrderList()
        {
            List<Order> oList = new List<Order>
            {
                //97390
                new Order() { OrderID = 97390, CustomerID = 51467, SourceID = -3, ByPhone = false, OrderDate = DateTime.Now, Total = 62.49M, Adjust = 0, TotalShipping = 0, TotalTax = 0 },
                //97389
                new Order() { OrderID = 97389, CustomerID = 51466, SourceID = -3, ByPhone = false, OrderDate = DateTime.Now.AddHours(-1), Total = 272.49M, Adjust = 0, TotalShipping = 0, TotalTax = 22.50M },
                //97388
                new Order() { OrderID = 97388, CustomerID = 48737, SourceID = -3, ByPhone = false, OrderDate = DateTime.Now.AddHours(-2), Total = 29.99M, Adjust = 10.00M, TotalShipping = 0, TotalTax = 0 },
                //97386
                new Order() { OrderID = 97386, CustomerID = 18892, SourceID = 7041, ByPhone = true, OrderDate = DateTime.Now.AddHours(-3), Total = 24.98M, Adjust = 0, TotalShipping = 4.99M, TotalTax = 0 },
                //97430
                new Order() { OrderID = 97430, CustomerID = 24257, SourceID = 7041, ByPhone = true, OrderDate = DateTime.Now.AddHours(-4), Total = 101.98M, Adjust = 0M, TotalShipping = 0M, TotalTax = 0 },
                //97456
                new Order() { OrderID = 97456, CustomerID = 51498, SourceID = 7041, ByPhone = true, OrderDate = DateTime.Now.AddHours(-5), Total = 49.99M, Adjust = 12.50M, TotalShipping = 0M, TotalTax = 0 },
            };

            return oList;
        }
    }
}