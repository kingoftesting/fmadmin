﻿using System;
using System.Collections.Generic;
using System.Linq;
using FMWebApp451.Interfaces;

namespace FM2015.Models
{
    public class TestOrdersWithDetailProvider : IOrdersWithDetailProvider
    {
        public List<OrdersWithDetail> GetMany(DateTime startDate)
        {
            OrdersWithDetailProvider.CheckDate(startDate);
            List<OrdersWithDetail> theList = GetTestDataList();
            theList = (from od in theList where od.OrderDate >= startDate select od).ToList();
            return theList;
        }

        public static List<OrdersWithDetail> GetTestDataList()
        {
            List<OrdersWithDetail> theList = new List<OrdersWithDetail>
            {
                new OrdersWithDetail() { OrdersWithDetailsId = 1, OrderDate = DateTime.Now.AddMonths(-15), CustomerId = 1, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 2, OrderDate = DateTime.Now.AddMonths(-14), CustomerId = 2, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 3, OrderDate = DateTime.Now.AddMonths(-13), CustomerId = 3, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 4, OrderDate = DateTime.Now.AddMonths(-11), CustomerId = 4, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 5, OrderDate = DateTime.Now.AddMonths(-11), CustomerId = 5, IsFMSystem = true, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 6, OrderDate = DateTime.Now.AddMonths(-11), CustomerId = 1, IsFMSystem = false, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 7, OrderDate = DateTime.Now.AddMonths(-10), CustomerId = 2, IsFMSystem = false, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 8, OrderDate = DateTime.Now.AddMonths(-9), CustomerId = 3, IsFMSystem = false, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 9, OrderDate = DateTime.Now.AddMonths(-8), CustomerId = 4, IsFMSystem = false, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 10, OrderDate = DateTime.Now.AddMonths(-7), CustomerId = 5, IsFMSystem = false, FirstName = "rodger", LastName = "mohme", Email = "rmohme@alumni.uci.edu" },
                new OrdersWithDetail() { OrdersWithDetailsId = 11, OrderDate = DateTime.Now.AddMonths(-3), CustomerId = 1, IsFMSystem = false, FirstName = "foo", LastName = "bar", Email = "foo@bar.com" }
            };

            theList = theList.OrderBy(x => x.OrderDate).ToList();
            return theList;
        }
    }
}