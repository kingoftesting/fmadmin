﻿using System.Collections.Generic;
using System.Linq;
using FMWebApp451.Interfaces;
using AdminCart;
using System;

namespace FMWebApp451.Respositories
{
    public class TestProductCostProvider : IProductCostProvider
    {
        public ProductCost GetById(int productCostId)
        {
            List<ProductCost> productCostList = GetAll();
            ProductCost productCost = new ProductCost();
            if (productCostList.Any(p => p.ProductCostID == productCostId))
            {
                productCost = (from p in productCostList where p.ProductID == productCostId select p).FirstOrDefault();
            }
            return productCost;
        }

        public ProductCost GetByProduct(Product product)
        {
            List<ProductCost> productCostList = GetAll();
            ProductCost productCost = new ProductCost();
            if (productCostList.Any(p => productCost.ProductID == product.ProductID))
            {
                productCost = (from p in productCostList where productCost.ProductID == product.ProductID select p).FirstOrDefault();
            }
            return productCost;
        }


        public int Save(ProductCost productCost)
        {
            if (productCost.ProductID == 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<ProductCost> GetAll()
        {
            List<ProductCost> productCostList = InitProductCostList();
            return productCostList;
        }

        public static List<ProductCost> InitProductCostList()
        {
            List<ProductCost> productCostList = new List<ProductCost>
            {
                new ProductCost() { ProductID = 1, Cost = 1M, StartDate = Convert.ToDateTime("1/1/2018") },
                new ProductCost() { ProductID = 2, Cost = 1M, StartDate = Convert.ToDateTime("1/1/2018") },
                new ProductCost() { ProductID = 3, Cost = 1M, StartDate = Convert.ToDateTime("1/1/2018") },
                new ProductCost() { ProductID = 6, Cost = 1M, StartDate = Convert.ToDateTime("1/1/2018") },
                new ProductCost() { ProductID = 14, Cost = 1M, StartDate = Convert.ToDateTime("1/1/2018") },
                new ProductCost() { ProductID = 17, Cost = 1M, StartDate = Convert.ToDateTime("1/1/2018") },
                new ProductCost() { ProductID = 24, Cost = 1M, StartDate = Convert.ToDateTime("1/1/2018") },
                new ProductCost() { ProductID = 124, Cost = 1M, StartDate = Convert.ToDateTime("1/1/2018") },
                new ProductCost() { ProductID = 131, Cost = 1M, StartDate = Convert.ToDateTime("1/1/2018") },
                new ProductCost() { ProductID = 150, Cost = 1M, StartDate = Convert.ToDateTime("1/1/2018") },
                new ProductCost() { ProductID = 152, Cost = 1M, StartDate = Convert.ToDateTime("1/1/2018") },
            };

            return productCostList;
        }
    }
}
