﻿using System.Collections.Generic;
using System.Linq;
using FMWebApp451.Interfaces;
using AdminCart;

namespace FMWebApp451.Respositories
{
    public class TestProductProvider : IProductProvider
    {
        public Product GetById(int productId)
        {
            List<Product> productList = GetAll();
            Product product = new Product();
            if (productList.Any(p => p.ProductID == productId))
            {
                product = (from p in productList where p.ProductID == productId select p).FirstOrDefault();
            }
            return product;
        }

        public Product GetBySku(string sku)
        {
            List<Product> productList = GetAll();
            Product product = new Product();
            if (productList.Any(p => p.Sku == sku))
            {
                product = (from p in productList where p.Sku == sku select p).FirstOrDefault();
            }
            return product;
        }

        public Product GetByShopifyProductId(long shopifyProductID)
        {
            List<Product> productList = GetAll();
            Product product = new Product();
            if (productList.Any(p => p.PartnerID == shopifyProductID))
            {
                product = (from p in productList where p.PartnerID == shopifyProductID select p).FirstOrDefault();
            }
            return product;
        }

        public int Save(Product product)
        {
            if (product.ProductID == 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<Product> GetAll()
        {
            List<Product> productList = InitProductList();
            return productList;
        }

        public List<Product> GetAll(bool customerOnly)
        {
            List<Product> productList = GetAll();
            productList = (from p in productList where p.CSOnly == customerOnly select p).ToList();
            return productList;
        }

        public static List<Product> InitProductList()
        {
            List<Product> productList = new List<Product>
            {
                new Product() { ProductID = 1, CSOnly = false,  IsSubscription = false, Sku = "FM-0001" },
                new Product() { ProductID = 2, CSOnly = true,  IsSubscription = false, Sku = "FM-0002" },
                new Product() { ProductID = 3, CSOnly = true,  IsSubscription = true, Sku = "FM-0003" },
                new Product() { ProductID = 6, Name ="FaceMaster Foam Caps - 2 Pack", Sku = "FM-2299-2PC", Price = 19.99M, SalePrice = 19.99M,  CSOnly = false, IsFMSystem = false,  IsSubscription = false, IsMultiPay = false, OnePayPrice = 0M, IsRefill = false, IsGiftCard = false, Category = "accessory", IsRefurbished = false },
                new Product() { ProductID = 14, Name ="FaceMaster Platinum", Sku = "FM-5010", Price = 249.99M, SalePrice = 249.99M,  CSOnly = true, IsFMSystem = true,  IsSubscription = false, IsMultiPay = false, OnePayPrice = 249.99M, IsRefill = false, IsGiftCard = false, Category = "device", IsRefurbished = false },
                new Product() { ProductID = 17, Name ="FaceMaster Soothing Conductive Serum - 2 Pack", Sku = "FM-51002PC", Price = 35.99M, SalePrice = 35.99M,  CSOnly = false, IsFMSystem = false,  IsSubscription = false, IsMultiPay = false, OnePayPrice = 0M, IsRefill = false, IsGiftCard = false, Category = "accessory", IsRefurbished = false },
                new Product() { ProductID = 24, Name ="Anti Aging e-Serum with GlycoPeptides", Sku = "FM-5160", Price = 59.99M, SalePrice = 59.99M,  CSOnly = true, IsFMSystem = false,  IsSubscription = false, IsMultiPay = false, OnePayPrice = 0M, IsRefill = false, IsGiftCard = false, Category = "accessory", IsRefurbished = false },
                new Product() { ProductID = 124, Name ="FaceMaster Platinum Club", Sku = "FM-51602199", Price = 39.99M, SalePrice = 39.99M,  CSOnly = true, IsFMSystem = false,  IsSubscription = true, IsMultiPay = false, OnePayPrice = 0M, IsRefill = false, IsGiftCard = false, Category = "club", IsRefurbished = false },
                new Product() { ProductID = 131, Name ="FaceMaster System 4 Easy Payments", Sku = "FM-5010", Price = 62.499M, SalePrice = 62.49M,  CSOnly = true, IsFMSystem = true,  IsSubscription = true, IsMultiPay = true, OnePayPrice = 249.99M, IsRefill = false, IsGiftCard = false, Category = "multipay", IsRefurbished = false },
                new Product() { ProductID = 150, Name ="4-Pay (reconditioned) FaceMaster Platinum", Sku = "FM-5010R", Price = 57.49M, SalePrice = 57.49M,  CSOnly = true, IsFMSystem = true,  IsSubscription = true, IsMultiPay = true, OnePayPrice = 229.99M, IsRefill = false, IsGiftCard = false, Category = "device", IsRefurbished = true },
                new Product() { ProductID = 152, Name ="(reconditioned) FaceMaster Platinum", Sku = "FM-5010R", Price = 229.99M, SalePrice = 229.99M,  CSOnly = true, IsFMSystem = true,  IsSubscription = false, IsMultiPay = false, OnePayPrice = 229.99M, IsRefill = false, IsGiftCard = false, Category = "device", IsRefurbished = true },
            };

            return productList;
        }
    }
}