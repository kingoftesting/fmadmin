﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMWebApp451.Interfaces;
using FM2015.Models;

namespace FMWebApp451.Respositories
{
    class TestRegisterProvider : IRegistration
    {
        public List<Registration> GetAll()
        {
            List<Registration> theList = InitRegistrationList();
            return theList;
        }

        public Registration GetById(int id)
        {
            List<Registration> theList = GetAll();
            Registration registration = new Registration();
            if (theList.Any(r => r.RegistrationId == id))
            {
                registration = (from r in theList where r.RegistrationId == id select r).FirstOrDefault();
            }
            return registration;
        }

        public int Save(Registration registration)
        {
            if (registration.RegistrationId == 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public static List<Registration> InitRegistrationList()
        {
            List<Registration> theList = new List<Registration>
            {
                new Registration() { RegistrationId = 1, FirstName = "Rodger",  LastName = "Mohme", Email = "mohme-paso@facemaster.com",
                    Phone ="805-610-6826", Serial = 61612100001, Created = Convert.ToDateTime("2/27/17"), Confirmed = true},

                new Registration() { RegistrationId = 2, FirstName = "Rodger",  LastName = "Mohme2", Email = "mohme-paso2@facemaster.com",
                    Phone ="805-610-6826", Serial = 61612100002, Created = Convert.ToDateTime("2/26/17"), Confirmed = true},

                new Registration() { RegistrationId = 3, FirstName = "Rodger",  LastName = "Mohme3", Email = "mohme-paso3@facemaster.com",
                    Phone ="805-610-6826", Serial = 61612100003, Created = Convert.ToDateTime("2/25/17"), Confirmed = false},

                new Registration() { RegistrationId = 4, FirstName = "Rodger",  LastName = "Mohme4", Email = "mohme-paso4@facemaster.com",
                    Phone ="805-610-6826", Serial = 616121, Created = Convert.ToDateTime("2/24/17"), Confirmed = false},

                new Registration() { RegistrationId = 5, FirstName = "Rodger",  LastName = "Mohme5", Email = "mohme-paso5@facemaster.com",
                    Phone ="805-610-6826", Serial = 61407100005, Created = Convert.ToDateTime("2/23/17"), Confirmed = true},
            };

            return theList;
        }
    }
}
