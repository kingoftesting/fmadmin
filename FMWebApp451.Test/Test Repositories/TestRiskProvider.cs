﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMWebApp451.Interfaces;
using ShopifyAgent;

namespace FMWebApp451.Respositories
{
    public class TestRiskProvider : IRiskProvider
    {
        public List<ShopOrderRisk> GetAll()
        {
            List<ShopOrderRisk> theList = InitRiskList();
            return theList;
        }

        public ShopOrderRisk GetByShopifyName(int shopifyName)
        {
            List<ShopOrderRisk> theList = GetAll();
            ShopOrderRisk risk = new ShopOrderRisk();
            if (theList.Any(r => r.Order_id == shopifyName))
            {
                risk = (from r in theList where (r.Order_id == shopifyName) select r).FirstOrDefault();
            }

            return risk;
        }

        public int Save(ShopOrderRisk risk)
        {
            return 0;
        }

        public List<ShopOrderRisk> InitRiskList()
        {
            List<ShopOrderRisk> theList = new List<ShopOrderRisk>()
            {
                new ShopOrderRisk {
                    RiskId = -1,
                    Id = -1,
                    Order_id = 1,
                    ShopifyName = 1,
                    Score = 0.50M,
                    Cause_cancel = "",
                    Display = true,
                    Message = "The billing address street does not match that associated with the credit card.",
                    Recommendation = "investigate", //accept, investigate, cancel
                    Source = "Gateway",
                },
                new ShopOrderRisk {
                    RiskId = 2,
                    Id = 2,
                    Order_id = 2,
                    Score = 0.00M,
                    Cause_cancel = "",
                    Display = true,
                    Message = "accept",
                    Recommendation = "accept", //accept, investigate, cancel
                    Source = "Gateway",
                },
                new ShopOrderRisk {
                    RiskId = 3,
                    Id = 3,
                    Order_id = 3,
                    Score = 0.30M,
                    Cause_cancel = "",
                    Display = true,
                    Message = "The billing zip does not match that associated with the credit card.",
                    Recommendation = "investigate", //accept, investigate, cancel
                    Source = "Gateway",
                },
                new ShopOrderRisk {
                    RiskId = 4,
                    Id = 4,
                    Order_id = 4,
                    Score = 0.80M,
                    Cause_cancel = "true",
                    Display = true,
                    Message = "Looks fraudulent!",
                    Recommendation = "cancel", //accept, investigate, cancel
                    Source = "Gateway",
                },
            };

            return theList;
        }
    }
}
