﻿using System;
using System.Collections.Generic;
using AdminCart;
using FMWebApp451.Interfaces;

namespace FM2015.Models
{
    public class TestSalesDetailsProvider : ISalesDetails
    {
        public List<SalesDetails> GetAll(DateTime startDate, DateTime endDate)
        {
            return InitSalesDetailsList();
        }

        public static List<SalesDetails> InitSalesDetailsList()
        {
            List<SalesDetails> theList = new List<SalesDetails>
            {
                new SalesDetails() { ProductID = 6, SKU = "FM-2299-2PC", Description = "FaceMaster Foam Caps - 2 Pack", UnitCost = 3.10M, TotalUnits = 1, TotalGrossRevenue = 19.99M, TotalDiscount = 0M, TotalCost = 3.10M, PhoneUnits = 1, PhoneGrossRevenue = 19.99M },
                new SalesDetails() { ProductID = 14, SKU = "FM-5010", Description = "FaceMaster Platinum", UnitCost = 38.00M, TotalUnits = 1, TotalGrossRevenue = 249.99M, TotalDiscount = 0M, TotalCost = 38.000M, PhoneUnits = 0, PhoneGrossRevenue = 0M },
                new SalesDetails() { ProductID = 17, SKU = "FM-51002PC", Description = "FaceMaster Soothing Conductive Serum - 2 Pack", UnitCost = 3.42M, TotalUnits = 2, TotalGrossRevenue = 71.98M, TotalDiscount = 0M, TotalCost = 6.84M, PhoneUnits = 2, PhoneGrossRevenue = 71.98M },
                new SalesDetails() { ProductID = 24, SKU = "FM-5160", Description = "Anti Aging e-Serum with GlycoPeptides", UnitCost = 7.26M, TotalUnits = 1, TotalGrossRevenue = 59.99M, TotalDiscount = 29.99M, TotalCost = 7.26M, PhoneUnits = 1, PhoneGrossRevenue = 59.99M },
                new SalesDetails() { ProductID = 124, SKU = "FM-51602199", Description = "FaceMaster Platinum Club", UnitCost = 3.260M, TotalUnits = 1, TotalGrossRevenue = 39.99M, TotalDiscount = 0M, TotalCost = 3.260M, PhoneUnits = 0, PhoneGrossRevenue = 0M },
                new SalesDetails() { ProductID = 131, SKU = "FM-5010", Description = "FaceMaster System 4 Easy Payments", UnitCost = 38.00M, TotalUnits = 2, TotalGrossRevenue = 249.99M, TotalDiscount = 12.50M, TotalCost = 38.00M, PhoneUnits = 1, PhoneGrossRevenue = 249.99M },
            };

            return theList;
        }
    }
}