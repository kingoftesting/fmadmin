﻿using System.Collections.Generic;
using System.Linq;
using FMWebApp451.Interfaces;

namespace FM2015.Models
{
    public class TestShopOrderNote : IShopOrderNote
    {
        public ShopOrderNote GetByShopName(int shopName)
        {
            List<ShopOrderNote> theList = GetAll();
            ShopOrderNote shopNote = new ShopOrderNote();
            if (theList.Any(s => s.ShopOrderName == shopName))
            {
                shopNote = (from s in theList where (s.ShopOrderName == shopName) select s).FirstOrDefault();
            }
            return shopNote;
        }

        public List<ShopOrderNote> GetAll()
        {
            List<ShopOrderNote> theList = new List<ShopOrderNote>();

            return theList;
        }

        public void ClearCache()
        {
            ;
        }
    }
}
