﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FM2015.Models;
using FMWebApp451.Interfaces;

namespace FMWebApp451.Respositories
{
    public class TestSubscriptionPlanProvider : ISubscriptionPlans
    {
        public SubscriptionPlans GetByPlanId(string planId)
        {
            List<SubscriptionPlans> theList = GetAll();
            SubscriptionPlans subPlan = (from s in theList where s.PlanId == planId select s).FirstOrDefault();
            if (subPlan == null)
            {
                subPlan = new SubscriptionPlans();
            }
            return subPlan;
        }

        public List<SubscriptionPlans> GetAll()
        {
            List<SubscriptionPlans> theList = new List<SubscriptionPlans>();
            return theList;
        }
    }
}
