﻿using System;
using System.Collections.Generic;
using System.Linq;
using FMWebApp451.Interfaces;
using AdminCart;

namespace FM2015.Models
{
    public class TestTransactionProvider : ITransaction
    {
        public List<Transaction> GetAll(DateTime startDate, DateTime endDate)
        {
            List<Transaction> tList = InitOrderList();
            return tList;
        }

        public static List<Transaction> InitOrderList()
        {
            List<Transaction> tList = new List<Transaction>
            {
                //97390
                new Transaction() { OrderID = 97390, TransactionID = "tst_ch_19NTZGINMTFgK3ce8ZIfrRQc", TransactionAmount = 62.49M, TransactionDate = DateTime.Now, TrxType = "S", CardType = "VISA", CardNo = "****XXXXXX7735", ResultCode = 0, ResultMsg = "Approved (Stripe)" },
                new Transaction() { OrderID = 97390, TransactionID = "tst_ch_19NTZGINMTFgK3ce8ZIfrRQc", TransactionAmount = 10.00M, TransactionDate = DateTime.Now, TrxType = "SA", CardType = "VISA", CardNo = "****XXXXXX7735", ResultCode = 0, ResultMsg = "Approved (Stripe)" },
                //97389
                new Transaction() { OrderID = 97389, TransactionID = "tst_ch_19NSgfLfRlOEM9uuj9hFK1Hn", TransactionAmount = 272.49M, TransactionDate = DateTime.Now.AddHours(-1), TrxType = "S", CardType = "AMEX", CardNo = "****XXXXXX8005", ResultCode = 0, ResultMsg = "Approved (Stripe)" },
                //97388
                new Transaction() { OrderID = 97388, TransactionID = "tst_ch_19NRr3INMTFgK3cexfdQsbrf", TransactionAmount = 29.99M, TransactionDate = DateTime.Now.AddHours(-2), TrxType = "S", CardType = "VISA", CardNo = "****XXXXXX8316", ResultCode = 0, ResultMsg = "Approved (Stripe)" },
                //97386
                new Transaction() { OrderID = 97386, TransactionID = "tst_ch_19NQ3sLfRlOEM9uuHmy81is7", TransactionAmount = 24.98M, TransactionDate = DateTime.Now.AddHours(-3), TrxType = "S", CardType = "AMEX", CardNo = "****XXXXXX1010", ResultCode = 0, ResultMsg = "Approved (Stripe)" },
                new Transaction() { OrderID = 97386, TransactionID = "tst_re_19NQ3sLfRlOEM9uuHmy81is7", TransactionAmount = 1.00M, TransactionDate = DateTime.Now.AddHours(-3), TrxType = "C", CardType = "AMEX", CardNo = "****XXXXXX1010", ResultCode = 0, ResultMsg = "Approved (Stripe)" },
                //97430
                new Transaction() { OrderID = 97430, TransactionID = "tst_ch_19O9S1INMTFgK3cecwOWwbUF", TransactionAmount = 101.98M, TransactionDate = DateTime.Now.AddHours(-4), TrxType = "S", CardType = "VISA", CardNo = "****XXXXXX5836", ResultCode = 0, ResultMsg = "Approved (Stripe)" },
                //97456
                new Transaction() { OrderID = 97456, TransactionID = "tst_ch_19OVmnINMTFgK3cezyjHM2rY", TransactionAmount = 49.99M, TransactionDate = DateTime.Now.AddHours(-5), TrxType = "S", CardType = "MasterCard", CardNo = "****XXXXXX0994", ResultCode = 0, ResultMsg = "Approved (Stripe)" },
            };

            return tList;
        }
    }
}