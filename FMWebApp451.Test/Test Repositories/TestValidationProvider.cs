﻿using System;
using System.Collections.Generic;
using System.Linq;
using FMWebApp451.Interfaces;
using FM2015.Models;

namespace FMWebApp451.Respositories
{
    public class testValidationProvider : IValidationProvider
    {
        public Validation GetById(int valId)
        {
            List<Validation> validationList = GetAll();
            Validation validation = new Validation();
            if (validationList.Any(v => v.ValidationId == valId))
            {
                validation = (from v in validationList where v.ValidationId == valId select v).FirstOrDefault();
            }

            return validation;
        }

        public int Save(Validation validation)
        {
            return 0;
        }

        public List<Validation> GetAll()
        {
            List<Validation> theList = InitValidationList();
            return theList;
        }

        public static List<Validation> InitValidationList()
        {
            List<Validation> productList = new List<Validation>
            {
                new Validation() { ValidationId = 1, Date = Convert.ToDateTime("1/1/2016"), By = "Rodger Mohme-1", Pass = false,  SerialNum = 1, Comments = "n/a-1" },
                new Validation() { ValidationId = 2, Date = Convert.ToDateTime("1/2/2016"), By = "Rodger Mohme-2", Pass = true,  SerialNum = 2, Comments = "n/a-2" },
                new Validation() { ValidationId = 3, Date = Convert.ToDateTime("1/3/2016"), By = "Rodger Mohme-3",Pass = true,  SerialNum = 3, Comments = "n/a-2" },
            };

            return productList;
        }
    }
}