﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMWebApp451.Interfaces;
using Stripe;
using Moq;

namespace FMWebApp451.Services.StripeServices
{
    public class TestStripeService : IStripeService
    {

        public StripeCustomer FindStripeCustomerByEmail(string email)
        {
            StripeCustomer c = new StripeCustomer
            {
                // add fake code here
                Id = "tst_cus_1"
            };
            return c;
        }

        public StripeCard CardService_Create(string customerId, StripeCardCreateOptions myCard)
        {
            var cardService = new StripeCardService();
            StripeCard card = new StripeCard();
            // add some fake test code here
            if (string.IsNullOrEmpty(myCard.SourceCard.Name)) { return card; } //******
            if (string.IsNullOrEmpty(myCard.SourceCard.Number)) { return card; }
            if (!Int32.TryParse(myCard.SourceCard.ExpirationMonth.ToString(), out int tempMonth)) { return card; }
            if (!Int32.TryParse(myCard.SourceCard.ExpirationYear.ToString(), out int tempYear)) { return card; }
            if (string.IsNullOrEmpty(myCard.SourceCard.Cvc)) { return card; }
            card.Id = "tst_card_1";
            return card;
        }

        public StripeCustomer GetStripeCustomer(string customerId, out string errMsg)
        {
            errMsg = string.Empty;
            var customerService = new StripeCustomerService();
            StripeCustomer stripeCustomer = new StripeCustomer
            {
                // add some fake code here
                Id = "tst_cus_2"
            };
            return stripeCustomer;
        }

        public StripeCustomer CustomerService_Update(string customerId, StripeCustomerUpdateOptions myCard)
        {
            var customerService = new StripeCustomerService();
            StripeCustomer stripeCustomer = new StripeCustomer
            {
                // add some fake code here
                Id = "tst_cus_3"
            };
            return stripeCustomer;
        }

        public List<StripeSubscription> GetCustomerSubscriptionList(string stipeCustomerID, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public List<StripeCustomer> GetStripeCustomerList(string param, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeProduct GetStripeProduct(string productId, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public List<StripeCard> GetStripeCustomerCards(string id, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public List<StripeCharge> GetStripeCustomerCharges(string id, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public List<StripeInvoice> GetStripeCustomerInvoices(string id, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeSubscription CreateStripeSubscription(string stripeCustomerID, string stripePlanID, string StripeCouponID, string subscriptionErrMsg, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public bool DeleteStripeSubscription(string stripeCustomerID, string stripeSubscriptionId, string subscriptionErrMsg, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public List<StripePlan> GetStripePlanList(out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripePlan GetStripePlanById(string planId, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripePlan GetStripePlanByName(string planName, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public bool CreateStripePlan(string planName, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public bool DeleteStripePlan(string planId, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripePlan CreateStripePlan(StripePlanCreateOptions planOptions, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripePlan UpdateStripePlan(string planId, StripePlanUpdateOptions planOptions, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeCard GetStripeCustomerCard(string stripeCustomerId, string stripeCardId, out string errMsg)
        {
            throw new NotImplementedException();
        }

        

        public StripeToken CreateStripeToken(StripeTokenCreateOptions tokenOptions, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeToken GetStripeToken(string tokenId, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeCard CreateStripeCard(string stripeCustomerId, StripeCardCreateOptions cardCreateOptions, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeCard UpdateStripeCard(string stripeCustomerId, string stripeCardId, StripeCardUpdateOptions cardCreateOptions, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public bool DeleteStripeCard(string stripeCustomerId, string stripeCardId, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeCharge GetStripeCustomerCharge(string stripeChargeId, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeCharge CreateStripeCharge(StripeChargeCreateOptions stripeChargeCreateOptions, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeCharge UpdateStripeCharge(string stripeChargeId, StripeChargeUpdateOptions stripeChargeUpdateOptions, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeCustomer CreateStripeCustomer(StripeCustomerCreateOptions stripeCustomerCreateOptions, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeCustomer UpdateStripeCustomer(string stripeCustomerId, StripeCustomerUpdateOptions stripeCustomerUpdateOptions, out string errMsg)
        {
            throw new NotImplementedException();
        }

        //Card Update
        //        {
        //  "object": {
        //    "id": "card_19TNTBINMTFgK3cedzVASLps",
        //    "object": "card",
        //    "address_city": null,
        //    "address_country": null,
        //    "address_line1": null,
        //    "address_line1_check": null,
        //    "address_line2": null,
        //    "address_state": null,
        //    "address_zip": null,
        //    "address_zip_check": null,
        //    "brand": "MasterCard",
        //    "country": "US",
        //    "customer": "cus_9mxNiPDn7jd1Ij",
        //    "cvc_check": "pass",
        //    "dynamic_last4": null,
        //    "exp_month": 4,
        //    "exp_year": 2017,
        //    "fingerprint": "XVidIiGF4WARnc5j",
        //    "funding": "credit",
        //    "last4": "2238",
        //    "metadata": {},
        //    "name": "Ruth E Snow",
        //    "tokenization_method": null
        //  }
        //}

        //Customer Update after card update?
        //        {
        //  "object": {
        //    "id": "cus_9mx4kIjujEDNd0",
        //    "object": "customer",
        //    "account_balance": 0,
        //    "created": 1482376942,
        //    "currency": "usd",
        //    "default_source": "card_19TN9uINMTFgK3cetfBmKEwb",
        //    "delinquent": false,
        //    "description": "Subscriber",
        //    "discount": null,
        //    "email": "jiwbarber@shaw.ca",
        //    "livemode": true,
        //    "metadata": {},
        //    "shipping": null,
        //    "sources": {
        //      "object": "list",
        //      "data": [
        //        {
        //          "id": "card_19TN9uINMTFgK3cetfBmKEwb",
        //          "object": "card",
        //          "address_city": null,
        //          "address_country": null,
        //          "address_line1": null,
        //          "address_line1_check": null,
        //          "address_line2": null,
        //          "address_state": null,
        //          "address_zip": null,
        //          "address_zip_check": null,
        //          "brand": "MasterCard",
        //          "country": "CA",
        //          "customer": "cus_9mx4kIjujEDNd0",
        //          "cvc_check": "pass",
        //          "dynamic_last4": null,
        //          "exp_month": 11,
        //          "exp_year": 2018,
        //          "fingerprint": "BQyrnUSlDtCgbDxr",
        //          "funding": "prepaid",
        //          "last4": "4353",
        //          "metadata": {},
        //          "name": "Ila Carrington",
        //          "tokenization_method": null
        //        }
        //      ],
        //      "has_more": false,
        //      "total_count": 1,
        //      "url": "/v1/customers/cus_9mx4kIjujEDNd0/sources"
        //    },
        //    "subscriptions": {
        //      "object": "list",
        //      "data": [
        //        {
        //          "id": "sub_9mx4T9FxLRmjY4",
        //          "object": "subscription",
        //          "application_fee_percent": 1,
        //          "cancel_at_period_end": false,
        //          "canceled_at": null,
        //          "created": 1482376945,
        //          "current_period_end": 1485055345,
        //          "current_period_start": 1482376945,
        //          "customer": "cus_9mx4kIjujEDNd0",
        //          "discount": null,
        //          "ended_at": null,
        //          "livemode": true,
        //          "metadata": {},
        //          "plan": {
        //            "id": "facemaster-system-4-easy-payments",
        //            "object": "plan",
        //            "amount": 6249,
        //            "created": 1435947415,
        //            "currency": "usd",
        //            "interval": "month",
        //            "interval_count": 1,
        //            "livemode": true,
        //            "metadata": {},
        //            "name": "FaceMaster System 4 Easy Payments",
        //            "statement_descriptor": null,
        //            "trial_period_days": null
        //          },
        //          "quantity": 1,
        //          "start": 1482376945,
        //          "status": "active",
        //          "tax_percent": null,
        //          "trial_end": null,
        //          "trial_start": null
        //        }
        //      ],
        //      "has_more": false,
        //      "total_count": 1,
        //      "url": "/v1/customers/cus_9mx4kIjujEDNd0/subscriptions"
        //    }
        //  },
        //  "previous_attributes": {
        //    "currency": null
        //  }
        //}


    }
}