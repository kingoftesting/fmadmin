﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using FM2015.Models;
using FM2015.ViewModels;
using FM2015.Controllers;
using AdminCart;
using FMWebApp451.Respositories;
using System.Web.Routing;
using FMWebApp451.Helpers;
using System.Web;

namespace FMWebApp451.Test
{
    public class ValidationTests
    {
        [Fact]
        public void ValidationProvider_SaveIDEqual1_Returns0()
        {
            //Arrange
            Validation v = new Validation();
            v.ValidationId = 1;
            testValidationProvider testProvider = new testValidationProvider();

            // Act
            int result = testProvider.Save(v);

            // Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void ValidationProvider_InitProducts_ReturnsList()
        {
            //Arrange
            testValidationProvider testProvider = new testValidationProvider();

            // Act
            List<Validation> resultList = testValidationProvider.InitValidationList();

            // Assert
            Assert.Equal(3, resultList.Count);
            Assert.Equal(1, resultList[0].ValidationId);
        }

        [Fact]
        public void ValidationProvider_GetById0_ReturnsValidationId0()
        {
            //Arrange
            int validationId = 0;
            testValidationProvider testProvider = new testValidationProvider();

            // Act
            Validation result = testProvider.GetById(validationId);

            // Assert
            Assert.Equal(0, result.ValidationId);
        }

        [Fact]
        public void ValidationProvider_GetById1_ReturnsValidationId1()
        {
            //Arrange
            int validationId = 1;
            testValidationProvider testProvider = new testValidationProvider();

            // Act
            Validation result = testProvider.GetById(validationId);

            // Assert
            Assert.Equal(1, result.ValidationId);
        }

        [Fact]
        public void Index_DoesExist()
        {
            //Arrange
            testValidationProvider testProvider = new testValidationProvider();
            ValidationController controller = new ValidationController(testProvider);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void Index_ReturnsGoodViewResult()
        {
            //Arrange
            testValidationProvider testProvider = new testValidationProvider();
            ValidationController controller = new ValidationController(testProvider);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model

            List<Validation> vList = (List<Validation>)result.ViewData.Model;
            vList = vList.OrderBy(v => v.ValidationId).ToList();
            Assert.Equal(3, vList.Count);
            Assert.Equal(1, vList[0].ValidationId);
            Assert.Equal(2, vList[1].ValidationId);
            Assert.True(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Index");
        }

        [Fact]
        public void Details_DoesExist()
        {
            //Arrange
            testValidationProvider testProvider = new testValidationProvider();
            ValidationController controller = new ValidationController(testProvider);
            int validationId = 2;

            // Act
            ViewResult result = controller.Details(validationId) as ViewResult;

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void Details_ValidIdReturnsGoodViewResult()
        {
            //Arrange
            testValidationProvider testProvider = new testValidationProvider();
            ValidationController controller = new ValidationController(testProvider);
            int validationId = 2;

            // Act
            ViewResult result = controller.Details(validationId) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model

            Validation validation = (Validation)result.ViewData.Model;
            Assert.Equal(2, validation.ValidationId);
            Assert.True(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Details");
        }

        [Fact]
        public void Details_Id0ReturnsId0()
        {
            //Arrange
            testValidationProvider testProvider = new testValidationProvider();
            ValidationController controller = new ValidationController(testProvider);
            int validationId = 0;

            // Act
            ViewResult result = controller.Details(validationId) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model

            Validation validation = (Validation)result.ViewData.Model;
            Assert.Equal(0, validation.ValidationId);
        }

        [Fact]
        public void Edit_DoesExist()
        {
            //Arrange
            testValidationProvider testProvider = new testValidationProvider();
            ValidationController controller = new ValidationController(testProvider);
            int validationId = 0;

            // Act
            ViewResult result = controller.Edit(validationId) as ViewResult;

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void Edit_ValidIdReturnsGoodViewResult()
        {
            //Arrange
            testValidationProvider testProvider = new testValidationProvider();
            ValidationController controller = new ValidationController(testProvider);
            int validationId = 2;

            // Act
            ViewResult viewResult = controller.Edit(validationId) as ViewResult;
            Validation validation = (Validation)viewResult.Model;

            // Assert
            Assert.NotNull(viewResult);
            Assert.NotNull(viewResult.Model); // add additional checks on the Model

            Assert.Equal(2, validation.ValidationId);
            Assert.True(string.IsNullOrEmpty(viewResult.ViewName) || viewResult.ViewName == "Edit");
        }

        [Fact]
        public void Edit_Post_IdZeroReturnsException()
        {
            //Arrange
            testValidationProvider testProvider = new testValidationProvider();
            ValidationController controller = new ValidationController(testProvider);
            Validation validation = new Validation();
            int validationId = 0;

            // Act
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => controller.Edit(validationId, validation));

            // Assert
            //Assert.Equal("Value does not fall within the expected range.", ex.Message);
            Assert.NotEmpty(ex.Message);
        }

        [Fact]
        public void Edit_Post_IdNegReturnsException()
        {
            testValidationProvider testProvider = new testValidationProvider();
            ValidationController controller = new ValidationController(testProvider);
            Validation validation = new Validation();
            int validationId = -1;

            // Act
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => controller.Edit(validationId, validation));

            // Assert
            //Assert.Equal("Value does not fall within the expected range.", ex.Message);
            Assert.NotEmpty(ex.Message);
        }

        [Fact]
        public void Edit_Post_ValidIdandModelReturnsRedirect()
        {
            //Arrange
            testValidationProvider testProvider = new testValidationProvider();
            ValidationController controller = new ValidationController(testProvider);
            RouteCollection routes = new RouteCollection();
            Validation validation = new Validation();
            int validationId = 1;

            // Act
            RedirectToRouteResult result = controller.Edit(validationId, validation) as RedirectToRouteResult;

            // Assert
            Assert.NotNull(result.RouteValues);
            Assert.Equal("Details", result.RouteValues["action"]);
            Assert.Equal("Validation", result.RouteValues["controller"]);
            Assert.Equal(1, result.RouteValues["id"]);
        }

        [Fact]
        public void Create_DoesExist()
        {
            //Arrange
            HttpContextBase ctx = SupportStuff.GetMockedHttpContext();
            User user = new User();
            user.Email = "rmohme@alumni.uci.edu";
            user.FirstName = "rodger";
            user.LastName = "mohme";
            user.UserID = 152;
            ctx.Session["_currentUser"] = user;
            HttpContextManager.SetCurrentContext(SupportStuff.GetMockedHttpContext());
            testValidationProvider testProvider = new testValidationProvider();
            ValidationController controller = new ValidationController(testProvider);

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            // Assert
            Assert.NotNull(result);
            Assert.NotNull(result.Model); // add additional checks on the Model
        }
    }
}
