﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;


public partial class Admin_AbortedCartEmailReminder : System.Web.UI.Page
{
    Boolean testmode = true;

    //use Class Generator:
    //object class name: AbortedCart
    //object class properties: int,  OrderID; string,EmailBody; DateTime,Date; Boolean, Test;

    public class theCart
    {
        private int id;
        public int ID { get { return id; } set { id = value; } }

        private int orderID;
        public int OrderID { get { return orderID; } set { orderID = value; } }

        private int customerID;
        public int CustomerID { get { return customerID; } set { customerID = value; } }

        private string email;
        public string Email { get { return email; } set { email = value; } }

        private DateTime orderDate;
        public DateTime OrderDate { get { return orderDate; } set { orderDate = value; } }

        private decimal total;
        public decimal Total { get { return total; } set { total = value; } }

        private int siteSettingsID;
        public int SiteSettingsID { get { return siteSettingsID; } set { siteSettingsID = value; } }

        private string lastName;
        public string LastName { get { return lastName; } set { lastName = value; } }

        private string firstName;
        public string FirstName { get { return firstName; } set { firstName = value; } }

        private string city;
        public string City { get { return city; } set { city = value; } }

        private string state;
        public string State { get { return state; } set { state = value; } }

        private string country;
        public string Country { get { return country; } set { country = value; } }

        private Boolean emailSuccess;
        public Boolean EmailSuccess { get { return emailSuccess; } set { emailSuccess = value; } }

        private string emailResult;
        public string EmailResult { get { return emailResult; } set { emailResult = value; } }

        public theCart()
        {
            OrderID = 0;
            CustomerID = 0;
            Email = "";
            OrderDate = Convert.ToDateTime("1/1/1900");
            Total = 0;
            SiteSettingsID = 0;
            LastName = "";
            FirstName = "";
            City = "";
            State = "";
            Country = "";
            EmailSuccess = false;
            EmailResult = "";
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        testmode = chkTest.Checked;
        if (testmode) { lblTestMode.Text = ": ON"; }
        else { lblTestMode.Text = ": OFF"; }

    }

    protected void btnStart_Click(object sender, EventArgs e)
    {
            List<theCart> theCartList = new List<theCart>();
            theCartList = GetCartList(30); //don't bother with purchase total under $30
            foreach (theCart c in theCartList)
            {
                SendReminderEmail(c);
            }

            grdCartReminder.DataSource = theCartList;
            grdCartReminder.DataBind();
    }

    protected List<theCart> GetCartList(int minTotal)
    {
        List<theCart> theCartList = new List<theCart>();
        int maxID = AbortedCart.MaxEmailSent();
        DateTime startDate = DateTime.Today;
        startDate = startDate.AddDays(-3); //begin looking yesterday unless otherwise determined
        DateTime endDate = DateTime.Now;
        endDate = endDate.AddHours(-12); //make sure only carts that are at least 12 hours old are considered
        AbortedCart c = new AbortedCart(maxID);
        if (c.ID > 0) { startDate = c.OrderDate; }
        String sql = @"

SELECT  o.orderid as orderID, c.customerID, c.email, o.orderdate, o.Total, o.SiteSettingsID, c.Lastname, c.firstname, 
		a.city, a.[state], a.CountryCode 
FROM orders o, orderactions oa, customers c, addresses a  
WHERE o.orderid = oa.orderid AND o.customerid = c.customerid AND o.shippingaddressid = a.addressid AND 
    (oa.orderactiondate BETWEEN @startDate AND @endDate) AND 
	o.orderid = (select Max(orderid) from orders where customerid = c.customerid) AND 
    (o.Total > @minTotal) AND 
	a.CountryCode = 'US' AND 
    (o.OrderID NOT IN (select orderid from abortedcart where test = 0) )
GROUP BY c.customerid, c.email, o.orderid, o.orderdate, o.Total, o.SiteSettingsID, c.lastname, c.firstname, 
		a.city, a.[state], a.CountryCode 
HAVING max(oa.orderactiontype) < 2 
order by o.orderdate asc 

";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, minTotal);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

        while (dr.Read())
        {
            theCart cart = new theCart();

            cart.OrderID = Convert.ToInt32(dr["OrderID"]);
            cart.CustomerID = Convert.ToInt32(dr["CustomerID"]);
            cart.Email = dr["Email"].ToString();
            cart.OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());
            cart.Total = Convert.ToDecimal(dr["Total"]);
            cart.SiteSettingsID = Convert.ToInt32(dr["SiteSettingsID"]);
            cart.LastName = dr["LastName"].ToString();
            cart.FirstName = dr["FirstName"].ToString();
            cart.City = dr["City"].ToString();
            cart.State = dr["State"].ToString();
            cart.Country = dr["CountryCode"].ToString();

            theCartList.Add(cart);
        }

        return theCartList;
    }

    protected void SendReminderEmail(theCart c)
    {
        string emailBody = "";
        string url = "";
        if (c.SiteSettingsID == -1)
        {
            emailBody = strRegularCart;
            url = "'http://www.facemaster.com/viewcart.aspx?promo=yes&sset=3";
            url += "&Action=Load&OrderID=" + c.OrderID.ToString() + "'";

        }
        else
        {
            emailBody = strPromoCart;
            url = "'http://www.facemaster.com/viewcart.aspx?promo=yes&sset=" + c.SiteSettingsID.ToString();
            url += "&Action=Load&OrderID=" + c.OrderID.ToString() + "'";
        }

        string anchor = "<a href=" + url + ">My FaceMaster Shopping Cart</a>";
        string name = c.FirstName + " " + c.LastName;
        emailBody = emailBody.Replace("%NAME%", name);
        emailBody = emailBody.Replace("%URL%", anchor);
        string sendTo = "rmohme@sbcglobal.net";

        try
        {
            if (testmode)
            { sendTo = "rmohme@sbcglobal.net"; }
            else { sendTo = c.Email; }

            mvc_Mail.SendMail(sendTo, AdminCart.Config.CustomerServiceEmail(), "", "", "Your Recent FaceMaster Shopping Cart", emailBody, true);
            c.EmailSuccess = true;
            c.EmailResult = "OK";
        }
        catch (Exception ex)
        {
            c.EmailSuccess = false;
            c.EmailResult = ex.Message;
        }

        AbortedCart aCart = new AbortedCart();
        aCart.ID = 0; //make sure we insert a new row
        aCart.OrderID = c.OrderID;
        aCart.EmailBody = emailBody;
        aCart.DateSent = DateTime.Now;
        aCart.OrderDate = c.OrderDate;
        aCart.Test = testmode;
        aCart.EmailSuccess = c.EmailSuccess;
        aCart.EmailResult = c.EmailResult;
        int result = aCart.Save(aCart.ID);
        if (result == 0)
        { ; } //raise some exception here...

    }

    protected string strPromoCart = @"
<p>
Dear %NAME%,
</p>
<p>
We noticed you weren't able to complete your purchase earlier. To make it easier, we're keeping your cart safe for you. You may find it via the link below:
</p>
<p>
%URL%
</p>
<p>
We've noted that you filled your cart using one of our special limited-time promotions. However by using the link above you will still be able to access the promotion, even if it is no longer available to the general public.
</p>
<p>
Let us know if you have any questions at all. We'd love to help you out! Simply reply to this email or give us a call at:
<br /><br />
1-800-770-2521 (M-F 9am-5pm Pacific / noon-8pm Eastern).
</p>
<p>
Thank you,<br />
FaceMaster Customer Service
</p>

";

    protected string strRegularCart = @"
<p>
Dear %NAME%,
</p>
<p>
We noticed you weren't able to complete your purchase earlier. To make it easier, we're keeping your cart safe for you. You may find it via the link below:
</p>
<p>
%URL%
</p>
<p>
Let us know if you have any questions at all. We'd love to help you out! Simply reply to this email or give us a call at:
<br /><br />
1-800-770-2521 (M-F 9am-5pm Pacific / noon-8pm Eastern).
</p>
<p>
<strong style=""color:#0026FF;"">Free Shipping!</strong><br />
Also, we wanted to make this even easier for you by offering you <strong style=""color:#0026FF;"">Free Shipping!</strong> 
</p>
<p>
Place your order successfully (using the link above) within the next 48 hours and have it delivered for <strong style=""color:#0026FF;"">Free</strong> (ground shipping only).
</p>
<p>
Thank you,<br />
FaceMaster Customer Service
</p>
";

}