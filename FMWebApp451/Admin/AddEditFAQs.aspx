<%@ Page Language="C#" Theme="Admin" MasterPageFile="MasterPage3_Blank.master" ValidateRequest="false" AutoEventWireup="true" Inherits="AddEditFAQs" Title="Add/Edit FAQs" Codebehind="AddEditFAQs.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<table cellpadding="5" cellspacing="5">
	<tr>
		<td>&nbsp;</td>
	</tr>			
	<tr>
		<td colspan=2><b>Add Faq:</b></td>
	</tr>
	<tr>
		<td><asp:Label ID="lblErrorMessage" ForeColor="red" Runat="server" /></td>
	</tr>
	<tr>
		<td><asp:Label ID="Label4" Text="Category" Runat="server" /></td>
		<td>
		    <asp:TextBox width="250px" ID="txtCategory" Runat="server" MaxLength="50" />
		    <asp:RequiredFieldValidator ID="valtxtCategory" runat="server" ControlToValidate="txtCategory" SetFocusOnError="true"
                Text="The Category field is required." ToolTip="The Category field is required." Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="valeditCategory" runat="server" ControlToValidate="txtCategory" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valeditCategory_ServerValidate"></asp:CustomValidator>

</td>
	</tr>				
	<tr>
		<td><asp:Label ID="Label3" Text="Question" Runat="server" /></td>
		<td>
		    <asp:TextBox width="500px" ID="txtQuestion" TextMode="MultiLine" Columns="100" Rows="5" Runat="server" MaxLength="2000" />
		    <asp:RequiredFieldValidator ID="valtxtQuestion" runat="server" ControlToValidate="txtQuestion" SetFocusOnError="true"
                Text="The Question field is required." ToolTip="The Question field is required." Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="valeditQuestion" runat="server" ControlToValidate="txtQuestion" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valeditQuestion_ServerValidate"></asp:CustomValidator>
		</td>
	</tr>
	<tr>
		<td><asp:Label ID="Label1" Text="Answer" Runat="server" /></td>
		<td>
		    <asp:TextBox width="500px" ID="txtAnswer" TextMode="MultiLine" Columns="100" Rows="5" Runat="server" MaxLength="2000" />
		    <asp:RequiredFieldValidator ID="valtxtAnswer" runat="server" ControlToValidate="txtAnswer" SetFocusOnError="true"
                Text="The Answer field is required." ToolTip="The Answer field is required." Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="valeditAnswer" runat="server" ControlToValidate="txtAnswer" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valeditAnswer_ServerValidate"></asp:CustomValidator>
		</td>
	</tr>
	<tr>
		<td><asp:Label ID="Label5" Text="SortOrder" Runat="server" /></td>
		<td>
		    <asp:TextBox width="30px" ID="txtSortOrder" Runat="server" MaxLength="2" />
            <asp:RangeValidator ID="valrangeSortOrder" runat="server" ControlToValidate="txtSortOrder"
                Type="Integer" MinimumValue= "1" MaximumValue="5"
                ErrorMessage="Input needs to be an integer between 1 and 5"></asp:RangeValidator>
		</td>
	</tr>
	<tr>
		<td><asp:Label ID="Label2" Text="Enabled" Runat="server" /></td>
		<td><asp:CheckBox width="100px" ID="chkEnabled" Runat="server" /></td>
	</tr>
	<tr>
		<td colspan="2"><asp:Button ID="btnSave" Text="Save Faq" OnClick="btnSave_Click" Runat="server" /></td>
	</tr>
</table>

</asp:Content>

