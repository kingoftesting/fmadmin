using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FM2015.Helpers;

public partial class AddEditFAQs : AdminBasePage //System.Web.UI.Page
{
    string FaqID = "";
    string action = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FaqID = Request["FaqID"];
            if (!ValidateUtil.IsNumeric(FaqID)) FaqID = ""; //don't pass a bogus param
            action = Request["action"];
            Session["FaqID"] = FaqID; //remember for Button Click

            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "review":
                        {
                            FaqID = "";
                            addFAQ(FaqID);
                        }
                        break;

                    case "add":
                        {
                            FaqID = "";
                            addFAQ(FaqID);
                        }
                        break;

                    case "edit":
                        addFAQ(FaqID);
                        break;

                    case "delete":
                        deleteFAQ(FaqID);
                        break;

                    default:
                        Response.Redirect("ManageFaqs.aspx?listingID=" + "");
                        break;

                }
            }
            else
            {
                FaqID = "";
                addFAQ(FaqID);
            } 
        }
        
    }

    public void deleteFAQ (string FaqID)
    {

        string sql = "DELETE FROM Faqs WHERE FaqID = @FaqID";
        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, FaqID);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

        Response.Redirect("ManageFaqs.aspx?listingID=" + "");

    }

    public void addFAQ (string FaqID)
    {
        if (!string.IsNullOrEmpty(FaqID))
        {
            string sql = @"
            SELECT Category, Question, Answer, SortOrder, Enabled 
            FROM Faqs 
            WHERE FaqID = @FaqID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, FaqID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);


            if (dr.Read())
            {
                txtCategory.Text = dr.IsDBNull(0) ? "" : dr.GetString(0);
                txtQuestion.Text = dr.IsDBNull(1) ? "" : dr.GetString(1);
                txtAnswer.Text = dr.IsDBNull(2) ? "" : dr.GetString(2);
                txtSortOrder.Text = dr.IsDBNull(3) ? "" : dr.GetSqlInt16(3).ToString();
                if (dr.GetBoolean(4)) chkEnabled.Checked = true;
            }
            if (dr != null) { dr.Close(); }

        }
        else
        {
            txtCategory.Text = "";
            txtQuestion.Text = "";
            txtAnswer.Text = "";
            txtSortOrder.Text = "";
            chkEnabled.Checked = false;
        }
    }

    public void reviewFAQ(string FaqID)
    {

        Response.Redirect("ManageFaqs.aspx?listingID=" + FaqID);
    }


    public void btnSave_Click(object sender, System.EventArgs e)
    {
        Page.Validate();
        FaqID = Session["FaqID"].ToString();

        if (Page.IsValid)
        {
            if (string.IsNullOrEmpty(FaqID))
            {
                string sql = @"
                    INSERT INTO FAQs (Category, Question, Answer, SortOrder, Enabled) 
                    VALUES (@Category, @Question, @Answer, @SortOrder, @Enabled)
                ";

                string category = txtCategory.Text.Replace("'", "''");
                string question = txtQuestion.Text.Replace("'", "''");
                string answer = txtAnswer.Text.Replace("'", "''");
                string sortOrder = txtSortOrder.Text.Replace("'", "''");
                string enabled = Convert.ToInt32(chkEnabled.Checked).ToString();

                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, category, question, answer, sortOrder, enabled);
                SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

                if (dr != null) { dr.Close(); }

                Response.Redirect("ManageFaqs.aspx?listingID=" + "");
            }
            else
            {
                string sql = @"
                    UPDATE Faqs 
                    SET  
                        Category = @Category, 
                        Question = @Question, 
                        Answer = @Answer, 
                        SortOrder = @SortOrder, 
                        Enabled = @Enabled, 
                        DateModified = getdate()
                    WHERE FaqID = @FaqID 
                ";

                string category = txtCategory.Text.Replace("'", "''");
                string question = txtQuestion.Text.Replace("'", "''");
                string answer = txtAnswer.Text.Replace("'", "''");
                string sortOrder = txtSortOrder.Text.Replace("'", "''");
                string enabled = Convert.ToInt32(chkEnabled.Checked).ToString();

                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, category, question, answer, sortOrder, enabled, FaqID);
                SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

                if (dr != null) { dr.Close(); }

                string resp = "ManageFaqs.aspx?listingID=" + FaqID;
                Response.Redirect(resp);
            }
        }
    }

    protected void valeditCategory_ServerValidate(object source, ServerValidateEventArgs args)
    {
        txtCategory.Text = ValidateUtil.CleanUpString(txtCategory.Text);
        args.IsValid = true;
    }

    protected void valeditQuestion_ServerValidate(object source, ServerValidateEventArgs args)
    {
        txtQuestion.Text = ValidateUtil.CleanUpString(txtQuestion.Text);
        args.IsValid = true;
    }

    protected void valeditAnswer_ServerValidate(object source, ServerValidateEventArgs args)
    {
        txtAnswer.Text = ValidateUtil.CleanUpString(txtAnswer.Text);
        args.IsValid = true;
    }

}
