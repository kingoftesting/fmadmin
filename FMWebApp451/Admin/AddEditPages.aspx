<%@ Page Language="C#" Theme="Admin" MasterPageFile="MasterPage3_Blank.master" ValidateRequest="false" AutoEventWireup="true" Inherits="FaceMaster.Admin.AddEditPages" Title="Edit Pages" Codebehind="AddEditPages.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="sectiontitle">
  <asp:Literal runat="server" ID="lblNewPage" Text="Create New Page Entry" Visible="false" />
</div>
<p></p>

   <div id="AdminNavContainer">
   <ul id="AdminNavlist">
    <li><asp:HyperLink runat="server" ID="lnkManagePages" NavigateUrl="ManagePages.aspx">Update Another Page</asp:HyperLink></li>
   </ul>
   </div>
   <p></p>
   
<asp:DetailsView 
    ID="pageUpdate"
    SkinID="dtlsAdmin"
    emptydatatext="DetailsView: No Data."
    autogeneraterows="false" 
    autogenerateinsertbutton="true"
    autogenerateeditbutton="true"
    allowpaging="true"
    DataKeyNames="PageID"  
    DataSourceID="myPagesDetails"
    DefaultMode="readonly" 
    HeaderText="New Page Entry" 
    OnItemCreated="pageUpdate_ItemCreated"
    OnItemUpdating="pageUpdate_DataUpdating"
    OnItemUpdated="pageUpdate_DataUpdated"
    OnItemInserting="pageUpdate_DataInserting" 
    OnDataBound="pageUpdate_DataBound" 
    OnModeChanged="pageUpdate_ModeChanged"
    runat="server">
     <FieldHeaderStyle Width="100px" />
     <Fields>
         <asp:BoundField DataField="PageID" HeaderText="PageID" Visible="False" ReadOnly="True"
            SortExpression="PageID" />

         <asp:TemplateField HeaderText="PageName" SortExpression="PageName">
            <ItemTemplate>
               <asp:Label ID="lblPageName" runat="server" Text='<%# Eval("PageName") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtPageName" runat="server" Text='<%# Bind("PageName") %>' Width="100%" MaxLength="50"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequirePageName" runat="server" ControlToValidate="edittxtPageName" SetFocusOnError="true"
                  Text="The PageName field is required." ToolTip="The PageName field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:CustomValidator ID="valeditPageName" runat="server" ControlToValidate="edittxtPageName" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valeditPageName_ServerValidate"></asp:CustomValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtPageName" runat="server" Text='<%# Bind("PageName") %>' Width="100%" MaxLength="50"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequirePageName" runat="server" ControlToValidate="InserttxtPageName" SetFocusOnError="true"
                  Text="The PageName field is required." ToolTip="The PageName field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:CustomValidator ID="valinsertPageName" runat="server" ControlToValidate="inserttxtPageName" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valinsertPageName_ServerValidate"></asp:CustomValidator>
            </InsertItemTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="PageLink" SortExpression="PageLink">
            <ItemTemplate>
               <asp:Label ID="lblPageLink" runat="server" Text='<%# Eval("PageLink") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtPageLink" runat="server" Text='<%# Bind("PageLink") %>' Width="100%" MaxLength="250"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequirePagLink" runat="server" ControlToValidate="edittxtPageLink" SetFocusOnError="true"
                  Text="The PageLink field is required." ToolTip="The PageLink field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:CustomValidator ID="valeditPageLink" runat="server" ControlToValidate="edittxtPageLink" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valeditPageLink_ServerValidate"></asp:CustomValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtPageLink" runat="server" Text='<%# Bind("PageLink") %>' Width="100%" MaxLength="250"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequirePagLink" runat="server" ControlToValidate="inserttxtPageLink" SetFocusOnError="true"
                  Text="The PageLink field is required." ToolTip="The PageLink field is required." Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="valinsertPageLink" runat="server" ControlToValidate="inserttxtPageLink" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valinsertPageLink_ServerValidate"></asp:CustomValidator>
           </insertItemTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="PageFile" SortExpression="PageLink">
            <ItemTemplate>
               <asp:Label ID="lblPageFile" runat="server" Text='<%# Eval("PageFile") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtPageFile" runat="server" Text='<%# Bind("PageFile") %>' Width="100%" MaxLength="254"></asp:TextBox>
               <asp:CustomValidator ID="valeditPageFile" runat="server" ControlToValidate="edittxtPageFile" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valeditPageFile_ServerValidate"></asp:CustomValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtPageFile" runat="server" Text='<%# Bind("PageFile") %>' Width="100%" MaxLength="254"></asp:TextBox>
                <asp:CustomValidator ID="valinsertPageFile" runat="server" ControlToValidate="inserttxtPageFile" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valinsertPageFile_ServerValidate"></asp:CustomValidator>
            </InsertItemTemplate>
         </asp:TemplateField>
 
         <asp:TemplateField HeaderText="PageTitle" SortExpression="PageTitle">
            <ItemTemplate>
               <asp:Label ID="lblPageTitle" runat="server" Text='<%# Eval("PageTitle") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtPageTitle" runat="server" Text='<%# Bind("PageTitle") %>' Width="100%" MaxLength="254"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequirePagTitle" runat="server" ControlToValidate="edittxtPageTitle" SetFocusOnError="true"
                  Text="The PageTitle field is required." ToolTip="The PageTitle field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:CustomValidator ID="valeditPageTitle" runat="server" ControlToValidate="edittxtPageTitle" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valeditPageTitle_ServerValidate"></asp:CustomValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtPageTitle" runat="server" Text='<%# Bind("PageTitle") %>' Width="100%" MaxLength="254"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequirePagTitle" runat="server" ControlToValidate="inserttxtPageTitle" SetFocusOnError="true"
                  Text="The PageTitle field is required." ToolTip="The PageTitle field is required." Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="valinsertPageTitle" runat="server" ControlToValidate="inserttxtPageTitle" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valinsertPageTitle_ServerValidate"></asp:CustomValidator>
            </insertItemTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="PageSubTitle" SortExpression="PageSubTitle">
            <ItemTemplate>
               <asp:Label ID="lblPageSubTitle" runat="server" Text='<%# Eval("PageSubTitle") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtPageSubTitle" runat="server" Text='<%# Bind("PageSubTitle") %>' Width="100%" MaxLength="254"></asp:TextBox>
               <asp:CustomValidator ID="valeditPageSubTitle" runat="server" ControlToValidate="edittxtPageSubTitle" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valeditPageSubTitle_ServerValidate"></asp:CustomValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtPageSubTitle" runat="server" Text='<%# Bind("PageSubTitle") %>' Width="100%" MaxLength="254"></asp:TextBox>
                <asp:CustomValidator ID="valinsertPageSubTitle" runat="server" ControlToValidate="inserttxtPageSubTitle" 
                ErrorMessage="Text input contains potentially illegal characters" OnServerValidate="valinsertPageSubTitle_ServerValidate"></asp:CustomValidator>
            </InsertItemTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="TopNavSort" SortExpression="TopNavSort">
            <ItemTemplate>
               <asp:Label ID="lblTopNavSort" runat="server" Text='<%# Eval("TopNavSort") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtTopNavSort" runat="server" Text='<%# Bind("TopNavSort") %>' Width="10%" MaxLength="20"></asp:TextBox>
               <asp:RangeValidator ID="valrangeeditTopNavSort" runat="server" ControlToValidate="edittxtTopNavSort"
                Type="Integer" MinimumValue = "1" MaximumValue="5"
                ErrorMessage="Input needs to be an integer between 1 and 5"></asp:RangeValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtTopNavSort" runat="server" Text='<%# Bind("TopNavSort") %>' Width="10%" MaxLength="20"></asp:TextBox>
               <asp:RangeValidator ID="valrangeinsertTopNavSort" runat="server" ControlToValidate="inserttxtTopNavSort"
                Type="Integer" MinimumValue= "1" MaximumValue="5"
                ErrorMessage="Input needs to be an integer between 1 and 5"></asp:RangeValidator>
            </InsertItemTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="BottomNavSort" SortExpression="BottomNavSort">
            <ItemTemplate>
               <asp:Label ID="lblBottomNavSort" runat="server" Text='<%# Eval("BottomNavSort") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtBottomNavSort" runat="server" Text='<%# Bind("BottomNavSort") %>' Width="10%" MaxLength="20"></asp:TextBox>
               <asp:RangeValidator ID="valrangeeditBottomNavSort" runat="server" ControlToValidate="edittxtBottomNavSort"
                Type="Integer" MinimumValue = "1" MaximumValue="5"
                ErrorMessage="Input needs to be an integer between 1 and 5"></asp:RangeValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtBottomNavSort" runat="server" Text='<%# Bind("BottomNavSort") %>' Width="10%" MaxLength="20"></asp:TextBox>
               <asp:RangeValidator ID="valrangeinsertBottomNavSort" runat="server" ControlToValidate="inserttxtBottomNavSort"
                Type="Integer" MinimumValue= "1" MaximumValue="5"
                ErrorMessage="Input needs to be an integer between 1 and 5"></asp:RangeValidator>
           </InsertItemTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="Public Display" SortExpression="PublicDisplay">
            <ItemTemplate>
               <asp:CheckBox ID="chkPublicDisplay" runat="server" Checked='<%# Eval("PublicDisplay") %>' Enabled="True" />
            </ItemTemplate>
            <EditItemTemplate>
               <asp:CheckBox ID="editchkPublicDisplay" runat="server" Checked='<%# Bind("PublicDisplay") %>' />
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:CheckBox ID="insertchkPublicDisplay" runat="server" Checked='<%# Bind("PublicDisplay") %>' />
            </InsertItemTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="SiteID" SortExpression="SiteID">
            <ItemTemplate>
               <asp:Label ID="lblSiteID" runat="server" Text='1'> </asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtSiteID" runat="server" Text='<%# Bind("SiteID") %>' Width="10%" MaxLength="20"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequireSiteID" runat="server" ControlToValidate="edittxtSiteID" SetFocusOnError="true"
                  Text="The SiteID field is required." ToolTip="The SiteID field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:RangeValidator ID="valrangeeditSiteID" runat="server" ControlToValidate="edittxtSiteID"
                Type="Integer" MinimumValue = "1" MaximumValue="1"
                ErrorMessage="SiteID Input needs to be 1"></asp:RangeValidator>
           </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtSiteID" runat="server" Text='<%# Bind("SiteID") %>' Width="10%" MaxLength="20"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequireSiteID" runat="server" ControlToValidate="inserttxtSiteID" SetFocusOnError="true"
                  Text="The SiteID field is required." ToolTip="The SiteID field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:RangeValidator ID="valrangeinsertSiteID" runat="server" ControlToValidate="inserttxtSiteID"
                Type="Integer" MinimumValue= "1" MaximumValue="1"
                ErrorMessage="Input needs to be an integer between 1 and 5"></asp:RangeValidator>
           </InsertItemTemplate>
         </asp:TemplateField>

     </Fields>
</asp:DetailsView>
<p></p>

<div style="width:900px; text-align:left;">

<asp:Label ID="lblParagraph1" runat="server" Text="Paragraph1: "></asp:Label>
<asp:Literal ID="litParagraph1" runat="server"></asp:Literal>
<asp:TextBox ID="edittxtParagraph1" runat="server" Text="" Rows="5" TextMode="MultiLine" Width="90%" MaxLength="8000"></asp:TextBox>
<asp:CustomValidator ID="valeditParagraph1" runat="server" ControlToValidate="edittxtParagraph1" 
    ErrorMessage="Text input is either too long or contains potentially illegal characters" OnServerValidate="valeditParagraph1_ServerValidate"></asp:CustomValidator>
<p></p>

<asp:Label ID="lblParagraph2" runat="server" Text="Paragraph2: "></asp:Label>
<asp:Literal ID="litParagraph2" runat="server"></asp:Literal>
<asp:TextBox ID="edittxtParagraph2" runat="server" Text="" Rows="5" TextMode="MultiLine" Width="90%" MaxLength="8000"></asp:TextBox>
<asp:CustomValidator ID="valeditParagraph2" runat="server" ControlToValidate="edittxtParagraph2" 
    ErrorMessage="Text input is either too long or contains potentially illegal characters" OnServerValidate="valeditParagraph2_ServerValidate"></asp:CustomValidator>
<p></p>

<asp:Label ID="lblParagraph3" runat="server" Text="Paragraph3: "></asp:Label>
<asp:Literal ID="litParagraph3" runat="server"></asp:Literal>
<asp:TextBox ID="edittxtParagraph3" runat="server" Text="" Rows="5" TextMode="MultiLine" Width="90%" MaxLength="8000"></asp:TextBox>
<asp:CustomValidator ID="valeditParagraph3" runat="server" ControlToValidate="edittxtParagraph3" 
    ErrorMessage="Text input is either too long or contains potentially illegal characters" OnServerValidate="valeditParagraph3_ServerValidate"></asp:CustomValidator>
<p></p>

<asp:Label ID="lblParagraph4" runat="server" Text="Paragraph4: "></asp:Label>
<asp:Literal ID="litParagraph4" runat="server"></asp:Literal>
<asp:TextBox ID="edittxtParagraph4" runat="server" Text="" Rows="5" TextMode="MultiLine" Width="90%" MaxLength="8000"></asp:TextBox>
<asp:CustomValidator ID="valeditParagraph4" runat="server" ControlToValidate="edittxtParagraph4" 
    ErrorMessage="Text input is either too long or contains potentially illegal characters" OnServerValidate="valeditParagraph4_ServerValidate"></asp:CustomValidator>
<p></p>

<asp:Label ID="lblParagraph5" runat="server" Text="Paragraph5: "></asp:Label>
<asp:Literal ID="litParagraph5" runat="server"></asp:Literal>
<asp:TextBox ID="edittxtParagraph5" runat="server" Text="" Rows="5" TextMode="MultiLine" Width="90%" MaxLength="8000"></asp:TextBox>
<asp:CustomValidator ID="valeditParagraph5" runat="server" ControlToValidate="edittxtParagraph5" 
    ErrorMessage="Text input is either too long or contains potentially illegal characters" OnServerValidate="valeditParagraph5_ServerValidate"></asp:CustomValidator>
<p></p>

</div>


<asp:SqlDataSource 
    ID="myPagesDetails"
    SelectCommand="
        SELECT * 
        FROM Pages 
        WHERE PageID=@PageID"
    UpdateCommand="
        UPDATE Pages 
        SET PageName=@PageName, PageLink=@PageLink, PageFile=@PageFile,
        PageTitle=@PageTitle, PageSubTitle=@PageSubTitle, TopNavSort=@TopNavSort, BottomNavSort=@BottomNavSort,
        PublicDisplay=@PublicDisplay, SiteID=@SiteID
        WHERE PageID=@PageID"
    InsertCommand="
        INSERT INTO Pages 
            (PageName, PageLink, PageFile, PageTitle, PageSubTitle, TopNavSort, BottomNavSort,
            PublicDisplay, SiteID)
        VALUES 
            (@PageName, @PageLink, @PageFile, @PageTitle, @PageSubTitle, @TopNavSort, @BottomNavSort,
            @PublicDisplay, @SiteID)"
    runat="server">
    <SelectParameters>
         <asp:Parameter Name="PageID" Type="Int32" />
    </SelectParameters>
    <UpdateParameters>
         <asp:Parameter Name="PageID" Type="Int32" />
         <asp:Parameter Name="PageName" Type="String" ConvertEmptyStringToNull="false"/>
         <asp:Parameter Name="PageLink" Type="String" ConvertEmptyStringToNull="false"/>
         <asp:Parameter Name="PageFile" Type="String" ConvertEmptyStringToNull="false"/>
         <asp:Parameter Name="PageTitle" Type="String" ConvertEmptyStringToNull="false"/>
         <asp:Parameter Name="PageSubTitle" Type="String" ConvertEmptyStringToNull="false"/>
         <asp:Parameter Name="TopNavSort" Type="Int16" />
         <asp:Parameter Name="BottomNavSort" Type="Int16" />
         <asp:Parameter Name="PublicDisplay" Type="Boolean" />
         <asp:Parameter Name="SiteID" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
         <asp:Parameter Name="PageID" Type="Int32" />
         <asp:Parameter Name="PageName" Type="String" ConvertEmptyStringToNull="false"/>
         <asp:Parameter Name="PageLink" Type="String" ConvertEmptyStringToNull="false"/>
         <asp:Parameter Name="PageFile" Type="String" ConvertEmptyStringToNull="false"/>
         <asp:Parameter Name="PageTitle" Type="String" ConvertEmptyStringToNull="false"/>
         <asp:Parameter Name="PageSubTitle" Type="String" ConvertEmptyStringToNull="false"/>
         <asp:Parameter Name="TopNavSort" Type="Int16" />
         <asp:Parameter Name="BottomNavSort" Type="Int16" />
         <asp:Parameter Name="PublicDisplay" Type="Boolean" />
         <asp:Parameter Name="SiteID" Type="Int32" />
    </InsertParameters>

</asp:SqlDataSource>


</asp:Content>


