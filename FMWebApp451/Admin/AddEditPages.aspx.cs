using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

using Certnet;
using FM2015.Helpers;

namespace FaceMaster.Admin
{

    public partial class AddEditPages : AdminBasePage //System.Web.UI.Page
    {
        Cart cart;

        void Page_Init(Object sender, EventArgs e)
        {
            cart = new Cart();
            cart.Load(Session["cart"]);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int customerID = cart.SiteCustomer.CustomerID;

            myPagesDetails.ConnectionString = AdminCart.Config.ConnStr();

            if (!this.IsPostBack)
            {
                // Check that the current user is an Administrator or an Editor
                if (!string.IsNullOrEmpty(this.Request.QueryString["PageID"]))
                {
                    myPagesDetails.SelectParameters["PageID"].DefaultValue = Request["PageID"];


                    if (ValidateUtil.IsInRole(customerID, "ADMIN"))
                    {
                        pageUpdate.ChangeMode(DetailsViewMode.Edit);
                        UpdateTitle();
                        int id = Convert.ToInt32(Request["PageID"]);
                        paraUpdate_Select(id);
                    }
                    else
                        throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
                }
                else
                {

                    if (ValidateUtil.IsInRole(customerID, "ADMIN"))
                    {
                        pageUpdate.ChangeMode(DetailsViewMode.Insert);
                        UpdateTitle();
                    }
                    else
                        throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
                }
            }

        }

        protected void pageUpdate_ItemCreated(object sender, EventArgs e)
        {
        }


        protected void paraUpdate_Select(int id)
        {
            try
            {
                //refresh the DetailsView...
                string sql = @"
                SELECT ParagraphText
                FROM Paragraphs
                WHERE (pageID =
                ";

                sql += "'" + Convert.ToString(id) + "')";
                sql += @"
                AND ParagraphVersion = 
                    (Select max(ParagraphVersion) 
                        FROM Paragraphs 
                        WHERE (PageID =
                ";

                sql += "'" + Convert.ToString(id) + "'))" + "ORDER BY PagePosition ASC";


                FillMyParagraphs(sql);
            }
            catch (Exception ex)
            {
                FM2015.Helpers.dbErrorLogging.LogError("AdminEditPages:paraUpdate_DataInserting: FillMyDetailsView", ex);
            }
            

        }

        protected void paraUpdate_DataSelecting(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["PageID"]))
            {
                int id = Convert.ToInt32(Request["PageID"]);
                paraUpdate_Select(id);
            }
            else
            {
                // do nothing
            }
        }

        protected void pageUpdate_DataInserting(object sender, EventArgs e)
        {
            TextBox inserttxtSiteID = pageUpdate.FindControl("inserttxtSiteID") as TextBox;
            inserttxtSiteID.Text = "1";


        }

        protected void paraUpdate_DataInserting(int id)
        {
            int NextVersion = 1;

            try
            {
                string sql = @"
                SELECT Max(ParagraphVersion) 
                FROM  Paragraphs 
                Where PageID = 
                ";
                sql += "'" + id.ToString() + "'";

                SqlDataReader dr = DBUtil.FillDataReader(sql);

                int t;
                if (dr.Read())
                {
                      if ( Int32.TryParse(dr[0].ToString(), out t) )//location 0 is "max(ParagraphVersion)"
                        { NextVersion = t + 1; }
                      else { NextVersion = 1; }
                }
                else { NextVersion = 1; }
                dr.Close();


                sql = @"
                INSERT Paragraphs 
                VALUES(@PageID, @PagePosition, @NextVersion, @ParagraphText) 
                ";

                string ParagraphText = edittxtParagraph1.Text;
                int PagePosition = 1;
                if (ParagraphText.Length > 8000)
                { }

                SqlParameter[] mySqlParameters = new SqlParameter[4];
                mySqlParameters[0] = new SqlParameter("@PageID", SqlDbType.Int);
                mySqlParameters[0].Value = id;
                mySqlParameters[1] = new SqlParameter("@PagePosition", SqlDbType.Int);
                mySqlParameters[1].Value = PagePosition;
                mySqlParameters[2] = new SqlParameter("@NextVersion", SqlDbType.Int);
                mySqlParameters[2].Value = NextVersion;
                mySqlParameters[3] = new SqlParameter("@ParagraphText", SqlDbType.VarChar, 8000);
                mySqlParameters[3].Value = ParagraphText;
                DBUtil.Exec(sql, mySqlParameters);

                ParagraphText = edittxtParagraph2.Text;
                PagePosition = 2;
                mySqlParameters = new SqlParameter[4];
                mySqlParameters[0] = new SqlParameter("@PageID", SqlDbType.Int);
                mySqlParameters[0].Value = id;
                mySqlParameters[1] = new SqlParameter("@PagePosition", SqlDbType.Int);
                mySqlParameters[1].Value = PagePosition;
                mySqlParameters[2] = new SqlParameter("@NextVersion", SqlDbType.Int);
                mySqlParameters[2].Value = NextVersion;
                mySqlParameters[3] = new SqlParameter("@ParagraphText", SqlDbType.VarChar, 8000);
                mySqlParameters[3].Value = ParagraphText;
                DBUtil.Exec(sql, mySqlParameters);

                ParagraphText = edittxtParagraph3.Text;
                PagePosition = 3;
                mySqlParameters = new SqlParameter[4];
                mySqlParameters[0] = new SqlParameter("@PageID", SqlDbType.Int);
                mySqlParameters[0].Value = id;
                mySqlParameters[1] = new SqlParameter("@PagePosition", SqlDbType.Int);
                mySqlParameters[1].Value = PagePosition;
                mySqlParameters[2] = new SqlParameter("@NextVersion", SqlDbType.Int);
                mySqlParameters[2].Value = NextVersion;
                mySqlParameters[3] = new SqlParameter("@ParagraphText", SqlDbType.VarChar, 8000);
                mySqlParameters[3].Value = ParagraphText;
                DBUtil.Exec(sql, mySqlParameters);

                ParagraphText = edittxtParagraph4.Text;
                PagePosition = 4;
                mySqlParameters = new SqlParameter[4];
                mySqlParameters[0] = new SqlParameter("@PageID", SqlDbType.Int);
                mySqlParameters[0].Value = id;
                mySqlParameters[1] = new SqlParameter("@PagePosition", SqlDbType.Int);
                mySqlParameters[1].Value = PagePosition;
                mySqlParameters[2] = new SqlParameter("@NextVersion", SqlDbType.Int);
                mySqlParameters[2].Value = NextVersion;
                mySqlParameters[3] = new SqlParameter("@ParagraphText", SqlDbType.VarChar, 8000);
                mySqlParameters[3].Value = ParagraphText;
                DBUtil.Exec(sql, mySqlParameters);

                ParagraphText = edittxtParagraph5.Text;
                PagePosition = 5;
                mySqlParameters = new SqlParameter[4];
                mySqlParameters[0] = new SqlParameter("@PageID", SqlDbType.Int);
                mySqlParameters[0].Value = id;
                mySqlParameters[1] = new SqlParameter("@PagePosition", SqlDbType.Int);
                mySqlParameters[1].Value = PagePosition;
                mySqlParameters[2] = new SqlParameter("@NextVersion", SqlDbType.Int);
                mySqlParameters[2].Value = NextVersion;
                mySqlParameters[3] = new SqlParameter("@ParagraphText", SqlDbType.VarChar, 8000);
                mySqlParameters[3].Value = ParagraphText;
                DBUtil.Exec(sql, mySqlParameters);

            }

            catch (Exception ex)
            {
                FM2015.Helpers.dbErrorLogging.LogError("AdminEditPages:paraUpdate_DataInserting: updating Paragraphs", ex);
            }

        }

        protected void pageUpdate_DataUpdating(object sender, EventArgs e)
        {
        }

        protected void pageUpdate_DataUpdated(object sender, EventArgs e)
        {
            //update the respective paragraphs after the Page DetailsView has been updated

            OrderedDictionary myparams = myPagesDetails.SelectParameters.GetValues(HttpContext.Current, myPagesDetails) as OrderedDictionary;

            int id = (int)myparams["PageID"];

            paraUpdate_DataInserting(id);

            paraUpdate_Select(id); //refresh the display in REVIEW mode (was EDIT)

        }
        
        protected void pageUpdate_DataBound(object sender, EventArgs e)
        {
            // In editMode, preselect the PublicDisply checkbox & SiteID
            if (pageUpdate.CurrentMode == DetailsViewMode.Insert)
            {
                CheckBox insertchkPublicDisplay = pageUpdate.FindControl("insertchkPublicDisplay") as CheckBox;
                insertchkPublicDisplay.Checked = true;


                /* example of setting a box based on roles
                 bool canApprove = (this.User.IsInRole("Administrators") || this.User.IsInRole("Editors"));
                 chkApproved.Enabled = canApprove;
                 chkApproved.Checked = canApprove;
                 */
            }

            if (pageUpdate.CurrentMode == DetailsViewMode.ReadOnly)
            {
                edittxtParagraph1.Visible = false;
                edittxtParagraph2.Visible = false;
                edittxtParagraph3.Visible = false;
                edittxtParagraph4.Visible = false;
                edittxtParagraph5.Visible = false;
                litParagraph1.Visible = true;
                litParagraph2.Visible = true;
                litParagraph3.Visible = true;
                litParagraph4.Visible = true;
                litParagraph5.Visible = true;
            }
            else
            {
                edittxtParagraph1.Visible = true;
                edittxtParagraph2.Visible = true;
                edittxtParagraph3.Visible = true;
                edittxtParagraph4.Visible = true;
                edittxtParagraph5.Visible = true;
                litParagraph1.Visible = false;
                litParagraph2.Visible = false;
                litParagraph3.Visible = false;
                litParagraph4.Visible = false;
                litParagraph5.Visible = false;
            }
        }

        protected void pageUpdate_ModeChanged(object sender, EventArgs e)
        {
            UpdateTitle();
        }

        private void UpdateTitle()
        {
            //initially turn off all label messages
            lblNewPage.Visible = false;

            //find the one that should be on, make it visible. Default is Review
            if (pageUpdate.CurrentMode == DetailsViewMode.Insert) lblNewPage.Visible = true;
        }

        private void FillMyParagraphs(string sql)
        {
 
            edittxtParagraph1.Text = ""; //initialize all the paragraph text
            edittxtParagraph2.Text = "";
            edittxtParagraph3.Text = "";
            edittxtParagraph4.Text = "";
            edittxtParagraph5.Text = "";

            SqlDataReader dr = DBUtil.FillDataReader(sql);

            if (dr.Read()) {edittxtParagraph1.Text = dr["paragraphText"].ToString();}
            if (dr.Read()) {edittxtParagraph2.Text = dr["paragraphText"].ToString();}
            if (dr.Read()) {edittxtParagraph3.Text = dr["paragraphText"].ToString();}
            if (dr.Read()) {edittxtParagraph4.Text = dr["paragraphText"].ToString();}
            if (dr.Read()) {edittxtParagraph5.Text = dr["paragraphText"].ToString();}

            //make sure ReadOnly & Edit modes have the same text
            litParagraph1.Text = edittxtParagraph1.Text;
            litParagraph2.Text = edittxtParagraph2.Text;
            litParagraph3.Text = edittxtParagraph3.Text;
            litParagraph4.Text = edittxtParagraph4.Text;
            litParagraph5.Text = edittxtParagraph5.Text;

            dr.Close();
        }


        protected void valeditPageName_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox t = pageUpdate.FindControl("edittxtPageName") as TextBox;
            t.Text = ValidateUtil.CleanUpString(t.Text);
            args.IsValid = true;
        }

        protected void valinsertPageName_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox t = pageUpdate.FindControl("inserttxtPageName") as TextBox;
            t.Text = ValidateUtil.CleanUpString(t.Text);
            args.IsValid = true;
        }

        protected void valeditPageLink_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox t = pageUpdate.FindControl("edittxtPageLink") as TextBox;
            t.Text = ValidateUtil.CleanUpString(t.Text);
            args.IsValid = true;
        }

        protected void valinsertPageLink_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox t = pageUpdate.FindControl("inserttxtPageLink") as TextBox;
            t.Text = ValidateUtil.CleanUpString(t.Text);
            args.IsValid = true;
        }

        protected void valeditPageFile_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox t = pageUpdate.FindControl("edittxtPageFile") as TextBox;
            t.Text = ValidateUtil.CleanUpString(t.Text);
            args.IsValid = true;
        }

        protected void valinsertPageFile_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox t = pageUpdate.FindControl("inserttxtPageFile") as TextBox;
            t.Text = ValidateUtil.CleanUpString(t.Text);
            args.IsValid = true;
        }

        protected void valeditPageTitle_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox t = pageUpdate.FindControl("edittxtPageTitle") as TextBox;
            t.Text = ValidateUtil.CleanUpString(t.Text);
            args.IsValid = true;
        }

        protected void valinsertPageTitle_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox t = pageUpdate.FindControl("inserttxtPageTitle") as TextBox;
            t.Text = ValidateUtil.CleanUpString(t.Text);
            args.IsValid = true;
        }

        protected void valeditPageSubTitle_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox t = pageUpdate.FindControl("edittxtPageSubTitle") as TextBox;
            t.Text = ValidateUtil.CleanUpString(t.Text);
            args.IsValid = true;
        }

        protected void valinsertPageSubTitle_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox t = pageUpdate.FindControl("inserttxtPageSubTitle") as TextBox;
            t.Text = ValidateUtil.CleanUpString(t.Text);
            args.IsValid = true;
        }

        protected void valeditParagraph1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            edittxtParagraph1.Text = ValidateUtil.CleanUpString(edittxtParagraph1.Text);
            if (edittxtParagraph1.Text.Length > 8000) { args.IsValid = false; }
            else { args.IsValid = true; }
        }

        protected void valeditParagraph2_ServerValidate(object source, ServerValidateEventArgs args)
        {
            edittxtParagraph2.Text = ValidateUtil.CleanUpString(edittxtParagraph2.Text);
            if (edittxtParagraph2.Text.Length > 8000) { args.IsValid = false; }
            else { args.IsValid = true; }
        }

        protected void valeditParagraph3_ServerValidate(object source, ServerValidateEventArgs args)
        {
            edittxtParagraph3.Text = ValidateUtil.CleanUpString(edittxtParagraph3.Text);
            if (edittxtParagraph3.Text.Length > 8000) { args.IsValid = false; }
            else { args.IsValid = true; }
        }

        protected void valeditParagraph4_ServerValidate(object source, ServerValidateEventArgs args)
        {
            edittxtParagraph4.Text = ValidateUtil.CleanUpString(edittxtParagraph4.Text);
            if (edittxtParagraph4.Text.Length > 8000) { args.IsValid = false; }
            else { args.IsValid = true; }
        }

        protected void valeditParagraph5_ServerValidate(object source, ServerValidateEventArgs args)
        {
            edittxtParagraph5.Text = ValidateUtil.CleanUpString(edittxtParagraph5.Text);
            if (edittxtParagraph5.Text.Length > 8000) { args.IsValid = false; }
            else { args.IsValid = true; }
        }

    }

}
