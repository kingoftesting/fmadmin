<%@ Page Language="C#" ValidateRequest="false" Theme="Admin" MasterPageFile="MasterPage3_Blank.master" AutoEventWireup="true" Inherits="AddEditProduct" Title="Add/Edit Product" Codebehind="AddEditProduct.aspx.cs" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" Runat="Server">
   <div class="sectiontitle">ADD OR EDIT A NEW FM PRODUCT TO THE DATABASE
   </div>
   <p></p>
   
    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="lblErrorMessage" ForeColor="red" Runat="server" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="lblProductID2" Text="ProductID" Runat="server" />
		<asp:Label ID="lblProductID" Text="" Runat="server" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="lblSku" Text="Sku" Runat="server" />
		<asp:TextBox width="200px" ID="txtSku" Runat="server" MaxLength="100" />
        <asp:requiredfieldvalidator id="Requiredfieldvalidator1" Runat="server" Display="Dynamic" ErrorMessage="SKU is required"
				ControlToValidate="txtSku"></asp:requiredfieldvalidator>

	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label1" Text="Name" Runat="server" />
		<asp:TextBox ID="txtName"  Runat="server" Width="50%" MaxLength="4000" />
        <asp:requiredfieldvalidator id="Requiredfieldvalidator2" Runat="server" Display="Dynamic" ErrorMessage="Name is required"
				ControlToValidate="txtName"></asp:requiredfieldvalidator>
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="lblIsSubscription" Text="Subscription Product?" Runat="server" />
		<asp:CheckBox width="100px" ID="chkIsSubscription" Runat="server" />
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="lblSubscriptionPlan" Text="Subscription Plan" Runat="server" />
		<asp:TextBox ID="txtSubscriptionPlan"  Runat="server" Width="50%" MaxLength="1024" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label20" Text="Short Descr." Runat="server" />
		<asp:TextBox ID="txtShortDescription" TextMode="MultiLine" Width="80%" Rows="2" Runat="server" MaxLength="4000" />
        <asp:requiredfieldvalidator id="Requiredfieldvalidator3" Runat="server" Display="Dynamic" ErrorMessage="Short Description is required"
				ControlToValidate="txtShortDescription"></asp:requiredfieldvalidator>

	</div>
    				
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label3" Text="Description" Runat="server" />
		<asp:TextBox ID="txtDescription" TextMode="MultiLine" Width="80%" Rows="5" Runat="server" MaxLength="4000" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="lblIsBundle" Text="IsBundle?" Runat="server" />
		<asp:CheckBox width="100px" ID="chkIsBundle" Runat="server" />
	</div>
    				
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="lblBundleString" Text="BundleString" Runat="server" />
		<asp:TextBox ID="txtBundleString" TextMode="MultiLine" Width="80%" Rows="5" Runat="server" MaxLength="4000" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label7" Text="Cart Image" Runat="server" />
		<asp:TextBox width="200px" ID="txtCartImage" Runat="server" MaxLength="500" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label8" Text="Thumbnail&nbsp;Image" Runat="server" />
		<asp:TextBox width="200px" ID="txtThumbnailImage" Runat="server" MaxLength="500" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label9" Text="Icon Image" Runat="server" />
		<asp:TextBox width="200px" ID="txtIconImage" Runat="server" MaxLength="500" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label10" Text="Details Image" Runat="server" />
		<asp:TextBox width="200px" ID="txtDetailsImage" Runat="server" MaxLength="500" />
	</div>
    	
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label11" Text="Zoom Image" Runat="server" />
		<asp:TextBox width="200px" ID="txtZoomImage" Runat="server" MaxLength="500" />
	</div>
    	
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label12" Text="Original Image" Runat="server" />
		<asp:TextBox width="200px" ID="txtOriginalImage" Runat="server" MaxLength="500" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label4" Text="Country Of Origin" Runat="server" />
		<asp:TextBox width="200px" ID="txtCountryOfOrigin" Runat="server" MaxLength="500" />
        <asp:requiredfieldvalidator id="Requiredfieldvalidator4" Runat="server" Display="Dynamic" ErrorMessage="Country of Origin is required"
				ControlToValidate="txtCountryOfOrigin"></asp:requiredfieldvalidator>

	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label15" Text="Weight" Runat="server" />
		<asp:TextBox width="50" ID="txtWeight" Runat="server" MaxLength="6" />
        <asp:requiredfieldvalidator id="Requiredfieldvalidator5" Runat="server" Display="Dynamic" ErrorMessage="Weight is required"
				ControlToValidate="txtWeight"></asp:requiredfieldvalidator>

	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label16" Text="Price" Runat="server" />
		<asp:TextBox width="75" ID="txtPrice" Runat="server" MaxLength="6" />
        <asp:requiredfieldvalidator id="Requiredfieldvalidator6" Runat="server" Display="Dynamic" ErrorMessage="Price is required"
				ControlToValidate="txtPrice"></asp:requiredfieldvalidator>
	</div>
    		
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label17" Text="Sale Price" Runat="server" />
		<asp:TextBox width="75" ID="txtSalePrice" Runat="server" MaxLength="6" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="lblPerUnitShipping" Text="PerUnitShipping" Runat="server" />
		<asp:TextBox width="75" ID="txtPerUnitShipping" Runat="server" MaxLength="6" />
	</div>					
					
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label13" Text="Stock" Runat="server" />
		<asp:TextBox width="30" ID="txtStock" Runat="server" MaxLength="3" />
	</div>
    				
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label6" Text="Sort" Runat="server" />
		<asp:TextBox width="30" ID="txtSort" Runat="server" MaxLength="3" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label14" Text="Taxable" Runat="server" />
		<asp:CheckBox width="100px" ID="chkTaxable" Runat="server" />
	</div>
    				
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label5" Text="Enabled" Runat="server" />
		<asp:CheckBox width="100px" ID="chkEnabled" Runat="server" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="lblIsFMSystem" Text="FM System?" Runat="server" />
		<asp:CheckBox width="100px" ID="chkFMSystem" Runat="server" />
	</div>
	
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="lblIsShippable" Text="Shippable?" Runat="server" />
		<asp:CheckBox width="100px" ID="chkIsShippable" Runat="server" />
	</div>
	
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label19" Text="CustomerServiceOnly" Runat="server" />
		<asp:CheckBox width="100px" ID="chkCustomerServiceOnly" Runat="server" />
	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label2" Text="Product Cost" Runat="server" />
		<asp:TextBox width="75" ID="txtCost" Runat="server" />
        <asp:requiredfieldvalidator id="Requiredfieldvalidator7" Runat="server" Display="Dynamic" ErrorMessage="Cost is required"
				ControlToValidate="txtCost"></asp:requiredfieldvalidator>

	</div>

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="lblStartDate" Text="Product Cost StartDate" Runat="server" />
		<asp:TextBox width="200" ID="txtStartDate" Runat="server" />
	</div>

    <hr />
		    Product Cost History:
		    <br />
		    <asp:Literal ID="litCosts" runat="server"></asp:Literal>

	<div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Button ID="btnCancel" Text="Return To ManageProducts" OnClick="btnCancel_Click" Runat="server" />
		<asp:Button ID="btnSave" Text="Update Product" OnClick="btnAdd_Click" Runat="server" />
		<asp:Button ID="btnClone" Text="Clone (New) Product" OnClick="btnClone_Click" Runat="server" />
    </div>

</asp:Content>

