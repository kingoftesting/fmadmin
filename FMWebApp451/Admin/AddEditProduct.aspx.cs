using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using AdminCart;

public partial class AddEditProduct : AdminBasePage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Certnet.Cart cart = new Certnet.Cart();
            cart.Load(Session["cart"]);
            int customerID = cart.SiteCustomer.CustomerID;
            try
            {
                if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "REPORTS")))
                { }
                else
                {
                    throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
                }
            }
            catch (SecurityException ex)
            {
                string msg = "Error in AddEditProduct" + Environment.NewLine;
                msg += "User did not have permission to access page" + Environment.NewLine;
                msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                Response.Redirect("../Login.aspx");
            }

            int ProductID = Convert.ToInt32(Request["ProductID"]);
            Session["ProductID"] = ProductID; //save for later

            if (ProductID == 0) 
            { 
                lblProductID.Text = "[new product entry]";
                txtSku.Text = "";
                txtName.Text = "";
                chkIsSubscription.Checked = false;
                txtSubscriptionPlan.Text = "";
                txtShortDescription.Text = "";
                txtDescription.Text = "";
                txtBundleString.Text = "";
                txtCartImage.Text = "ImageNotAvailable.jpg";
                txtThumbnailImage.Text = "ImageNotAvailable.jpg";
                txtIconImage.Text = "ImageNotAvailable.jpg";
                txtDetailsImage.Text = "ImageNotAvailable.jpg";
                txtZoomImage.Text = "ImageNotAvailable.jpg";
                txtOriginalImage.Text = "ImageNotAvailable.jpg";
                txtCountryOfOrigin.Text = "China";
                txtWeight.Text = "";
                txtPrice.Text = "";
                txtSalePrice.Text = "";
                txtPerUnitShipping.Text = "";
                txtStock.Text = "";
                txtSort.Text = "";
                chkIsBundle.Checked = false;
                chkTaxable.Checked = true;
                chkEnabled.Checked = false;
                chkFMSystem.Checked = false;
                chkIsShippable.Checked = true;
                chkCustomerServiceOnly.Checked = false;
                txtCost.Text = "";
                txtStartDate.Text = DateTime.Now.ToString();
            }
            else 
            { 
                lblProductID.Text = ProductID.ToString();

                Product p = new Product(ProductID);
                txtSku.Text = p.Sku;
                txtName.Text = p.Name;
                chkIsSubscription.Checked = p.IsSubscription;
                txtSubscriptionPlan.Text = p.SubscriptionPlan;
                txtShortDescription.Text = p.ShortDescription;
                txtDescription.Text = p.Description;

                txtBundleString.Text = p.BundleString;

                if (String.IsNullOrEmpty(p.CartImage)) { txtCartImage.Text = "ImageNotAvailable.jpg"; }
                else { txtCartImage.Text = p.CartImage; }

                if (String.IsNullOrEmpty(p.ThumbnailImage)) { txtThumbnailImage.Text = "ImageNotAvailable.jpg"; }
                else { txtThumbnailImage.Text = p.ThumbnailImage; }

                if (String.IsNullOrEmpty(p.IconImage)) { txtIconImage.Text = "ImageNotAvailable.jpg"; }
                else { txtIconImage.Text = p.IconImage; }

                if (String.IsNullOrEmpty(p.DetailsImage)) { txtDetailsImage.Text = "ImageNotAvailable.jpg"; }
                else { txtDetailsImage.Text = p.DetailsImage; }

                if (String.IsNullOrEmpty(p.ZoomImage)) { txtZoomImage.Text = "ImageNotAvailable.jpg"; }
                else { txtZoomImage.Text = p.ZoomImage; }

                if (String.IsNullOrEmpty(p.OriginalImage)) { txtOriginalImage.Text = "ImageNotAvailable.jpg"; }
                else { txtOriginalImage.Text = p.OriginalImage; }

                txtCountryOfOrigin.Text = p.CountryOfOrigin;

                txtWeight.Text = p.Weight.ToString();
                txtPrice.Text = p.Price.ToString();
                txtSalePrice.Text = p.SalePrice.ToString();
                txtPerUnitShipping.Text = p.PerUnitShipping.ToString();
                txtStock.Text = p.Stock.ToString();
                txtSort.Text = p.Sort.ToString();

                if (p.IsBundle) { chkIsBundle.Checked = true; }
                else { chkIsBundle.Checked = false; }

                if (p.Taxable) { chkTaxable.Checked = true; }
                else { chkTaxable.Checked = false; }

                if (p.Enabled) { chkEnabled.Checked = true; }
                else { chkEnabled.Checked = false; }

                if (p.IsFMSystem) { chkFMSystem.Checked = true; }
                else { chkFMSystem.Checked = false; }

                if (p.IsShippable) { chkIsShippable.Checked = true; }
                else { chkIsShippable.Checked = false; }

                if (p.CSOnly) { chkCustomerServiceOnly.Checked = true; }
                else { chkCustomerServiceOnly.Checked = false; }

                ProductCost pcost = new ProductCost(ProductID);
                txtCost.Text = pcost.Cost.ToString();
                txtStartDate.Text = pcost.StartDate.ToString();

                litCosts.Text = GetProductCostListHtml(ProductID);

            }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int ProductID = Convert.ToInt32(Session["ProductID"]);

            //ProductDetails p = new ProductDetails();
            Product p = new Product();
            bool ok = false;
            p.ProductID = ProductID;
            p.Sku = ValidateUtil.CleanUpString(txtSku.Text);
            p.Name = ValidateUtil.CleanUpString(txtName.Text);

            if (chkIsSubscription.Checked == true) { p.IsSubscription = true; }
            else { p.IsSubscription = false; }

            p.SubscriptionPlan = ValidateUtil.CleanUpString(txtSubscriptionPlan.Text);
            p.ShortDescription = ValidateUtil.CleanUpString(txtShortDescription.Text);
            p.Description = ValidateUtil.CleanUpString(txtDescription.Text);
            if (string.IsNullOrEmpty(p.Description)) { p.Description = p.ShortDescription; }

            p.BundleString = ValidateUtil.CleanUpString(txtBundleString.Text);
            p.CartImage = ValidateUtil.CleanUpString(txtCartImage.Text);
            p.ThumbnailImage = ValidateUtil.CleanUpString(txtThumbnailImage.Text);
            p.IconImage = ValidateUtil.CleanUpString(txtIconImage.Text);
            p.DetailsImage = ValidateUtil.CleanUpString(txtDetailsImage.Text);
            p.ZoomImage = ValidateUtil.CleanUpString(txtZoomImage.Text);
            p.OriginalImage = ValidateUtil.CleanUpString(txtOriginalImage.Text);
            p.CountryOfOrigin = ValidateUtil.CleanUpString(txtCountryOfOrigin.Text);

            //p.Weight = Convert.ToDecimal(txtWeight.Text);
            decimal weight = 0;
            ok = decimal.TryParse(txtWeight.Text, out weight);
            p.Weight = weight;

            //p.Price = Convert.ToDecimal(txtPrice.Text);
            decimal price = 0;
            ok = decimal.TryParse(txtPrice.Text, out price);
            p.Price = price;

            //p.SalePrice = Convert.ToDecimal(txtSalePrice.Text);
            decimal salePrice = 0;
            ok = decimal.TryParse(txtSalePrice.Text, out salePrice);
            if (salePrice == 0) { p.SalePrice = price; }
            else { p.SalePrice = salePrice; }
            
            //p.PerUnitShipping = Convert.ToDecimal(txtPerUnitShipping.Text);
            decimal perUnitShipping = 0;
            ok = decimal.TryParse(txtPerUnitShipping.Text, out perUnitShipping);
            p.PerUnitShipping = perUnitShipping;

            int stock = 0;
            ok = Int32.TryParse(txtStock.Text, out stock);
            p.Stock = stock;

            int sort = 0;
            ok = Int32.TryParse(txtSort.Text, out sort);
            p.Sort = sort;

            if (chkIsBundle.Checked) { p.IsBundle = true; }
            else { p.IsBundle = false; }

            if (chkTaxable.Checked) { p.Taxable = true; }
            else { p.Taxable = false; }

            if (chkEnabled.Checked) { p.Enabled = true; }
            else { p.Enabled = false; }

            if (chkFMSystem.Checked) { p.IsFMSystem = true; }
            else { p.IsFMSystem = false; }

            if (chkIsShippable.Checked) { p.IsShippable = true; }
            else { p.IsShippable = false; }

            if (chkCustomerServiceOnly.Checked) { p.CSOnly = true; }
            else { p.CSOnly = false; }

            //p.Cost = Convert.ToDecimal(txtCost.Text);
            decimal cost = 0;
            ok = Decimal.TryParse(txtCost.Text, out cost);
            p.Cost = cost;

            int Result = p.Save(ProductID);
            if ((ProductID == 0) && (Result > 0)) { p.ProductID = Result; }

            ProductCost pcost = new ProductCost();
            pcost.ProductID = p.ProductID;
            pcost.StartDate = Convert.ToDateTime(txtStartDate.Text);
            pcost.Cost = p.Cost;

            Result = pcost.Save(p.ProductID);

            Response.Redirect("AddEditProduct.aspx?ProductID=" + p.ProductID.ToString());

        }
    }

    protected void btnClone_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int productID = 0; //create new Product
            //ProductDetails p = new ProductDetails();
            Product p = new Product();
            p.ProductID = productID; 
            p.Sku = ValidateUtil.CleanUpString(txtSku.Text) + "(clone)";
            p.Name = ValidateUtil.CleanUpString(txtName.Text);
            if (chkIsSubscription.Checked == true) { p.IsSubscription = true; }
            else { p.IsSubscription = false; }

            p.SubscriptionPlan = ValidateUtil.CleanUpString(txtSubscriptionPlan.Text);
            p.ShortDescription = ValidateUtil.CleanUpString(txtShortDescription.Text);
            p.Description = ValidateUtil.CleanUpString(txtDescription.Text);
            p.BundleString = ValidateUtil.CleanUpString(txtBundleString.Text);
            p.CartImage = ValidateUtil.CleanUpString(txtCartImage.Text);
            p.ThumbnailImage = ValidateUtil.CleanUpString(txtThumbnailImage.Text);
            p.IconImage = ValidateUtil.CleanUpString(txtIconImage.Text);
            p.DetailsImage = ValidateUtil.CleanUpString(txtDetailsImage.Text);
            p.ZoomImage = ValidateUtil.CleanUpString(txtZoomImage.Text);
            p.OriginalImage = ValidateUtil.CleanUpString(txtOriginalImage.Text);
            p.Weight = Convert.ToDecimal(txtWeight.Text);
            p.Price = Convert.ToDecimal(txtPrice.Text);
            p.SalePrice = Convert.ToDecimal(txtSalePrice.Text);
            p.PerUnitShipping = Convert.ToDecimal(txtPerUnitShipping.Text);
            p.Stock = Convert.ToInt32(txtStock.Text);
            p.Sort = Convert.ToInt32(txtSort.Text);

            if (chkIsBundle.Checked) { p.IsBundle = true; }
            else { p.IsBundle = false; }

            if (chkTaxable.Checked) { p.Taxable = true; }
            else { p.Taxable = false; }

            if (chkEnabled.Checked) { p.Enabled = true; }
            else { p.Enabled = false; }

            if (chkFMSystem.Checked) { p.IsFMSystem = true; }
            else { p.IsFMSystem = false; }

            if (chkIsShippable.Checked) { p.IsShippable = true; }
            else { p.IsShippable = false; }

            if (chkCustomerServiceOnly.Checked) { p.CSOnly = true; }
            else { p.CSOnly = false; }

            p.Cost = Convert.ToDecimal(txtCost.Text);

            int Result = p.Save(productID);
            if ((productID == 0) && (Result > 0)) { p.ProductID = Result; }

            ProductCost pcost = new ProductCost();
            pcost.ProductID = p.ProductID;
            pcost.StartDate = Convert.ToDateTime(txtStartDate.Text);
            pcost.Cost = p.Cost;

            Result = pcost.Save(p.ProductID);

            Response.Redirect("AddEditProduct.aspx?ProductID=" + p.ProductID.ToString());

        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManageProducts.aspx");
    }


    private string GetProductCostListHtml(int ProductID)
    {
        string html = "";
        if (ProductID != 0)
        {
            List<ProductCost> plist = new List<ProductCost>();
            plist = ProductCost.GetProductCostList(ProductID);

            html = "<table border='1px solid gray'>";
            html += "<tr>";
            html += "<td class = 'custservice' width='200px'><b>StartDate</b></td>";
            html += "<td class = 'custservice' width='100px'><b>Cost</b></td>";
            html += "</tr>";

            string startdateStr = "";
            string costStr = "";

            foreach (ProductCost p in plist)
            {
                if (p.StartDate != null) { startdateStr = p.StartDate.ToString(); }
                costStr = p.Cost.ToString();
                html += "<tr>";
                html += "<td class = 'custservice' >" + startdateStr + "</td>";
                html += "<td class = 'custservice' >" + costStr + "</td>";
                html += "</tr>";
            }
            html += "</table>";
        }
        else
        { html = "No Cost History Availble"; }

        return html;
    }

}