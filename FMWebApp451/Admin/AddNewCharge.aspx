<%@ Page Language="C#" Theme="Admin" MasterPageFile="~/Admin/MasterPage3_Blank.master" ValidateRequest="false" AutoEventWireup="true" Inherits="AddNewCharge" Title="Add New Credit Card Charge" Codebehind="AddNewCharge.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

   <div class="sectiontitle">Add New Credit Card Charge To Order</div>
   <p>
       This function is to be used as a last resort, when you have to charge the customer 
       for an item that is not available on the product list and it has to be associated with a specific order.
   </p>
    <ul>
        <li>There is a $150 maximum charge limit</li>
        <li>Items charged this way will not be available on inventory reports</li>
    </ul>

    <h4 style="color:red"><asp:Label ID="CartLabel" runat="server" Text=""></asp:Label></h4>

    <table width="90%x">
        <tr>
            <td class = "custservice" width="35%" align="right">
                OrderID: &nbsp<asp:Label id="lblOrderID" runat="server"></asp:Label>
                <br /><br />
            </td>
 			<td width="400px"> &nbsp </td>			
			<td width="200px"> &nbsp </td>			
       </tr>
		<tr>
            <td class="custservice" align="right">Name on Card:&nbsp;</td>
            <td class="cschkoutbody" align="left">
	            <asp:TextBox id="Payment_CardName" runat="server"></asp:TextBox>
                <br />
            </td>
			<td> &nbsp </td>			
	    </tr>
		<tr >
			<td class="custservice" align="right">Credit Card No.:&nbsp;</td>
			<td class="custservice" align="left" >
				<asp:TextBox id="Payment_CardNumber" runat="server"></asp:TextBox>
				<ASP:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="Payment_CardNumber"
					Display="Dynamic" ErrorMessage="*"></ASP:RequiredFieldValidator>
			    <br />
			    Please Enter Only Numbers
			    <br />
			    Do Not Use Any Spaces or "-"
			    <br /><br />
			</td>
			<td> &nbsp </td>
	    </tr>
	    <tr>
			<td class="custservice" align="right">Expires (mm/yy):&nbsp;</td>
			<td class="custservice" align="left">
                <div class="groupHTML">
				    <asp:TextBox id="Payment_CcMonth" runat="server"  MaxLength="2"  Width="20px"></asp:TextBox>
				    <ASP:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ControlToValidate="Payment_CcMonth"
					    Display="Dynamic" ErrorMessage="*"></ASP:RequiredFieldValidator>
				    <asp:Label id="Separator" runat="server" Text="&nbsp;/&nbsp; "></asp:Label>
				    <asp:TextBox id="Payment_CcYear" runat="server" MaxLength="4"  Width="40px"></asp:TextBox>
				    <ASP:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ControlToValidate="Payment_CcYear" Display="Dynamic"
					    ErrorMessage="*"></ASP:RequiredFieldValidator>
                    <div class="simpleClear"></div>
                </div>
                <br />         
			</td>
			<td> &nbsp </td>			
		</tr>
	    <tr>
	        <td class="custservice" align="right">Code:&nbsp;</td>
            <td class="custservice" align="left">
                <div class="groupHTML">
	                <asp:TextBox id="Payment_CardCode" runat="server" Font-Size="12px" MaxLength="4"  Width="30"></asp:TextBox>
                    <asp:Label ID="lbl1" runat="server">&nbsp</asp:Label> 
                    <a href="javascript:CreateWin()">?</a>
                </div>
                <br />         
            </td>
			<td> &nbsp </td>			
		</tr>
        <tr>
            <td class = "custservice" align="right">Charge Amount:&nbsp;</td>
            <td colspan="2" class = "custservice" align="left">
                <asp:TextBox ID="txtAmount" Text="" width="75px" runat="server"></asp:TextBox>
		        <asp:requiredfieldvalidator id="valReqdAmount" Runat="server" Display="Dynamic" ErrorMessage="Please enter a Charge amount."
				        ControlToValidate="txtAmount"></asp:requiredfieldvalidator>
                <asp:CompareValidator ID="valAmount" runat="server" Type="Double" ControlToValidate="txtAmount" 
                    ErrorMessage="<br />The format of the Charge value is not valid (use x.yz)." ToolTip="The format of the Charge value is not valid (use x.yz)."
                    Display="Dynamic" Operator="DataTypeCheck" ></asp:CompareValidator>  
                <br />         
            </td>
         </tr>
         <tr>
            <td class = "custservice" align="right">Description:&nbsp;</td>
            <td colspan="2" class = "custservice" align="left">
                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Text="" Width="90%" Rows="5" MaxLength="2000"></asp:TextBox>
		        <asp:requiredfieldvalidator id="Requiredfieldvalidator4" Runat="server" Display="Dynamic" ErrorMessage="Please enter a description of the new charge."
				        ControlToValidate="txtDescription"></asp:requiredfieldvalidator>
                <br />         
                <asp:Label ID="lblEnterInfo" runat="server" Text="Enter your information, then click 'Submit' only once."></asp:Label><br /><br />
            </td>
         </tr>
         <tr>
            <td align="right">
                <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" CausesValidation="false" />
            </td>
            <td align="right">
                <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" Text="Submit Charge" />
            </td>
			<td> &nbsp </td>			
        </tr>
    </table>
    
</asp:Content>

