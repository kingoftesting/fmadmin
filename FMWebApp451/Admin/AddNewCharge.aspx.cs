using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;

public partial class AddNewCharge : AdminBasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            CanUserProceed(new string[2] { "ADMIN", "CUSTOMERSERVICE" });

            int OrderID = Convert.ToInt32(Request["OrderID"]);

            Cart tempcart = new Cart();
            tempcart = tempcart.GetCartFromOrder(OrderID);
            if (Config.CartSetting == "Test")
            {

                Payment_CardNumber.Text = "4111111111111111";
                Payment_CcMonth.Text = "12";
                Payment_CcYear.Text = "10";
                Payment_CardCode.Text = "987";
            }
            //Payment_CardName.Text = tempcart.SiteCustomer.FirstName + " " + tempcart.SiteCustomer.LastName;
            lblOrderID.Text = tempcart.SiteOrder.OrderID.ToString();

            if (Request["ResultMsg"] != null)
            {
                CartLabel.Text = Request["ResultMsg"].ToString();
            }

        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int OrderID = Convert.ToInt32(Request["OrderID"]);

            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(OrderID);

            cart.SiteOrder.Total = Convert.ToDecimal(txtAmount.Text);
            cart.Total = cart.SiteOrder.Total;
            if (cart.SiteOrder.Total <= 150)
            {
                cart.Tran.CardNo = Payment_CardNumber.Text;
                cart.Tran.CardHolder = ValidateUtil.CleanUpString(Payment_CardName.Text);
                cart.Tran.ExpDate = Payment_CcMonth.Text + Payment_CcYear.Text;
                cart.Tran.CvsCode = Payment_CardCode.Text;
                if (cart.Charge("SA") == 0)
                {
                    Certnet.Cart tempcart = new Certnet.Cart();
                    tempcart.Load(Session["cart"]); //get name of CS rep requesting address change
                    string username = tempcart.SiteCustomer.LastName + "," + tempcart.SiteCustomer.FirstName;
                    string ordernote = "New Charge transaction:";
                    ordernote += " NewID=" + cart.Tran.TransactionID + " Amt=" + cart.Tran.TransactionAmount.ToString();
                    ordernote += " Description: " + ValidateUtil.CleanUpString(txtDescription.Text);
                    OrderNote.Insertordernote(OrderID, username, ordernote);
                    //Response.Redirect("ReviewOrder.aspx?OrderID=" + cart.SiteOrder.OrderID.ToString());
                    Response.Redirect("/orders/details/" + cart.SiteOrder.OrderID.ToString());
                }
                else
                {
                    Response.Redirect("AddNewCharge.aspx?OrderID=" + cart.SiteOrder.OrderID.ToString() + "&" + "ResultMsg= Error: " + cart.Tran.ResultMsg);
                }
            }
            else
            { Response.Redirect("AddNewCharge.aspx?OrderID=" + cart.SiteOrder.OrderID.ToString() + "&" + "ResultMsg= Error: Maximum New Charge Can Not Exceed $150"); }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        int OrderID = Convert.ToInt32(Request["OrderID"]);
        Response.Redirect("ReviewOrder.aspx?OrderID=" + OrderID.ToString());
    }
}
