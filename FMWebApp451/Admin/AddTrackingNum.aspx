<%@ Page Language="C#" Theme="Admin" MasterPageFile="~/Admin/MasterPage3_Blank.master" AutoEventWireup="true" Inherits="AddTrackingNum" Title="Add Tracking Num" Codebehind="AddTrackingNum.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">


   <div class="sectiontitle">Add New Shipping Track Number Info</div>
   <p></p>

    <table width="90%">
      <tr>
            <td class="cslabel" style="width:10%">Weight:&nbsp;</td>
            <td class="csrightlabel" style="width:20%">
                <asp:textbox id="txtweight" runat="server"></asp:textbox>
                <asp:requiredfieldvalidator id="Requiredfieldvalidator7" ControlToValidate="txtweight" ErrorMessage="Please enter the weight of the package"
                  Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
            <asp:CompareValidator ID="valWeight" runat="server" Type="Double" ControlToValidate="txtweight" 
                ErrorMessage="<br />The format of the Weight value is not valid (use x.yz)." ToolTip="The format of the Weight value is not valid (use x.yz)."
                Display="Dynamic" Operator="DataTypeCheck" ></asp:CompareValidator>            
            </td>
      </tr>
     <tr><td colspan="2">&nbsp;</td></tr>
     <tr>
            <td class="cslabel">UPS Charge:&nbsp;</td>
            <td class="csrightlabel">
                <asp:textbox id="txtUps" runat="server"></asp:textbox>
                <asp:requiredfieldvalidator id="Requiredfieldvalidator1" ControlToValidate="txtUps" ErrorMessage="Please enter the UPS charge for the package"
                  Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
                <asp:CompareValidator ID="valUPS" runat="server" Type="Double" ControlToValidate="txtUps" 
                    ErrorMessage="<br />The format of the UPS Charge value is not valid (use x.yz)." ToolTip="The format of the UPS Charge value is not valid (use x.yz)."
                    Display="Dynamic" Operator="DataTypeCheck" ></asp:CompareValidator>            
            </td>
      </tr>
     <tr><td colspan="2">&nbsp;</td></tr>
      <tr>
            <td class="cslabel">Tracking Number:&nbsp;</td>
            <td class="csrightlabel">
                <asp:textbox id="txtTrackNum" runat="server"></asp:textbox>
                <asp:requiredfieldvalidator id="Requiredfieldvalidator8" ControlToValidate="txtTrackNum" ErrorMessage="Please enter the Tracking Number for the package"
                    Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
            </td>
       </tr>
     <tr><td colspan="2">&nbsp;</td></tr>
       <tr> 
           <td>&nbsp;</td>        
           <td>
               <asp:Button ID="btnAddTrack" onclick="btnTrackNum_Click" runat="server" Text="Submit" />
           </td>
       </tr>
    </table>

</asp:Content>

