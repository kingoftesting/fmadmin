using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;

public partial class AddTrackingNum : AdminBasePage
{

    Cart cart = new Cart();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        { CanUserProceed(new string[2] { "ADMIN", "CUSTOMERSERVICE" }); }
    }


    protected void btnTrackNum_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (Request["OrderID"] != null)
            {
                int FMorderID = Convert.ToInt32(Request["OrderID"]);
                Cart orderCart = new Cart();
                orderCart = orderCart.GetCartFromOrder(FMorderID);


                Certnet.Cart tempcart = new Certnet.Cart();
                tempcart.Load(Session["cart"]); //get name of CS rep requesting address change
                string username = tempcart.SiteCustomer.LastName + "," + tempcart.SiteCustomer.FirstName;

                double weight = Convert.ToDouble(txtweight.Text);
                double upscharge = Convert.ToDouble(txtUps.Text);
                string trackingnumber = ValidateUtil.CleanUpString(txtTrackNum.Text);

                Utilities.FMHelpers.AddTrackNum(FMorderID, weight, upscharge, trackingnumber, username);
                //if (orderCart.SiteOrder.SourceID == -2) { Helpers.AddHDITrackNum(orderCart.SiteOrder.SSOrderID, trackingnumber); }

                OrderNote.Insertordernote(FMorderID, username, "Added New Tracking Number");

                string trkNumURL = "";
                string carrierType = trackingnumber.Substring(0, 1);
                if (carrierType == "1") //if not "1" then assume USPS   
                { trkNumURL = "http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=" + trackingnumber; }
                else
                { trkNumURL = "https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=" + trackingnumber; }

                trkNumURL = String.Format("<a href=\"{0}\">{1}</a>", trkNumURL, trackingnumber);

                //now check to see if we need to fulfill an order at Shopify
                //SSOrderID > 0
                //SourceID <> -2 (HDI)
                if ( (orderCart.SiteOrder.SSOrderID != 0) && 
                    (orderCart.SiteOrder.OrderDate > Convert.ToDateTime("1/1/2015")) && 
                    (orderCart.SiteOrder.SourceID != -2) )
                {
                    ShopifyAgent.ShopifyFulfillment.SendShopifyTracking(orderCart.SiteOrder.SSOrderID, trackingnumber, trkNumURL, orderCart.SiteOrder.CompanyID);
                }

                Response.Redirect("ReviewOrder.aspx?OrderID=" + FMorderID.ToString());
            }
        }
    }
}
