using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Data.SqlClient;

public partial class Admin_BulkCopyXLtoDB : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //step 1
        //open XL file, read contents

        // Connection String to Excel Workbook
        string sExcelFileName = "POSalesFrom2005.xls";
        string excelConnectionString = @"
        Provider=Microsoft.Jet.OLEDB.4.0;Data Source=
        ";

        excelConnectionString += Server.MapPath(sExcelFileName);
        excelConnectionString += @"
        ;Extended Properties=""Excel 8.0;HDR=YES;""
        ";

        OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
        excelConnection.Open(); // This code will open excel file.

        string strSQL = @"
        Select Date, Customer, DevRevenue, DevUnits, AccyRevenue
        FROM [FMPOSales16Nov11$]
        ";

        OleDbCommand dbCommand = new OleDbCommand(strSQL, excelConnection);
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter(dbCommand);

        // create dataset
        DataSet dSet = new DataSet();
        //fill dSet with XL data
        dataAdapter.Fill(dSet);

        //step 2
        //connect to server; copy XL data

        DataTable dt = dSet.Tables[0];
        int ColumnCount = dt.Columns.Count;
        string msg = "Copy XL Succeeded!";
        foreach (DataRow r in dt.Rows)
        {
            FMPOSales obj = new FMPOSales();
            obj.ID = 0;
            obj.Date = Convert.ToDateTime(r["Date"].ToString());
            obj.Customer = r["Customer"].ToString();
            obj.DevRevenue = Convert.ToDecimal(r["DevRevenue"].ToString());
            obj.DevUnits = Convert.ToInt32(r["DevUnits"].ToString());
            obj.AccyRevenue = Convert.ToDecimal(r["AccyRevenue"].ToString());

            int ret = FMPOSales.InsertFMPOSales(obj.Date, obj.Customer, obj.DevRevenue, obj.DevUnits, obj.AccyRevenue);
            if (ret <= 0)
            {
            msg = "Copy XL Failed! ";
            msg += " Date:" + obj.Date.ToString() + ", ";
            msg += " Customer:" + obj.Customer.ToString() + ", ";
            msg += " DevRevenue:" + obj.DevRevenue.ToString() + ", ";
            msg += " DevUnits:" + obj.DevUnits.ToString() + ", ";
            msg += " AccyRevenue:" + obj.AccyRevenue.ToString() + ", ";
            break;
            }
        }

        // dispose used objects
        dSet.Dispose();
        dataAdapter.Dispose();
        dbCommand.Dispose();

        excelConnection.Close();
        excelConnection.Dispose();

        Label2.Text = msg;
        Label2.Visible = true;
    }

}
