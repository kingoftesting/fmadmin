<%@ Page Language="C#" Theme="Admin" MasterPageFile="~/Admin/MasterPage3_Blank.master" validateRequest="false" AutoEventWireup="true" Inherits="Admin_CEREntry" Title="CER Entry" Codebehind="CEREntry.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">



    <div class="sectiontitle">Create CER Entry</div>
    <p></p>

    <asp:label ID="Label15" Runat="server" Text="CustomerID:&nbsp;"></asp:label>
    <asp:label ID="lblCERCustID" Runat="server" ></asp:label>
    <div style="clear:both"></div>

    <asp:label ID="lblFirstName" Runat="server" Text="First&nbsp;Name:&nbsp;"></asp:label>
    <asp:label ID="txtFirstName" Runat="server" ></asp:label>
    <div style="clear:both"></div>

    <asp:label ID="lblLastName" Runat="server" Text="Last Name:&nbsp;"></asp:label>
    <asp:label ID="txtLastName" Runat="server" ></asp:label>
    <div style="clear:both"></div>

    <asp:label ID="lblStreet" Runat="server" Text="Street1:&nbsp;"></asp:label>
    <asp:label ID="txtStreet" Runat="server" ></asp:label>
    <div style="clear:both"></div>

    <asp:literal ID="lblStreet2" Runat="server" Text="Street2:&nbsp;"></asp:literal>
    <asp:label ID="txtStreet2" Runat="server" ></asp:label>
    <div style="clear:both"></div>

    <asp:label ID="lblCity" Runat="server" Text="City:&nbsp;"></asp:label>
    <asp:label ID="txtCity" Runat="server" ></asp:label>
    <div style="clear:both"></div>

    <asp:label ID="lblState" Runat="server" Text="State/Province:&nbsp;" ></asp:label>
    <asp:label ID="txtStateBill" Runat="server" ></asp:label>
    <div style="clear:both"></div>

    <asp:label id="Label8" Runat="server" Text="Zip:&nbsp;" ></asp:label>
    <asp:label id="BillAddress_ZipPostal" runat="server" ></asp:label>
    <div style="clear:both"></div>

    <asp:label ID="Label13" Runat="server" Text="Country:&nbsp;" ></asp:label>
    <asp:label ID="txtCountry" runat="server" ></asp:label>
    <div style="clear:both"></div>

    <asp:label ID="Literal11" Runat="server" Text="Phone:&nbsp;"></asp:label>
    <asp:label ID="txtPhone" Runat="server" ></asp:label>
    <div style="clear:both"></div>

    <asp:label ID="Literal5" Runat="server" Text="Email:&nbsp;"></asp:label>
    <asp:label ID="txtEmail" Runat="server" ></asp:label>
    <div style="clear:both"></div>

    <hr />

    <div class="groupHTML">
    <asp:Label ID="LabelSource" runat="Server" Text="Point of Purchase:&nbsp;"/>
        <asp:DropDownList ID="selPointOfPurchase" runat="server" >
        </asp:DropDownList>
    </div>

    <p>&nbsp;</p>

    <strong>Event Information</strong>
    <div style="clear:both">&nbsp;</div>
    <div class="groupHTML">
        <asp:Label ID="Label1" runat="Server" Text="Serial No:&nbsp;"/>
        <asp:TextBox ID="txtSerialNumber" runat="server" />
    </div>

    <div style="clear:both">&nbsp;</div>
    <div class="groupHTML">
        <asp:Label ID="Label2" runat="Server" Text="Order No:&nbsp;"/>
        <asp:TextBox ID="txtOrderNumber" runat="server" />
        <asp:Label ID="lblOrderNum" Visible="false" ForeColor="red" runat="Server" Text=" * Order Number must be numeric"/>
    </div>

    <div style="clear:both">&nbsp;</div>

    <div class="alpha eight columns">

    <asp:Panel ID="pnlEventAdd" DefaultButton="ButtonAdd" Visible="true" runat="server">

    <div class="groupHTML">
        <asp:Label ID="Label3" runat="Server" Text="Event Description:&nbsp;"/>
        <div class="simpleClear"></div>
        <asp:TextBox ID="txtEventDescription" runat="server" Width="100%" Rows="15" TextMode="MultiLine" />
        <asp:Label ID="lblEventDescription" Visible="false" ForeColor="red" runat="Server" Text=" * Must include an EventDescription"/>
        <asp:Label ID="lblEDLength" Visible="false" ForeColor="red" runat="Server" Text=" * EventDescription is too long - please shorten"/>
    </div>

 
        <div style="clear:both">&nbsp;</div>
        <div class="groupHTML">
            <asp:Label ID="Label4" runat="Server" Text="Reason:&nbsp;"/>
            <asp:TextBox ID="txtActionExplaination" runat="server" width="50%" Rows="1" /><asp:Label ID="lblExplanation" Visible="false" ForeColor="red" runat="Server" Text=" * Must include a Reason for Update"/>
            <asp:Label ID="Label14" runat="Server" Text="&nbsp;&nbsp;"/>
            <asp:Button ID="ButtonAdd" runat="server" Text="Add Record" OnClick="ButtonAdd_Click" OnClientClick="aspnetForm.target ='_self'" />
        </div>
    </asp:Panel>

    <div style="clear:both;"></div>

    </div>

    <div class="omega eight columns">

    <asp:Panel ID="pnlCERSrch" DefaultButton="btnCerKeyword" Visible="true" runat="server">

        <div style="clear:both">&nbsp;</div>
        <h4>
            CER Event Description Search... 
        </h4>

        <div class="groupHTML">
            <asp:Label ID="Label6" runat="server" Text="Find the first&nbsp;&nbsp;"></asp:Label>
            <asp:TextBox ID="txtResultNum" Text="10" Width="30px" runat="server"></asp:TextBox>
            <asp:Label ID="Label7" runat="server" Text="&nbsp;&nbsp;results&nbsp;&nbsp;&nbsp;&nbsp;"></asp:Label>
            <div style="clear:both">&nbsp;</div>

            <asp:Label ID="lblCerKeyword" runat="server" Text="Enter CER Keyword:&nbsp;"></asp:Label>
            <asp:TextBox ID="txtCerKeyword" runat="server"></asp:TextBox>
            <asp:Label ID="Label5" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
            <asp:Button ID="btnCerKeyword" OnClick="btnCerKeyword_Click" OnClientClick="aspnetForm.target ='_blank'" runat="server" Text="Submit" />

            <div class="simpleClear"></div>
            <asp:HyperLink ID="HyperLink1" NavigateUrl="GetCerED.aspx?CerYear=2011&CerIdentifier=2214" Target="_blank" runat="server">FWs for FML?</asp:HyperLink>
            <asp:Label ID="Label9" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
            <asp:HyperLink ID="HyperLink2" NavigateUrl="GetCerED.aspx?CerYear=2012&CerIdentifier=2414" Target="_blank" runat="server">Return RMA</asp:HyperLink>
            <asp:Label ID="Label17" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
            <asp:HyperLink ID="HyperLink3" NavigateUrl="GetCerED.aspx?CerYear=2012&CerIdentifier=2116" Target="_blank" runat="server">Warranty RMA</asp:HyperLink>
        </div>

    </asp:Panel>

    <div style="clear:both;"></div>

    <hr />

    <asp:Panel ID="pnlKYWDSrch" DefaultButton="btnFaqKeyword" Visible="true" runat="server">

        <h4>
        FAQ Search... 
        </h4>

        <div class="groupHTML">
            <asp:Label ID="lblFaqKeyword" runat="server" Text="Enter FAQ Keyword:&nbsp;"></asp:Label>
            <asp:TextBox ID="txtFaqKeyword" runat="server"></asp:TextBox>
            <asp:Label ID="Label16" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
            <asp:Button ID="btnFaqKeyword" OnClick="btnFaqKeyword_Click" OnClientClick="aspnetForm.target ='_blank'" runat="server" Text="Submit" />

            <div class="simpleClear"></div>
            <asp:HyperLink ID="lnkNeck" NavigateUrl="GetFAQ.aspx?FaqID=37" Target="_blank" runat="server">NeckFAQ</asp:HyperLink>
            <asp:Label ID="Label10" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
            <asp:HyperLink ID="lnkBeeps" NavigateUrl="GetFAQ.aspx?FaqID=41" Target="_blank" runat="server">Just keeps beeping</asp:HyperLink>
            <asp:Label ID="Label11" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
            <asp:HyperLink ID="lnkIntl" NavigateUrl="GetFAQ.aspx?FaqID=59" Target="_blank" runat="server">Shipping Int'l</asp:HyperLink>
            <asp:Label ID="Label12" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
            <asp:HyperLink ID="lnkAltSerum" NavigateUrl="GetFAQ.aspx?FaqID=60" Target="_blank" runat="server">Aloe vs. Serum</asp:HyperLink>
        </div>
    </asp:Panel>    
    
    <div class="simpleClear"></div>

        <hr />
    </div>

</asp:Content>


