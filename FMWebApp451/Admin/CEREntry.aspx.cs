using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;
using adminCER;
using FM2015.Helpers;

public partial class Admin_CEREntry : AdminBasePage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Define the name and type of the client scripts on the page.
        String csname1 = "fixform"; //so that only the button controls spec'd generate new windows
        Type cstype = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;

        // Check to see if the startup script is already registered.
        if (!cs.IsStartupScriptRegistered(cstype, csname1))
        {
            StringBuilder cstext1 = new StringBuilder();
            //cstext1.Append("<script type=text/javascript> alert('Hello World!') </");
            //cstext1.Append("script>");

            cstext1.Append("<script type='text/javascript'>");      
            cstext1.Append("function fixform() { ");           
            cstext1.Append("if (opener.document.getElementById('aspnetForm').target != '_blank') return; ");            
            cstext1.Append("opener.document.getElementById('aspnetForm').target = ''; ");            
            cstext1.Append("opener.document.getElementById('aspnetForm').action = opener.location.href; ");
            cstext1.Append("} </script>");

            cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
        }

        Certnet.Cart cart = new Certnet.Cart();
        cart.Load(Session["cart"]);
        int customerID = cart.SiteCustomer.CustomerID;
        try
        {
            if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "CUSTOMERSERVICE")))
            { }
            else
            {
                throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
            }
        }
        catch (SecurityException ex)
        {
            string msg = "Error in CER Entry" + Environment.NewLine;
            msg += "User did not have permission to access page" + Environment.NewLine;
            msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
            dbErrorLogging.LogError(msg, ex);
            Response.Redirect("Login.aspx");
        }

        if (!Page.IsPostBack)
        {
            string customerid = Request["FMCustomerID"];
            Customer c = new Customer(Convert.ToInt32(customerid));
            Address a = Address.LoadAddress(Convert.ToInt32(customerid), 1);
            lblCERCustID.Text = c.CustomerID.ToString();
            txtFirstName.Text = c.FirstName;
            txtLastName.Text = c.LastName;
            txtStreet.Text = a.Street;
            txtStreet2.Text = a.Street2;
            txtCity.Text = a.City;
            txtStateBill.Text = a.State;
            BillAddress_ZipPostal.Text = a.Zip;
            txtEmail.Text = c.Email;
            txtPhone.Text = c.Phone;
            txtCountry.Text = DBUtil.GetCountry(a.Country);

            //initialize Point of Purchase
            List<adminCER.CER_PointOfPurchase> popList = new List<CER_PointOfPurchase>();
            popList = adminCER.CER_PointOfPurchase.GetCER_PointOfPurchaseList();
            selPointOfPurchase.DataSource = popList;
            selPointOfPurchase.DataTextField = "popName";
            selPointOfPurchase.DataValueField = "popValue";
            selPointOfPurchase.DataBind();

            if (String.IsNullOrEmpty(Request["OrderID"])) 
            { 
                txtOrderNumber.Text = "";
                selPointOfPurchase.SelectedValue = "00";
            }
            else 
            {
                txtOrderNumber.Text = Request["OrderID"].ToString();
                Order o = new Order(Convert.ToInt32(Request["OrderID"]));
                if (o.SourceID == -2) //HDI Order
                { ListItem itemHDI = selPointOfPurchase.Items.FindByValue("08"); if (itemHDI != null) itemHDI.Selected = true; }
                else { ListItem itemFM = selPointOfPurchase.Items.FindByValue("01"); if (itemFM != null) itemFM.Selected = true; }
            }

            if (String.IsNullOrEmpty(Request["SerialNumber"])) { txtSerialNumber.Text = ""; }
            else { txtSerialNumber.Text = Request["SerialNumber"].ToString(); } 

        }
    }

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        
        //Page.Validate();
        if (((!String.IsNullOrEmpty(txtOrderNumber.Text)) && (!ValidateUtil.IsNumeric(txtOrderNumber.Text))) 
            || (String.IsNullOrEmpty(txtActionExplaination.Text))
            || (txtEventDescription.Text.Length > 4000)
            || (String.IsNullOrEmpty(txtEventDescription.Text)))
        {
            if ((!String.IsNullOrEmpty(txtOrderNumber.Text)) && (!ValidateUtil.IsNumeric(txtOrderNumber.Text))) { lblOrderNum.Visible = true; }
            if (String.IsNullOrEmpty(txtActionExplaination.Text)) {lblExplanation.Visible = true;}
            if (String.IsNullOrEmpty(txtEventDescription.Text)) { lblExplanation.Visible = true; }
            if (txtEventDescription.Text.Length > 4000) { lblEDLength.Visible = true; }
       }
        else
        {
            adminCER.CER cer = new adminCER.CER(); //add a new entry into the CER database
            Customer c = new Customer(Convert.ToInt32(Request["FMCustomerID"]));
            Address a = Address.LoadAddress(Convert.ToInt32(Request["FMCustomerID"]), 1);

            cer.CustomerID = c.CustomerID.ToString();
            cer.DateReportReceived = DateTime.Now;
            cer.PointOfPurchase = selPointOfPurchase.SelectedValue;
            cer.FirstName = CERCleanUpString(c.FirstName);
            cer.LastName = CERCleanUpString(c.LastName);
            cer.Email = CERCleanUpString(c.Email);
            cer.Phone = CERCleanUpString(c.Phone);
            cer.Street = CERCleanUpString(a.Street);
            cer.City = CERCleanUpString(a.City);
            cer.State = CERCleanUpString(a.State);
            cer.Zip = CERCleanUpString(a.Zip);
            cer.Country = CERCleanUpString(a.Country);
            //cer.ProductNumber = selProductNumber.SelectedValue;
            cer.SerialNumber = CERCleanUpString(txtSerialNumber.Text);
            cer.OrderNumber = txtOrderNumber.Text;
            cer.EventDescription = ValidateUtil.CleanUpString(txtEventDescription.Text);
            cer.ActionExplaination = ValidateUtil.CleanUpString(txtActionExplaination.Text);
            cer.StatusCs = "Opened";
            cer.StatusQa = "Unopened";
            cer.StatusMed = "Does Not Apply";
            cer.MedicalCondition = "n/a";
            cer.CERType = "NC";

            Certnet.Cart tempcart = new Certnet.Cart();
            tempcart.Load(Session["cart"]); //get name of CS rep requesting address change
            string username = tempcart.SiteCustomer.LastName + "," + tempcart.SiteCustomer.FirstName;
            cer.ReceivedBy = username; // Config.TheUser();
            cer.InsertBy = username; // Config.TheUser();

            if (Config.RedirectMode == "Live") //don't add a new CER entry if in Test mode
            { int result = cer.Add(); }

            Response.Redirect("Default.aspx");
        }

    }

    protected void btnCerKeyword_Click(object sender, EventArgs e)
    {
        string keyword = ValidateUtil.CleanUpString(txtCerKeyword.Text);
        string resultNum = ValidateUtil.CleanUpString(txtResultNum.Text);
        if (!ValidateUtil.IsNumeric(resultNum)) { resultNum = "10"; }
        Response.Redirect("../admin/GetCerED.aspx?Keyword=" + keyword + "&resultNum=" + resultNum);
    }

    protected void btnFaqKeyword_Click(object sender, EventArgs e)
    {
        string keyword = ValidateUtil.CleanUpString(txtFaqKeyword.Text);
        Response.Redirect("GetFaq.aspx?Keyword=" + keyword);
    }

    protected string CERCleanUpString(string str)
    {
        string cleanStr = ValidateUtil.CleanUpString(str);
        if (cleanStr.Length >= 50) { cleanStr = cleanStr.Substring(0, 50); }

        return cleanStr;
    }


}
