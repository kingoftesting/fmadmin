<%@ Page Language="C#" Theme="Admin" MasterPageFile="MasterPage3_Blank.master" AutoEventWireup="true" Inherits="FM.Admin.CheckOut" Title="Check Out" Codebehind="CheckOut.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <p><strong>YOUR INFO -&gt; <span style="color: #aaaaaa">SHIPPING OPTION -&gt; PAYMENT TYPE</span></strong></p>
    <p>
        <strong>STEP 1 of 3 - YOUR BILLING &amp; SHIPPING INFORMATION</strong><br />
		<i>Please fill in all fields below</i>
    </p>
    <p><asp:label id="dotnetCARTmessage" runat="server" Font-Bold="true" ForeColor="red"></asp:label></p>

    <p><strong>Billing Address:</strong></p>

    <div class="formHTML">
        <asp:Label ID="Label1" runat="server">First Name:&nbsp;</asp:Label>
		<asp:textbox id="Customer_FirstName" runat="server"></asp:textbox>
            <asp:requiredfieldvalidator id="Requiredfieldvalidator1" ControlToValidate="Customer_FirstName" ErrorMessage="* First name is required"
                Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label2" runat="server">Last Name:&nbsp;</asp:Label>
        <asp:textbox id="Customer_LastName" runat="server"></asp:textbox>
            <asp:requiredfieldvalidator id="Requiredfieldvalidator2" ControlToValidate="Customer_LastName" ErrorMessage="* Last name is required"
                Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label6" runat="server">Street:&nbsp;</asp:Label>
        <asp:textbox id="BillAddress_Street" runat="server"></asp:textbox>
            <asp:requiredfieldvalidator id="Requiredfieldvalidator7" ControlToValidate="BillAddress_Street" ErrorMessage="* Street name & number is required"
	            Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label5" runat="server">Street2:&nbsp;</asp:Label>
	    <asp:textbox id="BillAddress_Street2" runat="server"></asp:textbox>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label7" runat="server">City:&nbsp;</asp:Label>
	    <asp:textbox id="BillAddress_City" runat="server"></asp:textbox>
            <asp:requiredfieldvalidator id="Requiredfieldvalidator8" ControlToValidate="BillAddress_City" ErrorMessage="* City name is required"
			    Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label8" runat="server">State:&nbsp;</asp:Label>
		<asp:textbox id="txtStateBill" Visible="False" Runat="server" MaxLength="255"></asp:textbox>
        <asp:dropdownlist id="selStateBill" runat="server" Visible="True"></asp:dropdownlist>
            <asp:requiredfieldvalidator id="rqdStateBill" Visible="False" ControlToValidate="txtStateBill" ErrorMessage="* State or Province name is required"
		        Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label9" runat="server">Zip:&nbsp;</asp:Label>
	    <asp:textbox id="BillAddress_ZipPostal" runat="server" Width="100px"></asp:textbox>
            <asp:requiredfieldvalidator id="Requiredfieldvalidator9" ControlToValidate="BillAddress_ZipPostal" ErrorMessage="* Postal code is required"
		        Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label10" runat="server">Country:&nbsp;</asp:Label>
		<asp:dropdownlist id="selCountryBill" runat="server" OnSelectedIndexChanged="selCountryBill_SelectedIndexChanged"
		        AutoPostBack="true"></asp:dropdownlist>
	</div>

    <div class="simpleClear"></div>
    <p>
		<asp:checkbox id="SeparateBillingAddress" runat="server" AutoPostBack="true" OnCheckedChanged="Checked_SeparateBillingAddress"></asp:checkbox>
        <asp:Label ID="Label12" runat="server">Check here if  you wish to define a separate <strong>shipping address</strong>.</asp:Label>
	</p>

    <div class="simpleClear"></div>
    <asp:placeholder id="PlaceHolderBillAddress" runat="server" Visible="false">
    <p><strong>Shipping Address:</strong></p>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label13" runat="server">Street:&nbsp;</asp:Label>
	<asp:textbox id="ShipAddress_Street" runat="server"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator10" ControlToValidate="ShipAddress_Street" ErrorMessage="* Street name & number is required"
	        Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>


    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label14" runat="server">Street2:&nbsp;</asp:Label>
	    <asp:textbox id="ShipAddress_Street2" runat="server"></asp:textbox>
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label15" runat="server">City:&nbsp;</asp:Label>
		<asp:textbox id="ShipAddress_City" runat="server"></asp:textbox>
            <asp:requiredfieldvalidator id="Requiredfieldvalidator11" ControlToValidate="ShipAddress_City" ErrorMessage="* City name is required"
			    Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label16" runat="server">State:&nbsp;</asp:Label>
		<asp:textbox id="txtStateShip" Visible="False" Runat="server" MaxLength="255"></asp:textbox>
        <asp:dropdownlist id="selStateShip" runat="server" Visible="True"></asp:dropdownlist>
            <asp:requiredfieldvalidator id="rqdStateShip" Visible="False" ControlToValidate="txtStateShip" ErrorMessage="* State or Province name is required"
		        Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label17" runat="server">Zip:&nbsp;</asp:Label>
		<asp:textbox id="ShipAddress_ZipPostal" runat="server" Width="100px"></asp:textbox>
            <asp:requiredfieldvalidator id="Requiredfieldvalidator12" ControlToValidate="ShipAddress_ZipPostal" ErrorMessage="* Postal code is required"
		        Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
		<asp:Label ID="Label18" runat="server">Country:&nbsp;</asp:Label>
		<asp:dropdownlist id="selCountryShip" runat="server" OnSelectedIndexChanged="selCountryShip_SelectedIndexChanged"
			AutoPostBack="true"></asp:dropdownlist>
	</div>
	</asp:placeholder>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Button ID="btnCheckOut" runat="server" Text="Continue" OnClick="checkoutbutton_Click" />
    </div>

</asp:Content>