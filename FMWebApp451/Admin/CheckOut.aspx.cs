using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;
using FM2015.Helpers;

namespace FM.Admin
{
    public partial class CheckOut : AdminBasePage //System.Web.UI.Page
    {
        bool bBillingOptionChanged = false;
        Cart cart;

        protected void Page_Load(object sender, EventArgs e)
        {
            cart = new Cart();
            cart.Load(Session["CScart"]);
            if (cart.SiteCustomer.CustomerID == 0)
            {
                try
                {
                    string body = "";
                    body += "Logged in at: " + Request.ServerVariables["SERVER_NAME"].ToString() + Environment.NewLine + "------------------" + Environment.NewLine;
                    body += "login email: " + cart.SiteCustomer.Email + Environment.NewLine + "------------------" + Environment.NewLine;
                    body += "Last Name: " + cart.SiteCustomer.LastName + Environment.NewLine + "------------------" + Environment.NewLine;
                    body += "First Name: " + cart.SiteCustomer.FirstName + Environment.NewLine + "------------------" + Environment.NewLine;
                    body += "CustomerID: " + cart.SiteCustomer.CustomerID.ToString() + Environment.NewLine + "------------------" + Environment.NewLine;
                    body += "IP addr: " + Request.ServerVariables["REMOTE_ADDR"].ToString() + Environment.NewLine + "------------------" + Environment.NewLine;

                    Certnet.Cart tempcart = new Certnet.Cart(); //just need the current user ID
                    tempcart.Load(Session["cart"]);

                    if (HttpContext.Current.Request.IsSecureConnection) { body += "HTTP Status: HTTPS" + Environment.NewLine + "------------------" + Environment.NewLine; }
                    else { body += "HTTP Status: HTTP" + Environment.NewLine + "------------------" + Environment.NewLine; }

                    body += "Admin login email: " + tempcart.SiteCustomer.Email + Environment.NewLine + "------------------" + Environment.NewLine;
                    body += "Admin Last Name: " + tempcart.SiteCustomer.LastName + Environment.NewLine + "------------------" + Environment.NewLine;
                    body += "Admin First Name: " + tempcart.SiteCustomer.FirstName + Environment.NewLine + "------------------" + Environment.NewLine;
                    body += "AdminID: " + tempcart.SiteCustomer.CustomerID + Environment.NewLine + "------------------" + Environment.NewLine;

                    mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "Admin Checkout@Checkout: customerID=0", body);
                }
                catch
                {
                }
                Response.Redirect("ManageUsers.aspx");
            }

            if (!Page.IsPostBack)
            {
                bool DisplaySeparateShipping = false;

                if (cart.BillAddress.Street != cart.ShipAddress.Street) DisplaySeparateShipping = true;
                if (cart.BillAddress.City != cart.ShipAddress.City) DisplaySeparateShipping = true;
                if (cart.BillAddress.State != cart.ShipAddress.State) DisplaySeparateShipping = true;
                if (cart.BillAddress.Country != cart.ShipAddress.Country) DisplaySeparateShipping = true;

                if (DisplaySeparateShipping)
                {
                    PlaceHolderBillAddress.Visible = true;
                    SeparateBillingAddress.Checked = true;
                }


                Customer_FirstName.Text = cart.SiteCustomer.FirstName;
                Customer_LastName.Text = cart.SiteCustomer.LastName;

                BillAddress_Street.Text = cart.BillAddress.Street;
                BillAddress_Street2.Text = cart.BillAddress.Street2;
                BillAddress_City.Text = cart.BillAddress.City;
                BillAddress_ZipPostal.Text = cart.BillAddress.Zip;

                ShipAddress_Street.Text = cart.ShipAddress.Street;
                ShipAddress_Street2.Text = cart.ShipAddress.Street2;
                ShipAddress_City.Text = cart.ShipAddress.City;
                ShipAddress_ZipPostal.Text = cart.ShipAddress.Zip;

                SetStateAndCountries();
            }

        }

        protected void SetStateAndCountries()
        {
            ListItem itemA, itemB, itemC, itemD;

            //billing info
            selCountryBill.DataSource = DBUtil.GetLocations(0);
            selCountryBill.DataValueField = "Code";
            selCountryBill.DataTextField = "Name";
            selCountryBill.DataBind();

            selStateBill.DataSource = DBUtil.GetLocations(15);
            selStateBill.DataValueField = "Code";
            selStateBill.DataTextField = "Name";
            selStateBill.DataBind();

            if (cart.BillAddress.Country == "")
            {
                //initialize
                //todo
                selCountryBill.SelectedValue = "US";
                //selStateBill.SelectedValue = "CA";
            }
            else
            {
                itemA = selCountryBill.Items.FindByValue(cart.BillAddress.Country);
                if (itemA != null)
                {
                    itemA.Selected = true;

                    selStateBill.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(cart.BillAddress.Country)));
                    selStateBill.DataValueField = "Code";
                    selStateBill.DataTextField = "Name";
                    selStateBill.DataBind();
                }
                itemB = selStateBill.Items.FindByValue(cart.BillAddress.State);
                if (itemB != null)
                {
                    selStateBill.Visible = true;
                    txtStateBill.Visible = false;
                    itemB.Selected = true;
                }
                else
                {
                    selStateBill.Visible = false;
                    txtStateBill.Visible = true;
                    txtStateBill.Text = cart.BillAddress.State;
                }

            }

            //shipping info
            selCountryShip.DataSource = DBUtil.GetLocations(0);
            selCountryShip.DataValueField = "Code";
            selCountryShip.DataTextField = "Name";
            selCountryShip.DataBind();

            selStateShip.DataSource = DBUtil.GetLocations(15);
            selStateShip.DataValueField = "Code";
            selStateShip.DataTextField = "Name";
            selStateShip.DataBind();

            if (cart.ShipAddress.Country == "")
            {
                //initialize
                //todo put back
                selCountryShip.SelectedValue = "US";
                //selStateShip.SelectedValue = "CA";
            }
            else
            {
                itemC = selCountryShip.Items.FindByValue(cart.ShipAddress.Country);
                if (itemC != null)
                {
                    itemC.Selected = true;

                    selStateShip.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(cart.ShipAddress.Country)));
                    selStateShip.DataValueField = "Code";
                    selStateShip.DataTextField = "Name";
                    selStateShip.DataBind();

                    itemD = selStateShip.Items.FindByValue(cart.ShipAddress.State);
                    if (itemD != null)
                    {
                        selStateShip.Visible = true;
                        txtStateShip.Visible = false;
                        itemD.Selected = true;
                    }
                    else
                    {
                        selStateShip.Visible = false;
                        txtStateShip.Visible = true;
                        txtStateShip.Text = cart.ShipAddress.State;
                    }
                }
            }
        }

        protected void checkoutbutton_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                if (cart.BillAddress.AddressId <= 0) { GetBillingAddress(cart); }
                //this is not used anymore???
                if (!SeparateBillingAddress.Checked)
                {
                    cart.ShipAddress = cart.BillAddress;
                    cart.SiteOrder.ShippingAddressID = cart.ShipAddress.AddressId;
                    cart.SiteOrder.BillingAddressID = cart.BillAddress.AddressId;
                }
                else { GetShippingAddress(cart); }

                cart.SiteOrder.CustomerID = cart.SiteCustomer.CustomerID;
                cart.SiteOrder.BillingAddressID = cart.BillAddress.AddressId;
                cart.SiteOrder.ShippingAddressID = cart.ShipAddress.AddressId;
                Session["CScart"] = cart;
                Response.Redirect("Payment.aspx");
            }
        }


        protected void Checked_SeparateBillingAddress(Object sender, System.EventArgs e)
        {
            bBillingOptionChanged = true;

            if (SeparateBillingAddress.Checked)
            {
                PlaceHolderBillAddress.Visible = true;
                if (!Page.IsPostBack || bBillingOptionChanged)
                    bBillingOptionChanged = false;
            }
            else
            {
                PlaceHolderBillAddress.Visible = false;
            }
        }

        protected void CalculateShippingButton_Click(object sender, ImageClickEventArgs e)
        {
            cart.Load(Session["CScart"]);

            Page.Validate();
            if (Page.IsValid)
            {
                //check zipcode
                dotnetCARTmessage.Text = DBUtil.CheckZipCode(selCountryBill.SelectedValue, BillAddress_ZipPostal.Text);

                if (dotnetCARTmessage.Text == "" && SeparateBillingAddress.Checked)
                    dotnetCARTmessage.Text = DBUtil.CheckZipCode(selCountryShip.SelectedValue, ShipAddress_ZipPostal.Text);

                if (dotnetCARTmessage.Text == "")
                {
                    //cart.Calculate();
                    //cart.Add("billAddress,shipAddress,customer");

                    GetBillingAddress(cart);
                    if (!SeparateBillingAddress.Checked)
                        cart.ShipAddress = cart.BillAddress;
                    else
                    {
                        //force pass new ship state & country, if new
                        GetShippingAddress(cart);
                    }

                    cart.SiteOrder.CustomerID = cart.SiteCustomer.CustomerID;
                    cart.SiteOrder.BillingAddressID = cart.BillAddress.AddressId;

                    if (cart.ShipAddress.AddressId == 0)
                    { cart.SiteOrder.ShippingAddressID = cart.SiteOrder.BillingAddressID; }
                    else { cart.SiteOrder.ShippingAddressID = cart.ShipAddress.AddressId; }

                    Session["CScart"] = cart;
                    //cart.Save();
                    //cart.SaveCustomerDB();
                    Response.Redirect("Payment.aspx");
                }
            }
        }

        protected void selCountryBill_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (selCountryBill.SelectedValue != "")
            {
                selStateBill.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(selCountryBill.SelectedValue)));
                selStateBill.DataValueField = "Code";
                selStateBill.DataTextField = "Name";
                selStateBill.DataBind();

                if (selStateBill.Items.Count == 0)
                {
                    selStateBill.Visible = false;
                    txtStateBill.Visible = true;
                }
                else
                {
                    selStateBill.Visible = true;
                    txtStateBill.Visible = false;
                }
            }
        }

        protected void selCountryShip_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (selCountryShip.SelectedValue != "")
            {
                selStateShip.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(selCountryShip.SelectedValue)));
                selStateShip.DataValueField = "Code";
                selStateShip.DataTextField = "Name";
                selStateShip.DataBind();

                if (selStateShip.Items.Count == 0)
                {
                    selStateShip.Visible = false;
                    txtStateShip.Visible = true;
                }
                else
                {
                    selStateShip.Visible = true;
                    txtStateShip.Visible = false;
                }
            }
        }

        protected void GetBillingAddress(Cart cart)
        {
            cart.BillAddress.Street = ValidateUtil.CleanUpString(BillAddress_Street.Text);
            cart.BillAddress.Street2 = ValidateUtil.CleanUpString(BillAddress_Street2.Text);
            cart.BillAddress.City = ValidateUtil.CleanUpString(BillAddress_City.Text);
            cart.BillAddress.Zip = ValidateUtil.CleanUpString(BillAddress_ZipPostal.Text);
            cart.BillAddress.Country = selCountryBill.SelectedValue;

            if (txtStateBill.Visible) { cart.BillAddress.State = ValidateUtil.CleanUpString(txtStateBill.Text); }
            else { cart.BillAddress.State = selStateBill.SelectedValue; }

            cart.BillAddress.AddressId = 0; //get ready to create a new shipping address - need to begin w/ id=0
            cart.BillAddress.Save(cart.SiteCustomer, 1); // 1 = billing adr
        }

        protected void GetShippingAddress(Cart cart)
        {
            cart.ShipAddress.Street = ValidateUtil.CleanUpString(ShipAddress_Street.Text);
            cart.ShipAddress.Street2 = ValidateUtil.CleanUpString(ShipAddress_Street2.Text);
            cart.ShipAddress.City = ValidateUtil.CleanUpString(ShipAddress_City.Text);
            cart.ShipAddress.Zip = ValidateUtil.CleanUpString(ShipAddress_ZipPostal.Text);
            cart.ShipAddress.Country = selCountryShip.SelectedValue;

            if (txtStateShip.Visible) { cart.ShipAddress.State = ValidateUtil.CleanUpString(txtStateShip.Text); }
            else { cart.ShipAddress.State = selStateShip.SelectedValue; }

            cart.ShipAddress.AddressId = 0; //get ready to create a new shipping address - need to begin w/ id=0
            cart.ShipAddress.Save(cart.SiteCustomer, 2); // 2 = shiping adr
        }


    } 
}
