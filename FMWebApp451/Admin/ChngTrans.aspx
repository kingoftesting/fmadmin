<%@ Page Language="C#" Theme="Admin" MasterPageFile="~/Admin/MasterPage3_Blank.master" AutoEventWireup="true" Inherits="Admin_ChngTrans" Title="Credit Transaction" Codebehind="ChngTrans.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

   <div class="sectiontitle">Credit Original Transaction</div>
   <p></p>

    <table>
        <tr>
            <td class = "custservice" align="left">
                OrderID: &nbsp<asp:Label id="lblOrderID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class = "custservice" >
            Transaction:
            <br />
            <asp:Literal ID="litOrigTran" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class = "custservice" >
                <asp:Panel ID="pnlTran" runat="server">
                    Refund Amount:<asp:TextBox ID="txtAmount" Text="0.00" runat="server" Width="75px"></asp:TextBox>
			        <asp:requiredfieldvalidator id="valReqdAmount" Runat="server" Display="Dynamic" ErrorMessage="Please enter a refund amount."
					        ControlToValidate="txtAmount"></asp:requiredfieldvalidator>
                    <asp:CompareValidator ID="valAmount" runat="server" Type="Double" ControlToValidate="txtAmount" 
                        ErrorMessage="<br />The format of the Refund value is not valid (use x.yz)." ToolTip="The format of the Refund value is not valid (use x.yz)."
                        Display="Dynamic" Operator="DataTypeCheck" ></asp:CompareValidator>
                    <br /><br />
                    Reason: <asp:TextBox ID="txtReason" runat="server" Width="400px"></asp:TextBox> 
                    <asp:Label ID="lblErrMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                    <br />
                    <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" Text="Submit Refund" />
                    <asp:requiredfieldvalidator id="Requiredfieldvalidator1" Runat="server" Display="Dynamic" ErrorMessage="Please enter a reason for the refund."
					        ControlToValidate="txtReason"></asp:requiredfieldvalidator>
                    <br /><br />
                </asp:Panel>
               <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" CausesValidation="false" />
            </td>
        </tr>
        <tr>
            <td class = "custservice" >
                <asp:Panel ID="pnlNoTran" runat="server">
                    <div class="sectiontitle">You May Only Make Refunds To A Previous Sale</div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    
</asp:Content>

