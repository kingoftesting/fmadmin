using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;

public partial class Admin_ChngTrans : AdminBasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            CanUserProceed(new string[2] { "ADMIN", "CUSTOMERSERVICE" });

            int OrderID = 0;
            string OrigID = "";
            if (Request["OrderID"] != null) { OrderID = Convert.ToInt32(Request["OrderID"]); }
            if (Request["OrigID"] != null) { OrigID = Request["OrigID"].ToString(); }             
            if ((Request["OrderID"] != null) && (Request["OrigID"] != null))
            {
                Cart tcart = new Cart();
                tcart = tcart.GetCartFromOrder(OrderID);
                tcart.Tran = Transaction.GetTransaction(OrderID,OrigID);

                string tranhtml = "<table width='90%' border='1px solid gray'>";
                tranhtml += "<tr style='background-color:#DDDDDD'>";
                tranhtml += "<td width='75px'>ID</td>";
                tranhtml += "<td width='175px'>Date</td>";
                tranhtml += "<td width='150px'>CardHolder</td>";
                tranhtml += "<td width='75px'>TransType</td>";
                tranhtml += "<td width='75px'>CC Number</td>";
                tranhtml += "<td width='75px'>Amount</td>";
                tranhtml += "<td width='75px'>ResultMsg</td>";
                tranhtml += "</tr>";
                tranhtml += "<tr>";
                tranhtml += "<td>" + tcart.Tran.TransactionID.ToString() + "</td>";
                tranhtml += "<td>" + tcart.Tran.TransactionDate.ToString() + "</td>";
                tranhtml += "<td>" + tcart.Tran.CardHolder.ToString() + "</td>";
                tranhtml += "<td>" + tcart.Tran.TrxType.ToString() + "</td>";
                tranhtml += "<td>" + tcart.Tran.CardNo.ToString() + "</td>";
                tranhtml += "<td>" + tcart.Tran.TransactionAmount.ToString() + "</td>";
                tranhtml += "<td>" + tcart.Tran.ResultMsg.ToString() + "</td>";
                tranhtml += "</tr>";
                tranhtml += "</table>";
                litOrigTran.Text = tranhtml;

                lblOrderID.Text = OrderID.ToString();

                if ((tcart.Tran.TrxType.ToString() == "S") || (tcart.Tran.TrxType.ToString() == "SA"))
                {
                    pnlTran.Visible = true;
                    pnlNoTran.Visible = false;
                }
                else
                {
                    pnlTran.Visible = false; 
                    pnlNoTran.Visible = true;
                }
            }
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int OrderID = Convert.ToInt32(Request["OrderID"]);
            string OrigID = Request["OrigID"].ToString();

            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(OrderID);


            cart.SiteOrder.Total = Convert.ToDecimal(txtAmount.Text);
            cart.Credit(OrigID, cart.SiteOrder.SourceID);

            Certnet.Cart tempcart = new Certnet.Cart();
            tempcart.Load(Session["cart"]); //get name of CS rep requesting address change
            string username = tempcart.SiteCustomer.LastName + "," + tempcart.SiteCustomer.FirstName;
            string ordernote = "Refund transaction: OrigID=";
            ordernote += OrigID + " NewID=" + cart.Tran.TransactionID + " ResultMsg=" + cart.Tran.ResultMsg + " Amt=" + cart.Tran.TransactionAmount.ToString();
            ordernote += " Reason: " + ValidateUtil.CleanUpString(txtReason.Text);
            OrderNote.Insertordernote(OrderID, username, ordernote);

            if (cart.Tran.ResultCode == 0)
            {
                lblErrMsg.Text = "Refund Successfull!";

                adminCER.CER cer = new adminCER.CER(cart);
                cer.EventDescription = "REFUND TRANSACTION: Refund amount=" + cart.Tran.TransactionAmount.ToString()  + Environment.NewLine + cer.EventDescription;
                int result = cer.Add();

                //Helpers.CancelOrder(OrderID, username); //don't automatically cancel, could be a partial refund, etc.
                Response.Redirect("ReviewOrder.aspx?OrderID=" + OrderID.ToString());
            }
            else // Refund was not successful for some reason
            {
                lblErrMsg.Text = "Refund NOT Successfull:&nbsp;" + cart.Tran.ResultMsg;
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        int OrderID = Convert.ToInt32(Request["OrderID"]);
        Response.Redirect("ReviewOrder.aspx?OrderID=" + OrderID.ToString());
    }

}
