﻿<%@ Page Title="ClassMaker" Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="site_Admin_ClassMaker" Codebehind="ClassMaker.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<div style="margin-left:10px">
<strong>Class Generator:</strong> outputs 5 items (copy & paste into appropriate asp.net files or SQL Server page)<br />
** Assumes Database table column names are the same as Class var names
<ul>
    <li>class definition (.cs file-compatible) **Assumes & Inserts a Primary Key field = "ID"</li>
    <li>Admin .aspx page (.net HTML page to access database for new class)</li>
    <li>Admin .aspx.cs page (.net code-behind page to access database for new class)</li>
    <li>SQL Table CREATE (suitable for SQL Server 2008; creates a SQL DB table for new class)</li>
    <li>XL-DB copy .aspx & .aspx.cs pages (.net code to take an XL spreadsheet and copy the contents to the new DB table)</li>
</ul>
</div>

<div id="divClassInput" style="margin-left:10px">
    <asp:Panel id="pnlClassInput" Visible="true" runat="server">
        <strong>Class Name:</strong> &nbsp <asp:TextBox ID="txtClassName" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="valRequireClassName" runat="server" ControlToValidate="txtClassName" SetFocusOnError="true"
                        Text="The ClassName field is required." ToolTip="The ClassName field is required (type = string)." Display="Dynamic" ValidationGroup="ClassMaker"></asp:RequiredFieldValidator>
        <br /><br />
        <strong>Class Privat/Public Var Pairs:</strong> Input (type,name). Pairs are seperated by ";" Example: int,Num1; string,Name1; decimal,Price1 <br />
        <asp:TextBox ID="txtClassPairs" runat="server" Width="90%" TextMode="MultiLine" Columns="100" Rows="5" MaxLength="4000"></asp:TextBox>                      
        <asp:RequiredFieldValidator ID="RequiredFieldtxtClassPairs" runat="server" ControlToValidate="txtClassPairs" SetFocusOnError="true"
                        Text="The ClassPairs field is required." ToolTip="The ClassPairs field is required (type = string)." Display="Dynamic" ValidationGroup="ClassMaker"></asp:RequiredFieldValidator>
        <br /><br />
        <asp:Button ID="btnClassInput" runat="server" Text="Submit" OnClick="btnClassInput_OnClick" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblSelect" Visible="false" runat="server" Text="<strong>Select Output Template</strong> "> </asp:Label>
        <asp:DropDownList ID="ddlPagePanel" Visible="false" AutoPostBack="true" runat="server">
            <asp:ListItem Text="Class (.cs)" Value="Class" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Admin HTML (.aspx)" Value="MarkUp" Selected="False"></asp:ListItem>
            <asp:ListItem Text="Admin CodeBehind (.aspx.cs)" Value="CodeBehind" Selected="False"></asp:ListItem>
            <asp:ListItem Text="SQL Create (SQL Server 2008)" Value="SQL" Selected="False"></asp:ListItem>
            <asp:ListItem Text="XL Copy (.aspx & .aspx.cs)" Value="XL" Selected="False"></asp:ListItem>
        </asp:DropDownList>
        <hr />
    </asp:Panel>
</div>

<div id="divClassOutput" style="margin-left:10px">
    <asp:Panel id="pnlClassOutput" Visible="false" runat="server">
        Class Output: <br />
        <asp:Label ID="lblClassOutput" runat="server" ></asp:Label>
        <hr />
    </asp:Panel>
</div>

<div id="divAdminHTML" style="margin-left:10px">
    <asp:Panel id="pnlAdminHTML" Visible="false" runat="server">
        Admin HTML Page Output: <br /> 
        <asp:Label ID="lblAdminHTML" runat="server" ></asp:Label>
        <hr />
    </asp:Panel>
</div>

<div id="divCodeBehind" style="margin-left:10px">
    <asp:Panel id="pnlCodeBehind" Visible="false" runat="server">
        Code-Behind Output: <br />
        <asp:Label ID="lblCodeBehind" runat="server" ></asp:Label>
    </asp:Panel>
</div>

<div id="divSQL" style="margin-left:10px">
    <asp:Panel id="pnlSQL" Visible="false" runat="server">
        SQL Output: <br />
        <asp:Label ID="lblSQL" runat="server" ></asp:Label>
    </asp:Panel>
</div>

<div id="divXL" style="margin-left:10px">
    <asp:Panel id="pnlXL" Visible="false" runat="server">
        Copy-From-XL file Output: <br />
        <asp:Label ID="lblXL" runat="server" ></asp:Label>
    </asp:Panel>
</div>
</asp:Content>

