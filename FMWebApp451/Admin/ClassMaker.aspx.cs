﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class site_Admin_ClassMaker : AdminBasePage
{
    string strClass = "";
    string strMarkup = "";
    string strCBehind = "";
    string strSQL = "";
    string strXL = "";
    List<varPairs> vpList = new List<varPairs>();


    public class varPairs
    {
        private string type;
        public string Type { get { return type; } set { type = value; } }
        private string var;
        public string Var { get { return var; } set { var = value; } }
        private string privVar;
        public string PrivVar { get { return privVar; } set { privVar = value; } }
        private string pubVar;
        public string PubVar { get { return pubVar; } set { pubVar = value; } }

        public varPairs()
        {
            Type = "";
            Var = "";
            PrivVar = "";
            PubVar = "";
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        pnlClassOutput.Visible = false;
        pnlAdminHTML.Visible = false;
        pnlCodeBehind.Visible = false;
        pnlSQL.Visible = false;
        pnlXL.Visible = false;

        if (Page.IsPostBack)
        {
            switch (ddlPagePanel.SelectedValue)
            {
                case "Class": pnlClassOutput.Visible = true;
                     break;

                case "MarkUp": pnlAdminHTML.Visible = true;
                     break;

                case "CodeBehind": pnlCodeBehind.Visible = true;
                     break;

                case "SQL": pnlSQL.Visible = true;
                     break;

                case "XL": pnlXL.Visible = true;
                     break;

                default:
                     break;
            }
        }

    }

    protected void btnClassInput_OnClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            pnlClassOutput.Visible = true;
            ddlPagePanel.Visible = true;
            lblSelect.Visible = true;
            string className = ValidateUtil.CleanUpString(txtClassName.Text);
            initOutputs(className);

            string namePairs = ValidateUtil.CleanUpString(txtClassPairs.Text);
            namePairs = namePairs.Replace(" ", string.Empty);
            namePairs = namePairs.Replace("\r", string.Empty);
            namePairs = namePairs.Replace("\n", string.Empty);
            namePairs = namePairs.Replace("\t", string.Empty);
            string[] Pairs = namePairs.Split(';');
           foreach (string s in Pairs)
            {
                varPairs vP = new varPairs();
                string[] temp = s.Split(',');
                if (temp.Count() == 2)
                {
                    vP.Type = temp[0].ToLower();
                    if (vP.Type == "datetime") { vP.Type = "DateTime"; }
                    vP.Var = temp[1];
                    vP.PrivVar = vP.Var.First().ToString().ToLower() + String.Join("", vP.Var.Skip(1));
                    vP.PubVar = vP.Var.First().ToString().ToUpper() + String.Join("", vP.Var.Skip(1));
                    vpList.Add(vP);
                }
             }

            BuildClass(className);
            BuildMarkUp(className);
            BuildSQL(className);
            BuildXL(className);

        }
        lblClassOutput.Text = strClass;
        lblAdminHTML.Text = strMarkup;
        lblCodeBehind.Text = strCBehind;
        lblSQL.Text = strSQL;
        lblXL.Text = strXL;

    }

    private void initOutputs(string name)
    {

        foreach (string s in _initMarkup)
        {
            string temp = s.Replace("#Name", name);
            strMarkup += temp.Replace("#br", "<br />") + "<br />";
        }

        foreach (string s in _codebehind)
        {
            string temp = s.Replace("#Name", name);
            strCBehind += temp.Replace("#br", "<br />") + "<br />";
        }

    }

    private void BuildClass(string className)
    {
        //initialize the Class
        foreach (string s in _initClass)
        {
            string temp = s.Replace("#Name", className);
            strClass += temp.Replace("#br", "<br />") + "<br />";
        }

        //build the Class variables
        BuildClassVars("int", "id", "ID"); //build the primary key variable first
        foreach (varPairs s in vpList)
        {
            BuildClassVars(s.Type, s.PrivVar, s.PubVar);
        }
        strClass += "#endregion" + "<br /><br />";

        //build Class constructor #1
        strClass += "public " + className + "()" + "<br />";
        strClass += "	{" + "<br />";
        strClass += "		ID = 0;" + "<br />"; //build primary key default value
        foreach (varPairs s in vpList)
        {
            strClass += "		" + s.PubVar + " = " + TypeDefault(s.Type) + ";" + "<br />";
        }
        strClass += "}" + "<br /><br />";

        BuildClassConstructor2(className); //constructor to init class object at ID location
        BuildClassSave(className); //method to update at ID location or insert new object 

        strClass += "/***********************************" + "<br />";
        strClass += "* Static methods" + "<br />";
        strClass += "************************************/" + "<br />";
        strClass += "<br />";

        BuildClassGetList(className);

        //Get class obj by ID
        strClass += "/// &lt;summary&gt;" + "<br />";
        strClass += "/// Returns a className object with the specified ID" + "<br />";
        strClass += "/// &lt;/summary&gt;" + "<br />";
        strClass += "<br />";
        strClass += "public static " + className + " Get" + className + "ByID(int id)" + "<br />";
        strClass += "{" + "<br />";
        strClass += className + " obj = new " +  className + "(id);" + "<br />";
        strClass += "return obj;" + "<br />";
        strClass += "}" + "<br /><br />";

        BuildClassUpdate(className);
        BuildClassInsert(className);

        strClass += "}" + "<br /><br />";
    }

    private void BuildMarkUp(string className)
    {
        //build Gridview Bound fields
        strMarkup += "<br />";
        strMarkup += "&lt;Columns&gt;" + "<br />";
        strMarkup += "&lt;asp:BoundField DataField=&quot;ID&quot; HeaderText=&quot;ID&quot; HeaderStyle-HorizontalAlign=&quot;center&quot; /&gt;" + "<br />";
        foreach (varPairs s in vpList)
        {
            strMarkup += "&lt;asp:BoundField DataField=" + "&quot;" + s.PubVar + "&quot; HeaderText=" + "&quot;" + s.PubVar + "&quot; HeaderStyle-HorizontalAlign= &quot; center &quot; /&gt;" + "<br />";
        }
        strMarkup += "&lt;asp:CommandField ButtonType=&quot;Image&quot; SelectImageUrl=&quot;~/Images/Edit.gif&quot; SelectText=&quot;Edit&quot; ShowSelectButton=&quot;True&quot;&gt;" + "<br />";
        strMarkup += "&lt;ItemStyle HorizontalAlign=&quot;Center&quot; Width=&quot;20px&quot; /&gt;" + "<br />";
        strMarkup += "&lt;/asp:CommandField&gt;" + "<br />";
        strMarkup += "&lt;/Columns&gt;" + "<br />";
        strMarkup += "<br />";
        strMarkup += "&lt;EmptyDataTemplate&gt;&lt;b&gt;No " + className + " Data Available&lt;/b&gt;&lt;/EmptyDataTemplate&gt;" + "<br />";
        strMarkup += "&lt;/asp:GridView&gt;" + "<br />";
        strMarkup += "<br />";

        //Begin GridView datasource
        strMarkup += "&lt;asp:ObjectDataSource ID=&quot;obj" + className + "&quot; runat=&quot;server&quot; SelectMethod=&quot;Get" + className + "List&quot;" + "<br />";
        strMarkup += "TypeName=&quot;" + className + "&quot;&gt;" + "<br />";
        strMarkup += "&lt;/asp:ObjectDataSource&gt;" + "<br />";
        strMarkup += "<br />";

        //<p><p>
        strMarkup += "&lt;p&gt;&lt;/p&gt;" + "<br />";

        //begin DetailsView
        strMarkup += "&lt;asp:DetailsView ID=&quot;dvw" + className + "&quot; runat=&quot;server&quot;" + "<br />";
        strMarkup += "AutoGenerateRows=&quot;False&quot;" + "<br />";
        strMarkup += "DataSourceID=&quot;objCurr" + className + "&quot;" + "<br />";
        strMarkup += "Width=&quot;100%&quot;" + "<br />";
        strMarkup += "AutoGenerateEditButton=&quot;True&quot;" + "<br />";
        strMarkup += "AutoGenerateInsertButton=&quot;True&quot;" + "<br />";
        strMarkup += "HeaderText=&quot;" + className + " Details&quot;" + "<br />";
        strMarkup += "DataKeyNames=&quot;ID&quot;" + "<br />";
        strMarkup += "DefaultMode=&quot;Insert&quot;" + "<br />";
        strMarkup += "OnItemCommand=&quot;dvw" + className + "_ItemCommand&quot;" + "<br />";
        strMarkup += "OnItemInserted=&quot;dvw" + className + "_ItemInserted&quot;" + "<br />";
        strMarkup += "OnItemUpdated=&quot;dvw" + className + "_ItemUpdated&quot;" + "<br />";
        strMarkup += "OnItemCreated=&quot;dvw" + className + "_ItemCreated&quot;&gt;" + "<br />";

        //begin parameter fields & templates
        strMarkup += "&lt;FieldHeaderStyle Width=&quot;100px&quot; /&gt;" + "<br />";
        strMarkup += "&lt;Fields&gt;" + "<br />";

        //ID Template
        strMarkup += "&lt;asp:TemplateField HeaderText=&quot;ID&quot;&gt;" + "<br />";
        strMarkup += "&lt;ItemTemplate&gt;" + "<br />";
        strMarkup += "&lt;asp:Label ID=&quot;lblID&quot; Visible=&quot;true&quot; Text='&lt;%# Eval(&quot;ID&quot;) %&gt;' runat=&quot;server&quot;&gt;&lt;/asp:Label&gt;" + "<br />";
        strMarkup += "&lt;/ItemTemplate&gt;" + "<br />";
        strMarkup += "&lt;/asp:TemplateField&gt;" + "<br />";
        strMarkup += "<br />";

        //the rest of the parameter templates
        foreach (varPairs s in vpList)
        {
            strMarkup += "&lt;asp:TemplateField HeaderText=&quot;" + s.PubVar + "&quot;&gt;" + "<br />";
            strMarkup += "&lt;ItemTemplate&gt;" + "<br />";
            strMarkup += "&lt;asp:Label ID=&quot;lbl" + s.PubVar +"&quot; runat=&quot;server&quot; Text='&lt;%# Eval(&quot;" + s.PubVar + "&quot;) %&gt;'&gt;&lt;/asp:Label&gt;" + "<br />";
            strMarkup += "&lt;/ItemTemplate&gt;" + "<br />";
            strMarkup += "&lt;EditItemTemplate&gt;" + "<br />";
            strMarkup += GetDvwEditTemplate(s);
            strMarkup += "&lt;/EditItemTemplate&gt;" + "<br />";
            strMarkup += "&lt;/asp:TemplateField&gt;" + "<br />";
            strMarkup += "<br />";
        }

        strMarkup += "&lt;/Fields&gt;" + "<br />";
        strMarkup += "&lt;/asp:DetailsView&gt;" + "<br />";
        strMarkup += "<br />";

        //build ObjectDataSource for DetailsView
         strMarkup += "&lt;asp:ObjectDataSource ID=&quotobjCurr" + className + "&quot; runat=&quot;server&quot;" + "<br />";
         strMarkup += "TypeName=&quot" + className + "&quot;"  + "<br />";
         strMarkup += "SelectMethod=&quot;Get" + className + "ByID&quot;" + "<br />";
         strMarkup += "UpdateMethod=&quot;Update" + className + "&quot;" + "<br />";
         strMarkup += "InsertMethod=&quot;Insert" + className + "&quot;&gt;" + "<br />"; 
         strMarkup += "&lt;SelectParameters&gt;" + "<br />";
         strMarkup += "&lt;asp:ControlParameter ControlID=&quot;gvw" + className +"&quot; Name=&quot;id&quot; PropertyName=&quot;SelectedValue&quot; Type=&quot;Int32&quot; /&gt;" + "<br />"; 
         strMarkup += "&lt;/SelectParameters&gt;" + "<br />";
         strMarkup += "&lt;UpdateParameters&gt;" + "<br />";

         strMarkup += "&lt;asp:Parameter name=&quotid&quot Type=&quotInt32&quot /&gt;" + "<br />";
         //build rest of Update paramter list
         foreach (varPairs s in vpList)
         {
             strMarkup += GetSaveParameterList(s);
         }
         strMarkup += "&lt;/UpdateParameters&gt;" + "<br />";

         //build rest of Insert paramter list
         strMarkup += "&lt;InsertParameters&gt;" + "<br />";
         foreach (varPairs s in vpList)
         {
             strMarkup += GetSaveParameterList(s);
         }
         strMarkup += "&lt;/InsertParameters&gt;" + "<br />";

         strMarkup += "&lt;/asp:ObjectDataSource&gt;" + "<br />";
         strMarkup += "<br />";

    }

    private void BuildSQL(string className)
    {
        //build a SQL CREATE procedure for className
        strSQL += "<br /><br />";
        strSQL+= "SET ANSI_NULLS ON" + "<br />";
        strSQL+= "GO" + "<br />";
        strSQL+= "SET QUOTED_IDENTIFIER ON" + "<br />";
        strSQL+= "GO" + "<br />";
        strSQL+= "SET ANSI_PADDING ON" + "<br />";
        strSQL+= "GO" + "<br />";
        strSQL+= "IF NOT EXISTS (SELECT * FROM sys.objects WHERE id = OBJECT_ID(N'[dbo].[" + className +"]') AND type in (N'U'))" + "<br />";
        strSQL+= "BEGIN" + "<br />";
        strSQL += "CREATE TABLE [dbo].[" + className + "](" + "<br />";
        strSQL += "     [ID] [int] IDENTITY(1,1) NOT NULL";
        if (vpList.Count() == 0) { strSQL += " <br />"; }
        else { strSQL += ", <br />"; }

        int count = 0;
        foreach (varPairs s in vpList)
        {
            strSQL += GetSQLParameterList(s);
            count++;
            if (count == vpList.Count()) { strSQL += " <br />"; } //last param doesn't get a comma
            else { strSQL += ", <br />"; }
        }
        strSQL += ") ON [PRIMARY]" + "<br />";
        strSQL += "END" + "<br />";
        strSQL += "GO" + "<br />";
        strSQL += "SET ANSI_PADDING OFF" + "<br />";
        strSQL += "GO" + "<br />";
        strSQL += "<br />";
    }

    private void BuildXL(string className)
    {

        strXL += "<br />";

        strXL += "//HTML code for XL-to-DB copy (.aspx file)" + "<br />";
        strXL += "&lt;asp:Label ID=&quot;Label1&quot; runat=&quot;server&quot; Text=&quot;Press To Start Table Copy&quot;&gt;&lt;/asp:Label&gt;" + "<br />";
        strXL += "&lt;asp:Button ID=&quot;Button1&quot; runat=&quot;server&quot; Text=&quot;Start&quot; OnClick=&quot;Button1_Click&quot; /&gt;" + "<br />";
        strXL += "&lt;br /&gt;" + "<br />";
        strXL += "&lt;asp:Label ID=&quot;Label2&quot; runat=&quot;serve&quot; Visible=&quot;false&quot; Text=&quot;label not updated yet&quot;&gt;&lt;/asp:Label&gt;" + "<br />";
        strXL += "<br /><br />";


        strXL += "//C# code for XL-to-DB copy (.aspx.cs file)" + "<br />";

        strXL += "protected void Page_Load(object sender, EventArgs e)" + "<br />";
        strXL += "{" + "<br />";
        strXL += "}" + "<br /><br />";

        strXL += "protected void Button1_Click(object sender, EventArgs e)" + "<br />";
        strXL += "{" + "<br /><br />";

        strXL += "//step 1" + "<br />";
        strXL += "//open XL file, read contents" + "<br />";
        strXL += "<br />";
        strXL += "// Connection String to Excel Workbook" + "<br />";
        strXL += "string sExcelFileName = &quot;" + className + ".xls&quot;" + "<br />";
        strXL += "string excelConnectionString = @&quot;" + "<br />";
        strXL += "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + "<br />";
        strXL += "&quot;;" + "<br />";
        strXL += "<br />";

        strXL += "excelConnectionString += Server.MapPath(sExcelFileName);" + "<br />";
        strXL += "excelConnectionString += @&quot;" + "<br />";
        strXL += ";Extended Properties=&quot;&quot;Excel 8.0;HDR=YES;&quot;&quot;" + "<br />";
        strXL += "&quot;;" + "<br />";
        strXL += "<br />";

        strXL += "OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);" + "<br />";
        strXL += "excelConnection.Open(); // This code will open excel file." + "<br />";
        strXL += "<br />";
        strXL += "string strSQL = @&quot;" + "<br />";
        strXL += "Select ";
        int count = 0;
        foreach (varPairs s in vpList)
        {
            strXL += s.PubVar;
            count++;
            if (count == vpList.Count()) { strXL += " <br />"; } //last param doesn't get a comma
            else { strXL += ", "; }
        }
        strXL += "FROM [" + className + "$]" + "<br />";
        strXL += "&quot;;" + "<br />";
        strXL += "<br />";

        strXL += "OleDbCommand dbCommand = new OleDbCommand(strSQL, excelConnection);" + "<br />";
        strXL += "OleDbDataAdapter dataAdapter = new OleDbDataAdapter(dbCommand);" + "<br />";
        strXL += "<br />";

        strXL += "// create dataset" + "<br />";
        strXL += "DataSet dSet = new DataSet();" + "<br />";
        strXL += "//fill dSet with XL data" + "<br />";
        strXL += "dataAdapter.Fill(dSet);" + "<br />";
        strXL += "<br />";

        strXL += "//step 2" + "<br />"; 
        strXL += "//connect to server; copy XL data" + "<br />"; 
        strXL += "<br />";

        strXL += "DataTable dt = dSet.Tables[0];" + "<br />"; 
        strXL += "int ColumnCount = dt.Columns.Count;" + "<br />";
        strXL += "string msg = &quot;Copy XL Succeeded!&quot;;" + "<br />";
        strXL += "foreach (DataRow r in dt.Rows)" + "<br />";
        strXL += "{" + "<br />";
        strXL += className + " obj = new " + className + "();" + "<br />";
        strXL += "obj.ID = 0;" + "<br />";
        foreach (varPairs s in vpList)
        {
            strXL += "obj." + s.PubVar + " = " + GetXLConvert(s)+ "; <br />";
        }
        strXL += "<br />";

        strXL += "int ret = " + className + ".Insert" + className + "(";
        count = 0;
        foreach (varPairs s in vpList)
        {
            strXL +=  "obj." + s.PubVar;
            count ++;
            if (count == vpList.Count()) { strXL +=  "); <br />"; } //last param doesn't get a comma, but gets closed instead
            else { strXL +=  ", "; }
        }
         
        strXL += "if (ret <= 0)" + "<br />";
        strXL += "{" + "<br />";
        strXL += "msg = &quot;Copy XL Failed! &quot;;" + "<br />";
        foreach (varPairs s in vpList)
        {
            strXL += "msg += &quot; " + s.PubVar + ":&quot; + obj." + s.PubVar + ".ToString() + &quot;, &quot;;" + "<br />";
        }
        strXL += "break;" + "<br />";


        strXL += "}" + "<br />";
        strXL += "}" + "<br />";
        strXL += "<br />";

        strXL += "// dispose used objects" + "<br />";
        strXL += "dSet.Dispose();" + "<br />";
        strXL += "dataAdapter.Dispose();" + "<br />";
        strXL += "dbCommand.Dispose();" + "<br />";
        strXL += "<br />";

        strXL += "excelConnection.Close();" + "<br />";
        strXL += "excelConnection.Dispose();" + "<br />";
        strXL += "<br />";

        strXL += "Label2.Text = msg;" + "<br />";
        strXL += "Label2.Visible = true;" + "<br />";
        strXL += "}" + "<br />"; 
        strXL += "<br />";

        strXL += "} //end of Button1_Click" + "<br /><br />";

    }


    private void BuildClassVars(string type, string privVar, string pubVar)
    {
        strClass += "   private " + type + " " + privVar + ";" + "<br />";
        strClass += "   public " + type + " " + pubVar + " { get { return " + privVar + "; } set { " + privVar + " = value; } }" + "<br />";
    }

    private void BuildClassConstructor2(string className)
    {
        strClass += "public " + className + "(int IDfield)" + "<br />";
        strClass += "	{" + "<br />";
        strClass += "       string sql = @&quot;" + " <br />";
        strClass += "           SELECT * FROM &nbsp;" + className + "&nbsp;<br />";
        strClass += "           WHERE ID = @ID" + "&nbsp;<br />";
        strClass += "           &quot;;" + "&nbsp;<br />";
        strClass += "       SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, IDfield);" + "<br />";
        strClass += "       SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);" + "<br />";
        strClass += "       if (dr.Read())" + "<br />";
        strClass += "       {" + "<br />";
        strClass += "           ID = Convert.ToInt32(dr[&quotID&quot]);" + "<br />";

        foreach (varPairs s in vpList)
        {
            strClass += "           " + s.PubVar + " = " + DBColumnConvert(s.Type, s.PubVar) + ";" + "<br />";
        }
        strClass += "        }" + "<br />";
        strClass += "       if (dr != null) { dr.Close(); }" + "<br />";
        strClass += "   }" + "<br /><br />";
    }

    private void BuildClassSave(string className)
    {
        strClass += "public int Save(int ID)" + "<br />";
        strClass += "	{" + "<br />";
        strClass += "       int result = 0;" + "<br />";
        strClass += "       SqlParameter[] mySqlParameters = null;" + "<br />";
        strClass += "       SqlDataReader dr = null;" + "<br />";
        strClass += "       if (ID == 0)" + "<br />";
        strClass += "       {" + "<br />";
        strClass += "           string sql = @&quot;" + "&nbsp;<br />";
        strClass += "               INSERT INTO " + className + "&nbsp;<br />";
        strClass += "               (";
        foreach (varPairs s in vpList)
        {
            strClass += s.PubVar + ", ";
        }
        char[] trimChars = { ',', ' ' };
        string saveTemp = strClass.TrimEnd(trimChars); //remove last ","
        strClass = saveTemp;
        strClass += ")" + " <br />";
        strClass += "               VALUES(";
        foreach (varPairs s in vpList)
        {
            strClass += "@" + s.PubVar + ", ";
        }
        saveTemp = strClass.TrimEnd(trimChars); //remove last ","
        strClass = saveTemp;
        strClass += ");" + " <br />";
        strClass += "               SELECT ID=@@identity; " + "&nbsp;<br />";
        strClass += "               &quot;; " + " <br />";

        strClass += "       mySqlParameters = DBUtil.BuildParametersFrom(sql, ";
        foreach (varPairs s in vpList)
        {
            strClass += s.PubVar + ", ";
        }
        saveTemp = strClass.TrimEnd(trimChars); //remove last ","
        strClass = saveTemp;
        strClass += ");" + " <br />";

        strClass += "       dr = DBUtil.FillDataReader(sql, mySqlParameters);" + "<br />";
        strClass += "       if (dr.Read())" + "<br />";
        strClass += "       {" + "<br />";
        strClass += "           result = Convert.ToInt32(dr[&quot;ID&quot;]);" + "<br />";
        strClass += "       }" + "<br />";
        strClass += "       else" + "<br />";
        strClass += "       {" + "<br />";
        strClass += "           result = 99;" + "<br />";
        strClass += "       }" + "<br />";
        strClass += "   }" + "<br />";
        strClass += "   else" + "<br />";
        strClass += "   {" + "<br />";
        strClass += "       string sql = @&quot;" + "&nbsp;<br />";
        strClass += "        UPDATE " + className + "&nbsp;<br />";
        strClass += "        SET " + "&nbsp;<br />";

        int updateCount = 0;
        foreach (varPairs s in vpList)
        {
            updateCount++;
            if (updateCount == vpList.Count) { strClass += "       " + s.PubVar + "  =   @" + s.PubVar + " " + "&nbsp;<br />"; } //last param doesn't get a commna
            else { strClass += "       " + s.PubVar + "  =   @" + s.PubVar + ", " + "&nbsp;<br />"; }
        }

        strClass += "       WHERE ID = @ID " + "&nbsp;<br />";
        strClass += "       &quot;;" + "<br />";
        strClass += "       mySqlParameters = DBUtil.BuildParametersFrom(sql, ";
        foreach (varPairs s in vpList)
        {
            strClass += s.PubVar + ", ";
        }
        strClass += " ID);" + " <br />";

        strClass += "       DBUtil.Exec(sql, mySqlParameters);" + "<br />";
        strClass += "}" + "<br />";
        strClass += "return result;" + "<br />";
        strClass += "}" + "<br /><br />";
    }

    private void BuildClassGetList(string className)
    {
        strClass += "/// &lt;summary&gt;" + "<br />";
        strClass += "/// Returns a collection with all " + className + "<br />";
        strClass += "/// &lt;/summary&gt;" + "<br />";
        strClass += "public static List&lt;" + className + "&gt; Get" + className + "List()" + "<br />";
        strClass += "	{" + "<br />";
        strClass += "       List&lt;" + className + "&gt; thelist = new List&lt;" + className + "&gt;();" + "<br />";
        strClass += "       <br />";
        strClass += "       string sql = @&quot;" + "&nbsp;<br />";
        strClass += "           SELECT * FROM " + className + "&nbsp;<br />";
        strClass += "           &quot;;" + "&nbsp;<br />";
        strClass += "       <br />";
        strClass += "       SqlDataReader dr = DBUtil.FillDataReader(sql);" + "<br />";
        strClass += "       <br />";
        strClass += "       while (dr.Read())" + "<br />";
        strClass += "       {" + "<br />";
        strClass += "          " + className + " obj = new " + className + "();" + "<br />";
        strClass += "       <br />";
        strClass += "           obj.ID = Convert.ToInt32(dr[&quotID&quot]);" + "<br />";
        foreach (varPairs s in vpList)
        {
            strClass += "        obj." + s.PubVar + " = " + DBColumnConvert(s.Type, s.PubVar) + ";" + "<br />";
        }
        strClass += "       <br />";
        strClass += "       thelist.Add(obj);" + "<br />";
        strClass += "        }" + "<br />";
        strClass += "       if (dr != null) { dr.Close(); }" + "<br />";
        strClass += "       return thelist;" + "<br />";
        strClass += "   }" + "<br /><br />";
    }

    private void BuildClassUpdate(string className)
    {
        strClass += "/// &lt;summary&gt;" + "<br />";
        strClass += "/// Updates an existing " + className + "<br />";
        strClass += "/// &lt;/summary&gt;" + "<br />";
        strClass += "public static bool Update" + className + "(int id, " + "<br />";
        foreach (varPairs s in vpList)
        {
            strClass += s.Type + " " + s.PubVar.ToLower() + ", ";
        }
        char[] trimChars = { ',', ' ' };
        string saveTemp = strClass.TrimEnd(trimChars); //remove last ","
        strClass = saveTemp;
        strClass += ")" + "<br />";
        strClass += "	{" + "<br />";
        strClass +=         className + " obj = new " + className + "();" + "<br />";
        strClass += "       <br />";
        strClass += "obj.ID = id;" + "<br />";
        foreach (varPairs s in vpList)
        {
            strClass += "        obj." + s.PubVar + " = " + s.PubVar.ToLower() + ";" + "<br />";
        }
        strClass += "       int ret = obj.Save(obj.ID);" + "<br />";
        strClass += "       return (ret > 0)? true : false;" + "<br />";
        strClass += "   }" + "<br /><br />";
    }

    private void BuildClassInsert(string className)
    {
        strClass += "/// &lt;summary&gt;" + "<br />";
        strClass += "/// Creates a new " + className + "<br />";
        strClass += "/// &lt;/summary&gt;" + "<br />";
        strClass += "public static int Insert" + className + "(" + "<br />";
        foreach (varPairs s in vpList)
        {
            strClass += s.Type + " " + s.PubVar.ToLower() + ", ";
        }
        char[] trimChars = { ',', ' ' };
        string saveTemp = strClass.TrimEnd(trimChars); //remove last ","
        strClass = saveTemp;
        strClass += ")" + "<br />";
        strClass += "	{" + "<br />";
        strClass += className + " obj = new " + className + "();" + "<br />";
        strClass += "       <br />";
        foreach (varPairs s in vpList)
        {
            strClass += "        obj." + s.PubVar + " = " + s.PubVar.ToLower() + ";" + "<br />";
        }
        strClass += "       int ret = obj.Save(obj.ID);" + "<br />";
        strClass += "       return ret;" + "<br />";
        strClass += "   }" + "<br /><br />";
    }

    private string GetDvwEditTemplate(varPairs s)
    {
        string ret = "";
        switch (s.Type.ToLower())
        {
            case "int":
            case "double":
            case "decimal":
            case "string":
                ret = "&lt;asp:TextBox ID=&quottxt" + s.PubVar + "&quot runat=&quotserver&quot Text='&lt;%# Bind(&quot" + s.PubVar + "&quot) %&gt;' Width=&quot;20%&quot;&gt;&lt;/asp:TextBox&gt;" + "<br />";
                break;

            case "bool":
                ret = "&lt;asp:CheckBox ID=&quotchk" + s.PubVar + "&quot runat=&quotserver&quot IsChecked='&lt;%# Bind(&quot" + s.PubVar + "&quot) %&gt;' Width=&quot;20%&quot;&gt;&lt;/asp:CheckBox&gt;" + "<br />";
                break;

             default:
                ret = "&lt;asp:TextBox ID=&quottxt" + s.PubVar + "&quot runat=&quotserver&quot Text='&lt;%# Bind(&quot" + s.PubVar + "&quot) %&gt;' Width=&quot;20%&quot;&gt;&lt;/asp:TextBox&gt;" + "<br />";
                break;
       }

        return ret;
    }

    private string GetSaveParameterList(varPairs s)
    {
        string ret = "";
        switch (s.Type.ToLower())
        {
            case "int":
                ret = "&lt;asp:Parameter name=&quot" + s.PubVar + "&quot Type=&quotInt32&quot /&gt;" + "<br />";
                break;

            case "double":
                ret = "&lt;asp:Parameter name=&quot" + s.PubVar + "&quot Type=&quotDouble&quot /&gt;" + "<br />";
                break;

            case "decimal":
                ret = "&lt;asp:Parameter name=&quot" + s.PubVar + "&quot Type=&quotDecimal&quot /&gt;" + "<br />";
                break;

            case "string":
                ret = "&lt;asp:Parameter name=&quot" + s.PubVar + "&quot Type=&quotString&quot /&gt;" + "<br />";
                break;

            case "bool":
                ret = "&lt;asp:Parameter name=&quot" + s.PubVar + "&quot Type=&quotBoolean&quot /&gt;" + "<br />";
                break;

             default:
                ret = "&lt;asp:Parameter name=&quot" + s.PubVar + "&quot Type=&quotString&quot /&gt;" + "<br />";
                break;
       }

        return ret;
    }

    private string GetSQLParameterList(varPairs s)
    {
        string ret = "";
        switch (s.Type.ToLower())
        {
            case "int":
                ret = "[" + s.PubVar + "] [int] NULL";
                break;

            case "double":
                ret = "[" + s.PubVar + "] [bigint] NULL";
                break;

            case "decimal":
                ret = "[" + s.PubVar + "] [decimal] (10,2) NULL";
                break;

            case "string":
                ret = "[" + s.PubVar + "] [nvarchar](254) COLLATE SQL_Latin1_General_CP1_CI_AS NULL /* use nvarchar(max) if more then 4000 bytes */";
                break;

            case "bool":
                ret = "[" + s.PubVar + "] [bit] NULL";
                break;

            case "datetime":
                ret = "[" + s.PubVar + "] [DateTime] NULL";
                break;

            default:
                ret = "[" + s.PubVar + "] [nvarchar](254) COLLATE SQL_Latin1_General_CP1_CI_AS NULL /* use nvarch(max) if more then 4000 bytes */";
                break;
        }

        return ret;
    }

   private string GetXLConvert(varPairs s)
    {
        string ret = "";
        switch (s.Type.ToLower())
        {
            case "string": 
                ret = "r[&quot;" + s.PubVar + "&quot;].ToString()";
                break;

            case "int":
                ret = "Convert.ToInt32(r[&quot;" + s.PubVar + "&quot;].ToString())";
                break;

            case "decimal": ret = "Convert.ToDecimal(r[&quot;" + s.PubVar + "&quot;].ToString())";
                break;

            case "double": ret = "Convert.ToDouble(r[&quot;" + s.PubVar + "&quot;].ToString())";
                break;

            case "bool": ret = "Convert.ToBoolean(r[&quot;" + s.PubVar + "&quot;].ToString())";
                break;

            case "datetime": ret = "Convert.ToDateTime(r[&quot;" + s.PubVar + "&quot;].ToString())";
                break;

            default: ret = "r[&quot;" + s.PubVar + "&quot;].ToString()";
                break;

        }

        return ret;
    }

    private string TypeDefault(string type)
    {
        string ret = "";
        switch (type.ToLower())
        {
            case "string": ret = "&quot;n/a&quot;";
                break;

            case "datetime": ret = "Convert.ToDateTime(&quot;1/1/1900&quot;)";
                break;

            case "bool": ret = "false";
                break;

            default: ret = "0";
                break;
        }

        return ret;
    }

    private string DBColumnConvert(string type, string pubVar)
    {
        string ret = "";
        switch (type.ToLower())
        {
            case "string": ret = "dr[&quot;" + pubVar + "&quot;].ToString()";
                break;

            case "int": ret = "Convert.ToInt32(dr[&quot;" + pubVar + "&quot;].ToString())";
                break;

            case "decimal": ret = "Convert.ToDecimal(dr[&quot;" + pubVar + "&quot;].ToString())";
                break;

            case "double": ret = "Convert.ToDouble(dr[&quot;" + pubVar + "&quot;].ToString())";
                break;

            case "bool": ret = "Convert.ToBoolean(dr[&quot;" + pubVar + "&quot;].ToString())";
                break;

            case "datetime": ret = "Convert.ToDateTime(dr[&quot;" + pubVar + "&quot;].ToString())";
                break;

            default: ret = "dr[&quot;" + pubVar + "&quot;].ToString()";
                break;                

        }

        return ret;
    }

    private static string[] _codebehind = new string[] 
    {
    "<br />",
    "using System;",
    "using System.Data;",
    "using System.Configuration;",
    "using System.Collections;",
    "using System.Security;",
    "using System.Web;",
    "using System.Web.Security;",
    "using System.Web.UI;",
    "using System.Web.UI.WebControls;",
    "using System.Web.UI.WebControls.WebParts;",
    "using System.Web.UI.HtmlControls;",
    "using RM.TheWebSite;",
    "using RM.TheWebSite.ExceptionHandler;",
    "using AdminCart;",
    "<br /><br />",
    "namespace RM.TheWebSite.UI.Admin",
    "{",
    "public partial class Manage#Name : BasePage",
     "{<br />",
    "   protected void Page_Load(object sender, EventArgs e)",
    "   {",            
    "   }",
    "#br",
    "   private void Deselect#Name()",
    "   {",
    "       gvw#Name.SelectedIndex = -1;",
    "       gvw#Name.DataBind();",
    "       dvw#Name.ChangeMode(DetailsViewMode.Insert);",
    "   }",
    "#br",
    "   protected void gvw#Name_SelectedIndexChanged(object sender, EventArgs e)",
    "   {",
    "       dvw#Name.ChangeMode(DetailsViewMode.Edit);",
    "   }",
    "#br",
    "   protected void gvw#Name_RowDeleted(object sender, GridViewDeletedEventArgs e)",
    "   {",
    "       Deselect#Name();",
    "   }",
    "#br",
    "   protected void gvw#Name_RowCommand(object sender, GridViewCommandEventArgs e)",
    "   {",
    "   }",
    "#br",
    "   protected void gvw#Name_RowCreated(object sender, GridViewRowEventArgs e)",
    "   {",
    "   }",
    "#br",
    "   protected void dvw#Name_ItemInserted(object sender, DetailsViewInsertedEventArgs e)",
    "   {",
    "       Deselect#Name();",
    "   }",
    "#br",
    "   protected void dvw#Name_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)",
    "   {",
    "       Deselect#Name();",
    "   }",
    "#br",
    "   protected void dvw#Name_ItemCommand(object sender, DetailsViewCommandEventArgs e)",
    "   {",
    "       if (e.CommandName == &quot;Cancel&quot;)",
    "           Deselect#Name();",
    "   }",
    "#br",
    "   protected void dvw#Name_ItemCreated(object sender, EventArgs e)",
    "   {",
    "       foreach (Control ctl in dvw#Name.Rows[dvw#Name.Rows.Count - 1].Controls[0].Controls)",
    "       {",
    "           if (ctl is LinkButton)",
    "           {",
    "               LinkButton lnk = ctl as LinkButton;",
    "               if (lnk.CommandName == &quot;Insert&quot; || lnk.CommandName == &quot;Update&quot;)",
    "                   lnk.ValidationGroup = &quot;#Name&quot;;",
    "           }",
    "       }",
    "   }",
    "}",
    "}"

    };

    private static string[] _initMarkup = new string[] 
    {
        "<br />",
       "&lt;div class=&quot;sectiontitle&quot;&gt;Manage #Name&lt;/div&gt;",
       "&lt;p&gt;&lt;/p&gt;",
       "&lt;asp:GridView ID=&quot;gvw#Name&quot; runat=&quot;server&quot; ",
       "AutoGenerateColumns=&quot;False&quot; ",
       "DataSourceID=&quot;obj#Name&quot; ",
       "Width=&quot;100%&quot; ",
       "DataKeyNames=&quot;ID&quot; ",
       "OnRowCreated=&quot;gvw#Name_RowCreated&quot; ",
       "OnSelectedIndexChanged=&quot;gvw#Name_SelectedIndexChanged&quot; ",
       "OnRowCommand=&quot;gvw#Name_RowCommand&quot;&gt; "
    };

    private static string[] _initClass = new string[] 
    {
       "<br />",
        "using System;",
        "using System.Collections.Generic;",
        "using System.Configuration;",
        "using System.Web;",
        "using System.Data;",
        "using System.Data.SqlClient;",
       "<br />",
       "/// &lt;summary&gt;",
        "/// Summary description for #Name",
        "/// &lt;/summary&gt;",
        "public class #Name",
        "{",
            "#region Private/Public Vars"

    };

}
