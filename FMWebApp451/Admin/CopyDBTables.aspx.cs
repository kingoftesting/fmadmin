using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using AdminCart;
using FM2015.Helpers;

public partial class site_Admin_CopyDBTables : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Certnet.Cart tempcart = new Certnet.Cart();
            tempcart.Load(Session["cart"]);
            int customerID = tempcart.SiteCustomer.CustomerID;

            try
            {
                if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "CUSTOMERSERVICE")))
                { }
                else { throw new SecurityException("You do not have the apppropriate creditentials to complete this task."); }
            }
            catch (SecurityException ex)
            {
                string msg = "Error in CopyDBTable" + Environment.NewLine;
                msg += "User did not have permission to access page" + Environment.NewLine;
                msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                dbErrorLogging.LogError(msg, ex);
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void btnCopyDB_Click(object sender, EventArgs e)
    {
        string sourcestr = "";
        string sql = "";
        SqlParameter[] mySqlParameters = null;
        SqlDataReader dr = null;


        sql = @"
        SELECT * 
        FROM Products";

        sourcestr = "SERVER=10.10.0.20;UID=Site;PWD=pass;DATABASE=SiteV2;Application Name=FaceMasterWebTest";
        dr = DBUtil.FillDataReader(sql, sourcestr);

        while (dr.Read())
        {
            Product p = new Product();
            p.ProductID = 0;
            p.Sku = dr["Sku"].ToString();
            p.Name = dr["Name"].ToString();
            p.Price = Convert.ToDecimal(dr["Price"]);
            p.SalePrice = Convert.ToDecimal(dr["SalePrice"]);
            p.Taxable = Convert.ToBoolean(dr["Taxable"]);
            p.Weight = Convert.ToDecimal(dr["Weight"]);
            p.PerUnitShipping = Convert.ToDecimal(dr["PerUnitShipping"]);
            p.ShortDescription = dr["ShortDescription"].ToString();
            p.Description = dr["Description"].ToString();
            p.CartImage = dr["CartImage"].ToString();
            p.ThumbnailImage = dr["ThumbNailImage"].ToString();
            p.IconImage = dr["IconImage"].ToString();
            p.DetailsImage = dr["DetailsImage"].ToString();
            p.ZoomImage = dr["ZoomImage"].ToString();
            p.OriginalImage = dr["OriginalImage"].ToString();
            p.Stock = Convert.ToInt32(dr["Stock"]);
            p.Enabled = Convert.ToBoolean(dr["Enabled"]);
            p.CSOnly = Convert.ToBoolean(dr["CSOnly"]);
            p.Sort = Convert.ToInt32(dr["Sort"]);
            p.InStock = Convert.ToBoolean(dr["InStock"]);
            p.OutOfStockMessage = dr["OutOfStockMessage"].ToString();
            sql = @"
                INSERT Products(Sku, Name, Price, SalePrice, Taxable, Weight, PerUnitShipping, 
                              ShortDescription, Description, CartImage, ThumbNailImage, IconImage, DetailsImage, 
                              ZoomImage, OriginalImage, Stock, Enabled, CSOnly, Sort, InStock, OutOfStockMessage) 
                VALUES(@Sku, @Name, @Price, @SalePrice, @Taxable, @Weight, @PerUnitShipping, 
                              @ShortDescription, @Description, @CartImage, @ThumbNailImage, @IconImage, @DetailsImage, 
                              @ZoomImage, @OriginalImage, @Stock, @Enabled, @CSOnly, @Sort, @InStock, @OutOfStockMessage)";

            mySqlParameters = DBUtil.BuildParametersFrom(sql, p.Sku, p.Name, p.Price, p.SalePrice, p.Taxable, p.Weight, p.PerUnitShipping, p.ShortDescription, p.Description, p.CartImage, p.ThumbnailImage, p.IconImage, p.DetailsImage, p.ZoomImage, p.OriginalImage, p.Stock, p.Enabled, p.CSOnly, p.Sort, p.InStock, p.OutOfStockMessage);
            DBUtil.Exec(sql, mySqlParameters);
        }

        if (dr != null) { dr.Close(); }
        lblDone.Visible = true;
    
    }

}
