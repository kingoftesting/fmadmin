<%@ Page Language="C#" Theme="Admin" Title="Coupon Mgt" MasterPageFile="MasterPage3_Blank.master" AutoEventWireup="true" Inherits="Admin_Coupons" Codebehind="Coupons.aspx.cs" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" Runat="Server">
   <div class="sectiontitle">FaceMaster Coupon Management</div>
   <p></p>
    
   <asp:GridView 
    ID="gridcoupon"
    SkinID="Admin"
    runat="server"
    AllowSorting="true"
    AutoGenerateColumns="false">
	<HeaderStyle BackColor="blue" ForeColor="White" Font-Bold="True" HorizontalAlign="Center" Font-Size="12px" />
    <AlternatingRowStyle BackColor="blue" />
	<PagerStyle HorizontalAlign="Center" ForeColor="Blue" BackColor="White" Font-Bold="True" />
      <Columns>
         <asp:BoundField HeaderText="Used#" DataField="CouponUsageInstances" SortExpression="CouponUseageInstances" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Code" DataField="CouponCode" SortExpression="CouponCode" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Description" DataField="CouponDescription" SortExpression="CouponDescription" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="StartDate" DataField="CouponStartDate" SortExpression="CouponStartDate" DataFormatString="{0:MM/dd/yy h:mm tt}" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="EndDate" DataField="CouponEndDate" SortExpression="CouponEndDate" DataFormatString="{0:MM/dd/yy h:mm tt}" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Constraints" DataField="CouponSpecialRequirements" />
         <asp:BoundField HeaderText="OrderMin" DataField="CouponOrderMinimum" SortExpression="CouponOrderMinimum" DataFormatString="{0:C}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Amount" DataField="CouponAmount" SortExpression="CouponAmount" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Type" DataField="CouponType" SortExpression="CouponType" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Notes" DataField="CouponNotes" SortExpression="CouponNotes" HeaderStyle-HorizontalAlign="Center"/>
      </Columns>
      <EmptyDataTemplate><b>No Coupons Found</b></EmptyDataTemplate>
   </asp:GridView>
   
</asp:Content>
