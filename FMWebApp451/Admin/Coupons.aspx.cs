using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FM2015.Helpers;

public partial class Admin_Coupons : AdminBasePage //System.Web.UI.Page
{
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "DESC"; }
        set { ViewState["SortDirection"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = @"
        select top 50 *
        from coupons
        order by couponenddate desc";

        gridcoupon.DataSource = DBUtil.FillDataSet(sql, "coupons");
        gridcoupon.DataBind();
    }

    protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataView dv = Session["grvDataSource"] as DataView;

        if (dv != null)
        {

            string m_SortDirection = GetSortDirection();
            dv.Sort = e.SortExpression + " " + m_SortDirection;

            gridcoupon.DataSource = dv;
            gridcoupon.DataBind();
        }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;

            case "DESC":

                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
}
