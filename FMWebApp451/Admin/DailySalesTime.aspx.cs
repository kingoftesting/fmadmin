using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Certnet;
using FM2015.Helpers;

public partial class Admin_DailySalesTime : AdminBasePage //System.Web.UI.Page
{

    Cart cart;
    DataView websales = null;

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "DESC"; }
        set { ViewState["SortDirection"] = value; }
    }

    void Page_Init(Object sender, EventArgs e)
    {
        cart = new Cart();
        cart.Load(Session["cart"]);
        websales = new DataView(MakeWebSalesTable());
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
            {
                int customerID = cart.SiteCustomer.CustomerID;
                try
                {
                    if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "REPORTS")))
                    { }
                    else
                    {
                        throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
                    }
                }
                catch (SecurityException ex)
                {
                    string msg = "Error in DailySalesTime" + Environment.NewLine;
                    msg += "User did not have permission to access page" + Environment.NewLine;
                    msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                    FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                    Response.Redirect("../Login.aspx");
                }


                DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                23, 59, 59);

                //#1 = From: Date; #2 = To: Date 
                RDatePicker2.SelectedDate = time1.ToShortDateString();
                RDatePicker1.SelectedDate = time1.Subtract(new TimeSpan(0, 23, 59, 59)).ToShortDateString();
                
                btnGetCSV.Visible = false; //don't show CSV button until there's data displayed

            }
        }

    private void BindWebSales()
    {
        FillWebSales();
        websales.Sort = "DATE" + " " + "ASC";
        gvwDataList.DataSource = websales;
        Session["grvDataSource"] = websales; //save for later; used to Sort, CSV download, Email
        gvwDataList.DataBind();

        if (websales.Table.Rows.Count == 0) //hide Email & CSV buttons if empty table
        { btnGetCSV.Visible = false; }
        else
        {
            string FromDate = "";
            if (!string.IsNullOrEmpty(RDatePicker1.SelectedDate)) { FromDate = RDatePicker1.SelectedDate + " 12:00AM"; }
            else { FromDate = "1/1/1900"; }

            btnGetCSV.Visible = true;
        }
    }

    protected void btnListByDate_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            BindWebSales();
        }
    }

    protected void lnkbtnYesterday_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                0, 0, 0);

            time1 = time1.AddDays(-1);

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.ToShortDateString();
            RDatePicker1.SelectedDate = time1.ToShortDateString();

            BindWebSales();
        }
    }

    protected void lnkbtn7Day_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                0, 0, 0);

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.ToShortDateString();
            RDatePicker1.SelectedDate = time1.AddDays(-6).ToShortDateString();

            BindWebSales();
        }
    }

    protected void btnCSV_Click(object sender, EventArgs e)
    {
        //FillWebSales();
        websales = Session["grvDataSource"] as DataView;
        websales.Sort = "Date DESC";
        //Helpers.SendCSV(websales);
        Utilities.FMHelpers.SendCSV(websales);
    }

    protected void FillWebSales()
    {

        DateTime toDate = Convert.ToDateTime(RDatePicker2.SelectedDate);
        toDate = toDate.AddDays(1);

        string FromDate = RDatePicker1.SelectedDate;
        string ToDate = toDate.ToShortDateString();

        websales = FindAllWebSales(websales, FromDate, ToDate);
    }

    protected DataView FindAllWebSales(DataView websales, string fromDate, string toDate)
    {
        SqlDataReader dr = null;

        int SaulID = 7041;
        int DanID = 13625;
        int sourceID = 0;

        int FMOrderID = 0;
        bool FMbyPhone = false;
        bool FMApproved = false;
        double Total = 0;
        double Revenue = 0;
        double Shipping = 0;
        double Tax = 0;
        double Coupon = 0;
        double Adjust = 0;
        double TotalRevenue = 0;
        double WebRevenue = 0;
        double PhoneRevenue = 0;
        double SaulRevenue = 0;
        double DanRevenue = 0;
        double SumTotalRevenue = 0;
        double SumWebRevenue = 0;
        double SumPhoneRevenue = 0;
        double SumSaulRevenue = 0;
        double SumDanRevenue = 0;
        DateTime enddate = Convert.ToDateTime(toDate);
        DateTime startdate = Convert.ToDateTime(fromDate);
        TimeSpan n = enddate - startdate;
        int numdays = n.Days;

        for (int i = 0; i < numdays; ++i)
        {
            fromDate = startdate.ToShortDateString();
            toDate = startdate.AddDays(1).ToShortDateString();
            string sql = @" 
                SELECT TOP 2000 * 
                FROM Orders 
                WHERE OrderDate BETWEEN @FromDate AND @ToDate 
                ORDER BY OrderID ASC 
                ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, fromDate, toDate);
            dr = DBUtil.FillDataReader(sql, mySqlParameters);

            //new day! start the counts all over
            Total = 0;
            Revenue = 0;
            Adjust = 0;
            Coupon = 0;
            TotalRevenue = 0;
            WebRevenue = 0;
            PhoneRevenue = 0;
            SaulRevenue = 0;
            DanRevenue = 0;

            while (dr.Read())
            {

                FMOrderID = Convert.ToInt32(dr["OrderID"]);


                FMbyPhone = false;
                if (dr["Phone"] == DBNull.Value) { FMbyPhone = false; }
                else if (dr["Phone"].ToString() == "1") { FMbyPhone = true; }

                FMApproved = AdminCart.OrderAction.GetFMOrderApproved(FMOrderID);
                bool canceled = false;
                string actiontype = AdminCart.OrderAction.GetOrderAction(FMOrderID);
                if (actiontype.ToUpper() == "CANCEL") { canceled = true; }
                if (FMApproved && !canceled)
                {
                    Total = Convert.ToDouble(dr["Total"]);
                    Tax = Convert.ToDouble(dr["TotalTax"]);
                    Shipping = Convert.ToDouble(dr["TotalShipping"]);
                    Adjust = Convert.ToDouble(dr["Adjust"]);
                    Coupon = Convert.ToDouble(dr["TotalCoupon"]);
                    Revenue = Total - Tax - Shipping;
                    TotalRevenue = TotalRevenue + Revenue;
                    if (FMbyPhone)
                    { PhoneRevenue = PhoneRevenue + Revenue; }
                    else
                    { WebRevenue = WebRevenue + Revenue; }
                    sourceID = Convert.ToInt32(dr["SourceID"]);
                    if (sourceID == SaulID)
                    { SaulRevenue = SaulRevenue + Revenue; }
                    if (sourceID == DanID)
                    { DanRevenue = DanRevenue + Revenue; }
                }
            }

            DataRow newrow;
            newrow = websales.Table.NewRow();
            newrow["Date"] = startdate;
            newrow["NetRevenue"] = TotalRevenue;
            newrow["NetPhoneRevenue"] = PhoneRevenue;
            newrow["NetWebRevenue"] = WebRevenue;
            newrow["SaulRevenue"] = SaulRevenue;
            newrow["DanRevenue"] = DanRevenue;
            websales.Table.Rows.Add(newrow);

            SumTotalRevenue = SumTotalRevenue + TotalRevenue;
            SumPhoneRevenue = SumPhoneRevenue + PhoneRevenue;
            SumWebRevenue = SumWebRevenue + WebRevenue;
            SumSaulRevenue = SumSaulRevenue + SaulRevenue;
            SumDanRevenue = SumDanRevenue + DanRevenue;

            if (dr != null) { dr.Close(); }
            startdate = startdate.AddDays(1);
        }

        DataRow sumrow;
        sumrow = websales.Table.NewRow();
        sumrow["Date"] = DBNull.Value;
        sumrow["NetRevenue"] = SumTotalRevenue;
        sumrow["NetPhoneRevenue"] = SumPhoneRevenue;
        sumrow["NetWebRevenue"] = SumWebRevenue;
        sumrow["SaulRevenue"] = SumSaulRevenue;
        sumrow["DanRevenue"] = SumDanRevenue;
        websales.Table.Rows.Add(sumrow);


        return websales;
    }

    private DataTable MakeWebSalesTable()
    {
        // Create a new DataTable titled 'WebSales'
        DataTable WebSalesTable = new DataTable("WebSales");

        // Add three column objects to the table.
        DataColumn idColumn = new DataColumn();
        idColumn.DataType = System.Type.GetType("System.Int32");
        idColumn.ColumnName = "id";
        idColumn.AutoIncrement = true;
        WebSalesTable.Columns.Add(idColumn);

        DataColumn DateColumn = new DataColumn();
        DateColumn.DataType = System.Type.GetType("System.DateTime");
        DateColumn.ColumnName = "Date";
        DateColumn.DefaultValue = "1/1/1900";
        WebSalesTable.Columns.Add(DateColumn);

        DataColumn RevColumn = new DataColumn();
        RevColumn.DataType = System.Type.GetType("System.Double");
        RevColumn.ColumnName = "NetRevenue";
        RevColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(RevColumn);

        DataColumn WebRevColumn = new DataColumn();
        WebRevColumn.DataType = System.Type.GetType("System.Double");
        WebRevColumn.ColumnName = "NetWebRevenue";
        WebRevColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(WebRevColumn);

        DataColumn PhoneRevColumn = new DataColumn();
        PhoneRevColumn.DataType = System.Type.GetType("System.Double");
        PhoneRevColumn.ColumnName = "NetPhoneRevenue";
        PhoneRevColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(PhoneRevColumn);

        DataColumn SaulColumn = new DataColumn();
        SaulColumn.DataType = System.Type.GetType("System.Double");
        SaulColumn.ColumnName = "SaulRevenue";
        SaulColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(SaulColumn);

        DataColumn DanColumn = new DataColumn();
        DanColumn.DataType = System.Type.GetType("System.Double");
        DanColumn.ColumnName = "DanRevenue";
        DanColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(DanColumn);

        // Create an array for DataColumn objects.
        DataColumn[] keys = new DataColumn[1];
        keys[0] = idColumn;
        WebSalesTable.PrimaryKey = keys;

        // Once a table has been created, use the 
        /* NewRow to create a DataRow.
        DataRow row;
        row = WebSalesTable.NewRow();

        // Then add the new row to the collection.
        row["SKU"] = FM-5000;
        row["Units"] = 0;
        row["Revenue"] = 0;
        row["WebUnits"] = 0;
        row["WebRevenue"] = 0;
        row["PhoneUnits"] = 0;
        row["PhoneRevenue"] = 0;
        WebSales.Rows.Add(row);
        */


        // Return the new DataTable.
        return WebSalesTable;
    }

    protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataView dv = Session["grvDataSource"] as DataView;

        if (dv != null)
        {

            string m_SortDirection = GetSortDirection();
            dv.Sort = e.SortExpression + " " + m_SortDirection;

            gvwDataList.DataSource = dv;
            gvwDataList.DataBind();
        }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;

            case "DESC":

                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    public void gvwDataList_PageIndexChanging(object source, GridViewPageEventArgs e)
    {
        gvwDataList.PageIndex = e.NewPageIndex;
        //gvwDataList.DataBind();
        BindWebSales();
    }

    public void gvwDataList_PageIndexChanged(object source, EventArgs e)
    {
    }

    protected void gvwDataList_RowEditing(object sender, EventArgs e)
    {
    }

}
