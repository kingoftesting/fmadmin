<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" Inherits="FaceMaster.Admin.Default" Title="Main" Codebehind="Default.aspx.cs" %>

<asp:Content ID="MainContent" runat="server" ContentPlaceHolderID="MainContent">

<asp:Panel runat="server" ID="panAnon">
<div class="sectionsubtitle">You Do Not Have The Appropriate Credentials. Please Sign-In Again.</div>
<hr />
</asp:Panel>

<div style="width:100%">

<asp:Panel runat="server" ID="panCustService">

    <div class="sectionsubtitle">Actions for Customer Service</div>


     <div style="float:right; margin-left:10px; padding-left:10px; border: solid 2px Blue; ">
     <asp:Panel ID="pnlUnshippedReport" runat="server" Visible="true">
         <p>
             <span style="text-align: center; font-size: 18px; font-weight: bold; padding: 10px">
                 <asp:Label ID="lblUnshippedNum" Text=" Orders w/o Tracking#" runat="server"></asp:Label>
             </span>
             <br />
             <span style="text-align: center; font-size: 14px; font-weight: bold; padding: 10px">
                 <asp:Label ID="lblUnshippedDelay" Text=" Days = Longest Order Delay" runat="server"></asp:Label>
             </span>
             <br />
             <span style="text-align: center; padding: 10px">
                 <a href="UnshippedOrders3.aspx"><strong>Click Here to View These Orders</strong></a>
             </span>
         </p>
         <hr />
         <p>
             <span style="text-align: center; font-size: 18px; font-weight: bold; padding: 10px">
                 <asp:Label ID="lblUnshippedSerNum" Text=" Device Orders w/o Serial#" runat="server"></asp:Label>
             </span>
             <br />
             <span style="text-align: center; padding: 10px">
                 <a href="UnshippedSerOrders3.aspx"><strong>Click Here to View These Orders</strong></a>
             </span>
         </p>

    </asp:Panel>
    </div>
    <p>&nbsp;</p>
    <div class = "uladminlist">
    <ul>
       <li><a href="ManageUsers.aspx"><strong>Manage Customers</strong></a>: Find/Create Orders, Subscriptions, CERs, Details</li>
       <li>---------</li>
       <li><a href="ManageOrders.aspx"><strong>Find Existing Orders</strong></a>: Find, review, & modify existing orders.</li>
       <li>---------</li>
       <li><a href="https://east.exch022.serverdata.net/owa/" target="_blank"><strong>Email</strong></a>: Customer service email here (facemaster@slccompanies.com, Hello101)</li>
    </ul>
    </div>

    <hr />

</asp:Panel>

<asp:Panel runat="server" ID="panReports">
    <div class="sectionsubtitle">Actions for Report Users</div>

    <div class = "uladminlist">
    <ul>
        <li><asp:HyperLink ID="lnkPhoneSales2" runat="server" NavigateUrl="~/report/index">DSR: Detailed Sales by Date Range</asp:HyperLink></li>
        <li><asp:HyperLink ID="lnkMonthSales" runat="server" NavigateUrl="MonthSalesReview3.aspx">DSR2: Daily Sales by Month, Year</asp:HyperLink></li>
        <li>---------</li>
        <li><asp:HyperLink ID="lnkMultiPaySummary" runat="server" NavigateUrl="~/report/MultiPayRevenueByOrder">MP: Summary of MultiPay orders & statistics</asp:HyperLink></li>
        <li><asp:HyperLink ID="lnkClubSummary" runat="server" NavigateUrl="~/report/ClubRevenueByOrder">Club: Summary of Club orders & statistics</asp:HyperLink></li>
        <li><asp:HyperLink ID="lnkMultiPayStatus" runat="server" NavigateUrl="~/report/multipaystatus">MP2: Update on Multi-Pay & Club orders and payments made</asp:HyperLink></li>
        <li>---------</li>
        <li><asp:HyperLink ID="lnkUnShippedOrders2" runat="server" NavigateUrl="UnshippedOrders3.aspx">30 Day Look-Back for Unshipped Orders (no tracking #)</asp:HyperLink></li>
        <li><asp:HyperLink ID="lnkUnShippedSerOrders" runat="server" NavigateUrl="UnshippedSerOrders3.aspx">30 Day Look-Back for Unshipped Orders (no Scan)</asp:HyperLink></li>
        <li>---------</li>
        <li><asp:HyperLink ID="lnkPartNumbers" runat="server" NavigateUrl="PartNumbers.aspx">List of active part numbers</asp:HyperLink></li>
    </ul>
    </div>

    <hr />
</asp:Panel>

<asp:Panel runat="server" ID="panAdmin">
    <div class="sectionsubtitle">Actions for Administrators</div>

    <div class = "uladminlist">
    <ul>
        <li><asp:HyperLink ID="lnkErrLog" runat="server" NavigateUrl="RptListErrLog.aspx">Database Error Log</asp:HyperLink></li>
        <li>---------</li>
        <li><asp:HyperLink ID="lnkCharts" runat="server" Text="Charts" NavigateUrl="~/CER/Charts.aspx"  /></li>
        <li>---------</li>
        <li><a href="ManagePages.aspx">Manage Custom Page Content</a>: Add/Modify key Searchwords, Add/Modify customized page content</li>
        <li><a href="ManageFAQs.aspx">Manage FAQs</a>: Add/Modify FAQs to the website</li>
        <li><a href="RptRoles.aspx">Manage Roles</a>: Review/Add/Modify Admin Roles</li>
        <li><a href="ManageFMPOSales.aspx">Add/Modify PO Sales</a></li>
       <li>---------</li>
       <li><a href="../adminMVC/" ><strong>AdminMVC</strong></a></li>

    </ul>
    </div>

    <hr />
</asp:Panel>

<asp:Panel runat="server" ID="panStoreKeeper">
    <div class="sectionsubtitle">Actions for Store Keepers</div>

    <div class = "uladminlist">
    <ul>
       <li><a href="ManageProducts.aspx">Manage Store Catalog</a>: add/edit/remove store departments, products,
       shipping methods and order statuses.</li>
       <li><a href="ManageCoupons.aspx">Manage Coupons</a>: add/edit/remove coupons</li>
       <li><a href="ManageSiteSettings.aspx">Manage SiteSettings</a>: add/edit/remove Free Shipping, Specials</li>
    </ul>
    </div>

    <hr />
</asp:Panel>

<asp:Panel runat="server" ID="panMarketing">
    <div>
        <div class="sectionsubtitle">Actions for Marketers</div>
        <div class="uladminlist">
            <ul>
                <li><asp:HyperLink ID="lnkSales" runat="server" NavigateUrl="WebPhoneCER6.aspx">DSR: Detailed Sales by Date Range</asp:HyperLink></li>
                <li>---------</li>
                <li><asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/report/MultiPayRevenueByOrder">Summary of MultiPay orders & statistics</asp:HyperLink></li>
                <li><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/report/multipaystatus">Update on Multi-Pay orders & payments made</asp:HyperLink></li>
                <li>---------</li>
                <li><a href="ManageOrders.aspx">Find Existing Orders</a>: Find, review, & modify existing orders.</li>
                <li>---------</li>
                <li><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="MonthSalesReview3.aspx">Daily Sales by Month, Year</asp:HyperLink></li>
           </ul>
        </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>

    <hr />
</asp:Panel>


<asp:Panel runat="server" ID="panResources">
    <div class="sectionsubtitle">Resources for Administrators</div>

    <div class = "uladminlist">
    <ul>
        <asp:Panel ID="pnlFMResources" runat="server" Visible="false">
            <li><asp:HyperLink ID="lnkImportHDI" runat="server" NavigateUrl="HDI-Merge.aspx">Import HDI orders</asp:HyperLink>:&nbsp;Pulls HDI orders from FMInfomercial into the Legacy System</li>
            <li>---------</li>
            <li><asp:HyperLink ID="lnkHDIReconcile" runat="server" NavigateUrl="HDI-Herb-Batch-Reconcile.aspx">Reconcile order data</asp:HyperLink>:&nbsp;Pulls data from Herb's spreadsheet, db.Infomercial, db.Site2</li>
            <li>---------</li>
            <li><a href="https://www.google.com/analytics/web/#report/visitors-overview/a1560367w2739879p2793529/" target="_blank">Google Analytics: </a> &nbsp;(facemastermail@gmail.com; kingoftesting) </li>
            <li><a href="https://adwords.google.com/cm/CampaignMgmt?__u=5619506748&__c=2230475508&stylePrefOverride=2" target="_blank">Google AdWords: </a> &nbsp;(facemastermail@gmail.com; kingoftesting) </li>
            <li><a href="https://console.aws.amazon.com/console/home" target="_blank">Amazon S3: </a> &nbsp;(facemaster@slccompanies.com; hello101)&nbsp; update FM videos, large files, etc. </li>
            <li><a href="https://mail.google.com/mail/?shva=1#inbox" target="_blank">Google gmail (facemaster@gmail.com): </a> &nbsp;(facemastermail@gmail.com; kingoftesting) </li>
            <li><a href="https://login.mailchimp.com/" target="_blank">MailChimp Email List: </a> &nbsp;(facemaster@slccompanies.com; Hello101!) </li>
            <li><asp:HyperLink ID="lnkFMTestimonials" runat="server" NavigateUrl="FMTestimonialList.aspx">FM Testimonials, Before/After pics</asp:HyperLink></li>
        </asp:Panel>
        <asp:Panel ID="pnlCLResources" runat="server" Visible="false">
            <li>No CrystalLift Specific Resources, yet</li>
        </asp:Panel>
       <li><a href="Resources.aspx">Resources For Developers, QA </a> </li>
    </ul>
    </div>

    <hr />
</asp:Panel>

</div>

</asp:Content>