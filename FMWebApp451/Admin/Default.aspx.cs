using System;
using System.Data;
using System.Web;
using FMmetrics;

namespace FaceMaster.Admin
{

    public partial class Default : AdminBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool OKuser = false;
            CanUserProceed(new string[5] {"ADMIN", "CUSTOMERSERVICE", "REPORTS", "STORE", "MARKETING"} );

            int customerID = 0;
            if (Session["UserID"] != null) { customerID = Convert.ToInt32(Session["UserID"]); }

            HttpContext context = HttpContext.Current;
            //if ((Config.RedirectMode == "Test") || ((customerID != 0) && (context.Request.IsSecureConnection)))
            if ((AdminCart.Config.RedirectMode == "Test") || (customerID != 0))
            {
                panAdmin.Visible = (Convert.ToBoolean(Session["IsAdmin"]));
                if (!OKuser) { OKuser = panAdmin.Visible; }

                panCustService.Visible = (Convert.ToBoolean(Session["IsAdmin"])) || (Convert.ToBoolean(Session["IsCS"]));
                if (!OKuser) { OKuser = panCustService.Visible; }

                panReports.Visible = (Convert.ToBoolean(Session["IsAdmin"])) || (Convert.ToBoolean(Session["IsREPORTS"]));
                if (!OKuser) { OKuser = panReports.Visible; }

                panStoreKeeper.Visible = (Convert.ToBoolean(Session["IsAdmin"])) || (Convert.ToBoolean(Session["IsSTORE"]));
                if (!OKuser) { OKuser = panStoreKeeper.Visible; }

                panMarketing.Visible = (Convert.ToBoolean(Session["IsAdmin"])) || (Convert.ToBoolean(Session["IsMARKETING"]));
                if (!OKuser) { OKuser = panMarketing.Visible; }

                panResources.Visible = (Convert.ToBoolean(Session["IsAdmin"]));
                if (!OKuser) { OKuser = panResources.Visible; }

                panAnon.Visible = !OKuser; //show non-credentialed user statement if OKUser is false
                if (!OKuser) { Response.Redirect("~/FMAccount/SignIn"); }
            }
            else { Response.Redirect("~/FMAccount/SignIn"); }

            Response.Redirect("~/adminMVC/index");

            switch (AdminCart.Config.appName.ToUpper())
            {
                case "FACEMASTER":
                    pnlFMResources.Visible = true;
                    break;

                case "CRYSTALIFT":
                    pnlCLResources.Visible = true;
                    break;

                default:
                    break;
            }

            //lblUnshippedNum.Text = "0" + lblUnshippedNum.Text;
            //lblUnshippedDelay.Text = "0" + lblUnshippedDelay.Text;

            DataSet ds = new DataSet();
            ds = Metrics.GetUnshippedOrders();            
            if (ds.Tables[0].Rows.Count == 0) 
            {
                lblUnshippedNum.Text = "0" + lblUnshippedNum.Text;
                lblUnshippedDelay.Text = "0" + lblUnshippedDelay.Text;
            }
            else 
            {
                lblUnshippedNum.Text = ds.Tables[0].Rows.Count.ToString() + lblUnshippedNum.Text;
                lblUnshippedDelay.Text = ds.Tables[0].Rows[0]["daysDelayed"].ToString() + lblUnshippedDelay.Text;
            }

            ds = Metrics.GetUnshippedSerOrders();
            if (ds.Tables[0].Rows.Count == 0)
            { lblUnshippedSerNum.Text = "0" + lblUnshippedSerNum.Text; }
            else
            { lblUnshippedSerNum.Text = ds.Tables[0].Rows.Count.ToString() + lblUnshippedSerNum.Text; }
        
        }


    }
}
