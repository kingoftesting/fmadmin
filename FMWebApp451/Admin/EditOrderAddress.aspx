<%@ Page Language="C#" Theme="Admin" MasterPageFile="~/Admin/MasterPage3_Blank.master" AutoEventWireup="true" Inherits="Admin_EditOrderAddress" Title="Edit Order Addresses" Codebehind="EditOrderAddress.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

   <div class="sectiontitle">
        Update Customer Address Information - CustomerID:&nbsp;
        <asp:Label id="Customer_NameID" runat="server"></asp:Label>
   </div>

    <div class="simpleClear"></div>
    <asp:Label id="Label1" runat="server"><strong>Billing Address:</strong></asp:Label>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label2" runat="server">Street:</asp:Label>
        <asp:textbox id="BillAddress_Street" runat="server"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator7" ControlToValidate="BillAddress_Street" ErrorMessage="*"
            Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label3" runat="server">Street2:</asp:Label>
        <asp:textbox id="BillAddress_Street2" runat="server"></asp:textbox>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label4" runat="server">City:</asp:Label>
        <asp:textbox id="BillAddress_City" runat="server"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator8" ControlToValidate="BillAddress_City" ErrorMessage="*"
            Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label5" runat="server">State:</asp:Label>
        <asp:textbox id="txtStateBill" Visible="False" Runat="server" MaxLength="255"></asp:textbox>
        <asp:dropdownlist id="selStateBill" runat="server" Visible="True"></asp:dropdownlist>
        <asp:requiredfieldvalidator id="rqdStateBill" Visible="False" ControlToValidate="txtStateBill" ErrorMessage="*"
            Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label6" runat="server">Zip:</asp:Label>
        <asp:textbox id="BillAddress_ZipPostal" runat="server" Width="100px"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator9" ControlToValidate="BillAddress_ZipPostal" ErrorMessage="*"
            Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label7" runat="server">Country:</asp:Label>
        <asp:dropdownlist id="selCountryBill" runat="server" 
            AppendDataBoundItems="true" OnSelectedIndexChanged="selCountryBill_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Text="United States" Value="US" Selected="false" />
            <asp:ListItem Text="Canada" Value="CA" Selected="false" />
            <asp:ListItem Text="--------------" Value="" Selected="false" />
        </asp:dropdownlist>
    </div>

    <div class="simpleClear"></div>
    <asp:Label id="Label8" runat="server"><strong>Shipping Address:</strong></asp:Label>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label9" runat="server">Street:</asp:Label>
        <asp:textbox id="ShipAddress_Street" runat="server"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator10" ControlToValidate="ShipAddress_Street" ErrorMessage="*"
            Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label10" runat="server">Street2:</asp:Label>
        <asp:textbox id="ShipAddress_Street2" runat="server"></asp:textbox>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label11" runat="server">City:</asp:Label>
        <asp:textbox id="ShipAddress_City" runat="server"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator11" ControlToValidate="ShipAddress_City" ErrorMessage="*"
            Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label12" runat="server">State:</asp:Label>
        <asp:textbox id="txtStateShip" Visible="False" Runat="server" MaxLength="255"></asp:textbox>
        <asp:dropdownlist id="selStateShip" runat="server" Visible="True">
        </asp:dropdownlist><asp:requiredfieldvalidator id="rqdStateShip" Visible="False" ControlToValidate="txtStateShip" ErrorMessage="*"
            Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label13" runat="server">Zip:</asp:Label>
        <asp:textbox id="ShipAddress_ZipPostal" runat="server" Width="100px"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator12" ControlToValidate="ShipAddress_ZipPostal" ErrorMessage="*"
            Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label14" runat="server">Country:</asp:Label>
        <asp:dropdownlist id="selCountryShip" runat="server" 
            AppendDataBoundItems="true" OnSelectedIndexChanged="selCountryShip_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Text="United States" Value="US" Selected="false" />
            <asp:ListItem Text="Canada" Value="CA" Selected="false" />
            <asp:ListItem Text="--------------" Value="" Selected="false" />
        </asp:dropdownlist>
    </div>

    <div class="simpleClear"></div>
    <br /><br />
    <asp:Button ID="btnUpdateAddr" runat="server" onclick="btnUpdateAddr_Click" Text="Update Addresses" />

    <asp:placeholder id="PlaceHolderMessage" runat="server" Visible="false">
    </asp:placeholder>

                            
</asp:Content>

