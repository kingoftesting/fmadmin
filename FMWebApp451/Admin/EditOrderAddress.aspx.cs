using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using AdminCart;
using FM2015.Helpers;

public partial class Admin_EditOrderAddress : AdminBasePage
{
    Cart cart = new Cart();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            CanUserProceed(new string[2] { "ADMIN", "CUSTOMERSERVICE" });

            if (Request["OrderID"] != null)
            {
                int FMorderID = Convert.ToInt32(Request["OrderID"]);

                cart = cart.GetCartFromOrder(FMorderID);
                Customer_NameID.Text = cart.SiteCustomer.CustomerID.ToString() + " (" + cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + ")";

                BillAddress_Street.Text = cart.BillAddress.Street;
                BillAddress_Street2.Text = cart.BillAddress.Street2;
                BillAddress_City.Text = cart.BillAddress.City;
                BillAddress_ZipPostal.Text = cart.BillAddress.Zip;

                ShipAddress_Street.Text = cart.ShipAddress.Street;
                ShipAddress_Street2.Text = cart.ShipAddress.Street2;
                ShipAddress_City.Text = cart.ShipAddress.City;
                ShipAddress_ZipPostal.Text = cart.ShipAddress.Zip;

                SetStateAndCountries();
            }
        }

    }


    protected void btnUpdateAddr_Click(object sender, EventArgs e)
    {
        if (Request["OrderID"] != null)
        {
            int FMorderID = Convert.ToInt32(Request["OrderID"]);
            cart = cart.GetCartFromOrder(FMorderID);

            cart.BillAddress.CustomerID = cart.SiteCustomer.CustomerID;
            cart.BillAddress.Street = ValidateUtil.CleanUpString(BillAddress_Street.Text);

            if (BillAddress_Street2.Text == "") { BillAddress_Street2.Text = " "; }
            else { BillAddress_Street2.Text = ValidateUtil.CleanUpString(BillAddress_Street2.Text); }
            cart.BillAddress.Street2 = ValidateUtil.CleanUpString(BillAddress_Street2.Text);
            cart.BillAddress.City = ValidateUtil.CleanUpString(BillAddress_City.Text);
            cart.BillAddress.Zip = ValidateUtil.CleanUpString(BillAddress_ZipPostal.Text);

            if (txtStateBill.Visible) { cart.BillAddress.State =  ValidateUtil.CleanUpString(txtStateBill.Text); }
            else { cart.BillAddress.State = selStateBill.SelectedValue; }

            cart.BillAddress.Country = selCountryBill.SelectedValue;

            cart.ShipAddress.CustomerID = cart.SiteCustomer.CustomerID;
            cart.ShipAddress.Street = ValidateUtil.CleanUpString(ShipAddress_Street.Text);

            if (ShipAddress_Street2.Text == "") { ShipAddress_Street2.Text = " "; }
            else { ShipAddress_Street2.Text = ValidateUtil.CleanUpString(ShipAddress_Street2.Text); }
            cart.ShipAddress.Street2 = ValidateUtil.CleanUpString(ShipAddress_Street2.Text);
            cart.ShipAddress.City = ValidateUtil.CleanUpString(ShipAddress_City.Text);
            cart.ShipAddress.Zip = ValidateUtil.CleanUpString(ShipAddress_ZipPostal.Text);

            if (txtStateShip.Visible) { cart.ShipAddress.State = ValidateUtil.CleanUpString(txtStateShip.Text); }
            else { cart.ShipAddress.State = selStateShip.SelectedValue; }

            cart.ShipAddress.Country = selCountryShip.SelectedValue;

            int tempBillAdr = cart.BillAddress.AddressId; //save if needed later
            cart.BillAddress.AddressId = 0; //force a new address in the db
            cart.BillAddress.Save(cart.SiteCustomer, 1); //save new billing address

            int tempShipAdr = cart.ShipAddress.AddressId; //save if needed later
            cart.ShipAddress.AddressId = 0; //force a new address in the db
            cart.ShipAddress.Save(cart.SiteCustomer, 2); //save new shipping address

            //this is here to get around some logic in Address & Order; it's messy, I know!
            if ((cart.BillAddress.AddressId != 0) && (cart.ShipAddress.AddressId != 0))
            {
                //update Order with new Shipping ID
                string sql2 = @"
                UPDATE Orders 
                SET CustomerID = @CustomerID, ShippingAddressID = @ShippingAddressID, BillingAddressID = @BillingAddressID 
                WHERE OrderID = @OrderID 
                ";

                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql2, cart.SiteCustomer.CustomerID, cart.ShipAddress.AddressId, cart.BillAddress.AddressId, cart.SiteOrder.OrderID);
                DBUtil.Exec(sql2, mySqlParameters);
            }
            else
            {
                //replace the addresses because something went wrong with the db update
                cart.BillAddress.AddressId = tempBillAdr;
                cart.ShipAddress.AddressId = tempShipAdr;
            }

            Session["CSCart"] = cart;
            int orderid = cart.SiteOrder.OrderID; //save for redirect back to ReviewOrder

            Certnet.Cart tempcart = new Certnet.Cart();
            tempcart.Load(Session["cart"]); //get name of CS rep requesting address change
            string username = tempcart.SiteCustomer.LastName + "," + tempcart.SiteCustomer.FirstName;
            OrderNote.Insertordernote(orderid, username, "Address Update");

            Response.Redirect("ReviewOrder.aspx?OrderID=" + orderid.ToString());
        }
    }

    protected void SetStateAndCountries()
    {
        ListItem itemA, itemB, itemC, itemD;

        //billing info
        selCountryBill.DataSource = DBUtil.GetLocations(0);
        selCountryBill.DataValueField = "Code";
        selCountryBill.DataTextField = "Name";
        selCountryBill.DataBind();

        selStateBill.DataSource = DBUtil.GetLocations(15);
        selStateBill.DataValueField = "Code";
        selStateBill.DataTextField = "Name";
        selStateBill.DataBind();

        if (cart.BillAddress.Country == "")
        {
            //initialize
            //todo
            selCountryBill.SelectedValue = "US";
            //selStateBill.SelectedValue = "CA";
        }
        else
        {
            itemA = selCountryBill.Items.FindByValue(cart.BillAddress.Country);
            if (itemA != null)
            {
                itemA.Selected = true;

                selStateBill.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(cart.BillAddress.Country)));
                selStateBill.DataValueField = "Code";
                selStateBill.DataTextField = "Name";
                selStateBill.DataBind();
            }
            itemB = selStateBill.Items.FindByValue(cart.BillAddress.State);
            if (itemB != null)
            {
                selStateBill.Visible = true;
                txtStateBill.Visible = false;
                itemB.Selected = true;
            }
            else
            {
                selStateBill.Visible = false;
                txtStateBill.Visible = true;
                txtStateBill.Text = cart.BillAddress.State;
            }

        }

        //shipping info
        selCountryShip.DataSource = DBUtil.GetLocations(0);
        selCountryShip.DataValueField = "Code";
        selCountryShip.DataTextField = "Name";
        selCountryShip.DataBind();

        selStateShip.DataSource = DBUtil.GetLocations(15);
        selStateShip.DataValueField = "Code";
        selStateShip.DataTextField = "Name";
        selStateShip.DataBind();

        if (cart.ShipAddress.Country == "")
        {
            //initialize
            //todo put back
            selCountryShip.SelectedValue = "US";
            //selStateShip.SelectedValue = "CA";
        }
        else
        {
            itemC = selCountryShip.Items.FindByValue(cart.ShipAddress.Country);
            if (itemC != null)
            {
                itemC.Selected = true;

                selStateShip.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(cart.ShipAddress.Country)));
                selStateShip.DataValueField = "Code";
                selStateShip.DataTextField = "Name";
                selStateShip.DataBind();

                itemD = selStateShip.Items.FindByValue(cart.ShipAddress.State);
                if (itemD != null)
                {
                    selStateShip.Visible = true;
                    txtStateShip.Visible = false;
                    itemD.Selected = true;
                }
                else
                {
                    selStateShip.Visible = false;
                    txtStateShip.Visible = true;
                    txtStateShip.Text = cart.ShipAddress.State;
                }
            }
        }
    }


    protected void selCountryBill_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (selCountryBill.SelectedValue != "")
        {
            selStateBill.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(selCountryBill.SelectedValue)));
            selStateBill.DataValueField = "Code";
            selStateBill.DataTextField = "Name";
            selStateBill.DataBind();

            if (selStateBill.Items.Count == 0)
            {
                selStateBill.Visible = false;
                txtStateBill.Visible = true;
            }
            else
            {
                selStateBill.Visible = true;
                txtStateBill.Visible = false;
            }
        }
    }

    protected void selCountryShip_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (selCountryShip.SelectedValue != "")
        {
            selStateShip.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(selCountryShip.SelectedValue)));
            selStateShip.DataValueField = "Code";
            selStateShip.DataTextField = "Name";
            selStateShip.DataBind();

            if (selStateShip.Items.Count == 0)
            {
                selStateShip.Visible = false;
                txtStateShip.Visible = true;
            }
            else
            {
                selStateShip.Visible = true;
                txtStateShip.Visible = false;
            }
        }
    }
}
