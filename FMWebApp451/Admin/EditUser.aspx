<%@ Page Language="C#" Theme="Admin" MasterPageFile="MasterPage3_Blank.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" Inherits="EditUser" Title="Edit User" Codebehind="EditUser.aspx.cs" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="sectiontitle" align="left">Customer Details</div>

<asp:Panel ID="pnlNoCustID" runat="server" Visible="false">
<p>
The CustomerID was not recognized! Please try looking up the customer again.
</p>
</asp:Panel>

    <asp:label id="UpdateMsg" runat="server" /><br />

<asp:Panel ID="pnlDetails" DefaultButton="ButtonAdd" runat="server" Visible="true">

    <div>
	   	<table class="custservice" width="100%" border="1px solid gray">
	   	    <tr style="background-color:#DDDDDD;">
	   	        <td class = "custservice" > 
                    <strong>CustomerID:&nbsp;</strong> 
                    <asp:Label ID="lblCustomerID" Runat="server" />
                </td>
	   		    <td class = "custservice" > 
                    <strong>Created:&nbsp;</strong> 
                    <asp:Label ID="lblCreateDate2" Runat="server" />
                </td>
			    <td class = "custservice" > 
                    <strong>Last Update:&nbsp;</strong>
                    <asp:Label ID="lblLastUpdate2" Runat="server" /> 
                </td>
			    <td class = "custservice" > 
                    <strong>Last Sign-In: &nbsp;</strong> 
                    <asp:Label ID="lblLastSignIn2" Runat="server" />
                </td>
	   	    </tr>
	   	</table>
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="literal3" Runat="server" text="First Name:"></asp:Label>
		<asp:textbox id="txtFirstName" Runat="server" MaxLength="30"></asp:textbox>
		    <asp:requiredfieldvalidator id="Requiredfieldvalidator1" Runat="server" Display="Dynamic" ErrorMessage="Your first name is required"
				ControlToValidate="txtFirstName"></asp:requiredfieldvalidator>
			<asp:customvalidator id="Customvalidator2" Runat="server" ErrorMessage="Your first name must be up to 30 characters"
				Display="Dynamic" ControlToValidate="txtFirstName" ClientValidationFunction="ValidateLengthName" />
	</div>

	
    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Literal4" Runat="server" text="Last Name:"></asp:Label>
		<asp:textbox id="txtLastName" Runat="server" MaxLength="30"></asp:textbox>
		    <asp:requiredfieldvalidator id="Requiredfieldvalidator2" Runat="server" Display="Dynamic" ErrorMessage="Your last name is required."
				ControlToValidate="txtLastName"></asp:requiredfieldvalidator>
			<asp:customvalidator id="Customvalidator3" Runat="server" ErrorMessage="Your last name must be up to 30 characters"
				Display="Dynamic" ControlToValidate="txtLastName" ClientValidationFunction="ValidateLengthName" />
	</div>

    <div class="simpleClear"></div>
    <asp:Label id="lblBilling" Runat="server" text="<strong>Billing Address</strong>"></asp:Label>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Literal1" Runat="server" text="Street1:"></asp:Label>
		<asp:textbox id="txtStreet" Runat="server" MaxLength="60"></asp:textbox>
		    <asp:requiredfieldvalidator id="Requiredfieldvalidator3" Runat="server" Display="Dynamic" ErrorMessage="Your street address is required."
				ControlToValidate="txtStreet"></asp:requiredfieldvalidator>
			<asp:customvalidator id="Customvalidator7" Runat="server" ErrorMessage="Your street2 address 40 characters or fewer"
				Display="Dynamic" ControlToValidate="txtStreet" ClientValidationFunction="ValidateLengthName" />
	</div>
	
    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Literal6" Runat="server" text="Street2:"></asp:Label>
		<asp:textbox id="txtStreet2" Runat="server" MaxLength="60"></asp:textbox>
	</div>
	
    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Literal2" Runat="server" text="City:"></asp:Label>
		<asp:textbox id="txtCity" Runat="server" MaxLength="100"></asp:textbox>
		    <asp:requiredfieldvalidator id="Requiredfieldvalidator4" Runat="server" Display="Dynamic" ErrorMessage="Your city name is required."
				ControlToValidate="txtCity"></asp:requiredfieldvalidator>
			<asp:customvalidator id="Customvalidator8" Runat="server" ErrorMessage="Your city must be up to 100 characters"
				Display="Dynamic" ControlToValidate="txtCity" ClientValidationFunction="ValidateLengthName" />
	</div>
	
    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Literal7" Runat="server" text="State/Province:"></asp:Label>
		<asp:textbox id="txtStateBill" Visible="False" Runat="server" MaxLength="255"></asp:textbox>
		    <asp:dropdownlist id="selStateBill" runat="server" Visible="True" Width="195"></asp:dropdownlist>
		    <asp:requiredfieldvalidator id="rqdStateBill" Visible="False" ControlToValidate="selStateBill" ErrorMessage="*"
		        Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>
	
    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Literal8" Runat="server" text="Zip:"></asp:Label>
        <asp:textbox id="BillAddress_ZipPostal" runat="server" Width="100px"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator9" ControlToValidate="BillAddress_ZipPostal" ErrorMessage="*"
	        Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
        <asp:Label ID="lblZipMsg" runat="server" Text=""></asp:Label>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Literal9" Runat="server" text="Country:"></asp:Label>
        <asp:dropdownlist id="selCountryBill" width="195" runat="server"
                AppendDataBoundItems="true" OnSelectedIndexChanged="selCountryBill_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Text="United States" Value="US" Selected="true" />
                <asp:ListItem Text="Canada" Value="CA" Selected="false" />
                <asp:ListItem Text="--------------" Value="" Selected="false" />
            </asp:dropdownlist>
	</div>
	
    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Literal11" Runat="server" text="Phone:"></asp:Label>
		<asp:textbox id="txtPhone" Runat="server" MaxLength="32"></asp:textbox>
		    <asp:requiredfieldvalidator id="Requiredfieldvalidator10" Runat="server" Display="Dynamic" ErrorMessage="Your phone is required"
				ControlToValidate="txtPhone"></asp:requiredfieldvalidator>
			<asp:customvalidator id="Customvalidator6" Runat="server" ErrorMessage="Your phone must be up to 32 characters"
				Display="Dynamic" ControlToValidate="txtPhone" ClientValidationFunction="ValidateLengthPhone" />
	</div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Literal5" Runat="server" text="Email:"></asp:Label>
		<asp:textbox id="txtEmail" Width="195" Runat="server" MaxLength="50"></asp:textbox>
		    <asp:regularexpressionvalidator id="RegularExpressionValidator1" Runat="server" Display="Dynamic" ErrorMessage="Your email is invalid (example: joe@aol.com)"
				    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator>
			    <asp:requiredfieldvalidator id="Requiredfieldvalidator7" Runat="server" Display="Dynamic" ErrorMessage="Your email is required"
				    ControlToValidate="txtEmail"></asp:requiredfieldvalidator>
			    <asp:customvalidator id="Customvalidator5" Runat="server" ErrorMessage="Your email must be up to 50 characters"
				    Display="Dynamic" ControlToValidate="txtEmail" ClientValidationFunction="ValidateLengthEmail" />
	</div>
	
    <div class="simpleClear"></div>
    <div class="groupHTML">
        <asp:CheckBox ID="chkFaceMasterNewsletter" Width="100%" Runat="server" Text="I would like to receive email updates on Exclusive Offers from FaceMaster.com" />
	</div>

	<div class="simpleClear"></div>
    <div class="groupHTML">
        <asp:CheckBox ID="chkSuzanneSomersNewsletter" Width="100%" Runat="server" Text="I would like to receive email updates from SuzanneSomers.com" />
	</div>
	
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:image id="Image4" runat="server" ImageUrl="../images/pswd_create.gif" ></asp:image>
	</div>
	
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="litPassword" Runat="server" text="Password:"></asp:Label>
		<asp:textbox id="txtPassword" Runat="server" MaxLength="20"></asp:textbox>
		    <asp:requiredfieldvalidator id="Requiredfieldvalidator5" Runat="server" Display="Dynamic" ErrorMessage="A PassWord is required"
				    ControlToValidate="txtPassword"></asp:requiredfieldvalidator>
		    <asp:customvalidator id="CustomValidator1" Runat="server" ErrorMessage="Password must be at least 6 to 20 characters"
				    Display="Dynamic" ControlToValidate="txtPassword" ClientValidationFunction="ValidateLengthPassword"></asp:customvalidator>
	</div>
	
	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Literal15" Runat="server" text="Confirm Password:"></asp:Label>
		<asp:textbox id="txtPasswordConfirm" Runat="server" MaxLength="20"></asp:textbox>
			<asp:requiredfieldvalidator id="Requiredfieldvalidator6" Runat="server" Display="Dynamic" ErrorMessage="Please confirm your PassWord"
			ControlToValidate="txtPasswordConfirm"></asp:requiredfieldvalidator>
		    <asp:comparevalidator id="CompareValidator1" Runat="server" ErrorMessage="Passwords do not match" ControlToValidate="txtPasswordConfirm"
				Display="Dynamic" ControlToCompare="txtPassword"></asp:comparevalidator>
			<asp:customvalidator id="CustomValidator10" Runat="server" ErrorMessage="Password must be at least 6 to 20 characters"
				Display="Dynamic" ControlToValidate="txtPasswordConfirm" ClientValidationFunction="ValidateLengthPassword"></asp:customvalidator>
	</div>

    <div class="simpleClear"></div>
    <asp:CheckBox ID="chkShippingAddress" CssClass="groupHTML" Runat="server" Text="Check here if a separate shipping address is required" AutoPostBack="true" OnCheckedChanged="Checked_SeparateShippingAddress"/>

	<asp:Panel id="pnlShipAddress" runat="server" Visible="false">

    <div class="simpleClear"></div>
    <asp:Label id="Label1" Runat="server" text="<strong>Shipping Address</strong>"></asp:Label>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label2" Runat="server" text="Street1:"></asp:Label>
		<asp:textbox id="txtShipStreet" Runat="server" MaxLength="60"></asp:textbox>
		    <asp:requiredfieldvalidator id="Requiredfieldvalidator8" Runat="server" Display="Dynamic" ErrorMessage="Your street address is required."
				ControlToValidate="txtShipStreet"></asp:requiredfieldvalidator>
			<asp:customvalidator id="Customvalidator4" Runat="server" ErrorMessage="Your street2 address 40 characters or fewer"
				Display="Dynamic" ControlToValidate="txtShipStreet" ClientValidationFunction="ValidateLengthName" />
	</div>
	
    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label3" Runat="server" text="Street2:"></asp:Label>
		<asp:textbox id="txtShipStreet2" Runat="server" MaxLength="60"></asp:textbox>
	</div>
	
    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label4" Runat="server" text="City:"></asp:Label>
		<asp:textbox id="txtShipCity" Runat="server" MaxLength="100"></asp:textbox>
		    <asp:requiredfieldvalidator id="Requiredfieldvalidator11" Runat="server" Display="Dynamic" ErrorMessage="Your city name is required."
				ControlToValidate="txtShipCity"></asp:requiredfieldvalidator>
			<asp:customvalidator id="Customvalidator9" Runat="server" ErrorMessage="Your city must be up to 100 characters"
				Display="Dynamic" ControlToValidate="txtShipCity" ClientValidationFunction="ValidateLengthName" />
	</div>
	
    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label5" Runat="server" text="State/Province:"></asp:Label>
		<asp:textbox id="txtStateShip" Visible="False" Runat="server" MaxLength="255"></asp:textbox>
		<asp:dropdownlist id="selStateShip" runat="server" Visible="True" Width="195"></asp:dropdownlist>
		    <asp:requiredfieldvalidator id="Requiredfieldvalidator12" Visible="False" ControlToValidate="selStateShip" ErrorMessage="*"
		        Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>
	
    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label6" Runat="server" text="Zip:"></asp:Label>
        <asp:textbox id="ShipAddress_ZipPostal" runat="server" Width="100px"></asp:textbox>
            <asp:requiredfieldvalidator id="Requiredfieldvalidator13" ControlToValidate="BillAddress_ZipPostal" ErrorMessage="*"
	            Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
            <asp:Label ID="Label7" runat="server" Text=""></asp:Label>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label id="Label8" Runat="server" text="Country:"></asp:Label>
        <asp:dropdownlist id="selCountryShip" width="195" runat="server"
                AppendDataBoundItems="true" OnSelectedIndexChanged="selCountryShip_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Text="United States" Value="US" Selected="true" />
                <asp:ListItem Text="Canada" Value="CA" Selected="false" />
                <asp:ListItem Text="--------------" Value="" Selected="false" />
            </asp:dropdownlist>
	</div>

    </asp:Panel>
	
	<div class="simpleClear"></div>
    <asp:Label ID="lblFailed" runat="server" Text=""></asp:Label>
    <div class="formHTML">
        <asp:Button ID="ButtonAdd" runat="server" Text="Update" OnClick="ButtonAdd_Click" />
    </div>
</asp:Panel>

    <p></p>
    <asp:Panel ID="pnlRoles" CssClass= "custservice" Width="90%" runat="server">
    <hr />
    <br /><br />
    Roles:

	<div class="simpleClear"></div>
    <div class="formHTML">
        <asp:CheckBox ID="chkAdmin" Text="Admin" AutoPostBack="true" runat="server" />   
        <asp:CheckBox ID="chkTest" Text="Tester" AutoPostBack="true" runat="server" />   
        <asp:CheckBox ID="chkCS" Text="CS" AutoPostBack="true" runat="server" />   
        <asp:CheckBox ID="chkReport" Text="Reports" AutoPostBack="true" runat="server" />   
        <asp:CheckBox ID="chkStore" Text="Store" AutoPostBack="true" runat="server" />
        <asp:CheckBox ID="chkMkt" Text="Mkting" AutoPostBack="true" runat="server" />
    </div>
    <br /><br />
    <asp:Button ID="btnRole" runat="server" Text="Update Roles" OnClick="btnRole_Click" /> 
    </asp:Panel>
    
<script type="text/javascript" language="javascript">
	function ValidateLengthName(oSrc, args) 
	{
		args.IsValid = (args.Value.length <= 40);
	}
	function ValidateLengthEmail(oSrc, args) 
	{
		args.IsValid = (args.Value.length <= 50);
	}	
	function ValidateLengthPhone(oSrc, args) 
	{
		args.IsValid = (args.Value.length <= 32);
	}				
	function ValidateLengthPassword(oSrc, args)
	{
		args.IsValid = (args.Value.length >= 6 && args.Value.length <= 20);
	}
</script>
	   
</asp:Content>