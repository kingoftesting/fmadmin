using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

using AdminCart;
using FM2015.Helpers;

public partial class EditUser : AdminBasePage
{
 
  protected void Page_Load(object sender, EventArgs e) 
  {
      CanUserProceed(new string[2] { "ADMIN", "CUSTOMERSERVICE" });

        int customerID = -1; //set customerID to indicate new customer
        Session["CustomerID"] = customerID;

        if (Request["CustomerID"] != null)
        {
            if (Int32.TryParse(Request["CustomerID"].ToString(), out customerID))
            { if (customerID > 0) { Session["CustomerID"] = customerID; } } //remember for Update & Cancel
        }
        
        if (Request["ReturnURL"] != null)
        {
            string url = Request["ReturnURL"].ToString();
            Session["ReturnURL"] = url;
        }

        if (!Page.IsPostBack)
        {
            Customer c = new Customer();
            Address billAdr = new Address();
            Address shipAdr = new Address();

            if (customerID > 0)
            {
                c = new Customer(customerID);
                billAdr = Address.LoadAddress(customerID, 1);
                shipAdr = Address.LoadAddress(customerID, 2);
            }
            else
            {
                c.Phone = "n/a"; 
                billAdr.Street = "n/a";
                shipAdr.Street = "n/a";
                billAdr.City = "n/a";
                shipAdr.City = "n/a";
                billAdr.Zip = "00000";
                shipAdr.Zip = "00000";
                billAdr.State = "AL";
                shipAdr.State = "AL";
                billAdr.Country = "US";
                shipAdr.Country = "US";
            }

            lblCustomerID.Text = customerID.ToString();
            lblCreateDate2.Text = c.CreateDate.ToString();
            lblLastUpdate2.Text = c.LastUpdateDate.ToString();
            lblLastSignIn2.Text = c.LastLoginDate.ToString();

            txtFirstName.Text = c.FirstName;
            txtLastName.Text = c.LastName;
            txtStreet.Text = billAdr.Street;
            txtStreet2.Text = billAdr.Street2;
            txtCity.Text = billAdr.City;
            txtStateBill.Text = billAdr.State;
            BillAddress_ZipPostal.Text = billAdr.Zip;
            selCountryBill.SelectedValue = billAdr.Country;
            txtPhone.Text = c.Phone;
            txtEmail.Text = c.Email;
            chkFaceMasterNewsletter.Checked = c.FaceMasterNewsletter;
            chkSuzanneSomersNewsletter.Checked = c.SuzanneSomersNewsletter;
            txtPassword.Text = c.Password;
            txtPasswordConfirm.Text = c.Password;

            txtShipStreet.Text = shipAdr.Street;
            txtShipStreet2.Text = shipAdr.Street2;
            txtShipCity.Text = shipAdr.City;
            txtStateShip.Text = shipAdr.State;
            ShipAddress_ZipPostal.Text = shipAdr.Zip;
            selCountryShip.SelectedValue = shipAdr.Country;

            selCountryBill.DataSource = DBUtil.GetLocations(0); //set Country
            selCountryBill.DataValueField = "Code";
            selCountryBill.DataTextField = "Name";
            selCountryBill.DataBind();

            selCountryShip.DataSource = DBUtil.GetLocations(0); //set Country
            selCountryShip.DataValueField = "Code";
            selCountryShip.DataTextField = "Name";
            selCountryShip.DataBind();

            if (String.IsNullOrEmpty(billAdr.Country)) { selCountryBill.SelectedValue = "US"; }
            else { selCountryBill.SelectedValue = billAdr.Country; }

            if (String.IsNullOrEmpty(shipAdr.Country)) { selCountryShip.SelectedValue = "US"; }
            else { selCountryShip.SelectedValue = shipAdr.Country; }

            selStateBill.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(selCountryBill.SelectedValue)));
            selStateBill.DataValueField = "Code";
            selStateBill.DataTextField = "Name";
            selStateBill.DataBind();

            selStateShip.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(selCountryShip.SelectedValue)));
            selStateShip.DataValueField = "Code";
            selStateShip.DataTextField = "Name";
            selStateShip.DataBind();

            if (selStateBill.Items.Count == 0)
            {
                selStateBill.Visible = false;
                txtStateBill.Visible = true;
            }
            else
            {
                selStateBill.Visible = true;
                txtStateBill.Visible = false;
            }

            selStateBill.SelectedValue = billAdr.State;

            if (selStateShip.Items.Count == 0)
            {
                selStateShip.Visible = false;
                txtStateShip.Visible = true;
            }
            else
            {
                selStateShip.Visible = true;
                txtStateShip.Visible = false;
            }

            selStateShip.SelectedValue = shipAdr.State;

            bool DisplaySeparateShipping = false;

            if (billAdr.Street != shipAdr.Street) DisplaySeparateShipping = true;
            if (billAdr.Street2 != shipAdr.Street2) DisplaySeparateShipping = true;
            if (billAdr.City != shipAdr.City) DisplaySeparateShipping = true;
            if (billAdr.State != shipAdr.State) DisplaySeparateShipping = true;
            if (billAdr.Country != shipAdr.Country) DisplaySeparateShipping = true;

            if (DisplaySeparateShipping)
            {
                pnlShipAddress.Visible = true;
                chkShippingAddress.Checked = true;
            }

            chkAdmin.Checked = false;
            chkTest.Checked = false;
            chkCS.Checked = false;
            chkReport.Checked = false;
            chkStore.Checked = false;
            chkMkt.Checked = false;
            pnlRoles.Visible = false; //don't show the Role stuff except to an Admin
            if (Convert.ToBoolean(Session["isAdmin"]))
            {
                pnlRoles.Visible = true;
                string r = FM2015.Models.Roles.GetRole(customerID);
                String[] rolelist = r.Split(',');

                foreach (string str in rolelist)
                {
                    switch (str)
                    {
                        case "ADMIN":
                            chkAdmin.Checked = true;
                            break;

                        case "TESTER":
                            chkTest.Checked = true;
                            break;

                        case "CUSTOMERSERVICE":
                            chkCS.Checked = true;
                            break;

                        case "REPORTS":
                            chkReport.Checked = true;
                            break;

                        case "STORE":
                            chkStore.Checked = true;
                            break;
                        case "MARKETING":
                            chkMkt.Checked = true;
                            break;

                        default:
                            break;

                    }
                }
            }
        }
    }

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        //check zipcode

        lblZipMsg.Style.Add("color", "red");
        lblZipMsg.Text = DBUtil.CheckZipCode(selCountryBill.SelectedValue, BillAddress_ZipPostal.Text);

        if ((Page.IsValid) && (lblZipMsg.Text == ""))
        {
            Customer c = new Customer();
            int CustomerID = Convert.ToInt32(Session["CustomerID"]);
            if (CustomerID > 0) { c = new Customer(CustomerID); }
            
            Address billAdr = new Address();
            Address shipAdr = new Address();

            string newStreet = ValidateUtil.CleanUpString(txtStreet.Text);
            string newStreet2 = ValidateUtil.CleanUpString(txtStreet2.Text);
            string newCity = ValidateUtil.CleanUpString(txtCity.Text);
            string newState = selStateBill.Text;
            string newZip = ValidateUtil.CleanUpString(BillAddress_ZipPostal.Text);
            string newCountry = selCountryBill.Text;


            if (c.CustomerID > 0)
            {
                billAdr = Address.LoadAddress(CustomerID, 1);
                shipAdr = Address.LoadAddress(CustomerID, 2);
            }
           
            c.FirstName = ValidateUtil.CleanUpString(txtFirstName.Text);
            c.LastName = ValidateUtil.CleanUpString(txtLastName.Text);


            bool billAdrNoChange = ((billAdr.Street == newStreet) && (billAdr.Street2 == newStreet2) && (billAdr.City == newCity) && (billAdr.Zip == newZip) && (billAdr.State == newState) && (billAdr.Country == newCountry));
            
            if (!billAdrNoChange) //if something in the billing address changed thenm update it
            {
                billAdr.Street = newStreet;
                billAdr.Street2 = newStreet2;
                billAdr.City = newCity;
                billAdr.Zip = newZip;
                billAdr.State = newState;
                billAdr.Country = newCountry;
            }
            bool shipAdrNoChange = true; //assume no shipping change
            if (chkShippingAddress.Checked) //check for a new shipping address
            {
                string newShipStreet = ValidateUtil.CleanUpString(txtShipStreet.Text);
                string newShipStreet2 = ValidateUtil.CleanUpString(txtShipStreet2.Text);
                string newShipCity = ValidateUtil.CleanUpString(txtShipCity.Text);
                string newShipState = selStateShip.Text;
                string newShipZip = ValidateUtil.CleanUpString(ShipAddress_ZipPostal.Text);
                string newShipCountry = selCountryShip.Text;

                shipAdrNoChange = ((shipAdr.Street == newShipStreet) && (shipAdr.Street2 == newShipStreet2) && (shipAdr.City == newShipCity) && (shipAdr.Zip == newShipZip) && (shipAdr.State == newShipState) && (shipAdr.Country == newShipCountry));
            
                if (!shipAdrNoChange) //if something in the shipping address changed thenm update it
                {
                    shipAdr.Street = newShipStreet;
                    shipAdr.Street2 = newShipStreet2;
                    shipAdr.City = newShipCity;
                    shipAdr.Zip = newShipZip;
                    shipAdr.State = newShipState;
                    shipAdr.Country = newShipCountry;
                }               
            }
            else //no special shipping address required
            { 
                shipAdrNoChange = ((shipAdr.Street == billAdr.Street) && (shipAdr.Street2 == billAdr.Street2) && (shipAdr.City == billAdr.City) && (shipAdr.Zip == billAdr.Zip) && (shipAdr.State == billAdr.State) && (shipAdr.Country == billAdr.Country));

                if (!shipAdrNoChange) //update shipping address
                {
                    shipAdr.Street = billAdr.Street;
                    shipAdr.Street2 = billAdr.Street2;
                    shipAdr.City = billAdr.City;
                    shipAdr.Zip = billAdr.Zip;
                    shipAdr.State = billAdr.State;
                    shipAdr.Country = billAdr.Country;
                }
            }

            if (!billAdrNoChange) { billAdr.AddressId = 0; } //force new address entry & keep trail of past addresses
            if (!shipAdrNoChange) { shipAdr.AddressId = 0; }

            c.Phone = ValidateUtil.CleanUpString(txtPhone.Text);

            bool emailOK = false;
            if ((c.CustomerID > 0) && (c.Email != ValidateUtil.CleanUpString(txtEmail.Text)))
            { emailOK = Customer.CheckEmail(ValidateUtil.CleanUpString(txtEmail.Text)); }
            else { emailOK = true; }
            
            if (emailOK)
            {
                c.Email = ValidateUtil.CleanUpString(txtEmail.Text);
                c.FaceMasterNewsletter = chkFaceMasterNewsletter.Checked;
                c.SuzanneSomersNewsletter = chkSuzanneSomersNewsletter.Checked;

                c.Password = ValidateUtil.CleanUpString(txtPassword.Text);

                if (c.CustomerID <= 0)
                {
                    c.CustomerID = 0; //make sure to insert new customer: needs id = 0
                    c.CustomerID = AdminCart.Customer.EnterCustomer(c, billAdr, shipAdr);
                }
                else
                {
                    c.SaveCustomerToDB();
                    billAdr.Save(c, 1);
                    shipAdr.Save(c, 2);                    
                }

                if (c.CustomerID > 0) //if a valid customer id then update the Shopify side
                {
                    long PartnerID = ShopifyCustomer.UpdateShopifyCustomer(c);
                    if ((c.PartnerID == 0) && PartnerID > 0) { Customer.UpDatePartnerID(c.CustomerID, PartnerID); }
                }

                if (c.CustomerID <= 0)
                {
                    lblFailed.Style.Add("color", "red");
                    lblFailed.Text = "Registration Failed: email already in the FaceMaster system; use another address";
                }
                else
                {
                    if (Session["returnURL"] != null)
                    {
                        string url = Session["returnURL"].ToString();
                        Response.Redirect(url);
                    }
                    else { Response.Redirect("ManageUsers.aspx?PageAction=Lookup&FMCustomerID=" + c.CustomerID); }
                }
            }
            lblFailed.Style.Add("color", "red");
            lblFailed.Text = "Registration Failed: email already in the FaceMaster system; use another address";
        }
    }

    protected void btnRole_Click(object sender, EventArgs e)
    {
        string rolelist = "";
        if (chkAdmin.Checked == true) { rolelist += "admin,"; }
        if (chkTest.Checked == true) { rolelist += "tester,"; }
        if (chkCS.Checked == true) { rolelist += "customerservice,"; }
        if (chkReport.Checked == true) { rolelist += "reports,"; }
        if (chkStore.Checked == true) { rolelist += "store,"; }
        int CustomerID = Convert.ToInt32(Session["CustomerID"]);
        FM2015.Models.Roles r = new FM2015.Models.Roles(CustomerID);
        FM2015.Models.Roles.Updateroles(r.ID, CustomerID, rolelist);
    }

    protected void Checked_SeparateShippingAddress(Object sender, System.EventArgs e)
    {

        if (chkShippingAddress.Checked) { pnlShipAddress.Visible = true; }
        else { pnlShipAddress.Visible = false; }
    }

    protected void selCountryBill_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (selCountryBill.SelectedValue != "")
        {
            selStateBill.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(selCountryBill.SelectedValue)));
            selStateBill.DataValueField = "Code";
            selStateBill.DataTextField = "Name";
            selStateBill.DataBind();

            if (selStateBill.Items.Count == 0)
            {
                selStateBill.Visible = false;
                txtStateBill.Visible = true;
            }
            else
            {
                selStateBill.Visible = true;
                txtStateBill.Visible = false;
            }
        }
    }

    protected void selCountryShip_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (selCountryShip.SelectedValue != "")
        {
            selStateShip.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(selCountryShip.SelectedValue)));
            selStateShip.DataValueField = "Code";
            selStateShip.DataTextField = "Name";
            selStateShip.DataBind();

            if (selStateShip.Items.Count == 0)
            {
                selStateShip.Visible = false;
                txtStateShip.Visible = true;
            }
            else
            {
                selStateShip.Visible = true;
                txtStateShip.Visible = false;
            }
        }
    }
}