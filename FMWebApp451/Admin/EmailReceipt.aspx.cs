using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;
using FM2015.Helpers;

public partial class EmailReceipt : AdminBasePage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Define the name and type of the client scripts on the page.
        String csname1 = "fixform"; //so that only the button controls spec'd generate new windows
        Type cstype = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;

        // Check to see if the startup script is already registered.
        if (!cs.IsStartupScriptRegistered(cstype, csname1))
        {
            StringBuilder cstext1 = new StringBuilder();
            //cstext1.Append("<script type=text/javascript> alert('Hello World!') </");
            //cstext1.Append("script>");

            cstext1.Append("<script type='text/javascript'>");
            cstext1.Append("function fixform() { ");
            cstext1.Append("if (opener.document.getElementById('aspnetForm').target != '_blank') return; ");
            cstext1.Append("opener.document.getElementById('aspnetForm').target = ''; ");
            cstext1.Append("opener.document.getElementById('aspnetForm').action = opener.location.href; ");
            cstext1.Append("} </script>");

            cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
        } 
        
        Cart cart = new Cart();
        cart.Load(Session["CScart"]);
        DateTime orderDate = cart.SiteOrder.OrderDate;
        string cardno = cart.Tran.CardNo;
        if (cardno.Length > 11)
            cardno = cardno.Substring(0, 4) + "XXXXXX" + cardno.Substring(cardno.Length - 4, 4);

        string body = "Thank you for ordering from FaceMaster.com!\n\n" +
"Your order information appears below. If you need to get in touch with us about your order, just forward " +
"this email order confirmation, along with your question to " + Config.CustomerServiceEmail() + "\n\n" +
"Our charges will appear on your credit card statement as SLC WebSales, Inc.\n\n" +
"Again, thank you for shopping at FaceMaster.com.\n\n" +
"The Customer Service Team at FaceMaster.com\n" +
"\n\n\n" +
"YOUR ONLINE ORDER NUMBER IS: " + cart.OrderID.ToString() + "\n\n" +
"Order Date: " + orderDate + "\n\n" +
"Credit Card#: " + cardno + "\n\n" +
"Total Sale: " + cart.SiteOrder.Total.ToString("N") + "\n\n" +
"========================================================================\n" +
"CUSTOMER INFORMATION\n" +
"------------------------------------------------------------------------\n" +
cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + "\n" +
cart.BillAddress.Street + "\n";

        if ((string.IsNullOrEmpty(cart.BillAddress.Street2) || (cart.BillAddress.Street2 == " "))) { }
        else { body += cart.BillAddress.Street2 + "\n"; }

        body += cart.BillAddress.City + ", " + cart.BillAddress.State + " " + cart.BillAddress.Zip + "\n" +
        DBUtil.GetCountry(cart.BillAddress.Country) + "\n" +
        "\n" +
        "========================================================================\n" +
        "SHIPPING ADDRESS\n" +
        "------------------------------------------------------------------------\n" +
        cart.ShipAddress.Street + "\n";

        if ((string.IsNullOrEmpty(cart.ShipAddress.Street2) || (cart.ShipAddress.Street2 == " "))) { }
        else { body += cart.ShipAddress.Street2 + "\n"; }

        body += cart.ShipAddress.City + ", " + cart.ShipAddress.State + " " + cart.ShipAddress.Zip + "\n" +
            DBUtil.GetCountry(cart.ShipAddress.Country) + "\n" +
            "Shipping Method: ";
        if (cart.ShipType == 1) { body += "Ground"; }
        if (cart.ShipType == 2) { body += "Expedited (2-Day Air)"; }
        if (cart.ShipType == 3) { body += "Express (Guarenteed Overnight)"; }

        body += "\n" +
            "========================================================================\n" +
            "PRODUCTS ORDERED\n" +
            "------------------------------------------------------------------------\n" +
            "#      Description                            Qty Price   Total\n" +
            "------------------------------------------------------------------------\n";

        int counter = 1;
        string total = "";
        foreach (Item Item in cart.Items)
        {
            total = String.Format("{0:N}", Item.LineItemTotal);
            body += counter + DBUtil.AddBlanks(8 - counter.ToString().Length) +
                Item.LineItemProduct.Name + DBUtil.AddBlanks(40 - Item.LineItemProduct.Name.Length) +
                Item.Quantity + DBUtil.AddBlanks(5 - Item.Quantity.ToString().Length) +
                "$" + String.Format("{0:N}", Item.LineItemProduct.SalePrice) + DBUtil.AddBlanks(8 - String.Format("{0:N}", Item.LineItemProduct.SalePrice).Length) +
                "$" + total + "\n";
            counter++;
        }

        body += "------------------------------------------------------------------------\n" +
            "                                                     SUBTOTAL: $" + String.Format("{0:N}", cart.SiteOrder.SubTotal) + "\n" +
            "                                                     SHIPPING: $" + String.Format("{0:N}", cart.SiteOrder.TotalShipping) + "\n" +
            "                                                          TAX: $" + String.Format("{0:N}", cart.SiteOrder.TotalTax) + "\n";
        if (cart.SiteOrder.TotalCoupon != 0)
        {
            body += "                                                     DISCOUNT: $" + String.Format("{0:N}", cart.SiteOrder.TotalCoupon) + "\n";

        }
        body += "------------------------------------------------------------------------\n" +
            "                                                        TOTAL: $" + String.Format("{0:N}", cart.SiteOrder.Total) + "\n";

        try
        {
            if (AdminCart.Config.CartSetting == "Test")
            {
                //dhenson@certifiednetworks.com
                mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "Order Confirmation #" +
                      cart.OrderID.ToString(), body);

                litEmailReceipt.Text = "TEST: Email successfully sent to " + cart.SiteCustomer.Email;
            }
            else
            {
                mvc_Mail.SendMail(AdminCart.Config.MailFrom(), cart.SiteCustomer.Email, "Order Confirmation #" +
                         cart.OrderID.ToString(), body);
                litEmailReceipt.Text = "Email successfully sent to " + cart.SiteCustomer.Email;
            }
        }
        catch (Exception ex)
        {
            //just drop the exception sending mail
            string exceptionText = ex.Message;
            litEmailReceipt.Text = "Email NOT successfully sent to " + cart.SiteCustomer.Email;
            litEmailReceipt.Text += "<br /><br />";
            litEmailReceipt.Text += exceptionText;
        } 
    }
}
