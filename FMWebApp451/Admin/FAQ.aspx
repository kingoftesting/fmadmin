<%@ Page Language="C#" trace="false" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="FM.Admin.FAQ" Title="FAQ" Codebehind="FAQ.aspx.cs" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="alpha four columns">
    <span class="faqHead">FAQ Categories</span><br /><br />
    <asp:Literal ID="LeftHTML" runat="server" />
</div>

<div class="omega twelve columns">

	        <asp:repeater id="Repeater1" runat="server" EnableViewState="False">
            <ItemTemplate>
		        <b>
		        <%#DataBinder.Eval(Container.DataItem,"Question")%>
		        </b>
		        <br />
		        <br />
		        <%#DataBinder.Eval(Container.DataItem,"Answer")%>
	            <br />
		        <br />
		        <hr />
	        </ItemTemplate>
            </asp:repeater>

</div>
</asp:Content>
