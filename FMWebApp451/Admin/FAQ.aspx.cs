using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace FM.Admin
{
    public partial class FAQ : AdminBasePage //System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string left = "";

            SqlDataReader dr = DBUtil.FillDataReader("SELECT DISTINCT Category FROM FAQs WHERE Enabled = 1");
            while (dr.Read())
            {
                left += "<a href=FAQ.aspx?Category=" + dr["Category"].ToString().Replace(" ", "_") + ">" + dr["Category"].ToString() + "</a><br /><br />";
            }
            left += "<br /><a href=FAQ.aspx>" + "View All FAQs" + "</a><br />";
            if (dr != null) { dr.Close(); }
            LeftHTML.Text = left;


            string whereclause = "";
            string category = "";
            SqlDataReader body = null;

            if (Request["Category"] != null)
            {
                category = Request["Category"].Replace("_", " ");
                whereclause = " AND Category= @Category";
            }

            string sql = @"
            SELECT REPLACE(Question, 'FaceMaster ', 'FaceMaster® ') AS Question, 
                   REPLACE(Answer, 'FaceMaster ', 'FaceMaster® ') AS Answer 
            FROM FAQs 
            WHERE 0 = 0 AND Enabled = 1 
        ";
            sql += whereclause;
            sql += " ORDER BY SortOrder";

            if (whereclause != "")
            {
                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, category);
                body = DBUtil.FillDataReader(sql, mySqlParameters);
            }
            else { body = DBUtil.FillDataReader(sql); }

            Repeater1.DataSource = body;
            Repeater1.DataBind();
            if (body != null) { body.Close(); }

        }
    } 
}
