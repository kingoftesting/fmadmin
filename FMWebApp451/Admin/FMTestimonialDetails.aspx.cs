﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class FMTestimonialDetails : AdminBasePage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["id"] != null)
        {
            int id = Int32.Parse( Request["id"].ToString());
            FMTestimonial t = new FMTestimonial(id);

            Literal1.Text = "<a href='FMTestimonialList.aspx'>Back</a><br><br>";

            Literal1.Text += "<b>Name: </b>" + t.FirstName + " " + t.LastName + "<br>";
            Literal1.Text += "<b>Date: </b>" + t.SubmitDate + "<br><br>";

            Literal1.Text += "<b>Birth Date: </b>" + t.Birthdate + "<br>";
            Literal1.Text += "<b>Location: </b>" + t.City + ", " + t.State + "  " + t.Zip + "<br><br>";

            Literal1.Text += "<b>Phone:</b> " + t.Phone + "<br>";
            Literal1.Text += "<b>Email:</b> " + t.Email + "<br><br>";

            
            

            Literal1.Text += "<b>Testimonial:</b><div width='640px;' style='width:640px;'>" + t.Testimonial + "</div><br>";

            if (t.BeforePicID != 0)
            {
                Literal1.Text += "Before Pic Submitted";
                imgBefore.ImageUrl += "~/admin/ShowImage.aspx?id=" + t.BeforePicID.ToString();
            }
            else Literal1.Text += "Before Pic NOT Submitted";

            if (t.AfterPicID != 0)
            {
                Literal1.Text += "After Pic Submitted";
                imgAfter.ImageUrl = "~/admin/ShowImage.aspx?id=" + t.AfterPicID.ToString();
            }
            else Literal1.Text += "After Pic NOT Submitted";

        
        }
    }
}
