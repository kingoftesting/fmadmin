﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="Admin" Title="FM Testimonial List" MasterPageFile="MasterPage3_Blank.master" Inherits="FMTestimonialList" Codebehind="FMTestimonialList.aspx.cs" %>

<asp:Content ID="MainContent" runat="server" ContentPlaceHolderID="MainContent">

    <div>
    FM Testimonials<br /><br />
    
    <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" 
            GridLines="None" AutoGenerateColumns="false">
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
        <asp:TemplateField >
        
        </asp:TemplateField>
        <asp:BoundField DataField="FMTestimonialID" HeaderText="ID" />
        <asp:BoundField DataField="SubmitDate" HeaderText="Date"  />
        <asp:BoundField DataField="BeforePicID" HeaderText="BeforePicID"  />
        <asp:BoundField DataField="AfterPicID" HeaderText="AfterPicID"  />
        <asp:BoundField DataField="Testimonial" HeaderText="Testimonial"  />
        <asp:TemplateField>
        <ItemTemplate><a href="FMTestimonialDetails.aspx?id=<%#DataBinder.Eval(Container.DataItem, "FMTestimonialID")%>">Details...</a></ItemTemplate>
        
        </asp:TemplateField>
        </Columns>
        </asp:GridView>
    </div>
    
</asp:Content>