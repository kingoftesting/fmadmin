using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using FM2015.Helpers;

public partial class Admin_GetCerED : AdminBasePage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string CerYear = DateTime.Now.Year.ToString();
        string CerIdentifier = "-1";
        string keyword = "";
        string resultNum = "10";
        if (!String.IsNullOrEmpty(Request["CerYear"])) { CerYear = Request["CerYear"].ToString(); }
        if (!ValidateUtil.IsNumeric(CerYear)) { CerYear = DateTime.Now.Year.ToString(); }
        if (!String.IsNullOrEmpty(Request["CerIdentifier"])) { CerIdentifier = Request["CerIdentifier"].ToString(); }
        if (!ValidateUtil.IsNumeric(CerIdentifier)) { CerIdentifier = "-1"; }
        if (!String.IsNullOrEmpty(Request["keyword"])) { keyword = ValidateUtil.CleanUpString(Request["keyword"].ToString()); }
        if (!String.IsNullOrEmpty(Request["resultnum"])) { resultNum = Request["resultnum"].ToString(); }
        if (!ValidateUtil.IsNumeric(resultNum)) { resultNum = "10"; }
        if (!IsPostBack)
            BindGrid(CerYear, CerIdentifier, keyword, resultNum);

    }

    public void BindGrid(string CerYear, string CerIdentifier, string keyword, string resultNum)
    {
        string sql = "";
        SqlParameter[] mySqlParameters = null;
        if (CerIdentifier != "-1")
        {
            sql = @"
            SELECT top 10 CerYear, CerIdentifier, EventDescription, [Message] = ' ' 
            FROM CER2s 
            WHERE CerYear = @CerYear AND CerIdentifier = @CerIdentifier 
            ORDER BY CerYear DESC, CerIdentifier DESC  
        ";

            mySqlParameters = DBUtil.BuildParametersFrom(sql, CerYear, CerIdentifier);
        }
        else
        {
            sql = "SELECT top " + resultNum + " " + @"
            CerYear, CerIdentifier, EventDescription, [Message] 
            FROM ( 
		            SELECT top 10 CerYear, CerIdentifier, EventDescription, [Message] = ' ' 
		            FROM CER2s 
		            WHERE EventDescription LIKE @keyword1 
            		ORDER BY CerYear DESC, CerIdentifier DESC 

		            UNION ALL 
            		
		            SELECT top 10 CerYear, CerIdentifier, EventDescription = ' ', [Message] 
		            FROM messagesCER 
		            WHERE [Message] LIKE @keyword2 
                    ORDER BY CerYear DESC, CerIdentifier DESC 

	            ) AS tmp 
        ";

            keyword = "%" + keyword + "%";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, keyword, keyword);

        }

        DataSet ds = DBUtil.FillDataSet(sql, "CerTable", mySqlParameters, AdminCart.Config.ConnStrCER());

       //process dataset to hilite the keyword
        if (!string.IsNullOrEmpty(keyword))
        {
            keyword = keyword.Replace("%", string.Empty);
            string hiliteKeyword = "<span style='background-color:yellow'>" + keyword + "</span>";
            for (int i = 0; i < ds.Tables["CerTable"].Rows.Count; i++)
            {
                string evStr = ds.Tables["CerTable"].Rows[i]["EventDescription"].ToString();
                evStr = Regex.Replace(evStr, keyword, hiliteKeyword, RegexOptions.IgnoreCase);
                //evStr = evStr.Replace(keyword, hiliteKeyword);
                ds.Tables["CerTable"].Rows[i]["EventDescription"] = evStr;

                string msgStr = ds.Tables["CerTable"].Rows[i]["Message"].ToString();
                msgStr = Regex.Replace(msgStr, keyword, hiliteKeyword, RegexOptions.IgnoreCase);
                //msgStr = msgStr.Replace(keyword, hiliteKeyword);
                ds.Tables["CerTable"].Rows[i]["Message"] = msgStr;
            }
        }

        DataView myDataView = ds.Tables["CerTable"].DefaultView;
        gridCER.DataSource = myDataView;
        gridCER.DataBind();
    }
}
