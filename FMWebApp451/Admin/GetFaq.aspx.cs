using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using FM2015.Helpers;

public partial class Admin_GetFaq : AdminBasePage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string FaqID = "";
        string keyword = "";
        if (!String.IsNullOrEmpty(Request["FaqID"])) { FaqID = Request["FaqID"].ToString(); }
        if (!ValidateUtil.IsNumeric(FaqID)) { FaqID = ""; }
        if (!String.IsNullOrEmpty(Request["keyword"])) { keyword = ValidateUtil.CleanUpString(Request["keyword"].ToString()); }
        if (!IsPostBack)
            BindGrid(FaqID, keyword);
    }

    public void BindGrid(string FaqID, string keyword)
    {
        string sql = "";
        SqlParameter[] mySqlParameters = null;
        if (string.IsNullOrEmpty(FaqID))
        {
            sql = @"
            SELECT top 10 * 
            FROM Faqs 
            WHERE Answer LIKE @Answer  
        ";
            keyword = "%" + keyword + "%";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, keyword);
        }
        else
        {
            sql = @"
            SELECT top 10 * 
            FROM Faqs 
            WHERE FaqID = @FaqID  
        ";

            mySqlParameters = DBUtil.BuildParametersFrom(sql, FaqID);
        }
        DataSet ds = DBUtil.FillDataSet(sql, "FaqTable", mySqlParameters);

        //process dataset to hilite the keyword
        if (!string.IsNullOrEmpty(keyword))
        {
            keyword = keyword.Replace("%", string.Empty);
            string hiliteKeyword = "<span style='background-color:yellow'>" + keyword + "</span>";
            for (int i = 0; i < ds.Tables["FaqTable"].Rows.Count; i++)
            {
                string evStr = ds.Tables["FaqTable"].Rows[i]["Answer"].ToString();
                //evStr = evStr.Replace(keyword, hiliteKeyword);
                evStr = Regex.Replace(evStr, keyword, hiliteKeyword, RegexOptions.IgnoreCase);
                ds.Tables["FaqTable"].Rows[i]["Answer"] = evStr;
            }
        }

        DataView myDataView = ds.Tables[0].DefaultView;
        gridFaq.DataSource = myDataView;
        gridFaq.DataBind();
    }

}
