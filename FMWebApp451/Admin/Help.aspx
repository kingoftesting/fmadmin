<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="FM.Admin.Help" Title="Help" Codebehind="Help.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="alpha four columns">
    <asp:Literal ID="LeftHTML" runat="server" />
</div>

<div class="omega twelve columns">
    <asp:Literal ID="BodyText" runat="server" />
</div>

</asp:Content>


