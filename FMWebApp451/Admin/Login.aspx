<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="FM.Admin.Login" Title="SignIn" Codebehind="Login.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <script type="text/javascript">
        $.colorbox.close = function () { }; //redefine close method so that box doesn't close until page redirects

        $(document).ready(function () {
            $('#cboxOverlay, #colorbox').appendTo('form');
            $.colorbox({
                width: "50%",
                inline: true,
                href: "#loginPanel",
                opacity: 0.8,
                trapFocus: true,
                onComplete: function () {
                    $("#<%= UserEmail.ClientID%>").focus();
                },
                onClosed: function () {
                    $('div.lightbox-content').hide();
                    //$("#<%= pnlLogin.ClientID%>").hide();
                    //$('#pnlLogin').hide();
                }
            });

            $("input[name$='UserEmail']").focus();
        });

        function closeOverlay() {
            $.colorbox.close();
            return false;
        }
     </script>
             
    <asp:Panel ID="pnlLogin" runat="server" DefaultButton="btnLogin" Style="visibility: hidden; height: 1px">
        <div id="loginPanel" class="overlaypanel">
            <h2><asp:Label ID="lblSignInHdr" runat="server" Text="Admin Sign-In"></asp:Label></h2>

            <p>Please enter your email and password to login:</p>

            <div class="accountInfo">
                <div class="login">
                    <div class="field">
                        <asp:Label ID="UserNameLabel" runat="server">Email:</asp:Label>
                        <div class="boxRound">
                            <asp:TextBox ID="UserEmail" runat="server" CssClass="textEntry" TabIndex="1"></asp:TextBox>
                            <!--
                            <asp:RequiredFieldValidator ControlToValidate="UserEmail" ErrorMessage="*Please enter a valid email address."
                                Display="Dynamic" runat="server" ID="RequiredFieldValidator1" />
                            -->
                        </div>
                    </div>
                    <div class="field">
                        <asp:Label ID="PasswordLabel" runat="server">Password:</asp:Label>
                        <div class="boxRound">
                            <asp:TextBox ID="UserPass" runat="server" CssClass="passwordEntry" TextMode="Password" TabIndex="2"></asp:TextBox>
                        </div>
                    </div>

                    <asp:Panel ID="PanelMessage" Visible="true" runat="server">
                        <div class="message">
                            <asp:Label EnableViewState="false" ID="CartMessage" ForeColor="red" runat="server"></asp:Label>
                        </div>
                        <div class="simpleClear"></div>
                        <div class="submitButton">
                            <asp:Button ID="btnLogin" OnClientClick="colorboxDialogSubmitClicked('lblVerifyContinue', 'loginPanel');" OnClick="Login_Click" runat="server" CausesValidation="true" Text="Sign-In" /><br />
                        </div>
                    </asp:Panel>

                </div>
            </div>

        </div>
    </asp:Panel>

    <script type="text/javascript">
        // Making Search button as default button for this page
        $(document).keyup(function (event) {
            if (event.keyCode == 13) {
                //event.preventDefault();
                //alert("q keyup event");
                $("#<%= btnLogin.ClientID%>").click();
                return false;
            }
        });
    </script>

</asp:Content>


