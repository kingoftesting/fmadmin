using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Certnet;

namespace FM.Admin
{
    public partial class Login : AdminBasePage //System.Web.UI.Page
    {
        Cart cart;

        void Page_Init(Object sender, EventArgs e)
        {
            cart = new Cart();
            cart.Load(Session["cart"]);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.DefaultButton = btnLogin.UniqueID;
            lblSignInHdr.Text = AdminCart.Config.AdminName() + " " + lblSignInHdr.Text;

            if (!Page.IsPostBack)
            {
                CartMessage.Text = "Please Login to access the admin section.";

                if (AdminCart.Config.EasyTestLogin)
                {
                    UserEmail.Text = "rmohme@alumni.uci.edu";
                    UserPass.Attributes.Add("value", "kingoftest");
                    //UserPass.Text = "hello101";
                }
            }
        }

        protected void Login_Click(Object sender, EventArgs E)
        {
            int CustomerID = cart.SiteCustomer.Login(ValidateUtil.CleanUpString(UserEmail.Text), ValidateUtil.CleanUpString(UserPass.Text));
            bool userOK = false;
            Session["IsAdmin"] = false;
            Session["IsDoctor"] = false;
            Session["IsCS"] = false;
            Session["IsQA"] = false;
            Session["IsREPORTS"] = false;
            Session["IsSTORE"] = false;
            Session["IsUSER"] = false;
            Session["IsMARKETING"] = false;

            FM2015.Models.Roles role = new FM2015.Models.Roles(CustomerID); //get user roles, if any
            if (!String.IsNullOrEmpty(role.Role))
            {
                userOK = true;
                string[] roles = role.Role.Split(',');
                for (int i = 0; i < roles.Length; i++)
                {
                    if (roles[i].ToUpper() == "ADMIN") { Session["IsAdmin"] = true; }
                    if (roles[i].ToUpper() == "DOCTOR") { Session["IsDoctor"] = true; }
                    if (roles[i].ToUpper() == "CUSTOMERSERVICE") { Session["IsCS"] = true; }
                    if (roles[i].ToUpper() == "ISQA") { Session["IsQA"] = true; }
                    if (roles[i].ToUpper() == "REPORTS") { Session["IsREPORTS"] = true; }
                    if (roles[i].ToUpper() == "STORE") { Session["IsSTORE"] = true; }
                    if (roles[i].ToUpper() == "USER") { Session["IsUSER"] = true; }
                    if (roles[i].ToUpper() == "MARKETING") { Session["IsMARKETING"] = true; }
                }
            }
            //CustomerID = 150;
            if (userOK)
            {
                //login successful

                cart.SiteCustomer = new Customer(CustomerID);
                cart.BillAddress = Address.LoadAddress(CustomerID, 1);
                cart.ShipAddress = Address.LoadAddress(CustomerID, 2);
                Session["UserLastName"] = cart.SiteCustomer.LastName;
                Session["UserFirstName"] = cart.SiteCustomer.FirstName;
                Session["UserID"] = cart.SiteCustomer.CustomerID;

                FM2015.Models.User user = new FM2015.Models.User(CustomerID);
                Session["_currentUser"] = user;

                Session["Cart"] = cart;
                string redir = "";

                string RelativeWebRoot = VirtualPathUtility.ToAbsolute("~/admin/");

                HttpContext context = HttpContext.Current;
                if (context == null)
                    throw new System.Net.WebException("The current HttpContext is null");

                Uri uriPart = new Uri(context.Request.Url.GetLeftPart(UriPartial.Authority) + RelativeWebRoot);
                string redirPath = uriPart.ToString();

                if ((AdminCart.Config.RedirectMode == "Test") || (redirPath.ToUpperInvariant().Contains("LOCAL"))) { }
                else
                {
                    if (AdminCart.Config.appName.ToUpper() != "SONULASE")
                    {
                        redirPath = redirPath.Replace("http://", "https://");
                    }
                }

                if (Request["redir"] == null)
                {
                    redir = redirPath + "default.aspx";
                    //redir = "default.aspx";
                }
                else
                {
                    redir = redirPath + Request["redir"].ToString();
                    //redir = "~/adminMVC/";
                    //redir = Request["redir"].ToString();

                }

                Response.Redirect(redir);
            }
            else
            {
                CartMessage.Text = "Login failed.";
                Response.Redirect("login.aspx");
            }
        }
    } 
}
