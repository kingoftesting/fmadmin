using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace FM.Admin
{
    public partial class Logout : AdminBasePage //System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["cart"] = null;
            BodyText.Text = "You have been logged out.";
            Response.Redirect("login.aspx");
        }
    }
    
}