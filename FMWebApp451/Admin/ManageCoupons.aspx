<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" ValidateRequest="false" AutoEventWireup="true" Inherits="FaceMaster.Admin.ManageCoupons" Title="Manage Coupons" Codebehind="ManageCoupons.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="RDatePicker" Src="~/controls/RDatePicker.ascx" %>
<%@ Reference VirtualPath="~/controls/RDatePicker.ascx" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" Runat="Server">   
   <div class="sectiontitle">FaceMaster Coupon Management</div>
   <p>&nbsp;</p>

   <table width="100%">
      <tr>
         <td style="width:100%" valign="top">
         <asp:DetailsView ID="dvwCoupon" runat="server" 
         AutoGenerateRows="False" 
         DataSourceID="objCurrCoupon" 
         Width="100%" 
         AutoGenerateEditButton="True" 
         AutoGenerateInsertButton="True" 
         HeaderText="Coupon Details" 
         DataKeyNames="CouponCodeID" 
         DefaultMode="Insert" 
         OnItemCommand="dvwCoupon_ItemCommand" 
         OnItemInserted="dvwCoupon_ItemInserted" 
         OnItemUpdated="dvwCoupon_ItemUpdated" 
         OnItemCreated="dvwCoupon_ItemCreated">
            <FieldHeaderStyle Width="100px" />
            <Fields>
               <asp:BoundField DataField="CouponCodeID" HeaderText="ID" ReadOnly="True" InsertVisible="False" />

               <asp:TemplateField HeaderText="Code">
                  <ItemTemplate>
                     <asp:Label ID="lblCode" runat="server" Text='<%# Eval("CouponCode") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="txtCode" runat="server" Text='<%# Bind("CouponCode") %>' MaxLength="25" Width="125px"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="valRequireCode" runat="server" ControlToValidate="txtCode" SetFocusOnError="true"
                        Text="The CouponCode field is required." ToolTip="The CouponCode field is required." Display="Dynamic" ValidationGroup="Coupon"></asp:RequiredFieldValidator>
                  </EditItemTemplate>
               </asp:TemplateField>
               
               <asp:TemplateField HeaderText="Description">
                  <ItemTemplate>
                     <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("CouponDescription") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("CouponDescription") %>' MaxLength="50" Width="125px"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="valRequireDescription" runat="server" ControlToValidate="txtDescription" SetFocusOnError="true"
                        Text="The CouponDescription field is required." ToolTip="The CouponDescription field is required." Display="Dynamic" ValidationGroup="Coupon"></asp:RequiredFieldValidator>
                  </EditItemTemplate>
               </asp:TemplateField>

               <asp:TemplateField HeaderText="StartDate">
                  <ItemTemplate>
                     <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("CouponStartDate") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <uc1:RDatePicker id="RDatePicker1" SelectedDate='<%# Bind("CouponStartDate", "{0:d}") %>' runat="server"></uc1:RDatePicker> 
                  </EditItemTemplate>
               </asp:TemplateField>

               <asp:TemplateField HeaderText="EndDate">
                  <ItemTemplate>
                     <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("CouponEndDate") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <uc1:RDatePicker id="RDatePicker2" SelectedDate='<%# Bind("CouponEndDate", "{0:d}") %>' runat="server"></uc1:RDatePicker> 
                  </EditItemTemplate>
               </asp:TemplateField>

               <asp:TemplateField HeaderText="SpecialRequirements">
                  <ItemTemplate>
                     <asp:Label ID="lblSpecialRequirements" runat="server" Text='<%# Eval("CouponSpecialRequirements") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                      <asp:DropDownList ID="ddlSpecialRequirements" runat="server" SelectedValue='<%# Bind("CouponSpecialRequirements") %>'>
                          <asp:ListItem Text="N" Value="N" Selected="True"></asp:ListItem>
                          <asp:ListItem Text="Y" Value="Y" Selected="False"></asp:ListItem>
                      </asp:DropDownList>
                  </EditItemTemplate>               
               </asp:TemplateField>

               <asp:TemplateField HeaderText="OrderMinimum">
                  <ItemTemplate>
                     <asp:Label ID="lblOrderMinimum" runat="server" Text='<%# Eval("CouponOrderMinimum") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="txtOrderMinimum" runat="server" Text='<%# Bind("CouponOrderMinimum") %>' MaxLength="50" Width="125px"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="valRequireOrderMinimum" runat="server" ControlToValidate="txtOrderMinimum" SetFocusOnError="true"
                        Text="The CouponOrderMinimum field is required." ToolTip="The CouponOrderMinimum field is required." Display="Dynamic" ValidationGroup="Coupon"></asp:RequiredFieldValidator>
                  </EditItemTemplate>
               </asp:TemplateField>

               <asp:TemplateField HeaderText="Exclusive">
                  <ItemTemplate>
                     <asp:Label ID="lblExclusive" runat="server" Text='<%# Eval("CouponExclusive") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                      <asp:DropDownList ID="ddlExclusive" runat="server" SelectedValue='<%# Bind("CouponExclusive") %>'>
                          <asp:ListItem Text="N" Value="N" Selected="True"></asp:ListItem>
                          <asp:ListItem Text="Y" Value="Y" Selected="False"></asp:ListItem>
                      </asp:DropDownList>
                  </EditItemTemplate>
               </asp:TemplateField>

               <asp:TemplateField HeaderText="Type">
                  <ItemTemplate>
                     <asp:Label ID="lblType" runat="server" Text='<%# Eval("CouponType") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                      <asp:DropDownList ID="ddlType" runat="server" SelectedValue='<%# Bind("CouponType") %>'>
                          <asp:ListItem Text="Percent" Value="Percent" Selected="True"></asp:ListItem>
                          <asp:ListItem Text="Dollar" Value="Dollar" Selected="False"></asp:ListItem>
                      </asp:DropDownList>
                  </EditItemTemplate>

               </asp:TemplateField>

               <asp:TemplateField HeaderText="Variable">
                  <ItemTemplate>
                     <asp:Label ID="lblVariable" runat="server" Text='<%# Eval("CouponVariable") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                      <asp:DropDownList ID="ddlVariable" runat="server" SelectedValue='<%# Bind("CouponVariable") %>'>
                          <asp:ListItem Text="NonSaleProduct" Value="NonSaleProduct" Selected="True"></asp:ListItem>
                      </asp:DropDownList>
                  </EditItemTemplate>

               </asp:TemplateField>

               <asp:TemplateField HeaderText="Operand">
                  <ItemTemplate>
                     <asp:Label ID="lblOperand" runat="server" Text='<%# Eval("CouponOperand") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                      <asp:DropDownList ID="ddlOperand" runat="server" SelectedValue='<%# Bind("CouponOperand") %>'>
                          <asp:ListItem Text="%" Value="%" Selected="True"></asp:ListItem>
                          <asp:ListItem Text="$" Value="$" Selected="False"></asp:ListItem>
                      </asp:DropDownList>
                  </EditItemTemplate>
               </asp:TemplateField>

               <asp:TemplateField HeaderText="Amount">
                  <ItemTemplate>
                     <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("CouponAmount") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="txtAmount" runat="server" Text='<%# Bind("CouponAmount") %>' MaxLength="25" Width="125px"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="valRequireAmount" runat="server" ControlToValidate="txtAmount" SetFocusOnError="true"
                        Text="The CouponAmount field is required." ToolTip="The CouponAmount field is required." Display="Dynamic" ValidationGroup="Coupon"></asp:RequiredFieldValidator>
                  </EditItemTemplate>
               </asp:TemplateField>

               <asp:TemplateField HeaderText="Notes">
                  <ItemTemplate>
                     <asp:Label ID="lblNotes" runat="server" Text='<%# Eval("CouponNotes") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="txtNotes" runat="server" Text='<%# Bind("CouponNotes") %>' MaxLength="500" Width="100%"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="valRequireNotes" runat="server" ControlToValidate="txtNotes" SetFocusOnError="true"
                        Text="The CouponNotes field is required." ToolTip="The CouponNotes field is required." Display="Dynamic" ValidationGroup="Coupon"></asp:RequiredFieldValidator>
                  </EditItemTemplate>
               </asp:TemplateField>

               <asp:TemplateField HeaderText="UsageInstances">
                  <ItemTemplate>
                     <asp:Label ID="lblUsageInstances" runat="server" Text='<%# Eval("CouponUsageInstances") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="txtUsageInstances" runat="server" Text='<%# Bind("CouponUsageInstances") %>' MaxLength="50" Width="125px"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="valRequireUsageInstances" runat="server" ControlToValidate="txtUsageInstances" SetFocusOnError="true"
                        Text="The CouponUsageInstances field is required." ToolTip="The CouponUsageInstances field is required." Display="Dynamic" ValidationGroup="Coupon"></asp:RequiredFieldValidator>
                  </EditItemTemplate>
               </asp:TemplateField>
                            
            </Fields>
         </asp:DetailsView>
   <p>&nbsp;</p>
   <asp:GridView ID="gvwCoupon" runat="server" 
   AutoGenerateColumns="False" 
   DataSourceID="objCoupon" 
   Width="100%" 
   DataKeyNames="CouponCodeID"  
   AllowSorting="true"
   OnRowCreated="gvwCoupon_RowCreated" 
   OnRowDeleted="gvwCoupon_RowDeleted" 
   OnSelectedIndexChanged="gvwCoupon_SelectedIndexChanged" 
   OnRowCommand="gvwCoupon_RowCommand">
      <Columns>
         <asp:BoundField HeaderText="Used#" DataField="CouponUsageInstances" SortExpression="CouponUseageInstances" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Code" DataField="CouponCode" SortExpression="CouponCode" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Description" DataField="CouponDescription" SortExpression="CouponDescription" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="StartDate" DataField="CouponStartDate" SortExpression="CouponStartDate" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="EndDate" DataField="CouponEndDate" SortExpression="CouponEndDate" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Constraints" DataField="CouponSpecialRequirements" />
         <asp:BoundField HeaderText="OrderMin" DataField="CouponOrderMinimum" SortExpression="CouponOrderMinimum" DataFormatString="{0:C}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Amount" DataField="CouponAmount" SortExpression="CouponAmount" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Type" DataField="CouponType" SortExpression="CouponType" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Notes" DataField="CouponNotes" SortExpression="CouponNotes" HeaderStyle-HorizontalAlign="Center"/>
          <asp:CommandField ButtonType="Image" SelectImageUrl="~/Images/Edit.gif" SelectText="Edit poll" ShowSelectButton="True">
            <ItemStyle HorizontalAlign="Center" Width="20px" />
         </asp:CommandField>
      </Columns>
      <EmptyDataTemplate><b>No Coupons to show</b></EmptyDataTemplate>   
   </asp:GridView>
   <asp:ObjectDataSource ID="objCoupon" runat="server" 
      TypeName="Certnet.Coupon" 
      SelectMethod="GetCouponList">
   </asp:ObjectDataSource>
<p>&nbsp;</p>
         <asp:ObjectDataSource ID="objCurrCoupon" runat="server" 
         TypeName="Certnet.Coupon" 
         InsertMethod="InsertCoupon" 
         SelectMethod="GetCouponByID" 
         UpdateMethod="UpdateCoupon">
            <SelectParameters>
               <asp:ControlParameter ControlID="gvwCoupon" Name="CouponCodeID" PropertyName="SelectedValue"
                  Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
               <asp:Parameter Name="CouponCodeID" Type="Int32" />
               <asp:Parameter Name="CouponCode" Type="String" />
               <asp:Parameter Name="CouponDescription" Type="String" />
               <asp:Parameter Name="CouponStartDate" Type="DateTime" />
               <asp:Parameter Name="CouponEndDate" Type="DateTime" />
               <asp:Parameter Name="CouponSpecialRequirements" Type="String" />
               <asp:Parameter Name="CouponOrderMinimum" Type="Decimal" />
               <asp:Parameter Name="CouponExclusive" Type="String" />
               <asp:Parameter Name="CouponType" Type="String" />
               <asp:Parameter Name="CouponVariable" Type="String" />
               <asp:Parameter Name="CouponOperand" Type="String" />
               <asp:Parameter Name="CouponAmount" Type="String" />
               <asp:Parameter Name="CouponNotes" Type="String" />
            </UpdateParameters>
            <InsertParameters>
               <asp:Parameter Name="CouponCode" Type="String" />
               <asp:Parameter Name="CouponDescription" Type="String" />
               <asp:Parameter Name="CouponStartDate" Type="DateTime" />
               <asp:Parameter Name="CouponEndDate" Type="DateTime" />
               <asp:Parameter Name="CouponSpecialRequirements" Type="String" />
               <asp:Parameter Name="CouponOrderMinimum" Type="Decimal" />
               <asp:Parameter Name="CouponExclusive" Type="String" />
               <asp:Parameter Name="CouponType" Type="String" />
               <asp:Parameter Name="CouponVariable" Type="String" />
               <asp:Parameter Name="CouponOperand" Type="String" />
               <asp:Parameter Name="CouponAmount" Type="String" />
               <asp:Parameter Name="CouponNotes" Type="String" />
               <asp:Parameter Name="CouponUsageInstances" Type="Int32" />
            </InsertParameters>
         </asp:ObjectDataSource>
         </td>
      </tr>
   </table>
   
</asp:Content>

