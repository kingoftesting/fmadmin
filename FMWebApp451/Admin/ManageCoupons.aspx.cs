using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;

namespace FaceMaster.Admin
{
    public partial class ManageCoupons : AdminBasePage //System.Web.UI.Page
   {
       private string GridViewSortDirection
       {
           get { return ViewState["SortDirection"] as string ?? "DESC"; }
           set { ViewState["SortDirection"] = value; }
       }

       protected void Page_Load(object sender, EventArgs e)
       {

           if (!this.IsPostBack)
           {
               Certnet.Cart tempcart = new Certnet.Cart(); //just need the current user ID
               tempcart.Load(Session["cart"]);
               int customerID = tempcart.SiteCustomer.CustomerID;
               try
               {
                   if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "CUSTOMERSERVICE")))
                   { }
                   else { throw new SecurityException("You do not have the apppropriate creditentials to complete this task."); }
               }
               catch (SecurityException ex)
               {
                   string msg = "Error in ManageStressResistance" + Environment.NewLine;
                   msg += "User did not have permission to access page" + Environment.NewLine;
                   msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                   FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                   Response.Redirect("Login.aspx");
               }

               if (dvwCoupon.CurrentMode == DetailsViewMode.Insert)
               {
                   TextBox txt = (TextBox)dvwCoupon.FindControl("txtUsageInstances");
                   if (txt != null) { txt.Text = "0"; }

                   Controls_RDatePicker dt1 = (Controls_RDatePicker)dvwCoupon.FindControl("RDatePicker1");
                   if (dt1 != null) { dt1.SelectedDate = DateTime.Today.ToShortDateString(); }

                   Controls_RDatePicker dt2 = (Controls_RDatePicker)dvwCoupon.FindControl("RDatePicker2");
                   if (dt2 != null) { dt2.SelectedDate = DateTime.Today.ToShortDateString(); }
               }

           }
       }

        private void DeselectCoupon()
      {
          gvwCoupon.SelectedIndex = -1;
          gvwCoupon.DataBind();
          dvwCoupon.ChangeMode(DetailsViewMode.Insert);
      }

        protected void gvwCoupon_SelectedIndexChanged(object sender, EventArgs e)
       {
           dvwCoupon.ChangeMode(DetailsViewMode.Edit);
       }

        protected void gvwCoupon_RowDeleted(object sender, GridViewDeletedEventArgs e)
       {
           DeselectCoupon();
       }

        protected void gvwCoupon_RowCommand(object sender, GridViewCommandEventArgs e)
       {
           DeselectCoupon();
       }

        protected void gvwCoupon_RowCreated(object sender, GridViewRowEventArgs e)
       {
       }


        protected void dvwCoupon_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
       {
           DeselectCoupon();
       }

        protected void dvwCoupon_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
       {
           DeselectCoupon();
       }

        protected void dvwCoupon_ItemCommand(object sender, DetailsViewCommandEventArgs e)
       {
           if (e.CommandName == "Cancel")
               DeselectCoupon();
       }

        protected void dvwCoupon_ItemCreated(object sender, EventArgs e)
       {
           foreach (Control ctl in dvwCoupon.Rows[dvwCoupon.Rows.Count - 1].Controls[0].Controls)
           {
               if (ctl is LinkButton)
               {
                   LinkButton lnk = ctl as LinkButton;
                   if (lnk.CommandName == "Insert" || lnk.CommandName == "Update")
                       lnk.ValidationGroup = "Coupon";
               }
           }
       }

        protected void gvwCoupon_Sorting(object sender, GridViewSortEventArgs e)
       {
           DataView dv = Session["grvDataSource"] as DataView;

           if (dv != null)
           {

               string m_SortDirection = GetSortDirection();
               dv.Sort = e.SortExpression + " " + m_SortDirection;

               gvwCoupon.DataSource = dv;
               gvwCoupon.DataBind();
           }
       }

       private string GetSortDirection()
       {
           switch (GridViewSortDirection)
           {
               case "ASC":
                   GridViewSortDirection = "DESC";
                   break;

               case "DESC":

                   GridViewSortDirection = "ASC";
                   break;
           }
           return GridViewSortDirection;
       }

   }
}
