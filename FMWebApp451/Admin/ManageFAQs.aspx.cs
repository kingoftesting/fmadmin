using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Certnet;
using FM2015.Helpers;

namespace FaceMaster.Admin
{

    public partial class ManageFAQs : AdminBasePage //System.Web.UI.Page
    {
        string FaqID = "";

        Cart cart;

        void Page_Init(Object sender, EventArgs e)
        {
            cart = new Cart();
            cart.Load(Session["cart"]);
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            int customerID = cart.SiteCustomer.CustomerID;
            Session["isADMIN"] = ValidateUtil.IsInRole(customerID, "ADMIN");
             
            
            FaqID = Request["listingID"];
            if (!IsPostBack)
                BindGrid();
        }

        protected bool IsAdmin()
        {
            bool temp = Convert.ToBoolean(Session["isADMIN"]);
            return temp;

        }
        
        public void btnSearch_Click(object sender, System.EventArgs e)
        {
            //reset to show first page
            gridPages.PageIndex = 0;
            BindGrid();
        }

        public void BindGrid()
        {
             String sql = @"
                SELECT FaqID, Category, Question, Answer, SortOrder,
                CASE Enabled WHEN 1 THEN 'Yes' ELSE 'No' END 'Enabled'
                FROM Faqs 
                WHERE 0 = 0 
            ";

            string whereClause = "";
            SqlParameter[] mySqlParameters = null;

            if (!ValidateUtil.IsNumeric(FaqID)) { FaqID = ""; } //don't pass a bogus param

            string keywords = txtKeywords.Text;
            keywords = ValidateUtil.CleanUpString(keywords);

            if (!string.IsNullOrEmpty(FaqID))
            {
                sql += "AND FaqID = @FaqID";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, FaqID);
                lnkAddEditFAQs.NavigateUrl = "AddEditFAQs.aspx?listingID=" + FaqID;
                sql += whereClause;
            }
            else if (keywords != null && keywords != "")
            {
                keywords = keywords.Replace("'", "''");
                keywords = "%" + keywords + "%";
                whereClause += " AND (Category LIKE @Category";
                whereClause += " OR Question LIKE @Question";
                whereClause += " OR Answer LIKE @Answer)";
                sql += whereClause;
                lnkAddEditFAQs.NavigateUrl = "AddEditFAQs.aspx";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, keywords, keywords, keywords);
            }


            if (Session["adminSorterFaqs"] == null) Session["adminSorterFaqs"] = "FaqID";
            if (Session["adminDirFaqs"] == null) Session["adminDirFaqs"] = " DESC";

            DataSet ds = null;
            if (mySqlParameters == null) { ds = DBUtil.FillDataSet(sql, "FAQs"); }
            else { ds = FM2015.Helpers.DBUtil.FillDataSet(sql, "FAQs", mySqlParameters); }

            //process dataset to hilite the keyword
            if (!string.IsNullOrEmpty(keywords))
            {
                keywords = keywords.Replace("%", string.Empty);
                string hiliteKeyword = "<span style='background-color:yellow'>" + keywords + "</span>";
                string evStr = "";
                for (int i = 0; i < ds.Tables["FAQs"].Rows.Count; i++)
                {
                    evStr = ds.Tables["FAQs"].Rows[i]["Question"].ToString();
                    //evStr = evStr.Replace(keywords, hiliteKeyword);
                    evStr = Regex.Replace(evStr, keywords, hiliteKeyword, RegexOptions.IgnoreCase);
                    ds.Tables["FAQs"].Rows[i]["Question"] = evStr;

                    evStr = ds.Tables["FAQs"].Rows[i]["Answer"].ToString();
                    //evStr = evStr.Replace(keywords, hiliteKeyword);
                    evStr = Regex.Replace(evStr, keywords, hiliteKeyword, RegexOptions.IgnoreCase);
                    ds.Tables["FAQs"].Rows[i]["Answer"] = evStr;
                }
            }

            DataView dv = ds.Tables[0].DefaultView;
            dv.Sort = (string)Session["adminSorterFaqs"] + (string)Session["adminDirFaqs"];
            gridPages.DataSource = dv;
            gridPages.DataBind();

            Double x = gridPages.PageIndex + 1;
            Double y = gridPages.PageCount;
            if (y > 0) lblMessage.Text = "Viewing Page " + x.ToString() + " of " + y;
        }

        public void gridPages_PageIndexChanged(object source, EventArgs e)
        {
            BindGrid();
        }

        public void gridPages_PageIndexChanging(object source, GridViewPageEventArgs e)
        {
            int i = gridPages.PageIndex;
            gridPages.PageIndex = e.NewPageIndex;
        }

        public void gridPages_Sorting(object sender, GridViewSortEventArgs e)
        {
            Session["adminSorterFaqs"] = (string)e.SortExpression;
            if (Session["adminDirFaqs"].ToString() == " ASC")
                Session["adminDirFaqs"] = " DESC";
            else
                Session["adminDirFaqs"] = " ASC";

            BindGrid();
        }
    }
}
