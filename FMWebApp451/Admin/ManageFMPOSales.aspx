<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="ManageFMPOSales" Title="Manage PO Sales" Codebehind="ManageFMPOSales.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="sectiontitle">Manage FMPOSales</div>
<p></p>
<asp:GridView ID="gvwFMPOSales" runat="server"
AutoGenerateColumns="False"
DataSourceID="objFMPOSales"
Width="100%"
DataKeyNames="ID"
OnRowCreated="gvwFMPOSales_RowCreated"
OnSelectedIndexChanged="gvwFMPOSales_SelectedIndexChanged"
OnRowCommand="gvwFMPOSales_RowCommand">

<Columns>
<asp:BoundField DataField="ID" HeaderText="ID" HeaderStyle-HorizontalAlign= " center " />
<asp:BoundField DataField="Date" HeaderText="Date" HeaderStyle-HorizontalAlign= " center " />
<asp:BoundField DataField="Customer" HeaderText="Customer" HeaderStyle-HorizontalAlign= " center " />
<asp:BoundField DataField="DevRevenue" HeaderText="DevRevenue" HeaderStyle-HorizontalAlign= " center " />
<asp:BoundField DataField="DevUnits" HeaderText="DevUnits" HeaderStyle-HorizontalAlign= " center " />
<asp:BoundField DataField="AccyRevenue" HeaderText="AccyRevenue" HeaderStyle-HorizontalAlign= " center " />
<asp:CommandField ButtonType="Image" SelectImageUrl="~/Images/Edit.gif" SelectText="Edit" ShowSelectButton="True">
<ItemStyle HorizontalAlign="Center" Width="20px" />
</asp:CommandField>
</Columns>

<EmptyDataTemplate><b>No FMPOSales Data Available</b></EmptyDataTemplate>
</asp:GridView>

<asp:ObjectDataSource ID="objFMPOSales" runat="server" SelectMethod="GetFMPOSalesList"
TypeName="FMPOSales">
</asp:ObjectDataSource>

<p></p>
<asp:DetailsView ID="dvwFMPOSales" runat="server"
AutoGenerateRows="False"
DataSourceID="objCurrFMPOSales"
Width="100%"
AutoGenerateEditButton="True"
AutoGenerateInsertButton="True"
HeaderText="FMPOSales Details"
DataKeyNames="ID"
DefaultMode="Insert"
OnItemCommand="dvwFMPOSales_ItemCommand"
OnItemInserted="dvwFMPOSales_ItemInserted"
OnItemUpdated="dvwFMPOSales_ItemUpdated"
OnItemCreated="dvwFMPOSales_ItemCreated">
<FieldHeaderStyle Width="100px" />
<Fields>
<asp:TemplateField HeaderText="ID">
<ItemTemplate>
<asp:Label ID="lblID" Visible="true" Text='<%# Eval("ID") %>' runat="server"></asp:Label>
</ItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="Date">
<ItemTemplate>
<asp:Label ID="lblDate" runat="server" Text='<%# Eval("Date") %>'></asp:Label>
</ItemTemplate>
<EditItemTemplate>
<asp:TextBox ID="txtDate" runat="server" Text='<%# Bind("Date") %>' Width="20%"></asp:TextBox>
</EditItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="Customer">
<ItemTemplate>
<asp:Label ID="lblCustomer" runat="server" Text='<%# Eval("Customer") %>'></asp:Label>
</ItemTemplate>
<EditItemTemplate>
<asp:TextBox ID="txtCustomer" runat="server" Text='<%# Bind("Customer") %>' Width="20%"></asp:TextBox>
</EditItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="DevRevenue">
<ItemTemplate>
<asp:Label ID="lblDevRevenue" runat="server" Text='<%# Eval("DevRevenue") %>'></asp:Label>
</ItemTemplate>
<EditItemTemplate>
<asp:TextBox ID="txtDevRevenue" runat="server" Text='<%# Bind("DevRevenue") %>' Width="20%"></asp:TextBox>
</EditItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="DevUnits">
<ItemTemplate>
<asp:Label ID="lblDevUnits" runat="server" Text='<%# Eval("DevUnits") %>'></asp:Label>
</ItemTemplate>
<EditItemTemplate>
<asp:TextBox ID="txtDevUnits" runat="server" Text='<%# Bind("DevUnits") %>' Width="20%"></asp:TextBox>
</EditItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="AccyRevenue">
<ItemTemplate>
<asp:Label ID="lblAccyRevenue" runat="server" Text='<%# Eval("AccyRevenue") %>'></asp:Label>
</ItemTemplate>
<EditItemTemplate>
<asp:TextBox ID="txtAccyRevenue" runat="server" Text='<%# Bind("AccyRevenue") %>' Width="20%"></asp:TextBox>
</EditItemTemplate>
</asp:TemplateField>

</Fields>
</asp:DetailsView>

<asp:ObjectDataSource ID="objCurrFMPOSales" runat="server"
TypeName="FMPOSales"
SelectMethod="GetFMPOSalesByID"
UpdateMethod="UpdateFMPOSales"
InsertMethod="InsertFMPOSales">
<SelectParameters>
<asp:ControlParameter ControlID="gvwFMPOSales" Name="id" PropertyName="SelectedValue" Type="Int32" />
</SelectParameters>
<UpdateParameters>
<asp:Parameter name="id" Type="Int32" />
<asp:Parameter name="Date" Type="String" />
<asp:Parameter name="Customer" Type="String" />
<asp:Parameter name="DevRevenue" Type="Decimal" />
<asp:Parameter name="DevUnits" Type="Int32" />
<asp:Parameter name="AccyRevenue" Type="Decimal" />
</UpdateParameters>
<InsertParameters>
<asp:Parameter name="Date" Type="String" />
<asp:Parameter name="Customer" Type="String" />
<asp:Parameter name="DevRevenue" Type="Decimal" />
<asp:Parameter name="DevUnits" Type="Int32" />
<asp:Parameter name="AccyRevenue" Type="Decimal" />
</InsertParameters>
</asp:ObjectDataSource>

</asp:Content>

