using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;




public partial class ManageFMPOSales : AdminBasePage //System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
    }


    private void DeselectFMPOSales()
    {
        gvwFMPOSales.SelectedIndex = -1;
        gvwFMPOSales.DataBind();
        dvwFMPOSales.ChangeMode(DetailsViewMode.Insert);
    }


    protected void gvwFMPOSales_SelectedIndexChanged(object sender, EventArgs e)
    {
        dvwFMPOSales.ChangeMode(DetailsViewMode.Edit);
    }


    protected void gvwFMPOSales_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        DeselectFMPOSales();
    }


    protected void gvwFMPOSales_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }


    protected void gvwFMPOSales_RowCreated(object sender, GridViewRowEventArgs e)
    {
    }


    protected void dvwFMPOSales_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        DeselectFMPOSales();
    }


    protected void dvwFMPOSales_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        DeselectFMPOSales();
    }


    protected void dvwFMPOSales_ItemCommand(object sender, DetailsViewCommandEventArgs e)
    {
        if (e.CommandName == "Cancel")
            DeselectFMPOSales();
    }


    protected void dvwFMPOSales_ItemCreated(object sender, EventArgs e)
    {
        foreach (Control ctl in dvwFMPOSales.Rows[dvwFMPOSales.Rows.Count - 1].Controls[0].Controls)
        {
            if (ctl is LinkButton)
            {
                LinkButton lnk = ctl as LinkButton;
                if (lnk.CommandName == "Insert" || lnk.CommandName == "Update")
                    lnk.ValidationGroup = "FMPOSales";
            }
        }
    }
}

