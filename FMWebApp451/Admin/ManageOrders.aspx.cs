using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using AdminCart;

    public partial class ManageOrders : AdminBasePage
    {
        DataView Orders = null;

        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "DESC"; }
            set { ViewState["SortDirection"] = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                CanUserProceed(new string[4] { "ADMIN", "CUSTOMERSERVICE", "REPORTS", "MARKETING" });

                DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                23, 59, 59);

                //#1 = From: Date; #2 = To: Date 
                RDatePicker2.SelectedDate = time1.ToShortDateString();
                RDatePicker1.SelectedDate = time1.Subtract(new TimeSpan(0, 23, 59, 59)).ToShortDateString();

                btnGetCSV.Visible = false; //don't show Email & CSV button until there's data displayed

                List<int> list = ShopifyAgent.ShopifyAgent.SyncShopify(); //sync orders from Shopify
 
                if (Request["CustomerID"] != null)
                {
                    ddlSearchTypes.SelectedValue = "6"; //CustomerID
                    txtOrderID.Text = Convert.ToInt32(Request["CustomerID"]).ToString();
                    BindOrders();
                }

            }
        }

        private void BindOrders()
        {

            string searchmethod = ""; //default = no method
            string searchparameter = ValidateUtil.CleanUpString(txtOrderID.Text);

            switch (ddlSearchTypes.SelectedValue)
            {
                case "1": //OrderID (FM or HDI or Shopify)
                    if (ValidateUtil.IsNumeric(searchparameter)) { searchmethod = "byOrderID"; }
                    else 
                    {
                        searchmethod = "byLastName";
                        searchparameter = "%" + searchparameter + "%"; 
                    }
                    break;

                /*
                case "HDI OrderID":
                    if (ValidateUtil.IsNumeric(searchparameter)) { searchmethod = "byHDIOrderID"; }
                    break;
                */

                case "2": //LastName
                    searchmethod = "byLastName";
                    searchparameter = "%" + searchparameter + "%";
                    break;

                case "3": //Serial#
                    searchmethod = "bySerialNumber";
                    searchparameter = "%" + searchparameter + "%";
                    break;

                case "4": //E-mail
                    searchmethod = "byEmail";
                    searchparameter = "%" + searchparameter + "%";
                    break;

                case "5": //Phone
                    searchmethod = "byPhone";
                    searchparameter = "%" + searchparameter + "%";
                    break;

                case "6": //CustomerID
                    if (ValidateUtil.IsNumeric(searchparameter)) { searchmethod = "byFMCustomerID"; }
                    break;

                default:
                    searchmethod = "";
                    break;
            }

            gvwOrderList.Attributes.Add("SearchMethod", searchmethod);
            gvwOrderList.Attributes.Add("SearchParameter", searchparameter);

            FillOrders(searchmethod, searchparameter);

            Orders.Sort = "OrderID DESC";
            Session["grvDataSource"] = Orders; //save for later; used to Sort, CSV download, Email
            gvwOrderList.DataSource = Orders;
            gvwOrderList.DataBind();

            if (Orders.Table.Rows.Count == 0) //hide CSV buttons if empty table
            { btnGetCSV.Visible = false; }
            else
            {
                string FromDate = "";
                if (!string.IsNullOrEmpty(RDatePicker1.SelectedDate)) { FromDate = RDatePicker1.SelectedDate + " 12:00AM"; }
                else { FromDate = "1/1/1900"; }

                string ToDate = "";
                if (!string.IsNullOrEmpty(RDatePicker2.SelectedDate)) { ToDate = RDatePicker2.SelectedDate + " 11:59PM"; }
                else { ToDate = DateTime.Now.ToString(); }
                btnGetCSV.Visible = true;
            }
        }

        protected void btnOrderLookup_Click(object sender, EventArgs e)
        {
            BindOrders();
        }

        protected void lnkbtnYesterday_Click(object sender, EventArgs e)
        {
            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                            23, 59, 59);

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.ToShortDateString();
            RDatePicker1.SelectedDate = time1.Subtract(new TimeSpan(1, 23, 59, 59)).ToShortDateString();
            txtOrderID.Text = "";
            BindOrders();
        }

        protected void lnkbtn7Day_Click(object sender, EventArgs e)
        {
            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                            23, 59, 59);

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.ToShortDateString();
            RDatePicker1.SelectedDate = time1.Subtract(new TimeSpan(7, 23, 59, 59)).ToShortDateString();
            txtOrderID.Text = "";
            BindOrders();
        }

        protected void lnkbtn30Day_Click(object sender, EventArgs e)
        {
            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                            23, 59, 59);

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.ToShortDateString();
            RDatePicker1.SelectedDate = time1.Subtract(new TimeSpan(30, 23, 59, 59)).ToShortDateString();
            txtOrderID.Text = "";
            BindOrders();
        }

        protected void btnCSV_Click(object sender, EventArgs e)
        {
            Orders = Session["grvDataSource"] as DataView;
            Orders.Sort = "OrderID DESC";
            Utilities.FMHelpers.SendCSV(Orders);
        }


        protected void FillOrders(string searchmethod, string searchparameter)
        {
            string FromDate = "1/1/2000" + " 12:00:00AM";
            string ToDate = DateTime.Now.ToString();

            //use Date Range if a parameter wasn't specified or if the UseDates checkbox is checked
            if ( (string.IsNullOrEmpty(txtOrderID.Text)) || (chkUseDates.Checked) )
            {
                if (!string.IsNullOrEmpty(RDatePicker1.SelectedDate)) { FromDate = RDatePicker1.SelectedDate + " 12:00:00AM"; }
                if (!string.IsNullOrEmpty(RDatePicker2.SelectedDate)) { ToDate = RDatePicker2.SelectedDate + " 11:59:59PM"; }
            }            
            
            Orders = Order.FindAllOrders(FromDate, ToDate, searchmethod, searchparameter);
        }

        

        protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataView dv = Session["grvDataSource"] as DataView;

            if (dv != null)
            {

                string m_SortDirection = GetSortDirection();
                dv.Sort = e.SortExpression + " " + m_SortDirection;

                gvwOrderList.DataSource = dv;
                gvwOrderList.DataBind();
            }
        }

        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;

                case "DESC":

                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }
    }