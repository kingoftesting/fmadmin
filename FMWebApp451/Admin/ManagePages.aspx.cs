using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Certnet;

namespace FaceMaster.Admin
{

    public partial class Admin_ManagePages : AdminBasePage //System.Web.UI.Page
    {
        private bool _userCanEdit = false;
        protected bool UserCanEdit
        {
            get { return _userCanEdit; }
            set { _userCanEdit = value; }
        }


        Cart cart;

        void Page_Init(Object sender, EventArgs e)
        {
            cart = new Cart();
            cart.Load(Session["cart"]);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            myPages.ConnectionString = AdminCart.Config.ConnStr();
        }

        protected bool IsAdmin()
        {
            int customerID = cart.SiteCustomer.CustomerID;
            return ValidateUtil.IsInRole(customerID, "Admin");

        }

    }
}

