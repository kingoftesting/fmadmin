<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="ManageProducts" Title="Manage Products" Codebehind="ManageProducts.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="sectiontitle" align="left">FaceMaster Products Details</div>
   <p></p>
   
    <p align="left">
        <asp:Button ID="btnNew" runat="server" Text="ENTER NEW PRODUCT" OnClick="btnNew_Click" ValidationGroup="ListByDate" />
    </p>

    

   <asp:GridView 
    ID="gvwDataList"
    SkinID="Admin"
    runat="server" 
    EmptyDataText="No products in the database!" 
    AutoGenerateColumns="false" 
    Width="100%"
    AllowSorting="true"
    OnSorting="gridView_Sorting"
    OnRowCreated="gvwUsers_RowCreated"
    OnPageIndexChanging="gvwDataList_PageIndexChanging" 
    OnPageIndexChanged="gvwDataList_PageIndexChanged" 
    EnableViewState="False">
	<HeaderStyle />
    <AlternatingRowStyle />
	<PagerStyle />
	    <Columns>
            <asp:HyperLinkField Text="Edit" DataNavigateUrlFormatString="AddEditProduct.aspx?ProductID={0}" DataNavigateUrlFields="ProductID" />
		    <asp:BoundField HeaderText="ID" DataField="ProductID" SortExpression="ProductID" HeaderStyle-HorizontalAlign="Center" />
		    <asp:BoundField HeaderText="Sku" DataField="Sku" SortExpression="Sku" HeaderStyle-HorizontalAlign="Center" />								
		    <asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" HeaderStyle-HorizontalAlign="Center" />
		    <asp:BoundField HeaderText="Price" DataField="Price" SortExpression="Price" DataFormatString="{0:$#,##0.00}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Center" />
		    <asp:BoundField HeaderText="Sale Price" DataField="SalePrice" SortExpression="SalePrice" DataFormatString="{0:$#,##0.00}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Center" />
		    <asp:BoundField HeaderText="Cost" DataField="Cost" SortExpression="Cost" DataFormatString="{0:$#,##0.00}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Center" />														
		    <asp:BoundField HeaderText="FMSystem" DataField="IsFMSystem" SortExpression="IsFMSystem" HeaderStyle-HorizontalAlign="Center" />														
		    <asp:BoundField HeaderText="Bundle?" DataField="IsBundle" SortExpression="IsBundle" HeaderStyle-HorizontalAlign="Center" />														
		    <asp:BoundField HeaderText="Enabled" DataField="Enabled" SortExpression="Enabled" HeaderStyle-HorizontalAlign="Center" />														
         </Columns>
        <EmptyDataTemplate><b>No users found for the specified criteria</b></EmptyDataTemplate>
   </asp:GridView>
   
    <p></p>     
    <br />
    
    <p align="left">
    <asp:Button ID="btnGetCSV" runat="server" Text="Download Spreadsheet" OnClick="btnCSV_Click" ValidationGroup="ListByDate" />
    </p>



</asp:Content>


