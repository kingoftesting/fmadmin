using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using AdminCart;


public partial class ManageProducts : AdminBasePage //System.Web.UI.Page
{
    DataView products = new DataView();

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "DESC"; }
        set { ViewState["SortDirection"] = value; }
    }

    void Page_Init(Object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Certnet.Cart cart = new Certnet.Cart();
            cart.Load(Session["cart"]);
            int customerID = cart.SiteCustomer.CustomerID;
            try
            {
                if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "REPORTS")))
                { }
                else
                {
                    throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
                }
            }
            catch (SecurityException ex)
            {
                string msg = "Error in ManageProducts" + Environment.NewLine;
                msg += "User did not have permission to access page" + Environment.NewLine;
                msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                Response.Redirect("../Login.aspx");
            }

            List<Product> pList = Product.GetProducts();
            DataTable pTable = Utilities.FMHelpers.BuildDataTable<Product>(pList);
            products = new DataView(pTable);
            BindProducts();
            btnGetCSV.Visible = false; //don't show CSV button until there's data displayed
        }
    }

    private void BindProducts()
    {
        //FillProducts();
        products.Sort = "ENABLED" + " " + "DESC";
        gvwDataList.DataSource = products;
        Session["grvDataSource"] = products; //save for later; used to Sort, CSV download, Email
        gvwDataList.DataBind();

        if (products.Table.Rows.Count == 0) //hide Email & CSV buttons if empty table
        { btnGetCSV.Visible = false; }
        else
        { btnGetCSV.Visible = true; }
    }


    protected void btnCSV_Click(object sender, EventArgs e)
    {
        products = Session["grvDataSource"] as DataView;
        products.Sort = "Date DESC";
        Utilities.FMHelpers.SendCSV(products);
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddEditProduct.aspx?ProductID=0");
    }

    protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataView dv = Session["grvDataSource"] as DataView;

        if (dv != null)
        {

            string m_SortDirection = GetSortDirection();
            dv.Sort = e.SortExpression + " " + m_SortDirection;

            gvwDataList.DataSource = dv;
            gvwDataList.DataBind();
        }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;

            case "DESC":

                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    public void gvwDataList_PageIndexChanging(object source, GridViewPageEventArgs e)
    {
        gvwDataList.PageIndex = e.NewPageIndex;
        //gvwDataList.DataBind();
        BindProducts();
    }

    public void gvwDataList_PageIndexChanged(object source, EventArgs e)
    {
    }

    protected void gvwDataList_RowEditing(object sender, EventArgs e)
    {
    }

    protected void gvwUsers_RowCreated(object sender, EventArgs e)
    {
    }

}

