<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="Admin_ManageReports" Title="Reports" Codebehind="ManageReports.aspx.cs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

   <div class="sectiontitle">Reports</div>
   <p></p>


   <ul style="list-style-type: square; text-align:left;">
   </ul>
   <hr />
   <p></p>
   
   <ul style="list-style-type: square; text-align:left">
    <li><asp:HyperLink ID="lnkErrLog" runat="server" NavigateUrl="~/Admin/RptListErrLog.aspx">Report - Database Error Log</asp:HyperLink></li>
    <li><asp:HyperLink ID="lnkPhoneSales" runat="server" NavigateUrl="~/Admin/WebPhoneCER.aspx">Report - Web & Phone Sales</asp:HyperLink></li>
    <li><asp:HyperLink ID="lnkSalesCompare" runat="server" NavigateUrl="~/Admin/SalesCompare.aspx">Report - Sales Growth & Trends</asp:HyperLink></li>
   </ul>
    <br /><br />

</asp:Content>

