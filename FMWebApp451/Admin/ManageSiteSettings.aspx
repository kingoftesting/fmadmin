<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" ValidateRequest="false" AutoEventWireup="true" Inherits="FaceMaster.Admin.ManageSiteSettings" Title="Manage SiteSettings" Codebehind="ManageSiteSettings.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="RDatePicker" Src="~/controls/RDatePicker.ascx" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" Runat="Server">   
   <div class="sectiontitle">FaceMaster SiteSetting Management</div>
   <p></p>
   <asp:GridView ID="gvwSiteSettings" runat="server" 
   AutoGenerateColumns="False" 
   DataSourceID="objSiteSettings" 
   Width="100%" 
   DataKeyNames="SiteSettingsID"  
   AllowSorting="true"
   OnRowCreated="gvwSiteSettings_RowCreated" 
   OnRowDeleted="gvwSiteSettings_RowDeleted" 
   OnSelectedIndexChanged="gvwSiteSettings_SelectedIndexChanged" 
   OnRowCommand="gvwSiteSettings_RowCommand">
      <Columns>
         <asp:BoundField HeaderText="ID" DataField="SiteSettingsID" SortExpression="SiteSettingsID" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Name" DataField="SiteSettingsName" SortExpression="SiteSettingsName" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Description" DataField="SiteSettingsDescription" SortExpression="SiteSettingsName" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Value" DataField="SiteSettingsValue" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="FreeShip?" DataField="SiteSettingsFreeShipping" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="Trigger" DataField="SiteSettingsTrigger" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="TriggerValue" DataField="SiteSettingsTriggerValue" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="MinProduct" DataField="SiteSettingsMinProduct" DataFormatString="{0:C}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="StartDate" DataField="SiteSettingsStartDate" SortExpression="SiteSettingsStartDate" DataFormatString="{0:MM/dd/yy h:mm tt}" HeaderStyle-HorizontalAlign="Center"/>
         <asp:BoundField HeaderText="EndDate" DataField="SiteSettingsEndDate" SortExpression="SiteSettingsEndDate" DataFormatString="{0:MM/dd/yy h:mm tt}" HeaderStyle-HorizontalAlign="Center"/>
          <asp:CommandField ButtonType="Image" SelectImageUrl="~/Images/Edit.gif" SelectText="Edit poll" ShowSelectButton="True">
            <ItemStyle HorizontalAlign="Center" Width="20px" />
         </asp:CommandField>
      </Columns>
      <EmptyDataTemplate><b>No SiteSettings to show</b></EmptyDataTemplate>   
   </asp:GridView>
   <asp:ObjectDataSource ID="objSiteSettings" runat="server" 
      TypeName="AdminCart.SiteSettings" 
      SelectMethod="GetSiteSettingsList">
   </asp:ObjectDataSource>
   <p></p>
   <table width="100%">
      <tr>
         <td style="width:100%" valign="top">
         <asp:DetailsView ID="dvwSiteSettings" runat="server" 
         AutoGenerateRows="False" 
         DataSourceID="objCurrSiteSettings" 
         Width="100%" 
         AutoGenerateEditButton="True" 
         AutoGenerateInsertButton="True" 
         HeaderText="SiteSetting Details" 
         DataKeyNames="SiteSettingsID" 
         DefaultMode="Insert" 
         OnItemCommand="dvwSiteSettings_ItemCommand" 
         OnItemInserted="dvwSiteSettings_ItemInserted" 
         OnItemUpdated="dvwSiteSettings_ItemUpdated" 
         OnItemCreated="dvwSiteSettings_ItemCreated">
            <FieldHeaderStyle Width="100px" />
            <Fields>
               <asp:BoundField DataField="SiteSettingsID" HeaderText="ID" ReadOnly="True" InsertVisible="False" />

               <asp:TemplateField HeaderText="Name">
                  <ItemTemplate>
                     <asp:Label ID="lblSiteSettingsName" runat="server" Text='<%# Eval("SiteSettingsName") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="txtSiteSettingsName" runat="server" Text='<%# Bind("SiteSettingsName") %>' MaxLength="50" Width="125px"></asp:TextBox>
		             <asp:RequiredFieldValidator ID="valtxtSiteSettingsName" runat="server" ControlToValidate="txtSiteSettingsName" SetFocusOnError="true"
                        Text="The Name field is required." ToolTip="The Name field is required." Display="Dynamic"></asp:RequiredFieldValidator>
                  </EditItemTemplate>               
               </asp:TemplateField>

               <asp:TemplateField HeaderText="Description">
                  <ItemTemplate>
                     <asp:Label ID="lblSiteSettingsDescription" runat="server" Text='<%# Eval("SiteSettingsDescription") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
		            <asp:TextBox width="500px" ID="txtSiteSettingsDescription" TextMode="MultiLine" Columns="100" Rows="7" Text='<%# Bind("SiteSettingsDescription") %>' Runat="server" MaxLength="500" />
		            <asp:RequiredFieldValidator ID="valtxtSiteSettingsDescription" runat="server" ControlToValidate="txtSiteSettingsDescription" SetFocusOnError="true"
                        Text="The Description field is required." ToolTip="The Description field is required." Display="Dynamic"></asp:RequiredFieldValidator>
                  </EditItemTemplate>               
               </asp:TemplateField>

               <asp:TemplateField HeaderText="Value($-off, %-off)">
                  <ItemTemplate>
                     <asp:Label ID="lblSiteSettingsValue" runat="server" Text='<%# Eval("SiteSettingsValue") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="txtSiteSettingsValue" runat="server" Text='<%# Bind("SiteSettingsValue") %>' MaxLength="50" Width="125px"></asp:TextBox>
		             <asp:RequiredFieldValidator ID="valtxtSiteSettingsValue" runat="server" ControlToValidate="txtSiteSettingsValue" SetFocusOnError="true"
                        Text="The Value field is required." ToolTip="The Value field is required." Display="Dynamic"></asp:RequiredFieldValidator>
                  </EditItemTemplate>               
               </asp:TemplateField>

               <asp:TemplateField HeaderText="Trigger">
                  <ItemTemplate>
                     <asp:Label ID="lblSiteSettingsTrigger" runat="server" Text='<%# Eval("SiteSettingsTrigger") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                      <asp:DropDownList ID="ddlSiteSettingsTrigger" runat="server" SelectedValue='<%# Bind("SiteSettingsTrigger") %>'>
                          <asp:ListItem Text="No Trigger" Value="0" Selected="True"></asp:ListItem>
                          <asp:ListItem Text="SubTotal Discount: Dollar Amount" Value="1"></asp:ListItem>
                          <asp:ListItem Text="SubTotal Discount: %-off Amount" Value="2"></asp:ListItem>
                          <asp:ListItem Text="LineItem Discount: Dollar Amount" Value="3"></asp:ListItem>
                          <asp:ListItem Text="LineItem Discount: %-off Amount" Value="4"></asp:ListItem>
                      </asp:DropDownList>
                  </EditItemTemplate>               
               </asp:TemplateField>
                                             
               <asp:TemplateField HeaderText="FreeShipping">
                  <ItemTemplate>
                     <asp:Label ID="lblSiteSettingsFreeShipping" runat="server" Text='<%# Eval("SiteSettingsFreeShipping") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                      <asp:DropDownList ID="ddlSiteSettingsFreeShipping" runat="server" SelectedValue='<%# Bind("SiteSettingsFreeShipping") %>'>
                          <asp:ListItem Text="TRUE" Value="True" Selected="True"></asp:ListItem>
                          <asp:ListItem Text="FALSE" Value="False"></asp:ListItem>
                      </asp:DropDownList>
                  </EditItemTemplate>               
               </asp:TemplateField>
 
                <asp:TemplateField HeaderText="TriggerValue">
                  <ItemTemplate>
                     <asp:Label ID="lblSiteSettingsTriggerValue" runat="server" Text='<%# Eval("SiteSettingsTriggerValue") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="txtSiteSettingsTriggerValue" runat="server" Text='<%# Bind("SiteSettingsTriggerValue") %>' MaxLength="50" Width="125px"></asp:TextBox>
                  </EditItemTemplate>
               </asp:TemplateField>

                <asp:TemplateField HeaderText="MinProduct">
                  <ItemTemplate>
                     <asp:Label ID="lblSiteSettingsMinProduct" runat="server" Text='<%# Eval("SiteSettingsMinProduct") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="txtSiteSettingsMinProduct" runat="server" Text='<%# Bind("SiteSettingsMinProduct") %>' MaxLength="50" Width="125px"></asp:TextBox>
                  </EditItemTemplate>
               </asp:TemplateField>

               <asp:TemplateField HeaderText="StartDate">
                  <ItemTemplate>
                     <asp:Label ID="lblSiteSettingsStartDate" runat="server" Text='<%# Eval("SiteSettingsTrigger") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <uc1:RDatePicker id="RDatePicker1" SelectedDate='<%# Bind("SiteSettingsStartDate", "{0:d}") %>' runat="server"></uc1:RDatePicker> 
                  </EditItemTemplate>
               </asp:TemplateField>

               <asp:TemplateField HeaderText="EndDate">
                  <ItemTemplate>
                     <asp:Label ID="lblSiteSettingsEndDate" runat="server" Text='<%# Eval("SiteSettingsEndDate", "{0:d}") %>'></asp:Label>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <uc1:RDatePicker id="RDatePicker2" SelectedDate='<%# Bind("SiteSettingsEndDate") %>' runat="server"></uc1:RDatePicker> 
                  </EditItemTemplate>
               </asp:TemplateField>
                            
            </Fields>
         </asp:DetailsView>
         <asp:ObjectDataSource ID="objCurrSiteSettings" runat="server" 
         TypeName="AdminCart.SiteSettings" 
         InsertMethod="InsertSiteSettings" 
         SelectMethod="GetSiteSettingsByID" 
         UpdateMethod="UpdateSiteSettings">
            <SelectParameters>
               <asp:ControlParameter ControlID="gvwSiteSettings" Name="SiteSettingsID" PropertyName="SelectedValue"
                  Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
               <asp:Parameter Name="SiteSettingsID" Type="Int32" />
               <asp:Parameter Name="SiteSettingsName" Type="String" />
               <asp:Parameter Name="SiteSettingsDescription" Type="String" />
               <asp:Parameter Name="SiteSettingsValue" Type="String" />
               <asp:Parameter Name="SiteSettingsFreeShipping" Type="Boolean" />
               <asp:Parameter Name="SiteSettingsTrigger" Type="String" />
               <asp:Parameter Name="SiteSettingsTriggerValue" Type="String" />
               <asp:Parameter Name="SiteSettingsMinProduct" Type="Decimal" />
               <asp:Parameter Name="SiteSettingsStartDate" Type="DateTime" />
               <asp:Parameter Name="SiteSettingsEndDate" Type="DateTime" />
           </UpdateParameters>
            <InsertParameters>
               <asp:Parameter Name="SiteSettingsName" Type="String" />
               <asp:Parameter Name="SiteSettingsDescription" Type="String" />
               <asp:Parameter Name="SiteSettingsValue" Type="String" />
               <asp:Parameter Name="SiteSettingsFreeShipping" Type="Boolean" />
               <asp:Parameter Name="SiteSettingsTrigger" Type="String" />
               <asp:Parameter Name="SiteSettingsTriggerValue" Type="String" />
               <asp:Parameter Name="SiteSettingsMinProduct" Type="Decimal" />
               <asp:Parameter Name="SiteSettingsStartDate" Type="DateTime" />
               <asp:Parameter Name="SiteSettingsEndDate" Type="DateTime" />
            </InsertParameters>
         </asp:ObjectDataSource>
         </td>
      </tr>
   </table>
   
</asp:Content>

