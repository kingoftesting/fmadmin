using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;

namespace FaceMaster.Admin
{
    public partial class ManageSiteSettings : AdminBasePage //System.Web.UI.Page
   {
       private string GridViewSortDirection
       {
           get { return ViewState["SortDirection"] as string ?? "DESC"; }
           set { ViewState["SortDirection"] = value; }
       }

       protected void Page_Load(object sender, EventArgs e)
       {

           if (!this.IsPostBack)
           {
               Certnet.Cart tempcart = new Certnet.Cart(); //just need the current user ID
               tempcart.Load(Session["cart"]);
               int customerID = tempcart.SiteCustomer.CustomerID;
               try
               {
                   if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "CUSTOMERSERVICE")))
                   { }
                   else { throw new SecurityException("You do not have the apppropriate creditentials to complete this task."); }
               }
               catch (SecurityException ex)
               {
                   string msg = "Error in ManageSiteSettings" + Environment.NewLine;
                   msg += "User did not have permission to access page" + Environment.NewLine;
                   msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                   FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                   Response.Redirect("Login.aspx");
               }
               if (dvwSiteSettings.CurrentMode == DetailsViewMode.Insert)
               {
                   TextBox txt = (TextBox)dvwSiteSettings.FindControl("txtSiteSettingsName");
                   if (txt != null) { txt.Text = "FreeShipping"; }
               }
                   
           }
       }

        private void DeselectSiteSettings()
      {
          gvwSiteSettings.SelectedIndex = -1;
          gvwSiteSettings.DataBind();
          dvwSiteSettings.ChangeMode(DetailsViewMode.Insert);
      }

        protected void gvwSiteSettings_SelectedIndexChanged(object sender, EventArgs e)
       {
           dvwSiteSettings.ChangeMode(DetailsViewMode.Edit);
       }

        protected void gvwSiteSettings_RowDeleted(object sender, GridViewDeletedEventArgs e)
       {
           DeselectSiteSettings();
       }

        protected void gvwSiteSettings_RowCommand(object sender, GridViewCommandEventArgs e)
       {
           DeselectSiteSettings();
       }

        protected void gvwSiteSettings_RowCreated(object sender, GridViewRowEventArgs e)
       {
       }


        protected void dvwSiteSettings_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
       {
           DeselectSiteSettings();
       }

        protected void dvwSiteSettings_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
       {
           DeselectSiteSettings();
       }

        protected void dvwSiteSettings_ItemCommand(object sender, DetailsViewCommandEventArgs e)
       {
           if (e.CommandName == "Cancel")
               DeselectSiteSettings();
       }

        protected void dvwSiteSettings_ItemCreated(object sender, EventArgs e)
       {
           foreach (Control ctl in dvwSiteSettings.Rows[dvwSiteSettings.Rows.Count - 1].Controls[0].Controls)
           {
               if (ctl is LinkButton)
               {
                   LinkButton lnk = ctl as LinkButton;
                   if (lnk.CommandName == "Insert" || lnk.CommandName == "Update")
                       lnk.ValidationGroup = "SiteSettings";
               }
           }
       }

        protected void gvwSiteSettings_Sorting(object sender, GridViewSortEventArgs e)
       {
           DataView dv = Session["grvDataSource"] as DataView;

           if (dv != null)
           {

               string m_SortDirection = GetSortDirection();
               dv.Sort = e.SortExpression + " " + m_SortDirection;

               gvwSiteSettings.DataSource = dv;
               gvwSiteSettings.DataBind();
           }
       }

       private string GetSortDirection()
       {
           switch (GridViewSortDirection)
           {
               case "ASC":
                   GridViewSortDirection = "DESC";
                   break;

               case "DESC":

                   GridViewSortDirection = "ASC";
                   break;
           }
           return GridViewSortDirection;
       }

   }
}
