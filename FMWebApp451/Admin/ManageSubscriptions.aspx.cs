﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminCart;
using PayWhirl;
using Stripe;
using FMWebApp.ViewModels.Stripe;
using System.Configuration;

namespace FaceMaster.Admin
{
    public partial class ManageSubscriptions : AdminBasePage
    {
        int CustomerID;

        protected void Page_Load(object sender, EventArgs e)
        {
            Cart cart = new Cart();

            if (!this.IsPostBack)
            {
                CanUserProceed(new string[4] { "ADMIN", "CUSTOMERSERVICE", "REPORTS", "MARKETING" });

                DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                23, 59, 59);



                if (Request["PageAction"] == null)
                {
                    CustomerID = 0;
                    if (Request["CustomerID"] != null)
                    {
                        Int32.TryParse(Request["CustomerID"].ToString(), out CustomerID);
                    }
                    Session["mngSubCustomerID"] = CustomerID;
                }
                else
                {
                    CustomerID = (Session["mngSubCustomerID"] == null) ? 0 : Convert.ToInt32(Session["mngSubCustomerID"].ToString());

                    string stripeCustomerID = (Request["StripeCustomerID"] == null) ? "0" : Request["StripeCustomerID"].ToString();
                    string stripeSubscriptionID = (Request["StripeSubscriptionID"] == null) ? "n/a" : Request["StripeSubscriptionID"].ToString();

                    if (Request["PageAction"].ToString().ToUpper() == "UNSUBSCRIBE")
                    {
                        if (Request["StripeSubscriptionID"] != null)
                        {
                            try
                            {
                                FM2015.Models.Subscriptions.Stripe_UnSubscribe(stripeCustomerID, stripeSubscriptionID);

                                //add a new entry into the CER database
                                Customer c = new Customer(CustomerID);
                                cart.SiteCustomer = c;
                                Address billAdr = Address.LoadAddress(CustomerID, 1);
                                cart.BillAddress = billAdr;
                                Address shipAdr = Address.LoadAddress(CustomerID, 2);
                                cart.ShipAddress= shipAdr;
                                adminCER.CER cer = new adminCER.CER(cart);
                                cer.EventDescription = "FaceMaster Subscription UNSUBSCRIBE" + Environment.NewLine;
                                cer.EventDescription += "Subscription ID: " + stripeSubscriptionID + Environment.NewLine;
                                cer.EventDescription += "Subscriber Stripe ID: " + stripeCustomerID + Environment.NewLine;
                                cer.EventDescription += "Unsubscribed By: " + cer.ReceivedBy + Environment.NewLine;
                                cer.ActionExplaination = "Unsubscribe Subscription or Multi-Pay";

                                int result = cer.Add();
                            }
                            catch(StripeException ex)
                            {
                                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "ManageSubscriptions: Unsubscribe: Stripe Error");
                            }
                        }
                    }
                    if (Request["PageAction"].ToString().ToUpper() == "CHARGE")
                    {
                        Response.Redirect("subscriptions/PayWhirlSubscriptions.aspx?PageAction=charge&stripeCustomerID=" + stripeCustomerID + "&stripeSubscriptionID=" + stripeSubscriptionID);
                    }

                }

                BindSubscriptions(CustomerID);
            }
        }

        private void BindSubscriptions(int customerID)
        {
            Customer customer = new Customer(customerID);
            //PayWhirlSubscriber sub = PayWhirlSubscriber.FindSubscriberByEmail(customer.Email);
            PayWhirlSubscriber sub = PayWhirlBiz.FindSubscriberByEmail(customer.Email);
            StripeCustomerViewModel cvm = new StripeCustomerViewModel();
            StripeSubscriptionListOptions stripeSubscriptionListOptions = new StripeSubscriptionListOptions()
            {
                CustomerId = sub.Stripe_id,
            };
            if (sub.Id > 0)
            {
                var subscriptionService = new StripeSubscriptionService(); //get list of all subscriptions for customer
                try
                {
                    //cvm.stripeCustomerSubscriptionList = subscriptionService.List(sub.Stripe_id);
                    cvm.stripeCustomerSubscriptionList = subscriptionService.List(stripeSubscriptionListOptions);
                }
                catch (StripeException ex)
                {
                    Utilities.FMHelpers.ProcessStripeException(ex, true, "", "ManageSubscriptions: BindCustomers: Stripe Error");
                }
            }
            try
            {
                gvwSubscriptionList.DataSource = cvm.stripeCustomerSubscriptionList;
                gvwSubscriptionList.DataBind();
            }
            catch (Exception ex)
            {
                string msg = "error in ManageSubscriptions: BindSubscriptions" + Environment.NewLine;
                msg += "CustomerID: " + customerID.ToString() + Environment.NewLine;
                msg += "PayWhirl SubscriberID: " + sub.Id.ToString() + Environment.NewLine;
                msg += "Stripe Subscriber ID: " + sub.Stripe_id.ToString() + Environment.NewLine;
                msg += "Subscription List: " + Environment.NewLine;
                foreach (StripeSubscription sList in cvm.stripeCustomerSubscriptionList)
                {
                    msg += "Stripe Subscription ID: " + sList.Id.ToString() + Environment.NewLine;
                    msg += "Stripe Subscription Plan: " + sList.StripePlan.Product.Name + Environment.NewLine; //*******
                    msg += "Stripe Subscription Start: " + sList.Start.ToString() + Environment.NewLine;
                    msg += "Stripe Subscription QTY: " + sList.Quantity.ToString() + Environment.NewLine;
                    msg += "Stripe Subscription PeriodStart: " + sList.CurrentPeriodStart.ToString() + Environment.NewLine; //*******
                    msg += "Stripe Subscription PeriodEnd: " + sList.CurrentPeriodEnd.ToString() + Environment.NewLine; //*******
                    msg += "Stripe Subscription CancelAtPeriodEnd: " + sList.CancelAtPeriodEnd.ToString() + Environment.NewLine;
                    msg += "Stripe Subscription Status: " + sList.Status.ToString() + Environment.NewLine;
                    msg += "Stripe Subscription Tax%: " + sList.TaxPercent.ToString() + Environment.NewLine;
                }
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                cvm = new StripeCustomerViewModel(); //reset the list for gridview (empty list)
                gvwSubscriptionList.DataSource = cvm.stripeCustomerSubscriptionList;
                gvwSubscriptionList.DataBind();
            }

            lblSubscriber.Text = customer.FirstName + " " + customer.LastName + "; id = " + customer.CustomerID.ToString();

        }


    }
}