<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="FaceMaster.Admin.ManageUsers" Title="Account management" Codebehind="ManageUsers.aspx.cs" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="alpha omega sixteen columns">
   <div class="sectiontitle">Account Management</div>
   <p></p>

   <div class="sectionsubtitle">
       <strong>Total registered users:</strong> <asp:Literal runat="server" ID="lblTotUsers" />
       <p>
       <asp:Button ID="btnNewUser" runat="server" Text="Create New Customer" OnClick="btnNewUser_Click" />
       </p>
    </div>
 
    <asp:Panel ID="pnlCustSearch" DefaultButton="btnSearch" runat="server">    
       <p>
       Search customers by lastname, lastname,firstname, Email, or CustomerID:
        </p>   
    
        <div>
            <div class="groupHTML">
                <asp:DropDownList runat="server" ID="ddlSearchTypes">
                    <asp:ListItem Text="LastName FirstName(opt)" Value="2" />
                    <asp:ListItem Text="E-mail" Value="3"  />
                    <asp:ListItem Text="FMCustomerID" Value="4"  />
                </asp:DropDownList>
                <asp:Label ID="Label1" runat="server" Text="&nbsp;&nbsp;contains&nbsp;&nbsp;" ></asp:Label> 
                <asp:TextBox runat="server" Width="30%" ID="txtSearchText" />
                <asp:Label ID="Label2" runat="server" Text="&nbsp;&nbsp;" ></asp:Label> 
                <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click" />
           </div> 
       </div>
       <asp:Label ID="lbltxtboxmsg" runat="server" Text="Label" Visible="false" ></asp:Label>
        <p>&nbsp;</p>
    </asp:Panel>

    <p>
   <asp:GridView 
    ID="gvwUsers"
    SkinID="Admin"
    runat="server"
    AllowSorting="true"
    AutoGenerateColumns="false"
    OnSorting="gvwUsers_Sorting"
    OnRowCreated="gvwUsers_RowCreated"
    EnableViewState="False">
    <HeaderStyle  />
    <AlternatingRowStyle  />
	<PagerStyle  />
      <Columns>
         <asp:HyperLinkField Text="New ORDER" DataNavigateUrlFormatString="ManageUsers.aspx?PageAction=Order&CustomerID={0}" DataNavigateUrlFields="CustomerID" />
         <asp:HyperLinkField Text="New SUBSCRIPTION" DataNavigateUrlFormatString="ManageUsers.aspx?PageAction=OrderSub&CustomerID={0}" DataNavigateUrlFields="CustomerID" />
         <asp:BoundField HeaderText="ID" DataField="CustomerID"  />
         <asp:BoundField HeaderText="Last" DataField="LastName" SortExpression="LastName" />
         <asp:BoundField HeaderText="First" DataField="FirstName" SortExpression="FirstName" />
         <asp:HyperLinkField HeaderText="Email" DataTextField="Email" SortExpression="Email" DataNavigateUrlFormatString="mailto:{0}" DataNavigateUrlFields="Email" />
         <asp:HyperLinkField Text="Details" DataNavigateUrlFormatString="EditUser.aspx?CustomerID={0}" DataNavigateUrlFields="CustomerID" />
         <asp:HyperLinkField Text="Orders" DataNavigateUrlFormatString="ManageOrders.aspx?CustomerID={0}" DataNavigateUrlFields="CustomerID" />
         <asp:HyperLinkField Text="Subscriptions" DataNavigateUrlFormatString="ManageSubscriptions.aspx?CustomerID={0}" DataNavigateUrlFields="CustomerID" />
         <asp:HyperLinkField Text="CERs" DataNavigateUrlFormatString="~/CER/CERLookUp.aspx?pageaction=find&email={0}" DataNavigateUrlFields="Email" />
         <asp:HyperLinkField Text="new CER" DataNavigateUrlFormatString="CEREntry.aspx?FMCustomerID={0}" DataNavigateUrlFields="CustomerID" />
      </Columns>
      <EmptyDataTemplate><b>No users found for the specified criteria</b></EmptyDataTemplate>
   </asp:GridView>
   </p>

</div>
</asp:Content>

