using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Profile;
using System.Data.SqlClient;
using AdminCart;
using FM2015.Helpers;

namespace FaceMaster.Admin
{

    public partial class ManageUsers : AdminBasePage //System.Web.UI.Page
    {

        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "DESC"; }
            set { ViewState["SortDirection"] = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.DefaultButton = btnSearch.UniqueID;

            string s = "";
            lbltxtboxmsg.Visible = false;

            Certnet.Cart tempcart = new Certnet.Cart(); //just need the current user ID
            tempcart.Load(Session["cart"]);
            int customerID = tempcart.SiteCustomer.CustomerID;

            if (!this.IsPostBack)
            {
                try
                {
                    if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "CUSTOMERSERVICE")))
                    { }
                    else { throw new SecurityException("You do not have the apppropriate creditentials to complete this task."); }
                }
                catch (SecurityException ex)
                {
                    string msg = "Error in ManageUsers: not admin or id=0" + Environment.NewLine;
                    msg += "User did not have permission to access page" + Environment.NewLine;
                    msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                    dbErrorLogging.LogError(msg, ex);
                    Response.Redirect("Login.aspx");
                }

                int t = GetUserCount();
                lblTotUsers.Text = t.ToString();

                /* lblOnlineUsers.Text = Membership.GetNumberOfUsersOnline().ToString();

                string[] alphabet = "A;B;C;D;E;F;G;H;I;J;K;L;M;N;O;P;Q;R;S;T;U;V;W;X;Y;Z;All".Split(';');
                rptAlphabet.DataSource = alphabet;
                rptAlphabet.DataBind(); 
               
                */

                ddlSearchTypes.SelectedValue = "1";
            }

            int SourceID = 0;
            if (Request["PageAction"] != null) { s = Request["PageAction"].ToString(); }
            switch (s)
            {
                case "Order":
                    SourceID = customerID; //save ID of whoever is doing the order
                    customerID = Convert.ToInt32(Request["CustomerID"]);
                    if (customerID != 0)
                    {
                        Cart cart = new Cart();
                        cart.SiteOrder.SourceID = SourceID;
                        cart.SiteCustomer = new Customer(customerID);
                        cart.BillAddress = Address.LoadAddress(customerID, 1);

                        // if billing addressid = 0, make sure to back & get address update
                        if (cart.BillAddress.AddressId == 0) { Response.Redirect("EditUser.aspx?CustomerID=" + cart.SiteCustomer.CustomerID.ToString()); } //go get a billing address
                        cart.ShipAddress = Address.LoadAddress(customerID, 2);

                        // if shipping addressid = 0, save billing address to it
                        if (cart.ShipAddress.AddressId == 0) { cart.BillAddress.Save(cart.SiteCustomer, 2); }

                        Session["CScart"] = cart;
                        Response.Redirect("Products.aspx"); //admin/Products
                    }
                    break;

                case "OrderSub":
                    SourceID = customerID; //save ID of whoever is doing the order
                    customerID = Convert.ToInt32(Request["CustomerID"]);
                    if (customerID != 0)
                    {
                        Cart cart = new Cart();
                        cart.SiteOrder.SourceID = SourceID;
                        cart.SiteCustomer = new Customer(customerID);
                        cart.BillAddress = Address.LoadAddress(customerID, 1);

                        // if billing addressid = 0, make sure to back & get address update
                        if (cart.BillAddress.AddressId == 0) { Response.Redirect("EditUser.aspx?CustomerID=" + cart.SiteCustomer.CustomerID.ToString()); } //go get a billing address
                        cart.ShipAddress = Address.LoadAddress(customerID, 2);

                        // if shipping addressid = 0, save billing address to it
                        if (cart.ShipAddress.AddressId == 0) { cart.BillAddress.Save(cart.SiteCustomer, 2); }

                        Session["CScart"] = cart;
                        Response.Redirect("Subscriptions/SubProducts.aspx"); //admin/Products
                    }
                    break;

                case "Lookup":
                    int FMCustomerID = Convert.ToInt32(Request["FMCustomerID"]);
                    if (FMCustomerID != 0)
                    {
                        ddlSearchTypes.SelectedValue = "4";
                        txtSearchText.Text = FMCustomerID.ToString();
                        gvwUsers.Attributes.Add("SearchText", txtSearchText.Text);
                        gvwUsers.Attributes.Add("SearchType", "byFMCustomerID");
                        BindUsers(false);
                    }
                    break;
            }
        }

        private void BindUsers(bool reloadAllUsers)
        {
                   /*
                   if (reloadAllUsers)
                      allUsers = Membership.GetAllUsers();
                    */

                   DataView customers = null;


                   string searchText = "";
                   if (!string.IsNullOrEmpty(gvwUsers.Attributes["SearchText"]))
                      searchText = gvwUsers.Attributes["SearchText"];

                  string searchtype = gvwUsers.Attributes["SearchType"];

                  int n; //used for int.TryParse output
                  switch (searchtype)
                  {
                      case "byFMCustomerID":
                          {
                              if (ValidateUtil.IsNumeric(searchText))
                              {
                                  if (searchText == "0")
                                  {
                                      lbltxtboxmsg.Text = "ID 0 is not a valid customer ID";
                                      lbltxtboxmsg.Visible = true;
                                  }
                                  else { customers = FindUsersByFMCustomerID(searchText); }
                              }
                              else 
                              { 
                                  customers = null;
                                  lbltxtboxmsg.Text = "Customer ID must be a numeric value";
                                  lbltxtboxmsg.Visible = true;
                              }
                          }
                          break;

                      case "byLastFirst":
                          {
                              searchText = ValidateUtil.CleanUpString(searchText);
                              if (string.IsNullOrEmpty(searchText) || int.TryParse(searchText, out n))
                              {
                                  customers = null;
                                  lbltxtboxmsg.Text = "First,Last name must contain some alpha characters";
                                  lbltxtboxmsg.Visible = true;
                              }
                              else 
                              {
                                  String[] cclist = null;
                                  char[] delimiterChars = { ' ', ',', '.', ':', '\t' };
                                  cclist = searchText.Split(delimiterChars);
                                  string lastname = "";
                                  string firstname = "";

                                  for (int i = 0; i < cclist.Length; ++i )
                                  {
                                      try
                                      {
                                          if (!string.IsNullOrEmpty(cclist[i]))
                                          {
                                              if (i == 0)
                                              { lastname = cclist[i]; }
                                              else
                                              { if (cclist[i] != "") { firstname = cclist[i]; } }
                                          }
                                      }
                                      catch { } // let exception fall thru
                                  }
                                  
                                  customers = FindUsersByFirstLast(lastname, firstname);
                              }
                          }
                          break;

                      case "byEmail":
                          searchText = ValidateUtil.CleanUpString(searchText);
                          if (string.IsNullOrEmpty(searchText) || int.TryParse(searchText, out n))
                          {
                              customers = null;
                              lbltxtboxmsg.Text = "Email must contain some alpha characters";
                              lbltxtboxmsg.Visible = true;
                          }
                          else
                          { customers = FindUsersByEmail(searchText); }
                          break;

                      case "byLast":
                          searchText = ValidateUtil.CleanUpString(searchText);
                          if (string.IsNullOrEmpty(searchText) || int.TryParse(searchText, out n))
                          {
                              customers = null;
                              lbltxtboxmsg.Text = "Last name must contain some alpha characters";
                              lbltxtboxmsg.Visible = true;
                          }
                          else
                          { customers = FindUsersByLastName(searchText); } 
                          break;

                      default:
                          { customers = null; }
                          break;
                  }

                   Session["grvDataSource"] = customers; //remember for sorting later
                   gvwUsers.DataSource = customers;         
                   gvwUsers.DataBind();
        }

        protected void rptAlphabet_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            gvwUsers.Attributes.Add("SearchType", "byLast");
            if (e.CommandArgument.ToString().Length == 1)
            {
                gvwUsers.Attributes.Add("SearchText", e.CommandArgument.ToString());
                BindUsers(false);
            }
            else
            {
                gvwUsers.Attributes.Add("SearchText", "");
                BindUsers(false);
            }

        }

        protected void gvwUsers_RowCreated(object sender, GridViewRowEventArgs e)
        {
            /*
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton btn = e.Row.Cells[6].Controls[0] as ImageButton;
                btn.OnClientClick = "if (confirm('Are you sure you want to delete this user account?') == false) return false;";
            }
             */
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            switch (ddlSearchTypes.SelectedValue)
            {
                case ("1"):
                    gvwUsers.Attributes.Add("SearchType", "byLast");
                    break;

                case ("2"):
                    gvwUsers.Attributes.Add("SearchType", "byLastFirst");
                    break;

                case ("3"):
                    gvwUsers.Attributes.Add("SearchType", "byEmail");
                    break;

                case ("4"):
                    gvwUsers.Attributes.Add("SearchType", "byFMCustomerID");
                    break;

                default:
                    break;
            }
            gvwUsers.Attributes.Add("SearchText", txtSearchText.Text);
            BindUsers(false);
        }

        protected void btnNewUser_Click(object sender, EventArgs e)
        {
            //Response.Redirect("NewCustomer.aspx");
            Response.Redirect("edituser.aspx?customerid=-1");
        }

        protected int GetUserCount()
        {
            string sql = @"
            SELECT rows 
            FROM sysindexes
            WHERE id = OBJECT_ID('customers') AND indid < 2
            ";

            SqlDataReader dr = DBUtil.FillDataReader(sql);

            int count = 0;
            if (dr.Read()) { count = Convert.ToInt32(dr[0].ToString()); }//dr[0] = # rows result

            dr.Close();

            return count;
        }

        protected DataView FindUsersByEmail( string email)
        {
            string sql = @"
            SELECT * 
            FROM Customers
            WHERE Email LIKE @Email 
            ORDER BY LastName ASC  
            ";

            email = "%"+ email + "%";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, email);
            DataSet ds = DBUtil.FillDataSet(sql, "users", mySqlParameters);

            return ds.Tables["users"].DefaultView;
        }

        protected DataView FindUsersByLastName(string lastname)
        {
            string sql = @"
            SELECT * 
            FROM Customers
            WHERE LastName LIKE @LastName 
            ";

            lastname = "%" + lastname + "%";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, lastname);
            DataSet ds = DBUtil.FillDataSet(sql, "users", mySqlParameters);

            return ds.Tables["users"].DefaultView;
        }

        protected DataView FindUsersByFMCustomerID(string searchText)
        {
            string sql = @"
            SELECT * 
            FROM Customers
            WHERE CustomerID = @CustomerID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, searchText);
            DataSet ds = DBUtil.FillDataSet(sql, "users", mySqlParameters);

            return ds.Tables["users"].DefaultView;
        }

         protected DataView FindUsersByFirstLast(string lastname, string firstname)
        {
            string sql = @"
            SELECT * 
            FROM Customers
            WHERE LastName LIKE @Lastname AND FirstName LIKE @Firstname 
            ";
            lastname = "%"+ lastname + "%";
            firstname = "%" + firstname + "%";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, lastname, firstname);
            DataSet ds = DBUtil.FillDataSet(sql, "users", mySqlParameters);

            return ds.Tables["users"].DefaultView;
        }

       protected DataView FindAllUsers()
        {
            string sql = @"
            SELECT * 
            FROM Customers
            ORDER BY LastName ASC
            ";

            DataSet ds = DBUtil.FillDataSet(sql, "users");

            return ds.Tables["users"].DefaultView;
        }

        protected void gvwUsers_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataView dv = Session["grvDataSource"] as DataView;

            if (dv != null)
            {

                string m_SortDirection = GetSortDirection();
                dv.Sort = e.SortExpression + " " + m_SortDirection;

                gvwUsers.DataSource = dv;
                gvwUsers.DataBind();
            }
        }

        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;

                case "DESC":

                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }
    }

}