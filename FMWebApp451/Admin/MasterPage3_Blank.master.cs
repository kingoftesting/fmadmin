using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Certnet;

namespace FM.Admin
{
    public partial class MasterPage3_Blank : System.Web.UI.MasterPage
    {

        Cart cart = new Cart();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                imgHeaderPic1.ImageUrl = AdminCart.Config.AdminImage();
                lblAdminName.Text = "&nbsp;&nbsp;" + AdminCart.Config.AdminName() + " " + lblAdminName.Text;

                // GetCurrentPageName
                string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
                string requestedPage = oInfo.Name;
                requestedPage = requestedPage.ToUpper();
                if (requestedPage == "PROCESSING.ASPX") { Response.AppendHeader("Refresh", "2; URL=Processing.aspx?Proc=charge"); }
                else { Response.AppendHeader("Refresh", "900; URL=../admin/default.aspx"); }

                lblYearNow.Text = DateTime.Now.Year.ToString();
                lblCopyright.Text = AdminCart.Config.BizName() + " " + lblCopyright.Text;

                switch(AdminCart.Config.appName.ToUpper())
                {
                    case "FACEMASTER":
                        //lnkaltSite.NavigateUrl = "https://52.25.186.65/adminMVC/index";
                        //lnkaltSite.Text = "CL Admin";
                        //lnkalt2Site.NavigateUrl = "http://52.27.160.237/adminMVC/index";
                        //lnkalt2Site.Text = "SU Admin";
                        lblCERpermission.Text = "";
                        lblCERRightsReserved.Text = "";
                        break;

                    case "CRYSTALIFT":
                        //lnkaltSite.NavigateUrl = "https://fmadmin.facemaster.com/adminMVC/index";
                        //lnkaltSite.Text = "FM Admin";
                        //lnkalt2Site.NavigateUrl = "http://52.27.160.237/adminMVC/index";
                        //lnkalt2Site.Text = "SU Admin";
                        lblCERpermission.Text = "Crystalift Admin & CER Systems licensed by FaceMaster of Beverly Hills, Inc.";
                        break;

                    case "SONULASE":
                        //lnkaltSite.NavigateUrl = "https://fmadmin.facemaster.com/admin/default.aspx";
                        //lnkaltSite.Text = "FM Admin";
                        //lnkalt2Site.NavigateUrl = "https://52.25.186.65/default.aspx";
                        //lnkalt2Site.Text = "CL Admin";
                        lblCERpermission.Text = "Sonulase Admin & CER Systems licensed by FaceMaster of Beverly Hills, Inc.";
                        break;

                    case "RMSHOP":
                        break;

                    default:
                        break;
                }
            }

        }

        public static void SetRefreshValues(System.Web.UI.HtmlControls.HtmlHead head, string name, string content)
        {
            HtmlMeta metaValue = null;

            metaValue = new HtmlMeta();
            metaValue.Attributes.Add("http-equiv", name);
            metaValue.Attributes.Add("content", content);
            head.Controls.Add(metaValue);

            //return true;
        }

    } 
}
