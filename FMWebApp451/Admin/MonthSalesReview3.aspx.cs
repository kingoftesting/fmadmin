using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using AdminCart;
using FM2015.Helpers;


public partial class Admin_MonthSalesReview3 : AdminBasePage //System.Web.UI.Page
{
    Certnet.Cart cart;
    DataView websales = null;

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "DESC"; }
        set { ViewState["SortDirection"] = value; }
    }

    void Page_Init(Object sender, EventArgs e)
    {
        cart = new Certnet.Cart();
        cart.Load(Session["cart"]);
        websales = new DataView(MakeWebSalesTable());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            int customerID = cart.SiteCustomer.CustomerID;
            try
            {
                if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "REPORTS")) || (ValidateUtil.IsInRole(customerID, "MARKETING")))
                { }
                else
                {
                    throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
                }
            }
            catch (SecurityException ex)
            {
                string msg = "Error in DailySalesTime" + Environment.NewLine;
                msg += "User did not have permission to access page" + Environment.NewLine;
                msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                dbErrorLogging.LogError(msg, ex);
                Response.Redirect("../Login.aspx");
            }

            Populate_MonthList();
            Populate_YearList();

            btnGetCSV.Visible = false; //don't show CSV button until there's data displayed
        }
    }

    private void BindWebSales()
    {
        FillWebSales();
        websales.Sort = "DATE" + " " + "ASC";
        gvwDataList.DataSource = websales;
        Session["grvDataSource"] = websales; //save for later; used to Sort, CSV download, Email
        gvwDataList.DataBind();

        if (websales.Table.Rows.Count == 0) //hide Email & CSV buttons if empty table
        { btnGetCSV.Visible = false; }
        else
        { btnGetCSV.Visible = true; }
    }

    protected void btnListByDate_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            BindWebSales();
        }
    }

    protected void btnCSV_Click(object sender, EventArgs e)
    {
        //FillWebSales();
        websales = Session["grvDataSource"] as DataView;
        websales.Sort = "Date DESC";
        //Helpers.SendCSV(websales);
        Utilities.FMHelpers.SendCSV(websales);
    }

    protected void FillWebSales()
    {
        string month = ddlMonth.SelectedValue;
        string year = ddlYear.SelectedValue;

        DateTime dFrom = Convert.ToDateTime(month + "/1/" + year);
        DateTime dTo = dFrom.AddMonths(1);

        string FromDate = dFrom.ToShortDateString();
        string ToDate = dTo.ToShortDateString();

        websales = FindAllWebSales(websales, FromDate, ToDate);
    }

    protected DataView FindAllWebSales(DataView websales, string fromDate, string toDate)
    {
        SqlDataReader dr = null;

        int SaulID = 7041;
        int NikkiID = 30967;
        int StaiceyID = 37156;
        int ToureID = 36935;
        int AmandaID = 37428;
        int JanetID = 37395;
        int AlexaID = 27936; 
        int sourceID = 0;

        decimal Revenue = 0;
        decimal TotalRevenue = 0;
        decimal WebRevenue = 0;
        decimal PhoneRevenue = 0;
        decimal SaulRevenue = 0;
        decimal NikkiRevenue = 0;
        decimal StaiceyRevenue = 0;
        decimal ToureRevenue = 0;
        decimal AlexaRevenue = 0;
        decimal HDIRevenue = 0;
        decimal SumTotalRevenue = 0;
        decimal SumWebRevenue = 0;
        decimal SumPhoneRevenue = 0;
        decimal SumSaulRevenue = 0;
        decimal SumNikkiRevenue = 0;
        decimal SumStaiceyRevenue = 0;
        decimal SumToureRevenue = 0;
        decimal SumAlexaRevenue = 0;
        decimal SumHDIRevenue = 0;
        DateTime enddate = Convert.ToDateTime(toDate);
        DateTime startdate = Convert.ToDateTime(fromDate);
        TimeSpan n = enddate - startdate;
        int numdays = n.Days;

        for (int i = 0; i < numdays; ++i)
        {
            fromDate = startdate.ToShortDateString();
            toDate = startdate.AddDays(1).ToShortDateString();
            string sql = @" 
                SELECT * 
                FROM Orders o, OrderActions oa, Transactions t
                WHERE o.OrderID = oa.OrderID AND o.OrderID = t.OrderID AND oa.OrderActionType = 2 AND o.OrderDate BETWEEN @FromDate AND @ToDate 
                ORDER BY o.OrderID ASC, t.TransactionRowID ASC 
                ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, fromDate, toDate);
            dr = DBUtil.FillDataReader(sql, mySqlParameters);

            //new day! start the counts all over
            TotalRevenue = 0;
            WebRevenue = 0;
            PhoneRevenue = 0;
            SaulRevenue = 0;
            NikkiRevenue = 0;
            StaiceyRevenue = 0;
            ToureRevenue = 0;
            AlexaRevenue = 0;
            HDIRevenue = 0;

            //Convert list read from DB to something that we can process fast (ie, no more DB calls)
            List<Order> orders = new List<Order>();
            List<Transaction> tList = new List<Transaction>();
            Order o = new Order();

            while (dr.Read())
            {
                if (o.OrderID != Convert.ToInt32(dr["OrderID"]))
                {
                    if (o.OrderID != 0) { orders.Add(o); } //add the previous order to the list before starting a new one
                    o = new Order();
                    o.OrderID = Convert.ToInt32(dr["OrderID"]);
                    o.SourceID = Convert.ToInt32(dr["SourceID"]);

                    //o.ByPhone = Convert.ToBoolean(dr["Phone"]); //[Phone] is a string
                    if (dr["Phone"] == DBNull.Value) { o.ByPhone = false; }
                    else
                    {
                        if (dr["Phone"].ToString() == "0") { o.ByPhone = false; }
                        else { o.ByPhone = true; }
                    }

                    o.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    o.BillingAddressID = Convert.ToInt32(dr["BillingAddressID"]);
                    o.ShippingAddressID = Convert.ToInt32(dr["ShippingAddressID"]);
                    o.OrderDate = DateTime.Parse(dr["OrderDate"].ToString());

                    o.TotalShipping = Convert.ToDecimal(dr["TotalShipping"]);
                    o.TotalTax = Convert.ToDecimal(dr["TotalTax"]);
                    o.TotalCoupon = Convert.ToDecimal(dr["TotalCoupon"]);
                    o.Adjust = Convert.ToDecimal(dr["Adjust"].ToString());
                    o.Total = Convert.ToDecimal(dr["Total"]);
                    o.SubTotal = o.Total + o.TotalCoupon - o.TotalTax - o.TotalShipping;
                    o.ShipMode = Convert.ToInt32(dr["ShipMode"]);
                }

                //build Transaction List
                Transaction t = new Transaction();
                bool isInList = false; //assume already added to list
                t.TransactionRowID = Convert.ToInt32(dr["TransactionRowID"].ToString());
                t.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                t.TransactionID = dr["TransactionID"].ToString();
                t.TransactionDate = Convert.ToDateTime(dr["TransactionDate"].ToString());
                t.TrxType = dr["TrxType"].ToString();
                t.CardType = dr["CardType"].ToString();
                t.CardNo = dr["CardNo"].ToString();
                t.ExpDate = dr["ExpDate"].ToString();
                t.TransactionAmount = Convert.ToDecimal(dr["TransactionAmount"].ToString());
                t.ResultCode = Convert.ToInt32(dr["ResultCode"].ToString());
                t.ResultMsg = dr["ResultMsg"].ToString();
                t.Comment1 = dr["Comment1"].ToString();
                t.Comment2 = dr["Comment2"].ToString();
                t.CardHolder = dr["CardHolder"].ToString();
                t.Address = new Address();
                t.Address.City = dr["City"].ToString();
                t.Address.State = dr["State"].ToString();
                t.Address.Country = dr["Country"].ToString();
                t.CustomerCode = dr["AuthCode"].ToString();
                t.Iavs = dr["iavs"].ToString();
                t.AvsZip = dr["avszip"].ToString();
                t.AvsAddr = dr["Avsaddr"].ToString();

                isInList = false;
                if (tList.Count != 0)
                {
                    foreach (Transaction tempT in tList)
                    {
                        if (tempT.TransactionRowID == t.TransactionRowID) { isInList = true; break; }
                    }
                }
                if (!isInList) { tList.Add(t); }
            }

            if (o.OrderID != 0) { orders.Add(o); } //add the last order to the list
            if (dr != null) { dr.Close(); }

            foreach (Order newO in orders)
            {
                Revenue = newO.Total - newO.TotalTax - newO.TotalShipping;
                TotalRevenue = TotalRevenue + Revenue;
                if (newO.ByPhone)
                { PhoneRevenue = PhoneRevenue + Revenue; }
                else
                { WebRevenue = WebRevenue + Revenue; }
                sourceID = newO.SourceID;

                if (sourceID == SaulID) { SaulRevenue = SaulRevenue + Revenue; }
                if (sourceID == NikkiID) { NikkiRevenue = NikkiRevenue + Revenue; }
                if (sourceID == StaiceyID) { StaiceyRevenue = StaiceyRevenue + Revenue; }
                if (sourceID == ToureID) { ToureRevenue = ToureRevenue + Revenue; }
                if (sourceID == AmandaID) { HDIRevenue = HDIRevenue + Revenue; }
                if (sourceID == JanetID) { HDIRevenue = HDIRevenue + Revenue; }
                if (sourceID == AlexaID) { AlexaRevenue = AlexaRevenue + Revenue; }
                
            }

            DataRow newrow;
            newrow = websales.Table.NewRow();
            newrow["Date"] = startdate;
            newrow["NetRevenue"] = TotalRevenue;
            newrow["NetPhoneRevenue"] = PhoneRevenue;
            newrow["NetWebRevenue"] = WebRevenue;
            newrow["SaulRevenue"] = SaulRevenue;
            newrow["NikkiRevenue"] = NikkiRevenue;
            newrow["ToureRevenue"] = ToureRevenue;
            newrow["StaiceyRevenue"] = StaiceyRevenue;
            newrow["AlexaRevenue"] = AlexaRevenue;
            newrow["HDIRevenue"] = HDIRevenue;
            websales.Table.Rows.Add(newrow);

            //add in Revenue from additional transactions
            List<int> oProcessed = new List<int>();
            foreach (Transaction t in tList)
            {
                if ((t.ResultCode == 0) && (t.TransactionID != "0"))
                {
                    switch (t.TrxType[0])
                    {
                        case 'S':
                            bool orderInList = false;
                            foreach (int orderID in oProcessed)
                            {
                                if (orderID == t.OrderID) //if found in the list then we must have processed a previous 'S'
                                {
                                    orderInList = true;
                                    TotalRevenue += t.TransactionAmount;
                                    foreach (Order o1 in orders)
                                    {
                                        if (o1.OrderID == orderID)
                                        {
                                            if (o1.ByPhone)
                                            { PhoneRevenue += t.TransactionAmount; }
                                            else
                                            { WebRevenue += t.TransactionAmount; }

                                            if (o1.SourceID == SaulID) { SaulRevenue += t.TransactionAmount; }
                                            if (o1.SourceID == NikkiID) { NikkiRevenue += t.TransactionAmount; }
                                            if (o1.SourceID == StaiceyID) { StaiceyRevenue += t.TransactionAmount; }
                                            if (o1.SourceID == ToureID) { ToureRevenue += t.TransactionAmount; }
                                            if (o1.SourceID == AmandaID) { HDIRevenue += t.TransactionAmount; }
                                            if (o1.SourceID == JanetID) { HDIRevenue += t.TransactionAmount; }
                                            if (o1.SourceID == AlexaID) { AlexaRevenue += t.TransactionAmount; }
                                        }
                                    }
                                }
                            }
                            if (!orderInList) { oProcessed.Add(t.OrderID); }
                            break;

                        case 'C':
                            break;

                        case 'V':
                            break;
                    }
                }
            } 
            
            SumTotalRevenue = SumTotalRevenue + TotalRevenue;
            SumPhoneRevenue = SumPhoneRevenue + PhoneRevenue;
            SumWebRevenue = SumWebRevenue + WebRevenue;
            SumSaulRevenue = SumSaulRevenue + SaulRevenue;
            SumNikkiRevenue = SumNikkiRevenue + NikkiRevenue;
            SumStaiceyRevenue = SumStaiceyRevenue + StaiceyRevenue;
            SumToureRevenue = SumToureRevenue + ToureRevenue;
            SumAlexaRevenue = SumAlexaRevenue + AlexaRevenue;
            SumHDIRevenue = SumHDIRevenue + HDIRevenue;

            startdate = startdate.AddDays(1);
        }

        DataRow sumrow;
        sumrow = websales.Table.NewRow();
        sumrow["Date"] = DBNull.Value;
        sumrow["NetRevenue"] = SumTotalRevenue;
        sumrow["NetPhoneRevenue"] = SumPhoneRevenue;
        sumrow["NetWebRevenue"] = SumWebRevenue;
        sumrow["SaulRevenue"] = SumSaulRevenue;
        sumrow["NikkiRevenue"] = SumNikkiRevenue;
        sumrow["StaiceyRevenue"] = SumStaiceyRevenue;
        sumrow["ToureRevenue"] = SumToureRevenue;
        sumrow["AlexaRevenue"] = SumAlexaRevenue;
        sumrow["HDIRevenue"] = SumHDIRevenue;
        websales.Table.Rows.Add(sumrow);

        return websales;
    }

    private DataTable MakeWebSalesTable()
    {
        // Create a new DataTable titled 'WebSales'
        DataTable WebSalesTable = new DataTable("WebSales");

        // Add three column objects to the table.
        DataColumn idColumn = new DataColumn();
        idColumn.DataType = System.Type.GetType("System.Int32");
        idColumn.ColumnName = "id";
        idColumn.AutoIncrement = true;
        WebSalesTable.Columns.Add(idColumn);

        DataColumn DateColumn = new DataColumn();
        DateColumn.DataType = System.Type.GetType("System.DateTime");
        DateColumn.ColumnName = "Date";
        DateColumn.DefaultValue = "1/1/1900";
        WebSalesTable.Columns.Add(DateColumn);

        DataColumn RevColumn = new DataColumn();
        RevColumn.DataType = System.Type.GetType("System.Decimal");
        RevColumn.ColumnName = "NetRevenue";
        RevColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(RevColumn);

        DataColumn WebRevColumn = new DataColumn();
        WebRevColumn.DataType = System.Type.GetType("System.Decimal");
        WebRevColumn.ColumnName = "NetWebRevenue";
        WebRevColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(WebRevColumn);

        DataColumn PhoneRevColumn = new DataColumn();
        PhoneRevColumn.DataType = System.Type.GetType("System.Decimal");
        PhoneRevColumn.ColumnName = "NetPhoneRevenue";
        PhoneRevColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(PhoneRevColumn);

        DataColumn SaulColumn = new DataColumn();
        SaulColumn.DataType = System.Type.GetType("System.Decimal");
        SaulColumn.ColumnName = "SaulRevenue";
        SaulColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(SaulColumn);

        DataColumn NikkiColumn = new DataColumn();
        NikkiColumn.DataType = System.Type.GetType("System.Decimal");
        NikkiColumn.ColumnName = "NikkiRevenue";
        NikkiColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(NikkiColumn);

        DataColumn StaiceyColumn = new DataColumn();
        StaiceyColumn.DataType = System.Type.GetType("System.Decimal");
        StaiceyColumn.ColumnName = "StaiceyRevenue";
        StaiceyColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(StaiceyColumn);

        DataColumn ToureColumn = new DataColumn();
        ToureColumn.DataType = System.Type.GetType("System.Decimal");
        ToureColumn.ColumnName = "ToureRevenue";
        ToureColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(ToureColumn);

        DataColumn AlexaColumn = new DataColumn();
        AlexaColumn.DataType = System.Type.GetType("System.Decimal");
        AlexaColumn.ColumnName = "AlexaRevenue";
        AlexaColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(AlexaColumn);

        DataColumn HDIColumn = new DataColumn();
        HDIColumn.DataType = System.Type.GetType("System.Decimal");
        HDIColumn.ColumnName = "HDIRevenue";
        HDIColumn.DefaultValue = 0;
        WebSalesTable.Columns.Add(HDIColumn);
        
        // Create an array for DataColumn objects.
        DataColumn[] keys = new DataColumn[1];
        keys[0] = idColumn;
        WebSalesTable.PrimaryKey = keys;

        // Once a table has been created, use the 
        /* NewRow to create a DataRow.
        DataRow row;
        row = WebSalesTable.NewRow();

        // Then add the new row to the collection.
        row["SKU"] = FM-5000;
        row["Units"] = 0;
        row["Revenue"] = 0;
        row["WebUnits"] = 0;
        row["WebRevenue"] = 0;
        row["PhoneUnits"] = 0;
        row["PhoneRevenue"] = 0;
        WebSales.Rows.Add(row);
        */


        // Return the new DataTable.
        return WebSalesTable;
    }

    protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataView dv = Session["grvDataSource"] as DataView;

        if (dv != null)
        {

            string m_SortDirection = GetSortDirection();
            dv.Sort = e.SortExpression + " " + m_SortDirection;

            gvwDataList.DataSource = dv;
            gvwDataList.DataBind();
        }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;

            case "DESC":

                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    public void gvwDataList_PageIndexChanging(object source, GridViewPageEventArgs e)
    {
        gvwDataList.PageIndex = e.NewPageIndex;
        //gvwDataList.DataBind();
        BindWebSales();
    }

    public void gvwDataList_PageIndexChanged(object source, EventArgs e)
    {
    }

    protected void gvwDataList_RowEditing(object sender, EventArgs e)
    {
    }

    protected void gvwDataList_RowCreated(object sender, GridViewRowEventArgs e)
    {
        int customerID = cart.SiteCustomer.CustomerID;
        if (ValidateUtil.IsInRole(customerID, "MARKETING"))
        {
            e.Row.Cells[4].Visible = false; // hides Saul column
            e.Row.Cells[5].Visible = false; // hides Staicey column
            e.Row.Cells[6].Visible = false; // hides Nikki column
            e.Row.Cells[7].Visible = false; // hides HDI
        }

    }

    protected void Populate_MonthList()
    {

        ddlMonth.Items.Add("January");
        ddlMonth.Items.Add("February");
        ddlMonth.Items.Add("March");
        ddlMonth.Items.Add("April");
        ddlMonth.Items.Add("May");
        ddlMonth.Items.Add("June");
        ddlMonth.Items.Add("July");
        ddlMonth.Items.Add("August");
        ddlMonth.Items.Add("September");
        ddlMonth.Items.Add("October");
        ddlMonth.Items.Add("November");
        ddlMonth.Items.Add("December");

        ddlMonth.Items.FindByValue(DateTime.Now.ToString("MMMM")).Selected = true;
    }

    protected void Populate_YearList()
    {

        //Year list can be extended
        for (int intYear = 2006; intYear < DateTime.Now.Year+1; intYear++)
        {
            ddlYear.Items.Add(intYear.ToString());
        }

        string year = DateTime.Now.Year.ToString();
        ddlYear.Items.FindByValue(year).Selected = true;
    }

}
