<%@ Page Language="C#" MasterPageFile="~/Admin/MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="Admin_NewCustomer" Title="New Customer" Codebehind="NewCustomer.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="alpha omega sixteen columns">
    <div class="sectiontitle">Create New Customer Account</div>
</div>

<div class="alpha omega sixteen columns">

    <p>
        <asp:literal visible="False" id="PageTitle" runat="server" />
        Please fill in all fields below (fields labeled with a '*' are required)
        <asp:Label id="dotnetCARTmessage" runat="server" ForeColor="red" Font-Bold="true"></asp:Label>
    </p>

    <div class="formHTML">
        <asp:Label ID="Label1" runat="server" Text="First&nbsp;Name:*"></asp:Label>
		<asp:textbox id="txtFirstName" Runat="server" MaxLength="30"></asp:textbox>
 		<asp:requiredfieldvalidator id="Requiredfieldvalidator1" Runat="server" Display="Dynamic" ErrorMessage="Your first name is required"
				ControlToValidate="txtFirstName"></asp:requiredfieldvalidator>
		<asp:customvalidator id="Customvalidator2" Runat="server" ErrorMessage="Your first name must be up to 30 characters"
			Display="Dynamic" ControlToValidate="txtFirstName" ClientValidationFunction="ValidateLengthName" />
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label2" runat="server" Text="Last&nbsp;Name:*"></asp:Label>
		<asp:textbox id="txtLastName" Runat="server" MaxLength="30"></asp:textbox>
		<asp:requiredfieldvalidator id="Requiredfieldvalidator2" Runat="server" Display="Dynamic" ErrorMessage="Your last name is required."
				ControlToValidate="txtLastName"></asp:requiredfieldvalidator>
		<asp:customvalidator id="Customvalidator3" Runat="server" ErrorMessage="Your last name must be up to 30 characters"
			Display="Dynamic" ControlToValidate="txtLastName" ClientValidationFunction="ValidateLengthName" />
    </div>

   <div class="simpleClear">&nbsp;</div>
   <div class="simpleClear">&nbsp;</div>
    <div class="groupHTML">
        <asp:Label ID="Label22" runat="server" Text="<strong>Billing Address:</strong>"></asp:Label>
    </div>
    
    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label3" runat="server" Text="Street1:"></asp:Label>
		<asp:textbox id="txtStreet" Runat="server" MaxLength="60"></asp:textbox>
		<asp:requiredfieldvalidator id="Requiredfieldvalidator3" Runat="server" Display="Dynamic" ErrorMessage="Your street address is required."
					ControlToValidate="txtStreet"></asp:requiredfieldvalidator>
		<asp:customvalidator id="Customvalidator7" Runat="server" ErrorMessage="Your street2 address 40 characters or fewer"
			Display="Dynamic" ControlToValidate="txtStreet" ClientValidationFunction="ValidateLengthName" />
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label4" runat="server" Text="Street2:"></asp:Label>
		<asp:textbox id="txtStreet2" Runat="server" MaxLength="60"></asp:textbox>
		<asp:requiredfieldvalidator id="Requiredfieldvalidator8" Runat="server" Display="Dynamic" ErrorMessage="Your street address is required."
					ControlToValidate="txtStreet"></asp:requiredfieldvalidator>
		<asp:customvalidator id="Customvalidator9" Runat="server" ErrorMessage="Your street2 address 40 characters or fewer"
			Display="Dynamic" ControlToValidate="txtStreet" ClientValidationFunction="ValidateLengthName" />
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label5" runat="server" Text="City:"></asp:Label>
		<asp:textbox id="txtCity" Runat="server" MaxLength="100"></asp:textbox>
		<asp:requiredfieldvalidator id="Requiredfieldvalidator4" Runat="server" Display="Dynamic" ErrorMessage="Your city name is required."
					ControlToValidate="txtCity"></asp:requiredfieldvalidator>
		<asp:customvalidator id="Customvalidator8" Runat="server" ErrorMessage="Your city must be up to 100 characters"
			Display="Dynamic" ControlToValidate="txtCity" ClientValidationFunction="ValidateLengthName" />
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label6" runat="server" Text="State/Province:"></asp:Label>
        <asp:textbox id="txtStateBill" Visible="False" Runat="server" MaxLength="255"></asp:textbox>
		<asp:dropdownlist id="selStateBill" runat="server" Visible="True" Width="20%"></asp:dropdownlist>
		<asp:requiredfieldvalidator id="rqdStateBill" Visible="False" ControlToValidate="txtStateBill" ErrorMessage="*"
		    Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label7" runat="server" Text="Zip:"></asp:Label>
        <asp:textbox id="BillAddress_ZipPostal" runat="server" Width="10%"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator9" ControlToValidate="BillAddress_ZipPostal" ErrorMessage="*"
		        Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">        
        <asp:Label ID="Label8" runat="server" Text="Country:"></asp:Label>
        <asp:dropdownlist id="selCountryBill" width="20%" runat="server"
		        AppendDataBoundItems="true" OnSelectedIndexChanged="selCountryBill_SelectedIndexChanged" AutoPostBack="true">
		        <asp:ListItem Text="United States" Value="US" Selected="true" />
		        <asp:ListItem Text="Canada" Value="CA" Selected="false" />
		        <asp:ListItem Text="--------------" Value="" Selected="false" />
        </asp:dropdownlist>
    </div>

    <div class="simpleClear"></div>
    <div class="formHTML">
        <asp:Label ID="Label9" runat="server" Text="Email:*"></asp:Label>
		<asp:textbox id="txtEmail" Width="20%" Runat="server" MaxLength="50"></asp:textbox>
		<asp:regularexpressionvalidator id="RegularExpressionValidator1" Runat="server" Display="Dynamic" ErrorMessage="Your email is invalid (example: joe@aol.com)"
				ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator>
		<asp:requiredfieldvalidator id="Requiredfieldvalidator7" Runat="server" Display="Dynamic" ErrorMessage="Your email is required"
			ControlToValidate="txtEmail"></asp:requiredfieldvalidator>
		<asp:customvalidator id="Customvalidator5" Runat="server" ErrorMessage="Your email must be up to 50 characters"
			Display="Dynamic" ControlToValidate="txtEmail" ClientValidationFunction="ValidateLengthEmail" />
    </div>

	
	<asp:Panel ID="pnlEmails" Runat="server">

    <div class="simpleClear"></div>
    <div class="formHTML">        	
        <asp:Label ID="Label10" runat="server" Text="Confirm&nbsp;Email:"></asp:Label>
		<asp:textbox id="txtEmailConfirm" Width="20%" Runat="server" MaxLength="50"></asp:textbox>
        <asp:regularexpressionvalidator id="Regularexpressionvalidator2" Runat="server" ControlToValidate="txtEmailConfirm"
			    ErrorMessage="Your confirmation email is invalid (example: joe@aol.com)" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator>
		<asp:comparevalidator id="Comparevalidator2" Runat="server" ControlToValidate="txtEmailConfirm" ErrorMessage="Your email does not match with your confirmation email"
			ControlToCompare="txtEmail"></asp:comparevalidator>
		<asp:customvalidator id="Customvalidator4" Runat="server" ControlToValidate="txtEmailConfirm" ErrorMessage="Your email must be up to 50 characters"
			Display="Dynamic" ClientValidationFunction="ValidateLengthEmail"></asp:customvalidator>
    </div>

	</asp:Panel>
	
    <div class="simpleClear"></div>
    <div class="formHTML">	
        <asp:Label ID="Label11" runat="server" Text="Phone:"></asp:Label>
		<asp:textbox id="txtPhone" Runat="server" MaxLength="32"></asp:textbox>
		<asp:requiredfieldvalidator id="Requiredfieldvalidator10" Runat="server" Display="Dynamic" ErrorMessage="Your phone is required"
				ControlToValidate="txtPhone"></asp:requiredfieldvalidator>
		<asp:customvalidator id="Customvalidator6" Runat="server" ErrorMessage="Your phone must be up to 32 characters"
			Display="Dynamic" ControlToValidate="txtPhone" ClientValidationFunction="ValidateLengthPhone" />
    </div>
            

    <div class="simpleClear"></div>            
    <asp:CheckBox ID="chkFaceMasterNewsletter" CssClass="groupHTML" Runat="server" Text="I would like to receive email updates on Exclusive Offers from FaceMaster.com" />
    <div class="simpleClear"></div>
    <asp:CheckBox ID="chkSuzanneSomersNewsletter" CssClass="groupHTML" Runat="server" Text="I would like to receive email updates from SuzanneSomers.com" />

    <div class="simpleClear"></div>
	<asp:Label ID="Label14" runat="server" Text="Password must be at least 6 characters long (case-sensitive)"></asp:Label>

    <div class="simpleClear"></div>
    <div class="formHTML">		
        <asp:Label ID="Label12" runat="server" Text="Password:*"></asp:Label>
		<asp:textbox id="txtPassword" Runat="server" MaxLength="20"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator5" Runat="server" Display="Dynamic" ErrorMessage="A PassWord is required"
				ControlToValidate="txtPassword"></asp:requiredfieldvalidator>
		<asp:customvalidator id="CustomerValidator1" Runat="server" ErrorMessage="<nobr>Password must be at least 6 to 20 characters"
				Display="Dynamic" ControlToValidate="txtPassword" ClientValidationFunction="ValidateLengthPassword"></asp:customvalidator>
    </div>

    <div class="simpleClear"></div>                
    <div class="formHTML">		
        <asp:Label ID="Label13" runat="server" Text="Confirm Password:&nbsp;*"></asp:Label>
		<asp:textbox id="txtPasswordConfirm" Runat="server" MaxLength="20"></asp:textbox>
		<asp:requiredfieldvalidator id="Requiredfieldvalidator6" Runat="server" Display="Dynamic" ErrorMessage="Please confirm your PassWord"
			ControlToValidate="txtPasswordConfirm"></asp:requiredfieldvalidator>
		<asp:comparevalidator id="CompareValidator1" Runat="server" ErrorMessage="Passwords do not match" ControlToValidate="txtPasswordConfirm"
			Display="Dynamic" ControlToCompare="txtPassword"></asp:comparevalidator>
		<asp:customvalidator id="Customvalidator1" Runat="server" ErrorMessage="<nobr>Password must be at least 6 to 20 characters"
			Display="Dynamic" ControlToValidate="txtPasswordConfirm" ClientValidationFunction="ValidateLengthPassword"></asp:customvalidator>
    </div>

    <div class="simpleClear"></div>
    <asp:CheckBox ID="chkShippingAddress" CssClass="groupHTML" Runat="server" Text="Check here if a separate shipping address is required" AutoPostBack="true" OnCheckedChanged="Checked_SeparateShippingAddress"/>

	<asp:Panel id="pnlShipAddress" runat="server" Visible="false">

   <div class="simpleClear">&nbsp;</div>
   <div class="simpleClear">&nbsp;</div>
    <div class="groupHTML">
        <asp:Label ID="Label15" runat="server" Text="<strong>Shipping Address:</strong>"></asp:Label>
    </div>

    <div class="simpleClear"></div>                
    <div class="formHTML">		
        <asp:Label ID="Label16" runat="server" Text="Street:"></asp:Label>
        <asp:textbox id="ShipAddress_Street" runat="server"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator11" ControlToValidate="ShipAddress_Street" ErrorMessage="*"
	        Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
    </div>

   
    <div class="simpleClear"></div>                
    <div class="formHTML">		
        <asp:Label ID="Label17" runat="server" Text="Street2:"></asp:Label>
	    <asp:textbox id="ShipAddress_Street2" runat="server"></asp:textbox>
    </div>


    <div class="simpleClear"></div>                
    <div class="formHTML">		
        <asp:Label ID="Label18" runat="server" Text="City:"></asp:Label>
		<asp:textbox id="ShipAddress_City" runat="server"></asp:textbox>
        <asp:requiredfieldvalidator id="Requiredfieldvalidator12" ControlToValidate="ShipAddress_City" ErrorMessage="*"
			Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>

    <div class="simpleClear"></div>                
    <div class="formHTML">		
        <asp:Label ID="Label19" runat="server" Text="State:"></asp:Label>
		<asp:textbox id="txtStateShip" Visible="False" Runat="server" MaxLength="255"></asp:textbox>
        <asp:dropdownlist id="selStateShip" width="20%" Visible="True" runat="server" >
        </asp:dropdownlist><asp:requiredfieldvalidator id="rqdStateShip" Visible="False" ControlToValidate="txtStateShip" ErrorMessage="*"
		    Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>

    <div class="simpleClear"></div>                
    <div class="formHTML">		
        <asp:Label ID="Label20" runat="server" Text="Zip:"></asp:Label>
		<asp:textbox id="ShipAddress_ZipPostal" runat="server" Width="10%">
        </asp:textbox><asp:requiredfieldvalidator id="Requiredfieldvalidator13" ControlToValidate="ShipAddress_ZipPostal" ErrorMessage="*"
		    Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
	</div>

    <div class="simpleClear"></div>                
    <div class="formHTML">		
        <asp:Label ID="Label21" runat="server" Text="Country:"></asp:Label>
		<asp:dropdownlist id="selCountryShip" width="20%" runat="server" 
            AppendDataBoundItems="true" OnSelectedIndexChanged="selCountryShip_SelectedIndexChanged" AutoPostBack="true">
			<asp:ListItem Text="United States" Value="US" Selected="true" />
		        <asp:ListItem Text="Canada" Value="CA" Selected="false" />
		        <asp:ListItem Text="--------------" Value="" Selected="false" />
        </asp:dropdownlist>

	</div>

	</asp:Panel>

    <div class="simpleClear">&nbsp;</div>
    <div class="simpleClear">&nbsp;</div>
    <div class="formHTML">
        <asp:Button ID="btnSubmit" runat="server" onclick="btnSubmit_Click" Text="Submit" />
    </div>

</div>

			

<script type="text/javascript" language="javascript">
		function ValidateLengthName(oSrc, args) 
		{
			args.IsValid = (args.Value.length <= 40);
		}
		function ValidateLengthEmail(oSrc, args) 
		{
			args.IsValid = (args.Value.length <= 50);
		}	
		function ValidateLengthPhone(oSrc, args) 
		{
			args.IsValid = (args.Value.length <= 32);
		}				
		function ValidateLengthPassword(oSrc, args)
		{
			args.IsValid = (args.Value.length >= 6 && args.Value.length <= 20);
		}
		</script>
		
</asp:Content>

