using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;
using FM2015.Helpers;


public partial class Admin_NewCustomer : AdminBasePage
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        CanUserProceed(new string[2] { "ADMIN", "CUSTOMERSERVICE" });

        if (!Page.IsPostBack)
        {
            
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtStreet.Text = "n/a";
            txtStreet2.Text = "n/a";
            txtCity.Text = "n/a";
            BillAddress_ZipPostal.Text = "00000";
            txtEmail.Text = "";
            txtEmailConfirm.Text = "";
            txtPhone.Text = "n/a";
            chkFaceMasterNewsletter.Checked = false;
            chkSuzanneSomersNewsletter.Checked = false;
            txtPassword.Text = "";
            txtPasswordConfirm.Text = "";
            selCountryBill.DataSource = DBUtil.GetLocations(0);
            selCountryBill.DataValueField = "Code";
            selCountryBill.DataTextField = "Name";
            selCountryBill.DataBind();

            selStateBill.DataSource = DBUtil.GetLocations(15);
            selStateBill.DataValueField = "Code";
            selStateBill.DataTextField = "Name";
            selStateBill.DataBind();

            selCountryBill.SelectedValue = "US";
            selStateBill.SelectedValue = "CA";

            selCountryShip.DataSource = DBUtil.GetLocations(0);
            selCountryShip.DataValueField = "Code";
            selCountryShip.DataTextField = "Name";
            selCountryShip.DataBind(); 
            selStateShip.DataSource = DBUtil.GetLocations(15);
            selStateShip.DataValueField = "Code";
            selStateShip.DataTextField = "Name";
            selStateShip.DataBind();

            selCountryShip.SelectedValue = "US";
            selStateShip.SelectedValue = "CA";
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        Page.Validate();
        Cart cart = new Cart();
        if (
            Page.IsValid && (
            ((cart.SiteCustomer.CustomerID == 0) && (txtEmail.Text == txtEmailConfirm.Text)
                && (txtPassword.Text == txtPasswordConfirm.Text) && (txtPassword.Text != "")))
            )
        {
            cart.SiteCustomer.FirstName = txtFirstName.Text;
            cart.SiteCustomer.LastName = txtLastName.Text;
            cart.BillAddress.Street = txtStreet.Text;
            cart.BillAddress.Street2 = txtStreet2.Text;
            cart.BillAddress.City = txtCity.Text;
            cart.BillAddress.Zip = BillAddress_ZipPostal.Text;
            cart.BillAddress.State = selStateBill.Text;
            cart.BillAddress.Country = selCountryBill.Text;
            cart.SiteCustomer.Email = txtEmail.Text;
            cart.SiteCustomer.Phone = txtPhone.Text;
            cart.SiteCustomer.FaceMasterNewsletter = (chkFaceMasterNewsletter.Checked) ? true : false;
            cart.SiteCustomer.SuzanneSomersNewsletter = (chkSuzanneSomersNewsletter.Checked) ? true : false;

            //if new customer or confirming new password on update, save password in DB
            if ((cart.SiteCustomer.CustomerID == 0) || (txtPasswordConfirm.Text != ""))
                cart.SiteCustomer.Password = txtPassword.Text;
            else
                cart.SiteCustomer.Password = cart.SiteCustomer.Password;

            cart.SiteCustomer.SuzanneSomersCustomerID = -1;


            if (chkShippingAddress.Checked) //check for a new shipping address
            {
                cart.ShipAddress.Street = ShipAddress_Street.Text;
                cart.ShipAddress.Street2 = ShipAddress_Street2.Text;
                cart.ShipAddress.City = ShipAddress_City.Text;
                cart.ShipAddress.Zip = ShipAddress_ZipPostal.Text;
                cart.ShipAddress.State = selStateShip.Text;
                cart.ShipAddress.Country = selCountryShip.Text;
            }
            else { cart.ShipAddress = cart.BillAddress; }

            cart.BillAddress.AddressId = 0; //make sure we enter new addresses
            cart.ShipAddress.AddressId = 0;
            int OK = AdminCart.Customer.EnterCustomer(cart.SiteCustomer, cart.BillAddress, cart.ShipAddress);
            if (OK > 0) 
            { 
                dotnetCARTmessage.Text = "Saved account info.";
                Response.Redirect("ManageUsers.aspx?PageAction=Lookup&FMCustomerID=" + OK.ToString());

            }
            else
            { dotnetCARTmessage.Text = "Email already in the database."; }
         }
        else if ((cart.SiteCustomer.CustomerID == 0) && (txtPassword.Text.Length < 6 || txtPasswordConfirm.Text.Length < 6))
        {
            dotnetCARTmessage.Text = "Enter a password (at least 6 characters)";
        }
    }

    protected void Checked_SeparateShippingAddress(Object sender, System.EventArgs e)
    {

        if (chkShippingAddress.Checked) { pnlShipAddress.Visible = true; }
        else { pnlShipAddress.Visible = false; }
    }

    protected void selCountryBill_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (selCountryBill.SelectedValue != "")
        {
            selStateBill.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(selCountryBill.SelectedValue)));
            selStateBill.DataValueField = "Code";
            selStateBill.DataTextField = "Name";
            selStateBill.DataBind();

            if (selStateBill.Items.Count == 0)
            {
                selStateBill.Visible = false;
                txtStateBill.Visible = true;
            }
            else
            {
                selStateBill.Visible = true;
                txtStateBill.Visible = false;
            }
        }
    }

    protected void selCountryShip_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (selCountryShip.SelectedValue != "")
        {
            selStateShip.DataSource = DBUtil.GetLocations(Convert.ToInt32(DBUtil.GetLocationParentID(selCountryShip.SelectedValue)));
            selStateShip.DataValueField = "Code";
            selStateShip.DataTextField = "Name";
            selStateShip.DataBind();

            if (selStateShip.Items.Count == 0)
            {
                selStateShip.Visible = false;
                txtStateShip.Visible = true;
            }
            else
            {
                selStateShip.Visible = true;
                txtStateShip.Visible = false;
            }
        }
    }
}
