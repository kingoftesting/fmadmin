<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="FM.Admin.OperatingInstructions" Title="Operating Instructions" Codebehind="OperatingInstructions.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<table>
<tr>
<td width="100px">
    <br />
	<asp:Literal ID="LeftHTML" runat="server" />
</td>
<td class="TableDeptItemsNormal" >
    <br />
	<asp:Label ID="BodyText" runat="server" />
</td>
</tr>
</table>

</asp:Content>
