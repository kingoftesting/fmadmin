using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace FM.Admin
{
    public partial class OperatingInstructions : AdminBasePage //System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            int PageID = 3;// Convert.ToInt32(Request["PageID"].ToString());
            //PageTitle.Text = DBUtil.GetScalar("SELECT PageTitle FROM Pages WHERE PageID=" + PageID.ToString());

            BodyText.Text = Biz.GetPageText(PageID);
            LeftHTML.Text = Biz.GetPageText(PageID, 5);
        }
    } 
}
