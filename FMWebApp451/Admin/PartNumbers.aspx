<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" AutoEventWireup="true" Theme="Admin" Inherits="Admin_PartNumbers" Title="Part Numbers" Codebehind="PartNumbers.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<div style="width:950px">
<h1>FaceMaster Part Numbers</h1>

<h2>Newtronix Part Numbers</h2>
<table style="width:100%; border:solid 1px black; font-size:12px; border-collapse:collapse">
<tr style="background-color:blue">
<td style="font-size:12px; color:white; border:solid 1px black; width:30%">Part Number</td>
<td style="font-size:12px; color:white; border:solid 1px black;">Description</td>
<td style="font-size:12px; color:white; border:solid 1px black; width:30%">Notes</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">600011-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">FaceMaster Platinum, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">included in retail box</td>
</tr>

<tr style="background-color:LightBlue">
<td style="font-size:12px; color:black; border:solid 1px black">600004-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">Foam cap, qty=100</td>
<td style="font-size:12px; color:black; border:solid 1px black">finished goods or included in retail box</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">600018-B</td>
<td style="font-size:12px; color:black; border:solid 1px black">Retail Platinum Finger Wands, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">finished goods or included in retail box</td>
</tr>

<tr style="background-color:LightBlue">
<td style="font-size:12px; color:black; border:solid 1px black">600022-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">Retail Platinum Hand Wands, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">finished goods or included in retail box</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">600010-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">Refurbished 'purple' FaceMaster, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">included in retail box</td>
</tr>

<tr style="background-color:LightBlue">
<td style="font-size:12px; color:black; border:solid 1px black">600021-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">Retail 'purple' Hand Wands, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">LS-300024-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">Refurbished 'purple' FaceMaster Retail Box, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">incl. LS-300025-A Refurb Label</td>
</tr>

</table>
<br />

<h2>Cosmetix West Part Numbers</h2>
<table style="width:100%; border:solid 1px black; font-size:12px; border-collapse:collapse">
<tr style="background-color:blue">
<td style="font-size:12px; color:white; border:solid 1px black; width:30%">Part Number</td>
<td style="font-size:12px; color:white; border:solid 1px black;">Description</td>
<td style="font-size:12px; color:white; border:solid 1px black; width:30%">Notes</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">600013-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">Soothing Conductive Serum, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">finished goods or included in retail box</td>
</tr>

</table>
<br />

<h2>Cosmetic Labs Part Numbers</h2>
<table style="width:100%; border:solid 1px black; font-size:12px; border-collapse:collapse">
<tr style="background-color:blue">
<td style="font-size:12px; color:white; border:solid 1px black; width:30%">Part Number</td>
<td style="font-size:12px; color:white; border:solid 1px black;">Description</td>
<td style="font-size:12px; color:white; border:solid 1px black; width:30%">Notes</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">600019-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">Anti-Aging Serum w/ Glycopeptides, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">finished goods (incl. box & insert)</td>
</tr>

</table>
<br />

<h2>Non-Supplier-Specific Part Numbers</h2>
<table style="width:100%; border:solid 1px black; font-size:12px; border-collapse:collapse">
<tr style="background-color:Blue">
<td style="font-size:12px; color:white; border:solid 1px black; width:30%">Part Number</td>
<td style="font-size:12px; color:white; border:solid 1px black;">Description</td>
<td style="font-size:12px; color:white; border:solid 1px black; width:30%">Notes</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">LS-300029-B</td>
<td style="font-size:12px; color:black; border:solid 1px black">Sleeve, Platinum Retail Box (small), qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:LightBlue">
<td style="font-size:12px; color:black; border:solid 1px black">LS-300034-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">Sleeve, HDI Basic Platinum Retail Box (small), qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">DW-150009-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">Black Box, Landsberg, Platinum, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:LightBlue">
<td style="font-size:12px; color:black; border:solid 1px black">LS-300021-B</td>
<td style="font-size:12px; color:black; border:solid 1px black">Platinum QuickStart Guide, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">LS-300022-B</td>
<td style="font-size:12px; color:black; border:solid 1px black">Platinum Instruction Manual, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:LightBlue">
<td style="font-size:12px; color:black; border:solid 1px black">LS-300023-B</td>
<td style="font-size:12px; color:black; border:solid 1px black">Platinum DVD, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">LS-300001-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">'purple' Instruction Manual, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:LightBlue">
<td style="font-size:12px; color:black; border:solid 1px black">LS-300004-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">'purple' Insert (proper plug-in), qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">LS-300012-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">'purple' DVD, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">DW-150010-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">Brown Shipper, Landsberg, Platinum, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">DW-150011-A</td>
<td style="font-size:12px; color:black; border:solid 1px black">White Insert, Landsberg, Platinum, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

<tr style="background-color:white">
<td style="font-size:12px; color:black; border:solid 1px black">FM2BAT</td>
<td style="font-size:12px; color:black; border:solid 1px black">Battery, 9V, qty=1</td>
<td style="font-size:12px; color:black; border:solid 1px black">&nbsp;</td>
</tr>

</table>

<br /><br />

</div>
</asp:Content>

