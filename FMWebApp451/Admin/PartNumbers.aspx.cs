using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using AdminCart;

public partial class Admin_PartNumbers : AdminBasePage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Certnet.Cart cart = new Certnet.Cart();
        cart.Load(Session["cart"]);
        int customerID = cart.SiteCustomer.CustomerID;
        try
        {
            if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "REPORTS")))
            { }
            else
            {
                throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
            }
        }
        catch (SecurityException ex)
        {
            string msg = "Error in PartNumbers" + Environment.NewLine;
            msg += "User did not have permission to access page" + Environment.NewLine;
            msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            Response.Redirect("../Login.aspx");
        }
    }
}
