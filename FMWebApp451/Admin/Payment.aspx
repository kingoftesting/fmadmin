<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" Inherits="FM.Admin.Payment" Title="Payment" Codebehind="Payment.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="alpha omega sixteen columns">

    <div>
        <strong><span style="color:#aaaaaa">YOUR INFO -&gt; SHIPPING OPTION -&gt; </span>PAYMENT TYPE</strong>
		<br />
		<br />
		<strong>STEP 3 of 3 - PAYMENT TYPE</strong>
        <br />
		<i>Please fill in the type of payment below</i><br />
		<hr />
    </div>

    <div>
        <table class="custom" width="100%">
            <tr>
                <td class="custservice">
                    <asp:Literal ID="cd" runat="server" />
                </td>
            </tr>
        </table>
        <hr />
    </div>

    <div>
        <asp:Label ID="Label14" Runat="server" Text="<strong>Add an order note (mandatory if this is non-standard transaction):</strong>"/>
        <div class="simpleClear">&nbsp;</div>
        <asp:TextBox ID="txtOrderNote" runat="server" TextMode="MultiLine" Width="90%" Rows="5" MaxLength="2000"></asp:TextBox>
        <hr />
    </div>

    <div>
        <table>
            <tr>
                <td class = "custservice" style="width:30%" valign="top"><strong>Billing Information:</strong><br />
                    <asp:Label id="BillInfo" Visible="true" runat="server" /></td>
                <td class = "custservice" style="width:30%" valign="top"><strong>Shipping Information:</strong><br />
                    <asp:Label id="ShipInfo" Visible="true" runat="server" /></td>
            </tr>
        </table>
        <hr />
    </div>

</div>

<asp:Panel ID="pnlPayment" runat="server" Visible="true">

<div class="alpha omega sixteen columns">

    <div>
        <table>
            <tr>
                <td class="custservice" valign="top" ><strong>Amount Due:&nbsp;</strong></td>
                <td class="custservice" valign="top" >
                    <asp:Label id="TotalLabel" runat="server" />&nbsp;&nbsp;
                    <asp:Label id="YourTotalLabel" Visible="false" runat="server" />
                </td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td class="custservice" style="display:none"><strong>Coupon Code:&nbsp;</strong></td>
                <td class="custservice">
                    <div class="groupHTML" style="display:none">
                        <asp:TextBox ID="txtCoupon" runat="server" Width="30%"/>
                        <asp:Label ID="labCouponid" runat="server" Width="30%"/>
                        <asp:Label ID="Label1" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;" />
                        <asp:Button ID="btnCoupon" runat="server" Text="Apply" OnClick="btnCoupon_Click" CausesValidation="false" /><br />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="custservice" colspan="2"><asp:Label ID="lblCoupon" runat="server" ForeColor="red" Font-Size="12px" /></td>
            </tr>				
        </table>
        <hr />				
    </div>

    <div>
        <table>
            <tr >
                <td class="custservice" style="width:150px">Credit Card Number:&nbsp;</td>
                <td class="custservice" align="left">
                    <asp:TextBox id="Payment_CardNumber" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="Payment_CardNumber"
							Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
			            <span style="color:red">Please Enter Numbers Only!</span>
			        <br /><br />
        		    <asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" ControlToValidate="Payment_CardNumber"
    	                    Display="Dynamic" ErrorMessage="** Card Number Required."></asp:RequiredFieldValidator>
                </td>
                <td class="custservice" align="right" style="width:150px">Expires (mm/yy):&nbsp;</td>
                <td class="custservice" align="left" style="width:200px">
                    <div class="groupHTML">
                        <asp:DropDownList ID="ddlccMonth" Width="30%" runat="server" AutoPostBack="true">
                          <asp:ListItem Text="" Selected="True" />
                          <asp:ListItem Text="01" Value = "1" Selected="False" />
                          <asp:ListItem Text="02" Value = "2" Selected="False"  />
                          <asp:ListItem Text="03" Value = "3" Selected="False"  />
                          <asp:ListItem Text="04" Value = "4" Selected="False" />
                          <asp:ListItem Text="05" Value = "5" Selected="False"  />
                          <asp:ListItem Text="06" Value = "6" Selected="False"  />
                          <asp:ListItem Text="07" Value = "7" Selected="False"  />
                          <asp:ListItem Text="08" Value = "8" Selected="False"  />
                          <asp:ListItem Text="09" Value = "9" Selected="False"  />
                          <asp:ListItem Text="10" Value = "10" Selected="False"  />
                          <asp:ListItem Text="11" Value = "11" Selected="False"  />
                          <asp:ListItem Text="12" Value = "12" Selected="False"  />
                        </asp:DropDownList>
                        <asp:Label ID="lblDateSpace" Text="&nbsp; &nbsp;" runat="server"></asp:Label>
                        <asp:DropDownList ID="ddlccYear" Width="40%" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <br />
                    <asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" ControlToValidate="ddlccMonth"
		    	            Display="Dynamic" ErrorMessage="** An Expiration Month Is Required."></asp:RequiredFieldValidator>
		    	     <br />
		             <asp:RequiredFieldValidator id="RequiredFieldValidator6" runat="server" ControlToValidate="ddlccYear" Display="Dynamic"
		    	            ErrorMessage="** An Expiration Year Is Required."></asp:RequiredFieldValidator>

                </td>
            </tr>
            <tr>
                <td class="custservice" style="width:150px">Name on Card:&nbsp;</td>
                <td class="cschkoutbody" align="left"><asp:TextBox id="Payment_CardName" runat="server"></asp:TextBox></td>			        
                    <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ControlToValidate="Payment_CardName"
							Display="Dynamic" ErrorMessage="** Card Name Is Required"></asp:RequiredFieldValidator>
                <td class="custservice" align="right" style="width:150px">Security Code:&nbsp;</td>
                <td class="custservice" align="left">
                    <div class="groupHTML">
			            <asp:TextBox id="Payment_CardCode" runat="server" Font-Size="12px" MaxLength="4"  Width="30"></asp:TextBox>
                        <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ControlToValidate="Payment_CardCode"
							Display="Dynamic" ErrorMessage="** Card Code Is Required"></asp:RequiredFieldValidator>
                   </div>
                </td>
            </tr>
        </table>
        <hr />
    </div>

    <div>
        <table>
            <tr>
                <td class="custservice" colspan="2">
                    <asp:Label ID="CartLabel" runat="server" Text="<h4>Enter your information, then click 'Complete Order' only once.</h4>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="custservice" align="left" width="150px">
                    <asp:ImageButton AlternateText="Revise Order" id="Imagebutton1" runat="server" ImageUrl="../images/buttons/ReviseOrder.jpg" OnClick="ReviseOrder_Click" CausesValidation="False" />
                </td>
                <td class="custservice" align="right">
                    <asp:ImageButton alternatetext="complete Order" id="ImageButton3" runat="server" ImageUrl="../images/buttons/CompleteOrder.jpg" OnClick="CompleteOrder_Click" />
                </td>
            </tr>			   
        </table>
    </div>
    
    <div>	        
        <table class="custom" width="600px">
            <tr >
                <td class="custservice" >    		        
                    <asp:Label id="Errormsg" runat="server" CssClass="CartError" />
                    <asp:PlaceHolder id="PlaceHolderPayment2" runat="server">&nbsp;</asp:PlaceHolder>
			    	&nbsp;
		            <asp:Literal ID="Literal1" runat="server" />
                </td>
            </tr>
        </table>
    </div>	

</div>
</asp:Panel>

<asp:Panel ID="pnlNoPayment" runat="server" Visible="false">

<div class="alpha omega sixteen columns">

    <h1>This is a $0 Transaction! <br />Are you sure you want to proceed?</h1>

    <table width="900px">
        <tr>
            <td class="custservice" colspan="2">
               <asp:Label ID="lblNoCharge" runat="server" Text=""></asp:Label>
               <br />
            </td>
        </tr>
        <tr>
            <td class="custservice" align="left" style="width:30%" >
                <asp:ImageButton AlternateText="Revise Order" id="Imagebutton2" runat="server" ImageUrl="../images/buttons/ReviseOrder.jpg" OnClick="ReviseOrder_Click" CausesValidation="False" />
            </td>
            <td class="custservice" align="left" >
                <asp:ImageButton alternatetext="complete Order" id="ImageButton4" runat="server" ImageUrl="../images/buttons/CompleteOrder.jpg" OnClick="CompleteOrder_Click" />
            </td>
        </tr>
    </table>
    <p>&nbsp;</p>

</div>
</asp:Panel>

	
<script type="text/javascript" language="JavaScript">
      var viewer;
      function CreateWin() {
        viewer = "good";
        viewer = open('cchelp.aspx', 'viewer', 'toolbar=yes,status=yes,scrollbars=yes,location=yes,menubar=yes,directories=yes,height=400,width=400');
        if (viewer.opener == null) viewer.opener = window; 
        viewer.opener.name = "labopener";
      }
    </script>

</asp:Content>

