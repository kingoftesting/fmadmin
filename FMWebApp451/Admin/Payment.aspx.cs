using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;
using FM2015.Helpers;

namespace FM.Admin
{
    public partial class Payment : AdminBasePage //System.Web.UI.Page
    {
        Cart cart;

        void Page_Init(Object sender, EventArgs e)
        {

            cart = new Cart();
            cart.Load(Session["CScart"]);
            int test = cart.ShipType;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (cart.Total == 0)
            {
                pnlNoPayment.Visible = true;
                pnlPayment.Visible = false;
            }

            if (cart.SiteCoupon.CouponCodeID == 0)
            {
                txtCoupon.Visible = true;
                btnCoupon.Text = "Apply";
            }
            else
            {
                txtCoupon.Visible = false;
                labCouponid.Text = cart.SiteCoupon.CouponCode;
                labCouponid.Visible = true;
                btnCoupon.Text = "Remove";
            }

            if (Session["lblCoupon"] == null)
            { lblCoupon.Visible = false; }
            else
            {
                lblCoupon.Visible = true;
                lblCoupon.Text = Session["lblCoupon"].ToString();
            }


            if (!Page.IsPostBack)
            {
                //set expiration date range
                ddlccMonth.SelectedValue = DateTime.Now.Month.ToString();

                SortedList yearList = new SortedList();
                int year = DateTime.Now.Year;
                for (int i = 0; i < 15; i++)
                {
                    yearList.Add(year.ToString(), year.ToString());
                    year++;
                }
                ddlccYear.DataSource = yearList;
                ddlccYear.DataTextField = "Key";
                ddlccYear.DataValueField = "Value";
                ddlccYear.DataBind();
                ddlccYear.SelectedValue = DateTime.Now.Year.ToString();

                if (AdminCart.Config.CartSetting == "Test")
                {

                    Payment_CardNumber.Text = "4111111111111111";
                    //Payment_CcMonth.Text = "12";
                    //Payment_CcYear.Text = "09";
                    Payment_CardCode.Text = "987";
                    Payment_CardName.Text = "Joe Testrecord";
                }
                else
                {
                    Payment_CardName.Text = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName;
                }

                CartLabel.Text = "Enter your information, then click 'Complete Order' only once.";
                lblNoCharge.Text = "Enter your information, then click 'Complete Order' only once.";
            }


            if (cart.SiteCoupon.CouponCodeID != 0)
            {
                lblCoupon.Text = cart.SiteCoupon.CouponCode + "-" + cart.SiteCoupon.CouponDescription;
            }

            if (cart.SiteCustomer.CustomerID == 0) Response.Redirect("ManageUsers.aspx");

            ShipInfo.Text = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + "<br>" + cart.ShipAddress.Street;
            if ((string.IsNullOrEmpty(cart.ShipAddress.Street2) || (cart.ShipAddress.Street2 == " ")))
            { ShipInfo.Text += "<br>" + cart.ShipAddress.City + ", " + cart.ShipAddress.State + " " + cart.ShipAddress.Zip + "<br>" + DBUtil.GetCountry(cart.ShipAddress.Country) + "<br> Shipping Method: "; }
            else { ShipInfo.Text += "<br>" + cart.ShipAddress.Street2 + "<br>" + cart.ShipAddress.City + ", " + cart.ShipAddress.State + " " + cart.ShipAddress.Zip + "<br>" + DBUtil.GetCountry(cart.ShipAddress.Country) + "<br> Shipping Method: "; }

            if (cart.ShipType == 1) { ShipInfo.Text += "Ground"; }
            if (cart.ShipType == 2) { ShipInfo.Text += "Expedited (2-Day Air)"; }
            if (cart.ShipType == 3) { ShipInfo.Text += "Express"; }

            BillInfo.Text = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName;
            if ((string.IsNullOrEmpty(cart.BillAddress.Street2) || (cart.BillAddress.Street2 == " ")))
            { BillInfo.Text += "<br>" + cart.BillAddress.Street + "<br>" + cart.BillAddress.City + ", " + cart.BillAddress.State + " " + cart.BillAddress.Zip + "<br>" + DBUtil.GetCountry(cart.BillAddress.Country); }
            else { BillInfo.Text += "<br>" + cart.BillAddress.Street + "<br>" + cart.BillAddress.Street2 + "<br>" + cart.BillAddress.City + ", " + cart.BillAddress.State + " " + cart.BillAddress.Zip + "<br>" + DBUtil.GetCountry(cart.BillAddress.Country); }

            cd.Text = DisplayCart();

            TotalLabel.Text = cart.Total.ToString("N");
            //if (cart.Total < 0)
            //{
            //    TotalLabel.Text = 0; //cart.Currency.Format(0, cart.Currency.Base);
            //    YourTotalLabel.Text = 0;// cart.Currency.ConvertFormat(0, cart.Customer.Currency);
            //}
            if (cart.Tran.ResultMsg != "")
            {
                CartLabel.Style.Add("color", "red");
                CartLabel.Text = cart.Tran.ResultMsg;

                lblNoCharge.Style.Add("color", "red");
                lblNoCharge.Text = cart.Tran.ResultMsg;
            }

        }

        protected string DisplayCart()
        {
            cart.CalculateTotals();

            string carthtml = @"
<table cellpadding='5' rules='all' border='1px solid black' width='90%'; style='border-collapse:collapse';>
	<tr align='Center' style='background-color:#DDDDDD;'>
		<td class = 'custservice'><span>Item</span></td>
        <td class = 'custservice'><span>Quantity</span></td>
        <td class = 'custservice'><span>Price</span></td>
        <td class = 'custservice'><span>TotalDiscount</span></td>
        <td class = 'custservice'><span>Total</span></td>
	</tr>";


            foreach (Item i in cart.Items)
            {
                carthtml += @"
    <tr>
		<td class = 'custservice' align=left valign='center'><span><img Width='160px'  src=../images/products/" + i.LineItemProduct.CartImage + "> " + i.LineItemProduct.Name + @"</span></td>
        <td class = 'custservice' align='Right'><span>" + i.Quantity.ToString() + @"</span></td>
        <td class = 'custservice' align='Right'><span>" + i.LineItemProduct.Price.ToString("N") + @"</span></td>
        <td class = 'custservice' align='Right'><span>" + i.LineItemDiscount.ToString("N") + @"</span></td>
        <td class = 'custservice' align='Right'><span>" + i.LineItemTotal.ToString("N") + @"</span></td>
	</tr>
        ";
            }

            string shipmode = "";
            if (cart.ShipType == 1) shipmode = "Ground";
            if (cart.ShipType == 2) shipmode = "Expedited (2-Day Air)";
            if (cart.ShipType == 3) shipmode = "Express (Guarenteed Overnight)";

            carthtml += @"
    <tr>
		<td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>SubTotal:</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.TotalProduct.ToString("N") + @"</span></td>
	</tr><tr>
		<td class = 'custservice' align='Right' colspan='4'><span>Shipping:  " + shipmode + @"</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.TotalShipping.ToString("N") + @"</span></td>
	</tr><tr>
		<td class = 'custservice' align='Right' colspan='4'><span>Sales Tax:</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.TotalTax.ToString("N") + @"</span></td>
	</tr>";
            if (cart.TotalDiscounts != 0)
            {
                carthtml += @"        <tr>
		<td class = 'custservice' align='Right' colspan='4'><span>Coupon Discount: (" + cart.SiteCoupon.CouponCode + " - " + cart.SiteCoupon.CouponDescription + @")</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.TotalDiscounts.ToString("N") + @"</span></td>";
            }
            carthtml += @"</tr><tr>
		<td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>Total:</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.Total.ToString("N") + @"</span></td>
	</tr>
</table>";

            return carthtml;
        }

        protected void CompleteOrder_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            string ordernote = ValidateUtil.CleanUpString(txtOrderNote.Text);
            ordernote = ordernote.TrimStart(' ').TrimEnd(' '); //get rid of any space at frontend & backend
            Session["PaymentOrderNote"] = ordernote;

            cart.SiteOrder.OrderID = 600001;
            cart.SiteOrder.ByPhone = true;
            cart.SiteOrder.SubTotal = cart.TotalProduct;
            cart.SiteOrder.TotalShipping = cart.TotalShipping;
            cart.SiteOrder.TotalTax = cart.TotalTax;
            cart.SiteOrder.TotalCoupon = cart.TotalDiscounts;
            cart.SiteOrder.Total = cart.Total;
            cart.SiteOrder.OrderDate = DateTime.Now;
            Session["CScart"] = cart;

            if (cart.Total != 0)
            {
                //validate data
                bool NoInputErrors = true;

                if (Payment_CardNumber.Text.Length < 15)
                {
                    NoInputErrors = false;
                    cart.Tran.ResultMsg = "Card is less than 15 characters.  Please correct, then try again.";
                    Session["CScart"] = cart;
                }
                if (Payment_CardName.Text.Length < 1)
                {
                    NoInputErrors = false;
                    cart.Tran.ResultMsg = "Name on card is required.  Please correct, then try again.";
                    Session["CScart"] = cart;
                }
                if (cart.Items.ItemCount() == 0)
                {
                    NoInputErrors = false;
                    cart.Tran.ResultMsg = "There are no items in your cart.  You must add items to proceed.";
                    Session["CScart"] = cart;
                }

                //check for existing charge on this order
                bool NoExistingCarges = true;

                if (NoInputErrors && NoExistingCarges)
                {
                    cart.Tran.CardNo = Payment_CardNumber.Text;
                    //cart.Tran.CardType = Payment_CardCode.Text;
                    cart.Tran.CardHolder = Payment_CardName.Text;

                    //if (Payment_CcMonth.Text.Length == 1) { cart.Tran.ExpDate = "0" + Payment_CcMonth.Text + Payment_CcYear.Text; }
                    //else { cart.Tran.ExpDate = Payment_CcMonth.Text + Payment_CcYear.Text; }
                    string expYear = ddlccYear.SelectedValue.ToString();
                    expYear = expYear.Substring(expYear.Length - 2);
                    cart.Tran.ExpDate = ddlccMonth.SelectedItem.ToString() + expYear;
                    cart.Tran.CvsCode = Payment_CardCode.Text;
                    cart.Tran.ResultMsg = "";
                    //determine card type

                    Session["CScart"] = cart;
                    Response.Redirect("Processing.aspx");
                }
            }
            else
            {

                if (string.IsNullOrEmpty(ordernote))
                {
                    cart.Tran.ResultMsg = "This is a $0 Transaction. You must include an Order Note";
                    Session["CScart"] = cart;
                    //exit back to Payment with the Error Message
                }
                else
                {
                    Response.Redirect("Processing.aspx"); 
                }
            }
            Response.Redirect("Payment.aspx");
        }

        protected void ReviseOrder_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            cart.Tran.ResultMsg = "";
            Session["CScart"] = cart;
            Response.Redirect("ViewCart.aspx");
        }

        protected void btnCoupon_Click(object sender, EventArgs e)
        {
            if (btnCoupon.Text == "Remove")
            {
                cart.SiteCoupon = new Coupon();
                Session["lblCoupon"] = "";
                Session["CScart"] = cart;
                Response.Redirect("Payment.aspx");
            }

            if ((txtCoupon.Text.Length < 0) || (txtCoupon.Text.Length > 10))
                Session["lblCoupon"] = "This coupon code was not found.";
            //lblCoupon.Text = "This coupon code was not found.";

            Coupon c = new Coupon(txtCoupon.Text);
            if (c.CouponCodeID == 0)
            {
                cart.SiteCoupon = new Coupon(); //default couponid=0
                Session["lblCoupon"] = "This coupon code was not found.";
                //lblCoupon.Text = "This coupon code was not found.";

            }
            else if (c.CouponEndDate < DateTime.Now)
            {
                cart.SiteCoupon = new Coupon(); //default couponid=0
                Session["lblCoupon"] = "This coupon has expired";
                //lblCoupon.Text = "This coupon has expired";
            }
            else
            {
                cart.SiteCoupon = c;
                Session["lblCoupon"] = "Coupon Applied.";
                //lblCoupon.Text = "Coupon Applied.";
            }
            Session["CScart"] = cart;
            Response.Redirect("Payment.aspx");
        }
    } 
}
