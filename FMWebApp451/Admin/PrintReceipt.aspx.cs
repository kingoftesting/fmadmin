using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;
using FM2015.Helpers;

public partial class Admin_PrintReceipt : AdminBasePage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Define the name and type of the client scripts on the page.
        String csname1 = "fixform"; //so that only the button controls spec'd generate new windows
        Type cstype = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;

        // Check to see if the startup script is already registered.
        if (!cs.IsStartupScriptRegistered(cstype, csname1))
        {
            StringBuilder cstext1 = new StringBuilder();
            //cstext1.Append("<script type=text/javascript> alert('Hello World!') </");
            //cstext1.Append("script>");

            cstext1.Append("<script type='text/javascript'>");
            cstext1.Append("function fixform() { ");
            cstext1.Append("if (opener.document.getElementById('aspnetForm').target != '_blank') return; ");
            cstext1.Append("opener.document.getElementById('aspnetForm').target = ''; ");
            cstext1.Append("opener.document.getElementById('aspnetForm').action = opener.location.href; ");
            cstext1.Append("} </script>");

            cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
        } 
        
        Cart cart = new Cart();
        cart.Load(Session["CScart"]);
        DateTime orderDate = cart.SiteOrder.OrderDate;
        string cardno = cart.Tran.CardNo;
        if (cardno.Length > 11)
            cardno = cardno.Substring(0, 4) + "XXXXXX" + cardno.Substring(cardno.Length - 4, 4);

        string body = "Thank you for shopping at FaceMaster.com! We appreciate your business." + "<br /><br />" +
"Order Number: " + cart.OrderID.ToString() + "<br /><br />" +
"Order Date: " + orderDate + "<br /><br />" +
"Credit Card#: " + cardno + "<br /><br />" +
"Total Sale: " + cart.SiteOrder.Total.ToString("C") + "<br /><br />" +
"========================================================================<br />" +
"CUSTOMER INFORMATION<br />" +
"------------------------------------------------------------------------<br />" +
cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + "<br />" +
cart.BillAddress.Street + "<br />";

        if ((string.IsNullOrEmpty(cart.BillAddress.Street2) || (cart.BillAddress.Street2 == " "))) { }
        else { body += cart.BillAddress.Street2 + "<br />"; }

        body += cart.BillAddress.City + ", " + cart.BillAddress.State + " " + cart.BillAddress.Zip + "<br />" +
        DBUtil.GetCountry(cart.BillAddress.Country) + "<br />" +
        "<br />" +
        "========================================================================<br />" +
        "SHIPPING ADDRESS<br />" +
        "------------------------------------------------------------------------<br />" +
        cart.ShipAddress.Street + "<br />";

        if ((string.IsNullOrEmpty(cart.ShipAddress.Street2) || (cart.ShipAddress.Street2 == " "))) { }
        else { body += cart.ShipAddress.Street2 + "<br />"; }

        body += cart.ShipAddress.City + ", " + cart.ShipAddress.State + " " + cart.ShipAddress.Zip + "<br />" +
            DBUtil.GetCountry(cart.ShipAddress.Country) + "<br />" +
            "Shipping Method: ";
        if (cart.ShipType == 1) { body += "Ground"; }
        if (cart.ShipType == 2) { body += "Expedited (2-Day Air)"; }
        if (cart.ShipType == 3) { body += "Express (Guarenteed Overnight)"; }

        body += "<br />" +
            "========================================================================<br />" +
            "PRODUCTS ORDERED<br /><br />";

        string carthtml = @"
                    <table width = '600px' colspan = '4' cellpadding='5' rules='all' BorderColor='Black' bordercolor='Black' border='1' style='border-color:Black;width:80%;border-collapse:collapse;'>
                    <tr align='Center' style='background-color:#DDDDDD;'>
                        <td class = 'custservice' ><span>Item</span></td>
                        <td class = 'custservice' ><span>Quantity</span></td>
                        <td class = 'custservice' ><span>Price</span></td>
                        <td class = 'custservice' ><span>Discount</span></td>
                        <td class = 'custservice' ><span>Total</span></td>
                    </tr>";


        foreach (Item i in cart.Items)
        {
            carthtml += @"
                    <tr>
                        <td class = 'custservice' align=left valign='center'><span><img src=../images/products/" + i.LineItemProduct.CartImage + "> " + i.LineItemProduct.Name + @"</span></td>
                        <td class = 'custservice' align='Right'><span>" + i.Quantity.ToString() + @"</span></td>
                        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + i.LineItemProduct.Price.ToString("N") + @"</span></td>
                        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + i.LineItemDiscount.ToString("N") + @"</span></td>
                        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + i.LineItemTotal.ToString("N") + @"</span></td>
                    </tr>";
        }

        string shipmode = "";
        if (cart.ShipType == 1) shipmode = "Ground";
        if (cart.ShipType == 2) shipmode = "Expedited (2-Day Air)";
        if (cart.ShipType == 3) shipmode = "Express (Guarenteed Overnight)";

        carthtml += @"
                <tr>
                    <td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>SubTotal:</span></td>
                    <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.SubTotal.ToString("N") + @"</span></td>
                </tr><tr>
                    <td class = 'custservice' align='Right' colspan='4'><span>Shipping: " + shipmode + @"</span></td>
                    <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.TotalShipping.ToString("N") + @"</span></td>
                </tr><tr>
                    <td class = 'custservice' align='Right' colspan='4'><span>Sales Tax:</span></td>
                    <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.TotalTax.ToString("N") + @"</span></td>
                </tr>";

        if (cart.TotalDiscounts != 0)
        {
            carthtml += @"<tr>
                        <td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>Coupon Discount: (" + cart.SiteCoupon.CouponCode + " - " + cart.SiteCoupon.CouponDescription + @")</span></td>
                        <td class = 'custservice' nowrap='nowrap' align='Right'><span> (" + cart.SiteOrder.TotalCoupon.ToString("N") + @")</span></td>
                    </tr>";
        }

        carthtml += @"<tr>
                    <td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>Total:</span></td>
                    <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.Total.ToString("N") + @"</span></td>
                </tr>
                </table>";

        litPrintReceipt.Text = body + carthtml;

    }
}
