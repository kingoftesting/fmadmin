using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;
using adminCER;

namespace FM.Admin
{
    public partial class Processing : AdminBasePage //System.Web.UI.Page
    {
        Cart cart;

        void Page_Init(Object sender, EventArgs e)
        {
            //show shipping info only
            //bShipcalculated = true;
            //PlaceholderShipping1.Visible = false;

            cart = new Cart();
            cart.Load(Session["CScart"]);

            this.Page.Header.Attributes.Add("Created By", "Dave, modifed by Rodger");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (cart.SiteCustomer.CustomerID != 0)
            {
                if (Request["Proc"] != null)
                {
                    Cart subCart = cart.CreateSubscriptionCart();
                    if (subCart.Items.ItemCount() > 0) //OK, there is at least one subscription item to be processed
                    {
                        string promoCodeID = Session["PayWhirlPromoCodeID"].ToString();
                        if (subCart.SubscriptionProcessing(promoCodeID) != 0) //unsuccessful subscription for some reason
                        {
                            cart.Load(Session["CScart"]); //get back original cart
                            cart.CalculateTotals();
                            Response.Redirect("Payment.aspx"); 
                        }
                    }

                    //continue processing rest of order; subscription lineitemTotal = 0 & will be processed by Shopify
                    cart.CalculateTotals();

                    cart.SaveToOrder(); //drop into orders table
                    OrderAction orderaction = new OrderAction();
                    orderaction.OrderID = cart.OrderID;
                    orderaction.OrderActionType = 1; //PLACED
                    orderaction.OrderActionDate = DateTime.Now;
                    orderaction.OrderActionReason = "New Order Saved To DB";
                    orderaction.OrderActionBy = "AdminCart";
                    orderaction.SSOrderID = -1;
                    orderaction.SaveDB();

                    Session["CScart"] = cart;

                    if (cart.Charge() == 0)
                    {
                        orderaction = new OrderAction();
                        orderaction.OrderID = cart.OrderID;
                        orderaction.OrderActionType = 2; //APPROVED
                        orderaction.OrderActionDate = DateTime.Now;
                        orderaction.OrderActionReason = "Order Successfully Charged";
                        orderaction.OrderActionBy = "AdminCart";
                        orderaction.SSOrderID = -1;
                        orderaction.SaveDB();

                        //next, check for any coupon usage
                        if (cart.SiteCoupon.CouponCodeID != 0) { cart.SiteCoupon.SaveUsageDB(cart.OrderID); }

                        //add a new entry into the CER database
                        adminCER.CER cer = new adminCER.CER(cart);
                        int result = cer.Add();

                        string ordernote = "Processed Order; " + Session["PaymentOrderNote"].ToString();
                        OrderNote.Insertordernote(cart.OrderID, cer.ReceivedBy, ordernote);

                        Response.Redirect("ThankYou.aspx");
                    }
                    else
                    {
                        Response.Redirect("Payment.aspx");
                    }
                }

                MasterPage3_Blank.SetRefreshValues(this.Page.Header, "refresh", "2;url=Processing.aspx?Proc=charge");

                ProcessingMessage.Text = @"Processing Your Order...Please Wait<br /><img src='../images\wait30trans.gif' alt='Please Wait...' />";
            }
            else
            {
                ProcessingMessage.Text = "It appears you have already completed your order, or you session has timed out.";
            }

        }
    } 
}
