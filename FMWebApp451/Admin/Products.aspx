<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="FM.Admin.Products" Title="Products" Codebehind="Products.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>&nbsp;</p>
    <asp:Repeater ID="productrepeater" runat="server" OnItemDataBound="productrepeater_ItemDataBound" >
    
    
    <ItemTemplate>
        <table class="Catalog" style="width:750px;" >
			<tr class="Catalog">
			    <td style="width:160px;"><img alt="product image" width="125px" src="../images/products/<%# DataBinder.Eval(Container.DataItem, "thumbnailImage")%>" /></td>
				<td class="aCatalog" style="text-align:left;" ><strong><%# DataBinder.Eval(Container.DataItem, "name")%></strong><br />
					$<%# DataBinder.Eval(Container.DataItem, "saleprice","{0:n}")%>
					<br />
				    <%# DataBinder.Eval(Container.DataItem,"ShortDescription") %>
                    <asp:HyperLink ID="lnkImg" runat="server"><img alt="buynow" src="../images/buynow_pink.jpg" /></asp:HyperLink>
                </td>
			</tr>
		</table>
	</ItemTemplate>
	
	<ItemTemplate>
	    <table class="Catalog" style="width:100%;">
			<tr class="Catalog">
				<td style="margin:5px; width:15%;">
                    <asp:HyperLink ID="lnkImg3" runat="server">
					    <img alt="" width="125px" src='../images/products/<%# DataBinder.Eval(Container.DataItem,"thumbnailimage") %>' />
                    </asp:HyperLink>
                 </td>
				<td align="left" valign="top" class="aCatalog" style="width:50%;"><b><%# Eval("Name") %></b><br><br>
					<%# DataBinder.Eval(Container.DataItem,"ShortDescription") %>
				</td>
                <td class="aCatalog" style="width: 100px; text-align: right; vertical-align: top;"><strong>$<%# DataBinder.Eval(Container.DataItem,"Price", "{0:n}") %></strong><br />
                    <asp:Literal ID="LitSalePriceLine" runat="server" />
                </td>
                <td>
                    <asp:HyperLink ID="lnkImg4" runat="server"><img alt="buynow" src="../images/buynow_pink.jpg" ></asp:HyperLink>
                </td>
			</tr>
		</table><hr>
	</ItemTemplate>
    
    </asp:Repeater>
    <asp:Literal ID="BodyText" runat="server" /></asp:Content>