using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;

namespace FM.Admin
{
    public partial class Products : AdminBasePage //System.Web.UI.Page
    {
        Cart cart = new Cart();

        protected void Page_Load(object sender, EventArgs e)
        {
            cart.Load(Session["CScart"]);

            productrepeater.DataSource = Biz.GetAdminProductList(true);
            productrepeater.DataBind();
        }

        protected void productrepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Product p = (Product)e.Item.DataItem;
                string pLink = "";
                if (p.IsBundle)
                { pLink = "ViewCart.aspx?" + p.BundleString; }
                else
                { pLink = "ViewCart.aspx?Action=Add&ProductID=" + p.ProductID.ToString(); }


                HyperLink lnkImg = (HyperLink)e.Item.FindControl("lnkImg");
                if (lnkImg != null)
                { lnkImg.NavigateUrl = pLink; }

                HyperLink lnkImg2 = (HyperLink)e.Item.FindControl("lnkImg2");
                if (lnkImg2 != null)
                { lnkImg2.NavigateUrl = pLink; }

                HyperLink lnkImg3 = (HyperLink)e.Item.FindControl("lnkImg3");
                if (lnkImg3 != null)
                { lnkImg3.NavigateUrl = pLink; }

                HyperLink lnkImg4 = (HyperLink)e.Item.FindControl("lnkImg4");
                if (lnkImg4 != null)
                { lnkImg4.NavigateUrl = pLink; }

                if (p.Price > p.SalePrice)
                {
                    Literal litSalePrice = (Literal)e.Item.FindControl("LitSalePriceLine");

                    litSalePrice.Text = "$" + p.SalePrice.ToString("N") + " - Sale!";
                }
            }
        }
    } 
}
