<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin"  AutoEventWireup="true" Inherits="Admin_Resources" Title="Resources" Codebehind="Resources.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

   <div style="font-size:14px; margin-left:0px">
        <p>Admin Resources</p>
        <p>&nbsp</p>
        <p>FDA Stuff (QSR, Labeling, MDR, etc.)</p>
        <p>&nbsp;</p>
        <ul style="list-style-type: square; text-align:left;">
            <li><a href="https://userfees.fda.gov/OA_HTML/furls.jsp" target="_blank">Payment Site</a>: Annual Registration Payment (has to be done before registering), Listing #s (htschmidt, Sl$1234$) </li>
            <li><a href="https://www.access.fda.gov/oaa/" target="_blank">Registration & Device Listing</a>: Annual Registration, Listing #s (fac40020, FM$hello101) </li>
            <li><a href="http://www.fda.gov/MedicalDevices/DeviceRegulationandGuidance/PostmarketRequirements/QualitySystemsRegulations/MedicalDeviceQualitySystemsManual/default.htm" target="_blank">QSR Manual</a>: Quality System Regulation </li>
            <li><a href="http://www.fda.gov/ICECI/Inspections/InspectionGuides/ucm074883.htm" target="_blank">Guide to Quality System Inspections</a>: Get ready for an inspection or audit! </li>
            <li><a href="http://www.accessdata.fda.gov/scripts/cdrh/cfdocs/cfMAUDE/TextSearch.cfm" target="_blank">FDA's MAUDE System</a>: Find Complaints in FDA's database here! </li>
            <li><a href="http://www.accessdata.fda.gov/scripts/cdrh/cfdocs/cfRL/rl.cfm" target="_blank">Establishment Registration Look-up</a>: Find 510(k)s here! </li>
            <li><a href="http://www.accessdata.fda.gov/scripts/cdrh/cfdocs/cfCFR/CFRSearch.cfm?CFRPart=801" target="_blank">Medical Device Pakage Labeling</a>: 21CFR801 Subpart C--Labeling Requirements for Over-the-Counter Devices </li>
            <li><a href="http://www.usitc.gov/tata/hts/bychapter/index.htm" target="_blank">Harmonized Tariif Schedule</a>: Look up tax & import duties </li>
        </ul>
        <hr />


        <p>FaceMaster Distributors</p>
        <p>&nbsp;</p>
        <ul style="list-style-type: square; text-align:left;">
           <li><a href="http://www.shopnbc.com/" target="_blank">Shop NBC</a>: US TV shipping outlet </li>
           <li><a href="http://www.theshoppingchannel.com/" target="_blank">The Shopping Channel</a>: Canadian TV shopping outlet </li>
           <li><a href="http://www.lef.org/Vitamins-Supplements/Item01054/FaceMaster-Platinum-Facial-Toning-System.html" target="_blank">LEI</a>: LifeExtension Retail </li>
        </ul>
        <hr />

    </div>
</asp:Content>

