<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="Admin_ReturnImporter" Title="HDI Returns Import-to-CER" Codebehind="ReturnImporter.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            //$("#uploadFile").colorbox({ width: "550px", inline: true, href: "#uploadFilePanel", opacity: 0.5 });
            var clientState = '<%= state %>';
            if (clientState == 0) {
                $.colorbox({ width: "50%", inline: true, href: "#uploadFilePanel", opacity: 0.5 });
            }
            if (clientState == 2) {
                $.colorbox({ width: "75%", inline: true, href: "#verifyPanel", opacity: 0.5 });
            }
            if (clientState == 3) {
                $.colorbox({ width: "75%", inline: true, href: "#dupPanel", opacity: 0.5 });
            }
            if (clientState == 6) {
                $.colorbox({ width: "50%", inline: true, href: "#ErrMsgPanel", opacity: 0.5 });
            }
            if (clientState == 7) {
                $.colorbox({ width: "50%", inline: true, href: "#SuccessPanel", opacity: 0.5 });
            }


        });

        function closeOverlay() {
            $.colorbox.close();
            return false;
        }

        function getFileName() {
            var hdnName = document.getElementById('<%= Page.Master.FindControl("MainContent").FindControl("txtUploadFile").ClientID %>').value;
            document.getElementById('<%= Page.Master.FindControl("MainContent").FindControl("hdnField1").ClientID %>').value = hdnName;
            colorboxDialogSubmitClicked('fileupload', 'uploadFilePanel');
            return true;
        }
    </script>

    <asp:HiddenField ID="hdnField1" runat="server" />

    <!-- State = 0; query XL file name -->
    <asp:Panel ID="pnlUpload" runat="server" style="visibility:hidden; height:1px">
        <div id="uploadFilePanel" class="overlaypanel">
            <h2>Get Excel Return File Name</h2>
            <ul class="fl" style="margin: 0;">
                <li>
                    <asp:Label ID="Label3" CssClass="lbl" AssociatedControlID="txtUploadFile" runat="server" Text="Choose File Name" />
                    <asp:FileUpload runat="server" ID="txtUploadFile" Width="400" size="50" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtUploadFile" ErrorMessage="Required"
                        ValidationGroup="fileUpload" />
                </li>
                <li style="margin: 0;">
                    <asp:Button runat="server" ID="btnUploadFile" Text="Submit"
                        ValidationGroup="fileUpload" CssClass="btn primary" OnClientClick="return getFileName();" OnClick="BtnUploadFileClick"/>
                    or <a href="#" onclick="return closeOverlay();">Cancel</a>
                    <br /><br />
                    <asp:Label runat="server" id="lblUploadStatus" text="Upload status: " />
                </li>
            </ul>
        </div>
    </asp:Panel>

    <!-- State = 1; import Return data from file provided in State = 0 -->

    <!-- State = 6; something bad happened -->
    <asp:Panel ID="pnlErrMessage" runat="server" Style="visibility: hidden; height: 1px">
        <div id="ErrMsgPanel" class="overlaypanel">
            <h2>Error In Import/Export Returns Data</h2>
            <asp:Literal ID="lblErrMsg" runat="server" Text= "" />
        </div>
    </asp:Panel>

    <!-- State = 7; something bad happened -->
    <asp:Panel ID="pnlSuccess" runat="server" Style="visibility: hidden; height: 1px">
        <div id="SuccessPanel" class="overlaypanel">
            <h2>Returns Export Completed Successfully</h2>
            <asp:Literal ID="litSuccessMsg" runat="server" Text= "" />
        </div>
    </asp:Panel>

    <!--
    <a href="#" id="uploadFile" class="file">Get XL Spreadsheet to Process</a>
    <br />

    <asp:Label ID="Label1" runat="server" Text="Press To Start Table Copy"></asp:Label>

    <asp:Button ID="btnImportXLFile" runat="server" Text="Start" OnClick="btnImportXLFile_Click" />
    <br />
    <asp:Label ID="Label2" runat="server" Visible="false" Text="label not updated yet"></asp:Label>
    -->

    <!-- hidden fields to pass javascript info -->
    <asp:HiddenField ID="hf1" runat="server" />

    <asp:Panel ID="pnlVerify" runat="server" style="visibility:hidden; height:1px">
        <div id="verifyPanel" class="overlaypanel">
            <h2>Verify Returns Data</h2>
            <asp:Label ID="lblVerifyContinue" runat="server" Text="Press To continue"></asp:Label>
            <asp:Button ID="btnChkForDups" runat="server" Text="Continue" OnClientClick="colorboxDialogSubmitClicked('lblVerifyContinue', 'verifyPanel');" OnClick="btnChkForDups_Click" />
            <asp:GridView
                ID="gvwReturnsList"
                CssClass="gridResults"
                runat="server"
                EmptyDataText="No orders with this criteria in the database!"
                AutoGenerateColumns="false"
                Width="100%"
                AllowSorting="false"
                AllowPaging="false"
                PageSize="10"
                PagerSettings-Mode="Numeric"
                AutoGenerateDeleteButton="false"
                AutoGenerateEditButton="false"
                AutoGenerateSelectButton="false"
                EnableViewState="False">
                <HeaderStyle />
                <AlternatingRowStyle BackColor="blue" />
                <PagerStyle HorizontalAlign="Center" ForeColor="Blue" BackColor="White" Font-Bold="True" />
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%" />
                    <asp:BoundField HeaderText="Name" DataField="Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="POP" DataField="Pop" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%" />
                    <asp:BoundField HeaderText="Order#" DataField="OrderId" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%" />
                    <asp:BoundField HeaderText="Ser#" DataField="SerNum" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="Received" DataField="DateDelivered" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:MM-dd-yyyy}" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="RMA Issued" DataField="DateRMAIssued" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:MM-dd-yyyy}" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="Reason" DataField="Reason" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="RVal" DataField="ReasonVal" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%" />
                    <asp:BoundField HeaderText="Reason2" DataField="Reason2" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="RVal2" DataField="Reason2Val" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%" />
                    <asp:BoundField HeaderText="Description" DataField="Description" HeaderStyle-HorizontalAlign="Center" />
                </Columns>
            </asp:GridView>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlDuplicate" runat="server" style="visibility:hidden; height:1px">
        <div id="dupPanel" class="overlaypanel">
            <h2>Duplicate Return Data Has Been Found</h2>
            <asp:GridView
                ID="gvwDupList"
                CssClass="gridResults"
                runat="server"
                EmptyDataText="No orders with this criteria in the database!"
                AutoGenerateColumns="false"
                Width="100%"
                AllowSorting="false"
                AllowPaging="false"
                PageSize="10"
                PagerSettings-Mode="Numeric"
                AutoGenerateDeleteButton="false"
                AutoGenerateEditButton="false"
                AutoGenerateSelectButton="false"
                EnableViewState="False">
                <HeaderStyle />
                <AlternatingRowStyle BackColor="blue" />
                <PagerStyle HorizontalAlign="Center" ForeColor="Blue" BackColor="White" Font-Bold="True" />
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%" />
                    <asp:BoundField HeaderText="Name" DataField="Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="POP" DataField="Pop" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%" />
                    <asp:BoundField HeaderText="Order#" DataField="OrderId" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%" />
                    <asp:BoundField HeaderText="Ser#" DataField="SerNum" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="Received" DataField="DateDelivered" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:MM-dd-yyyy}" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="RMA Issued" DataField="DateRMAIssued" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:MM-dd-yyyy}" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="Reason" DataField="Reason" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="RVal" DataField="ReasonVal" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%" />
                    <asp:BoundField HeaderText="Reason2" DataField="Reason2" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" />
                    <asp:BoundField HeaderText="RVal2" DataField="Reason2Val" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%" />
                    <asp:BoundField HeaderText="Description" DataField="Description" HeaderStyle-HorizontalAlign="Center" />
                </Columns>
            </asp:GridView>
        </div>
    </asp:Panel>


    <script type="text/javascript">
        function enterVal() {
            var hf = document.getElementById('<%= Page.Master.FindControl("MainContent").FindControl("hf1").ClientID %>');
            var val = null;
            var obj = JSON.parse(<%= jsonStr %>);

            promptVal += 'Alert: ' + '<%= alertStr %>' + '\n';

            for (var i in Obj) {
                promptVal += i + ': ' + Obj[i] + '\n';
            }

            promptVal += 'Please Enter A Value: ';
            while ((val == null) || (val == '')) {
                if (val == null) val = prompt(promptVal, 'non-null')
                if (val == '') val = prompt(promptVal, 'non-empty')
            }
            if (hf)
                hf.value = val;
            else
                alert("No Hiddenfield!");

        }
    </script>  

</asp:Content>



