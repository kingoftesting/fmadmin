using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Linq;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;
using AdminCart;
using FM2015.Helpers;

public partial class Admin_ReturnImporter : AdminBasePage //System.Web.UI.Page
{

    #region Private Variables & Public Properties & Constructors

    public enum PurchasePoint
    {
        FaceMaster, SNBC, LEI, HDI, unknown
    }

    private class HDI_Returns
    {
        private int id;
        public int Id { get { return id; } set { id = value; } }

        private string name;
        public string Name { get { return name; } set { name = value; } }

        private PurchasePoint pop;
        public PurchasePoint Pop { get { return pop; } set { pop = value; } }

        private int orderId;
        public int OrderId { get { return orderId; } set { orderId = value; } }

        private Int64 serNum;
        public Int64 SerNum { get { return serNum; } set { serNum = value; } }

        private DateTime dateDelivered;
        public DateTime DateDelivered { get { return dateDelivered; } set { dateDelivered = value; } }

        private DateTime dateRMAIssued;
        public DateTime DateRMAIssued { get { return dateRMAIssued; } set { dateRMAIssued = value; } }

        private string reason;
        public string Reason { get { return reason; } set { reason = value; } }

        private string reasonVal;
        public string ReasonVal { get { return reasonVal; } set { reasonVal = value; } }

        private string reason2;
        public string Reason2 { get { return reason2; } set { reason2 = value; } }

        private string reason2Val;
        public string Reason2Val { get { return reason2Val; } set { reason2Val = value; } }

        private string description;
        public string Description { get { return description; } set { description = value; } }

        public HDI_Returns()
        {
            Id = 0;
            Name = "";
            Pop = PurchasePoint.unknown;
            OrderId = 0;
            SerNum = 0;
            DateDelivered = Convert.ToDateTime("1/1/2000");
            DateRMAIssued = Convert.ToDateTime("1/1/2000");
            Reason = "";
            Reason2 = "";
            Description = "";
        }
    }
    #endregion Private Variables & Public Properties & Constructors

    public int state = 1; //default state
    public string jsonStr = "";
    public string alertStr = "";


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            List<HDI_Returns> importList = new List<HDI_Returns>();
            List<HDI_Returns> exportList = new List<HDI_Returns>();

            if (!Int32.TryParse(Request["s"], out state)) { state = 0; };

            switch (state)
            {
                case 0:
                    //GetXLFile(); 
                    //State = 0; jQuery.colobox takes over
                    break;

                case 1:
                    Session["Returns_importList"] = ImportReturnData(importList); //State = 1
                    Response.Redirect(Request.Path + "?s=2"); //move to State = 2; let user verify the upload
                    break;

                case 2:
                    VerifyReturnData((List<HDI_Returns>)Session["Returns_importList"]); //State = 2
                    gvwReturnsList.DataSource = (List<HDI_Returns>)Session["Returns_importList"];
                    gvwReturnsList.DataBind();
                    //pnlVerify.Visible = true;
                    break;

                case 3:
                    List<HDI_Returns> objList = new List<HDI_Returns>();
                    objList = CheckForDuplicateReturns((List<HDI_Returns>)Session["Returns_importList"]); //State = 3
                    if (objList.Count != 0)
                    {
                        gvwDupList.DataSource = objList;
                        gvwDupList.DataBind();
                    }
                    else Response.Redirect(Request.Path + "?s=4"); //move to State = 4; export data into DB
                    break;

                case 4:
                    ExportReturnData((List<HDI_Returns>)Session["Returns_importList"]); //State = 4
                    StringBuilder sb = new StringBuilder("Returns Export Completed Successfully" + "<br />");
                    Session["Returns_errMsg"] = sb.ToString();
                    Response.Redirect(Request.Path + "?s=7");
                    break;

                case 5:
                    //DisplayExportData(); State = 5
                    break;

                case 6:
                    //DisplayErrorMessage(); State = 6
                    lblErrMsg.Text = Session["Returns_errMsg"].ToString();
                    CleanUp();
                    break;

                case 7:
                    //DisplaySuccessMessage(); State = 7
                    litSuccessMsg.Text = Session["Returns_errMsg"].ToString();
                    CleanUp();
                    break;

                default:
                    break;
            }

                       
        }

    }

    protected void btnImportXLFile_Click(object sender, EventArgs e)
    {
            Response.Redirect(Request.Path + "?s=2");
    }

    protected void btnChkForDups_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.Path + "?s=3");
    }


    private void ExportReturnData(List<HDI_Returns> importList)
    {
        foreach(HDI_Returns obj in importList)
        {
            adminCER.CER cer = new adminCER.CER(); //add a new entry into the CER database
            cer = InitCER(cer, obj);

            if (Config.RedirectMode == "Live") //don't add a new CER entry if in Test mode
            { 
                int cerResult = cer.Add();
                if (cerResult < 0)
                {
                    string msg = "Export Failed! (ExportReturnData: cer.Add)" + Environment.NewLine;
                    msg += "Customer:" + obj.Name + Environment.NewLine;
                    msg += " OrderID:" + obj.OrderId.ToString() + ", " + Environment.NewLine;
                    msg += " DateDelivered:" + obj.DateDelivered.ToString() + ", " + Environment.NewLine;
                    msg += " Reason:" + obj.Reason.ToString() + ", " + Environment.NewLine;
                    Session["Returns_errMsg"] = msg;
                    Response.Redirect(Request.Path + "?s=6");
                    break;
                }
                else
                {
                    cer.CERYear = DateTime.Now.Year.ToString();
                    cer.CERIdentifier = cerResult;
                }
                CER.Returns ret = new CER.Returns();
                ret.Id = 0;
                ret.Date = obj.DateRMAIssued;
                ret.DateReceived = obj.DateDelivered;
                ret.Year = Convert.ToInt32(cer.CERYear);
                ret.CerNum = cer.CERIdentifier;
                ret.ReasonVal = Convert.ToInt32(obj.ReasonVal);
                ret.Reason2Val = Convert.ToInt32(obj.Reason2Val);
                int retResult = ret.Add();
            }
        }
    }


    private adminCER.CER InitCER(adminCER.CER cer, HDI_Returns obj)
    {
        int FMOrderID = ConvertHDIORderIDtoFMOrderID(obj.OrderId);
        if (FMOrderID == 0)
        {
            int line = obj.Id;
            string msg = "InitCER Failed! (FMORderID = 0)" + Environment.NewLine;
            msg += "Customer:" + obj.Name + Environment.NewLine;
            msg += " OrderID:" + obj.OrderId.ToString() + ", " + Environment.NewLine;
            msg += " DateDelivered:" + obj.DateDelivered.ToString() + ", " + Environment.NewLine;
            msg += " Reason:" + obj.Reason.ToString() + ", " + Environment.NewLine;
            Session["Returns_errMsg"] = msg;
            Response.Redirect(Request.Path + "?s=6");
        }
        AdminCart.Cart cart = new AdminCart.Cart();
        cart = cart.GetCartFromOrder(FMOrderID);

        cer.CustomerID = cart.SiteCustomer.CustomerID.ToString();
        cer.DateReportReceived = DateTime.Now;
        cer.PointOfPurchase = "08"; //HDI
        cer.FirstName = cart.SiteCustomer.FirstName;
        cer.LastName = cart.SiteCustomer.LastName;
        cer.Email = cart.SiteCustomer.Email;
        cer.Phone = cart.SiteCustomer.Phone;
        cer.Street = cart.ShipAddress.Street;
        cer.City = cart.ShipAddress.City;
        cer.State = cart.ShipAddress.State;
        cer.Zip = cart.ShipAddress.Zip;
        cer.Country = cart.ShipAddress.Country;
        //cer.ProductNumber = selProductNumber.SelectedValue;
        cer.SerialNumber = obj.SerNum.ToString();
        cer.OrderNumber = FMOrderID.ToString();
        cer.EventDescription = "HDI Returned Product: " + obj.Description;
        cer.ActionExplaination = "HDI Returned Product";
        if ((obj.ReasonVal == "8") || (obj.ReasonVal == "10")) // Complaint; let QA determine next steps
        {
            cer.StatusCs = "Opened";
            cer.StatusQa = "UnOpened";
            cer.StatusMed = "Does Not Apply";
            cer.CERType = "C";
        }
        else
        {
            cer.StatusCs = "Closed";
            cer.StatusQa = "Closed";
            cer.StatusMed = "Does Not Apply";
            cer.CERType = "NC";
        }
        cer.MedicalCondition = "n/a";

        cer.ReceivedBy = "Automated HDI Returns";
        cer.InsertBy = "Automated HDI Returns";
        cer.ComplaintDeterminationBy = "Rodger Mohme";

        return cer;
    }

    private int ConvertHDIORderIDtoFMOrderID(int HDIOrderID)
    {
        int FMOrderID = 0;

        string sql = @" 
            SELECT orderid FROM  orders 
            WHERE SuzanneSomersOrderID = @SSOrderID AND SourceID = -2 
        ";
        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, HDIOrderID);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
        if (dr.Read())
        { FMOrderID = Convert.ToInt32(dr["OrderID"]); }
        if (dr != null) { dr.Close(); }

        return FMOrderID;
    }

    /// <summary>
    /// Handles the Click event of the btnUploadFile control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void BtnUploadFileClick(object sender, EventArgs e)
    {
        //Session["fileName"] = hdnField1.Value;

        //var relativeFolder = DateTime.Now.Year.ToString() + Path.DirectorySeparatorChar + DateTime.Now.Month +
        //             Path.DirectorySeparatorChar;
        //var folder = "~/App_Data/ReturnData/" + relativeFolder + Path.DirectorySeparatorChar;
        bool isOK = false;
        if (txtUploadFile.HasFile)
        {
            var directory = Server.MapPath("~/App_Data/ReturnData/");
            string fileName = String.Format("{1}_{0:yyyy-MM-dd-hh-mm-ss}.xlsx", DateTime.Now, Path.GetFileName(txtUploadFile.FileName));
            Session["fileName"] = fileName;
            string filePath = Path.Combine(directory, fileName); 

            try
            {
                isOK = true;
                txtUploadFile.SaveAs(filePath);
            }
            catch (Exception ex)
            {
                isOK = false;
                lblUploadStatus.Text = "Upload status: The existing file could not be deleted. The following error occured: " + ex.Message;
            }

        }
        else
        { lblUploadStatus.Text = "Upload status: The existing file was not uploaded."; }

        if (isOK)
        {
            Response.Redirect(Request.Path + "?s=1"); //time to try to import the date from the XL file
        }
        else
        {
            Session["Returns_errMsg"] = lblUploadStatus.Text;
            Response.Redirect(Request.Path + "?s=6"); //report the error

        }

    }

    private List<HDI_Returns> ImportReturnData(List<HDI_Returns> importList)
    {
        //State = 1
        //open XL file, read contents

        List<HDI_Returns> objList = new List<HDI_Returns>();


        /*
         * Note that if you're reading xls, you need to use this connection string instead: 
         * string.Format("Provider=Microsoft.ACE.OLEDB.4.0;Data Source={0}; Extended Properties=Excel 8.0;HDR=YES", fileName)

         * Note that if you're reading xlsx, you need to use this connection string instead: 
         * string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Extended Properties=Excel 12.0;HDR=YES", fileName)
        */

        // Connection String to Excel Workbook
        string sExcelFileName = Server.MapPath("~/App_Data/ReturnData/" + Session["fileName"].ToString());
        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Extended Properties=\"Excel 12.0;HDR=YES;\"", sExcelFileName);
        OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
        try
        {
            excelConnection.Open(); // This code will open excel file.
        }
        catch (Exception ex)
        {
            StringBuilder sb = new StringBuilder("Error in opening Excel file: excelconnection.open" + "<br />");
            sb.AppendLine("<br />");
            sb.Append("excelConnectionString: " + excelConnectionString + "<br />");
            sb.AppendLine("<br />");
            sb.Append("exception info: " + ex.Message + "<br />");
            sb.AppendLine("<br />");

            Session["Returns_errMsg"] = sb.ToString();
            Response.Redirect(Request.Path + "?s=6");
        }

        string strSQL = @"
        Select ID, Name, Pop, OrderID, Serial, Delivered, Date_RMA_Issued, Reason, ReasonVal, Reason2, Reason2Val, Description
        FROM [Opens_ForDB$]
        ";

        OleDbCommand dbCommand = new OleDbCommand(strSQL, excelConnection);
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter(dbCommand);

        // create dataset
        DataSet dSet = new DataSet();
        //fill dSet with XL data
        try
        {
            dataAdapter.Fill(dSet);
        }
        catch (Exception ex)
        {
            StringBuilder sb = new StringBuilder("Error in opening Excel file: dataAdapter.Fill" + "<br />");
            sb.AppendLine("<br />");
            sb.Append("excelConnectionString: " + excelConnectionString + "<br />");
            sb.AppendLine("<br />");
            sb.Append("exception info: " + ex.Message + "<br />");
            sb.AppendLine("<br />");

            Session["Returns_errMsg"] = sb.ToString();
            Response.Redirect(Request.Path + "?s=6");
        }

        //step 2
        //connect to server; copy XL data

        DataTable dt = dSet.Tables[0];
        int ColumnCount = dt.Columns.Count;

        foreach (DataRow r in dt.Rows)
        {
            HDI_Returns obj = new HDI_Returns();

            Int32 tempInt = 0; //default OrderId
            if (!Int32.TryParse(r["ID"].ToString(), out tempInt)) { obj.Id = 0; }
            else { obj.Id = tempInt; }

            if (!String.IsNullOrEmpty(r["Name"].ToString())) { obj.Name = r["Name"].ToString(); }
            else { obj.Name = ""; }

            string pop = "";
            if (!String.IsNullOrEmpty(r["Pop"].ToString())) { pop = r["Pop"].ToString(); }
            switch (pop)
            {
                case "FaceMaster":
                    obj.Pop = PurchasePoint.FaceMaster;
                    break;

                case "SNBC":
                    obj.Pop = PurchasePoint.SNBC;
                    break;

                case "LEI":
                    obj.Pop = PurchasePoint.LEI;
                    break;

                case "HDI":
                    obj.Pop = PurchasePoint.HDI;
                    break;

                default:
                    obj.Pop = PurchasePoint.unknown;
                    break;
            }

            tempInt = 0; //default OrderId
            if (!Int32.TryParse(r["OrderID"].ToString(), out tempInt)) { obj.OrderId = 0; }
            else { obj.OrderId = tempInt; }

            Int64 tempIntL = 0; //default Serial number
            if (!Int64.TryParse(r["Serial"].ToString(), out tempIntL)) 
            {
                //try going to the order to get the serial #
                int FMOrderId = ConvertHDIORderIDtoFMOrderID(obj.OrderId);
                string serStr = GetSerialNumber(FMOrderId);
                if (!String.IsNullOrEmpty(serStr))
                { if (!Int64.TryParse(serStr, out tempIntL)) { tempIntL = 0; } }
            }
            obj.SerNum = tempIntL;
            
            DateTime tempDate = Convert.ToDateTime("1/1/2000"); //default Delivered date
            if (!DateTime.TryParse(r["Delivered"].ToString(), out tempDate))
            {
                //if no date entered then try going to the order to get the tracking number
                int FMOrderId = ConvertHDIORderIDtoFMOrderID(obj.OrderId);
                string trackStr = GetTrackingNumber(FMOrderId);
                if (!String.IsNullOrEmpty(trackStr))
                {
                    if (trackStr.Substring(0, 2) == "1Z") //if a UPS tracking number then query UPS for delivery date
                    { 
                        string deliveryDate = GetUPSDeliveryDate(trackStr);
                        if (!string.IsNullOrEmpty(deliveryDate)) { tempDate = Convert.ToDateTime(deliveryDate);  }
                        else
                        {
                            StringBuilder sb = new StringBuilder("Error in acquiring tracking date from UPS" + "<br />");
                            sb.AppendLine("<br />");
                            sb.Append("ID = " + obj.Id.ToString() + "<br />");
                            sb.AppendLine("<br />");
                            Session["Returns_errMsg"] =sb;

                            // dispose used objects
                            dSet.Dispose();
                            dataAdapter.Dispose();
                            dbCommand.Dispose();
                            excelConnection.Close();
                            excelConnection.Dispose();
                            Response.Redirect(Request.Path + "?s=6");

                        }
                    }
                }
            }
            obj.DateDelivered = tempDate;

            tempDate = Convert.ToDateTime("1/1/2000"); //default RMA_Issued date
            if (!DateTime.TryParse(r["Date_RMA_Issued"].ToString(), out tempDate)) { obj.DateRMAIssued = Convert.ToDateTime("1/1/2000"); }
            else { obj.DateRMAIssued = tempDate; }

            if (!String.IsNullOrEmpty(r["Reason"].ToString())) { obj.Reason = r["Reason"].ToString(); }
            else { obj.Reason = ""; }

            if (!String.IsNullOrEmpty(r["ReasonVal"].ToString())) { obj.ReasonVal = r["ReasonVal"].ToString(); }
            else { obj.ReasonVal = ""; }

            if (!String.IsNullOrEmpty(r["Reason2"].ToString())) { obj.Reason2 = r["Reason2"].ToString(); }
            else { obj.Reason2 = ""; }

            if (!String.IsNullOrEmpty(r["Reason2Val"].ToString())) { obj.Reason2Val = r["Reason2Val"].ToString(); }
            else { obj.Reason2Val = ""; }

            if (!String.IsNullOrEmpty(r["Description"].ToString())) { obj.Description = r["Description"].ToString(); }
            else { obj.Description = ""; }

            objList.Add(obj);
        }

        // dispose used objects
        dSet.Dispose();
        dataAdapter.Dispose();
        dbCommand.Dispose();

        excelConnection.Close();
        excelConnection.Dispose();

        return objList;

    }

    private void VerifyReturnData(List<HDI_Returns> importList)
    {
        //state = 2; if it returns to caller then no errors found; otherwise state = 6

        bool errFound = false;
        string errMsg = "";
        foreach (HDI_Returns obj in importList)
        {
            if (obj.Id <= 0)
            {
                errFound = true;
                errMsg += "ID =" + obj.Id.ToString() + ": Error: ID is <= 0" + "<br />";
            }
            if (String.IsNullOrEmpty(obj.Name))
            {
                errFound = true;
                errMsg += "ID =" + obj.Id.ToString() + ": Error: Name is NullOrEmpty" + "<br />";
            }
            if (obj.Pop == PurchasePoint.unknown)
            {
                errFound = true;
                errMsg += "ID =" + obj.Id.ToString() + ": Error: Pop is unknown" + "<br />";
            }
            if (obj.OrderId < 400000)
            {
                errFound = true;
                errMsg += "ID =" + obj.Id.ToString() + ": Error: OrderId < 400000: " + obj.OrderId.ToString() + "<br />";
            }
            if ((obj.SerNum < 10000000000) && (obj.SerNum != 0))
            {
                errFound = true;
                errMsg += "ID =" + obj.Id.ToString() + ": Error: Bad Serial Number: " + obj.SerNum.ToString() + "<br />";
            }

            if (obj.DateRMAIssued < obj.DateDelivered)
            {
                errFound = true;
                errMsg += "ID =" + obj.Id.ToString() + ": Error: Date Issue; RMA < Delivery: " + obj.DateRMAIssued.ToString() + "earlier than " + obj.DateDelivered.ToString() + "<br />";
            }
            if ((obj.DateRMAIssued < Convert.ToDateTime("1/1/2013")) || (obj.DateRMAIssued > DateTime.Today))
            {
                errFound = true;
                errMsg += "ID =" + obj.Id.ToString() + ": Error: RMA date out-of-range: " + obj.DateRMAIssued.ToString() + "<br />";
            }
            if ((obj.DateDelivered < Convert.ToDateTime("1/1/2013")) || (obj.DateDelivered > DateTime.Today))
            {
                errFound = true;
                errMsg += "ID =" + obj.Id.ToString() + ": Error: Delivery date out-of-range: " + obj.DateDelivered.ToString() + "<br />";
            }
            if ((String.IsNullOrEmpty(obj.Reason)) || (String.IsNullOrEmpty(obj.ReasonVal)) || (String.IsNullOrEmpty(obj.Reason2)) || (String.IsNullOrEmpty(obj.Reason2Val)))
            {
                errFound = true;
                errMsg += "ID =" + obj.Id.ToString() + ": Error: One or more Reasons are Null or Empty: " + "<br />";
                errMsg += "Reason: " + obj.Reason + "; ReasonVal: " + obj.ReasonVal + "<br />";
                errMsg += "Reason2: " + obj.Reason2 + "; Reason2Val: " + obj.Reason2Val + "<br />";
            }
            if (String.IsNullOrEmpty(obj.Description))
            {
                errFound = true;
                errMsg += "ID =" + obj.Id.ToString() + ": Error: Description is Null or Empty: " + "<br />";
            }
            if (errFound)
            {
                Session["Returns_errMsg"] = errMsg;
                Response.Redirect(Request.Path + "?s=6");
            }
        }
     }

    private List<HDI_Returns> CheckForDuplicateReturns(List<HDI_Returns> importList)
    {
        List<HDI_Returns> objList = new List<HDI_Returns>();

        foreach (HDI_Returns obj in importList)
        {
            int FMOrderId = ConvertHDIORderIDtoFMOrderID(obj.OrderId);

            string sql = @"
                Select top 1 * 
                From ReturnAnalysis
                Where Exists (Select *  
                              From CER2s 
                              Where OrderNumber = @FMOrderId 
                              AND (ReturnAnalysis.[Year] = CER2s.CERYear) 
                              AND (ReturnAnalysis.CerNum = CER2s.CERIdentifier)  
                             )

            ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, FMOrderId);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.HasRows) { objList.Add(obj); }
            if (dr != null) { dr.Close(); }
        }

        return objList;
    }

    private string GetSerialNumber(int FMOrderID)
    {
        string sql = @"
                        SELECT * 
                        FROM FacemasterScans 
                        WHERE OrderID = @orderid 
                        ORDER By ScanDate DESC
                        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, FMOrderID);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
        string ScanStr = "";

        if (dr.Read()) //just pull the first ser# if there are multiple
        { if (dr["Serial"] != DBNull.Value) { ScanStr = dr["Serial"].ToString(); } }

        if (dr != null) { dr.Close(); }
        return ScanStr;
    }

    private String GetTrackingNumber(int FMOrderID)
    {
        string sql = @"
                        SELECT * 
                        FROM UPSShipments
                        WHERE OrderID = @orderid 
                        ORDER By UpLoadDate ASC
                        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, FMOrderID);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
        String TrackStr = "";

        if (dr.Read()) //just pull the first tracking# if there are multiple
        { if (dr["TrackingNumber"] != DBNull.Value) { TrackStr = dr["TrackingNumber"].ToString(); } }

        if (dr != null) { dr.Close(); }

        return TrackStr;
    }

    private String GetUPSDeliveryDate(string trackStr)
    {
        string deliveryDate = "";

        string xml_req = @"
            <?xml version=""1.0""?>
                <AccessRequest xml:lang=""en-US"">
                    <AccessLicenseNumber>{0}</AccessLicenseNumber>
                    <UserId>{1}</UserId>
                    <Password>{2}</Password>
                </AccessRequest>
            <?xml version=""1.0""?>
                <TrackRequest xml:lang=""en-US"">
                    <Request>
                        <TransactionReference>
                            <CustomerContext>QAST Track</CustomerContext>
                            <XpciVersion>1.0</XpciVersion>
                        </TransactionReference>
                        <RequestAction>Track</RequestAction>
                        <RequestOption>activity</RequestOption>
                    </Request>
                   <TrackingNumber>#{3}</TrackingNumber>
                </TrackRequest>
        ";
        string licence_no = "1CCFD8A3A1A71152";
        string user_ID = "mohme";
        string password = "Kingoftesting1";
        //string track_no = "1Z12345E1512345676"; // (This is test track no)
        //string track_no = "1ZX5070V0342288005"; // (live tracking #)

        xml_req = string.Format(xml_req, licence_no, user_ID, password, trackStr);

        string URI = "https://www.ups.com/ups.app/xml/Track"; // (live access)
        //string URI = "https://wwwcie.ups.com/ups.app/xml/Track"; // (testing access)
        string xmlResult = "";
        using (WebClient wc = new WebClient())
        {

            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            xmlResult = wc.UploadString(URI, xml_req);
        }

        XDocument result = XDocument.Parse(xmlResult);
        XElement responseStatusCode = new XElement("ResponseStatusCode");
        XElement activity = new XElement("Activity");
        XElement code = new XElement("Code");
        XElement date = new XElement("Date");
        XElement time = new XElement("Time");

        try
        {
            responseStatusCode = result.Descendants("TrackResponse")
                .Descendants("Response")
                .Descendants("ResponseStatusCode")
                .First();
        }
        catch (Exception)
        {
            responseStatusCode.Value = "0";
        }
        if (responseStatusCode.Value == "1")
        {
            activity = result.Descendants("TrackResponse")
                .Descendants("Shipment")
                .Descendants("Package")
                .Descendants("Activity")
                .First();
            code = activity.Descendants("Status").Descendants("StatusType").Descendants("Code").First();

            if ((code.Value == "D") && (!activity.Descendants("Date").First().IsEmpty))
            {
                date = activity.Descendants("Date").First();
                time = activity.Descendants("Time").First();
                if (date.Value.Length >= 8)
                {
                    string dyear = date.Value.ToString().Substring(0, 4);
                    string dmonth = date.Value.ToString().Substring(4, 2);
                    string dday = date.Value.ToString().Substring(6, 2);
                    deliveryDate = dmonth + "/" + dday + "/" + dyear;
                }
            }
        }

        return deliveryDate;
    }

    private void CleanUp()
    {
        //delete the uploaded Return file, if necessary, so they don't pile up
        var directory = Server.MapPath("~/App_Data/ReturnData/");

        System.IO.DirectoryInfo dirInfo = new DirectoryInfo(directory);

        foreach (FileInfo file in dirInfo.GetFiles())
        {
            file.Delete();
        }
    }

}
