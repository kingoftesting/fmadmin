<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" Inherits="Admin_ReviewOrder" Title="Review Order" Codebehind="ReviewOrder.aspx.cs" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" Runat="Server"> 
<div class="alpha omega sixteen columns">  
   <div class="sectiontitle">Review Order Details</div>
</div>

<div class="alpha omega sixteen columns"> 
 
	    <div>
	   		<strong>CustomerID:</strong> <asp:Label ID="lblFMCustomerId" Runat="server" />
            <!--
	   		&nbsp &nbsp &nbsp &nbsp
            <asp:HyperLink ID="lnkCustomerDetails" NavigateUrl="EditUser.aspx?CustomerID=" Target="_blank" runat="server">Review Customer Details</asp:HyperLink>
            -->

	   		&nbsp &nbsp &nbsp &nbsp
            <asp:HyperLink ID="lnkCER" NavigateUrl="CEREntry.aspx?FMCustomerID=" Target="_blank" runat="server">NEW CER</asp:HyperLink>
	   		&nbsp &nbsp &nbsp &nbsp
            <asp:HyperLink ID="lnkCEROrderLookup" NavigateUrl="~/CER/CERLookup.aspx?pageaction=find&orderNum=" Target="_blank" runat="server">Lookup CER</asp:HyperLink>
	   		&nbsp &nbsp &nbsp &nbsp
	   		<asp:LinkButton ID="lnkBtnEmail" OnClick="lnkBtnEmail_Click" OnClientClick="aspnetForm.target ='_blank'" runat="server">Email Receipt</asp:LinkButton>
	   		&nbsp &nbsp &nbsp &nbsp
	   		<asp:LinkButton ID="lnkBtnPrint" OnClick="lnkBtnPrint_Click" OnClientClick="aspnetForm.target ='_blank'" runat="server">Print Receipt</asp:LinkButton>
	   	</div>
        <hr />

        <div>
	   	    <table class="custservice" width="100%" border="1px solid gray">
	   	        <tr style="background-color:#DDDDDD;">
	   	            <td class = "custservice" > <strong>Order #</strong> </td>
	   		        <td class = "custservice" > <strong>Order Date</strong> </td>
			        <td class = "custservice" > <strong>Order SourceID</strong> </td>
			        <td class = "custservice" > <strong>CS Order?</strong> </td>
			        <td class = "custservice" > <strong>Company</strong> </td>
			        <td class = "custservice" > <strong>Affiliate Order?</strong> </td>
			        <td class = "custservice" > <strong>SiteSpecial Order?</strong> </td>
                    <td class = "custservice" > <strong>Partner Order #?</strong> </td>
	   	        </tr>
	   	        <tr>
	   	            <td class = "custservice"> <asp:Label ID="lblFMOrderId" Runat="server" /> </td>
	   		        <td class = "custservice"> <asp:Label ID="lblOrderDate" Runat="server" /> </td>
			        <td class = "custservice"> <asp:Label ID="lblSourceId" Runat="server" /> </td>
			        <td class = "custservice"> <asp:Label ID="lblByPhone" Runat="server" /> </td>
			        <td class = "custservice"> <asp:Label ID="lblCompanyID" Runat="server" /> </td>
			        <td class = "custservice"> <asp:Label ID="lblByAffiliate" Runat="server" /> </td>
			        <td class = "custservice"> <asp:Label ID="lblbySiteSpecial" Runat="server" /> </td>
                    <td class = "custservice"> <asp:Label ID="lblHDIOrderId" Runat="server" /> </td>
	   	        </tr>
	   	    </table>
	   	</div>
        <hr />

		<div>
	   		<table>
		        <tr>
			        <td class = "custservice" style="width:30%" valign="top"><strong>Billing Information:</strong><br />
				        <asp:Label id="BillInfo" Visible="true" runat="server" /></td>
			        <td class = "custservice" style="width:30%" valign="top"><strong>Shipping Information:</strong><br />
				        <asp:Label id="ShipInfo" Visible="true" runat="server" /></td>
		        </tr>
		        <tr>
		            <td class = "custservice"colspan="2">
                        <asp:Button ID="btnChgAddr" runat="server" onclick="btnChgAddr_Click" Text="Change Addresses" CausesValidation="false" />
		            </td>
		        </tr>
	   		</table>
		</div>
        <hr />
</div>

    <div class="alpha omega sixteen columns">        				
        <div>
	        <asp:Label ID="Label6" Runat="server" Text="<strong>Order Risk Assessment:</strong>"/>
			<asp:GridView ID="gvwOrderRisk" SkinID="Admin" AutoGenerateColumns="false" Width="90%" runat="server">
                <HeaderStyle  />
                    <RowStyle />
                    <AlternatingRowStyle  />
                    <FooterStyle />
	                <PagerStyle  />
                    <Columns>
                        <asp:BoundField HeaderText="Risk Score" DataField="Score"  />
                        <asp:BoundField HeaderText="Recommendation" DataField="Recommendation"  />
                        <asp:BoundField HeaderText="Message" DataField="Message"  />
                     </Columns>
                    <EmptyDataTemplate><strong>No Risk Assessments Found for this order</strong></EmptyDataTemplate>
            </asp:GridView>
        </div>
            <hr />
    </div>


<div class="alpha omega sixteen columns"> 

        <div>
	        <asp:Label ID="Label10" Runat="server" Text="<strong>Order Description:</strong>"/>
            <div class="simpleClear"></div>
			<asp:Literal ID="cd" runat="server" />
        </div>
        <hr />
</div>

<div class="alpha omega sixteen columns"> 
		<div>
            <asp:Panel ID="pnlTracking" runat="server" DefaultButton="btnTrackNumber">
            <div class="groupHTML">
			    <asp:Label ID="Label4" Runat="server" Text="<strong>BatchID:&nbsp;</strong>" />
                <asp:Label ID="lblBatchID" Runat="server" />
                <div class="simpleClear"></div>			    
			    <asp:Label ID="Label5" Runat="server" Text="<strong>Ship Date:&nbsp;</strong>" />
                <asp:Label ID="lblShipDate" Runat="server" />
                <div class="simpleClear"></div>
		        <strong>Tracking Number(s):&nbsp;</strong>
                <asp:Button ID="btnTrackNumber" onclick="btnTrackNum_Click" runat="server" Text="Add New" TabIndex="3"  />
                <div class="simpleClear"></div>                
            </div>
            <asp:GridView ID="gvwTracking" SkinID="Admin" AutoGenerateColumns="false" OnRowDataBound="gvwTracking_OnRowDataBound" Width="90%" runat="server">
                <HeaderStyle  />
                <RowStyle />
                <AlternatingRowStyle  />
                <FooterStyle />
	            <PagerStyle  />
                <Columns>
                    <asp:BoundField HeaderText="UpLoadDate" DataField="UpLoadDate"  />
                    <asp:TemplateField HeaderText="TrackingNumber" >
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkTrackingNum" NavigateUrl="http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums="  Text='<%# Eval("TrackingNumber") %>' Target="_blank" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Weight (lbs)" DataField="Weight"  />
                    <asp:BoundField HeaderText="Charge" DataField="UPSCharge" DataFormatString="{0:$#,##0.00}" HtmlEncode="False" />
                </Columns>
                <EmptyDataTemplate><strong>No Tracking Numbers found for this order</strong></EmptyDataTemplate>
            </asp:GridView>
            </asp:Panel>
            <hr />

            <asp:Panel ID="pnlSerial" runat="server" DefaultButton="btnSerial">
            <div class="groupHTML">
                <div class="simpleClear"></div>
                <div class="formHTML">
                    <asp:Label ID="Label1" runat="server" Text="<strong>Serial Number:</strong>"></asp:Label>
                    <asp:TextBox ID="txtSerial" Text="" runat="server" TabIndex="1"></asp:TextBox>
                    <asp:Button ID="btnSerial" onclick="btnSerial_Click" OnClientClick="aspnetForm.target ='_self'" runat="server" Text="Add New" TabIndex="2" CausesValidation="False" />
                </div>
                <div class="simpleClear"></div>
                <asp:Label ID="lblSerial" Runat="server" />
            </div>
            <asp:GridView ID="gvwSerialNum" SkinID="Admin" AutoGenerateColumns="false" Width="90%" runat="server">
                <HeaderStyle  />
                <RowStyle />
                <AlternatingRowStyle  />
                <FooterStyle />
	            <PagerStyle  />
                <Columns>
                    <asp:BoundField HeaderText="Scan Date" DataField="ScanDate"  />
                    <asp:BoundField HeaderText="Serial Number" DataField="Serial"  />
                 </Columns>
                <EmptyDataTemplate><strong>No Serial Numbers found for this order</strong></EmptyDataTemplate>
            </asp:GridView>
            </asp:Panel>
            <hr />
			    
            <asp:Panel ID="pnlOrderAction" runat="server" DefaultButton="btnOrderAction">
            <div class="groupHTML">
			    <asp:Label ID="Label2" runat="server" Text="<strong>Current Order Action:&nbsp;</strong>"></asp:Label>
                <asp:Label ID="lblOrderAction" Runat="server" />
			    <div class="simpleClear"></div>
                <asp:Label ID="lblSelOrderAction" runat="server" Text="Select New Action:&nbsp;"></asp:Label>
                <asp:DropDownList ID="ddlOrderAction" OnSelectedIndexChanged="ddlOrderAction_SelectedIndexChanged" OnDataBound="ddlOrderAction_OnDataBound" runat="server"></asp:DropDownList>
                <asp:Label ID="lblReasonOrderAction" runat="server" Text="&nbsp;&nbsp;Reason:&nbsp;"></asp:Label>
                <asp:TextBox ID="txtOrderAction" Text="" runat="server"></asp:TextBox>
                <asp:Label ID="Label3" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
                <asp:Button ID="btnOrderAction" onclick="btnOrderAction_Click" OnClientClick="aspnetForm.target ='_self'" runat="server" Text="Change Order Action" CausesValidation="false" />
                <div class="simpleClear"></div>
                <asp:Label ID="lblOrderTxtBox" Runat="server" />
            </div>
            <asp:GridView ID="gvwOrderAction" SkinID="Admin" AutoGenerateColumns="false" Width="90%" runat="server">
                <HeaderStyle  />
                <RowStyle />
                <AlternatingRowStyle  />
                <FooterStyle />
	            <PagerStyle  />
                <Columns>
                    <asp:BoundField HeaderText="Date" DataField="OrderActionDate"  />
                    <asp:BoundField HeaderText="Action" DataField="Action"  />
                    <asp:BoundField HeaderText="Reason" DataField="OrderActionReason"  />
                    <asp:BoundField HeaderText="By" DataField="OrderActionBy"  />
                    </Columns>
                <EmptyDataTemplate><strong>No Order Actions found for this order</strong></EmptyDataTemplate>
            </asp:GridView>
            </asp:Panel>
		</div>
        <hr />
</div>

<div class="alpha omega sixteen columns">        				
        <div>
	        <asp:Label ID="Label11" Runat="server" Text="<strong>Order History:</strong>"/>
			<asp:GridView ID="gvwOrderHistory" SkinID="Admin" AutoGenerateColumns="false" Width="90%" runat="server">
                <HeaderStyle  />
                    <RowStyle />
                    <AlternatingRowStyle  />
                    <FooterStyle />
	                <PagerStyle  />
                    <Columns>
                        <asp:HyperLinkField HeaderText="Order#" DataTextField="OrderID" DataNavigateUrlFormatString="ReviewOrder.aspx?OrderID={0}" DataNavigateUrlFields="OrderID" />
                        <asp:BoundField HeaderText="OrderDate" DataField="OrderDate"  />
                        <asp:BoundField HeaderText="OrderAmount" DataField="Total" DataFormatString="{0:$#,##0.00}" HtmlEncode="False" />
                     </Columns>
                    <EmptyDataTemplate><strong>No Other Orders found for this order</strong></EmptyDataTemplate>
            </asp:GridView>
        </div>
        <hr />
        				
        <div>
	        <asp:Label ID="Label12" Runat="server" Text="<strong>Transaction History:</strong>"/>
			<asp:GridView ID="gvwTransaction" SkinID="Admin" AutoGenerateColumns="false" Width="90%" runat="server">
                <HeaderStyle  />
                    <RowStyle />
                    <AlternatingRowStyle  />
                    <FooterStyle />
	                <PagerStyle  />
                    <Columns>
                        <asp:BoundField HeaderText="ID" DataField="TransactionID"  />
                        <asp:BoundField HeaderText="Date" DataField="TransactionDate" />
                        <asp:BoundField HeaderText="TransType" DataField="TrxType" />
                        <asp:BoundField HeaderText="CC Number" DataField="CardNo" />
                        <asp:BoundField HeaderText="Amount" DataField="TransactionAmount" DataFormatString="{0:$#,##0.00}" HtmlEncode="False" />
                        <asp:BoundField HeaderText="ResultMsg" DataField="ResultMsg" />
                        <asp:HyperLinkField HeaderText="" Text="Refund" DataNavigateUrlFormatString="ChngTrans.aspx?OrderID={0}&OrigID={1}" DataNavigateUrlFields="OrderID, TransactionID" />
                        <asp:HyperLinkField HeaderText="" Text="Void" DataNavigateUrlFormatString="Voidtrans.aspx?OrderID={0}&OrigID={1}" DataNavigateUrlFields="OrderID, TransactionID" />
                     </Columns>
                    <EmptyDataTemplate><strong>No Transactions found for this order</strong></EmptyDataTemplate>
            </asp:GridView>
            <div class="simpleClear"></div>            
            <asp:Button ID="btnCharge" onclick="btnAddCharge_Click" Visible="true" runat="server" Text="New Charge ($50 Max)" CausesValidation="false" />
        </div>
        <hr />

        <div>
	        <asp:Label ID="Label13" Runat="server" Text="<strong>Order Notes:</strong>"/>
            <asp:GridView ID="gvwOrderNotes" SkinID="Admin" AutoGenerateColumns="false" Width="90%" runat="server">
                <HeaderStyle  />
                    <RowStyle />
                    <AlternatingRowStyle  />
                    <FooterStyle />
	                <PagerStyle  />
                    <Columns>
                        <asp:BoundField HeaderText="Date" DataField="OrderNoteDate"  />
                        <asp:BoundField HeaderText="By" DataField="OrderNoteBy" />
                        <asp:BoundField HeaderText="Note" DataField="OrderNote" />
                     </Columns>
                    <EmptyDataTemplate><strong>No OrderNotes found for this order</strong></EmptyDataTemplate>
            </asp:GridView>
            <div class="simpleClear">&nbsp;</div>
            <asp:Label ID="Label14" Runat="server" Text="<strong>Add Order Notes:</strong>"/>
            <div class="simpleClear">&nbsp;</div>
            <asp:TextBox ID="txtOrderNote" runat="server" TextMode="MultiLine" Width="90%" Rows="5" MaxLength="2000"></asp:TextBox>
            <div class="simpleClear"></div>
            <asp:Button ID="btnAddOrderNote" onclick="btnAddOrderNote_Click" OnClientClick="aspnetForm.target ='_self'" CausesValidation="false" runat="server" Text="Add Order Note" />
        </div>
        <hr />

        <div>         
            <strong>CER Event&nbsp;Description:&nbsp;</strong><br />
            <asp:HyperLink ID="lnkCER1" NavigateUrl="/CER/EditCER.aspx?pageaction=lookup&CERNumber=" Target="_blank" runat="server"></asp:HyperLink>
            <asp:Literal ID="litEventDescription" runat="server" />
        </div>
        <hr />

        <div>
        <strong>CER Messages</strong>
        <asp:GridView ID="gridMessages" SkinID="Admin" runat="server" AutoGenerateColumns="false" Width="100%" AlternatingRowStyle-BackColor="#fff8c6">
        <Columns>
            <asp:BoundField HeaderStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" HeaderText="Dated" DataField="Dated" />    
            <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderText="Message" DataField="Message" /> 
            <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderText="AddBy" DataField="AddBy" /> 
        </Columns>
        <EmptyDataTemplate><strong>There are no messages for this CER</strong></EmptyDataTemplate>        
        </asp:GridView>
    </div>

</div>

       
</asp:Content>