using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using AdminCart;
using FM2015.Helpers;


public partial class Admin_ReviewOrder : AdminBasePage
{


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!this.IsPostBack)
        {
            CanUserProceed(new string[3] { "ADMIN", "CUSTOMERSERVICE", "MARKETING" });

            ddlOrderAction.DataSource = OrderAction.GetOrderActionTypes();
            ddlOrderAction.DataValueField = "OrderActionTypeID";
            ddlOrderAction.DataTextField = "OrderActionType";
            ddlOrderAction.DataBind();            
        }
        if (Request["OrderID"] != null)
        {
            int FMorderID = Convert.ToInt32(Request["OrderID"]);

            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(FMorderID, false);
            Session["CSCart"] = cart;

            string carthtml = @"
                        <table class='custservice' width='90%' colspan = '4' cellpadding='5' rules='all' BorderColor='Black' bordercolor='Black' border='1' style='border-collapse:collapse;'>
                        <tr align='Center' style='background-color:#DDDDDD;'>
                            <td class = 'custservice' ><span>Item</span></td>
                            <td class = 'custservice' ><span>Quantity</span></td>
                            <td class = 'custservice' ><span>Price</span></td>
                            <td class = 'custservice' ><span>Discount</span></td>
                            <td class = 'custservice' ><span>Total</span></td>
                        </tr>";


            foreach (Item i in cart.Items)
            {
                carthtml += @"
                        <tr>
                            <td class = 'custservice' align=left valign='center'><span><img class='oDescImg' width='160px' src=../images/products/" + i.LineItemProduct.CartImage + "> " + "<strong>" + i.LineItemProduct.Sku + ": " + i.LineItemProduct.Name + "</strong><br />" + i.LineItemProduct.ShortDescription + @"</span></td>
                            <td class = 'custservice' align='Right'><span>" + i.Quantity.ToString() + @"</span></td>
                            <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + i.LineItemProduct.Price.ToString("N") + @"</span></td>
                            <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + i.LineItemDiscount.ToString("N") + @"</span></td>
                            <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + i.LineItemTotal.ToString("N") + @"</span></td>
                        </tr>";
            }

            string shipmode = "";
            if (cart.ShipType == 1) shipmode = "Ground";
            if (cart.ShipType == 2) shipmode = "Expedited (2-Day Air)";
            if (cart.ShipType == 3) shipmode = "Express (Guarenteed Overnight)";

            
            carthtml += @"
                    <tr>
                        <td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>SubTotal:</span></td>
                        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.SubTotal.ToString("N") + @"</span></td>
                    </tr>
                    ";

                    if (cart.SiteOrder.TotalCoupon != 0)
                    {
                        carthtml += @"<tr>
                                    <td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>Coupon Discount: (" + cart.SiteCoupon.CouponCode + " - " + cart.SiteCoupon.CouponDescription + @")</span></td>
                                    <td class = 'custservice' nowrap='nowrap' align='Right'><span> (" + cart.SiteOrder.TotalCoupon.ToString("N") + @")</span></td>
                                </tr>";
                    }
                    else if (cart.SiteOrder.Adjust != 0)
                    {
                        carthtml += @"<tr>
                                    <td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>Discount: </span></td>
                                    <td class = 'custservice' nowrap='nowrap' align='Right'><span> (" + cart.SiteOrder.Adjust.ToString("N") + @")</span></td>
                                </tr>";

                    }

            carthtml += @"
                    <tr>
                        <td class = 'custservice' align='Right' colspan='4'><span>Shipping: " + shipmode + @"</span></td>
                        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.TotalShipping.ToString("N") + @"</span></td>
                    </tr><tr>
                        <td class = 'custservice' align='Right' colspan='4'><span>Sales Tax:</span></td>
                        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.TotalTax.ToString("N") + @"</span></td>
                    </tr>";


            carthtml += @"<tr>
                        <td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>Total:</span></td>
                        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.Total.ToString("N") + @"</span></td>
                    </tr>
                    </table>";

            cd.Text = carthtml;

            //lblSSCustomerId.Text = DAL.SqlClient.Customer.GetSSCustomerID(SSorderID).ToString();
            lblFMCustomerId.Text = cart.SiteCustomer.CustomerID.ToString() + " (" + cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + ")";
            lnkCustomerDetails.NavigateUrl += cart.SiteCustomer.CustomerID.ToString();
            lnkCER.NavigateUrl += cart.SiteCustomer.CustomerID.ToString();
            lnkCER.NavigateUrl += "&OrderID=" + cart.OrderID.ToString();
            string sernum = GetSerialNumber(cart.OrderID);
            if (!string.IsNullOrEmpty(sernum)) { lnkCER.NavigateUrl += "&SerialNumber=" + sernum; }
            lnkCEROrderLookup.NavigateUrl += cart.OrderID.ToString();
            


            lblFMOrderId.Text = FMorderID.ToString();
            lblHDIOrderId.Text = cart.SiteOrder.SSOrderID.ToString();

            if (cart.SiteOrder.SourceID != -2) //don't try to send to an HDI customer
            {                
                lnkBtnEmail.Visible = true;
                lnkBtnPrint.Visible = false;
            }

            lblOrderDate.Text = cart.SiteOrder.OrderDate.ToString();

            if (cart.SiteOrder.SourceID == -1) { lblSourceId.Text = cart.SiteOrder.SourceID + " (FM website)"; }
            if (cart.SiteOrder.SourceID == -2) { lblSourceId.Text = cart.SiteOrder.SourceID + " (HDI website)"; }
            if (cart.SiteOrder.SourceID == -3) { lblSourceId.Text = cart.SiteOrder.SourceID + " (Shopify website)"; }
            if (cart.SiteOrder.SourceID < -3) { lblSourceId.Text = cart.SiteOrder.SourceID + " (unknown: ?)"; }
            if (cart.SiteOrder.SourceID == 0) 
            {
                if (cart.SiteOrder.ByPhone) { lblSourceId.Text = cart.SiteOrder.SourceID + " (unknown: possibly Saul)"; }
                else { lblSourceId.Text = cart.SiteOrder.SourceID + " (unknown: dhenson)"; }
            }
            if (cart.SiteOrder.SourceID > 0) 
            {
                Customer c = new Customer(cart.SiteOrder.SourceID);
                lblSourceId.Text = cart.SiteOrder.SourceID.ToString() + " (" + c.FirstName + " " + c.LastName + ")";
            }

            if (cart.SiteOrder.CompanyID == 0) { lblCompanyID.Text = cart.SiteOrder.SourceID + "FaceMaster"; }
            else if (cart.SiteOrder.CompanyID == 1) { lblCompanyID.Text = cart.SiteOrder.SourceID + "HDI"; }
            else if (cart.SiteOrder.CompanyID == 2) { lblCompanyID.Text = cart.SiteOrder.SourceID + "SS.com"; }
            else if (cart.SiteOrder.CompanyID == 1) { lblCompanyID.Text = cart.SiteOrder.SourceID + "HDI"; }
            else { lblCompanyID.Text = cart.SiteOrder.SourceID + "uknown"; }


            if (cart.SiteOrder.ByPhone) { lblByPhone.Text = "YES"; }
            else { lblByPhone.Text = "NO"; }

            if (cart.SiteOrder.AffiliateOrderDetailID > 0)
            {
                //make this into a link to the affiliate order detail report
                Certnet.AffiliateOrderDetail afdtl = new Certnet.AffiliateOrderDetail(cart.SiteOrder.AffiliateOrderDetailID);
                Certnet.Affiliate af = new Certnet.Affiliate(afdtl.AffiliateID);
                lblByAffiliate.Text = cart.SiteOrder.AffiliateOrderDetailID.ToString() + " (" + af.Source + ")";
            }
            else
            {
                lblByAffiliate.Text = cart.SiteOrder.AffiliateOrderDetailID.ToString() + " (none)";
            }

            if (cart.SiteOrder.SiteSettingsID > 0)
            {
                //make this into a link to the affiliate order detail report
                SiteSettings sset = new SiteSettings(cart.SiteOrder.SiteSettingsID);
                lblbySiteSpecial.Text = cart.SiteOrder.SiteSettingsID.ToString() + " (" + sset.SiteSettingsName + ")";
            }
            else
            {
                lblbySiteSpecial.Text = cart.SiteOrder.SiteSettingsID.ToString() + " (none)";
            }

            lblBatchID.Text = Utilities.FMHelpers.GetBatchID(FMorderID);
            if (String.IsNullOrEmpty(lblBatchID.Text)) 
            { 
                lblBatchID.Text = "N/A";
                btnChgAddr.Visible = true;
            }
            else 
            { 
                btnChgAddr.Visible = false;
            }


            lblOrderAction.Text = OrderAction.GetOrderAction(FMorderID);
            lblShipDate.Text = Utilities.FMHelpers.GetShipDate(FMorderID);
            if (String.IsNullOrEmpty(lblShipDate.Text)) { lblShipDate.Text = "N/A"; }

            GetTrackNumHTML(FMorderID); //initialize TrackingNum display

            BillInfo.Text = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName;
            if ((string.IsNullOrEmpty(cart.BillAddress.Street2) || (cart.BillAddress.Street2 == " ")))
            { BillInfo.Text += "<br>" + cart.BillAddress.Street + "<br>" + cart.BillAddress.City + ", " + cart.BillAddress.State + " " + cart.BillAddress.Zip + "<br>" + DBUtil.GetCountry(cart.BillAddress.Country); }
            else { BillInfo.Text += "<br>" + cart.BillAddress.Street + "<br>" + cart.BillAddress.Street2 + "<br>" + cart.BillAddress.City + ", " + cart.BillAddress.State + " " + cart.BillAddress.Zip + "<br>" + DBUtil.GetCountry(cart.BillAddress.Country); }

            BillInfo.Text += "<br>" + cart.SiteCustomer.Email;
            BillInfo.Text += "<br>" + cart.SiteCustomer.Phone;
 
            ShipInfo.Text = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + "<br>" + cart.ShipAddress.Street;
            if ((string.IsNullOrEmpty(cart.ShipAddress.Street2) || (cart.ShipAddress.Street2 == " ")))
            { ShipInfo.Text += "<br>" + cart.ShipAddress.City + ", " + cart.ShipAddress.State + " " + cart.ShipAddress.Zip + "<br>" + DBUtil.GetCountry(cart.ShipAddress.Country); }
            else { ShipInfo.Text += "<br>" + cart.ShipAddress.Street2 + "<br>" + cart.ShipAddress.City + ", " + cart.ShipAddress.State + " " + cart.ShipAddress.Zip + "<br>" + DBUtil.GetCountry(cart.ShipAddress.Country); }
                
            ShipInfo.Text += "<br /> Ship Method:" + shipmode;


            GetSerialHTML(FMorderID); //initializes Serial Number view
            GetOrderActionsHtml(FMorderID); //initializes Order Action view
            GetOtherOrdersHtml(cart.SiteCustomer.CustomerID);
            GetTransactionsHtml(FMorderID);            
            GetOrderNotesHtml(FMorderID);
            GetCERStuff(FMorderID);

            List<ShopifyAgent.ShopOrderRisk> riskList = new List<ShopifyAgent.ShopOrderRisk>();
            long shopOrderId = ShopifyAgent.ShopifyOrder.GetShopifyIDFromName(cart.SiteOrder.SSOrderID.ToString(), cart.SiteOrder.CompanyID);
            if (shopOrderId != 0)
            {
                riskList = ShopifyAgent.ShopOrderRisk.GetOrderRiskList(shopOrderId, cart.SiteOrder.CompanyID);
            }
            decimal riskTotal = 0;
            foreach(ShopifyAgent.ShopOrderRisk risk in riskList)
            {
                riskTotal = riskTotal + (decimal)risk.Score;
            }
            if (riskTotal == 0)
            {
                riskList.Clear();
            }
            gvwOrderRisk.DataSource = riskList;
            gvwOrderRisk.DataBind();

        }
    }

    protected void btnChgAddr_Click(object sender, EventArgs e)
    {
        Cart cart = new Cart();
        cart.Load(Session["CScart"]);
        Response.Redirect("EditOrderAddress.aspx?OrderID=" + cart.SiteOrder.OrderID);
    }

    protected void btnFakeButton_Click(object sender, EventArgs e)
    {
    }


    protected void btnAddOrderNote_Click(object sender, EventArgs e)
    {
        Cart cart = new Cart();
        cart.Load(Session["CScart"]);
        int orderid = cart.SiteOrder.OrderID; //save for redirect back to ReviewOrder

        Certnet.Cart tempcart = new Certnet.Cart();
        tempcart.Load(Session["cart"]); //get name of CS rep requesting address change
        string username = tempcart.SiteCustomer.LastName + "," + tempcart.SiteCustomer.FirstName;
        string ordernote = ValidateUtil.CleanUpString(txtOrderNote.Text);
        OrderNote.Insertordernote(orderid, username, ordernote);

        Response.Redirect("ReviewOrder.aspx?OrderID=" + orderid.ToString());
    }

    protected void btnOrderAction_Click(object sender, EventArgs e)
    {
        if (txtOrderAction.Text != "")
        {
            Cart cart = new Cart();
            cart.Load(Session["CScart"]);
            int orderid = cart.SiteOrder.OrderID;

            Certnet.Cart tempcart = new Certnet.Cart();
            tempcart.Load(Session["cart"]); //get name of CS rep requesting address change
            string username = tempcart.SiteCustomer.LastName + "," + tempcart.SiteCustomer.FirstName;
            string ordernote = ValidateUtil.CleanUpString(txtOrderAction.Text);
            int type = Convert.ToInt32(ddlOrderAction.SelectedValue);
            string reason = ValidateUtil.CleanUpString(txtOrderAction.Text);

            //DAL.SqlClient.Customer.ChangeOrderAction(orderid, type, reason, username);
            OrderAction oa = new OrderAction();
            oa.OrderID = orderid;
            if ((type == 4) || (type == 9)) { oa.OrderActionDate = DateTime.Now.AddYears(-200); } //record the off-temp-hold or return, but don't allow it to be counted in the sales reports
            else { oa.OrderActionDate = DateTime.Now; }
            if (type == 5) // if this is a Cancel, then use the Helpers method to get it done correctly
            { Utilities.FMHelpers.CancelOrder(orderid, username); }
            else 
            {
                oa.OrderActionType = type;
                oa.OrderActionReason = ordernote;
                oa.OrderActionBy = username;
                oa.SaveDB();
                OrderNote.Insertordernote(orderid, username, "Changed Order Action to type = " + OrderAction.TranslateOrderAction(type) + " reason: " + ordernote);
            }
            Response.Redirect("ReviewOrder.aspx?OrderID=" + orderid.ToString());
        }
        else 
        {
            lblOrderTxtBox.Visible = true;
            lblOrderTxtBox.Text = "You Must Provide A Reason For Updating OrderActivity";
        }
    }

    protected void btnSerial_Click(object sender, EventArgs e)
    {
        if (txtSerial.Text != "")
        {
            Cart cart = new Cart();
            cart.Load(Session["CScart"]);
            int orderid = cart.SiteOrder.OrderID;

            Certnet.Cart tempcart = new Certnet.Cart();
            tempcart.Load(Session["cart"]); //get name of CS rep requesting address change
            string username = tempcart.SiteCustomer.LastName + "," + tempcart.SiteCustomer.FirstName;
            string serialnum = ValidateUtil.CleanUpString(txtSerial.Text);
            string ordernote = "Added New FM Serial = " + serialnum;

            //DAL.SqlClient.Customer.ChangeOrderAction(orderid, type, reason, username);

            Utilities.FMHelpers.AddSerialNum(orderid, serialnum, DateTime.Now);

            OrderNote.Insertordernote(orderid, username, ordernote);

            //Response.Redirect("ReviewOrder.aspx?OrderID=" + orderid.ToString());
            Response.Redirect("ManageOrders.aspx");
        }
        else
        {
            lblSerial.Visible = true;
            lblSerial.Text = "You Must Enter a FaceMaster Serial Number";
        }
    }

    protected void btnTrackNum_Click(object sender, EventArgs e)
    {
        Cart cart = new Cart();
        cart.Load(Session["CScart"]);
        Response.Redirect("AddTrackingNum.aspx?OrderID=" + cart.SiteOrder.OrderID.ToString());
   }

    protected void btnAddCharge_Click(object sender, EventArgs e)
   {
       Cart cart = new Cart();
       cart.Load(Session["CScart"]);
       Response.Redirect("AddNewCharge.aspx?OrderID=" + cart.SiteOrder.OrderID.ToString());
   }

   protected void ddlOrderAction_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = ddlOrderAction.SelectedIndex;
    }

    protected void ddlOrderAction_OnDataBound(object sender, EventArgs e)
    {
        ddlOrderAction.SelectedIndex = 0;
    }

    private string GetSerialNumber(int FMOrderID)
    {
        string sql = @"
                        SELECT * 
                        FROM FacemasterScans 
                        WHERE OrderID = @orderid 
                        ORDER By ScanDate DESC
                        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, FMOrderID);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
        string ScanStr = "";

        if (dr.Read()) //just pull the first ser# if there are multiple
        { if (dr["Serial"] != DBNull.Value) { ScanStr = dr["Serial"].ToString(); } }

        if (dr != null) { dr.Close(); }
        return ScanStr;
    }


    private void GetTransactionsHtml(int OrderID)
    {
        string sql = @"
                SELECT * 
                FROM Transactions 
                WHERE OrderID = @OrderID 
                ORDER BY TransactionDate DESC 
            ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID);
        DataSet ds = DBUtil.FillDataSet(sql, "TransActions", mySqlParameters);
        gvwTransaction.DataSource = ds.Tables["TransActions"].DefaultView;
        gvwTransaction.DataBind();
    }

    private void GetOrderActionsHtml(int OrderID)
    {
        string sql = @"
            SELECT oa.OrderActionDate, 
                    [Action] = (Select OrderActionType from OrderActionTypes where OrderActionTypeID = oa.OrderActionType),
                    OrderActionReason,
                    OrderActionBy
            FROM OrderActions oa 
            WHERE oa.OrderID = @OrderID  
            ORDER BY oa.OrderActionDate DESC 
        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID);
        DataSet ds = DBUtil.FillDataSet(sql, "OrderActions", mySqlParameters);
        gvwOrderAction.DataSource = ds.Tables["OrderActions"].DefaultView;
        gvwOrderAction.DataBind(); 
    }

    private void GetTrackNumHTML(int FMOrderID)
    {
        string sql = @"
            SELECT UpLoadDate, TrackingNumber, Weight, UPSCharge 
            FROM UPSShipments 
            WHERE OrderID = @OrderID 
            ORDER BY UpLoadDate DESC 
        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, FMOrderID);
        DataSet ds = DBUtil.FillDataSet(sql, "Tracking", mySqlParameters);
        gvwTracking.DataSource = ds.Tables["Tracking"].DefaultView;
        gvwTracking.DataBind(); 
    }

    public void gvwTracking_OnRowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=1ZXXXXXXXXXXXXXXXX
            //http://wwwapps.ups.com/WebTracking/processInputRequest?sort_by=status&tracknums_displayed=1&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1={0}
            //http://www.fedex.com/Tracking?action=track&tracknumbers=XXXXXXXXXXXXXXXXX
            //https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=XXXXXXXXXXXXXXXXX


            HyperLink lnk = (HyperLink)e.Row.FindControl("lnkTrackingNum");
            if (lnk != null)
            {
                string trkNum = lnk.Text;
                string carrierType = trkNum.Substring(0, 1);
                if (carrierType == "1") //if not "1" then assume USPS   
                { lnk.NavigateUrl += trkNum; }
                else { lnk.NavigateUrl = "https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=" + trkNum; }
            }
        }


    }


    private void GetSerialHTML(int FMOrderID)
    {
        string sql = @"
            SELECT * 
            FROM FacemasterScans 
            WHERE OrderID = @orderid 
            ORDER By ScanDate DESC 
        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, FMOrderID);
        DataSet ds = DBUtil.FillDataSet(sql, "SerialNum", mySqlParameters);
        gvwSerialNum.DataSource = ds.Tables["SerialNum"].DefaultView;
        gvwSerialNum.DataBind(); 

    }

   private void GetOrderNotesHtml(int OrderID)
    {
        string sql = @"
            SELECT * FROM OrderNotes 
            WHERE OrderID = @OrderID 
            ORDER BY OrderNoteDate DESC 
        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID);
        DataSet ds = DBUtil.FillDataSet(sql, "OrderNotes", mySqlParameters);
        gvwOrderNotes.DataSource = ds.Tables["OrderNotes"].DefaultView;
        gvwOrderNotes.DataBind();
    }

    private void GetOtherOrdersHtml(int CustomerID)
    {
        string sql = @"
                SELECT * 
                FROM Orders 
                WHERE CustomerID = @CustomerID 
                ORDER BY OrderDate DESC 
            ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, CustomerID);
        DataSet ds = DBUtil.FillDataSet(sql, "OrderHistory", mySqlParameters);
        gvwOrderHistory.DataSource = ds.Tables["OrderHistory"].DefaultView;
        gvwOrderHistory.DataBind();        
    }

    private void GetCERStuff(int FMorderID)
    {
        List<adminCER.CER> cerList = new List<adminCER.CER>();
        cerList = adminCER.CER.FindCERList("","","","","","",FMorderID.ToString());

        if (cerList.Count != 0)
        {
            adminCER.CER cer = new adminCER.CER();
            cer = cerList[0];
            lnkCER1.NavigateUrl += cer.CERYear + cer.CERIdentifier.ToString();
            lnkCER1.Text = cer.CERYear + "-" + cer.CERIdentifier.ToString();
            string c = (cer.CERType == "C") ? "Complaint" : "Non-Complaint";
            litEventDescription.Text = " (" + c + " ): " + cer.EventDescription;

            string sql = @"
            SELECT MessageId, CERYear, CERIdentifier, Dated, Type, Message, AddDate, AddBy
            FROM MessagesCER
            WHERE CERYear = '" + cer.CERYear + @"' 
            AND CERIdentifier = '" + cer.CERIdentifier + @"' 
            ORDER BY Dated DESC";

            gridMessages.DataSource = DBUtil.FillDataReader(sql, Config.ConnStrCER());
            gridMessages.DataBind();
        }
        else
        { litEventDescription.Text = "<br /> No CER entry for this order #"; }
    }

    protected void lnkBtnPrint_Click(object sender, EventArgs e)
    {
        Response.Redirect("PrintReceipt.aspx");
    }

    protected void lnkBtnEmail_Click(object sender, EventArgs e)
    {
        Response.Redirect("EmailReceipt.aspx");
    }
  
}
