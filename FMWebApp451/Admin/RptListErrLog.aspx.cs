using System;
using System.Security;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Certnet;
using FM2015.Helpers;

public partial class Admin_RptListErrLog : AdminBasePage //System.Web.UI.Page
{
    Cart cart;

    void Page_Init(Object sender, EventArgs e)
    {
        cart = new Cart();
        cart.Load(Session["cart"]);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int customerID = cart.SiteCustomer.CustomerID;
        try
        {
            if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "REPORTS")))
            { }
            else
            {
                throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
            }
        }
        catch (SecurityException ex)
        {
            string msg = "Error in RptListErrLog" + Environment.NewLine;
            msg += "User did not have permission to access page" + Environment.NewLine;
            msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            Response.Redirect("Login.aspx");
        }

        SpaceUsed sp = SpaceUsed.GetSpaceUsedByTable("ExceptionLogItems");
        lblRows.Text = sp.Rows;
        lblSize.Text = sp.Reserved;

        myDBErrLog.ConnectionString = AdminCart.Config.ConnStr();
        dtlDBErrLog.ConnectionString = AdminCart.Config.ConnStr();
        myDBErrLog.DataBind();
        dtlDBErrLog.DataBind();
    }

    public void gridPages_PageIndexChanged(object source, EventArgs e)
    {
    }

    public void gridPages_PageIndexChanging(object source, GridViewPageEventArgs e)
    {
        int i = grdRptErrLog.PageIndex;
        grdRptErrLog.PageIndex = e.NewPageIndex;
    }

    protected void lnkbtnTruncate_OnClick(object sender, EventArgs e)
    {
        string sql = @"
            Truncate Table ExceptionLogItems
            ";

        SqlDataReader dr = DBUtil.FillDataReader(sql);
        if (dr != null) { dr.Close(); }
        Response.Redirect("RptListErrLog.aspx");
    }

}
