<%@ Page Language="C#" MasterPageFile="~/Admin/MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="FaceMaster.Admin.Admin_RptRoles" Title="Report: Roles" Codebehind="RptRoles.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

   <div class="sectiontitle">Admin Role Assignments</div>
   <p></p>

   <div class="sectionsubtitle">
   <asp:Button ID="btnNewUser" runat="server" Text="Add New User" OnClick="btnNewUser_Click" />
   <br /><br />
   <p></p>
    </div>

   <asp:GridView 
    ID="gvwUsers"
    SkinID="Admin"
    runat="server"
    AllowSorting="true"
    AutoGenerateColumns="false"
    OnSorting="gvwUsers_Sorting">
	<HeaderStyle BackColor="blue" ForeColor="White" Font-Bold="True" HorizontalAlign="Center" Font-Size="12px" />
    <AlternatingRowStyle BackColor="blue" />
	<PagerStyle HorizontalAlign="Center" ForeColor="Blue" BackColor="White" Font-Bold="True" />
      <Columns>
         <asp:HyperLinkField Text="Modify Roles" DataNavigateUrlFormatString="EditUser.aspx?CustomerID={0}" DataNavigateUrlFields="CustomerID" />
         <asp:BoundField HeaderText="FMCustomerID" DataField="CustomerID" SortExpression="CustomerID" />
         <asp:BoundField HeaderText="LastName" DataField="LastName" SortExpression="LastName" />
         <asp:BoundField HeaderText="FirstName" DataField="FirstName" SortExpression="FirstName" />
         <asp:BoundField HeaderText="Roles" DataField="Role"  />
      </Columns>
      <EmptyDataTemplate><b>No users found for the specified criteria</b></EmptyDataTemplate>
   </asp:GridView>

</asp:Content>

