using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using AdminCart;

namespace FaceMaster.Admin
{

    public partial class Admin_RptRoles : AdminBasePage //System.Web.UI.Page
    {

        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "DESC"; }
            set { ViewState["SortDirection"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Certnet.Cart tempcart = new Certnet.Cart(); //just need the current user ID
            tempcart.Load(Session["cart"]);
            int customerID = tempcart.SiteCustomer.CustomerID;

            if (!this.IsPostBack)
            {
                try
                {
                    if (ValidateUtil.IsInRole(customerID, "ADMIN"))
                    { }
                    else { throw new SecurityException("You do not have the apppropriate creditentials to complete this task."); }
                }
                catch (SecurityException ex)
                {
                    string msg = "Error in RptRoles" + Environment.NewLine;
                    msg += "User did not have permission to access page" + Environment.NewLine;
                    msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                    FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                    Response.Redirect("Login.aspx");
                }

                DataView users = FM2015.Models.Roles.GetFromRoles();
                Session["grvDataSource"] = users; //remember for sorting later
                gvwUsers.DataSource = users;
                gvwUsers.DataBind();
            }
        }

        protected void btnNewUser_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManageUsers.aspx");
        }

        protected void gvwUsers_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataView dv = Session["grvDataSource"] as DataView;

            if (dv != null)
            {

                string m_SortDirection = GetSortDirection();
                dv.Sort = e.SortExpression + " " + m_SortDirection;

                gvwUsers.DataSource = dv;
                gvwUsers.DataBind();
            }
        }

        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;

                case "DESC":

                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

    }
}
