using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Certnet;

public partial class Admin_SalesCompare : AdminBasePage //System.Web.UI.Page
{
    Cart cart;
    DataView websales = null;

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "DESC"; }
        set { ViewState["SortDirection"] = value; }
    }

    void Page_Init(Object sender, EventArgs e)
    {
        cart = new Cart();
        cart.Load(Session["cart"]);
        websales = new DataView(MakeWebSalesTable());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            int customerID = cart.SiteCustomer.CustomerID;
            try
            {
                if (!ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "REPORTS")))
                {
                    throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
                }
            }
            catch (SecurityException ex)
            {
                string msg = "Error in SalesCompare" + Environment.NewLine;
                msg += "User did not have permission to access page" + Environment.NewLine;
                msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                Response.Redirect("../ErrorPage.aspx");
            }


            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                          23, 59, 59);

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.ToShortDateString();
            RDatePicker1.SelectedDate = time1.Subtract(new TimeSpan(0, 23, 59, 59)).ToShortDateString();

            btnGetCSV.Visible = false; //don't show Email & CSV button until there's data displayed
            btnSendEmail.Visible = false;

        }
    }

    private void BindWebSales()
    {
        FillWebSales();
        websales.Sort = "Revenue" + " " + "DESC";
        gvwDataList.DataSource = websales;
        Session["grvDataSource"] = websales; //save for later; used to Sort, CSV download, Email
        gvwDataList.DataBind();

        if (websales.Table.Rows.Count == 0) //hide Email & CSV buttons if empty table
        {
            btnGetCSV.Visible = false;
            btnSendEmail.Visible = false;
        }
        else
        {
            string FromDate = "";
            if (!string.IsNullOrEmpty(RDatePicker1.SelectedDate)) { FromDate = RDatePicker1.SelectedDate + " 12:00AM"; }
            else { FromDate = "1/1/1900"; }

            string ToDate = "";
            if (!string.IsNullOrEmpty(RDatePicker2.SelectedDate)) { ToDate = RDatePicker2.SelectedDate + " 11:59PM"; }
            else { ToDate = DateTime.Now.ToString(); }
            btnGetCSV.Visible = true;
            btnSendEmail.Visible = true;
        }

    }

    protected void btnListByDate_Click(object sender, EventArgs e)
    {

        lblFeedbackOK.Visible = false;
        lblFeedbackKO.Visible = false;

        if (Page.IsValid)
        {
            string startdate = RDatePicker1.SelectedDate + " 12:00:00AM";
            string enddate = RDatePicker2.SelectedDate + " 11:59:59PM";
            gvwDataList.Attributes.Add("FromDate", startdate);
            gvwDataList.Attributes.Add("ToDate", enddate);

            BindWebSales();
        }
    }


    protected void lnkbtnMOM_Click(object sender, EventArgs e)
    {

        lblFeedbackOK.Visible = false;
        lblFeedbackKO.Visible = false;

        if (Page.IsValid)
        {
            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                          23, 59, 59);

            DateTime time2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1,
                                          0, 0, 0);

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.ToShortDateString();
            RDatePicker1.SelectedDate = time2.ToShortDateString();

            string startdate = RDatePicker1.SelectedDate + " 12:00:00AM";
            string enddate = RDatePicker2.SelectedDate + " 11:59:59PM";
            gvwDataList.Attributes.Add("FromDate", startdate);
            gvwDataList.Attributes.Add("ToDate", enddate);

            BindWebSales();
        }
    }

    protected void lnkbtnYOY_Click(object sender, EventArgs e)
    {

        lblFeedbackOK.Visible = false;
        lblFeedbackKO.Visible = false;

        if (Page.IsValid)
        {
            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                          23, 59, 59);

            DateTime time2 = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.ToShortDateString();
            RDatePicker1.SelectedDate = time2.ToShortDateString();

            string startdate = RDatePicker1.SelectedDate + " 12:00:00AM";
            string enddate = RDatePicker2.SelectedDate + " 11:59:59PM";
            gvwDataList.Attributes.Add("FromDate", startdate);
            gvwDataList.Attributes.Add("ToDate", enddate);

            BindWebSales();
        }
    }

    protected void btnCSV_Click(object sender, EventArgs e)
    {
        //FillWebSales();
        websales = Session["grvDataSource"] as DataView;
        websales.Sort = "Revenue DESC";
        //Helpers.SendCSV(websales);
        Utilities.FMHelpers.SendCSV(websales);
    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {

        //Need to write the code for email delivery!

        //FillWebSales();
        websales = Session["grvDataSource"] as DataView;
        websales.Sort = "Revenue DESC";

        int customerID = cart.SiteCustomer.CustomerID;
        string contactTo = cart.SiteCustomer.Email;
        string contactFrom = AdminCart.Config.MailFrom();

        //SqlDataReader dr = Customer.GetCustomerData(Convert.ToInt32(Session["CustomerID"]));
        String contactRawCC = "";
        //if (dr.Read()) { contactRawCC = dr["SampleEmailCC"].ToString(); }
        string contactName = "";
        string contactSubject = "FM REPORTS: Sales Update";

        string contactBody = "For best viewing, select a equally spaced font (ie, Courier)" + Environment.NewLine + Environment.NewLine;
        contactBody += "Time Period: ";
        contactBody += RDatePicker1.SelectedDate + " 12:00:00 AM";
        contactBody += " to ";
        contactBody += RDatePicker2.SelectedDate + " 11:59:59 PM";
        contactBody += Environment.NewLine + Environment.NewLine;


        contactBody += Utilities.FMHelpers.GetMtlIDTableText(websales);
        contactBody += Environment.NewLine + Environment.NewLine;
        contactBody += "MtlID --> SKU Conversion:" + Environment.NewLine;
        contactBody += "1872    FM-5000         FaceMaster Platinum" + Environment.NewLine;
        contactBody += "1873    FM-5150         Platinum Collagen Enhancing Serum with Peptides" + Environment.NewLine;
        contactBody += "1874    FM-5100         FaceMaster Soothing Conductive Serum" + Environment.NewLine;
        contactBody += "1875    FM-51002PC      FaceMaster Soothing Conductive Serum - 2 Pack" + Environment.NewLine;
        contactBody += "1876    FM-51002199     FaceMaster Soothing Conductive Serum and Foam Cap Combo" + Environment.NewLine;
        contactBody += "1503    FM-2299	        Foam Caps" + Environment.NewLine;
        contactBody += "1504    FM-2299-2PC	    Foam Caps - 2 Pack" + Environment.NewLine;
        contactBody += "1877    FM-5800         FaceMaster Platinum Extended Warranty" + Environment.NewLine;
        contactBody += "1878    FM-5240         FaceMaster Platinum Instructional Kit" + Environment.NewLine;
        contactBody += "1879    FM-5050         FaceMaster Platinum Finger Wands" + Environment.NewLine;
        contactBody += "1880    FM-5060         FaceMaster Platinum Hand Wands" + Environment.NewLine;
        contactBody += Environment.NewLine;
        contactBody += "Legacy Products" + Environment.NewLine;
        contactBody += "1457    FM-2000	        FaceMaster®" + Environment.NewLine;
        contactBody += "1825    FM-2000-3001    FaceMaster PLUS Collagen Enhancing Serum with Peptides" + Environment.NewLine;
        contactBody += "1505    FM-21992299	    Conductive Solution and Foam Cap Combo" + Environment.NewLine;
        contactBody += "1502    FM-2199	        Conductive Solution" + Environment.NewLine;
        contactBody += "1548    FM-21992PC	    Conductive Solution - 2 Pack" + Environment.NewLine;
        contactBody += "1506    FM-2DVD	        Step by Step Instructional DVD" + Environment.NewLine;
        contactBody += "1507    FM-2VT	        Step by Step Instructional VHS Tape" + Environment.NewLine;
        contactBody += "1508    FM-2PROBE	    FaceMaster Replacement Probes" + Environment.NewLine;
        contactBody += "1813    FM-2000-3	    FaceMaster® 3-Pay" + Environment.NewLine;
        contactBody += "1812    FM-3001	        FaceMaster Collagen Enhancing Serum with Peptides" + Environment.NewLine;
        contactBody += "1823    FM-4000	        FaceMaster Extended Warranty" + Environment.NewLine;
        contactBody += Environment.NewLine;
        contactBody += "The above is an automated report sent to you from FaceMaster.com" + Environment.NewLine;

        if (mvc_Mail.SendMail(contactTo, contactFrom, contactName, contactRawCC, contactSubject, contactBody))
        {
            // show a confirmation message, and reset the fields
            lblFeedbackOK.Visible = true;
            lblFeedbackKO.Visible = false;
        }
        else
        {
            //otherwise let the customer know it didn't work
            lblFeedbackOK.Visible = false;
            lblFeedbackKO.Visible = true;
        }

        btnGetCSV.Visible = false;
        btnSendEmail.Visible = false;

    }

    protected void FillWebSales()
    {
        string FromDate = "";
        if (!string.IsNullOrEmpty(RDatePicker1.SelectedDate)) { FromDate = RDatePicker1.SelectedDate + " 12:00:00AM"; }
        else { FromDate = "1/1/1900"; }

        string ToDate = "";
        if (!string.IsNullOrEmpty(RDatePicker2.SelectedDate)) { ToDate = RDatePicker2.SelectedDate + " 11:59:59PM"; }
        else { ToDate = DateTime.Now.ToString(); }


        websales = null;
    }



    public void gvwDataList_PageIndexChanging(object source, GridViewPageEventArgs e)
    {
        gvwDataList.PageIndex = e.NewPageIndex;
        //gvwDataList.DataBind();
        BindWebSales();
    }

    public void gvwDataList_PageIndexChanged(object source, EventArgs e)
    {
    }

    protected void gvwDataList_RowEditing(object sender, EventArgs e)
    {
    }

    private DataTable MakeWebSalesTable()
    {
        // Create a new DataTable titled 'WebSales'
        DataTable WebSalesTable = new DataTable("WebSales");

        // Add three column objects to the table.
        DataColumn idColumn = new DataColumn();
        idColumn.DataType = System.Type.GetType("System.Int32");
        idColumn.ColumnName = "id";
        idColumn.AutoIncrement = true;
        WebSalesTable.Columns.Add(idColumn);

        DataColumn SKUColumn = new DataColumn();
        SKUColumn.DataType = System.Type.GetType("System.String");
        SKUColumn.ColumnName = "SKU";
        SKUColumn.DefaultValue = "";
        WebSalesTable.Columns.Add(SKUColumn);

        DataColumn DescColumn = new DataColumn();
        DescColumn.DataType = System.Type.GetType("System.String");
        DescColumn.ColumnName = "Description";
        DescColumn.DefaultValue = "";
        WebSalesTable.Columns.Add(DescColumn);

        DataColumn OrdersColumn = new DataColumn();
        OrdersColumn.DataType = System.Type.GetType("System.Int32");
        OrdersColumn.ColumnName = "Orders";
        OrdersColumn.DefaultValue = null;
        WebSalesTable.Columns.Add(OrdersColumn);

        DataColumn UnitsColumn = new DataColumn();
        UnitsColumn.DataType = System.Type.GetType("System.Int32");
        UnitsColumn.ColumnName = "Units";
        UnitsColumn.DefaultValue = null;
        WebSalesTable.Columns.Add(UnitsColumn);

        DataColumn RevenueColumn = new DataColumn();
        RevenueColumn.DataType = System.Type.GetType("System.Double");
        RevenueColumn.ColumnName = "Revenue";
        RevenueColumn.DefaultValue = null;
        WebSalesTable.Columns.Add(RevenueColumn);

        DataColumn WebOrdersColumn = new DataColumn();
        WebOrdersColumn.DataType = System.Type.GetType("System.Int32");
        WebOrdersColumn.ColumnName = "WebOrders";
        WebOrdersColumn.DefaultValue = null;
        WebSalesTable.Columns.Add(WebOrdersColumn);

        DataColumn WebUnitsColumn = new DataColumn();
        WebUnitsColumn.DataType = System.Type.GetType("System.Int32");
        WebUnitsColumn.ColumnName = "WebUnits";
        WebUnitsColumn.DefaultValue = null;
        WebSalesTable.Columns.Add(WebUnitsColumn);

        DataColumn WebRevenueColumn = new DataColumn();
        WebRevenueColumn.DataType = System.Type.GetType("System.Double");
        WebRevenueColumn.ColumnName = "WebRevenue";
        WebRevenueColumn.DefaultValue = null;
        WebSalesTable.Columns.Add(WebRevenueColumn);

        DataColumn PhoneOrdersColumn = new DataColumn();
        PhoneOrdersColumn.DataType = System.Type.GetType("System.Int32");
        PhoneOrdersColumn.ColumnName = "PhoneOrders";
        PhoneOrdersColumn.DefaultValue = null;
        WebSalesTable.Columns.Add(PhoneOrdersColumn);

        DataColumn PhoneUnitsColumn = new DataColumn();
        PhoneUnitsColumn.DataType = System.Type.GetType("System.Int32");
        PhoneUnitsColumn.ColumnName = "PhoneUnits";
        PhoneUnitsColumn.DefaultValue = null;
        WebSalesTable.Columns.Add(PhoneUnitsColumn);

        DataColumn PhoneRevenueColumn = new DataColumn();
        PhoneRevenueColumn.DataType = System.Type.GetType("System.Double");
        PhoneRevenueColumn.ColumnName = "PhoneRevenue";
        PhoneRevenueColumn.DefaultValue = null;
        WebSalesTable.Columns.Add(PhoneRevenueColumn);

        // Create an array for DataColumn objects.
        DataColumn[] keys = new DataColumn[1];
        keys[0] = idColumn;
        WebSalesTable.PrimaryKey = keys;

        // Once a table has been created, use the 
        /* NewRow to create a DataRow.
        DataRow row;
        row = WebSalesTable.NewRow();

        // Then add the new row to the collection.
        row["SKU"] = FM-5000;
        row["Units"] = 0;
        row["Revenue"] = 0;
        row["WebUnits"] = 0;
        row["WebRevenue"] = 0;
        row["PhoneUnits"] = 0;
        row["PhoneRevenue"] = 0;
        WebSales.Rows.Add(row);
        */


        // Return the new DataTable.
        return WebSalesTable;
    }

    protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataView dv = Session["grvDataSource"] as DataView;

        if (dv != null)
        {

            string m_SortDirection = GetSortDirection();
            dv.Sort = e.SortExpression + " " + m_SortDirection;

            gvwDataList.DataSource = dv;
            gvwDataList.DataBind();
        }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;

            case "DESC":

                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

}
