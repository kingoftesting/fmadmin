using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FM2015.Helpers;

public partial class Admin_Shipping : AdminBasePage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = @"
select *
from sitesettings
where sitesettingsname = 'FreeShipping'
";
        gridShip.DataSource = DBUtil.FillDataSet(sql, "ShippingSpecials").Tables[0];
        gridShip.DataBind();
    }
}
