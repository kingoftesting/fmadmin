<%@ Page Language="C#" AutoEventWireup="true" Theme="Admin" Title="Show Coupons" Inherits="Admin_ShowCoupons" Codebehind="ShowCoupons.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Coupon use over a date range</title>
    <link href="../App_Themes/Platinum/default.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
    <div class="alpha omega sixteen columns">

    <h1>Order Discounts and Coupon Usage</h1>
    Start Date:&nbsp; <asp:Label ID="lblstartDate" runat="server"></asp:Label>
    <br />
    End Date:&nbsp; <asp:Label ID="lblendtDate" runat="server"></asp:Label>
    <br />
    Total Order Discount & Coupon: <asp:Label ID="lblTotal" runat="server"></asp:Label>
    <br />
    Order Discounts:&nbsp; <asp:Label ID="lblOrderDiscount" runat="server"></asp:Label>
    <br />
    Coupons:&nbsp; <asp:Label ID="lblCouponAmount" runat="server"></asp:Label>
    <br />
   	<asp:Repeater ID="repeateritems" runat="server" EnableViewState="false" OnItemDataBound="repeateritems_ItemDataBound"  >
	
	<HeaderTemplate>
	<table width="1000" cellpadding='5' rules='all' border='1' style='border-width:0px;border-collapse:collapse;' >

	<tr align='Center'>
		<td class="rptHdrRow" style="width:75px" >Order#</td>
		<td class="rptHdrRow" style="width:200px" >Date</td>
		<td class="rptHdrRow" style="width:75px" >CouponCode</td>
		<td class="rptHdrRow" style="width:75px" >Amount</td>
		<td class="rptHdrRow" >Description</td>
	</tr>
	</HeaderTemplate>
	
	<ItemTemplate>
    <tr>
        <td class="rptRow" align='Right'>
            <asp:HyperLink ID="lnkOrderID" Target="_self" runat="server"></asp:HyperLink>
        </td>
        <td class="rptRow" align='Right'><asp:Literal ID="litDate" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="litCouponCode" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="litCouponAmount" runat="server" /></td>
        <td class="rptRow" align='left'><asp:Literal ID="litDescription" runat="server" /></td>
	</tr>
	</ItemTemplate>
	
	<AlternatingItemTemplate>
    <tr>
        <td class="rptAltRow" align='Right'>
            <asp:HyperLink ID="lnkOrderID" Target="_self" runat="server"></asp:HyperLink>
        </td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="litDate" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="litCouponCode" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="litCouponAmount" runat="server" /></td>
        <td class="rptAltRow" align='left'><asp:Literal ID="litDescription" runat="server" /></td>
	</tr>
	</AlternatingItemTemplate>

	
	<FooterTemplate>
	    <tr><td colspan="10" class="custservice"> <hr /> </td></tr>

	</tr>
	</table>
	</FooterTemplate>
    </asp:Repeater>

    
    </div>
    </form>
</body>
</html>
