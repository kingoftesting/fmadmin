using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;

public partial class Admin_ShowCoupons : AdminBasePage //System.Web.UI.Page
{
    public class Summary
    {
        #region Members
        private int couponUsageID;
        private int orderID;
        private int couponCodeID;
        private DateTime dateUsed;
        private string couponCode;
        private string couponDescription;
        private decimal amount;
        #endregion

        #region Properties
        public int CouponUsageID { get { return couponUsageID; } set { couponUsageID = value; } }
        public int OrderID { get { return orderID; } set { orderID = value; } }
        public int CouponCodeID { get { return couponCodeID; } set { couponCodeID = value; } }
        public DateTime DateUsed { get { return dateUsed; } set { dateUsed = value; } }
        public string CouponCode { get { return couponCode; } set { couponCode = value; } }
        public string CouponDescription { get { return couponDescription; } set { couponDescription = value; } }
        public decimal Amount { get { return amount; } set { amount = value; } }
        #endregion

        public Summary()
        {
            CouponUsageID = 0;
            OrderID = 0;
            CouponCodeID = 0;
            DateUsed = DateTime.Parse("1/1/1900");
            CouponCode = "";
            CouponDescription = "";
            Amount = 0;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        string start = "1/1/2000";
        if (Request["start"] != null) start = Request["start"].ToString();
        lblstartDate.Text = start;

        string end = DateTime.Now.ToShortDateString();
        if (Request["end"] != null) end = Request["end"].ToString();
        lblendtDate.Text = end;

        decimal total = 0;
        if (Request["coupon"] != null) total = Convert.ToDecimal(Request["coupon"].ToString());
        lblTotal.Text = total.ToString("C");

        List<CouponUsage> couponUseList = new List<CouponUsage>();
        couponUseList = CouponUsage.GetCouponList(start, end);

        decimal orderDiscount = 0;
        decimal couponAmount = 0;
        List<Summary> couponList = new List<Summary>();
        foreach (CouponUsage c in couponUseList)
        {
            Coupon coupon = new Coupon(c.CouponCodeID);
            Summary s = new Summary();
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(c.OrderID);
            s.Amount = cart.SiteOrder.TotalCoupon;
            couponAmount += cart.SiteOrder.TotalCoupon;
            s.OrderID = c.OrderID;
            s.CouponCodeID = c.CouponCodeID;
            s.DateUsed = c.DateUsed;
            s.CouponCode = coupon.CouponCode;
            s.CouponDescription = coupon.CouponDescription;
            couponList.Add(s);
        }

        orderDiscount = total - couponAmount;
        lblCouponAmount.Text = couponAmount.ToString("C");
        lblOrderDiscount.Text = orderDiscount.ToString("C");

        if (couponList.Count == 0)
        { repeateritems.Visible = false; }
        else
        {
            repeateritems.Visible = true;
            repeateritems.DataSource = couponList;
            repeateritems.DataBind();
        }
    }

    protected void repeateritems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Item it = (Item)e.Item.DataItem;
            //litTotalOrders.Text = cart.TotalProduct.ToString("N");

            Summary r = (Summary)e.Item.DataItem;

            HyperLink OrderID = (HyperLink)e.Item.FindControl("lnkOrderID");
            if (OrderID != null)
            {
                OrderID.NavigateUrl = "ReviewOrder.aspx?OrderID=" + r.OrderID.ToString();
                OrderID.Text = r.OrderID.ToString();
            }

            Literal litDate = (Literal)e.Item.FindControl("litDate");
            if (litDate != null) litDate.Text = Convert.ToDateTime(r.DateUsed).ToString();

            Literal litCouponCode = (Literal)e.Item.FindControl("litCouponCode");
            if (litCouponCode != null) litCouponCode.Text = r.CouponCode;

            Literal litCouponAmount = (Literal)e.Item.FindControl("litCouponAmount");
            if (litCouponAmount != null) litCouponAmount.Text = r.Amount.ToString("C");
            
            Literal litDescription = (Literal)e.Item.FindControl("litDescription");
            if (litDescription != null) litDescription.Text = r.CouponDescription;
        }

        if (e.Item.ItemType == ListItemType.Footer)
        {
        }
    }
}
