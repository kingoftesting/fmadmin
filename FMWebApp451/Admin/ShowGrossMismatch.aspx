<%@ Page Language="C#" AutoEventWireup="true" Theme="Admin" Title="Show GrossMisMatch" Inherits="Admin_ShowGrossMismatch" Codebehind="ShowGrossMismatch.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>FM Admin</title>
    <link href="../App_Themes/Platinum/default.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h1>Order IDs with Gross Revenue Errors At The LineItem Level</h1>
   	<asp:Repeater ID="repeateritems" runat="server" EnableViewState="false" OnItemDataBound="repeateritems_ItemDataBound"  >
	
	<HeaderTemplate>
	<table width="500" cellpadding='5' rules='all' border='1' style='border-width:0px;border-collapse:collapse;' >

	<tr align='Center'>
		<td class="rptHdrRow" style="width:75px" >Order#</td>
		<td class="rptHdrRow" style="width:200px" >Date</td>
		<td class="rptHdrRow" style="width:100px" >Amount</td>
	</tr>
	</HeaderTemplate>
	
	<ItemTemplate>
    <tr>
        <td class="rptRow" align='Right'>
            <asp:HyperLink ID="lnkOrderID" Target="_self" runat="server"></asp:HyperLink>
        </td>
        <td class="rptRow" align='Right'><asp:Literal ID="litDate" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="litAmount" runat="server" /></td>

	</tr>
	</ItemTemplate>
	
	<AlternatingItemTemplate>
    <tr>
        <td class="rptAltRow" align='Right'>
            <asp:HyperLink ID="lnkOrderID" Target="_self" runat="server"></asp:HyperLink>
        </td>
        <td class="rptRow" align='Right'><asp:Literal ID="litDate" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="litAmount" runat="server" /></td>
	</tr>
	</AlternatingItemTemplate>

	
	<FooterTemplate>
	    <tr><td colspan="10" class="custservice"></td></tr>

	</tr>
	</table>
	</FooterTemplate>
    </asp:Repeater>

    
    </div>
    </form>
</body>
</html>
