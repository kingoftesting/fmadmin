using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;

public partial class Admin_ShowGrossMismatch : AdminBasePage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<Order> GrossMismatchList = new List<Order>();
        if (Request["comp"] != null) { GrossMismatchList = (List<Order>)Session["GrossMismatchListComp"]; }
        else { GrossMismatchList = (List<Order>)Session["GrossMismatchList"]; }
        

        repeateritems.DataSource = GrossMismatchList;
        repeateritems.DataBind();
    }

    protected void repeateritems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Item it = (Item)e.Item.DataItem;
            //litTotalOrders.Text = cart.TotalProduct.ToString("N");

            Order o = (Order)e.Item.DataItem;

            HyperLink lnkOrderID = (HyperLink)e.Item.FindControl("lnkOrderID");
            if (lnkOrderID != null)
            {
                lnkOrderID.NavigateUrl = "ReviewOrder.aspx?OrderID=" + o.OrderID.ToString();
                lnkOrderID.Text = o.OrderID.ToString();
            }

            Literal litDate = (Literal)e.Item.FindControl("litDate");
            if (litDate != null) { litDate.Text = o.OrderDate.ToString(); }

            Literal litAmount = (Literal)e.Item.FindControl("litAmount");
            if (litAmount != null) { litAmount.Text = o.Total.ToString("C"); }

        }

        if (e.Item.ItemType == ListItemType.Footer)
        {
        }
    }
}
