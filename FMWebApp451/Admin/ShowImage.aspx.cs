﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class ShowImage : AdminBasePage //System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Int32 pictureid = 5;

        if (Request["id"] != null)
        {
            pictureid = Convert.ToInt32( Request["id"].ToString());
        }

        HttpContext context = HttpContext.Current;

        AppHelper ah = new AppHelper();

        Response.Clear();

        

        ah.ProcessRequest(context, pictureid);
    }
}
