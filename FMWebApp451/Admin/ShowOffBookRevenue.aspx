<%@ Page Language="C#" AutoEventWireup="true" Theme="Admin" Title="Show Off-Book Revenue" Inherits="Admin_ShowOffBookRevenue" Codebehind="ShowOffBookRevenue.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Off-Order Transactions</title>
    <link href="../App_Themes/Platinum/default.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
    <div class="sixteen columns">

    <h1>Revenue transactions via "New Charge" instead of shopping cart</h1>
   	<asp:Repeater ID="repeateritems" runat="server" EnableViewState="false" OnItemDataBound="repeateritems_ItemDataBound"  >
	
	<HeaderTemplate>
	<table width="90%" cellpadding="5" rules="all" border="1" style='border-width:0px;border-collapse:collapse;' >

	<tr align="Center">
		<td class="rptHdrRow" style="width:75px" >Order#</td>
		<td class="rptHdrRow" style="width:75px" >Amount</td>
		<td class="rptHdrRow" style="width:100px" >TransID</td>
		<td class="rptHdrRow" >Notes</td>
	</tr>
	</HeaderTemplate>
	
	<ItemTemplate>
    <tr>
        <td class="rptRow" align='Right'>
            <asp:HyperLink ID="lnkOrderID" Target="_self" runat="server"></asp:HyperLink>
        </td>
        <td class="rptRow" align='Right'><asp:Literal ID="litAmount" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="litTransID" runat="server" /></td>
        <td class="rptRow" align='left'><asp:Literal ID="litNotes" runat="server" /></td>
	</tr>
	</ItemTemplate>
	
	<AlternatingItemTemplate>
    <tr>
        <td class="rptAltRow" align='Right'>
            <asp:HyperLink ID="lnkOrderID" Target="_self" runat="server"></asp:HyperLink>
        </td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="litAmount" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="litTransID" runat="server" /></td>
        <td class="rptAltRow" align='left'><asp:Literal ID="litNotes" runat="server" /></td>
	</tr>
	</AlternatingItemTemplate>

	
	<FooterTemplate>
	    <tr><td colspan="10" class="custservice"> <hr /> </td></tr>

	</tr>
	</table>
	</FooterTemplate>
    </asp:Repeater>

    </div>
    </div>
    </form>
</body>
</html>
