using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;

public partial class Admin_ShowOffBookRevenue : AdminBasePage //System.Web.UI.Page
{
    public class Summary
    {
        #region Members
        private int orderID;
        private decimal amount;
        private string transactionID;
        private string transactionBy;
        private string ordernote;
        #endregion

        #region Properties
        public int OrderID { get { return orderID; } set { orderID = value; } }
        public decimal Amount { get { return amount; } set { amount = value; } }
        public string TransactionID { get { return transactionID; } set { transactionID = value; } }
        public string TransactionBy { get { return transactionBy; } set { transactionBy = value; } }
        public string Ordernote { get { return ordernote; } set { ordernote = value; } }
        #endregion

        public Summary()
        {

            OrderID = 0;
            Amount = 0;
            TransactionID = "";
            TransactionBy = "cart";
            Ordernote = "";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        List<Transaction> obrTransactionList = new List<Transaction>();
        if (Request["comp"] != null) { obrTransactionList = (List<Transaction>)Session["ObrTransactionListComp"]; }
        else { obrTransactionList = (List<Transaction>)Session["ObrTransactionList"]; }

        List<Summary> obrList = new List<Summary>();
        if (obrTransactionList != null)
        {
            foreach (Transaction t in obrTransactionList)
            {
                Summary obr = new Summary();
                obr.OrderID = t.OrderID;
                obr.Amount = t.TransactionAmount;
                obr.TransactionID = t.TransactionID;
                List<OrderNote> onList = new List<OrderNote>();
                onList = OrderNote.GetOrderNoteList(t.OrderID);
                foreach (OrderNote on in onList)
                {
                    if (on.OrderNoteText.Contains(t.TransactionID))
                    {
                        obr.TransactionBy = on.OrderNoteBy;
                        obr.Ordernote += "Date: " + on.OrderNoteDate.ToString() + "<br />";
                        obr.Ordernote += "By: " + on.OrderNoteBy + "<br />";
                        obr.Ordernote += "Note: " + on.OrderNoteText + "<br /><br />";
                    }
                }

                obrList.Add(obr);
            }
        }

        repeateritems.DataSource = obrList;
        repeateritems.DataBind();
    }

    protected void repeateritems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Item it = (Item)e.Item.DataItem;
            //litTotalOrders.Text = cart.TotalProduct.ToString("N");

            Summary r = (Summary)e.Item.DataItem;

            HyperLink OrderID = (HyperLink)e.Item.FindControl("lnkOrderID");
            if (OrderID != null)
            {
                OrderID.NavigateUrl = "ReviewOrder.aspx?OrderID=" + r.OrderID.ToString();
                OrderID.Text = r.OrderID.ToString();
            }

            Literal litAmount = (Literal)e.Item.FindControl("litAmount");
            if (litAmount != null) litAmount.Text = Convert.ToDecimal(r.Amount).ToString("C");

            Literal litTransactionID = (Literal)e.Item.FindControl("litTransID");
            if (litTransactionID != null) litTransactionID.Text = r.TransactionID;

            Literal litOrdernote = (Literal)e.Item.FindControl("litNotes");
            if (litOrdernote != null) litOrdernote.Text = r.Ordernote;
        }

        if (e.Item.ItemType == ListItemType.Footer)
        {
        }
    }
}
