﻿<%@ Page Title="Subscription Products" Language="C#" Theme="Admin" MasterPageFile="~/admin/MasterPage3_Blank.master" AutoEventWireup="true" CodeBehind="PayWhirlSubscriptions.aspx.cs" Inherits="FMWebApp451.Admin.Subscriptions.PayWhirlSubscriptions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Panel ID="pnlPayWhirlSubs" runat="server" Visible="false">
        <div style="width: 800px; margin-left: 100px">
            <script type='text/javascript' src="https://www.paywhirl.com/js/widget.js"></script>
            <asp:Literal ID="litPayWhirlIFrame" runat="server"></asp:Literal>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlDefault" runat="server" Visible="false">
        <div style="width: 800px; margin-left: 100px">
            <p><strong>Subscriptions are currently NOT available. Please check again later.</strong></p>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlFMAdminSubs" runat="server" Visible="false">
        <div style="width: 800px; margin-left: 50px">

            <h2><asp:Label ID="lblProductName" runat="server" Text="Plan:&nbsp;"></asp:Label></h2>
            <h2><asp:Label ID="lblMonthlyPayment" runat="server" Text=""></asp:Label></h2>
            <div class="formHTML">
                <h4><asp:Label Runat="server" text="Promo Code:"></asp:Label></h4>
                <asp:dropdownlist id="ddlPromoCode" width="150px" runat="server"
                     Height="25px"
                    OnSelectedIndexChanged="SelPromoCode_SelectedIndexChanged" AutoPostBack="true"></asp:dropdownlist>
            </div>

            <asp:Panel ID="pnlAccountBalance" runat="server" Visible="false">
                <h4><asp:Label id="lblAccountBalance" Runat="server" text="Amount Due: "></asp:Label></h4>
            </asp:Panel>


            <div class="simpleClear"></div>
            <div class="formHTML">
                <h4>
                    <asp:Label id="Label1" Runat="server" text="Name:"></asp:Label>
                    <asp:Label id="lblName" Runat="server" text="" ></asp:Label>
                </h4>
            </div>

            <hr />
            <p><strong>Shipping Address</strong></p>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="Street:"></asp:Label>
                <asp:Label id="lblStreet"  Runat="server" text=""></asp:Label>
            </div>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="Street2:"></asp:Label>
                <asp:Label id="lblStreet2" Runat="server" text=""></asp:Label>
            </div>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="City:"></asp:Label>
                <asp:Label id="lblCity" Runat="server" text=""></asp:Label>
            </div>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="State/Province:"></asp:Label>
                <asp:Label id="lblState" Runat="server" text=""></asp:Label>
            </div>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="Country:"></asp:Label>
                <asp:Label id="lblCountry" Runat="server" text=""></asp:Label>
            </div>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="Postal Code:"></asp:Label>
                <asp:Label id="lblZip" Runat="server" text=""></asp:Label>
            </div>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="Phone:"></asp:Label>
                <asp:Label id="lblPhone" Runat="server" text=""></asp:Label>
            </div>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="Email:"></asp:Label>
                <asp:Label id="lblEmail" Runat="server" text=""></asp:Label>
            </div>

            <hr />
            <p><strong>Credit Card Info</strong></p>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="Card Name:"></asp:Label>
                <asp:textbox id="txtCardName" runat="server" Width="200px"></asp:textbox>
                <asp:requiredfieldvalidator id="Requiredfield_txtCardName" ControlToValidate="txtCardName" ErrorMessage="* card holder's name is required"
	                Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
            </div>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="Card #:"></asp:Label>
                <asp:textbox id="txtCardNumber" runat="server" Width="200px"></asp:textbox>
                <asp:requiredfieldvalidator id="Requiredfield_txtCardNumber" ControlToValidate="txtCardNumber" ErrorMessage="* card number is required"
	                Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
            </div>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="Expires (MM/YYYY):"></asp:Label>
                <asp:dropdownlist id="ddlExpMonth" width="100px" runat="server" AutoPostBack="true">
                    <asp:ListItem Text="" Selected="True" />
                          <asp:ListItem Text="01-JAN" Value = "1" Selected="False" />
                          <asp:ListItem Text="02-FEB" Value = "2" Selected="False"  />
                          <asp:ListItem Text="03-MAR" Value = "3" Selected="False"  />
                          <asp:ListItem Text="04-APR" Value = "4" Selected="False" />
                          <asp:ListItem Text="05-MAY" Value = "5" Selected="False"  />
                          <asp:ListItem Text="06-JUN" Value = "6" Selected="False"  />
                          <asp:ListItem Text="07-JUL" Value = "7" Selected="False"  />
                          <asp:ListItem Text="08-AUG" Value = "8" Selected="False"  />
                          <asp:ListItem Text="09-SEP" Value = "9" Selected="False"  />
                          <asp:ListItem Text="10-OCT" Value = "10" Selected="False"  />
                          <asp:ListItem Text="11-NOV" Value = "11" Selected="False"  />
                          <asp:ListItem Text="12-DEC" Value = "12" Selected="False"  />
                </asp:dropdownlist>
                <asp:dropdownlist id="ddlExpYear" width="100px" runat="server" AutoPostBack="true"></asp:dropdownlist>
            </div>

            <div class="simpleClear"></div>
            <div class="formHTML">
                <asp:Label Runat="server" text="CSV:"></asp:Label>
                <asp:textbox id="txtCSV" runat="server" Width="40px"></asp:textbox>
                <asp:requiredfieldvalidator id="Requiredfield_txtCSV" ControlToValidate="txtCSV" ErrorMessage="* card CSV is required"
	                Display="Dynamic" Runat="server"></asp:requiredfieldvalidator>
            </div>

            <div class="simpleClear"></div>
            <p>&nbsp;</p>
   
            <hr />
            <h4><asp:Label ID="CartLabel" runat="server" Text="Enter your information, then click 'Enter Transaction' only once."></asp:Label></h4>

            <asp:Button ID="btnSubscribe" runat="server" Text="Complete Transaction" OnClick="btnSubscribe_Click" /> 

        </div>
    </asp:Panel>

    <asp:Panel ID="pnlSubThankyou" runat="server" Visible="false">
        <table width="900px">
	        <tr>
		        <td class = "custservice" colspan="2">			
			        <h2>Order Successful</h2>
                </td>
	        </tr>
	
	        <tr>
		        <td class = "custservice" colspan="2">
		            <br />
			            Below is a copy of your Subscription Order to print for your records:<br />
			        <br />
			        <b>FM Order Number:</b> <asp:Label ID="lblOrderId" Runat="server" />
                    <br />
			        <b>Order Date:</b> <asp:Label ID="lblOrderDate" Runat="server" />
                    <br />
			        <b>Confirmation Code:</b> <asp:Label ID="lblTranID" Runat="server" />
			        <br /><br />			
		        </td>
		    </tr>										
	    </table>
	
    </asp:Panel>

    <asp:Panel ID="pnlSubRetry" runat="server" Visible="false">
        <table width="900px">
	        <tr>
		        <td class = "custservice" colspan="2">			
			        <h2>***ORDER NOT SUCCESSUL*** Please Retry</h2>
                </td>
	        </tr>
            <tr>
                <td>
                    <h4><asp:Label ID="lblRetry" runat="server" Text="Processing Error: Unknown"></asp:Label></h4>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnRetry" runat="server" Text="Retry" OnClick="btnRetry_Click" /> 
                </td>
            </tr>									
	    </table>
	
    </asp:Panel>

    <asp:Panel ID="pnlNotUniqueEmail" runat="server" Visible="false">
        <table width="900px">
	        <tr>
		        <td class = "custservice" colspan="2">			
			        <h2>***Customer's Email Address is not Unique*** Please Update the Email Address and Try Again</h2>
                </td>
	        </tr>								
	    </table>
	
    </asp:Panel>

        <asp:Panel ID="pnlAmbiguousOrder" runat="server" Visible="false">
        <table width="900px">
	        <tr>
		        <td class = "custservice" colspan="2">			
			        <h2>***ORDER POSSIBLY NOT SUCCESSUL*** Please Check Customer Subscriptions & Orders</h2>
                </td>
	        </tr>
            <tr>
                <td>
                    <h4><asp:Label ID="lblAmbi" runat="server" Text="Processing Error: Unknown"></asp:Label></h4>
                </td>
            </tr>							
	    </table>
	
    </asp:Panel>


</asp:Content>
