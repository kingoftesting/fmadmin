﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminCart;
using FM2015.Models;
using FMWebApp451.Respositories;
using Stripe;

namespace FMWebApp451.Admin.Subscriptions
{
    public partial class PayWhirlSubscriptions : AdminBasePage //System.Web.UI.Page
    {
        Cart cart = new Cart();
        int fmFormID = 0;
        int widgetID = 0;
        int formID = 0;
        decimal setupFee = 0;
        int productID = 0;
        bool useFMAdmin = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["ddlSelectedPromoCode"] = string.Empty;
            bool requestIsSubscribe = true; //assume that this is going to be a subscription setup
            if (!Page.IsPostBack)
            {
                cart.Load(Session["CScart"]);
                if (cart.Tran.ResultMsg != "")
                {
                    CartLabel.Style.Add("color", "red");
                    CartLabel.Text = cart.Tran.ResultMsg;
                }

                if(Request["PageAction"] != null)
                {
                    if(Request["PageAction"].ToString() == "thankyou")
                    {
                        pnlSubThankyou.Visible = true;
                        lblOrderId.Text = (HttpContext.Current.Session["SubOrderNumber"] == null) ? "n/a" : HttpContext.Current.Session["SubOrderNumber"].ToString();
                        lblOrderDate.Text = (HttpContext.Current.Session["SubOrderDate"] == null) ? DateTime.Now.ToString() : Convert.ToDateTime(HttpContext.Current.Session["SubOrderDate"]).ToString();
                        lblTranID.Text = (HttpContext.Current.Session["SubOrderConfirm"] == null) ? "n/a" : HttpContext.Current.Session["SubOrderConfirm"].ToString();
                        System.Threading.Thread.Sleep(1100);
                        List<int> list = ShopifyAgent.ShopifyAgent.SyncShopify(); //sync orders from Shopify
                    }
                    if (Request["PageAction"].ToString() == "notUniqueEmail")
                    {
                        pnlNotUniqueEmail.Visible = true;
                    }
                    if (Request["PageAction"].ToString() == "retry")
                    {
                        pnlSubRetry.Visible = true;
                        if (Session["subErrorMessage"] != null)
                        {
                            lblRetry.Text = Session["subErrorMessage"].ToString();
                        }
                    }
                    if (Request["PageAction"].ToString() == "ambi")
                    {
                        pnlAmbiguousOrder.Visible = true;
                        if (Session["subErrorMessage"] != null)
                        {
                            lblAmbi.Text = Session["subErrorMessage"].ToString();
                        }
                    }
                    if (Request["PageAction"].ToString() == "charge")
                    {
                        requestIsSubscribe = false;
                        Session["RequestIsSubscribe"] = requestIsSubscribe;
                        useFMAdmin = (ConfigurationManager.AppSettings["UseFMAdminSubscription"].ToString().ToUpper() == "YES") ? true : false;
                        int CustomerID = Convert.ToInt32(Session["mngSubCustomerID"]);
                        Customer c = new Customer(CustomerID);
                        Address billAdr = Address.LoadAddress(CustomerID, 1);
                        Address shipAdr = Address.LoadAddress(CustomerID, 2);
                        cart.Clear(); //no items to be processed, just a charge amount
                        cart.SiteCustomer = c;
                        cart.BillAddress = billAdr;
                        cart.ShipAddress = shipAdr;
                        
                        string stripeCustomerID = (Request["stripeCustomerID"] == null) ? "n/a" : Request["stripeCustomerID"].ToString();
                        string stripeSubscriptionID = (Request["stripeSubscriptionID"] == null) ? "n/a" : Request["stripeSubscriptionID"].ToString();

                        var subscriptionService = new StripeSubscriptionService();
                        StripeSubscription stripeSubscription = new StripeSubscription();
                        try
                        {
                            stripeSubscription = subscriptionService.Get(stripeSubscriptionID);
                        }
                        catch (StripeException ex)
                        {
                            HttpContext.Current.Session["subErrorMessage"] = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Charge: Get StripeSubscription: Stripe Error");
                            Response.Redirect("PayWhirlSubscriptions.aspx?PageAction=retry");
                        }

                        lblProductName.Text = lblProductName.Text + stripeSubscription.StripePlan.Product.Name;
                        lblMonthlyPayment.Text = lblMonthlyPayment.Text + stripeSubscription.StripePlan.Amount;
                        fmFormID = -1;
                        Session["fmFormID"] = fmFormID;

                        //get the customer from Stripe
                        var customerService = new StripeCustomerService();
                        StripeCustomer stripeCustomer = new StripeCustomer();

                        try
                        {
                            stripeCustomer = customerService.Get(stripeCustomerID);
                        }
                        catch (StripeException ex)
                        {
                            HttpContext.Current.Session["subErrorMessage"] = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Charge: Get StripeCustomer: Stripe Error");
                            Response.Redirect("PayWhirlSubscriptions.aspx?PageAction=retry");
                        }
                        var invoiceService = new StripeInvoiceService();
                        var invoiceServiceOptions = new StripeInvoiceListOptions();
                        StripeInvoice stripeInvoice = new StripeInvoice();
                        IEnumerable<StripeInvoice> stripeInvoiceList = new List<StripeInvoice>();
                        try
                        {
                            invoiceServiceOptions.CustomerId = stripeCustomerID;
                            stripeInvoiceList = invoiceService.List(invoiceServiceOptions);
                        }
                        catch (StripeException ex)
                        {
                            HttpContext.Current.Session["subErrorMessage"] = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Charge: Get Invoce: Stripe Error");
                            Response.Redirect("PayWhirlSubscriptions.aspx?PageAction=retry");
                        }
                        stripeInvoice = stripeInvoiceList.OrderByDescending(x => x.Date).ToList().First();

                        cart.Total = Convert.ToDecimal(stripeInvoice.AmountDue) / 100;
                        lblAccountBalance.Text = lblAccountBalance.Text + cart.Total.ToString("C");
                        pnlAccountBalance.Visible = true;
                        Session["stripeCustomerID"] = stripeCustomer.Id;
                        Session["stripeInvoiceID"] = stripeInvoice.Id;
                        Session["CScart"] = cart;
                        this.UpdatePage();
                    } //end PageAction Charge
                } //end PageAction
                else //must be a Subscription
                {
                    Session["RequestIsSubscribe"] = requestIsSubscribe;
                    cart.Load(Session["CScart"]);
                    int customerID = cart.SiteCustomer.CustomerID;
                    bool useFMAdmin = (ConfigurationManager.AppSettings["UseFMAdminSubscription"].ToString().ToUpper() == "YES") ? true : false;

                    if (requestIsSubscribe)
                    {
                        if (!Customer.IsEmailUnique(cart.SiteCustomer.Email))
                        {
                            Response.Redirect("PayWhirlSubscriptions.aspx?PageAction=notUniqueEmail");
                        }

                        bool ok = Int32.TryParse(Request["widgetID"], out widgetID);
                        Session["widgetID"] = widgetID;

                        ok = Int32.TryParse(Request["formID"], out formID);
                        Session["formID"] = formID;

                        ok = Int32.TryParse(Request["fmFormID"], out fmFormID);
                        Session["fmFormID"] = fmFormID;

                        ok = Decimal.TryParse(Request["setupFee"], out setupFee);
                        Session["setupFee"] = setupFee;

                        ok = Int32.TryParse(Request["productID"], out productID);

                        string ddlSelectedPromoCode = Session["ddlSelectedPromoCode"].ToString();
                        List<StripeCoupon> promoCodeList = GetCouponCodeList();
                        StripeCoupon stripeCoupon = (from c in promoCodeList where c.Id == ddlSelectedPromoCode select c).FirstOrDefault();
                        decimal monthlyPayment = 0;

                        SubscriptionPlanProvider subscriptionPlanProvider = new Respositories.SubscriptionPlanProvider();
                        if (ok)
                        {
                            if (cart.Items.ItemCount() > 0)
                            {
                                cart.Items = new CartItems();
                            }
                            Item item = new Item(productID, 1);
                            cart.Items.AddItem(item);
                            Session["CScart"] = cart;
                            lblProductName.Text = lblProductName.Text + item.LineItemProduct.SubscriptionPlan;
                            Session["SubscriptionPlan"] = item.LineItemProduct.SubscriptionPlan;
                            SubscriptionPlans subPlan = subscriptionPlanProvider.GetByPlanId(item.LineItemProduct.SubscriptionPlan);
                            monthlyPayment = AdjustPayment(subPlan.Amount, stripeCoupon);
                            lblMonthlyPayment.Text = "Payment Amount: " + monthlyPayment.ToString("C");
                        }
                        else //check if this might be a retry
                        {
                            if ((cart.Items.ItemCount() == 0) || (cart.Items.ItemCount() > 1))
                            {
                                fmFormID = 0; //not a retry, make sure we show the default page
                                cart.Items = new CartItems();
                            }
                            else
                            {
                                if (cart.Items.ItemCount() > 0)
                                {
                                    cart.Items = new CartItems();
                                }
                                foreach (Item i in cart.Items) //if retry get the subscription name to display
                                {
                                    lblProductName.Text = lblProductName.Text + i.LineItemProduct.SubscriptionPlan;
                                    Session["SubscriptionPlan"] = i.LineItemProduct.SubscriptionPlan;
                                    SubscriptionPlans subPlan = subscriptionPlanProvider.GetByPlanId(i.LineItemProduct.SubscriptionPlan);
                                    monthlyPayment = AdjustPayment(subPlan.Amount, stripeCoupon);
                                    lblMonthlyPayment.Text = "Payment Amount: " + monthlyPayment.ToString();
                                }
                            }
                        }
                        this.UpdatePage();
                    } //end else (if requestIsSubscribe)
                } //end Else (PageAction)
            } //end Page.IsPostBack()
        } //end Page.Load

        private void UpdatePage()
        {
            if (useFMAdmin)
            {
                if (fmFormID != 0)
                {
                    pnlFMAdminSubs.Visible = true;
                }
                else
                {
                    pnlDefault.Visible = true;
                }
            }
            else
            {
                if (widgetID != 0)
                {
                    pnlPayWhirlSubs.Visible = true;
                }
                else
                {
                    pnlDefault.Visible = true;
                }
            }

            if (string.IsNullOrEmpty(cart.ShipAddress.Street)) { cart.ShipAddress.Street = ""; }
            if (string.IsNullOrEmpty(cart.ShipAddress.Street2)) { cart.ShipAddress.Street2 = ""; }
            if (string.IsNullOrEmpty(cart.ShipAddress.City)) { cart.ShipAddress.City = ""; }
            if (string.IsNullOrEmpty(cart.ShipAddress.State)) { cart.ShipAddress.State = ""; }
            if (string.IsNullOrEmpty(cart.ShipAddress.Country)) { cart.ShipAddress.Country = ""; }
            if (string.IsNullOrEmpty(cart.ShipAddress.Zip)) { cart.ShipAddress.Zip = ""; }

            string iFrame = @"
                        <iframe frameborder='0' style='width:100%; height:auto; border:0; overflow:hidden;' id='paywhirl_frame' scrolling='no' src='";

            iFrame += "https://www.paywhirl.com/widget/payment-form/##FORMID##?api_key=";

            iFrame += Config.ApiKeyPayWhirl();
            //iFrame += "&widget=" + widgetID;
            iFrame += "&email=" + cart.SiteCustomer.Email.Replace(" ", "+").Replace("'", "''");
            iFrame += "&first_name=" + cart.SiteCustomer.FirstName.Replace(" ", "+").Replace("'", "''");
            iFrame += "&last_name=" + cart.SiteCustomer.LastName.Replace(" ", "+").Replace("'", "''");
            iFrame += "&address_1=" + cart.ShipAddress.Street.Replace(" ", "+").Replace("'", "''");
            iFrame += "&address_2=" + cart.ShipAddress.Street2.Replace(" ", "+").Replace("'", "''");
            iFrame += "&city=" + cart.ShipAddress.City.Replace(" ", "+").Replace("'", "''");
            iFrame += "&state=" + cart.ShipAddress.State.Replace(" ", "+").Replace("'", "''");
            iFrame += "&country=" + cart.ShipAddress.Country.Replace(" ", "+").Replace("'", "''");
            iFrame += "&zip=" + cart.ShipAddress.Zip.Replace(" ", "+").Replace("'", "''");
            iFrame += "&phone=" + cart.SiteCustomer.Phone.Replace(" ", "+").Replace("'", "''");

            iFrame += "'></iframe>";
            iFrame = iFrame.Replace("##FORMID##", formID.ToString());
            litPayWhirlIFrame.Text = iFrame;

            lblName.Text = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName;
            lblStreet.Text = cart.ShipAddress.Street;
            lblStreet2.Text = cart.ShipAddress.Street2;
            lblCity.Text = cart.ShipAddress.City;
            lblState.Text = cart.ShipAddress.State;
            lblCountry.Text = cart.ShipAddress.Country;
            lblZip.Text = cart.ShipAddress.Zip;
            lblPhone.Text = cart.SiteCustomer.Phone;
            lblEmail.Text = cart.SiteCustomer.Email;

            txtCardName.Text = lblName.Text;

            List<StripeCoupon> promoCodeList = GetCouponCodeList();
            promoCodeList.Insert(0, new StripeCoupon() { Id = "**No PromoCode" });
            ddlPromoCode.DataSource = promoCodeList;
            ddlPromoCode.DataTextField = "Id";
            ddlPromoCode.DataValueField = "Id";
            ddlPromoCode.DataBind();
            ddlPromoCode.SelectedValue = "";

            //set expiration date range
            ddlExpMonth.SelectedValue = DateTime.Now.Month.ToString();

            SortedList yearList = new SortedList();
            int year = DateTime.Now.Year;
            for (int i = 0; i < 15; i++)
            {
                yearList.Add(year.ToString(), year.ToString());
                year++;
            }
            ddlExpYear.DataSource = yearList;
            ddlExpYear.DataTextField = "Key";
            ddlExpYear.DataValueField = "Value";
            ddlExpYear.DataBind();
            ddlExpYear.SelectedValue = DateTime.Now.Year.ToString();
        }



        protected void btnSubscribe_Click(object sender, System.EventArgs e)
        {
            Cart cart = new Cart();
            cart.Load(Session["CScart"]);
            int customerID = cart.SiteCustomer.CustomerID;
            int fmFormID = (int)Session["fmFormID"];
            string stripeCustomerID = (Session["StripeCustomerID"] == null) ? "0" : Session["StripeCustomerID"].ToString();
            string stripeSubscriberID = (Session["StripeSubscriberID"] == null) ? "0" : Session["StripeSubscriberID"].ToString();
            string stripeInvoiceID = (Session["StripeInvoiceID"] == null) ? "0" : Session["StripeInvoiceID"].ToString();

            if (Page.IsValid)
            {
                Session["PayWhirlPromoCodeID"] = null; //assume no promoCode
                bool isSubscribe = (Session["RequestIsSubscribe"] == null) ? true : Convert.ToBoolean(Session["RequestIsSubscribe"]);
                
                if (isSubscribe)
                {
                    cart.CalculateTotals();
                }

                if (true) //(cart.Total != 0)
                {
                    //validate data
                    bool NoInputErrors = true;
                    txtCardNumber.Text = txtCardNumber.Text.Replace(" ", ""); //get rid of any spaces 

                    if (txtCardNumber.Text.Length < 15)
                    {
                        NoInputErrors = false;
                        cart.Tran.ResultMsg = "Card is less than 15 characters.  Please correct, then try again.";
                        Session["CScart"] = cart;

                    }
                    if (txtCardName.Text.Length < 1)
                    {
                        NoInputErrors = false;
                        cart.Tran.ResultMsg = "Name on card is required.  Please correct, then try again.";
                        Session["CScart"] = cart;
                    }
                    if ((isSubscribe) && ((cart.Items == null) || (cart.Items.ItemCount() == 0)))
                    {
                        NoInputErrors = false;
                        cart.Tran.ResultMsg = "There are no items in your cart.  You must add items to proceed.";
                        Session["CScart"] = cart;
                    }

                    if (!string.IsNullOrEmpty(ddlPromoCode.SelectedValue))
                    {
                        Session["PayWhirlPromoCodeID"] = null;
                        
                        if (string.IsNullOrEmpty(ddlPromoCode.SelectedValue))
                        {
                            NoInputErrors = false;
                            cart.Tran.ResultMsg = "Promo Code: " + ddlPromoCode.SelectedValue + " Not Found or Invalid. Please Try Again";
                            Session["CScart"] = cart;
                        }
                        else
                        {
                            if (!ddlPromoCode.SelectedValue.Contains("**")) //don't use placeholder for a prom code
                            {
                                Session["PayWhirlPromoCodeID"] = ddlPromoCode.SelectedValue;
                            }                           
                        }
                    }
                                                

                    //check for existing charge on this order
                    bool NoExistingCarges = true;

                    if (NoInputErrors && NoExistingCarges)
                    {
                        cart.Tran.CardNo = txtCardNumber.Text;
                        //cart.Tran.CardType = Payment_CardCode.Text;
                        cart.Tran.CardHolder = txtCardName.Text;

                        //if (Payment_CcMonth.Text.Length == 1) { cart.Tran.ExpDate = "0" + Payment_CcMonth.Text + Payment_CcYear.Text; }
                        //else { cart.Tran.ExpDate = Payment_CcMonth.Text + Payment_CcYear.Text; }
                        string expYear = ddlExpYear.SelectedValue.ToString();
                        expYear = expYear.Substring(expYear.Length - 2);
                        cart.Tran.ExpDate = ddlExpMonth.SelectedItem.ToString() + expYear;
                        cart.Tran.CvsCode = txtCSV.Text;
                        //determine card type

                        Session["CScart"] = cart;
                        Session["PaymentOrderNote"] = "processed by PayWhirlSubscriptions Admin";
                        Response.Redirect("Processing.aspx");
                    }
                }
            }
            Response.Redirect("PayWhirlSubscriptions.aspx");

        } //end btnSubscribe_Click

        protected void btnRetry_Click(object sender, System.EventArgs e)
        {
            string fmFormID = Session["fmFormID"].ToString();
            Response.Redirect("PayWhirlSubscriptions.aspx?fmFormID=" + fmFormID);
        }

        private List<StripeCoupon> GetCouponCodeList()
        {
            IEnumerable<StripeCoupon> promoCodeListIE = new List<StripeCoupon>();
            StripeCouponListOptions stripeCouponListOptions = new StripeCouponListOptions();
            stripeCouponListOptions.Limit = 40;
            var couponService = new StripeCouponService();
            try
            {
                promoCodeListIE = couponService.List(stripeCouponListOptions);  // optional StripeListOptions
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "PayWhirl Subscriptions: Promo Code Lookup Error: Coupon list can't be found");
            }
            return promoCodeListIE.ToList();
        }

        private decimal AdjustPayment(decimal origPayment, StripeCoupon stripeCoupon)
        {
            decimal newPayment = origPayment;
            if (stripeCoupon == null)
            {
                return newPayment;
            }

            if (stripeCoupon.AmountOff != null)
            {
                decimal amountOff = (decimal)(stripeCoupon.AmountOff);
                amountOff = amountOff / 100;
                if (amountOff < newPayment)
                {
                    newPayment = newPayment - amountOff;
                }
            }
            else if (stripeCoupon.PercentOff != null)
            {
                decimal percentOff = (decimal)(stripeCoupon.PercentOff);
                percentOff = percentOff / 100;
                if (percentOff < 1)
                {
                    newPayment = newPayment * (1 - percentOff);
                }
            }
            return newPayment;
        }

        protected void SelPromoCode_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ddlPromoCode.SelectedValue != "")
            {
                Session["ddlSelectedPromoCode"] = ddlPromoCode.SelectedValue;
                string subscriptionPlan = Session["SubscriptionPlan"].ToString();
                SubscriptionPlanProvider subscriptionPlanProvider = new Respositories.SubscriptionPlanProvider();
                SubscriptionPlans subPlan = subscriptionPlanProvider.GetByPlanId(subscriptionPlan);

                List<StripeCoupon> promoCodeList = GetCouponCodeList();
                StripeCoupon stripeCoupon = (from c in promoCodeList where c.Id == ddlPromoCode.SelectedValue select c).FirstOrDefault();

                decimal monthlyPayment = AdjustPayment(subPlan.Amount, stripeCoupon);
                lblMonthlyPayment.Text = "Payment Amount: " + monthlyPayment.ToString("C");
            }
        }

    }
}