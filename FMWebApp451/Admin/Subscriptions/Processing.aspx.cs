using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;
using Stripe;
using adminCER;
using System.Collections.Generic;
using System.Linq;

namespace FM.Admin
{
    public partial class SubProcessing : AdminBasePage //System.Web.UI.Page
    {
        Cart cart;

        protected void Page_Load(object sender, EventArgs e)
        {
            cart = new Cart();
            cart.Load(Session["CScart"]);
            if (cart.SiteCustomer.CustomerID != 0)
            {
                if (Request["Proc"] != null)
                {
                    string promoCodeID = (Session["PayWhirlPromoCodeID"] != null) ? Session["PayWhirlPromoCodeID"].ToString() : string.Empty;
                    if (cart.SubscriptionProcessing(promoCodeID) == 0) //transaction was OK
                    {
                        bool isSubscription = (HttpContext.Current.Session["RequestIsSubscribe"] == null) ? true : Convert.ToBoolean(HttpContext.Current.Session["RequestIsSubscribe"]);
                        if (!isSubscription) //was an invoice payment instead of a subscription setup?
                        {
                            //get ready to leave a CER for the invoice transaction
                            cart.SiteOrder = new Order();
                            cart.SiteOrder.ByPhone = true;
                            Certnet.Cart cscart = new Certnet.Cart();
                            cscart.Load(HttpContext.Current.Session["cart"]); //get name of CS rep requesting address change
                            cart.SiteOrder.SourceID = cscart.SiteCustomer.CustomerID; //let the world know this was a customer service order                           
                            HttpContext.Current.Session["SubOrderNumber"] = cart.SiteOrder.OrderID;
                            HttpContext.Current.Session["SubOrderDate"] = DateTime.Now;
                            string stripeInvoiceID = (Session["StripeInvoiceID"] == null) ? "0" : Session["StripeInvoiceID"].ToString();
                            string orderConfirm = (stripeInvoiceID.Length < 8) ? "0" : stripeInvoiceID.Substring(0, 8).ToUpper();
                            HttpContext.Current.Session["SubOrderConfirm"] = Config.AppCode() + orderConfirm;
                            //add a new entry into the CER database
                            adminCER.CER cer = new adminCER.CER(cart);
                            int result = cer.Add();

                            string ordernote = "";
                            if (Session["PaymentOrderNote"] != null)
                            {
                                ordernote = "Processed Subscription; " + Session["PaymentOrderNote"].ToString();
                            }
                            else
                            {
                                ordernote = "Processed Subscription; Error in Payment Processing";
                            }

                            OrderNote.Insertordernote(cart.OrderID, cer.ReceivedBy, ordernote);
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(2000); //wait 2sec to allow PayWhirl to send Shopify the order                            
                            ShopifyCustomer shopCustomer = new ShopifyCustomer();
                            ShopifyAgent.ShopifyOrder shopOrder = new ShopifyAgent.ShopifyOrder();
                            for (int i = 0; i < 10; i++)
                            {
                                shopCustomer = new ShopifyCustomer(cart.SiteCustomer.Email); //syncs w/ Shopify, then finds the customer in ShopifyFM
                                if (shopCustomer.Id != 0)
                                {
                                    //long last_order_id = (shopCustomer.Last_order_id == 0) ? 0 : (long)shopCustomer.Last_order_id;
                                    long last_order_id = 0;
                                    bool ok = Int64.TryParse(shopCustomer.Last_order_id.ToString(), out last_order_id);
                                    if (last_order_id != 0)
                                    {
                                        shopOrder = new ShopifyAgent.ShopifyOrder(last_order_id, 0);
                                        if ((shopOrder.Id != 0) && (shopOrder.Created_at > DateTime.Now.AddMinutes(-5)))
                                        {
                                            break; //get out of loop if order found
                                        }
                                        else
                                        {
                                            shopOrder = new ShopifyAgent.ShopifyOrder(); //clear to be ready to catch failure
                                        }
                                    }
                                }
                                System.Threading.Thread.Sleep(1100); //wait 1.1sec to send the next sync request                            
                            }

                            Order order = new Order(); //hopefully PayWhirl has sent the order to Shopify, which is now sync'd with FM
                            if (!String.IsNullOrEmpty(shopOrder.Name))
                            {
                                order = new Order(shopOrder.Name);
                                if (order.OrderID == 0)
                                {
                                    string msg = "SubscriptionProcessing: ShopifyCustomer.GetOrderList: FM Order not found" + Environment.NewLine;
                                    msg += "FM customer name: " + cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + Environment.NewLine;
                                    msg += "FM customer email: " + cart.SiteCustomer.Email + Environment.NewLine;
                                    mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "", "", "ShopifyCustomer.GetOrderList Error", msg, true);
                                    //Response.Redirect("PayWhirlSubscriptions.aspx?PageAction=ambi");
                                }
                            }
                            else //PayWhirl didn't place order, so place one manually
                            {
                                string msg = "SubscriptionProcessing: ShopifyCustomer.GetOrderList: shopify order not found: shopOrder.Name is null or empty" + Environment.NewLine;
                                msg += "FM customer name: " + cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + Environment.NewLine;
                                msg += "FM customer email: " + cart.SiteCustomer.Email + Environment.NewLine;
                                mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "", "", "ShopifyCustomer.GetOrderList Error", msg, true);
                                //Response.Redirect("PayWhirlSubscriptions.aspx?PageAction=ambi");
                            }

                            //if we got here then Order(shopOrder.Name) must have been found
                            //AdminCart.Cart tempCart = new Cart();
                            if ((order.OrderID != 0) && (order.SourceID == -3)) //make sure it was placed at Shopify
                            {
                                cart.SiteOrder = order;
                                cart.OrderID = order.OrderID;
                                cart.SiteOrder.ByPhone = true;
                                Certnet.Cart cscart = new Certnet.Cart();
                                cscart.Load(HttpContext.Current.Session["cart"]); //get name of CS rep requesting address change
                                cart.SiteOrder.SourceID = cscart.SiteCustomer.CustomerID; //let the world know this was a customer service order                           
                            }
                            else if (order.OrderID == 0)
                            {
                                //continue processing rest of order; subscription lineitemTotal = 0 & will be processed by Shopify
                                
                                cart = BuildOrder(cart);
                                Session["CScart"] = cart;
                                order.OrderID = cart.OrderID;

                                if (cart.OrderID > 0)
                                {
                                    OrderAction orderaction = new OrderAction();
                                    orderaction = new OrderAction();
                                    orderaction.OrderID = cart.OrderID;
                                    orderaction.OrderActionType = 2; //APPROVED
                                    orderaction.OrderActionDate = DateTime.Now;
                                    orderaction.OrderActionReason = "Order Successfully Charged";
                                    orderaction.OrderActionBy = "AdminCart";
                                    orderaction.SSOrderID = -1;
                                    orderaction.SaveDB();
                                }
                            }

                            if (cart.OrderID != 0)
                            {
                                cart.UpdateOrder(); //update orders table
                                HttpContext.Current.Session["SubOrderNumber"] = cart.SiteOrder.OrderID;
                                HttpContext.Current.Session["SubOrderDate"] = cart.SiteOrder.OrderDate;
                                HttpContext.Current.Session["SubOrderConfirm"] = Config.AppCode() + cart.SiteOrder.OrderID.ToString();
                                //add a new entry into the CER database
                                adminCER.CER cer = new adminCER.CER(cart);
                                int result = cer.Add();

                                string ordernote = "";
                                if (Session["PaymentOrderNote"] != null)
                                {
                                    ordernote = "Processed Subscription; " + Session["PaymentOrderNote"].ToString();
                                }
                                else
                                {
                                    ordernote = "Processed Subscription; Error in Payment Processing";
                                }

                                OrderNote.Insertordernote(cart.OrderID, cer.ReceivedBy, ordernote);

                                if (!string.IsNullOrEmpty(promoCodeID))
                                {
                                    ordernote = "Processed Subscription w/ PromoCode; " + promoCodeID.ToString();
                                    OrderNote.Insertordernote(cart.OrderID, cer.ReceivedBy, ordernote);
                                }
                            }
                            else //something went wrong with the order between Shopify & us
                            {
                                adminCER.CER cer = new adminCER.CER(cart);
                                cer.EventDescription = "order not found from Shopify, but subscription was placed correctly: " + cer.EventDescription;
                                int result = cer.Add();

                                string msg = "SubscriptionProcessing: Cart Not Loaded Successfully, but Subscription has been placed" + Environment.NewLine;
                                msg += "FM customer name: " + cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + Environment.NewLine;
                                msg += "FM customer email: " + cart.SiteCustomer.Email + Environment.NewLine;
                                mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "", "", "ShopifyCustomer.GetOrderList Error", msg, true);
                                //Response.Redirect("PayWhirlSubscriptions.aspx?PageAction=ambi");
                            }
                            Response.Redirect("PayWhirlSubscriptions.aspx?PageAction=thankyou");
                        } //end isSubscription processing
                    }
                    else //else cart payment processing didn't work
                    {
                        Response.Redirect("PayWhirlSubscriptions.aspx?PageAction=retry");
                    }

                }

                MasterPage3_Blank.SetRefreshValues(this.Page.Header, "refresh", "2;url=Processing.aspx?Proc=charge");

                ProcessingMessage.Text = @"Processing Your Order...Please Wait<br /><img src='../../images\wait30trans.gif' alt='Please Wait...' />";
            }
            else
            {
                ProcessingMessage.Text = "It appears you have already completed your order, or you session has timed out.";
            }

        }

        public static AdminCart.Cart BuildOrder(AdminCart.Cart cart)
        {

            cart.SiteOrder.OrderID = 600001; //placeholder
            cart.SiteOrder.ByPhone = true;
            cart.SiteOrder.SubTotal = cart.TotalProduct; //account for PromoCode...
            cart.SiteOrder.TotalShipping = 0;
            cart.SiteOrder.TotalTax = 0;
            cart.SiteOrder.TotalCoupon = 0;
            cart.SiteOrder.OrderDate = DateTime.Now;
            //get PayWhirl Customer
            AdminCart.Customer c = new AdminCart.Customer(cart.SiteCustomer.CustomerID);
            AdminCart.Address a = AdminCart.Address.LoadAddress(c.CustomerID, 1); //get billing address
            cart.SiteOrder.BillingAddressID = a.AddressId;
            a = AdminCart.Address.LoadAddress(c.CustomerID, 2); //get shipping address
            cart.SiteOrder.ShippingAddressID = a.AddressId;
            PayWhirl.PayWhirlSubscriber pwSubScriber = new PayWhirl.PayWhirlSubscriber();
            try
            {
                //pwSubScriber = PayWhirl.PayWhirlSubscriber.FindSubscriberByEmail(c.Email);
                pwSubScriber = PayWhirl.PayWhirlBiz.FindSubscriberByEmail(c.Email);

            }
            catch (Exception ex)
            {
                string body = "Customer Last Name: " + c.LastName + Environment.NewLine;
                body += "Customer Email: " + c.Email + Environment.NewLine;
                body += "Ex Message: " + ex.Message + Environment.NewLine;
                mvc_Mail.SendDiagEmail("Error in Subscription Manual Order Processing: No PayWhirl Subscriber found", body);
            }

            //get the customer from Stripe
            var customerService = new StripeCustomerService();
            StripeCustomer stripeCustomer = new StripeCustomer();

            try
            {
                stripeCustomer = customerService.Get(pwSubScriber.Stripe_id);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "Error in Subscription Manual Order Processing: Get StripeCustomer: ", "Stripe Error in Translator");
            }
            //get last Invoice
            var invoiceService = new StripeInvoiceService();
            var invoiceListOptions = new StripeInvoiceListOptions();
            invoiceListOptions.CustomerId = stripeCustomer.Id;
            invoiceListOptions.Date = new StripeDateFilter { GreaterThanOrEqual = DateTime.UtcNow.Date }; // today's invoices
            List<StripeInvoice> stripeInvoiceList = new List<StripeInvoice>();
            try
            {
                stripeInvoiceList = invoiceService.List().ToList(); // optional StripeInvoiceListOptions
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "Error in Subscription Manual Order Processing: ", "Stripe Error in Translator: Invoice");
            }
            if (stripeInvoiceList.Count != 0)
            {
                stripeInvoiceList.OrderByDescending(x => x.Date);
                StripeInvoice stripeInvoice = stripeInvoiceList.First(); //get the last invoice
                cart.SiteOrder.Total = Convert.ToDecimal(stripeInvoice.Total) / 100;
                decimal adjust = Convert.ToDecimal(stripeInvoice.Subtotal - stripeInvoice.Total) / 100; //Stripe use integer cents not dollars
                cart.SiteOrder.Adjust = cart.SiteOrder.Adjust + adjust;
                List<Transaction> tList = ShopifyFMOrderTranslator.Get24HRStripeTransactions(stripeCustomer.Id, cart.SiteOrder);
                if (tList.Count == 0)
                {
                    string body = "Customer Last Name: " + c.LastName + Environment.NewLine;
                    body += "Customer Email: " + c.Email + Environment.NewLine;
                    mvc_Mail.SendDiagEmail("Error in Subscription Manual Order Processing: No Transaction found", body);
                }
                else
                {
                    cart.Tran = tList.FirstOrDefault();
                }
                cart.SaveToOrder(); //drop into orders table; cart.OrderID is updated 
                cart.SiteOrder.OrderID = cart.OrderID;
                cart.Tran.OrderID = cart.OrderID;
                cart.Tran.SaveDB();
                string promoCode = "n/a";
                try
                {
                    promoCode = (stripeInvoice.StripeDiscount.StripeCoupon.Id == null) ? "n/a" : stripeInvoice.StripeDiscount.StripeCoupon.Id;
                }
                catch 
                { ; } //do nothing on exception

                if (adjust > 0) // add an OrderNote if a promoCode was used
                {
                    OrderNote.Insertordernote(cart.SiteOrder.OrderID,
                        "Subscription Manual Order Processing", "OrderID: " + cart.SiteOrder.OrderID.ToString() + "; Discount Code= " + promoCode + "; Discount Amount= " + adjust.ToString("C") + "; Discount Type= fixed_amount");
                }
            }
            return cart;
        }
    } 
}
