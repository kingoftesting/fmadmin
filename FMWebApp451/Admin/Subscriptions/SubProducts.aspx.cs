﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminCart;

namespace FM.Admin
{
    public partial class SubProducts : AdminBasePage //System.Web.UI.Page
    {

        Cart cart = new Cart();

        protected void Page_Load(object sender, EventArgs e)
        {
            cart.Load(Session["CScart"]);

            productrepeater.DataSource = Product.GetSubProducts();
            productrepeater.DataBind();
        }

        protected void productrepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int widgetID = 0;
                int formID = 0;
                int fmFormID = 0;
                decimal setupFee = 0.00M; // use if an upfront charge (setup fee) and then trial period before subscription begins
                Product p = (Product)e.Item.DataItem;
                fmFormID = p.ProductID;

                //switch (p.SubscriptionPlan.ToLower())
                //{
                //    case "facemaster-gold-club":

                //        widgetID = 2854;
                //        formID = 2154;
                //        fmFormID = 1;
                //        break;

                //    case "facemaster-platinum-club":
                //        widgetID = 2855;
                //        formID = 2155;
                //        fmFormID = 2;
                //        break;

                //    case "facemaster-system-3-easy-payments":
                //        widgetID = 2866;
                //        formID = 2156;
                //        fmFormID = 3;
                //        break;

                //    case "facemaster-system-4-easy-payments":
                //        widgetID = 2864;
                //        formID = 2157;
                //        fmFormID = 4;
                //        break;

                //    case "e-serum-continuity":
                //        widgetID = 2865;
                //        formID = 2158;
                //        fmFormID = 5;
                //        break;

                //    case "fm-platinum-4-pay":
                //        widgetID = 2864;
                //        formID = 2150;
                //        fmFormID = 6;
                //        break;

                //    case "99-crystalift-with-club-membership":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 7;
                //        setupFee = 59.01M;
                //        break;

                //    case "crystalift-club-membership":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 8;
                //        break;

                //    case "crystalift-3-easy-payments":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 9;
                //        break;

                //    case "crystalift-4-easy-payments":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 10;
                //        break;

                //    case "sonulase-3-easy-payments":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 11;
                //        break;

                //    case "sonulase-4-easy-payments":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 12;
                //        break;

                //    case "sonulase-4-payments-4999":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 13;
                //        break;

                //    case "facemaster-black-friday-3-pay-rf":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 14;
                //        break;

                //    case "facemaster-4-easy-payments-rf":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 15;
                //        break;

                //    case "facemaster---4-payments-of-7499":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 16;
                //        break;

                //    case "facemaster-system-rf-4-pay-4999":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 17;
                //        break;

                //    case "facemaster-ultimate-plan":
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 18;
                //        break;

                //    default:
                //        widgetID = 0;
                //        formID = 0;
                //        fmFormID = 0;
                //        break;

                //}

                string pLink = "PayWhirlSubscriptions.aspx?widgetID=" + widgetID.ToString() + "&formID=" + formID.ToString() + "&fmFormID=" + fmFormID.ToString() + "&productID=" + p.ProductID.ToString() + "&setupFee=" + setupFee.ToString();

                HyperLink lnkImg = (HyperLink)e.Item.FindControl("lnkImg");
                if (lnkImg != null)
                { lnkImg.NavigateUrl = pLink; }

                HyperLink lnkImg2 = (HyperLink)e.Item.FindControl("lnkImg2");
                if (lnkImg2 != null)
                { lnkImg2.NavigateUrl = pLink; }

                HyperLink lnkImg3 = (HyperLink)e.Item.FindControl("lnkImg3");
                if (lnkImg3 != null)
                { lnkImg3.NavigateUrl = pLink; }

                HyperLink lnkImg4 = (HyperLink)e.Item.FindControl("lnkImg4");
                if (lnkImg4 != null)
                { lnkImg4.NavigateUrl = pLink; }

                if (p.Price > p.SalePrice)
                {
                    Literal litSalePrice = (Literal)e.Item.FindControl("LitSalePriceLine");

                    litSalePrice.Text = "$" + p.SalePrice.ToString("N") + " - Sale!";
                }
            }
        }
    }
}