<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="FM.Admin.ThankYou" Title="Thank You" Codebehind="ThankYou.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<table width="900px">
	<tr>
		<td class = "custservice" colspan="2">			
			<h2>Order Successful</h2>
        </td>
	</tr>
	
	<tr>
		<td class = "custservice" colspan="2">
		    <br />
			Below is a copy of your order to print for your records:<br />
			<br />
			<b>Order Number:</b> <asp:Label ID="lblOrderId" Runat="server" />
			&nbsp;&nbsp;<b>Order Date:</b> <asp:Label ID="lblOrderDate" Runat="server" />
			<br /><br />
			
			
			
    		        <asp:Literal ID="cd" runat="server" />
			
		</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>					
		<tr>
			<td class = "custservice" valign="top"><b>Shipping Information:</b><br />
				<asp:Label id="ShipInfo" Visible="true" runat="server" /></td>
			<td class = "custservice" valign="top"><b>Billing Information:</b><br />
				<asp:Label id="BillInfo" Visible="true" runat="server" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>										
	</table>
	
	<asp:Literal ID="GoogLit" runat="server" />

</asp:Content>

