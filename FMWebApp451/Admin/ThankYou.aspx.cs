using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;
using FM2015.Helpers;
using System.Collections.Generic;

namespace FM.Admin
{
    public partial class ThankYou : AdminBasePage //System.Web.UI.Page
    {
        Cart cart;

        void Page_Init(Object sender, EventArgs e)
        {
            //show shipping info only
            //bShipcalculated = true;
            //PlaceholderShipping1.Visible = false;

            cart = new Cart();
            cart.Load(Session["CScart"]);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (cart.SiteCustomer.CustomerID == 0) { Response.Redirect("ManageUsers.aspx"); }

            ShipInfo.Text = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + "<br>" + cart.ShipAddress.Street;
            if ((string.IsNullOrEmpty(cart.ShipAddress.Street2) || (cart.ShipAddress.Street2 == " ")))
            { ShipInfo.Text += "<br>" + cart.ShipAddress.City + ", " + cart.ShipAddress.State + " " + cart.ShipAddress.Zip + "<br>" + DBUtil.GetCountry(cart.ShipAddress.Country) + "<br> Shipping Method: "; }
            else { ShipInfo.Text += "<br>" + cart.ShipAddress.Street2 + "<br>" + cart.ShipAddress.City + ", " + cart.ShipAddress.State + " " + cart.ShipAddress.Zip + "<br>" + DBUtil.GetCountry(cart.ShipAddress.Country) + "<br> Shipping Method: "; }

            if (cart.ShipType == 1) { ShipInfo.Text += "Ground"; }
            if (cart.ShipType == 2) { ShipInfo.Text += "Expedited (2-Day Air)"; }
            if (cart.ShipType == 3) { ShipInfo.Text += "Express"; }

            BillInfo.Text = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName;
            if ((string.IsNullOrEmpty(cart.BillAddress.Street2) || (cart.BillAddress.Street2 == " ")))
            { BillInfo.Text += "<br>" + cart.BillAddress.Street + "<br>" + cart.BillAddress.City + ", " + cart.BillAddress.State + " " + cart.BillAddress.Zip + "<br>" + DBUtil.GetCountry(cart.BillAddress.Country); }
            else { BillInfo.Text += "<br>" + cart.BillAddress.Street + "<br>" + cart.BillAddress.Street2 + "<br>" + cart.BillAddress.City + ", " + cart.BillAddress.State + " " + cart.BillAddress.Zip + "<br>" + DBUtil.GetCountry(cart.BillAddress.Country); }


            DateTime orderDate = cart.SiteOrder.OrderDate;
            DateTime shipDate = cart.SiteOrder.OrderDate;
            DateTime arrivalDate = cart.SiteOrder.OrderDate;

            int TimeOfDayPenalty = 0;
            int ShipDays = 7;
            int CanadaShipDays = 21;

            int ShipDaysTotal = 7;

            if (orderDate.Hour > 10)
            {
                TimeOfDayPenalty = 1;
            }
            if (cart.ShipAddress.Country == "US")
            {
                ShipDaysTotal = TimeOfDayPenalty + ShipDays;
            }
            else
            {
                //assume Canada & Int'l both take about 21 days for delivery
                ShipDaysTotal = TimeOfDayPenalty + CanadaShipDays;
            }

            Certnet.Utils.XDateTime xshipDate = new Certnet.Utils.XDateTime(orderDate.ToString());
            xshipDate.AddBusinessDays(TimeOfDayPenalty);

            Certnet.Utils.XDateTime xarrivalDate = new Certnet.Utils.XDateTime(orderDate.ToString());
            xarrivalDate.AddBusinessDays(ShipDaysTotal);

            shipDate = xshipDate.Date;
            arrivalDate = xarrivalDate.Date;

            string carthtml = @"
<table cellpadding='5' rules='all' BorderColor='Black' bordercolor='Black' border='1' style='border-color:Black;width:80%;border-collapse:collapse;'>
	<tr align='Center' style='background-color:#DDDDDD;'>
		<td class = 'custservice'><span>Item</span></td>
        <td class = 'custservice'><span>Quantity</span></td>
        <td class = 'custservice'><span>Price</span></td>
        <td class = 'custservice'><span>PriceAdjustment</span></td>
        <td class = 'custservice'><span>Total</span></td>
	</tr>";

            foreach (Item i in cart.Items)
            {
                carthtml += @"
    <tr>
		<td class = 'custservice' align=left valign='center'><span><img Width='160px' src=../images/products/" + i.LineItemProduct.CartImage + "> " + i.LineItemProduct.Name + @"</span></td>
        <td class = 'custservice' align='Right'><span>" + i.Quantity.ToString() + @"</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + i.LineItemProduct.Price.ToString("N") + @"</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + i.LineItemDiscount.ToString("N") + @"</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + i.LineItemTotal.ToString("N") + @"</span></td>
	</tr>
        ";
            }

            string shipmode = "";
            if (cart.ShipType == 1) shipmode = "Ground";
            if (cart.ShipType == 2) shipmode = "Expedited (2-Day Air)";
            if (cart.ShipType == 3) shipmode = "Express (Guarenteed Overnight)";

            carthtml += @"
    <tr>
		<td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>SubTotal:</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.SubTotal.ToString("N") + @"</span></td>
	</tr><tr>
		<td class = 'custservice' align='Right' colspan='4'><span>Shipping:  " + shipmode + @"</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.TotalShipping.ToString("N") + @"</span></td>
	</tr><tr>
		<td class = 'custservice' align='Right' colspan='4'><span>Sales Tax:</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.TotalTax.ToString("N") + @"</span></td>
	</tr>";

            if (cart.TotalDiscounts != 0)
            {
                carthtml += @"<tr>
		<td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>Coupon Discount: (" + cart.SiteCoupon.CouponCode + " - " + cart.SiteCoupon.CouponDescription + @")</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.TotalCoupon.ToString("N") + @"</span></td>
	</tr>";
            }

            carthtml += @"<tr>
		<td class = 'custservice' nowrap='nowrap' align='Right' colspan='4'><span>Total:</span></td>
        <td class = 'custservice' nowrap='nowrap' align='Right'><span>" + cart.SiteOrder.Total.ToString("N") + @"</span></td>
	</tr>
</table>";
            cd.Text = carthtml;

            //lblOrderId.Text = order.OrderId.ToString();
            lblOrderId.Text = cart.OrderID.ToString();
            lblOrderDate.Text = cart.SiteOrder.OrderDate.ToString();

            GoogLit.Text = "<script type='text/javascript'>";
            GoogLit.Text += "var gaJsHost = (('https:' == document.location.protocol ) ? 'https://ssl.' : 'http://www.');";
            GoogLit.Text += "document.write(unescape('%3Cscript src=' + gaJsHost + 'google-analytics.com/ga.js type= text/javascript %3E%3C/script%3E'));";
            GoogLit.Text += "</script>";

            GoogLit.Text += "<script type='text/javascript'>";
            GoogLit.Text += "var pageTracker = _gat._getTracker('UA-1560367-1');";
            GoogLit.Text += "pageTracker._trackPageview();";

            string orderid = "'" + cart.OrderID.ToString() + "',";
            string description = "'FaceMaster Order',";
            string gtotal = "'" + cart.TotalProduct.ToString("N") + "',";
            string tax = "'" + cart.TotalTax.ToString("N") + "',";
            string shipping = "'" + cart.TotalShipping.ToString("N") + "',";
            string city = "'" + cart.ShipAddress.City + "',";
            string state = "'" + cart.ShipAddress.State + "',";
            string country = "'" + cart.ShipAddress.Country + "'";
            GoogLit.Text += "pageTracker._addTrans(" + orderid + description + gtotal + tax + shipping + city + state + country + ");";

            foreach (Item it in cart.Items)
            {
                string sku = "'" + it.LineItemProduct.Sku + "',";
                string name = "'" + it.LineItemProduct.Name + "',";
                string cat = "' ',";
                decimal adjPrice = (it.LineItemProduct.SalePrice - it.LineItemDiscount);
                string price = "'" + adjPrice.ToString("N") + "',";
                string qty = "'" + it.Quantity + "'";
                GoogLit.Text += "pageTracker._addItem(" + orderid + sku + name + cat + price + qty + ");";
            }

            GoogLit.Text += "pageTracker._trackTrans();";
            GoogLit.Text += " </script>";

            if (ConfigurationManager.AppSettings["AllowShopifyTranslations"].ToString().ToUpperInvariant() == "YES")
            {
                if (cart.Total != 0) // don't bother on a make-good order
                {
                    FMShopifyOrderTranslator.CreateShopifyOrder(cart.OrderID);
                    System.Threading.Thread.Sleep(1100);
                    List<int> list = ShopifyAgent.ShopifyAgent.SyncShopify(); //sync orders from Shopify
                }
            }

            Session["CScart"] = null;
        }
    } 
}
