using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using AdminCart;
using FMmetrics;


public partial class Admin_UnshippedOrders : AdminBasePage //System.Web.UI.Page
{

    DataView dvUnshipped = new DataView();

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "DESC"; }
        set { ViewState["SortDirection"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Certnet.Cart cart = new Certnet.Cart();
            cart.Load(Session["cart"]);
            int customerID = cart.SiteCustomer.CustomerID;
            try
            {
                if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "REPORTS")))
                { }
                else
                {
                    throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
                }
            }
            catch (SecurityException ex)
            {
                string msg = "Error in DailySalesTime" + Environment.NewLine;
                msg += "User did not have permission to access page" + Environment.NewLine;
                msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                Response.Redirect("../Login.aspx");
            }

            btnGetCSV.Visible = false; //don't show CSV button until there's data displayed
            btnSendEmail.Visible = false;

            BindUnShipped();
        }
    }

    private void BindUnShipped()
    {
        Session["UnshippedReportDate"] = null; //invalidate the cache before running the report
        DataSet ds = Metrics.GetUnshippedOrders();
        dvUnshipped = new DataView(ds.Tables[0]);
        dvUnshipped.Sort = "OrderDate" + " " + "ASC";
        gvwDataList.DataSource = dvUnshipped;
        Session["grvDataSource"] = dvUnshipped; //save for later; used to Sort, CSV download, Email
        gvwDataList.DataBind();

        if (dvUnshipped.Table.Rows.Count == 0) //hide Email & CSV buttons if empty table
        { 
            btnGetCSV.Visible = false;
            btnSendEmail.Visible = false;
        }
        else
        { 
            btnGetCSV.Visible = true;
            btnSendEmail.Visible = true;
        }
    }
 
    protected void btnCSV_Click(object sender, EventArgs e)
    {
        //FillWebSales();
        dvUnshipped = Session["grvDataSource"] as DataView;
        dvUnshipped.Sort = "OrderDate ASC";
        //Helpers.SendCSV(websales);
        Utilities.FMHelpers.SendCSV(dvUnshipped);
    }

    protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataView dv = Session["grvDataSource"] as DataView;

        if (dv != null)
        {

            string m_SortDirection = GetSortDirection();
            dv.Sort = e.SortExpression + " " + m_SortDirection;

            gvwDataList.DataSource = dv;
            gvwDataList.DataBind();
        }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;

            case "DESC":

                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    public void gvwDataList_PageIndexChanging(object source, GridViewPageEventArgs e)
    {
        gvwDataList.PageIndex = e.NewPageIndex;
        //gvwDataList.DataBind();
        BindUnShipped();
    }

    public void gvwDataList_PageIndexChanged(object source, EventArgs e)
    {
    }

    protected void gvwDataList_RowEditing(object sender, EventArgs e)
    {
    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {

        //Need to write the code for email delivery!

        //FillWebSales();
        dvUnshipped = Session["grvDataSource"] as DataView;
        dvUnshipped.Sort = "OrderDate ASC";


        Certnet.Cart cart = new Certnet.Cart();
        cart.Load(Session["cart"]);
        int customerID = cart.SiteCustomer.CustomerID;
        string contactTo = cart.SiteCustomer.Email;
        string contactFrom = AdminCart.Config.MailFrom();

        //SqlDataReader dr = Customer.GetCustomerData(Convert.ToInt32(Session["CustomerID"]));
        String contactRawCC = "";
        //if (dr.Read()) { contactRawCC = dr["SampleEmailCC"].ToString(); }
        string contactName = "";
        string contactSubject = "FM REPORTS: Unshipped Orders";

        string contactBody = "FM REPORTS: Unshipped Orders";
        contactBody += "<br /><br /><br /><br />";

        contactBody += Utilities.FMHelpers.ConvertDataViewToHTML(dvUnshipped);

        if (mvc_Mail.SendMail(contactTo, contactFrom, contactName, contactRawCC, contactSubject, contactBody, true))
        {
            // show a confirmation message, and reset the fields
            lblFeedbackOK.Visible = true;
            lblFeedbackKO.Visible = false;
        }
        else
        {
            //otherwise let the customer know it didn't work
            lblFeedbackOK.Visible = false;
            lblFeedbackKO.Visible = true;
        }

        btnGetCSV.Visible = false;
        btnSendEmail.Visible = false;

    }


}

