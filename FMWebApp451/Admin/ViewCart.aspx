<%@ Page Language="C#" trace="False" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="FM.Admin.ViewCart" Title="View Cart" Codebehind="ViewCart.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<span style=" width:600px; text-align :center" >
<asp:Literal ID="CartList" runat="server" />
</span><br />

	<asp:Literal ID="debug" runat="server" />
	
	<asp:Repeater ID="repeateritems" runat="server" EnableViewState="false" OnItemDataBound="repeateritems_ItemDataBound"  >
	
	<HeaderTemplate>
	<table class="custservice" width="90%" cellpadding='5' rules='all' border='1' style='border-width:0px;border-collapse:collapse;' >

	<tr align='center' style='background-color:#DDDDDD;'>
		<td class = "custservice" style="width:50%;">Item</td>
        <td class = "custservice" >Quantity</td>
        <td class = "custservice" >Price</td>
        <td class = "custservice" >Discount</td>
        <td class = "custservice" >CS Adjust</td>
        <td class = "custservice" >Total</td>
        
	</tr>
	</HeaderTemplate>
	<ItemTemplate>
    <tr>
		<td class = "custservice" >
             <div style="float:left; vertical-align:top; margin:5px;">
                <asp:Image ID="CartImage" Width="160px" runat="server" /> 
            </div>
                <asp:label ID="CartProductName" runat="server" />
           <!--
                <asp:Literal ID="ProductIDLit" runat="server" Visible="false" />
            -->

		</td>
        <td class = "custservice" align='center'><asp:DropDownList CssClass="CartDropdown" ID="CartQuantity" Width="50%" runat="server"  AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"/><br /><asp:Literal ID="DeleteLink" runat="server" /></td>
        <td class = "custservice" align='right'><asp:Literal ID="Price" runat="server" /></td>
        <td class = "custservice" align='right'><asp:Literal ID="Discount" runat="server" /></td>
        <td class = "custservice" align='right'>
            <asp:TextBox ID="Adjust" Width="50px" runat="server"></asp:TextBox>
           <asp:CompareValidator ID="valAdjust" runat="server" Type="Double" ControlToValidate="Adjust" 
                ErrorMessage="<br />The format of the CS adjust value is not valid (use x.yz)." ToolTip="The format of the CS Adjust value is not valid (use x.yz)."
                Display="Dynamic" Operator="DataTypeCheck" ></asp:CompareValidator>            
        </td>
        <td class = "custservice" align='right' width="50"><asp:Literal ID="LineItemTotal" runat="server" /></td>
	</tr>
	</ItemTemplate>
	
	
	<FooterTemplate>
        <tr>
            <td class="custservice" align="left" style="border-style: none;"">
                <asp:Button Text="Continue Shopping" ID="buttonContinueShopping" runat="server" OnClick="buttonContinueShopping_Click" />
                <asp:Button Text="CheckOut" ID="buttonCheckout" runat="server" OnClick="buttonCheckout_Click" />
            </td>
           <td class="custservice" colspan="5" align="right" style="border-style: none;">
                <asp:Button Text="Use Defaults" ID="btnDefault" runat="server" OnClick="btnDefault_Click" />
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button Text="ReCalcTotals" ID="btnReCalc" runat="server" OnClick="btnReCalc_Click" />
            </td>
        </tr>
        <tr>
            <td class="custservice" colspan="5" align="right" style="border-style: none;">
                SubTotal
            </td>
            <td class="custservice" align="right" style="border-style: none;">
                <asp:Literal ID="SubTotal" runat="server" />
            </td>
        </tr>
	    <tr>
            <td class = "custservice" colspan="5" align="right" style="border-style:none;">
	            <div class="groupHTML" style="float:right">
                    <asp:Label ID="lblShipping" runat="server" Text="Shipping:&nbsp;"></asp:Label>
	                <asp:DropDownList CssClass="groupHTML" ID="ddlShipping" Width="50%" runat="server"  AutoPostBack="true" OnSelectedIndexChanged="ddlShipping_SelectedIndexChanged">
		                <asp:ListItem Text="ground" Value="1" Selected="true" />
		            </asp:DropDownList>
                </div>               
	        </td>
	        <td class = "custservice" align="right" style="border-style:none;">
            <asp:TextBox ID="Shipping" Width="40px" runat="server"></asp:TextBox>
               <asp:CompareValidator ID="valShipping" runat="server" Type="Double" ControlToValidate="Shipping" 
                    ErrorMessage="<br />The format of the S&H value is not valid (use x.yz)." ToolTip="The format of the S&H value is not valid (use x.yz)."
                    Display="Dynamic" Operator="DataTypeCheck" ></asp:CompareValidator>            
            </td></tr>
        <tr><td class = "custservice" colspan="5" align="right" style="border-style:none;">Sales Tax</td><td class = "custservice" align="right" style="border-style:none;"><asp:Literal ID="Tax" runat="server" /></td></tr>
	    <tr><td class = "custservice" colspan="5" align="right" style="border-style:none;">Discounts</td><td class = "custservice" align="right" style="border-style:none;"><asp:Literal ID="TotalDiscounts" runat="server" /></td></tr>
	    <tr><td class = "custservice" colspan="5" align="right" style="border-style:none;">Total</td><td class = "custservice" align="right" style="border-style:none;"><asp:Literal ID="Total" runat="server" /></td></tr>
	    
	
	</tr>
	</table>
	</FooterTemplate>
    </asp:Repeater>

    <p>&nbsp;</p>

</asp:Content>

