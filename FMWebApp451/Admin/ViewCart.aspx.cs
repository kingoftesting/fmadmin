using System;
using System.Web.UI.WebControls;
using AdminCart;

namespace FM.Admin
{
    public partial class ViewCart : AdminBasePage //System.Web.UI.Page
    {
        Cart cart;

        void Page_Init(Object sender, EventArgs e)
        {
            cart = new Cart();
            cart.Load(Session["CScart"]);

            //if (!Page.IsPostBack)
            //{
            repeateritems.DataSource = cart.Items;
            repeateritems.DataBind();
            //}
            //CartList.Text = "hello";
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            Trace.Warn("start pageload");
            cart.Load(Session["CScart"]);

            if (Request["sset"] != null) { Session["sset"] = Request["sset"].ToString(); }

            if (Request["action"] != null)
            {
                cart.CSShipping = -1; //clear any previous customer service shipping $$; about to change products in cart
                if (Request["action"].ToString() == "AddMult")
                {
                    cart.Items = new CartItems(); //clear cart

                    if (Request["count"] != null)
                    {
                        int count = 0;
                        Int32.TryParse(Request["count"].ToString(), out count);
                        for (int i = 0; i < count; i++)
                        {
                            int pID = 0;
                            int qty = 0;
                            string reqPID = "pID" + i.ToString();
                            if (Request[reqPID] != null)
                            { Int32.TryParse(Request[reqPID].ToString(), out pID); }
                            string reqQTY = "qty" + i.ToString();
                            if (Request[reqQTY] != null)
                            { Int32.TryParse(Request[reqQTY].ToString(), out qty); }
                            else { qty = 1; }
                            if ((pID <= 0) || (qty <= 0))
                            { Response.Redirect("http://www.phrack.com?From=DumbHacker&Response=GoAway"); }
                            else
                            {
                                Item item = new Item(pID, qty);
                                if (Session["sset"] != null)
                                {
                                    int ssetID = Convert.ToInt32(Session["sset"].ToString());
                                    cart.SiteOrder.SiteSettingsID = ssetID;
                                    item = UpdateCartItem(item, ssetID);

                                }
                                cart.Items.AddItem(item);
                            }
                        }
                    }
                    Session["CScart"] = cart;
                    Response.Redirect("ViewCart.aspx");
                }

                if (Request["action"].ToString() == "Load")
                {
                    cart.Items = new CartItems(); //clear cart

                    int orderID = 0;
                    Int32.TryParse(Request["OrderID"].ToString(), out orderID);
                    if (orderID <= 0)
                    {
                        Response.Redirect("http://www.phrack.com?From=DumbHacker&Response=GoAway");
                    }
                    cart = cart.GetCartFromOrder(orderID);
                    if (Session["sset"] != null)
                    {
                        foreach (Item item in cart.Items)
                        {
                            int ssetID = Convert.ToInt32(Session["sset"].ToString());
                            cart.SiteOrder.SiteSettingsID = ssetID;
                            cart.Items.UpdateItem(UpdateCartItem(item, ssetID));

                        }
                    }
                    Session["CScart"] = cart;
                    Response.Redirect("ViewCart.aspx");
                }

                if (Request["action"].ToString() == "Add")
                {
                    int possibleItem = 0;
                    Int32.TryParse(Request["ProductID"].ToString(), out possibleItem);
                    if (possibleItem == 0)
                    {
                        Response.Redirect("http://www.phrack.com?From=DumbHacker&Response=GoAway");
                    }
                    Item item = new Item(possibleItem, 1);
                    cart.Items.AddItem(item);

                    Session["CScart"] = cart;
                    Response.Redirect("ViewCart.aspx");
                }
                if (Request["action"].ToString() == "Delete")
                {
                    int possibleItem = 0;
                    Int32.TryParse(Request["ProductID"].ToString(), out possibleItem);
                    if (possibleItem == 0)
                    {
                        Response.Redirect("http://www.phrack.com?From=DumbHacker&Response=GoAway");
                    }
                    Item item = new Item(possibleItem, 1);
                    cart.Items.DeleteItem(item);

                    Session["CScart"] = cart;
                    Response.Redirect("ViewCart.aspx");
                }


            }

            /*
                DropDownList ddlShipping = (DropDownList)repeateritems.Controls[repeateritems.Controls.Count - 1].FindControl("ddlShipping") as DropDownList;
                string t = ddlShipping.SelectedItem.ToString();
            if (!IsPostBack)
            {
                //select the appropriate shipping type, if changed (default is Ground)
                ddlShipping = (DropDownList)repeateritems.Controls[repeateritems.Controls.Count - 1].FindControl("ddlShipping") as DropDownList;
                if ((cart.ShipType - 1) != ddlShipping.SelectedIndex) { ddlShipping.SelectedIndex = cart.ShipType - 1; }
            }
             */

            BindCart();
        }


        protected void BindCart()
        {
            cart.CalculateTotals();
        }


        protected void buttonCheckout_Click(object sender, EventArgs e)
        {
            cart.Load(Session["CScart"]);
            ReCalcCart();
            cart.CalculateTotals();
            Session["CScart"] = cart;

            if (cart.SiteCustomer.CustomerID != 0)
            {
                if (Config.RedirectMode == "Test")
                {
                    try
                    {
                        string body = "";
                        body += "Logged in at: " + Request.ServerVariables["SERVER_NAME"].ToString() + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "login email: " + cart.SiteCustomer.Email + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += ", Last Name: " + cart.SiteCustomer.LastName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += ", First Name: " + cart.SiteCustomer.FirstName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += ", ID: " + cart.SiteCustomer.CustomerID.ToString() + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += ", IP addr: " + Request.ServerVariables["REMOTE_ADDR"].ToString() + Environment.NewLine + "------------------" + Environment.NewLine;

                        Certnet.Cart tempcart = new Certnet.Cart(); //just need the current user ID
                        tempcart.Load(Session["cart"]);
                        body += "Admin login email: " + tempcart.SiteCustomer.Email + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "Admin Last Name: " + tempcart.SiteCustomer.LastName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "Admin First Name: " + tempcart.SiteCustomer.FirstName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "AdminID: " + tempcart.SiteCustomer.CustomerID + Environment.NewLine + "------------------" + Environment.NewLine;
                        mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "test mode Checkout occurred", body);
                    }
                    catch
                    {
                    }
                    Response.Redirect("CheckOut.aspx");
                }
                else
                {
                    //if (HttpContext.Current.Request.IsSecureConnection) { Response.Redirect("CheckOut.aspx"); }
                    //else { Response.Redirect("https://legacy.facemaster.com/admin/CheckOut.aspx"); }
                    Response.Redirect("CheckOut.aspx");
                }
            }
            else
            {
                if (Config.RedirectMode == "Test")
                {
                    try
                    {
                        string body = "";
                        body += "Logged in at: " + Request.ServerVariables["SERVER_NAME"].ToString() + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "login email: " + cart.SiteCustomer.Email + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += ", Last Name: " + cart.SiteCustomer.LastName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += ", First Name: " + cart.SiteCustomer.FirstName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += ", ID: " + cart.SiteCustomer.CustomerID.ToString() + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += ", IP addr: " + Request.ServerVariables["REMOTE_ADDR"].ToString() + Environment.NewLine + "------------------" + Environment.NewLine;

                        Certnet.Cart tempcart = new Certnet.Cart(); //just need the current user ID
                        tempcart.Load(Session["cart"]);
                        body += "Admin login email: " + tempcart.SiteCustomer.Email + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "Admin Last Name: " + tempcart.SiteCustomer.LastName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "Admin First Name: " + tempcart.SiteCustomer.FirstName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "AdminID: " + tempcart.SiteCustomer.CustomerID + Environment.NewLine + "------------------" + Environment.NewLine;

                        mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "test mode Checkout occurred: not logged in", body);
                    }
                    catch
                    {
                    }
                    Response.Redirect("Login.aspx?redir=admin/CheckOut.aspx");
                }
                else
                {
                    try
                    {
                        string body = "";
                        body += "Logged in at: " + Request.ServerVariables["SERVER_NAME"].ToString() + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "login email: " + cart.SiteCustomer.Email + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "Last Name: " + cart.SiteCustomer.LastName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "First Name: " + cart.SiteCustomer.FirstName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "CustomerID: " + cart.SiteCustomer.CustomerID.ToString() + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "IP addr: " + Request.ServerVariables["REMOTE_ADDR"].ToString() + Environment.NewLine + "------------------" + Environment.NewLine;

                        Certnet.Cart tempcart = new Certnet.Cart(); //just need the current user ID
                        tempcart.Load(Session["cart"]);
                        body += "Admin login email: " + tempcart.SiteCustomer.Email + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "Admin Last Name: " + tempcart.SiteCustomer.LastName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "Admin First Name: " + tempcart.SiteCustomer.FirstName + Environment.NewLine + "------------------" + Environment.NewLine;
                        body += "AdminID: " + tempcart.SiteCustomer.CustomerID + Environment.NewLine + "------------------" + Environment.NewLine;

                        mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "Admin Checkout: not logged in", body);
                    }
                    catch
                    {
                    }
                    //Response.Redirect("https://legacy.facemaster.com/Login.aspx?redir=admin/CheckOut.aspx");
                    Response.Redirect("Login.aspx?redir=CheckOut.aspx");
                }
            }
        }


        protected void buttonContinueShopping_Click(object sender, EventArgs e)
        {
            ResetShipping();
            ReCalcCart(); //save current itemdiscount & shipping settings
            Session["CScart"] = cart;
            Response.Redirect("Products.aspx");
        }


        protected void btnReCalc_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ReCalcCart();  //save current itemdiscount & shipping settings
                Session["CScart"] = cart;
                Response.Redirect("ViewCart.aspx");
            }
        }

        protected void btnDefault_Click(object sender, EventArgs e)
        {
            foreach (Item it in cart.Items)
            {
                it.LineItemAdjustment = 0;
                it.LineItemTotal = it.Quantity * (it.LineItemProduct.SalePrice - it.LineItemAdjustment);
            }

            DropDownList ddlShipping = (DropDownList)repeateritems.Controls[repeateritems.Controls.Count - 1].FindControl("ddlShipping") as DropDownList;
            ddlShipping.SelectedIndex = 0; //show ground shipping to the user
            ResetShipping();
            Session["CScart"] = cart;
            Response.Redirect("ViewCart.aspx");

        }

        protected void repeateritems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Item it = (Item)e.Item.DataItem;

                Image imgImage = (Image)e.Item.FindControl("CartImage");
                imgImage.ImageUrl = "../images/products/" + it.LineItemProduct.ThumbnailImage;
                imgImage.AlternateText = it.LineItemProduct.Name;

                Label litName = (Label)e.Item.FindControl("CartProductName");
                litName.Text = it.LineItemProduct.Name;

                Literal litProductIDlit = (Literal)e.Item.FindControl("ProductIDlit");
                litProductIDlit.Text = it.LineItemProduct.ProductID.ToString();

                DropDownList dr = (DropDownList)e.Item.FindControl("CartQuantity");
                for (int i = 1; i < 20; i++)
                    dr.Items.Add(i.ToString());

                Trace.Warn(e.Item.DataItem.ToString());
                //Trace.Warn(e.Item.);
                Trace.Warn(cart.Items.ToString());
                //dblookup to compare?
                //if(it.Quantity 
                foreach (Item i in cart.Items)
                {
                    if (i.LineItemProduct.ProductID == it.LineItemProduct.ProductID)
                    {
                        if (i.Quantity != it.Quantity)
                        {
                            i.Quantity = it.Quantity;
                            cart.CalculateTotals();
                            Session["CScart"] = cart;
                        }
                    }
                }
                dr.SelectedIndex = it.Quantity - 1;

                Trace.Warn(cart.ToString());
                Trace.Warn(Session["CScart"].ToString());

                Cart tempcart = (Cart)Session["CScart"];



                Literal litPrice = (Literal)e.Item.FindControl("Price");
                litPrice.Text = it.LineItemProduct.Price.ToString("N");

                Literal litDiscount = (Literal)e.Item.FindControl("Discount");
                decimal Discount = it.LineItemProduct.Price - it.LineItemProduct.SalePrice;
                litDiscount.Text = Discount.ToString("N");

                Literal litDeleteLink = (Literal)e.Item.FindControl("DeleteLink");
                litDeleteLink.Text = "<a href='ViewCart.aspx?Action=Delete&ProductID=" + it.LineItemProduct.ProductID + "'>Remove</a>";

                TextBox txtAdjust = (TextBox)e.Item.FindControl("Adjust");
                if (it.LineItemAdjustment == 0)
                { txtAdjust.Text = "0.00"; }
                else { txtAdjust.Text = it.LineItemAdjustment.ToString("N"); }

                Literal litLineItemTotal = (Literal)e.Item.FindControl("LineItemTotal");
                litLineItemTotal.Text = it.LineItemTotal.ToString("N");


                Trace.Warn("ItemBound");
            }
            if (e.Item.ItemType == ListItemType.Footer)
            {
                cart.CalculateTotals();

                if (cart.Items.ItemCount() == 0)
                {
                    Button btnChkOut = (Button)e.Item.FindControl("buttonCheckout");
                    btnChkOut.Visible = false;

                    Button btnDefault = (Button)e.Item.FindControl("btnDefault");
                    btnDefault.Visible = false;

                    Button btnRecalc = (Button)e.Item.FindControl("btnReCalc");
                    btnRecalc.Visible = false;

                }

                Literal litSubtotal = (Literal)e.Item.FindControl("SubTotal");
                litSubtotal.Text = cart.TotalProduct.ToString("N");

                DropDownList ddl = (DropDownList)e.Item.FindControl("ddlShipping");
                ddl.SelectedIndex = cart.ShipType - 1;

                TextBox txtShipping = (TextBox)e.Item.FindControl("Shipping");
                txtShipping.Text = cart.TotalShipping.ToString("N");


                Literal litSalesTax = (Literal)e.Item.FindControl("Tax");
                litSalesTax.Text = cart.TotalTax.ToString("N");

                Literal litTotalDiscounts = (Literal)e.Item.FindControl("TotalDiscounts");
                litTotalDiscounts.Text = cart.TotalDiscounts.ToString("N");

                Literal litTotal = (Literal)e.Item.FindControl("Total");
                litTotal.Text = cart.Total.ToString("N");
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Trace.Warn("i change");
            ResetShipping();
            DropDownList dr = (DropDownList)sender;
            Literal l = (Literal)dr.Parent.FindControl("ProductIDLit");
            //debug.Text = l.Text;

            foreach (Item i in cart.Items)
            {
                if (i.LineItemProduct.ProductID == Convert.ToInt32(l.Text))
                {
                    i.Quantity = Convert.ToInt32(dr.SelectedValue);
                    //i.LineItemTotal = i.Quantity * (i.LineItemProduct.SalePrice - i.LineItemAdjustment);
                    i.LineItemTotal = i.Quantity * (i.LineItemProduct.Price - i.LineItemDiscount);
                    cart.CalculateTotals();
                    Session["CScart"] = cart;
                    Response.Redirect("ViewCart.aspx");
                }
            }
            //foreach (Control c in dr.Parent.Controls)
            //{
            //   if(c.Ty
            //  debug.Text += ;
            //}
            //Literal ouch = (Literal)dr.Parent.Controls[0];
            //string pid = ouch.Text;
            //Trace.Warn(pid);

            Trace.Warn(dr.SelectedValue);
        }

        protected void ddlShipping_SelectedIndexChanged(object sender, EventArgs e)
        {
            Trace.Warn("shiptype change");
            DropDownList ddl = (DropDownList)repeateritems.Controls[repeateritems.Controls.Count - 1].FindControl("ddlShipping") as DropDownList;
            cart.SiteOrder.ShipMode = Convert.ToInt32(ddl.SelectedValue);
            cart.ShipType = cart.SiteOrder.ShipMode;
            cart.CalculateTotals();
            ddl.SelectedIndex = cart.ShipType - 1;

            Trace.Warn(ddl.SelectedValue);
            Session["CScart"] = cart;
            Response.Redirect("Viewcart.aspx");
        }


        protected void ReCalcCart()
        {
            int RepeaterItemIndex = 0;

            //while (RepeaterItemIndex < repeateritems.Items.Count)
            foreach (Item it in cart.Items)
            {
                if (RepeaterItemIndex < repeateritems.Items.Count)
                {
                    TextBox txtAdjust = (TextBox)repeateritems.Items[RepeaterItemIndex].FindControl("Adjust") as TextBox;
                    if (!String.IsNullOrEmpty(txtAdjust.Text))
                    {
                        if (it.LineItemAdjustment != Convert.ToDecimal(txtAdjust.Text))
                        {
                            if ((Convert.ToDecimal(txtAdjust.Text) >= 0) && (Convert.ToDecimal(txtAdjust.Text) <= it.LineItemProduct.SalePrice))
                            {
                                it.LineItemDiscount = (it.LineItemProduct.Price - it.LineItemProduct.SalePrice); //AUG 26, 2010: reset the discount
                                it.LineItemAdjustment = Convert.ToDecimal(txtAdjust.Text);
                                it.LineItemDiscount = it.LineItemDiscount + it.LineItemAdjustment;

                                //it.LineItemTotal = it.Quantity * (it.LineItemProduct.SalePrice - it.LineItemAdjustment);
                                it.LineItemTotal = it.Quantity * (it.LineItemProduct.Price - it.LineItemDiscount);
                                ResetShipping();
                            }
                        }
                        else { txtAdjust.Text = "0.00"; }
                    }

                    //Label lblHeader = Repeater1.Controls[0].FindControl("lblHeaderDisplay") as Label;
                    //Label lblFooter = repeateritems.Controls[repeateritems.Controls.Count - 1].FindControl("Shipping") as Label;
                    TextBox txtShipping = (TextBox)repeateritems.Controls[repeateritems.Controls.Count - 1].FindControl("Shipping") as TextBox;
                    if (!String.IsNullOrEmpty(txtShipping.Text))
                    {
                        Decimal shipping = Convert.ToDecimal(txtShipping.Text);
                        if ((shipping < 0) || (shipping > 100)) { cart.CSShipping = -1; }
                        else
                        {
                            //if (cart.TotalShipping > shipping) 
                            { cart.CSShipping = shipping; }
                        }
                    }

                }
                ++RepeaterItemIndex;
            }
        }

        protected void ResetShipping()
        {
            TextBox txtShipping = (TextBox)repeateritems.Controls[repeateritems.Controls.Count - 1].FindControl("Shipping") as TextBox;
            txtShipping.Text = "-1";
            cart.CSShipping = -1;
        }

        protected Item UpdateCartItem(Item item, int ssetID)
        {
            AdminCart.SiteSettings sset = new AdminCart.SiteSettings(ssetID);
            switch (sset.SiteSettingsTrigger)
            {
                case "3": //lineitem discount, dollar amount; trigger value = productID
                    if ((item.LineItemProduct.ProductID == Convert.ToInt32(sset.SiteSettingsTriggerValue)) || (Convert.ToInt32(sset.SiteSettingsTriggerValue) == 0))
                    {
                        item.LineItemAdjustment = Convert.ToDecimal(sset.SiteSettingsValue.ToString());
                        item.LineItemDiscount = item.LineItemDiscount + item.LineItemAdjustment;
                        item.LineItemTotal = item.Quantity * (item.LineItemProduct.Price - item.LineItemDiscount);
                    }
                    break;

                case "4": //lineitem discount, %-off amount; trigger value = productID
                    if ((item.LineItemProduct.ProductID == Convert.ToInt32(sset.SiteSettingsTriggerValue)) || (Convert.ToInt32(sset.SiteSettingsTriggerValue) == 0))
                    {
                        decimal percentOff = Convert.ToDecimal(sset.SiteSettingsValue.ToString());
                        item.LineItemAdjustment = item.LineItemProduct.Price * (percentOff / 100);
                        item.LineItemDiscount = item.LineItemDiscount + item.LineItemAdjustment;
                        item.LineItemTotal = item.Quantity * (item.LineItemProduct.Price - item.LineItemDiscount);
                    }
                    break;

                default: //do nothing
                    break;
            }
            return item;
        }

    } 
}
