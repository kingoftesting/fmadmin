<%@ Page Language="C#" MasterPageFile="~/Admin/MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="Admin_Voidtrans" Title="Void Transaction" Codebehind="Voidtrans.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

   <div class="sectiontitle">Void Original Transaction</div>
   <p></p>

    <table style="width:90%">
        <tr>
            <td class = "custservice" align="left">
                OrderID: &nbsp<asp:Label id="lblOrderID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class = "custservice" >
                Transaction:
                <br />
                <asp:Literal ID="litOrigTran" runat="server"></asp:Literal>
                <div class="simpleClear"></div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblErrMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class = "custservice" >
                <asp:Panel ID="pnlTran" runat="server">
                   <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" Text="Submit Void" />
                   <div class="simpleClear"></div>
                   <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" CausesValidation="false" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class = "custservice" >
                <asp:Panel ID="pnlNoTran" runat="server">
                    <div class="sectiontitle">You May Only Void A Transaction If It Hasn't Already Been Batched-Out Or Porcessed by the credit card provider</div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    
</asp:Content>

