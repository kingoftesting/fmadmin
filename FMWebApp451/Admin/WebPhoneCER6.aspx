<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="Admin_WebPhoneCER6" Title="Daily Web/Phone Sales" Codebehind="WebPhoneCER6.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="RDatePicker" Src="../controls/RDatePicker.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="alpha omega sixteen columns">

<div class="sectiontitle" style="text-align:center">FaceMaster Daily Sales Reports</div>
   <p></p>
    
   <table style="border:solid 1px black; width:100%">
   <tr>
       <td align="left" width="100%">

          <div style="float:left; padding-top:5px">
                <div style ="float:left; height: 25px; padding-left:10px; border-right:solid 1px grey">
                    <asp:LinkButton ID="lnkbtnToday" OnClick="lnkbtnToday_Click" OnClientClick="aspnetForm.target ='_self'" runat="server">Today</asp:LinkButton>
                    &nbsp; &nbsp; 
                    <asp:LinkButton ID="lnkbtnYesterday" OnClick="lnkbtnYesterday_Click" OnClientClick="aspnetForm.target ='_self'" runat="server">Yesterday</asp:LinkButton>
                    &nbsp; &nbsp; 
                    <asp:LinkButton ID="lnkbtnMTD" OnClick="lnkbtnMTD_Click" OnClientClick="aspnetForm.target ='_self'" runat="server">MTD</asp:LinkButton>
                    &nbsp; &nbsp; 
                    <asp:LinkButton ID="lnkbtnYTD" OnClick="lnkbtnYTD_Click" OnClientClick="aspnetForm.target ='_self'" runat="server">YTD</asp:LinkButton>
                    &nbsp; &nbsp;
                </div>

                <div style ="float:left; padding-left:10px;">
                    <div class="groupHTML">
                        <asp:Label ID="Label2" style="vertical-align:middle" Text="Start:&nbsp;" runat="server"></asp:Label> 
                        <uc1:RDatePicker id="RDatePicker1" runat="server"></uc1:RDatePicker>
                    </div>
                </div>

                 <div style ="float:left; padding-left:10px;">
                   <div class="groupHTML">
                        <asp:Label ID="Label3" style="vertical-align:middle" Text="End:&nbsp;" runat="server"></asp:Label>  
                        <uc1:RDatePicker id="RDatePicker2" runat="server"></uc1:RDatePicker>
                        <asp:Label ID="Label4" style="vertical-align:middle" Text="&nbsp;&nbsp;" runat="server"></asp:Label> 
                   </div>
                 </div>

                 <div style ="float:left;">
                        <asp:Label ID="Label5" style="vertical-align:middle" Text="&nbsp;&nbsp;&nbsp;&nbsp;" runat="server"></asp:Label>  
                        <asp:Button ID="btnListByDate" runat="server" Text="Search" OnClick="btnListByDate_Click" OnClientClick="aspnetForm.target ='_self'" ValidationGroup="ListByDate" />
                        <asp:Label ID="Label6" style="vertical-align:middle" Text="&nbsp;&nbsp;" runat="server"></asp:Label>  
                 </div>


            <div style="float:left; padding-right:0%;">
                    <div class="groupHTML">  
                        <asp:Label ID="Label1" style="vertical-align:middle" Text="Comparison:&nbsp;" runat="server"></asp:Label> 
                        <asp:RadioButtonList ID="radlstCompare" CssClass="groupHTML" AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                        <asp:ListItem Value="0" Text="none" Selected="true" ></asp:ListItem>
                        <asp:ListItem Value="1" Text="MOM"></asp:ListItem>
                        <asp:ListItem Value="2" Text="YOY"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
            </div>


        </div>

        <div style="clear:both">          
            <asp:Label runat="server" ID="lblFeedbackOK" Text="Your message has been successfully sent." SkinID="FeedbackOK" Visible="false" />
            <asp:Label runat="server" ID="lblFeedbackKO" Text="Sorry, there was a problem sending your message." SkinID="FeedbackKO" Visible="false" />
        </div>

       </td>
   </tr>
   </table>
    
<div id="divStd" style="float:left; border-right:solid 1px grey; width:47%">    
    <table>
	    <tr>
	        <td colspan="3" class="custservice" align="left" style="border-style:none;">
	           <div style="float:left">
	                Start Date:&nbsp<asp:Literal ID="litStartDate" runat="server" />
	           </div>
	           <div style="float:right; padding-left:50px">
                   <asp:LinkButton ID="lnkbtnGetCSV" OnClick="btnCSV_Click" OnClientClick="aspnetForm.target ='_self'" Visible="false" runat="server">Spreadsheet</asp:LinkButton>
	               &nbsp;&nbsp;&nbsp;&nbsp;
                   <asp:LinkButton ID="lnkbtnSendEmail" OnClick="btnSendEmail_Click" OnClientClick="aspnetForm.target ='_self'" Visible="false" runat="server">Email</asp:LinkButton>
               </div>
            </td>
        </tr>
        <tr>
	        <td colspan="3" class="custservice" align="left" style="border-style:none">
	            End Date:&nbsp<asp:Literal ID="litEndDate" runat="server" />
            </td>
	    </tr>
        <tr><td colspan="3" class="custservice" align="left" style="border-style:none"></td></tr>

	    <tr><td class="custservice" align="right" style="border-style:none;">Total Orders</td><td colspan="2" class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalOrders" runat="server" /></td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Total Units</td><td colspan="2" class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalUnits" runat="server"/></td></tr>

        <tr><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td>&nbsp</td></tr>

        <tr>
            <td class="custservice" align="right" style="border-style:none;">Gross Revenue</td>
            <td class="custservice" align="right" style="border-style:none;">
                <asp:Literal ID="litAdjTotalGrossRev" runat="server" />
            </td>        
        </tr>
        <tr>
            <td class="custservice" align="right" style="border-style:none;">Adj Gross Revenue</td>
            <td class="custservice" align="right" style="border-style:none;">
                <asp:Literal ID="litTotalGrossRev" runat="server" />
            </td>
            <td>
                <asp:Panel ID="pnlGrossMismatch" Visible="false" runat="server">
                    <span style="color:red">&nbsp;GrossRevenue Mismatch:&nbsp;</span>
                    <asp:LinkButton ID="lnkBtnGrossMismatch" CssClass="custservice" OnClick="lnkBtnGrossMisMatch_Click" OnClientClick="aspnetForm.target ='_blank'" Text="$0.00" runat="server"><span class="custservice"></span></asp:LinkButton>               
                </asp:Panel>
            </td>
        </tr>

        <tr><td class="custservice" align="right" style="border-style:none;">+(Other CS)</td>
            <td class="custservice" align="right" style="border-style:none;">
                <asp:Literal ID="litTotalOther" Visible="true" runat="server" />
                <asp:LinkButton ID="lnkBtnTotalOther" CssClass="custservice" Visible="false" OnClick="lnkBtnTotalOther_Click" OnClientClick="aspnetForm.target ='_blank'" Text="$0.01" runat="server"><span class="custservice"></span></asp:LinkButton>
            </td>
            <td>&nbsp</td>
        </tr>
        <tr><td class="custservice" align="right" style="border-style:none;">-(CS LineItem Discounts)</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalDiscounts" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">-(Shopify Promo Codes)</td>
            <td class="custservice" align="right" style="border-style:none;">
                <asp:Literal ID="litTotalCoupon" Visible="true" runat="server" />
                <asp:LinkButton ID="lnkbtnTotalCoupon" CssClass="custservice" Visible="false" OnClick="lnkbtnTotalCoupon_Click" OnClientClick="aspnetForm.target ='_blank'" Text="$0.01" runat="server"><span class="custservice"></span></asp:LinkButton>
            </td>
            <td>&nbsp</td>
        </tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Net Revenue</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalNet" runat="server" /></td><td>&nbsp</td></tr>

        <tr><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td>&nbsp</td></tr>

        <tr><td class="custservice" align="right" style="border-style:none;">Total Cost</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalCost" runat="server"/></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Margin</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalMargin" runat="server"/></td><td>&nbsp</td></tr>
        
        <tr><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td>&nbsp</td></tr>

        <tr><td class="custservice" align="right" style="border-style:none;">Sales Tax</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalTax" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Shipping</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalShipping" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Refunds</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalRefunds" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Voids</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalVoids" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Collected(orders)</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalSiteCollected" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Collected(transactions)</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalCollected" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">----------</td><td class="custservice" align="right" style="border-style:none;">----------</td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Multi-Pay Revenue</td><td class="custservice" align="right" style="border-style:none;"></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;"><asp:Label ID="lblMMminus1" runat="server" /></td><td class="custservice" align="right" style="border-style:none;"><asp:Label ID="lblccMMminus1" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;"><asp:Label ID="lblMMminus2" runat="server" /></td><td class="custservice" align="right" style="border-style:none;"><asp:Label ID="lblccMMminus2" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;"><asp:Label ID="lblMMminus3" runat="server" /></td><td class="custservice" align="right" style="border-style:none;"><asp:Label ID="lblccMMminus3" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">----------</td><td class="custservice" align="right" style="border-style:none;">----------</td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;"><asp:Label ID="lblChkCCDiffLabel" runat="server" />Collected Check:</td><td class="custservice" align="right" style="border-style:none;"><asp:Label ID="lblChkCCDiff" runat="server" /></td><td>&nbsp</td></tr>
    </table>
</div>

<asp:Panel id="pnlComp" style="float:right; width:47%; padding-left:20px" visible="false" runat="server">    
    <table>
	    <tr>
	        <td colspan="3" class="custservice" align="left" style="border-style:none;">Start Date:&nbsp<asp:Literal ID="litStartDateComp" runat="server" /></td>
        </tr>
        <tr>
	        <td colspan="3" class="custservice" align="left" style="border-style:none">End Date:&nbsp<asp:Literal ID="litEndDateComp" runat="server" /></td>
	    </tr>
        <tr><td colspan="3" class="custservice" align="left" style="border-style:none"></td></tr>

	    <tr><td class="custservice" align="right" style="border-style:none;">Total Orders</td><td class="custservice" align="right" style="border-style:none; width:95px"><asp:Literal ID="litTotalOrdersComp" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Total Units</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalUnitsComp" runat="server"/></td><td>&nbsp</td></tr>

        <tr><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td>&nbsp</td></tr>

        <tr>
            <td class="custservice" align="right" style="border-style:none;">Gross Revenue</td>
            <td class="custservice" align="right" style="border-style:none;">
                <asp:Literal ID="litTotalGrossRevComp" runat="server" />
            </td>
            <td>
                <asp:Panel ID="pnlGMMComp" Visible="false" runat="server">
                    <span style="color:red">&nbsp;GrossRevenue Mismatch:&nbsp;</span>
                    <asp:LinkButton ID="lnkBtnGMMComp" CssClass="custservice" OnClick="lnkBtnGrossMisMatchComp_Click" OnClientClick="aspnetForm.target ='_blank'" Text="$0.00" runat="server"><span class="custservice"></span></asp:LinkButton>               
                </asp:Panel>
            </td>
        </tr>

        <tr><td class="custservice" align="right" style="border-style:none;">+(Other CS)</td>
            <td class="custservice" align="right" style="border-style:none;">
                <asp:Literal ID="litTotalOtherComp" Visible="true" runat="server" />
                <asp:LinkButton ID="lnkBtnTotalOtherComp" CssClass="custservice" Visible="false" OnClick="lnkBtnTotalOtherComp_Click" OnClientClick="aspnetForm.target ='_blank'" Text="$0.01" runat="server"><span class="custservice"></span></asp:LinkButton>
            </td>
            <td>&nbsp</td>
        </tr>
        <tr><td class="custservice" align="right" style="border-style:none;">-(LineItem Discounts)</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalDiscountsComp" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">-(Order Discounts/Coupons)</td>
            <td class="custservice" align="right" style="border-style:none;">
                <asp:Literal ID="litTotalCouponComp" Visible="true" runat="server" />
                <asp:LinkButton ID="lnkbtnTotalCouponComp" CssClass="custservice" Visible="false" OnClick="lnkbtnTotalCouponComp_Click" OnClientClick="aspnetForm.target ='_blank'" Text="$0.01" runat="server"><span class="custservice"></span></asp:LinkButton>
            </td>
            <td>&nbsp</td>
        </tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Net Revenue</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalNetComp" runat="server" /></td><td>&nbsp</td></tr>

        <tr><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td>&nbsp</td></tr>

        <tr><td class="custservice" align="right" style="border-style:none;">Total Cost</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalCostComp" runat="server"/></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Margin</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalMarginComp" runat="server"/></td><td>&nbsp</td></tr>
        
        <tr><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td class="custservice" align="right" style="border-style:none;">&nbsp</td><td>&nbsp</td></tr>

        <tr><td class="custservice" align="right" style="border-style:none;">Sales Tax</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalTaxComp" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Shipping</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalShippingComp" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Refunds</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalRefundsComp" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Voids</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalVoidsComp" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Collected(website)</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalSiteCollectedComp" runat="server" /></td><td>&nbsp</td></tr>
        <tr><td class="custservice" align="right" style="border-style:none;">Collected(CC)</td><td class="custservice" align="right" style="border-style:none;"><asp:Literal ID="litTotalCollectedComp" runat="server" /></td><td>&nbsp</td></tr>
    </table>
</asp:Panel>

<div id="divRptr" style="clear:both"></div>
    <p></p>
    Sales Details:
   
   	<asp:Repeater ID="repeateritems" runat="server" EnableViewState="false" OnItemDataBound="repeateritems_ItemDataBound"  >
	
	<HeaderTemplate>
	<table width="100%" cellpadding='5' rules='all' border='1' style='border-width:0px;border-collapse:collapse;' >

	<tr align='Center'>
		<td class="rptHdrRow">SKU</td>
		<td class="rptHdrRow">Description</td>
        <td class="rptHdrRow">UnitCost</td>
        <td class="rptHdrRow">Units</td>
        <td class="rptHdrRow">Gross</td>
        <td class="rptHdrRow">Discount</td>
        <td class="rptHdrRow">Cost</td>
        <td class="rptHdrRow">phUnits</td>
        <td class="rptHdrRow">phGross</td>
        <td class="rptHdrRow">phDiscount</td>
	</tr>
	</HeaderTemplate>
	
	<ItemTemplate>
    <tr>
        <td class="rptRow" align='Right'><asp:Literal ID="SKU" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="Description" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="UnitCost" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="Units" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="Gross" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="Discount" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="Cost" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="phUnits" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="phGross" runat="server" /></td>
        <td class="rptRow" align='Right'><asp:Literal ID="phDiscount" runat="server" /></td>
	</tr>
	</ItemTemplate>
	
	<AlternatingItemTemplate>
    <tr>
        <td class="rptAltRow" align='Right'><asp:Literal ID="SKU" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="Description" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="UnitCost" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="Units" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="Gross" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="Discount" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="Cost" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="phUnits" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="phGross" runat="server" /></td>
        <td class="rptAltRow" align='Right'><asp:Literal ID="phDiscount" runat="server" /></td>
	</tr>
	</AlternatingItemTemplate>

	
	<FooterTemplate>
	    <tr><td colspan="10" class="custservice"> <hr /> </td></tr>

	</tr>
	</table>
	</FooterTemplate>
    </asp:Repeater>

  
</div>
</asp:Content>
