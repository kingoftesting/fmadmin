using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using AdminCart;
using FMmetrics;
using System.Linq;

public partial class Admin_WebPhoneCER6 : AdminBasePage //System.Web.UI.Page
{
    public class Summary
    {
        #region Members
        private int totalOrders;
        private int totalUnits;
        private decimal totalGrossRev;
        private decimal adjTotalGrossRev;
        private decimal totalCost;
        private decimal totalDiscounts;
        private decimal totalCoupon;
        private decimal totalOther;
        private decimal totalNet;
        private decimal totalTax;
        private decimal totalShipping;
        private decimal totalRefunds;
        private decimal totalVoids;
        private decimal totalSiteCollected; //calculated from income statement
        private decimal totalCollected; //calculated by adding transaction amounts
        #endregion

        #region Properties
        public int TotalOrders { get { return totalOrders; } set { totalOrders = value; } }
        public int TotalUnits { get { return totalUnits; } set { totalUnits = value; } }
        public decimal TotalGrossRev { get { return totalGrossRev; } set { totalGrossRev = value; } }
        public decimal AdjTotalGrossRev { get { return adjTotalGrossRev; } set { adjTotalGrossRev = value; } }
        public decimal TotalCost { get { return totalCost; } set { totalCost = value; } }
        public decimal TotalDiscounts { get { return totalDiscounts; } set { totalDiscounts = value; } }
        public decimal TotalCoupon { get { return totalCoupon; } set { totalCoupon = value; } }
        public decimal TotalOther { get { return totalOther; } set { totalOther = value; } }
        public decimal TotalNet { get { return totalNet; } set { totalNet = value; } }
        public decimal TotalTax { get { return totalTax; } set { totalTax = value; } }
        public decimal TotalShipping { get { return totalShipping; } set { totalShipping = value; } }
        public decimal TotalRefunds { get { return totalRefunds; } set { totalRefunds = value; } }
        public decimal TotalVoids { get { return totalVoids; } set { totalVoids = value; } }
        public decimal TotalSiteCollected { get { return totalSiteCollected; } set { totalSiteCollected = value; } }
        public decimal TotalCollected { get { return totalCollected; } set { totalCollected = value; } }
        #endregion

        public Summary()
        {

            TotalOrders = 0;
            TotalUnits = 0;
            TotalGrossRev = 0;
            AdjTotalGrossRev = 0;
            TotalCost = 0;
            TotalDiscounts = 0;
            TotalCoupon = 0;
            TotalOther = 0;
            TotalNet = 0;
            TotalTax = 0;
            TotalShipping = 0;
            TotalRefunds = 0;
            TotalVoids = 0;
            TotalSiteCollected = 0;
            TotalCollected = 0;
        }

        public void Load(object s)
        {
            //Get Summary From session data
            //this = s;
            Summary tempS = (Summary)s;

            this.TotalOrders = tempS.TotalOrders;
            this.TotalUnits = tempS.TotalUnits;
            this.TotalGrossRev = tempS.TotalGrossRev;
            this.TotalGrossRev = tempS.AdjTotalGrossRev;
            this.TotalCost = tempS.TotalCost;
            this.TotalDiscounts = tempS.TotalDiscounts;
            this.TotalCoupon = tempS.TotalCoupon;
            this.TotalOther = tempS.TotalOther;
            this.TotalNet = tempS.TotalNet;
            this.TotalTax = tempS.TotalTax;
            this.TotalShipping = tempS.TotalShipping;
            this.TotalRefunds = tempS.TotalRefunds;
            this.TotalVoids = tempS.TotalVoids;
            this.TotalSiteCollected = tempS.TotalSiteCollected;
            this.TotalCollected = tempS.TotalCollected;
        }
    }
    
    DataView websales = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Define the name and type of the client scripts on the page.
        String csname1 = "fixform"; //so that only the button controls spec'd generate new windows
        Type cstype = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;

        // Check to see if the startup script is already registered.
        if (!cs.IsStartupScriptRegistered(cstype, csname1))
        {
            StringBuilder cstext1 = new StringBuilder();
            //cstext1.Append("<script type=text/javascript> alert('Hello World!') </");
            //cstext1.Append("script>");

            cstext1.Append("<script type='text/javascript'>");
            cstext1.Append("function fixform() { ");
            cstext1.Append("if (opener.document.getElementById('aspnetForm').target != '_blank') return; ");
            cstext1.Append("opener.document.getElementById('aspnetForm').target = ''; ");
            cstext1.Append("opener.document.getElementById('aspnetForm').action = opener.location.href; ");
            cstext1.Append("} </script>");

            cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
        }

        if (!this.IsPostBack)
        {
            Certnet.Cart cart = new Certnet.Cart();
            cart.Load(Session["cart"]);
            int customerID = cart.SiteCustomer.CustomerID;
            try
            {
                if (ValidateUtil.IsInRole(customerID, "ADMIN") || (ValidateUtil.IsInRole(customerID, "REPORTS")) || (ValidateUtil.IsInRole(customerID, "MARKETING")))
                { }
                else
                {
                    throw new SecurityException("You do not have the apppropriate creditentials to complete this task.");
                }
            }
            catch (SecurityException ex)
            {
                string msg = "Error in WebPhoneCER" + Environment.NewLine;
                msg += "User did not have permission to access page" + Environment.NewLine;
                msg += "CustomerID=" + customerID.ToString() + Environment.NewLine;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                Response.Redirect("../Login.aspx");
            }


            List<int> list = ShopifyAgent.ShopifyAgent.SyncShopify(); //sync orders from Shopify

            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                          23, 59, 59);

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.ToShortDateString();
            RDatePicker1.SelectedDate = time1.Subtract(new TimeSpan(0, 23, 59, 59)).ToShortDateString();
            BindWebSales();

            }
        else
        {
            Control ctl = null;
            string ctlname = Page.Request.Params["__EVENTTARGET"];
            if (!string.IsNullOrEmpty(ctlname)) // if __EVENTTARGET is null, the control is a button type
            { 
                ctl = Page.FindControl(ctlname); 
                if (ctl.GetType() == typeof(RadioButtonList)) 
                { 
                    BindWebSales();
                }
            }       
        }
        if (radlstCompare.SelectedValue == "0") { pnlComp.Visible = false; }
        else { pnlComp.Visible = true; }
    }

        private void BindWebSales()
        {
            Session["GrossReconcile"] = 0; //clear gross revenue mismatch placeholder
            pnlGrossMismatch.Visible = false;

            FillWebSales();
            websales.Sort = "GrossRevenue" + " " + "DESC";
            Session["grvDataSource"] = websales; //save for later; used to Sort, CSV download, Email
            repeateritems.DataSource = websales;
            repeateritems.DataBind();

            Summary s = new Summary();
            //s.Load(Session["Summary"]);
            if (Session["Summary"] != null)
            { s = (Summary)Session["Summary"]; }
            else { s.TotalOrders = -1; }

            litStartDate.Text = RDatePicker1.SelectedDate + " 12:00 AM";
            litEndDate.Text = RDatePicker2.SelectedDate + " 12:00 PM";
            litTotalOrders.Text = s.TotalOrders.ToString();
            litTotalUnits.Text = s.TotalUnits.ToString();
            litAdjTotalGrossRev.Text = s.AdjTotalGrossRev.ToString("C");
            litTotalGrossRev.Text = s.TotalGrossRev.ToString("C");
            litTotalDiscounts.Text = s.TotalDiscounts.ToString("C");

            if (s.TotalCoupon == 0)
            {
                litTotalCoupon.Visible = true;
                lnkbtnTotalCoupon.Visible = false;
                litTotalCoupon.Text = s.TotalCoupon.ToString("C");
            }
            else
            {
                litTotalCoupon.Visible = false;
                lnkbtnTotalCoupon.Visible = true;
                lnkbtnTotalCoupon.Text = s.TotalCoupon.ToString("C");
            }

            litTotalCoupon.Text = s.TotalCoupon.ToString("C");
            //litTotalOther.Text = s.TotalOther.ToString("C");
            litTotalNet.Text = s.TotalNet.ToString("C");
            litTotalCost.Text = s.TotalCost.ToString("C");
            decimal TotalMargin = s.TotalNet - s.TotalCost;
            litTotalMargin.Text = TotalMargin.ToString("C");
            litTotalTax.Text = s.TotalTax.ToString("C");
            litTotalShipping.Text = s.TotalShipping.ToString("C");
            litTotalRefunds.Text = s.TotalRefunds.ToString("C");
            litTotalVoids.Text = s.TotalVoids.ToString("C");
            litTotalSiteCollected.Text = s.TotalSiteCollected.ToString("C");
            litTotalCollected.Text = s.TotalCollected.ToString("C");

            //DateTime startDate = Convert.ToDateTime(litStartDate.Text);
            //DateTime endDate = Convert.ToDateTime(litEndDate.Text);
            DateTime startDate = (!string.IsNullOrEmpty(RDatePicker1.SelectedDate)) ? Convert.ToDateTime(RDatePicker1.SelectedDate + " 12:00:00AM") : DateTime.Now;
            DateTime endDate = (!string.IsNullOrEmpty(RDatePicker2.SelectedDate)) ? Convert.ToDateTime(RDatePicker2.SelectedDate + " 11:59:59PM") : DateTime.Now;

            List<FMWebApp451.ViewModels.MultiPayTransactionsByDate> tList = new List<FMWebApp451.ViewModels.MultiPayTransactionsByDate>();
            tList = FMWebApp451.ViewModels.MultiPayTransactionsByDate.GetList(startDate, endDate);


            List<FMWebApp451.ViewModels.MultiPayTransactionsByDate> newtList = (from t in tList
                                                                                where t.OrderDate.Month == endDate.AddMonths(-1).Month
                                                                                select t).ToList();

            decimal ccMonthMinus1 = (from t in tList
                                     where t.OrderDate.Month == endDate.AddMonths(-1).Month
                                     select t.TransactionAmount).Sum();
            lblccMMminus1.Text = ccMonthMinus1.ToString("C");
            lblMMminus1.Text = "Orders from: " + Utilities.FMHelpers.MonthLookUp(endDate.AddMonths(-1).Month);

            newtList = (from t in tList
                        where t.OrderDate.Month == endDate.AddMonths(-2).Month
                        select t).ToList();
            decimal ccMonthMinus2 = (from t in tList
                                     where t.OrderDate.Month == endDate.AddMonths(-2).Month
                                     select t.TransactionAmount).Sum();
            lblccMMminus2.Text = ccMonthMinus2.ToString("C");
            lblMMminus2.Text = "Orders from: " + Utilities.FMHelpers.MonthLookUp(endDate.AddMonths(-2).Month);

            newtList = (from t in tList
                        where t.OrderDate.Month == endDate.AddMonths(-3).Month
                        select t).ToList();
            decimal ccMonthMinus3 = (from t in tList
                                     where t.OrderDate.Month == endDate.AddMonths(-3).Month
                                     select t.TransactionAmount).Sum();
            lblccMMminus3.Text = ccMonthMinus3.ToString("C");
            lblMMminus3.Text = "Orders from: " + Utilities.FMHelpers.MonthLookUp(endDate.AddMonths(-3).Month);

            decimal collectedCheck = (s.TotalSiteCollected + ccMonthMinus1 + ccMonthMinus2 + ccMonthMinus3) - s.TotalCollected;
            lblChkCCDiff.Text = collectedCheck.ToString("C");

            if (s.TotalOther == 0)
            {
                litTotalOther.Visible = true;
                lnkBtnTotalOther.Visible = false;
                litTotalOther.Text = s.TotalOther.ToString("C");
            }
            else
            {
                litTotalOther.Visible = false;
                lnkBtnTotalOther.Visible = true;
                lnkBtnTotalOther.Text = s.TotalOther.ToString("C");
            }

            if (Session["SummaryComp"] != null)
            {
                //s.Load(Session["SummaryComp"]);
                s = (Summary)Session["SummaryComp"];

                litStartDateComp.Text = Session["startDateComp"].ToString();
                litEndDateComp.Text = Session["endDateComp"].ToString();
                litTotalOrdersComp.Text = s.TotalOrders.ToString();
                litTotalUnitsComp.Text = s.TotalUnits.ToString();
                litTotalGrossRevComp.Text = s.TotalGrossRev.ToString("C");
                litTotalDiscountsComp.Text = s.TotalDiscounts.ToString("C");

                if (s.TotalCoupon == 0)
                {
                    litTotalCouponComp.Visible = true;
                    lnkbtnTotalCouponComp.Visible = false;
                    litTotalCouponComp.Text = s.TotalCoupon.ToString("C");
                }
                else
                {
                    litTotalCouponComp.Visible = false;
                    lnkbtnTotalCouponComp.Visible = true;
                    lnkbtnTotalCouponComp.Text = s.TotalCoupon.ToString("C");
                }

                litTotalCouponComp.Text = s.TotalCoupon.ToString("C");
                //litTotalOther.Text = s.TotalOther.ToString("C");
                litTotalNetComp.Text = s.TotalNet.ToString("C");
                litTotalCostComp.Text = s.TotalCost.ToString("C");
                TotalMargin = s.TotalNet - s.TotalCost;
                litTotalMarginComp.Text = TotalMargin.ToString("C");
                litTotalTaxComp.Text = s.TotalTax.ToString("C");
                litTotalShippingComp.Text = s.TotalShipping.ToString("C");
                litTotalRefundsComp.Text = s.TotalRefunds.ToString("C");
                litTotalVoidsComp.Text = s.TotalVoids.ToString("C");
                litTotalSiteCollectedComp.Text = s.TotalSiteCollected.ToString("C");
                litTotalCollectedComp.Text = s.TotalCollected.ToString("C");

                if (s.TotalOther == 0)
                {
                    litTotalOtherComp.Visible = true;
                    lnkBtnTotalOtherComp.Visible = false;
                    litTotalOtherComp.Text = s.TotalOther.ToString("C");
                }
                else
                {
                    litTotalOtherComp.Visible = false;
                    lnkBtnTotalOtherComp.Visible = true;
                    lnkBtnTotalOtherComp.Text = s.TotalOther.ToString("C");
                }
            }

            if (websales.Table.Rows.Count == 0) //hide Email & CSV buttons if empty table
            {
                lnkbtnGetCSV.Visible = false;
                lnkbtnSendEmail.Visible = false;
            }
            else
            {
                string FromDate = "";
                if (!string.IsNullOrEmpty(RDatePicker1.SelectedDate)) { FromDate = RDatePicker1.SelectedDate + " 12:00AM"; }
                else { FromDate = "1/1/1900"; }

                string ToDate = "";
                if (!string.IsNullOrEmpty(RDatePicker2.SelectedDate)) { ToDate = RDatePicker2.SelectedDate + " 11:59PM"; }
                else { ToDate = DateTime.Now.ToString(); }
                lnkbtnGetCSV.Visible = true;
                lnkbtnSendEmail.Visible = true;
            }

        }
        
        protected void btnListByDate_Click(object sender, EventArgs e)
        {

            lblFeedbackOK.Visible = false;
            lblFeedbackKO.Visible = false;

            if (Page.IsValid)
            {
                BindWebSales();
            }
        }

        protected void lnkbtnToday_Click(object sender, EventArgs e)
        {

            lblFeedbackOK.Visible = false;
            lblFeedbackKO.Visible = false;

            if (Page.IsValid)
            {
                DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                              0, 0, 0);

                //#1 = From: Date; #2 = To: Date 
                RDatePicker2.SelectedDate = time1.Add(new TimeSpan(0, 23, 59, 59)).ToShortDateString();
                RDatePicker1.SelectedDate = time1.ToShortDateString();

                BindWebSales();
            }
        }

        protected void lnkbtnYesterday_Click(object sender, EventArgs e)
        {

        lblFeedbackOK.Visible = false;
        lblFeedbackKO.Visible = false;

        if (Page.IsValid)
        {
            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                          23, 59, 59);
            time1 = time1.Subtract(new TimeSpan(1, 23, 59, 59)); //get yesterday's date

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.Add(new TimeSpan(0, 23, 59, 59)).ToShortDateString();
            RDatePicker1.SelectedDate = time1.ToShortDateString();

            BindWebSales();
        }
    }

    protected void lnkbtnMTD_Click(object sender, EventArgs e)
    {

        lblFeedbackOK.Visible = false;
        lblFeedbackKO.Visible = false;

        if (Page.IsValid)
        {
            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                          23, 59, 59);

            DateTime time2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1,
                                          0, 0, 0);

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.ToShortDateString();
            RDatePicker1.SelectedDate = time2.ToShortDateString();

            BindWebSales();
        }
    }

    protected void lnkbtnYTD_Click(object sender, EventArgs e)
    {

        lblFeedbackOK.Visible = false;
        lblFeedbackKO.Visible = false;

        if (Page.IsValid)
        {
            DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                          23, 59, 59);

            DateTime time2 = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);

            //#1 = From: Date; #2 = To: Date 
            RDatePicker2.SelectedDate = time1.ToShortDateString();
            RDatePicker1.SelectedDate = time2.ToShortDateString();

            BindWebSales();
        }
    }

        protected void btnCSV_Click(object sender, EventArgs e)
        {
            //FillWebSales();
            websales = Session["grvDataSource"] as DataView;
            websales.Sort = "GrossRevenue DESC";
            //Helpers.SendCSV(websales);
            Utilities.FMHelpers.SendCSV(websales);
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {

            //Need to write the code for email delivery!

            //FillWebSales();
            websales = Session["grvDataSource"] as DataView;
            websales.Sort = "GrossRevenue DESC";

            Certnet.Cart cart = new Certnet.Cart();
            cart.Load(Session["cart"]);
            int customerID = cart.SiteCustomer.CustomerID;
            string contactTo = cart.SiteCustomer.Email;
            string contactFrom = AdminCart.Config.MailFrom();

            //SqlDataReader dr = Customer.GetCustomerData(Convert.ToInt32(Session["CustomerID"]));
            String contactRawCC = "";
            //if (dr.Read()) { contactRawCC = dr["SampleEmailCC"].ToString(); }
            string contactName = "";
            string contactSubject = "FM REPORTS: Sales Update";

            string contactBody = "Time Period: ";
            contactBody += RDatePicker1.SelectedDate + " 12:00:00 AM";
            contactBody += " to ";
            contactBody += RDatePicker2.SelectedDate + " 11:59:59 PM";
            contactBody += Environment.NewLine + Environment.NewLine;
            contactBody = Utilities.FMHelpers.ConvertToHtml(contactBody);

            Summary s = new Summary();
            s.Load(Session["Summary"]);

            contactBody += "<br />";
            contactBody += "<table>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Total Orders</td><td class='custservice' align='right' style='border-style:none; width=75px'>" + s.TotalOrders.ToString() + "</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Total Units</td><td class='custservice' align='right' style='border-style:none;'>" + s.TotalUnits.ToString() + "</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>&nbsp</td><td class='custservice' align='right' style='border-style:none;'>&nbsp</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Gross Revenue</td><td class='custservice' align='right' style='border-style:none;'>" + s.TotalGrossRev.ToString("C") + "</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Discounts</td><td class='custservice' align='right' style='border-style:none;'>" + s.TotalDiscounts.ToString("C") + "</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Coupons</td><td class='custservice' align='right' style='border-style:none;'>" + s.TotalCoupon.ToString("C") + "</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Net Revenue</td><td class='custservice' align='right' style='border-style:none;'>" + s.TotalNet.ToString("C") + "</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>&nbsp</td><td class='custservice' align='right' style='border-style:none;'>&nbsp</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Total Cost</td><td class='custservice' align='right' style='border-style:none;'>" + s.TotalCost.ToString("C") + "</td></tr>";
            decimal TotalMargin = s.TotalNet - s.TotalCost;
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Margin</td><td class='custservice' align='right' style='border-style:none;'>" + TotalMargin.ToString("C") + "</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>&nbsp</td><td class='custservice' align='right' style='border-style:none;'>&nbsp</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Sales Tax</td><td class='custservice' align='right' style='border-style:none;'>" + s.TotalTax.ToString("C") + "</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Shipping</td><td class='custservice' align='right' style='border-style:none;'>" + s.TotalShipping.ToString("C") + "</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Refunds</td><td class='custservice' align='right' style='border-style:none;'>" + s.TotalRefunds.ToString("C") + "</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Voids</td><td class='custservice' align='right' style='border-style:none;'>" + s.TotalVoids.ToString("C") + "</td></tr>";
            contactBody += "<tr><td class='custservice' align='right' style='border-style:none;'>Collected</td><td class='custservice' align='right' style='border-style:none;'>" + s.TotalCollected.ToString("C") + "</td></tr>";
            contactBody += "</table>";

            contactBody += "<br /><br />Sales Details:";

            contactBody += Utilities.FMHelpers.ConvertDataViewToHTML(websales, "Cost");

            if (mvc_Mail.SendMail(contactTo, contactFrom, contactName, contactRawCC, contactSubject, contactBody, true))
            {
                // show a confirmation message, and reset the fields
                lblFeedbackOK.Visible = true;
                lblFeedbackKO.Visible = false;
            }
            else
            {
                //otherwise let the customer know it didn't work
                lblFeedbackOK.Visible = false;
                lblFeedbackKO.Visible = true;
            }

            lnkbtnGetCSV.Visible = false;
            lnkbtnSendEmail.Visible = false;

        }

        protected void FillWebSales()
        {
            string FromDate = "";
            if (!string.IsNullOrEmpty(RDatePicker1.SelectedDate)) { FromDate = RDatePicker1.SelectedDate + " 12:00:00AM"; }
            else { FromDate = DateTime.Now.ToString(); }

            string ToDate = "";
            if (!string.IsNullOrEmpty(RDatePicker2.SelectedDate)) { ToDate = RDatePicker2.SelectedDate + " 11:59:59PM"; }
            else { ToDate = DateTime.Now.ToString(); }


            websales = FindAllWebSales(FromDate, ToDate);
        }

    private DataView FindAllWebSales(string fromDate, string toDate)
    {
        Summary s = new Summary();

        List<Order> GrossMismatchList = new List<Order>(); //get ready to fill a list of orderIDs from order exceptions

        DateTime startDate = Convert.ToDateTime(fromDate);
        DateTime stopDate = Convert.ToDateTime(toDate);

        DataSet dsRev = Metrics.GetRevenueSet("ObrTransactionList", startDate, stopDate); //also sets Session["ObrTransactionList"]
        DataSet dsOrderDetails = Metrics.GetOrderDetailReport(startDate, stopDate);
        Decimal GrossReconcile = Metrics.GetGrossReconcile("GrossMismatchList", startDate, stopDate);//order total vs, orderdetail total: mismatches by orderID in Session["GrossMisMatch"]
        Session["GrossReconcile"] = GrossReconcile;

        s.TotalCost = 0;
        s.TotalUnits = 0;
        for (int i = 0; i < dsOrderDetails.Tables[0].Rows.Count; i++)
        {
            s.TotalCost += Convert.ToDecimal(dsOrderDetails.Tables[0].Rows[i]["Cost"]);
            s.TotalUnits += Convert.ToInt32(dsOrderDetails.Tables[0].Rows[i]["Units"]);
        }

        s.TotalOrders = Convert.ToInt32(dsRev.Tables[0].Rows[0]["OrderCount"]);
        s.TotalGrossRev = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["GrossRevenue"]);
        s.AdjTotalGrossRev = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["AdjGrossRevenue"]);
        s.TotalDiscounts = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["LineItemDiscounts"]);
        s.TotalCoupon = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["OrderDiscounts"]);
        s.TotalOther = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["OtherCS"]);
        s.TotalNet = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["NetRevenue"]);
        s.TotalTax = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Tax"]);
        s.TotalShipping = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Shipping"]);
        s.TotalSiteCollected = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Collected"]);
        s.TotalRefunds = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Refunds"]);
        s.TotalVoids = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Voids"]);
        s.TotalCollected = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["CollectedCC"]);

        //let the world see that things didn't add up quite right; makes report available
        if (GrossReconcile != 0) 
        {
            lnkBtnGrossMismatch.Text = GrossReconcile.ToString("C");
            pnlGrossMismatch.Visible = true;
        }

        Session["Summary"] = s;
        websales = new DataView(dsOrderDetails.Tables[0]);

        //get Compare Data, if requested

        if (radlstCompare.SelectedValue != "0")
        {
            List<Order> GrossMismatchListComp = new List<Order>(); //get ready to fill a list of orderIDs from order exceptions

            if (radlstCompare.SelectedValue == "1")
            {
                startDate = startDate.AddMonths(-1);
                stopDate = stopDate.AddMonths(-1);
            }
            else
            {
                startDate = startDate.AddYears(-1);
                stopDate = stopDate.AddYears(-1);
            }
            Session["startDateComp"] = startDate;
            Session["endDateComp"] = stopDate;

            dsRev = Metrics.GetRevenueSet("ObrTransactionListComp", startDate, stopDate); //also sets Session["ObrTransactionList"]
            dsOrderDetails = Metrics.GetOrderDetailReport(startDate, stopDate);
            GrossReconcile = Metrics.GetGrossReconcile("GrossMismatchListComp", startDate, stopDate);//order total vs, orderdetail total: mismatches by orderID in Session["GrossMisMatch"]
            Session["GrossReconcileComp"] = GrossReconcile;

            Summary sc = new Summary();
            sc.TotalCost = 0;
            sc.TotalUnits = 0;
            for (int i = 0; i < dsOrderDetails.Tables[0].Rows.Count; i++)
            {
                sc.TotalCost += Convert.ToDecimal(dsOrderDetails.Tables[0].Rows[i]["Cost"]);
                sc.TotalUnits += Convert.ToInt32(dsOrderDetails.Tables[0].Rows[i]["Units"]);
            }

            sc.TotalOrders = Convert.ToInt32(dsRev.Tables[0].Rows[0]["OrderCount"]);
            sc.TotalGrossRev = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["GrossRevenue"]);
            sc.AdjTotalGrossRev = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["AdjGrossRevenue"]);
            sc.TotalDiscounts = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["LineItemDiscounts"]);
            sc.TotalCoupon = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["OrderDiscounts"]);
            sc.TotalOther = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["OtherCS"]);
            sc.TotalNet = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["NetRevenue"]);
            sc.TotalTax = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Tax"]);
            sc.TotalShipping = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Shipping"]);
            sc.TotalSiteCollected = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Collected"]);
            sc.TotalRefunds = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Refunds"]);
            sc.TotalVoids = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Voids"]);
            sc.TotalCollected = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["CollectedCC"]);

            //let the world see that things didn't add up quite right; makes report available
            if (GrossReconcile != 0)
            {
                lnkBtnGMMComp.Text = GrossReconcile.ToString("C");
                pnlGMMComp.Visible = true;
            }

            Session["SummaryComp"] = sc;
        }
        return websales; 
    }

    protected void lnkBtnGrossMisMatch_Click(object sender, EventArgs e)
    {
        Response.Redirect("ShowGrossMisMatch.aspx");
    }

    protected void lnkBtnGrossMisMatchComp_Click(object sender, EventArgs e)
    {
        Response.Redirect("ShowGrossMisMatch.aspx?comp=yes");
    }

    protected void lnkBtnTotalOther_Click (object sender, EventArgs e)
    {
        Response.Redirect("ShowOffBookRevenue.aspx");
    }

    protected void lnkBtnTotalOtherComp_Click(object sender, EventArgs e)
    {
        Response.Redirect("ShowOffBookRevenue.aspx?comp=yes");
    }

    protected void lnkbtnTotalCoupon_Click(object sender, EventArgs e)
    {
        string start = "1/1/2000";
        string end = DateTime.Now.ToShortDateString();
        ContentPlaceHolder MainContent = (ContentPlaceHolder)Master.FindControl("MainContent");
        if (MainContent != null)
        {
            Literal StartDate = (Literal)MainContent.FindControl("litStartDate");
            if (StartDate != null) { start = ValidateUtil.CleanUpString(StartDate.Text); }
            Literal EndDate = (Literal)MainContent.FindControl("litEndDate");
            if (EndDate != null) { end = ValidateUtil.CleanUpString(EndDate.Text); }
        }
        Summary s = (Summary)Session["Summary"];
        Response.Redirect("ShowCoupons.aspx?start=" + start + "&end=" + end + "&coupon=" + s.TotalCoupon.ToString());
    }

    protected void lnkbtnTotalCouponComp_Click(object sender, EventArgs e)
    {
        string start = "1/1/2000";
        string end = DateTime.Now.ToShortDateString();
        ContentPlaceHolder MainContent = (ContentPlaceHolder)Master.FindControl("MainContent");
        DateTime dtStart = (DateTime)Session["startDateComp"];
        DateTime dtEnd = (DateTime)Session["startDateComp"];

        if (MainContent != null)
        {
            Literal StartDate = (Literal)MainContent.FindControl("litStartDate");
            if (StartDate != null) 
            { 
                start = ValidateUtil.CleanUpString(StartDate.Text);
                dtStart = Convert.ToDateTime(start);
            }

            Literal EndDate = (Literal)MainContent.FindControl("litEndDate");
            if (EndDate != null) 
            { 
                end = ValidateUtil.CleanUpString(EndDate.Text);
                dtEnd = Convert.ToDateTime(end);
            }
        }
        Summary s = (Summary)Session["SummaryComp"];
        Response.Redirect("ShowCoupons.aspx?start=" + start + "&end=" + end + "&coupon=" + s.TotalCoupon.ToString());
    }
    protected void repeateritems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Item it = (Item)e.Item.DataItem;
            //litTotalOrders.Text = cart.TotalProduct.ToString("N");

            DataRowView r = (DataRowView)e.Item.DataItem;

            Literal litSKU = (Literal)e.Item.FindControl("SKU");
            litSKU.Text = r["SKU"].ToString();

            Literal litDescription = (Literal)e.Item.FindControl("Description");
            litDescription.Text = r["Name"].ToString();

            Literal litUnitCost = (Literal)e.Item.FindControl("UnitCost");
            litUnitCost.Text = Convert.ToDecimal(r["UnitCost"]).ToString("C");

            Literal litUnits = (Literal)e.Item.FindControl("Units");
            litUnits.Text = r["Units"].ToString();

            Literal litGross = (Literal)e.Item.FindControl("Gross");
            litGross.Text = Convert.ToDecimal(r["GrossRevenue"]).ToString("C");

            Literal litCost = (Literal)e.Item.FindControl("Cost");
            litCost.Text = Convert.ToDecimal(r["Cost"]).ToString("C");

            Literal litDiscount = (Literal)e.Item.FindControl("Discount");
            litDiscount.Text = Convert.ToDecimal(r["Discount"]).ToString("C");

            Literal litphUnits = (Literal)e.Item.FindControl("phUnits");
            litphUnits.Text = Convert.ToDecimal(r["phUnits"]).ToString();

            Literal litphGross = (Literal)e.Item.FindControl("phGross");
            litphGross.Text = Convert.ToDecimal(r["phGross"]).ToString("C");

            Literal litphDiscount = (Literal)e.Item.FindControl("phDiscount");
            litphDiscount.Text = Convert.ToDecimal(r["phDiscount"]).ToString("C");
        }

        if (e.Item.ItemType == ListItemType.Footer)
        {
        }
    }

}
