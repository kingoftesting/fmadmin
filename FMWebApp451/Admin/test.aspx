<%@ Page Language="C#" MasterPageFile="MasterPage3_Blank.master" Theme="Admin" AutoEventWireup="true" Inherits="Admin_test" Title="Test Page" Codebehind="test.aspx.cs" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $.colorbox({ width: "50%", height: "350px", inline: true, href: "#progressBarTest", opacity: 0.5 });

            $("#uploadFile").colorbox({ width: "550px", inline: true, href: "#uploadFilePanel" });

            $("#progressbar").progressbar({ value: false });
            $("#progressbar").css('width', "25%");

           $("#MainContent_btnGetData").click(function () {
                var intervalID = setInterval(updateProgress, 250);
                $.ajax({
                    type: "POST",
                    url: "test.aspx/GetText",
                    data: "{}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    success: function (msg) {
                        $("#progressbar").progressbar("value", 100);
                        $("#result").text(msg.d);
                        clearInterval(intervalID);
                    }
                });
                return false;
            });

        });

        function updateProgress() {
            var value = $("#progressbar").progressbar("option", "value");
            if (value < 100) {
                $("#progressbar").progressbar("value", value + 3);
                $("#progValue").text("Completed: " + value + "%");
            }
        }

        function closeOverlay() {
            $.colorbox.close();
            return false;
        }
    </script>

<h2>Section: testing jQuery ProgressBar</h2>

    <div id="testProgressBar" runat="server" style="visibility: hidden; height: 1px">
        <div id="progressBarTest" class="overlaypanel">
            <h2>Test for Progress Bar</h2>
            <div id="progressbar"></div>
            <div id="progValue"></div>
            <div id="result"></div>
            <asp:Button ID="btnGetData" runat="server" Text="Get Data" />
        </div>
    </div>


<hr />

    <h2>Section: testing uploading files</h2>

    <p>
        Select file to upload to server:
        <input id="MyFile" type="file" size="40" runat="server" />
    </p>

    <p>
        <asp:Button ID="bntGetFile" runat="server" Text="Select File" OnClick="submit_click" />
    </p>

    <div id="Div1" runat="server">
        FileName: <span id="fname" runat="server" />
        <br />
        ContentLength: <span id="clength" runat="server" />bytes
    </div>

    <p>&nbsp;</p>

    <a href="#" id="uploadFile" class="file">upLoad File</a>

    <div id="Div2" runat="server" style="visibility:hidden;height:1px">
        <div id="uploadFilePanel" class="overlaypanel">
            <h2>Get File Name</h2>
            <ul class="fl" style="margin: 0;">
                <li>
                    <asp:Label ID="Label3" CssClass="lbl" AssociatedControlID="txtUploadFile" runat="server" Text="Choose File Name" />
                    <asp:FileUpload runat="server" ID="txtUploadFile" Width="400" size="50" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtUploadFile" ErrorMessage="Required"
                        ValidationGroup="fileUpload" />
                </li>
                <li style="margin: 0;">
                    <asp:Button runat="server" ID="btnUploadFile" Text="Submit"
                        ValidationGroup="fileUpload" CssClass="btn primary" OnClientClick="colorboxDialogSubmitClicked('fileupload', 'uploadFilePanel');" OnClick="BtnUploadFileClick"/>
                    or <a href="#" onclick="return closeOverlay();">Cancel</a>
                </li>
            </ul>
        </div>
    </div>

<p>&nbsp;</p>

<hr />

<h2>Section: testing passing variables to/from  server & client</h2>

<div>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="HF1" runat="server" />
    <asp:HiddenField ID="hfObj1" runat="server" />

    <asp:Label ID="Label2" runat="server" Text="Press Button to Enter Value"></asp:Label>
    <asp:Button ID="Button1" runat="server" Text="Button" OnClientClick="enterVal();" />
    <br />
    <asp:Label ID="Label1" runat="server" Text="Value = "></asp:Label>


</div>

    <script type="text/javascript">
        function enterVal() {
            var hf = document.getElementById('<%= Page.Master.FindControl("MainContent").FindControl("HiddenField1").ClientID %>');
            var val = null;
            var hf1 = document.getElementById('<%= Page.Master.FindControl("MainContent").FindControl("HF1").ClientID %>');
            var jsonStr = document.getElementById('<%= Page.Master.FindControl("MainContent").FindControl("hfObj1").ClientID %>');
            var hfObj1 = JSON.parse(jsonStr.value);

            var promptVal = 'name = ' + '<%= Session["name"] %>' + '\n';
            promptVal += 'ID = ' + hf1.value + '\n';

            promptVal += 'Alert: ' + '<%= alertStr %>' + '\n';

            for (var i in hfObj1) {
                    promptVal += i + ': ' + hfObj1[i] + '\n';
            }

            promptVal += 'Please Enter A Value: ';
            while ((val == null) || (val == '')) {
                if (val == null) val = prompt(promptVal, 'non-null')
                if (val == '') val = prompt(promptVal, 'non-empty')
            }
            if (hf)
                hf.value = val;
            else
                alert("No Hiddenfield!");
            
        }
    </script>  

</asp:Content>

