using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_test : AdminBasePage //System.Web.UI.Page
{
    #region Private Variables & Public Properties & Constructors
    private class HDI_Returns
    {
        private int id;
        public int Id { get { return id; } set { id = value; } }

        private string name;
        public string Name { get { return name; } set { name = value; } }

        private int orderId;
        public int OrderId { get { return orderId; } set { orderId = value; } }

        private Int64 serNum;
        public Int64 SerNum { get { return serNum; } set { serNum = value; } }

        private DateTime dateDelivered;
        public DateTime DateDelivered { get { return dateDelivered; } set { dateDelivered = value; } }

        private DateTime dateRMAIssued;
        public DateTime DateRMAIssued { get { return dateRMAIssued; } set { dateRMAIssued = value; } }

        private string reason;
        public string Reason { get { return reason; } set { reason = value; } }

        private string reasonVal;
        public string ReasonVal { get { return reasonVal; } set { reasonVal = value; } }

        private string reason2;
        public string Reason2 { get { return reason2; } set { reason2 = value; } }

        private string reason2Val;
        public string Reason2Val { get { return reason2Val; } set { reason2Val = value; } }

        private string description;
        public string Description { get { return description; } set { description = value; } }

        public HDI_Returns()
        {
            Id = 0;
            Name = "";
            OrderId = 0;
            SerNum = 0;
            DateDelivered = Convert.ToDateTime("1/1/2000");
            DateRMAIssued = Convert.ToDateTime("1/1/2000");
            Reason = "";
            Reason2 = "";
            Description = "";
        }
    }
    #endregion Private Variables & Public Properties & Constructors

    public string alertStr = "No Alert";

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["name"] = "rodger"; //pass variable via Session
        HF1.Value = "152"; //pass variable via HiddenField

        HDI_Returns myVar = new HDI_Returns();
        myVar.Id = 152;
        myVar.Name = "rmohme";
        myVar.OrderId = 1;
        myVar.DateRMAIssued = DateTime.Today;
        myVar.DateDelivered = DateTime.Today;
        myVar.Description = "This is a test of passing an obj to javascript";
        myVar.SerNum = 61212102353;
        myVar.Reason = "One too many FM";
        myVar.ReasonVal = "5";
        myVar.Reason2 = "uncomfortable/Painful";
        myVar.Reason2Val = "3";

        System.Web.Script.Serialization.JavaScriptSerializer oSerializer =
         new System.Web.Script.Serialization.JavaScriptSerializer();

        hfObj1.Value = oSerializer.Serialize(myVar);

        if (IsPostBack) 
        {

            Label1.Text += HiddenField1.Value;
        }
        else 
        { 
            HiddenField1.Value = "default.Value";
        }

    }

    protected void submit_click(object sender, EventArgs e)
    {
        fname.InnerHtml = MyFile.PostedFile.FileName;
        clength.InnerHtml = MyFile.PostedFile.ContentLength.ToString();
    }

    /// <summary>
    /// Handles the Click event of the btnUploadFile control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void BtnUploadFileClick(object sender, EventArgs e)
    {
        fname.InnerHtml = txtUploadFile.FileName;
        clength.InnerHtml = txtUploadFile.PostedFile.ContentLength.ToString();
    }

    [System.Web.Services.WebMethod]
    public static string GetText()
    {
        for (int i = 0; i < 10; i++)
        {
            Thread.Sleep(1000);
        }
        return "All finished!";
    }
    
    

}
