using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Linq;

public partial class Admin_testTrkNum : AdminBasePage //System.Web.UI.Page
{
    #region Private Variables & Public Properties & Constructors
    
    #endregion Private Variables & Public Properties & Constructors


    protected void Page_Load(object sender, EventArgs e)
    {
        string xml_req = @"
            <?xml version=""1.0""?>
                <AccessRequest xml:lang=""en-US"">
                    <AccessLicenseNumber>{0}</AccessLicenseNumber>
                    <UserId>{1}</UserId>
                    <Password>{2}</Password>
                </AccessRequest>
            <?xml version=""1.0""?>
                <TrackRequest xml:lang=""en-US"">
                    <Request>
                        <TransactionReference>
                            <CustomerContext>QAST Track</CustomerContext>
                            <XpciVersion>1.0</XpciVersion>
                        </TransactionReference>
                        <RequestAction>Track</RequestAction>
                        <RequestOption>activity</RequestOption>
                    </Request>
                   <TrackingNumber>#{3}</TrackingNumber>
                </TrackRequest>
        ";
        string licence_no = "1CCFD8A3A1A71152";
        string user_ID = "mohme";
        string password = "Kingoftesting1";
        //string track_no = "1Z12345E1512345676"; // (This is test track no)
        string track_no = "1ZX5070V0342288005"; // (live tracking #)

        xml_req = string.Format(xml_req, licence_no, user_ID, password, track_no);

        /*
        string path = "https://www.ups.com/ups.app/xml/Track";
        string url = URI.parse(path);
        string http = Net::HTTP.new(url.host,url.port);
        string http.use_ssl = true;
        string http.verify_mode = OpenSSL::SSL::VERIFY_NONE;

        string response =  http.post(url.path, xml_req);
        string response_body = response.body;
        string hash = Hash.from_xml(response_body.to_s);
        */

        string URI = "https://www.ups.com/ups.app/xml/Track"; // (live access)
        //string URI = "https://wwwcie.ups.com/ups.app/xml/Track"; // (testing access)
        string xmlResult = "";
        using (WebClient wc = new WebClient())
        {

            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            xmlResult = wc.UploadString(URI, xml_req);
        }

        XDocument result = XDocument.Parse(xmlResult);
        XElement responseStatusCode = new XElement("ResponseStatusCode");
        XElement activity = new XElement("Activity");
        XElement code = new XElement("Code");
        XElement date = new XElement("Date");
        XElement time = new XElement("Time");

        try
        {
            responseStatusCode = result.Descendants("TrackResponse")
                .Descendants("Response")
                .Descendants("ResponseStatusCode")
                .First();
        } 
        catch (Exception)
        {
            responseStatusCode.Value = "0";
        }
        if (responseStatusCode.Value == "1")
        {
            activity = result.Descendants("TrackResponse")
                .Descendants("Shipment")
                .Descendants("Package")
                .Descendants("Activity")
                .First();
            code = activity.Descendants("Status").Descendants("StatusType").Descendants("Code").First(); 
  
            if (code.Value == "D")
            {
                date = activity.Descendants("Date").First();
                time = activity.Descendants("Time").First();
            }
        }

        string dyear = date.Value.ToString().Substring(0, 4);
        string dmonth = date.Value.ToString().Substring(4, 2);
        string dday = date.Value.ToString().Substring(6, 2);

        string responseStr = "ResponseStatusCode: " + responseStatusCode.ToString() + "<br />";
        responseStr += "Activity Status Code: " + code.ToString() + "<br />";
        responseStr += "Delivery Date: " + dmonth + "/" + dday + "/" + dyear + "<br />";
        responseStr += "Delivery Time: " + time.Value.ToString() + "<br />";
        litResponse.Text = responseStr;
    }


    
}
