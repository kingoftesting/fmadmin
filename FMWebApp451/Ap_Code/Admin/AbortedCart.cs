﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;


/// <summary>
/// Summary description for AbortedCart
/// </summary>
public class AbortedCart
{
    #region Private/Public Vars
    private int id;
    public int ID { get { return id; } set { id = value; } }
    private int orderID;
    public int OrderID { get { return orderID; } set { orderID = value; } }
    private string emailBody;
    public string EmailBody { get { return emailBody; } set { emailBody = value; } }
    private DateTime dateSent;
    public DateTime DateSent { get { return dateSent; } set { dateSent = value; } }
    private DateTime orderDate;
    public DateTime OrderDate { get { return orderDate; } set { orderDate = value; } }
    private Boolean test;
    public Boolean Test { get { return test; } set { test = value; } }
    private Boolean emailSuccess;
    public Boolean EmailSuccess { get { return emailSuccess; } set { emailSuccess = value; } }
    private string emailResult;
    public string EmailResult { get { return emailResult; } set { emailResult = value; } }

    #endregion

    public AbortedCart()
    {
        ID = 0;
        OrderID = 0;
        EmailBody = "n/a";
        DateSent = Convert.ToDateTime("1/1/1900");
        OrderDate = Convert.ToDateTime("1/1/1900");
        Test = true;
        EmailSuccess = true;
        EmailResult = "";
    }

    public AbortedCart(int IDfield)
    {
        string sql = @" 
SELECT * FROM  AbortedCart 
WHERE ID = @ID 
";
        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, IDfield);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
        if (dr.Read())
        {
            ID = Convert.ToInt32(dr["ID"]);
            OrderID = Convert.ToInt32(dr["OrderID"].ToString());
            EmailBody = dr["EmailBody"].ToString();
            DateSent = Convert.ToDateTime(dr["DateSent"].ToString());
            OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());
            Test = Convert.ToBoolean(dr["Test"]);
            EmailSuccess = Convert.ToBoolean(dr["EmailSuccess"]);
            EmailResult = dr["EmailResult"].ToString();

        }
        if (dr != null) { dr.Close(); }
    }

    public int Save(int ID)
    {
        int result = 0;
        SqlParameter[] mySqlParameters = null;
        SqlDataReader dr = null;
        if (ID == 0)
        {
            string sql = @" 
INSERT INTO AbortedCart 
(OrderID, EmailBody, DateSent, Test, EmailSuccess, EmailResult, OrderDate) 
VALUES(@OrderID, @EmailBody, @Date, @Test, @EmailSuccess, @EmailResult, @OrderDate); 
SELECT ID=@@identity;  
";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID, EmailBody, DateSent, Test, EmailSuccess, EmailResult, OrderDate);
            dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                result = Convert.ToInt32(dr["ID"]);
            }
            else
            {
                result = 99;
            }
        }
        else
        {
            string sql = @" 
UPDATE AbortedCart 
SET  
OrderID = @OrderID,  
EmailBody = @EmailBody,  
DateSent = @DateSent,  
Test = @Test, 
EmailSuccess = @EmailSuccess, 
EmailResult = @EmailResult, 
OrderDate = @OrderDate 
WHERE ID = @ID  
";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID, EmailBody, DateSent, Test, EmailSuccess, EmailResult, OrderDate, ID);
            DBUtil.Exec(sql, mySqlParameters);
        }
        return result;
    }

    /***********************************
    * Static methods
    ************************************/

    /// <summary>
    /// Returns a collection with all AbortedCart
    /// </summary>
    public static List<AbortedCart> GetAbortedCartList()
    {
        List<AbortedCart> thelist = new List<AbortedCart>();

        string sql = @" 
SELECT * FROM AbortedCart WHERE Test = 0 ORDER BY OrderID DESC 
";

        SqlDataReader dr = DBUtil.FillDataReader(sql);

        while (dr.Read())
        {
            AbortedCart obj = new AbortedCart();

            obj.ID = Convert.ToInt32(dr["ID"]);
            obj.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
            obj.EmailBody = dr["EmailBody"].ToString();
            obj.DateSent = Convert.ToDateTime(dr["DateSent"].ToString());
            obj.OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());
            obj.Test = Convert.ToBoolean(dr["Test"]);
            obj.EmailSuccess = Convert.ToBoolean(dr["EmailSuccess"]);
            obj.EmailResult = dr["EmailResult"].ToString();


            thelist.Add(obj);
        }
        if (dr != null) { dr.Close(); }
        return thelist;
    }

    /// <summary>
    /// Returns a className object with the specified ID
    /// </summary>

    public static AbortedCart GetAbortedCartByID(int id)
    {
        AbortedCart obj = new AbortedCart(id);
        return obj;
    }

    /// <summary>
    /// Updates an existing AbortedCart
    /// </summary>
    public static bool UpdateAbortedCart(int id,
    int orderid, string emailbody, DateTime dateSent, Boolean test, Boolean emailSuccess, string emailResult, DateTime orderDate)
    {
        AbortedCart obj = new AbortedCart();

        obj.ID = id;
        obj.OrderID = orderid;
        obj.EmailBody = emailbody;
        obj.DateSent = dateSent;
        obj.OrderDate = orderDate;
        obj.Test = test;
        obj.EmailSuccess = emailSuccess;
        obj.EmailResult = emailResult;
        int ret = obj.Save(obj.ID);
        return (ret > 0) ? true : false;
    }

    /// <summary>
    /// Creates a new AbortedCart
    /// </summary>
    public static int InsertAbortedCart(
    int orderid, string emailbody, DateTime dateSent, Boolean test, Boolean emailSuccess, string emailResult, DateTime orderDate)
    {
        AbortedCart obj = new AbortedCart();

        obj.OrderID = orderid;
        obj.EmailBody = emailbody;
        obj.DateSent = dateSent;
        obj.OrderDate = orderDate;
        obj.Test = test;
        obj.EmailSuccess = emailSuccess;
        obj.EmailResult = emailResult;
        int ret = obj.Save(obj.ID);
        return ret;
    }

    public static int MaxEmailSent()
    {
        int max = 0;
        string sql = "select top 1 id from abortedcart where test = 0 order by id desc";
        //SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID);
        SqlDataReader dr = DBUtil.FillDataReader(sql);

        dr.Read();
        if (dr.HasRows)
        { max = Convert.ToInt32(dr["id"]); }
        else { max = 0; }
        dr.Close();

        return max;
    }

}