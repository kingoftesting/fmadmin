using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using AdminCart;

public class AdminBasePage : System.Web.UI.Page
{

    protected override void InitializeCulture()
    {
        /* hopefully this defaults to _en culture
        string culture = (HttpContext.Current.Profile as ProfileCommon).Preferences.Culture;
        this.Culture = culture;
        this.UICulture = culture;
        */
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        // Define the name and type of the client scripts on the page.
        String csname1 = "fixform"; //so that only the button controls spec'd generate new windows
        Type cstype = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;

        // Check to see if the startup script is already registered.
        if (!cs.IsStartupScriptRegistered(cstype, csname1))
        {
            StringBuilder cstext1 = new StringBuilder();
            //cstext1.Append("<script type=text/javascript> alert('Hello World!') </");
            //cstext1.Append("script>");

            cstext1.Append("<script type='text/javascript'>");
            cstext1.Append("function fixform() { ");
            cstext1.Append("if (opener.document.getElementById('aspnetForm').target != '_blank') return; ");
            cstext1.Append("opener.document.getElementById('aspnetForm').target = ''; ");
            cstext1.Append("opener.document.getElementById('aspnetForm').action = opener.location.href; ");
            cstext1.Append("} </script>");

            cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
        }

        // add onfocus and onblur javascripts to all input controls on the forum,
        // so that the active control has a difference appearance
        Utilities.FMHelpers.SetInputControlsHighlight(this, "highlight", false);

        //set update title to reflect which app we're on
        Page.Title = Config.appName + " Admin: " + Page.Title;

        base.OnLoad(e);
    }

    public void CanUserProceed(string[] roles)
    {
        bool userOK = false; //assume user can't proceed
        for (int i = 0; i < roles.Length; i++)
        {
            if (Convert.ToBoolean(Session["IsAdmin"])) { userOK = true; }
            if (Convert.ToBoolean(Session["IsDoctor"])) { userOK = true; }
            if (Convert.ToBoolean(Session["IsCS"])) { userOK = true; }
            if (Convert.ToBoolean(Session["IsQA"])) { userOK = true; }
            if (Convert.ToBoolean(Session["IsREPORTS"])) { userOK = true; }
            if (Convert.ToBoolean(Session["IsSTORE"])) { userOK = true; }
            if (Convert.ToBoolean(Session["IsUSER"])) { userOK = true; }
            if (Convert.ToBoolean(Session["IsMARKETING"])) { userOK = true; }
        }

        if (!userOK) 
        {
            Response.Redirect("~/FMAccount/SignIn");

            /*
            try
            { throw new SecurityException("You do not have the apppropriate creditentials to complete this task."); }

            catch (SecurityException ex)
            {
                // GetCurrentPageName
                string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
                string requestedPage = oInfo.Name;
                requestedPage = requestedPage.ToUpper();

                string msg = "Error in " + requestedPage + Environment.NewLine;
                msg += "User did not have permission to access page" + Environment.NewLine;

                string id = "0";
                if (Session["UserID"] != null) { id = Session["UserID"].ToString(); }
                msg += "CustomerID=" + id.ToString() + Environment.NewLine;
                //if (!requestedPage.Contains("DEFAULT")) { FM2015.Helpers.dbErrorLogging.LogError(msg, ex); }
            }
            */
        }

        if ((Config.RedirectMode == "Test") && (!Convert.ToBoolean(Session["isAdmin"]))) //gotta be an Admin to be in Test mode
        { Response.Redirect("~/FMAccount/SignIn"); }
    }
}
