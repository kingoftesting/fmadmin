using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using FM2015.Helpers;

namespace AdminCart
{
    public class Address
    {
        public enum AddressType
        {
            Billing, Shipping
        }

        #region Private Variables
        private int addressId;
        private int customerID;
        private string street;
        private string street2;
        private string city;
        private string state;
        private string zip;
        private string country;
        #endregion

        #region Public Properties
        public int AddressId
        {
            get { return addressId; }
            set { addressId = value; }
        }
        public int CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }

        [Required]
        [Display(Name = "Street")]
        public string Street
        {
            get { return street; }
            set { street = value; }
        }

        public string Street2
        {
            get { return street2; }
            set { street2 = value; }
        }

        [Required]
        [Display(Name = "City")]
        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string State
        {
            get { return state; }
            set { state = value; }
        }

        public string Zip
        {
            get { return zip; }
            set { zip = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        #endregion

        #region Constructors
        public Address()
        {
            street = "";
            city = "";
            state = "";
            zip = "";
            country = "";
        }
        #endregion

        static public Address LoadAddress(int AddressID)
        {
            Address theAddress = new Address();

            string sql = @"
                SELECT AddressID, CustomerID, Street1, Street2, City, State, Zip, CountryCode 
                FROM Addresses 
                WHERE AddressID = @AddressID 
                ORDER BY AddressID DESC";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, AddressID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read()) //just take the first address, if there are multiple
            {
                theAddress.addressId = dr.GetInt32(0);
                theAddress.customerID = dr.GetInt32(1);
                theAddress.street = dr.GetString(2);

                if (dr["Street2"] == DBNull.Value)
                { theAddress.street2 = " "; }
                else
                { theAddress.street2 = dr["Street2"].ToString(); }

                theAddress.city = dr.GetString(4);
                theAddress.state = dr.GetString(5);
                theAddress.zip = dr.GetString(6);
                theAddress.country = dr.GetString(7);
            }
            dr.Close();

            return theAddress;
        }

        static public Address LoadAddress(int customerID, int addressType)
        {
            Address theAddress = new Address();

            string sql = @"
                SELECT AddressID, CustomerID, Street1, Street2, City, State, Zip, CountryCode, AddressType 
                FROM Addresses 
                WHERE AddressID = (select max(AddressID) from Addresses where customerid = @CustomerID AND addresstype = @AddressType) 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, customerID, addressType);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                theAddress.addressId = dr.GetInt32(0);
                theAddress.customerID = dr.GetInt32(1);
                theAddress.street = dr.GetString(2);

                if (dr["Street2"] == DBNull.Value)
                { theAddress.street2 = ""; }
                else
                { theAddress.street2 = dr["Street2"].ToString(); }

                theAddress.city = dr.GetString(4);
                theAddress.state = dr.GetString(5);
                theAddress.zip = dr.GetString(6);
                theAddress.country = dr.GetString(7);
            }
            dr.Close();

            return theAddress;
        }

        public void Save(Customer c, int addressType)
        {
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            Street2 = (string.IsNullOrEmpty(Street2)) ? " " : Street2;

           //bypass problem saving billing address
           if (addressId == 0)
           {
               sql = @"
                    INSERT INTO Addresses ( CustomerID, Street1, Street2, City, State, Zip, CountryCode, AddressType ) 
                    VALUES ( @CustomerID, @Street1, @Street2, @City, @State, @Zip, @CountryCode, @AddressType );  
                    SELECT AddressID=@@identity; 
                ";
               mySqlParameters = DBUtil.BuildParametersFrom(sql, c.CustomerID, Street, Street2, City, State, Zip, Country, addressType);
               dr = DBUtil.FillDataReader(sql, mySqlParameters);
               if (dr.Read())
               { addressId = Convert.ToInt32(dr["AddressID"]); }
               if (dr != null) { dr.Close(); }
           }
           else
           {
               sql = @"UPDATE Addresses 
                       SET 
                        Street1 = @Street1, 
                        Street2 = @Street2, 
                        City = @City, 
                        State = @State, 
                        Zip = @Zip, 
                        CountryCode = @CountryCode 
                    WHERE AddressID = @AddressID 
                ";
               mySqlParameters = DBUtil.BuildParametersFrom(sql, Street, Street2, City, State, Zip, Country, addressId);
               DBUtil.Exec(sql, mySqlParameters);
           }
        }

        static public List<Address> GetAddressList(string customerLastName, int aType)
        {
            List<Address> aList = new List<Address>();

            string sql = @"
                SELECT a.AddressID, a.CustomerID, a.Street1, a.Street2, a.City, a.[State], a.Zip, a.CountryCode, a.AddressType 
                FROM Addresses a, Customers c 
                WHERE (a.CustomerID = c.CustomerID) AND c.LastName = @LastName AND a.AddressType = @AddressType 
                ORDER BY a.AddressID DESC 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, customerLastName, aType);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                Address theAddress = new Address();
                theAddress.addressId = Convert.ToInt32(dr["AddressID"].ToString());
                theAddress.customerID = Convert.ToInt32(dr["CustomerID"].ToString());
                theAddress.street = dr["Street1"].ToString();

                if (dr["Street2"] == DBNull.Value)
                { theAddress.street2 = ""; }
                else
                { theAddress.street2 = dr["Street2"].ToString(); }

                theAddress.city = dr["City"].ToString();
                theAddress.state = dr["State"].ToString();
                theAddress.zip = dr["Zip"].ToString();
                theAddress.country = dr["CountryCode"].ToString();

                aList.Add(theAddress);
            }
            if (dr != null) { dr.Close(); }

            return aList;

        }
    }
}
