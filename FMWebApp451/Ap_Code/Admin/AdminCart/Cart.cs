using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using PayPal.Payments.Common;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;
using System.Data.SqlClient;
using System.Linq;
using Stripe;
using FM2015.Helpers;
using PayWhirl;

namespace AdminCart
{
    public class Cart
    {

        #region Member Classes and Collections
        //Member classes
        public Customer SiteCustomer;
        public CartItems Items;
        public Address BillAddress;
        public Address ShipAddress;
        public Order SiteOrder;
        public OrderPayments Payments;
        //public Certnet.ProductItems Products;
        public ProductItems Products;
        public Transaction Tran;

        public Coupon SiteCoupon;

        public int coupon;
        #endregion

        #region Members
        private Guid cartID;
        private string cartStatus;

        private int orderID;
        private int affiliateOrderID;
        private decimal totalProduct;
        private decimal totalTax;
        private decimal totalShipping;
        private decimal totalDiscounts;
        private decimal total;
        private int shipType;
        private decimal groundShipping;
        private decimal expeditedShipping;
        private decimal expressShipping;
        private decimal csShipping;
        private string cartMode;
        #endregion

        #region Properties
        public int AffiliateOrderID
        {
            get { return affiliateOrderID; }
            set { affiliateOrderID = value; }
        }
        public int OrderID
        {
            get { return orderID; }
            set { orderID = value; }
        }
        [Display(Name = "SubTotal")]
        [DataType(DataType.Currency)]
        public decimal TotalProduct
        {
            get{ return totalProduct;}
            set{ totalProduct=value;}
        }

        [Display(Name = "Tax")]
        [DataType(DataType.Currency)]
        public decimal TotalTax
        {
            get{ return totalTax;}
            set{ totalTax=value;}
        }

        [Display(Name = "Shipping")]
        [DataType(DataType.Currency)]
        public decimal TotalShipping
        {
            get{ return totalShipping;}
            set{ totalShipping =value;}
        }

        [Display(Name = "Shopify Discounts")]
        [DataType(DataType.Currency)]
        public decimal TotalDiscounts
        {
            get{ return totalDiscounts;}
            set{ totalDiscounts=value;}
        }

        [Display(Name = "Total")]
        [DataType(DataType.Currency)]
        public decimal Total
        {
            get { return total; }
            set { total = value; }
        }
        public Guid CartID
        {
            get { return cartID; }
            set { cartID = value; }
        }
        public string CartStatus
        {
            get { return cartStatus; }
            set { cartStatus = value; }
        }
        public int ShipType
        {
            get { return shipType; }
            set { shipType = value; }
        }


        public decimal CSShipping { get { return csShipping; } set { csShipping = value; } }
        public decimal GroundShipping { get { return groundShipping; } set { groundShipping = value; } }
        public decimal ExpeditedShipping { get { return expeditedShipping; } set { expeditedShipping = value; } }
        public decimal ExpressShipping { get { return expressShipping; } set { expressShipping = value; } }

        public string CartMode
        {
            get { return cartMode; }
            set { cartMode = value; }
        }

        
     #endregion


        public Cart()
        {
            orderID = 0;
            affiliateOrderID = 0;
            totalProduct = 0;
            totalTax = 0;
            totalShipping = 0;
            csShipping = -1;
            totalDiscounts = 0;
            total = 0;

            cartID = Guid.NewGuid();
            SiteCustomer = new Customer();
            Items = new CartItems();
            BillAddress = new Address();
            ShipAddress = new Address();
            SiteOrder = new Order();
            //Products = new Certnet.ProductItems();
            Products = new ProductItems();

            SiteCoupon = new Coupon();

            Tran = new Transaction();

            ShipType = SiteOrder.ShipMode;
            coupon = 0;
            cartMode = Config.CartMode;
        }

        public Cart GetCartFromOrder(int _OrderID)
        {
            Cart c = new Cart();
            c.SiteOrder = new Order(_OrderID);
            c.SiteCustomer = new Customer(c.SiteOrder.CustomerID);
            c.ShipAddress = Address.LoadAddress(c.SiteOrder.ShippingAddressID);
            c.BillAddress = Address.LoadAddress(c.SiteOrder.BillingAddressID);
            
            c.Tran = Transaction.GetTransaction(_OrderID);

            c.orderID = _OrderID;
            c.Total = c.SiteOrder.Total;
            c.TotalProduct = c.SiteOrder.SubTotal;
            c.TotalShipping = c.SiteOrder.TotalShipping;
            c.TotalTax = c.SiteOrder.TotalTax;
            c.TotalDiscounts = c.SiteOrder.TotalCoupon;
            c.shipType = c.SiteOrder.ShipMode;

            //load items from actual items sold
            foreach(OrderDetail od in OrderDetail.GetOrderDetails(c.orderID))
            {
                Item i = new Item(od.ProductID, od.Quantity, od.UnitPrice, od.Discount);

                i.LineItemTotal = i.Quantity * (od.UnitPrice - i.LineItemDiscount);

                c.Items.AddItem(i);
            }

            c.SiteCoupon.CouponCodeID = c.SiteCoupon.GetUsageID(c.orderID);
            if (c.SiteCoupon.CouponCodeID != 0) { c.SiteCoupon = new Coupon(c.SiteCoupon.CouponCodeID); }

            c.CalculateTotals();
            return c;
        }

        public Cart GetCartFromOrder(int _OrderID, bool calcTotals)
        {
            Cart c = new Cart();
            c.SiteOrder = new Order(_OrderID);
            c.SiteCustomer = new Customer(c.SiteOrder.CustomerID);
            c.ShipAddress = Address.LoadAddress(c.SiteOrder.ShippingAddressID);
            c.BillAddress = Address.LoadAddress(c.SiteOrder.BillingAddressID);

            c.Tran = Transaction.GetTransaction(_OrderID);

            c.orderID = _OrderID;
            c.Total = c.SiteOrder.Total;
            c.TotalProduct = c.SiteOrder.SubTotal;
            c.TotalShipping = c.SiteOrder.TotalShipping;
            c.TotalTax = c.SiteOrder.TotalTax;
            //c.TotalDiscounts = (c.SiteCoupon.CouponCodeID == 0) ? c.SiteOrder.Adjust : c.SiteOrder.TotalCoupon;
            c.TotalDiscounts = (c.SiteCoupon.CouponCodeID == 0) ? 0 : c.SiteOrder.TotalCoupon;
            c.shipType = c.SiteOrder.ShipMode;

            //load items from actual items sold
            foreach (OrderDetail od in OrderDetail.GetOrderDetails(c.orderID))
            {
                Item i = new Item(od.ProductID, od.Quantity, od.UnitPrice, od.Discount);

                i.LineItemTotal = i.Quantity * (od.UnitPrice - i.LineItemDiscount);

                c.Items.AddItem(i);
            }

            c.SiteCoupon.CouponCodeID = c.SiteCoupon.GetUsageID(c.orderID);
            if (c.SiteCoupon.CouponCodeID != 0) { c.SiteCoupon = new Coupon(c.SiteCoupon.CouponCodeID); }

            if (calcTotals) { c.CalculateTotals(); }
            return c;
        }

        public Cart GetCartFromOrder(Order _Order, bool fullDetails)
        {
            Cart c = new Cart();
            c.SiteOrder = _Order;
            c.SiteCustomer = new Customer(c.SiteOrder.CustomerID);
            c.ShipAddress = Address.LoadAddress(c.SiteOrder.ShippingAddressID);
            c.BillAddress = Address.LoadAddress(c.SiteOrder.BillingAddressID);

            c.orderID = c.SiteOrder.OrderID;
            c.Total = c.SiteOrder.Total;
            c.TotalProduct = c.SiteOrder.SubTotal;
            c.TotalShipping = c.SiteOrder.TotalShipping;
            c.TotalTax = c.SiteOrder.TotalTax;
            c.TotalDiscounts = c.SiteOrder.TotalCoupon;
            c.shipType = c.SiteOrder.ShipMode;

            if (fullDetails)
            {
                c.Tran = Transaction.GetTransaction(c.SiteOrder.OrderID);
                foreach (OrderDetail d in OrderDetail.GetOrderDetails(c.SiteOrder.OrderID))
                {
                    c.SiteOrder.details.AddItem(d);
                }
                //load items from actual items sold
                foreach (OrderDetail od in c.SiteOrder.details)
                {
                    Item i = new Item(od.ProductID, od.Quantity, od.UnitPrice, od.Discount);

                    i.LineItemTotal = i.Quantity * (od.UnitPrice - i.LineItemDiscount);

                    c.Items.AddItem(i);
                }

                c.SiteCoupon.CouponCodeID = c.SiteCoupon.GetUsageID(c.orderID);
                if (c.SiteCoupon.CouponCodeID != 0) { c.SiteCoupon = new Coupon(c.SiteCoupon.CouponCodeID); }

                c.CalculateTotals();
            }

            return c;
        }

        public void Load(object mycart)
        {
            //Get Cart From session data
            //this = mycart;
            if (mycart == null)
            {
                //add new cart database entry, set cartid value
                cartID = Guid.NewGuid();
                cartStatus = "newly created.";
            }
            else
            {
                Cart tempcart = (Cart)mycart;
                //object o2 = Convert.ChangeType(o1, Type.GetType(sType)); 
                //mycart = Convert.ChangeType(mycart, Type.GetType("Cart"));
                //load card data from db
                this.cartStatus = "reloaded";
                this.cartID= tempcart.CartID;
                //this.cartStatus = tempcart.CartStatus;
                this.SiteCustomer= tempcart.SiteCustomer;
                this.BillAddress = tempcart.BillAddress;
                this.ShipAddress = tempcart.ShipAddress;

                this.Items = tempcart.Items;
                this.SiteCustomer = tempcart.SiteCustomer;
                this.SiteOrder = tempcart.SiteOrder;
                this.ShipAddress = tempcart.ShipAddress;
                this.BillAddress = tempcart.BillAddress;
                this.SiteCoupon = tempcart.SiteCoupon;

                this.ShipType = tempcart.ShipType;
                this.OrderID = tempcart.OrderID;
                this.AffiliateOrderID = tempcart.AffiliateOrderID;

                this.TotalDiscounts = tempcart.TotalDiscounts;
                this.CSShipping = tempcart.CSShipping;
                this.TotalShipping = tempcart.TotalShipping;
                this.TotalProduct = tempcart.TotalProduct;
                this.TotalTax = tempcart.TotalTax;
                this.Total = tempcart.Total;
                
                this.Tran = tempcart.Tran;

                this.cartMode = tempcart.CartMode;
            
            }
        }

        public Cart CreateSubscriptionCart()
        {
            Cart subCart = new Cart();
            subCart.Load(this); //create a copy of the original cart
            subCart.Items = new CartItems(); //clear items
            foreach(Item i in this.Items)
            {
                if (i.LineItemProduct.IsSubscription)
                {
                    subCart.Items.AddItem(i); //add the subscription item to the new cart
                    i.LineItemTotal = 0; //going to pass the subscription to Stripe & don't want it calculated in the total
                    i.LineItemProduct.Name = "**SUBSCRIPTION WAS PROCESSED BY SHOPIFY** " + i.LineItemProduct.Name;
                }
            }
            return subCart;
        }

        public void CalculateTotals()
        {
            CalculateProductTotal();
            CalculateDiscounts();
            CalculateShipping();
            CalculateTax();
            CalculateTotal();
        }

        private void CalculateProductTotal()
        {
            totalProduct = 0;
            SiteOrder.Adjust = 0;
            foreach (Item i in Items)
            {
                totalProduct += i.LineItemTotal;
                //SiteOrder.Adjust += i.LineItemAdjustment;
                //SiteOrder.Adjust += (i.Quantity * i.LineItemDiscount);
            }
        }

        private void CalculateOrigShipping()
        {
            //DEFAULT VALUES
            TotalShipping = 0;

            groundShipping = 0;
            expeditedShipping = 0;
            expressShipping = 0;

            bool NoShipping = true; //assume there is no s&h for any product, anywhere

            decimal expeditedUpcharge = 8; // US Only
            decimal expressUpcharge = 16;  // US only

            
            if (ShipAddress.Country == "")
            {
                groundShipping = 0;
            } 
            
            else //assume country is US and calculate shipping
            {
                foreach (Item i in Items)
                {
                    if (!i.LineItemProduct.IsSubscription)
                    {
                        int pid = i.LineItemProduct.ProductID;
                        switch (pid)
                        {
                            case 12: //Purple x-waranty shipping
                            case 19: //Platinum x-warranty
                            case 118: //no min shipping for platinum 1 year x-warranty
                            case 119: //no min shipping for platinum 3 year x-warranty
                            case 120: //no min shipping for platinum 5 year x-warranty
                                groundShipping += 0;
                                break;

                            default:
                                NoShipping = false; //OK, now we have s&h $$ to be calculated
                                if (i.LineItemProduct.PerUnitShipping != 0)
                                {
                                    groundShipping += i.LineItemProduct.PerUnitShipping * i.Quantity;
                                }
                                else
                                {
                                    groundShipping += .1M * (i.Quantity * i.LineItemProduct.SalePrice);
                                };
                                break;
                        }
                    }
                    else { NoShipping = true; } //subscription products don't get shipping
                }


                //adjust for minshipping . for now exclude fm unit & x-warranty

                bool MinShippingProductTrigger = false;

                foreach (Item i in Items)
                {
                    if (!NoShipping)
                    {
                        int pid = i.LineItemProduct.ProductID;
                        switch (pid)
                        {
                            case 1: //no min shipping for FM
                            case 12: //no min shipping for purple x-warranty
                            case 19: //no min shipping for platinum x-warranty
                            case 118: //no min shipping for platinum 1 year x-warranty
                            case 119: //no min shipping for platinum 3 year x-warranty
                            case 120: //no min shipping for platinum 5 year x-warranty
                                break;

                            case 13: //no min shipping for FM+serum
                                MinShippingProductTrigger = false; 
                                break;

                            case 14: //no min shipping for Platinum
                                MinShippingProductTrigger = false;
                                break;

                            default: 
                                MinShippingProductTrigger = true;
                                break;
                        }
                    }

                }

                if ((groundShipping < AdminCart.Config.MinimumShipping) && MinShippingProductTrigger && !NoShipping)
                {
                    groundShipping = AdminCart.Config.MinimumShipping;
                }

            }

            string cid = ShipAddress.Country;

            if (!NoShipping)
            {
                switch (cid)
                {
                    case "":
                        groundShipping = 0; //unknown country
                        ShipType = 1; //groundShipping if unknown country
                        TotalShipping = groundShipping;
                        break;

                    case "CA":
                        groundShipping += 6; //Canada = US + $6; except x-warranty
                        ShipType = 1; // only ground to Canada
                        TotalShipping = groundShipping;
                        break;

                    case "US":
                        groundShipping += 0; //just leave the calc alone
                        //adjust for free shipping
                        //adjust for free shipping
                        if (HttpContext.Current.Session["sset"] != null) //first, check for any site specials
                        {
                            int ssetID = Convert.ToInt32(HttpContext.Current.Session["sset"].ToString());
                            AdminCart.SiteSettings sset = new AdminCart.SiteSettings(ssetID);
                            if (sset.SiteSettingsID > 0) //make sure it's legit
                            {
                                if (sset.SiteSettingsFreeShipping && (TotalProduct >= sset.SiteSettingsMinProduct))
                                {
                                    groundShipping = 0;
                                    if (SiteOrder.SiteSettingsID <= 0) { SiteOrder.SiteSettingsID = sset.SiteSettingsID; }
                                }
                            }
                        }

                        string sql = @"
                        select * 
                        from sitesettings 
                        where SiteSettingsName = 'FreeShipping' 
                        and getdate() between SiteSettingsStartDate and SiteSettingsEndDate";
                        SqlDataReader dr = DBUtil.FillDataReader(sql);
                        if (dr != null)
                        {
                            while (dr.Read())
                            {
                                decimal minProduct = 0;
                                int ssetID = 0;
                                bool isID = int.TryParse(dr["siteSettingsID"].ToString(), out ssetID);
                                bool isMin = Decimal.TryParse(dr["siteSettingsMinProduct"].ToString(), out minProduct);
                                if (TotalProduct >= minProduct)
                                {
                                    groundShipping = 0;
                                    if (SiteOrder.SiteSettingsID <= 0) { SiteOrder.SiteSettingsID = ssetID; }
                                }
                            }
                        }
                        if (dr != null) { dr.Close(); }

                        //set total based on type

                        if (shipType == 1)
                        {
                            TotalShipping = GroundShipping;
                        }
                        if (ShipType == 2)
                        {
                            TotalShipping = GroundShipping + expeditedUpcharge;
                        }
                        if (ShipType == 3)
                        {
                            TotalShipping = GroundShipping + expressUpcharge;
                        }

                        break;

                    default:
                        groundShipping = 40; //must be non-US and non-CA
                        shipType = 1; //only Ground is available!
                        TotalShipping = groundShipping;
                        break;
                }
            }

            //overide if CS has decided to charge an arbitrary shipping value
            if (CSShipping < 0)
            { }
            else { TotalShipping = Convert.ToDecimal(CSShipping); }
        }

        private void CalculateShipping()
        {
            //DEFAULT VALUES
            TotalShipping = 0;

            groundShipping = 0;
            expeditedShipping = 0;
            expressShipping = 0;

            //decimal expeditedUpcharge = 8; // US Only
            //decimal expressUpcharge = 16;  // US only

            decimal shippableProduct = TotalProduct;
            Boolean isSubscription = false;
            foreach (Item i in Items)
            {
                if (!i.LineItemProduct.IsShippable)
                {
                    shippableProduct = shippableProduct - i.LineItemTotal; //substract out items that don't ship (like warranties)
                }
                if (i.LineItemProduct.IsSubscription)
                {
                    isSubscription = true;
                }
            }


            if (ShipAddress.Country == "") { groundShipping = 0; }
            else //assume country is US and calculate shipping
            {
                switch(ShipAddress.Country.ToUpper())
                {
                    case "US":
                        switch(Config.appName.ToUpper())
                        {
                            case "FACEMASTER":
                                {
                                    if (isSubscription)
                                    {
                                        GroundShipping = 0; //multi-pay and subscriptions in the US are free
                                    }
                                    else if (shippableProduct > 0)
                                    {
                                        if (shippableProduct < 50.00M) { groundShipping = 4.99M; }
                                        else if ((shippableProduct >= 50.00M) && (shippableProduct < 95.00M)) { groundShipping = 8.99M; }
                                        else if (shippableProduct >= 95.00M) { groundShipping = 0M; }
                                    }
                                    TotalShipping = GroundShipping;
                                }
                                break;

                            case "CRYSTALIFT":
                                {
                                    if (isSubscription)
                                    {
                                        GroundShipping = 0; //multi-pay and subscriptions in the US are free
                                    }
                                    else if (shippableProduct > 0)
                                    {
                                        if (shippableProduct < 98.00M) { groundShipping = 6.95M; }
                                        else if (shippableProduct >= 98.00M) { groundShipping = 0M; }
                                    }
                                    TotalShipping = GroundShipping;
                                }
                                break;

                            default:
                                {
                                    if (isSubscription)
                                    {
                                        GroundShipping = 0; //multi-pay and subscriptions in the US are free
                                    }
                                    else if (shippableProduct > 0) { groundShipping = 8.88M; }
                                    TotalShipping = GroundShipping;
                                }
                                break;
                        }
                        break;

                    case "CA":
                        switch (Config.appName.ToUpper())
                        {
                            case "FACEMASTER":
                                {
                                    if (shippableProduct > 0)
                                    {
                                        if (shippableProduct < 25.00M) { groundShipping = 11.99M; }
                                        else if ((shippableProduct >= 25.00M) && (shippableProduct < 50.00M)) { groundShipping = 18.99M; }
                                        else if ((shippableProduct >= 50.00M) && (shippableProduct < 150.00M)) { groundShipping = 23.99M; }
                                        else if (shippableProduct >= 150.00M) { groundShipping = 11.99M; }
                                    }
                                    TotalShipping = GroundShipping;
                                }
                                break;

                            case "CRYSTALIFT":
                                {
                                    if (shippableProduct > 0) { groundShipping = 14.99M; }
                                    TotalShipping = 14.99M;
                                }
                                break;

                            default:
                                {
                                    if (shippableProduct > 0) { groundShipping = 18.88M; }
                                    TotalShipping = GroundShipping;
                                }
                                break;
                        }
                        break;

                    case "AU":
                        switch (Config.appName.ToUpper())
                        {
                            case "FACEMASTER":
                                {
                                    if (shippableProduct > 0) { groundShipping = 29.99M; }
                                    TotalShipping = GroundShipping;
                                }
                                break;

                            case "CRYSTALIFT":
                                {
                                    if (shippableProduct > 0) { groundShipping = 39.99M; }
                                    TotalShipping = GroundShipping;
                                }
                                break;

                            default:
                                {
                                    if (shippableProduct > 0) { groundShipping = 38.88M; }
                                    TotalShipping = GroundShipping;
                                }
                                break;
                        }
                        break;

                    default:
                        switch (Config.appName.ToUpper())
                        {
                            case "FACEMASTER":
                                {
                                    if (shippableProduct > 0) { groundShipping = 39.99M; }
                                    TotalShipping = GroundShipping;
                                }
                                break;

                            case "CRYSTALIFT":
                                {
                                    if (shippableProduct > 0) { groundShipping = 39.99M; }
                                    TotalShipping = GroundShipping;
                                }
                                break;

                            default:
                                {
                                    if (shippableProduct > 0) { groundShipping = 38.88M; }
                                    TotalShipping = GroundShipping;
                                }
                                break;
                        }
                        break;

                }

            }

            //overide if CS has decided to charge an arbitrary shipping value
            if (CSShipping >= 0) { TotalShipping = Convert.ToDecimal(CSShipping); }
        }

        private void CalculateTax()
        {
            totalTax = 0;
            if (this.ShipAddress.State == "CA")
            {
                totalTax = (totalProduct - totalDiscounts) * .0900M;
            }
        }

        private void CalculateTotal()
        {
            total = totalProduct - totalDiscounts + totalTax + totalShipping;
        }

        private void CalculateDiscounts()
        {
            totalDiscounts = 0;

            if (SiteCoupon.CouponCodeID != 0)
            {
                if (totalProduct >= SiteCoupon.CouponOrderMinimum)
                {
                    TotalDiscounts = (Decimal.Parse(SiteCoupon.CouponAmount) / 100) * totalProduct;
                }
            }
            else
            {
                //now check for a discount that goes with FreeShipping
                string sql = @"
                        select * 
                        from sitesettings 
                        where ((SiteSettingsName = 'FreeShipping') or (SiteSettingsName = 'siteDiscount')) 
                        and getdate() between SiteSettingsStartDate and SiteSettingsEndDate";
                SqlDataReader dr = DBUtil.FillDataReader(sql);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        int ssID = Convert.ToInt32(dr["SiteSettingsID"]);
                        AdminCart.SiteSettings siteSetting = new AdminCart.SiteSettings(ssID);
                        if (TotalProduct >= siteSetting.SiteSettingsMinProduct)
                        {
                            switch (siteSetting.SiteSettingsTrigger)
                            {
                                case "0":
                                    break;

                                case "1":
                                    totalDiscounts = totalProduct - Convert.ToDecimal(siteSetting.SiteSettingsValue);
                                    break;

                                case "2":
                                    totalDiscounts = totalProduct * (Convert.ToDecimal(siteSetting.SiteSettingsValue) / 100);
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                    dr.Close();
                }
            }
        }

        public string GetVersion()
        {
            return "FaceMaster Admin Cart Version 4.0.0.0";
        }

        public int Charge()
        {
            int Result = -1;
            bool NoCharge = (total == 0);
            string chargeType = "S";

                //process live
                Tran.TransactionAmount = Total;
                Tran.Comment2 = OrderID.ToString();
                Tran.TrxType = chargeType;
                Tran.CardType = Tran.GetCardTypeByNumber(Tran.CardNo);
                Tran.OrderID = OrderID;
                Tran.CardHolder = SiteCustomer.FirstName + ' ' + SiteCustomer.LastName;
                Tran.CustomerCode = AdminCart.Config.AppCode() + SiteCustomer.CustomerID;
                Tran.Address = BillAddress;
                Tran.TransactionDate = DateTime.Now;

                string gateway = ConfigurationManager.AppSettings["PaymentGateWay"].ToString();
                if (gateway == "PayPal")
                {
                    Result = PayPalCharge(chargeType, this.Tran, OrderID, this.SiteOrder.Total, this.SiteCustomer, this.BillAddress);
                }
                else if (gateway == "Stripe")
                {
                    Result = StripeCharge(chargeType, this.Tran, OrderID, this.SiteOrder.Total, this.SiteCustomer, this.BillAddress);
                }

            return Result;
        }

        public int Charge(string chargeType)
        {
            int Result = -1;
            bool NoCharge = (total == 0);

            //process live
            Tran.TransactionAmount = Total;
            Tran.Comment2 = OrderID.ToString();
            Tran.TrxType = chargeType;
            Tran.CardType = Tran.GetCardTypeByNumber(Tran.CardNo);
            Tran.OrderID = OrderID;
            Tran.CardHolder = SiteCustomer.FirstName + ' ' + SiteCustomer.LastName;
            Tran.CustomerCode = AdminCart.Config.AppCode() + SiteCustomer.CustomerID;
            Tran.Address = BillAddress;
            Tran.TransactionDate = DateTime.Now;

            string gateway = ConfigurationManager.AppSettings["PaymentGateWay"].ToString();
            if (gateway == "PayPal")
            {
                Result = PayPalCharge(chargeType, this.Tran, OrderID, this.SiteOrder.Total, this.SiteCustomer, this.BillAddress);
            }
            else if (gateway == "Stripe")
            {
                Result = StripeCharge(chargeType, this.Tran, OrderID, this.SiteOrder.Total, this.SiteCustomer, this.BillAddress);
            }

            return Result;
        }

        public int Credit(string OrigID)
        {
            int Result = -1;

            string gateway = ConfigurationManager.AppSettings["PaymentGateWay"].ToString();
            if (gateway == "PayPal")
            {
                Result = PayPalCredit(OrigID, this.Tran, OrderID, this.SiteOrder.Total, this.SiteCustomer, this.BillAddress);
            }
            else if (gateway == "Stripe")
            {
                Result = StripeCredit(OrigID, this.Tran, OrderID, this.SiteOrder.Total, this.SiteCustomer, this.BillAddress);
            }
            return Result;
        }

        public int Credit(string OrigID, int sourceID)
        {
            int Result = -1;

            string gateway = ConfigurationManager.AppSettings["PaymentGateWay"].ToString();
            if (gateway == "PayPal")
            {
                Result = PayPalCredit(OrigID, this.Tran, OrderID, this.SiteOrder.Total, this.SiteCustomer, this.BillAddress);
            }
            else if (gateway == "Stripe")
            {
                if (sourceID == -3)
                {
                    Result = ShopifyCredit(OrigID, this.Tran, OrderID, this.SiteOrder.Total, this.SiteOrder.SSOrderID.ToString(), this.SiteCustomer, this.BillAddress);
                    if (Result != 0) // if it failed at Shopify then try Stripe, could be multi-pay or subscription
                    {
                        Result = StripeCredit(OrigID, this.Tran, OrderID, this.SiteOrder.Total, this.SiteCustomer, this.BillAddress);
                    }
                }
                else //most likely a FM Admin order; go straight to Stripe
                {
                    Result = StripeCredit(OrigID, this.Tran, OrderID, this.SiteOrder.Total, this.SiteCustomer, this.BillAddress);
                }
            }
            return Result;
        }

        public int Void(string OrigID)
        {
            //voids have been obsoleted
            int Result = -1;
            Tran.ResultCode = -1;
            Tran.ResultMsg = "Void is not implemented for Stripe; Please do a Refund";
            return Result;
        }

        public int SubscriptionProcessing(string promoCodeID)
        {
            //int result = 0; //assume transaction failure
            string respMsg = "";

            //get Stripe Customer via FM email address
            string email = this.SiteCustomer.Email;
            string stripeCustomerID = GetStripeCustomerId(email);
            if (string.IsNullOrEmpty(stripeCustomerID))
            {
                return -1;
            }

            // ************************* CREATE STRIPE METHODS FOR THIS STUFF **************
            //get the customer from Stripe
            StripeCustomerService customerService = new StripeCustomerService();
            StripeCustomer stripeCustomer = new StripeCustomer();

            try
            {
               stripeCustomer = customerService.Get(stripeCustomerID); 
            }
            catch (StripeException ex)
            {
                respMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Cart: Get StripeCustomer: Stripe Error");
                HttpContext.Current.Session["subErrorMessage"] = respMsg;
                return -1; // return w/ error
            }
            //get a credit card token for this subscription purchase
            StripeTokenCreateOptions myToken = new StripeTokenCreateOptions
            {
                Card = new StripeCreditCardOptions
                {
                    Name = this.Tran.CardHolder,
                    Number = this.Tran.CardNo,
                    ExpirationYear = Convert.ToInt32(DateTime.Now.Year.ToString().Substring(0, 2) + this.Tran.ExpDate.Substring(Tran.ExpDate.Length - 2)),
                    ExpirationMonth = Convert.ToInt32(this.Tran.ExpDate.Substring(0, 2)),
                    Cvc = this.Tran.CvsCode
                }
            }; //create a token from the customers card
            StripeToken stripeToken = new StripeToken();
            try
            {
                StripeTokenService tokenService = new StripeTokenService();
                stripeToken = tokenService.Create(myToken);
            }
            catch (StripeException ex)
            {
                respMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Cart: Get Token: Stripe Error");
                HttpContext.Current.Session["subErrorMessage"] = respMsg;
                return -1;
            }
            //update the Stripe customer with this token
            var myCustomer = new StripeCustomerUpdateOptions
            {
                SourceToken = stripeToken.Id //************
            };

            bool isSubscription = (HttpContext.Current.Session["RequestIsSubscribe"] == null) ? true : Convert.ToBoolean(HttpContext.Current.Session["RequestIsSubscribe"]);
            if (isSubscription)
            {
                decimal setupFee = (HttpContext.Current.Session["setupFee"] == null) ? 0.00M : Convert.ToDecimal(HttpContext.Current.Session["setupFee"]);
                setupFee = setupFee + TotalTax + TotalShipping;
                myCustomer.AccountBalance = Convert.ToInt32((setupFee * 100)); //establish a setupFee by initializing the customer account balance
            }
            try
            {
                stripeCustomer = customerService.Update(stripeCustomer.Id, myCustomer, null);
            }
            catch (StripeException ex)
            {               
                respMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Cart: Authorize Credit Card Purchase: Stripe Error");
                HttpContext.Current.Session["subErrorMessage"] = respMsg;
                return -1;
            }
            if (!isSubscription) //here's where a one-time charge is made; like for a past-due notice
            {
                string stripeInvoiceID = (HttpContext.Current.Session["StripeInvoiceID"] == null) ? "0" : HttpContext.Current.Session["StripeInvoiceID"].ToString();
                var invoiceService = new StripeInvoiceService();
                StripeInvoice stripeInvoice = new StripeInvoice();
                try
                {
                    stripeInvoice = invoiceService.Get(stripeInvoiceID);
                }
                catch (StripeException ex)
                {
                    respMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Cart: Stripe Error: (card updated) Get Invoice");
                    HttpContext.Current.Session["subErrorMessage"] = respMsg;
                    return -1;
                }
                if ((stripeInvoice.Closed != true) && (stripeInvoice.AmountDue != 0))
                {
                    //StripeInvoicePayOptions stripeInvoicePayOptions = new StripeInvoicePayOptions()
                    //{
                    //    SourceId = stripeCustomer.DefaultSource.Id,
                    //};
                    try
                    {
                        stripeInvoice = invoiceService.Pay(stripeInvoiceID);
                        //stripeInvoice = invoiceService.Pay(stripeInvoiceID, stripeInvoicePayOptions, null);
                    }
                    catch (StripeException ex)
                    {
                        respMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Cart: Stripe Error: (card updated) Paying Invoice");
                        HttpContext.Current.Session["subErrorMessage"] = respMsg;
                        return -1;
                    }
                }
                HttpContext.Current.Session["subErrorMessage"] = ""; //credit card was updated & possibly charged w/ no error
                return 0;
            }
            var planService = new StripePlanService();
            IEnumerable<StripePlan> planList = planService.List(new StripePlanListOptions { Limit = 50 }); // optional StripeListOptions

            //now we have a PayWhirl subscriber & a Stripe customer
            StripeSubscription stripeSubscription = new StripeSubscription();
            foreach (Item i in Items)
            {
                string plan = i.LineItemProduct.SubscriptionPlan;
                StripePlan stripePlan = new StripePlan();
                try
                {
                    //StripePlan stripePlan = planService.Get(plan); //verify the plan exists out in Stripe-land
                    stripePlan = (from StripePlan in planList
                                 where (StripePlan.Id.ToUpper() == plan.ToUpper())
                                 select StripePlan).Single();
                }
                catch (Exception ex)
                {
                    string errMessage = "Cart: Error Finding Subscription Plan: ";
                    errMessage += (string.IsNullOrEmpty(ex.Message)) ? "" :  ex.Message;
                    HttpContext.Current.Session["subErrorMessage"] = errMessage;
                    return -1;
                }

                var subscriptionService = new StripeSubscriptionService();
                var subCreateOptions = new StripeSubscriptionCreateOptions()
                {
                    CustomerId = stripeCustomer.Id,
                };
                subCreateOptions.Items = new List<StripeSubscriptionItemOption>();
                StripeSubscriptionItemOption item = new StripeSubscriptionItemOption()
                {
                    Quantity = 1,
                    PlanId = stripePlan.Id,
                };
                subCreateOptions.Items.Add(item);

                subCreateOptions.CouponId = (!string.IsNullOrEmpty(promoCodeID)) ? promoCodeID : string.Empty;

                try
                {
                    stripeSubscription = subscriptionService.Create(stripeCustomer.Id, subCreateOptions, null);
                    //stripeSubscription = subscriptionService.Create(subCreateOptions, null);
                }
                catch (StripeException ex)
                {
                    respMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "CCart: Create Subscription: Stripe Error");
                    HttpContext.Current.Session["subErrorMessage"] = respMsg;
                    return -1;
                }
            }

            HttpContext.Current.Session["subErrorMessage"] = "";
            return 0; //if we get here then the new subscription was created w/o error
        }

        private string GetStripeCustomerId(string email)
        {
            PayWhirl.PayWhirlSubscriber pwSubScriber = new PayWhirl.PayWhirlSubscriber();
            try
            {
                //pwSubScriber = PayWhirl.PayWhirlSubscriber.FindSubscriberByEmail(email);
                pwSubScriber = PayWhirl.PayWhirlBiz.FindSubscriberByEmail(email);
            }
            catch (Exception ex)
            {
                //some exception handling stuff here
                string respMsg = "Cart: Error Finding PayWhirl Customer: " + ex.Message;
                HttpContext.Current.Session["subErrorMessage"] = respMsg;
                return string.Empty; // return w/ error
            }

            if (pwSubScriber.Id == 0) //wasn't located in the PayWhirl world so create a new subscriber
            {
                pwSubScriber = new PayWhirl.PayWhirlSubscriber
                {
                    Email = email,
                    First_name = this.SiteCustomer.FirstName,
                    Last_name = this.SiteCustomer.LastName,
                    Address_1 = this.ShipAddress.Street,
                    Address_2 = this.ShipAddress.Street2,
                    City = this.ShipAddress.City,
                    State = this.ShipAddress.State,
                    Country = this.ShipAddress.Country,
                    Zip = this.ShipAddress.Zip,
                    Phone = this.SiteCustomer.Phone
                };

                try
                {
                    pwSubScriber = PayWhirl.PayWhirlSubscriber.CreateSubscriber(pwSubScriber);
                }
                catch (Exception ex)
                {
                    //some exception handling stuff here
                    string respMsg = "Cart: Error Creating PayWhirl Customer: " + ex.Message;
                    HttpContext.Current.Session["subErrorMessage"] = respMsg;
                    return string.Empty; // return w/ error
                }
            }
            return pwSubScriber.Stripe_id;
        }

        public void SaveToOrder()
        {
            SqlConnection conn = new SqlConnection(Config.ConnStr());
            string sql = @"INSERT Orders(CartID, CustomerID, OrderDate, Total, Adjust, 
                    TotalShipping, TotalTax,TotalCoupon,Status,
                    ShippingAddressID, BillingAddressID, SourceID, CompanyID, Phone, SuzanneSomersOrderID, 
                    ShipMode, AffiliateOrderDetailID, SiteSettingsID)
                   VALUES(@CartID, @CustomerID, @OrderDate, @Total, @Adjust,
                    @TotalShipping, @TotalTax, @TotalCoupon, 'New',
                    @ShipAddressID, @BillAddressID, @SourceID, @CompanyID, @Phone, 
                    @SuzanneSomersOrderID, @ShipMode, @AffiliateOrderDetailID, @SiteSettingsID);  select OrderID=@@identity;";

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlParameter CartIDParam = new SqlParameter("@CartID", SqlDbType.UniqueIdentifier);
            SqlParameter CustomerIDParam = new SqlParameter("@CustomerID", SqlDbType.Int);
            SqlParameter OrderDateParam = new SqlParameter("@OrderDate", SqlDbType.DateTime);
            SqlParameter TotalParam = new SqlParameter("@Total", SqlDbType.Money);

            SqlParameter TotalShippingParam = new SqlParameter("@TotalShipping", SqlDbType.Money);
            SqlParameter TotalTaxParam = new SqlParameter("@TotalTax", SqlDbType.Money);
            SqlParameter TotalCouponParam = new SqlParameter("@TotalCoupon", SqlDbType.Money);
            SqlParameter AdjustParam = new SqlParameter("@Adjust", SqlDbType.Money);
            SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.VarChar);
            SqlParameter ShipAddressIDParam = new SqlParameter("@ShipAddressID", SqlDbType.Int);
            SqlParameter BillAddressIDParam = new SqlParameter("@BillAddressID", SqlDbType.Int);
            SqlParameter SourceIDParam = new SqlParameter("@SourceID", SqlDbType.Int);
            SqlParameter CompanyIDParam = new SqlParameter("@CompanyID", SqlDbType.Int);
            SqlParameter PhoneParam = new SqlParameter("@Phone", SqlDbType.Bit);
            SqlParameter SuzanneSomersOrderIDParam = new SqlParameter("@SuzanneSomersOrderID", SqlDbType.Int);
            SqlParameter ShipModeParam = new SqlParameter("@ShipMode", SqlDbType.Int);
            SqlParameter AffiliateOrderDetailIDParam = new SqlParameter("@AffiliateOrderDetailID", SqlDbType.Int);
            SqlParameter SiteSettingsIDParam = new SqlParameter("@SiteSettingsID", SqlDbType.Int);

            CartIDParam.Value = CartID;
            CustomerIDParam.Value = SiteCustomer.CustomerID;
            //OrderDateParam.Value = DateTime.Now;
            OrderDateParam.Value = SiteOrder.OrderDate;
            TotalParam.Value = Total;

            TotalShippingParam.Value = TotalShipping;
            TotalTaxParam.Value = TotalTax;
            TotalCouponParam.Value = TotalDiscounts;
            AdjustParam.Value = SiteOrder.Adjust;

            StatusParam.Value = "New";
            ShipAddressIDParam.Value = SiteOrder.ShippingAddressID;
            BillAddressIDParam.Value = SiteOrder.BillingAddressID;
            SourceIDParam.Value = SiteOrder.SourceID;
            CompanyIDParam.Value = SiteOrder.CompanyID;
            PhoneParam.Value = SiteOrder.ByPhone;
            SuzanneSomersOrderIDParam.Value = SiteOrder.SSOrderID;
            ShipModeParam.Value = SiteOrder.ShipMode;
            AffiliateOrderDetailIDParam.Value = SiteOrder.AffiliateOrderDetailID;
            SiteSettingsIDParam.Value = SiteOrder.SiteSettingsID;

            cmd.Parameters.Add(CartIDParam);
            cmd.Parameters.Add(CustomerIDParam);
            cmd.Parameters.Add(OrderDateParam);
            cmd.Parameters.Add(TotalParam);
            cmd.Parameters.Add(TotalShippingParam);
            cmd.Parameters.Add(TotalTaxParam);
            cmd.Parameters.Add(TotalCouponParam);
            cmd.Parameters.Add(AdjustParam);
            cmd.Parameters.Add(StatusParam);
            cmd.Parameters.Add(ShipAddressIDParam);
            cmd.Parameters.Add(BillAddressIDParam);
            cmd.Parameters.Add(SourceIDParam);
            cmd.Parameters.Add(CompanyIDParam);
            cmd.Parameters.Add(PhoneParam);
            cmd.Parameters.Add(SuzanneSomersOrderIDParam);
            cmd.Parameters.Add(ShipModeParam);
            cmd.Parameters.Add(AffiliateOrderDetailIDParam);
            cmd.Parameters.Add(SiteSettingsIDParam);

            conn.Open();
            int result = 0;

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                result = Convert.ToInt32(dr["OrderID"]);
            }
            else
            {
                result = -99;
            }
            OrderID = result;

            //next, save all order details
            foreach (Item i in Items) 
            {
                OrderDetail od = new OrderDetail();
                od.OrderID = OrderID;
                od.ProductID = i.LineItemProduct.ProductID;

                //od.UnitPrice = i.LineItemProduct.SalePrice;
                od.UnitPrice = i.LineItemProduct.Price;

                //od.Discount = i.LineItemAdjustment;
                od.Discount = (i.LineItemProduct.Price - i.LineItemProduct.SalePrice) + i.LineItemAdjustment;

                od.Quantity = i.Quantity;

                ProductCost pcost = new ProductCost(i.LineItemProduct.ProductID);
                od.UnitCost = pcost.Cost;

                od.SaveToOrderDetail();
            }
        }

        public void UpdateOrder()
        {
            SqlConnection conn = new SqlConnection(Config.ConnStr());
            string sql = @"
                UPDATE Orders
                SET
                    CartID = @CartID,
                    CustomerID = @CustomerID, 
                    OrderDate = @OrderDate, 
                    Total = @Total, 
                    Adjust = @Adjust, 
                    TotalShipping = @TotalShipping, 
                    TotalTax = @TotalTax,
                    TotalCoupon = @TotalCoupon,
                    Status = @Status,
                    ShippingAddressID = @ShippingAddressID, 
                    BillingAddressID = @BillingAddressID, 
                    SourceID = @SourceID, 
                    CompanyID = @CompanyID, 
                    Phone = @Phone, 
                    SuzanneSomersOrderID = @SuzanneSomersOrderID, 
                    ShipMode = @ShipMode, 
                    AffiliateOrderDetailID = @AffiliateOrderDetailID, 
                    SiteSettingsID = @SiteSettingsID 
                WHERE OrderID = @OrderID
                ";

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlParameter OrderIDParam = new SqlParameter("@OrderID", SqlDbType.Int);
            SqlParameter CartIDParam = new SqlParameter("@CartID", SqlDbType.UniqueIdentifier);
            SqlParameter CustomerIDParam = new SqlParameter("@CustomerID", SqlDbType.Int);
            SqlParameter OrderDateParam = new SqlParameter("@OrderDate", SqlDbType.DateTime);
            SqlParameter TotalParam = new SqlParameter("@Total", SqlDbType.Money);

            SqlParameter TotalShippingParam = new SqlParameter("@TotalShipping", SqlDbType.Money);
            SqlParameter TotalTaxParam = new SqlParameter("@TotalTax", SqlDbType.Money);
            SqlParameter TotalCouponParam = new SqlParameter("@TotalCoupon", SqlDbType.Money);
            SqlParameter AdjustParam = new SqlParameter("@Adjust", SqlDbType.Money);
            SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.VarChar);
            SqlParameter ShipAddressIDParam = new SqlParameter("@ShippingAddressID", SqlDbType.Int);
            SqlParameter BillAddressIDParam = new SqlParameter("@BillingAddressID", SqlDbType.Int);
            SqlParameter SourceIDParam = new SqlParameter("@SourceID", SqlDbType.Int);
            SqlParameter CompanyIDParam = new SqlParameter("@CompanyID", SqlDbType.Int);
            SqlParameter PhoneParam = new SqlParameter("@Phone", SqlDbType.Bit);
            SqlParameter SuzanneSomersOrderIDParam = new SqlParameter("@SuzanneSomersOrderID", SqlDbType.Int);
            SqlParameter ShipModeParam = new SqlParameter("@ShipMode", SqlDbType.Int);
            SqlParameter AffiliateOrderDetailIDParam = new SqlParameter("@AffiliateOrderDetailID", SqlDbType.Int);
            SqlParameter SiteSettingsIDParam = new SqlParameter("@SiteSettingsID", SqlDbType.Int);

            OrderIDParam.Value = OrderID;
            CartIDParam.Value = CartID;
            CustomerIDParam.Value = SiteCustomer.CustomerID;
            //OrderDateParam.Value = DateTime.Now;
            OrderDateParam.Value = SiteOrder.OrderDate;
            TotalParam.Value = SiteOrder.Total;

            TotalShippingParam.Value = SiteOrder.TotalShipping;
            TotalTaxParam.Value = SiteOrder.TotalTax;
            TotalCouponParam.Value = SiteOrder.TotalCoupon;
            AdjustParam.Value = SiteOrder.Adjust;

            StatusParam.Value = "New";
            ShipAddressIDParam.Value = SiteOrder.ShippingAddressID;
            BillAddressIDParam.Value = SiteOrder.BillingAddressID;
            SourceIDParam.Value = SiteOrder.SourceID;
            CompanyIDParam.Value = SiteOrder.CompanyID;
            PhoneParam.Value = SiteOrder.ByPhone;
            SuzanneSomersOrderIDParam.Value = SiteOrder.SSOrderID;
            ShipModeParam.Value = SiteOrder.ShipMode;
            AffiliateOrderDetailIDParam.Value = SiteOrder.AffiliateOrderDetailID;
            SiteSettingsIDParam.Value = SiteOrder.SiteSettingsID;

            cmd.Parameters.Add(OrderIDParam);
            cmd.Parameters.Add(CartIDParam);
            cmd.Parameters.Add(CustomerIDParam);
            cmd.Parameters.Add(OrderDateParam);
            cmd.Parameters.Add(TotalParam);
            cmd.Parameters.Add(TotalShippingParam);
            cmd.Parameters.Add(TotalTaxParam);
            cmd.Parameters.Add(TotalCouponParam);
            cmd.Parameters.Add(AdjustParam);
            cmd.Parameters.Add(StatusParam);
            cmd.Parameters.Add(ShipAddressIDParam);
            cmd.Parameters.Add(BillAddressIDParam);
            cmd.Parameters.Add(SourceIDParam);
            cmd.Parameters.Add(CompanyIDParam);
            cmd.Parameters.Add(PhoneParam);
            cmd.Parameters.Add(SuzanneSomersOrderIDParam);
            cmd.Parameters.Add(ShipModeParam);
            cmd.Parameters.Add(AffiliateOrderDetailIDParam);
            cmd.Parameters.Add(SiteSettingsIDParam);

            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();

        }

        public List<Cart> FindAllCarts(string orderid, string lastName, string serialNum, string customerID, bool fullDetails)
        {
            List<Cart> cartList = new List<Cart>();
            List<Order> orderList = Order.FindAllOrders(orderid, lastName, serialNum, customerID, false);
            foreach (Order o in orderList)
            {
                Cart cart = GetCartFromOrder(o, false);
                cartList.Add(cart);
            }

            return cartList;
        }

        public List<Cart> FindAllCarts(string lastName, string email, bool fullDetails)
        {
            List<Cart> cartList = new List<Cart>();
            List<Order> orderList = Order.FindAllOrders(lastName, email, false);
            foreach (Order o in orderList)
            {
                Cart cart = GetCartFromOrder(o, false);
                cartList.Add(cart);
            }

            return cartList;
        }

        public List<Cart> FindAllCarts(DateTime startDate, DateTime endDate, bool fullDetails)
        {
            List<Cart> cartList = new List<Cart>();
            List<Order> orderList = Order.FindAllOrders(startDate, endDate, fullDetails);
            foreach (Order o in orderList)
            {
                Cart cart = GetCartFromOrder(o, false);
                cartList.Add(cart);
            }

            return cartList;
        }

        public void Clear()
        {
            Items = null;
        }

        public static int PayPalCharge(string chargeType, AdminCart.Transaction Tran, int OrderID, decimal Total, AdminCart.Customer SiteCustomer, AdminCart.Address BillAddress)
        {
            int Result = -1;
            string RespMsg = "";
            bool NoCharge = (Total == 0);

            UserInfo User = new UserInfo(AdminCart.Config.PayPalUser, AdminCart.Config.PayPalVendor,
                AdminCart.Config.PayPalPartner, AdminCart.Config.PayPalPassword);

            string server = AdminCart.Config.PayPalServer;
            if (AdminCart.Config.CartSetting == "Test")
            {
                server = AdminCart.Config.PayPalTestServer;
            }


            Invoice Inv = new Invoice();

            // Set the amount object.

            Currency Amt = new Currency(Total, "840"); // 840 is US ISO currency code.  If no code passed, 840 is default.
            Amt.NoOfDecimalDigits = 2;
            Amt.Round = true;
            Inv.Amt = Amt;

            //Inv.InvNum = theOrderID.ToString();
            //Inv.CustRef = theOrderID.ToString();
            Inv.Comment2 = OrderID.ToString();

            // Create the BillTo object.
            BillTo Bill = new BillTo();

            Bill.FirstName = SiteCustomer.FirstName; //"Joe";
            Bill.LastName = SiteCustomer.LastName; //"Smith";
            Bill.Street = BillAddress.Street;// "123 Main St.";
            Bill.City = BillAddress.City; ;
            Bill.State = BillAddress.State;
            Bill.Zip = BillAddress.Zip;
            Bill.BillToCountry = BillAddress.Country;

            Bill.PhoneNum = SiteCustomer.Phone;
            Bill.Email = SiteCustomer.Email; //"Joe.Smith@anyemail.com";

            // Set the BillTo object into invoice.
            Inv.BillTo = Bill;

            ShipTo Ship = new ShipTo();

            // If billing and shipping details are the same, uncomment the following code.
            Ship = Bill.Copy();
            Inv.ShipTo = Ship;

            CreditCard CC = new CreditCard(Tran.CardNo, Tran.ExpDate);

            CC.Cvv2 = Tran.CvsCode;

            CardTender Card = new CardTender(CC);

            Tran.TransactionAmount = Total;
            Tran.Comment2 = OrderID.ToString();
            Tran.TrxType = chargeType;
            Tran.CardType = Tran.GetCardTypeByNumber(Tran.CardNo);

            Tran.OrderID = OrderID;

            Tran.CardHolder = SiteCustomer.FirstName + ' ' + SiteCustomer.LastName;
            Tran.CustomerCode = AdminCart.Config.AppCode() + SiteCustomer.CustomerID;
            Tran.Address = BillAddress;
            Tran.TransactionDate = DateTime.Now;

            if (!NoCharge)
            {
                //Using the PayPal SDK example
                string HostAddress = server;
                int HostPort = 443;
                int Timeout = 30;

                //PayflowConnectionData Connection = new PayflowConnectionData(server, 443, 45, "", 0, "", "", @"c:\program files\Payflow SDK for .Net\certs");
                PayflowConnectionData Connection = new PayflowConnectionData(HostAddress, HostPort, Timeout);

                // Create a new Sale Transaction.
                SaleTransaction Trans = new SaleTransaction(
                    User, Connection, Inv, Card, PayflowUtility.RequestId);

                // Submit the Transaction
                Response Resp = Trans.SubmitTransaction();
                if (Resp != null)
                {
                    // Get the Transaction Response parameters.
                    TransactionResponse TrxnResponse = Resp.TransactionResponse;
                    if (TrxnResponse != null)
                    {
                        Result = TrxnResponse.Result;
                        RespMsg = TrxnResponse.RespMsg;

                        Tran.ResultCode = TrxnResponse.Result;
                        Tran.AuthCode = TrxnResponse.AuthCode;
                        Tran.ResultMsg = TrxnResponse.RespMsg;
                        Tran.TransactionID = TrxnResponse.Pnref;
                        Tran.AvsAddr = TrxnResponse.AVSAddr;
                        Tran.AvsZip = TrxnResponse.AVSZip;
                        Tran.Iavs = TrxnResponse.IAVS;
                        Tran.SaveDB();
                    }
                }
                else
                {
                    Result = -1;
                    RespMsg = "Response = null; Charge transaction did not succeed";
                }
            }
            else
            {
                Result = 0;
                RespMsg = "Approved (No Charge)";

                Tran.ResultCode = 0;
                Tran.AuthCode = "NoCharge";
                Tran.ResultMsg = "Approved (No Charge)";
                Tran.TransactionID = "NoCharge";
                Tran.CardNo = "xxxxxxxxxxxx";
                Tran.AvsAddr = "";
                Tran.AvsZip = "";
                Tran.Iavs = "";
                Tran.SaveDB();
            }
            return Result;
        }

        public static int PayPalCredit(string origID, AdminCart.Transaction Tran, int OrderID, decimal Total, AdminCart.Customer SiteCustomer, AdminCart.Address BillAddress)
        {
            int Result = -1;
            string RespMsg = "";

            UserInfo User = new UserInfo(AdminCart.Config.PayPalUser, AdminCart.Config.PayPalVendor,
                AdminCart.Config.PayPalPartner, AdminCart.Config.PayPalPassword);

            string server = AdminCart.Config.PayPalServer;
            if (AdminCart.Config.CartSetting == "Test")
            {
                server = AdminCart.Config.PayPalTestServer;
            }


            Invoice Inv = new Invoice();

            // Set the amount object.

            Currency Amt = new Currency(Total, "840"); // 840 is US ISO currency code.  If no code passed, 840 is default.
            Amt.NoOfDecimalDigits = 2;
            Amt.Round = true;
            Inv.Amt = Amt;

            //Inv.InvNum = theOrderID.ToString();
            //Inv.CustRef = theOrderID.ToString();
            Inv.Comment2 = OrderID.ToString();

            // Create the BillTo object.
            BillTo Bill = new BillTo();

            Bill.FirstName = SiteCustomer.FirstName; //"Joe";
            Bill.LastName = SiteCustomer.LastName; //"Smith";
            Bill.Street = BillAddress.Street;// "123 Main St.";
            Bill.City = BillAddress.City; ;
            Bill.State = BillAddress.State;
            Bill.Zip = BillAddress.Zip;
            Bill.BillToCountry = BillAddress.Country;

            Bill.PhoneNum = SiteCustomer.Phone;
            Bill.Email = SiteCustomer.Email; //"Joe.Smith@anyemail.com";

            // Set the BillTo object into invoice.
            Inv.BillTo = Bill;

            ShipTo Ship = new ShipTo();

            // If billing and shipping details are the same, uncomment the following code.
            Ship = Bill.Copy();
            Inv.ShipTo = Ship;

            CreditCard CC = new CreditCard(Tran.CardNo, Tran.ExpDate);

            CC.Cvv2 = Tran.CvsCode;

            CardTender Card = new CardTender(CC);

            //Using the PayPal SDK example
            string HostAddress = server;
            int HostPort = 443;
            int Timeout = 30;

            //PayflowConnectionData Connection = new PayflowConnectionData(server, 443, 45, "", 0, "", "", @"c:\program files\Payflow SDK for .Net\certs");
            PayflowConnectionData Connection = new PayflowConnectionData(HostAddress, HostPort, Timeout);

            // Create a new Credit Transaction.
            CreditTransaction Trans = new CreditTransaction(
                origID, User, Connection, Inv, Card, PayflowUtility.RequestId);

            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                Result = TrxnResponse.Result;
                RespMsg = TrxnResponse.RespMsg;

                // use original TrxnResponse if passed w/o retry to old processor; otherwise use retry values
                if (TrxnResponse != null)
                {
                    Tran.TransactionAmount = Total;
                    Tran.Comment2 = OrderID.ToString();
                    Tran.TrxType = "C";
                    Tran.CardType = Tran.GetCardTypeByNumber(Tran.CardNo);

                    Tran.OrderID = OrderID;

                    Tran.CardHolder = SiteCustomer.FirstName + ' ' + SiteCustomer.LastName;
                    Tran.CustomerCode = "FM" + SiteCustomer.CustomerID;
                    Tran.Address = BillAddress;

                    Tran.ResultCode = TrxnResponse.Result;
                    Tran.AuthCode = (TrxnResponse.AuthCode == null) ? "n/a" : TrxnResponse.AuthCode;
                    Tran.ResultMsg = (TrxnResponse.RespMsg == null) ? "n/a" : TrxnResponse.RespMsg;
                    Tran.TransactionID = (TrxnResponse.Pnref == null) ? "n/a" : TrxnResponse.Pnref;
                    Tran.TransactionDate = DateTime.Now;

                    Tran.AvsAddr = (TrxnResponse.AVSAddr == null) ? "n/a" : TrxnResponse.AVSAddr;
                    Tran.AvsZip = (TrxnResponse.AVSZip == null) ? "n/a" : TrxnResponse.AVSZip;
                    Tran.Iavs = (TrxnResponse.IAVS == null) ? "n/a" : TrxnResponse.IAVS;

                    Tran.SaveDB();
                }

                // Get the Transaction Context
                Context TransCtx = Resp.TransactionContext;

                switch (Result)
                {
                    case 0:
                        RespMsg = "Your transaction was approved.";
                        break;
                    case 1: // Transaction Not Completed.
                        RespMsg = "There was an error processing your transaction.  Please contact Customer Service." + Environment.NewLine + "Error: " + Result;
                        break;
                    case 12:
                        RespMsg = "Your transaction was declined.";
                        break;
                    case 13:
                        RespMsg = "Your transaction was declined.";
                        break;
                    case 125:
                        RespMsg = "Your Transactions has been declined. Contact Customer Service.";
                        break;
                    case 126:
                        // Decline transaction if AVS fails.
                        if (TrxnResponse.AVSAddr != "Y" | TrxnResponse.AVSZip != "Y")
                            // Display message that transaction was not accepted.  At this time, you
                            // could redirect user to re-enter STREET and ZIP information.  
                            RespMsg = "Your Billing Information does not match.  Please re-enter.";
                        else
                            RespMsg = "Your Transactions is Under Review. We will notify you via e-mail if accepted.";
                        break;
                    case 127:
                        RespMsg = "Your Transactions is Under Review. We will notify you via e-mail if accepted.";
                        break;
                    default:
                        // Get the Transaction Context and check for any contained SDK specific errors (optional code).
                        if (TransCtx != null & TransCtx.getErrorCount() > 0)
                        {
                            Certnet.Mail.SendMail(TransCtx.ToString());
                        }
                        //label1.Text += Environment.NewLine + "Transaction Errors = " + TransCtx.ToString();
                        break;
                }


                // Check the Transaction Context for any contained SDK specific errors (optional code).
                if (TransCtx != null && TransCtx.getErrorCount() > 0)
                {
                    //  label1.Text+="TRANSACTION CONTEXT ERRORS";
                    //  label1.Text+=TransCtx.ToString();
                    //  label1.Text +="------------------------------------------------------";
                }

                return Result;
            }
            else
            {
                return -99;
            }

        }

        public static int StripeCharge(string chargeType, AdminCart.Transaction Tran, int OrderID, decimal Total, AdminCart.Customer SiteCustomer, AdminCart.Address BillAddress)
        {
            int Result = -1;
            string RespMsg = "";
            bool NoCharge = (Total == 0);

            Tran.Address.Street = BillAddress.Street;
            Tran.Address.Street2 = BillAddress.Street2;
            Tran.Address.City = BillAddress.City;
            Tran.Address.State = BillAddress.State;
            Tran.Address.Country = BillAddress.Country;
            Tran.Address.Zip = BillAddress.Zip;
            Tran.CustomerCode = AdminCart.Config.AppCode() + OrderID.ToString();            //Tran.AvsAddr = TrxnResponse.AVSAddr;

            if (!NoCharge)
            {
                var myCharge = new StripeChargeCreateOptions
                {

                    // always set these properties
                    Amount = Convert.ToInt32(100 * Total), //Stripe wants an int so convert $$ --> cents
                    Currency = "usd",

                    // set this if you want to
                    Description = AdminCart.Config.AppCode() + "order# " + OrderID.ToString(),

                    // setting up the card
                    SourceCard = new SourceCard() //*********
                    {
                        // set this property if using a token
                        //TokenId = *tokenId*,

                        // set these properties if passing full card details (do not
                        // set these properties if you set TokenId)
                        Name = Tran.CardHolder,               // optional
                        Number = Tran.CardNo,
                        ExpirationYear = Convert.ToInt32(DateTime.Now.Year.ToString().Substring(0, 2) + Tran.ExpDate.Substring(Tran.ExpDate.Length - 2)),
                        ExpirationMonth = Convert.ToInt32(Tran.ExpDate.Substring(0, 2)),
                        Cvc = Tran.CvsCode,
                        AddressCountry = Tran.Address.Country,                // optional
                        AddressLine1 = Tran.Address.Street,    // optional
                        AddressLine2 = Tran.Address.Street2,              // optional
                        AddressCity = Tran.Address.City,        // optional
                        AddressState = Tran.Address.State,                  // optional
                        AddressZip = Tran.Address.Zip                 // optional
                    }
                };

                // set this property if using a customer
                //myCharge.CustomerId = *customerId*;

                // if using a customer, you may also set this property to charge
                // a card other than the customer's default card
                //myCharge.CardId = *cardId*;

                // set this if you have your own application fees (you must have your application configured first within Stripe)
                //myCharge.ApplicationFee = 25;

                // (not required) set this to false if you don't want to capture the charge yet - requires you call capture later
                //myCharge.Capture = true;

                var chargeService = new StripeChargeService();
                try
                {
                    StripeCharge stripeCharge = chargeService.Create(myCharge);
                    string failCode = (string.IsNullOrEmpty(stripeCharge.FailureCode)) ? "" : stripeCharge.FailureCode;
                    RespMsg = (string.IsNullOrEmpty(stripeCharge.FailureMessage)) ? "Approved (Stripe)" : stripeCharge.FailureMessage;

                    Result = (string.IsNullOrEmpty(failCode)) ? 0 : -1;
                    Tran.TransactionID = stripeCharge.Id;

                }
                catch (StripeException ex)
                {
                    int response = (int)ex.HttpStatusCode;
                    StripeError err = ex.StripeError;
                    string errType = err.ErrorType;
                    string errCode = (string.IsNullOrEmpty(err.Code)) ? "" : err.Code;
                    string errError = (string.IsNullOrEmpty(err.Error)) ? "" : err.Error;
                    string errMessage = (string.IsNullOrEmpty(err.Message)) ? "" : err.Message;
                    Result = -1;
                    RespMsg = errMessage;

                    //throw ex;
                }

                Tran.OrderID = OrderID;
                Tran.TrxType = chargeType;
                Tran.ResultCode = Result;
                //Tran.AuthCode = Tran.TransactionID.Substring(0,8).ToUpper();
                Tran.AuthCode = (Tran.TransactionID.Length > 8) ? Tran.TransactionID.Substring(0, 8).ToUpper() : Tran.TransactionID;
                Tran.ResultMsg = RespMsg;
                //Tran.AvsAddr = TrxnResponse.AVSAddr;
                //Tran.AvsZip = TrxnResponse.AVSZip;
                //Tran.Iavs = TrxnResponse.IAVS;
                Tran.SaveDB();
            }
            else
            {
                Result = 0;
                RespMsg = "Approved (No Charge)";

                Tran.ResultCode = 0;
                Tran.AuthCode = "NoCharge";
                Tran.ResultMsg = "Approved (No Charge)";
                Tran.TransactionID = "NoCharge";
                Tran.CardNo = "xxxxxxxxxxxx";
                Tran.AvsAddr = "";
                Tran.AvsZip = "";
                Tran.Iavs = "";
                Tran.SaveDB();
            }


            return Result;
        }

        public static int StripeCredit(string origID, AdminCart.Transaction Tran, int OrderID, decimal Total, AdminCart.Customer SiteCustomer, AdminCart.Address BillAddress)
        {
            int Result = -1;
            string RespMsg = "";
            int refundAmount = Convert.ToInt32(100 * Total); //Stripe wants an int so convert $$ --> cents 

            StripeRefundCreateOptions stripeRefundCreateOptions = new StripeRefundCreateOptions
            {
                Amount = refundAmount,
                Reason = StripeRefundReasons.RequestedByCustomer,
                RefundApplicationFee = false
            };

            var refundService = new StripeRefundService();
            try
            {
                StripeRefund stripeRefund = refundService.Create(origID, stripeRefundCreateOptions);
                RespMsg = "Approved (Stripe)";
                Result = 0;
                Tran.TransactionID = stripeRefund.Id;
            }
            catch (StripeException ex)
            {
                int response = (int)ex.HttpStatusCode;
                StripeError err = ex.StripeError;
                string errType = err.ErrorType;
                string errCode = (string.IsNullOrEmpty(err.Code)) ? "" : err.Code;
                string errError = (string.IsNullOrEmpty(err.Error)) ? "" : err.Error;
                string errMessage = (string.IsNullOrEmpty(err.Message)) ? "" : err.Message;
                Result = -1;
                RespMsg = "Not Approved (Stripe): " + errMessage;
            }

            Tran.OrderID = OrderID;
            Tran.TransactionAmount = Total;
            Tran.TransactionDate = DateTime.Now;
            Tran.TrxType = "C";
            Tran.ResultCode = Result;
            //Tran.AuthCode = Tran.TransactionID.Substring(0,8).ToUpper();
            Tran.AuthCode = (Tran.TransactionID.Length > 8) ? Tran.TransactionID.Substring(0, 8).ToUpper() : Tran.TransactionID;
            Tran.ResultMsg = RespMsg;
            Tran.CardHolder = SiteCustomer.FirstName + " " + SiteCustomer.LastName;
            Tran.Address.Street = BillAddress.Street;
            Tran.Address.Street2 = BillAddress.Street2;
            Tran.Address.City = BillAddress.City;
            Tran.Address.State = BillAddress.State;
            Tran.Address.Country = BillAddress.Country;
            Tran.Address.Zip = BillAddress.Zip;
            Tran.CustomerCode = AdminCart.Config.AppCode() + OrderID.ToString();            //Tran.AvsAddr = TrxnResponse.AVSAddr;
            Tran.AvsAddr = (Tran.AvsAddr == null) ? "n/a" : Tran.AvsAddr;
            Tran.AvsZip = (Tran.AvsZip == null) ? "n/a" : Tran.AvsZip;
            Tran.Iavs = (Tran.Iavs == null) ? "n/a" : Tran.Iavs;

            Tran.SaveDB();

            return Result;
        }

        public static int ShopifyCredit(string origID, AdminCart.Transaction Tran, int OrderID, decimal Total, string shopifyName, AdminCart.Customer SiteCustomer, AdminCart.Address BillAddress)
        {
            int Result = -1;
            string RespMsg = "";

            ShopifyAgent.ShopifyTransaction shopTrans = ShopifyAgent.ShopifyTransaction.Refund(shopifyName, Total);
            if (shopTrans.Id != 0)
            {
                if (!string.IsNullOrEmpty(shopTrans.Message))
                {
                    shopTrans.Message = "no message";
                }
                RespMsg = "Approved (Shopify): " + shopTrans.Status + ": " + shopTrans.Message;
                Result = (shopTrans.Status == "success") ? 0 : -1;                              
            }
            else
            {
                RespMsg = "Not Approved (Shopify): Shopify Transaction ID = 0";
                Result = -1;
            }

            Tran.OrderID = OrderID;
            if (string.IsNullOrEmpty(shopTrans.Authorization_key))
            {
                Tran.TransactionID = "re_fake_" + origID.Substring(3);
                string msg = "tranRowID = " + Tran.TransactionRowID.ToString() + "; OrderID =" + Tran.OrderID.ToString();
                mvc_Mail.SendDiagEmail("FATAL!!**Shopify Refund Approved w/o Auth Key: ", msg);
            }
            else
            {
                Tran.TransactionID = shopTrans.Authorization_key;
            }

            Tran.TransactionAmount = Total;
            Tran.TransactionDate = DateTime.Now;
            Tran.TrxType = "C";
            Tran.ResultCode = Result;
            Tran.AuthCode = (Tran.TransactionID.Length > 8) ? Tran.TransactionID.Substring(0, 8).ToUpper() : Tran.TransactionID;
            Tran.ResultMsg = RespMsg;
            Tran.CardHolder = SiteCustomer.FirstName + " " + SiteCustomer.LastName;
            Tran.Address.Street = BillAddress.Street;
            Tran.Address.Street2 = BillAddress.Street2;
            Tran.Address.City = BillAddress.City;
            Tran.Address.State = BillAddress.State;
            Tran.Address.Country= BillAddress.Country;
            Tran.Address.Zip = BillAddress.Zip;
            Tran.CustomerCode = AdminCart.Config.AppCode() + OrderID.ToString();
            Tran.AvsAddr = (Tran.AvsAddr == null) ? "n/a" : Tran.AvsAddr;
            Tran.AvsZip = (Tran.AvsZip == null) ? "n/a" : Tran.AvsZip;
            Tran.Iavs = (Tran.Iavs == null) ? "n/a" : Tran.Iavs;
            Tran.SaveDB();

            return Result;
        }
    }
}
