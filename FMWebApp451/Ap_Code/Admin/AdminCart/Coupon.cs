using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace AdminCart
{
    public class Coupon
    {
        //Built with template TemplateBasic.cs.tem
	#region Fields
	private int couponCodeID;
	private string couponCode;
	private string couponDescription;
	private DateTime couponStartDate;
	private DateTime couponEndDate;
	private string couponSpecialRequirements;
	private decimal couponOrderMinimum;
	private string couponExclusive;
	private string couponType;
	private string couponVariable;
	private string couponOperand;
	private string couponAmount;
	private string couponNotes;
	private int couponUsageInstances;
	#endregion
	
	#region Properties
	public int CouponCodeID	{	get{	return couponCodeID;	} 	set	{couponCodeID=value;	}}
	public string CouponCode	{	get{	return couponCode;	} 	set	{couponCode=value;	}}
	public string CouponDescription	{	get{	return couponDescription;	} 	set	{couponDescription=value;	}}
	public DateTime CouponStartDate	{	get{	return couponStartDate;	} 	set	{couponStartDate=value;	}}
	public DateTime CouponEndDate	{	get{	return couponEndDate;	} 	set	{couponEndDate=value;	}}
	public string CouponSpecialRequirements	{	get{	return couponSpecialRequirements;	} 	set	{couponSpecialRequirements=value;	}}
	public decimal CouponOrderMinimum	{	get{	return couponOrderMinimum;	} 	set	{couponOrderMinimum=value;	}}
	public string CouponExclusive	{	get{	return couponExclusive;	} 	set	{couponExclusive=value;	}}
	public string CouponType	{	get{	return couponType;	} 	set	{couponType=value;	}}
	public string CouponVariable	{	get{	return couponVariable;	} 	set	{couponVariable=value;	}}
	public string CouponOperand	{	get{	return couponOperand;	} 	set	{couponOperand=value;	}}
	public string CouponAmount	{	get{	return couponAmount;	} 	set	{couponAmount=value;	}}
	public string CouponNotes	{	get{	return couponNotes;	} 	set	{couponNotes=value;	}}
	public int CouponUsageInstances	{	get{	return couponUsageInstances;	} 	set	{couponUsageInstances=value;	}}
	#endregion
	
	#region Constructors
	public Coupon()
	{
		couponCodeID = 0;
		couponCode = "";
		couponDescription = "";
		couponStartDate = DateTime.Parse("1/1/1900");
		couponEndDate = DateTime.Parse("1/1/1900");
		couponSpecialRequirements = "";
		couponOrderMinimum = 0;
		couponExclusive = "";
		couponType = "";
		couponVariable = "";
		couponOperand = "";
		couponAmount = "";
		couponNotes = "";
		couponUsageInstances = 0;
	}

        public Coupon(int CouponID)
        {
            string sql = @" 
                select * 
                from coupons 
                where couponcodeid = @couponcodeid 
            "; 


            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, CouponID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read())
            {
                couponCodeID = Int32.Parse(dr["couponCodeID"].ToString());
                couponCode = dr["couponCode"].ToString();
                couponDescription = dr["couponDescription"].ToString();
                couponStartDate = DateTime.Parse(dr["couponStartDate"].ToString());
                couponEndDate = DateTime.Parse(dr["couponEndDate"].ToString());
                couponSpecialRequirements = dr["couponSpecialRequirements"].ToString();
                couponOrderMinimum = Decimal.Parse(dr["couponOrderMinimum"].ToString());
                couponExclusive = dr["couponExclusive"].ToString();
                couponType = dr["couponType"].ToString();
                couponVariable = dr["couponVariable"].ToString();
                couponOperand = dr["couponOperand"].ToString();
                couponAmount = dr["couponAmount"].ToString();
                couponNotes = dr["couponNotes"].ToString();
                couponUsageInstances = Int32.Parse(dr["couponUsageInstances"].ToString());

            }
            if (dr != null) dr.Close();
        }

        public Coupon(string CouponCode)
        {
            string sql = @"
                select *
                from coupons
                where couponcode = @couponcode 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, CouponCode);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read())
            {
                couponCodeID = Int32.Parse(dr["couponCodeID"].ToString());
                couponCode = dr["couponCode"].ToString();
                couponDescription = dr["couponDescription"].ToString();
                couponStartDate = DateTime.Parse(dr["couponStartDate"].ToString());
                couponEndDate = DateTime.Parse(dr["couponEndDate"].ToString());
                couponSpecialRequirements = dr["couponSpecialRequirements"].ToString();
                couponOrderMinimum = Decimal.Parse(dr["couponOrderMinimum"].ToString());
                couponExclusive = dr["couponExclusive"].ToString();
                couponType = dr["couponType"].ToString();
                couponVariable = dr["couponVariable"].ToString();
                couponOperand = dr["couponOperand"].ToString();
                couponAmount = dr["couponAmount"].ToString();
                couponNotes = dr["couponNotes"].ToString();
                couponUsageInstances = Int32.Parse(dr["couponUsageInstances"].ToString());

            }
            if (dr != null) dr.Close();
        }
    #endregion
	
	#region Methods
	
        public void Save()
        {
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            if (couponCodeID == 0)
            {
                sql = @"
                INSERT INTO coupons   
                   (CouponCode, CouponDescription, CouponStartDate, CouponEndDate, CouponSpecialRequirements, 
                    CouponOrderMinimum, CouponExclusive, CouponType, CouponVariable, CouponOperand, CouponAmount, 
                    CouponNotes, CouponUsageInstances)  
                VALUES (@CouponCode, @CouponDescription, @CouponStartDate, @CouponEndDate, @CouponSpecialRequirements, 
                    @CouponOrderMinimum, @CouponExclusive, @CouponType, @CouponVariable, @CouponOperand, @CouponAmount, 
                    @CouponNotes, 0)  
                SELECT CouponCodeID = scope_identity()  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, couponCode, couponDescription, couponStartDate, couponEndDate, couponSpecialRequirements, couponOrderMinimum, couponExclusive, couponType, couponVariable, couponOperand, couponAmount, couponNotes);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read()) { couponCodeID = Convert.ToInt32(dr["CouponCodeID"]); }
            }
            else
            {

                sql = @"
                UPDATE coupons 
                SET 
                    CouponCode = @CouponCode, 
                    CouponDescription = @CouponDescription, 
                    CouponStartDate = @CouponStartDate, 
                    CouponEndDate = @CouponEndDate, 
                    CouponSpecialRequirements = @CouponSpecialRequirements, 
                    CouponOrderMinimum = @CouponOrderMinimum, 
                    CouponExclusive = @CouponExclusive, 
                    CouponType = @CouponType, 
                    CouponVariable = @CouponVariable, 
                    CouponOperand = @CouponOperand, 
                    CouponAmount = @CouponAmount, 
                    CouponNotes = @CouponNotes, 
                    CouponUsageInstances = @CouponUsageInstances 
                WHERE CouponCodeID = @CouponCodeID  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, couponCode, couponDescription, couponStartDate, couponEndDate, couponSpecialRequirements, couponOrderMinimum, couponExclusive, couponType, couponVariable, couponOperand, couponAmount, couponNotes, couponUsageInstances, couponCodeID);
                DBUtil.Exec(sql, mySqlParameters);
            }
            if (dr != null) { dr.Close(); }
        }

    /***********************************
    * Static methods
    ************************************/
    /// <summary>
        /// Returns a Coupon object from an ID
    /// </summary>
    public static Coupon GetCouponByID(int couponCodeID)
    {
        Coupon obj = new Coupon(couponCodeID);
        return obj;
    }

    /// <summary>
    /// Creates a new Coupon
    /// </summary>
        public static int InsertCoupon(string couponCode, string couponDescription, DateTime couponStartDate, DateTime couponEndDate, string couponSpecialRequirements, decimal couponOrderMinimum, string couponExclusive, string couponType, string couponVariable, string couponOperand, string couponAmount, string couponNotes, int couponUsageInstances)
    {
        Coupon obj = new Coupon();
        obj.couponCodeID = 0;
        obj.couponCode = couponCode;
        obj.couponDescription = couponDescription;
        obj.couponStartDate = couponStartDate;
        obj.couponEndDate = couponEndDate;
        obj.couponSpecialRequirements = couponSpecialRequirements;
        obj.couponOrderMinimum = couponOrderMinimum;
        obj.couponExclusive = couponExclusive;
        obj.couponType = couponType;
        obj.couponVariable = couponVariable;
        obj.couponOperand = couponOperand;
        obj.couponAmount = couponAmount;
        obj.couponNotes = couponNotes;
        obj.couponUsageInstances = couponUsageInstances;
        obj.Save();
        return obj.CouponCodeID;
    }

    /// <summary>
    /// Updates a Coupon
    /// </summary>
        public static bool UpdateCoupon(int couponCodeID, string couponCode, string couponDescription, DateTime couponStartDate, DateTime couponEndDate, string couponSpecialRequirements, decimal couponOrderMinimum, string couponExclusive, string couponType, string couponVariable, string couponOperand, string couponAmount, string couponNotes, int couponUsageInstances)
    {
        Coupon obj = new Coupon();
        obj.couponCodeID = couponCodeID;
        obj.couponCode = couponCode;
        obj.couponDescription = couponDescription;
        obj.couponStartDate = couponStartDate;
        obj.couponEndDate = couponEndDate;
        obj.couponSpecialRequirements = couponSpecialRequirements;
        obj.couponOrderMinimum = couponOrderMinimum;
        obj.couponExclusive = couponExclusive;
        obj.couponType = couponType;
        obj.couponVariable = couponVariable;
        obj.couponOperand = couponOperand;
        obj.couponAmount = couponAmount;
        obj.couponNotes = couponNotes;
        obj.couponUsageInstances = couponUsageInstances;
        obj.Save();
        return true;
    }

    public int GetUsageID(int orderid)
    {
        //db code to write record
        string sql = @"
		SELECT *
        FROM CouponUsage 
        WHERE OrderID = @OrderID";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderid);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

        int cID = 0;
        if (dr.Read()) { cID = Convert.ToInt16(dr["CouponCodeID"]); }
        if (dr != null) { dr.Close(); }
        return cID;
    }

    public void SaveUsageDB(int orderid)
    {
        string sql = @"
		INSERT couponusage( OrderID, CouponCodeID, DateCreated )
        VALUES( @OrderID, @CouponCodeID, @DateCreated )";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderid, couponCodeID, DateTime.Now);
        DBUtil.Exec(sql, mySqlParameters);

        //now go and incrememnt the # of times this coupon has been used
        //begin by getting the current instance count
        sql = @"
	    SELECT CouponUsageInstances 
        FROM Coupons 
        WHERE CouponCodeID = @CouponCodeID";

        mySqlParameters = DBUtil.BuildParametersFrom(sql, couponCodeID);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

        //then increment it
        if (dr.Read())
        {
            int couponusageinstances = Convert.ToInt32(dr["CouponUsageInstances"]);
            ++couponusageinstances;

            sql = @"
	        UPDATE Coupons
            SET CouponUsageInstances = @CouponUsageInstances 
            WHERE CouponCodeID = @CouponCodeID";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, couponusageinstances, couponCodeID);
            DBUtil.Exec(sql, mySqlParameters);
        }
    }
	
	static public List<Coupon> GetCouponList()
	{
	    List<Coupon> thelist = new List<Coupon>();
	    
	    string sql = "SELECT * FROM coupons ORDER BY couponcodeid DESC ";
	    
	    SqlDataReader dr = DBUtil.FillDataReader(sql);
	    
	    while(dr.Read())
	    {
            Coupon c = new Coupon();

	        c.couponCodeID = Int32.Parse(dr["couponCodeID"].ToString());
	        c.couponCode = dr["couponCode"].ToString();
            c.couponDescription = dr["couponDescription"].ToString();
            c.couponStartDate = DateTime.Parse(dr["couponStartDate"].ToString());
            c.couponEndDate = DateTime.Parse(dr["couponEndDate"].ToString());
            c.couponSpecialRequirements = dr["couponSpecialRequirements"].ToString();
            c.couponOrderMinimum = Decimal.Parse(dr["couponOrderMinimum"].ToString());
            c.couponExclusive = dr["couponExclusive"].ToString();
            c.couponType = dr["couponType"].ToString();
            c.couponVariable = dr["couponVariable"].ToString();
            c.couponOperand = dr["couponOperand"].ToString();
            c.couponAmount = dr["couponAmount"].ToString();
            c.couponNotes = dr["couponNotes"].ToString();
            c.couponUsageInstances = Int32.Parse(dr["couponUsageInstances"].ToString());

            thelist.Add(c);
	    }
	    
	    return thelist;
	
	}

        static public List<Coupon> GetCouponList(string start, string end)
        {
            List<Coupon> thelist = new List<Coupon>();

            string sql = @"
            SELECT * 
            FROM coupons 
            WHERE 
            ORDER BY couponcodeid DESC 
            ";

            SqlDataReader dr = DBUtil.FillDataReader(sql);

            while (dr.Read())
            {
                Coupon c = new Coupon();

                c.couponCodeID = Int32.Parse(dr["couponCodeID"].ToString());
                c.couponCode = dr["couponCode"].ToString();
                c.couponDescription = dr["couponDescription"].ToString();
                c.couponStartDate = DateTime.Parse(dr["couponStartDate"].ToString());
                c.couponEndDate = DateTime.Parse(dr["couponEndDate"].ToString());
                c.couponSpecialRequirements = dr["couponSpecialRequirements"].ToString();
                c.couponOrderMinimum = Decimal.Parse(dr["couponOrderMinimum"].ToString());
                c.couponExclusive = dr["couponExclusive"].ToString();
                c.couponType = dr["couponType"].ToString();
                c.couponVariable = dr["couponVariable"].ToString();
                c.couponOperand = dr["couponOperand"].ToString();
                c.couponAmount = dr["couponAmount"].ToString();
                c.couponNotes = dr["couponNotes"].ToString();
                c.couponUsageInstances = Int32.Parse(dr["couponUsageInstances"].ToString());

                thelist.Add(c);
            }

            return thelist;

        }

	#endregion
    }
}
