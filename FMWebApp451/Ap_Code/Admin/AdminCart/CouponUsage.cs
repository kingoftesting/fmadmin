using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FM2015.Helpers;

/// <summary>
/// Summary description for CouponUSage
/// </summary>
namespace AdminCart
{
    public class CouponUsage
    {
        //Built with template TemplateBasic.cs.tem
	    #region Fields
	    private int couponUsageID;
	    private int orderID;
	    private int couponCodeID;
	    private DateTime dateUsed;
	    #endregion
    	
	    #region Properties
	    public int CouponUsageID	{	get{	return couponUsageID;	} 	set	{couponUsageID=value;	}}
	    public int OrderID	{	get{	return orderID;	} 	set	{orderID=value;	}}
	    public int CouponCodeID	{	get{	return couponCodeID;	} 	set	{couponCodeID=value;	}}
	    public DateTime DateUsed	{	get{	return dateUsed;	} 	set	{dateUsed=value;	}}
	    #endregion
    	
	    #region Constructors
	    public CouponUsage()
	    {
		    CouponUsageID = 0;
		    OrderID = 0;
		    CouponCodeID = 0;
		    DateUsed = DateTime.Parse("1/1/1900");

	    }

        public CouponUsage(int CouponUsageID)
        {
            string sql = @" 
                    select * 
                    from CouponUsage 
                    where CouponUsageID = @CouponUsageID 
                ";


            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, CouponUsageID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read())
            {
                CouponUsageID = Int32.Parse(dr["CouponUsageID"].ToString());
                OrderID = Int32.Parse(dr["OrderID"].ToString());
                CouponCodeID = Int32.Parse(dr["CouponCodeID"].ToString());
                DateUsed = DateTime.Parse(dr["DateCreated"].ToString());
            }
            if (dr != null) dr.Close();
        }
        #endregion
    	
	    #region Methods
    	


        /***********************************
        * Static methods
        ************************************/
        /// <summary>
            /// Returns a Coupon object from an ID
        /// </summary>
        public static CouponUsage GetCouponUsageByID(int couponCodeID)
        {
            CouponUsage obj = new CouponUsage(couponCodeID);
            return obj;
        }
    	
	    static public List<CouponUsage> GetCouponList()
	    {
	        List<CouponUsage> thelist = new List<CouponUsage>();
    	    
	        string sql = "SELECT * FROM CouponUsage ORDER BY CouponUsageID DESC ";
    	    
	        SqlDataReader dr = DBUtil.FillDataReader(sql);
    	    
	        while(dr.Read())
	        {
                CouponUsage c = new CouponUsage();

                c.CouponUsageID = Int32.Parse(dr["CouponUsageID"].ToString());
                c.OrderID = Int32.Parse(dr["OrderID"].ToString());
                c.CouponCodeID = Int32.Parse(dr["CouponCodeID"].ToString());
                c.DateUsed = DateTime.Parse(dr["DateCreated"].ToString());

                thelist.Add(c);
	        }
    	    
	        return thelist;
    	
	    }

            static public List<CouponUsage> GetCouponList(string start, string end)
            {
                List<CouponUsage> thelist = new List<CouponUsage>();

                string sql = @"
                SELECT * 
                FROM CouponUsage  
                WHERE DateCreated BETWEEN @Start and @End 
                ORDER BY CouponUsageID DESC 
                ";

                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, start, end);
                SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

                while (dr.Read())
                {
                    CouponUsage c = new CouponUsage();

                    c.CouponUsageID = Int32.Parse(dr["CouponUsageID"].ToString());
                    c.OrderID = Int32.Parse(dr["OrderID"].ToString());
                    c.CouponCodeID = Int32.Parse(dr["CouponCodeID"].ToString());
                    c.DateUsed = DateTime.Parse(dr["DateCreated"].ToString());

                    thelist.Add(c);
                }

                return thelist;

            }

	    #endregion
    }
}
