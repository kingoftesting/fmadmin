using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using FM2015.Helpers;
using FM2015.ViewModels;
using FMWebApp451.Interfaces;
using FM2015.Models;

namespace AdminCart
{
    public class Customer : ICustomer
    {
        //static private string FMConnString = AdminCart.Config.ConnStr();

        #region private Attributes
        private int customerID;
        private string firstName;
        private string lastName;
        private string email;
        private string password;
        private string phone;
        private DateTime createDate;
        private DateTime lastUpdateDate;
        private DateTime lastLoginDate;
        private int siteID;
        private string referrerDomain;
        private string inBoundQuery;
        private bool faceMasterNewsletter;
        private bool suzanneSomersNewsletter;
        private int suzanneSomersCustomerID;
        private long partnerID;
        #endregion

        #region public Properties
        public int CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }

        [Required]
        [Display(Name = "FirstName")]
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        [Required]
        [Display(Name = "LastName")]
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [Required]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Password")]
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        public DateTime CreateDate
        {
            get { return createDate; }
            set { createDate = value; }
        }

        public DateTime LastUpdateDate
        {
            get { return lastUpdateDate; }
            set { lastUpdateDate = value; }
        }

        public DateTime LastLoginDate
        {
            get { return lastLoginDate; }
            set { lastLoginDate = value; }
        }

        public int SiteID
        {
            get { return siteID; }
            set { siteID = value; }
        }
        public string ReferrerDomain
        {
            get { return referrerDomain; }
            set { referrerDomain = value; }
        }
        public string InBoundQuery
        {
            get { return inBoundQuery; }
            set { inBoundQuery = value; }
        }

        [Display(Name = "Subscribe to FaceMaster Offers")]
        public bool FaceMasterNewsletter
        {
            get { return faceMasterNewsletter; }
            set { faceMasterNewsletter = value; }
        }

        [Display(Name = "Subscribe to SuzanneSomers Offers")]
        public bool SuzanneSomersNewsletter
        {
            get { return suzanneSomersNewsletter; }
            set { suzanneSomersNewsletter = value; }
        }

        public int SuzanneSomersCustomerID
        {
            get { return suzanneSomersCustomerID; }
            set { suzanneSomersCustomerID = value; }
        }

        public long PartnerID { get { return partnerID; } set { partnerID = value; } }
        
        #endregion

        public Customer()
        {
            customerID = 0;
            firstName = "";
            lastName = "";
            email = "";
            password = "";
            phone = "";
            createDate = DateTime.Now;
            lastUpdateDate = DateTime.Now;
            lastLoginDate = DateTime.Now;
            siteID = 2;
            referrerDomain = "";
            inBoundQuery = "";
            faceMasterNewsletter = false;
            suzanneSomersNewsletter = false;
            suzanneSomersCustomerID = 0;
            partnerID = 0;

        }

        public Customer(int CustomerID)
        {
            //customerID = CustomerID;
            if (CustomerID > 0)
            {
                LoadCustomerFromDB(CustomerID);
            }
            else
            {
                customerID = 0;
                firstName = "";
                lastName = "";
                email = "";
                password = "";
                phone = "";
                createDate = DateTime.Now;
                lastUpdateDate = DateTime.Now;
                lastLoginDate = DateTime.Now;
                siteID = 2;
                referrerDomain = "";
                inBoundQuery = "";
                faceMasterNewsletter = false;
                suzanneSomersNewsletter = false;
                suzanneSomersCustomerID = 0;
                partnerID = 0;
            }
        }

        public Customer GetById (int id)
        {
            Customer c = new Customer(id);
            return c;
        }

        public List<Customer> GetAll()
        {
            CustomerProvider customerProvider = new CustomerProvider();
            return customerProvider.GetAll();
        }

        public bool Save(Customer customer)
        {
            CustomerProvider customerProvider = new CustomerProvider();
            return customerProvider.Save(customer);
        }

        public int Login(string email, string password)
        {
            string sql = @"SELECT * FROM Customers 
                          WHERE Email = @email and Password = @password";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, email, password);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            while (dr.Read())
            {
                customerID = Convert.ToInt32(dr["CustomerID"].ToString());
                email = dr["email"].ToString();
                firstName = dr["firstname"].ToString();
                lastName = dr["lastname"].ToString();
                suzanneSomersCustomerID = Convert.ToInt32(dr["SuzanneSomerscustomerID"].ToString());
                UpdateLastLogin(customerID);
            }
            if (dr != null) { dr.Close(); }
            
            return customerID;

        }

        public void LoadCustomerFromDB(int CustomerID)
        {
            string sql = @"
                SELECT * 
                FROM Customers 
                WHERE CustomerID = @CustomerID";

            //load and set parameter collection
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@CustomerID", CustomerID);
            SqlDataReader dr = null;

            try
            {
                dr = DBUtil.FillDataReader(sql, sqlParameters);
                dr.Read();
                this.customerID = Int32.Parse(dr["CustomerID"].ToString());
                this.firstName = dr["FirstName"].ToString();
                this.lastName = dr["LastName"].ToString();

                this.email = dr["Email"].ToString();
                this.password = dr["Password"].ToString();
                this.phone = dr["phone"].ToString();

                this.createDate = DateTime.Parse(dr["CreateDate"].ToString());
                this.lastUpdateDate = DateTime.Parse(dr["LastUpdateDate"].ToString());
                this.lastLoginDate = DateTime.Parse(dr["LastLoginDate"].ToString());

                this.siteID = Int32.Parse(dr["SiteID"].ToString());
                this.referrerDomain = dr["ReferrerDomain"].ToString();
                this.inBoundQuery = dr["inBoundQuery"].ToString();

                this.suzanneSomersCustomerID = Convert.ToInt32(dr["suzannesomerscustomerid"].ToString());
                this.partnerID = (dr["PartnerID"] == DBNull.Value) ? 0 : Convert.ToInt64(dr["PartnerID"].ToString());

                if (dr["FaceMasterNewsletter"].ToString() == "True") this.faceMasterNewsletter = true; else this.faceMasterNewsletter = false;
                if (dr["SuzanneSomersNewsletter"].ToString() == "True") this.suzanneSomersNewsletter = true; else this.suzanneSomersNewsletter = false;

            }
            catch (Exception ex)
            {
                string s = "Exception during DAL.SqlClient.Cart.Customer.LoadCustomerFromDB: " + Environment.NewLine;
                s += "sql: " + sql + " CustomerID = " + CustomerID.ToString() + Environment.NewLine;
                s += "Exception Message: " + ex.Message;
                dbErrorLogging.LogError(s, ex);
            }
            finally
            {
            }

            if (dr != null) { dr.Close(); }
        }

        public void SaveCustomerToDB()
        {
            CacheHelper.Clear(Config.cachekey_OrderList);
            string sql = "";
            
            if (customerID == 0)
                sql = @"
                    SET NOCOUNT ON 
                    
                    INSERT Customers( FirstName, LastName, Email, Phone, Password, 
                        Company, [Group], CreateDate, LastLoginDate, LastUpdateDate, 
                        FaceMasterNewsletter, SuzanneSomersNewsletter, SuzanneSomersCustomerID, 
                        SiteID, ReferrerDomain, InBoundQuery, PartnerID) 
                    VALUES ( 
                        @FirstName, @LastName, @Email, @Phone, @Password,
                        '', '', getdate(), getdate(), getdate(), 
                        @FaceMasterNewsletter, @SuzanneSomersNewsletter, @SuzanneSomersCustomerID,
                        @SiteID, @ReferrerDomain, @InboundQuery, @PartnerID) ;  
                    
                    SELECT CustomerId = scope_identity();";
            
            else
            
                sql = @"
                        UPDATE Customers 
                        SET 
                          FirstName = @FirstName, 
                          LastName = @LastName,
                          Email = @Email,
                          Phone = @Phone,
                          Password = @Password,
                          FaceMasterNewsletter = @FaceMasterNewsletter,
                          SuzanneSomersNewsletter = @SuzanneSomersNewsletter,
                          SuzanneSomersCustomerID = @SuzanneSomersCustomerID,
                          SiteID = @SiteID,
                          ReferrerDomain = @ReferrerDomain,
                          InBoundQuery = @InboundQuery,
                          LastUpdateDate = @LastUpdateDate,
                          PartnerID = @PartnerID  
                        WHERE 
                          CustomerID = @CustomerID";

            DateTime lastupdatedate = DateTime.Now;

            if (FirstName.Length > 30) { FirstName = FirstName.Substring(0, 30); }
            if (LastName.Length > 30) { LastName = LastName.Substring(0, 30); }
            if (Email.Length > 50) { Email = Email.Substring(0, 50); }

            //establish parameter collection
            int ParameterCount = 14;
            SqlParameter[] sqlparameters = new SqlParameter[ParameterCount];

            sqlparameters[00] = new SqlParameter("@CustomerID", CustomerID);
            sqlparameters[01] = new SqlParameter("@FirstName", FirstName);
            sqlparameters[02] = new SqlParameter("@LastName", LastName);
            sqlparameters[03] = new SqlParameter("@Email", Email);
            sqlparameters[04] = new SqlParameter("@Password", Password);
            sqlparameters[05] = new SqlParameter("@Phone", Phone);
            sqlparameters[06] = new SqlParameter("@FaceMasterNewsletter", FaceMasterNewsletter);
            sqlparameters[07] = new SqlParameter("@SuzanneSomersNewsletter", SuzanneSomersNewsletter);
            sqlparameters[08] = new SqlParameter("@SuzanneSomersCustomerID", SuzanneSomersCustomerID);
            sqlparameters[09] = new SqlParameter("@SiteID", SiteID);
            sqlparameters[10] = new SqlParameter("@ReferrerDomain", ReferrerDomain);
            sqlparameters[11] = new SqlParameter("@InBoundQuery", InBoundQuery);
            sqlparameters[12] = new SqlParameter("@LastUpdateDate", lastupdatedate);
            sqlparameters[13] = new SqlParameter("@PartnerID", partnerID);

            SqlDataReader dr = null;
            try
            {
                if (customerID != 0)
                {
                    //already a customers, so we don't need the id
                    DBUtil.Exec(sql, sqlparameters);
                }
                else
                {
                    //we need a reader to get the new customerid
                    dr = DBUtil.FillDataReader(sql, sqlparameters);
                    if (dr.Read()) customerID = Convert.ToInt32(dr["CustomerId"]);
                }
            }
            catch (Exception ex)
            {
                string s = "Exception during DAL.SqlClient.Cart.Customer.SaveCustomerToDB: " + Environment.NewLine;
                s += "sql: " + sql + " CustomerID = " + CustomerID.ToString() + Environment.NewLine;
                s += "Exception Message: " + ex.Message;
                dbErrorLogging.LogError(s, ex);
            }
            finally
            {
                if (dr != null) { dr.Close();}
            }
        }

        private void UpdateLastLogin(int customerid)
        {
            string sql =@"
            UPDATE Customers 
            SET 
              LastLoginDate = @LastLoginDate 
            WHERE 
              CustomerID = @CustomerID ";

            DateTime lastLoginDate = DateTime.Now;

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, lastLoginDate, customerid);
            DBUtil.Exec(sql, mySqlParameters);

        }

        public static Customer FindCustomer(string firstName, string lastName, string email, string phone )
        {
            Customer c = new Customer();

            string sql = @"
                Select CustomerID 
                FROM Customers            
                WHERE LastName LIKE @LastName AND FirstName LIKE @FirstName 
                AND Phone LIKE @Phone 
                AND Email LIKE @Email 
            ";

            firstName = "%" + firstName + "%";
            lastName = "%" + lastName + "%";
            phone = "%" + phone + "%";
            email = "%" + email + "%";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, lastName, firstName, phone, email);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read()) { c = new Customer(Convert.ToInt32(dr["CustomerID"].ToString())); }
            if (dr != null) { dr.Close(); }

            return c;

        }

        public static Customer FindCustomerByEmail(string email)
        {
            Customer c = new Customer();
            email = email.Trim();

            string sql = @"
                Select CustomerID 
                FROM Customers            
                WHERE Email = @Email 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, email);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read()) { c = new Customer(Convert.ToInt32(dr["CustomerID"].ToString())); }
            if (dr != null) { dr.Close(); }

            return c;
        }

        public static bool IsEmailUnique(string email)
        {
            bool isUnique = false; //assume the worst...
            int count = 0;
            email = email.Trim();

            string sql = @"
                Select [count] = Count(CustomerID) 
                FROM Customers            
                WHERE Email = @Email 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, email);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                count = Convert.ToInt32(dr["count"]);
            }
            if (dr != null) { dr.Close(); }

            if (count == 1)
            { isUnique = true; }

            return isUnique;
        }

        public static Customer FindCustomer(string firstName, string lastName, string address, string city, string zip)
        {
            Customer c = new Customer();

            string sql = @"
                Select c.CustomerID
                FROM Customers c, Addresses a           
                WHERE c.CustomerID = a.CustomerID 
                AND c.LastName = @LastName AND c.FirstName = @FirstName 
                AND a.Street1 = @Address 
                AND a.City = @City  
                AND a.Zip = @Zip 
                AND a.AddressType = 1 
                ORDER BY c.CustomerID ASC 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, lastName, firstName, address, city, zip);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read())
            { c.customerID = Int32.Parse(dr["CustomerID"].ToString()); }
            if (dr != null) { dr.Close(); }

            return c;

        }

        public static int CreateNewCustomer(string firstName, string lastName, string email, string street, string street2, string city, string state, string zip, string country)
        {
            Customer c = new Customer();
            Address billAdr = new Address();
            Address shipAdr = new Address();

            //check for email already in the DB
            int count = 1;
            string chkEmail = email;
            while (!CheckEmail(chkEmail))
            {
                chkEmail = "FM" + count + email;
                count++;
            }

            //build customer data using chkEmail
            c.CustomerID = 0;
            c.FirstName = firstName;
            c.LastName = lastName;
            c.Email = chkEmail;
            c.Password = "123456";
            c.Phone = "n/a";
            c.FaceMasterNewsletter = false;

            //build customer billing address
            billAdr.AddressId = 0;
            billAdr.Street = street;
            billAdr.Street2 = street2;
            billAdr.City = city;
            billAdr.State = state;
            billAdr.Zip = zip;
            billAdr.Country = country;

            //build customer shipping address
            shipAdr.AddressId = 0;
            shipAdr.Street = billAdr.Street;
            shipAdr.Street2 = billAdr.Street2;
            shipAdr.City = billAdr.City;
            shipAdr.State = billAdr.State;
            shipAdr.Zip = billAdr.Zip;
            shipAdr.Country = billAdr.Country;

            c.CustomerID = EnterCustomer(c, billAdr, shipAdr);

            return c.CustomerID;
        }

        public static int CreateNewCustomer(ShopifyCustomer shopCust, ShopifyBillingAddress shopBillAdr, ShopifyShippingAddress shopShipAdr)
        {
            Customer c = new Customer();
            Address billAdr = new Address();
            Address shipAdr = new Address();

            //check for email already in the DB
            int count = 1;
            string chkEmail = shopCust.Email;
            while (!CheckEmail(chkEmail))
            {
                chkEmail = "FM" + count + shopCust.Email;
                count++;
            }

            //build customer data using chkEmail
            c.CustomerID = 0;
            c.FirstName = shopCust.First_name;
            c.LastName = shopCust.Last_name;

            if (shopShipAdr.Phone != null)
            {
                c.Phone = shopShipAdr.Phone;
            }
            else
            {
                c.Phone = "n/a";
            }
            
            c.Email = chkEmail;
            c.Password = "123456";
            c.FaceMasterNewsletter = shopCust.Accepts_marketing;

            //build customer billing address
            billAdr.AddressId = 0;
            billAdr.Street = shopBillAdr.Address1;
            billAdr.Street2 = shopBillAdr.Address2;
            billAdr.City = shopBillAdr.City;
            billAdr.State = shopBillAdr.Province_code;
            billAdr.Zip = shopBillAdr.Zip;
            billAdr.Country = shopBillAdr.Country_code;

            //build customer shipping address
            if (shopShipAdr.ShopifyShippingAddressID == 0)
            {
                //no shipping address, use billing
                shipAdr.AddressId = 0;
                shipAdr.Street = shopBillAdr.Address1;
                shipAdr.Street2 = shopBillAdr.Address2;
                shipAdr.City = shopBillAdr.City;
                shipAdr.State = shopBillAdr.Province_code;
                shipAdr.Zip = shopBillAdr.Zip;
                shipAdr.Country = shopBillAdr.Country_code;
            }
            else
            {
                shipAdr.AddressId = 0;
                shipAdr.Street = shopShipAdr.Address1;
                shipAdr.Street2 = shopShipAdr.Address2;
                shipAdr.City = shopShipAdr.City;
                shipAdr.State = shopShipAdr.Province_code;
                shipAdr.Zip = shopShipAdr.Zip;
                shipAdr.Country = shopShipAdr.Country_code;
            }


            c.CustomerID = EnterCustomer(c, billAdr, shipAdr);

            return c.CustomerID;
        }

        public static int CreateNewCustomer(Customer fmCust, Address fmBillAdr, Address fmShipAdr)
        {
            Customer c = new Customer();
            Address billAdr = new Address();
            Address shipAdr = new Address();

            //check for email already in the DB
            int count = 1;
            string chkEmail = fmCust.Email;
            while (!CheckEmail(chkEmail))
            {
                chkEmail = "FM" + count + fmCust.Email;
                count++;
            }

            //build customer data using chkEmail
            c.CustomerID = 0;
            c.FirstName = fmCust.FirstName;
            c.LastName = fmCust.LastName;
            c.Email = chkEmail;
            c.Password = fmCust.Password;
            c.Phone = fmCust.Phone;
            c.FaceMasterNewsletter = fmCust.FaceMasterNewsletter;

            //build customer billing address
            billAdr.AddressId = 0;
            billAdr.Street = fmBillAdr.Street;
            billAdr.Street2 = fmBillAdr.Street2;
            billAdr.City = fmBillAdr.City;
            billAdr.State = fmBillAdr.State;
            billAdr.Zip = fmBillAdr.Zip;
            billAdr.Country = fmBillAdr.Country;

                shipAdr.AddressId = 0;
                shipAdr.Street = fmShipAdr.Street;
                shipAdr.Street2 = fmShipAdr.Street2;
                shipAdr.City = fmShipAdr.City;
                shipAdr.State = fmShipAdr.State;
                shipAdr.Zip = fmShipAdr.Zip;
                shipAdr.Country = fmShipAdr.Country;

            c.CustomerID = EnterCustomer(c, billAdr, shipAdr);

            return c.CustomerID;
        }

        public static int EnterCustomer(Customer c, Address a)
        {

            if (CheckEmail(c.Email))
            {
                //save new customer info to the database
                c.SaveCustomerToDB();
                a.Save(c, 1);
                a.Save(c, 2); //initially assume bill addr = ship addr
                return c.CustomerID;
            }
            else { return -1; } //email already in the DB
        }

        public static int EnterCustomer(Customer c, Address bill, Address ship)
        {
            if (CheckEmail(c.Email))
            {
                //save new customer info to the database
                c.SaveCustomerToDB();
                bill.Save(c, 1);
                ship.Save(c, 2); //initially assume bill addr = ship addr
                return c.CustomerID;
            }
            else { return -1; } //email already in the DB
        }

        public static bool CheckEmail(string email)
        {

            string dupEmailCount = DBUtil.GetScalar("SELECT COUNT(*) FROM Customers WHERE email = '"
                    + email.Replace("'", "") + "'");

            if (dupEmailCount == "0")
            { return true; }
            else { return false; } //email already in the DB
        }

        public static List<FM2015.ViewModels.CustomerViewModel> GetCvmList(string firstName, string lastName, string email, int customerID)
        {
            List<FM2015.ViewModels.CustomerViewModel> cvmList = new List<FM2015.ViewModels.CustomerViewModel>();

            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            string sql = @"
                    select top #COUNT# c.CustomerID, a.AddressID, c.FirstName, c.LastName, c.Email, c.[Password], c.Phone, 
                        a.Street1, a.Street2, a.City, a.State, a.Zip, a.CountryCode, 
                        c.FaceMasterNewsletter, c.SuzannesomersNewsletter, 
                        c.SuzanneSomersCustomerID, c.CreateDate, c.LastUpdateDate, c.LastLoginDate, c.SiteID, c.ReferrerDomain, c.InBoundQuery 
                    from customers c, addresses a 
                    where c.customerID = a.customerID  
                        AND (AddressID = (select max(AddressID) from Addresses where customerid = c.customerID AND addresstype = 1)) 
                ";

            if (string.IsNullOrEmpty(lastName) && string.IsNullOrEmpty(firstName) && string.IsNullOrEmpty(email) && (customerID == 0))
            {
                sql = sql.Replace("#COUNT#", "20");
                sql += @" ORDER BY c.CreateDate DESC ";
                dr = DBUtil.FillDataReader(sql);
            }
            else if (customerID > 0)
            {
                sql = sql.Replace("#COUNT#", "1");
                sql += " AND c.CustomerID = @CustomerID  ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, customerID);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
            }
            else
            {
                sql = sql.Replace("#COUNT#", "100");

                //lastName only
                if (string.IsNullOrEmpty(firstName) && (firstName == email) && (firstName != lastName))
                {
                    lastName = "%" + lastName + "%";
                    sql += " AND c.LastName LIKE @LastName ORDER BY c.LastName DESC ";
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, lastName);
                    dr = DBUtil.FillDataReader(sql, mySqlParameters);
                }

                //first name only
                else if ((lastName == "*") && (string.IsNullOrEmpty(email)) && (!string.IsNullOrEmpty(firstName)))
                {
                    firstName = "%" + firstName + "%";
                    sql += " AND c.firstName LIKE @FirstName ORDER BY c.LastName DESC ";
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, firstName);
                    dr = DBUtil.FillDataReader(sql, mySqlParameters);
                }

                //email only
                else if (string.IsNullOrEmpty(firstName) && (firstName == lastName) && (firstName != email))
                {
                    email = "%" + email + "%";
                    sql += " AND c.Email LIKE @Email ORDER BY c.LastName DESC ";
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, email);
                    dr = DBUtil.FillDataReader(sql, mySqlParameters);
                }

                //first & last, no email
                else if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
                {
                    lastName = "%" + lastName + "%";
                    firstName = "%" + firstName + "%";
                    sql += " AND c.LastName LIKE @LastName AND c.FirstName LIKE @FirstName ORDER BY c.LastName DESC ";
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, lastName, firstName);
                    dr = DBUtil.FillDataReader(sql, mySqlParameters);
                }
            }

                while (dr.Read())
                {
                    CustomerViewModel cvm = new CustomerViewModel();

                    cvm.Customer.CustomerID = Int32.Parse(dr["CustomerID"].ToString());
                    cvm.CustomerViewModelKey = cvm.Customer.CustomerID;

                    cvm.Customer.FirstName = dr["FirstName"].ToString();
                    cvm.Customer.LastName = dr["LastName"].ToString();

                    cvm.Customer.Email = dr["Email"].ToString();
                    cvm.Customer.Password = dr["Password"].ToString();
                    cvm.Customer.Phone = dr["phone"].ToString();

                    cvm.Customer.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                    cvm.Customer.LastUpdateDate = DateTime.Parse(dr["LastUpdateDate"].ToString());
                    cvm.Customer.LastLoginDate = DateTime.Parse(dr["LastLoginDate"].ToString());

                    cvm.Customer.SiteID = Int32.Parse(dr["SiteID"].ToString());
                    cvm.Customer.ReferrerDomain = dr["ReferrerDomain"].ToString();
                    cvm.Customer.InBoundQuery = dr["inBoundQuery"].ToString();

                    cvm.Customer.SuzanneSomersCustomerID = Convert.ToInt32(dr["suzannesomerscustomerid"].ToString());

                    if (dr["FaceMasterNewsletter"].ToString() == "True") cvm.Customer.FaceMasterNewsletter = true; else cvm.Customer.FaceMasterNewsletter = false;
                    if (dr["SuzanneSomersNewsletter"].ToString() == "True") cvm.Customer.SuzanneSomersNewsletter = true; else cvm.Customer.SuzanneSomersNewsletter = false;

                    cvm.BillAddress.AddressId = Convert.ToInt32(dr["AddressID"].ToString());
                    cvm.BillAddress.CustomerID = Convert.ToInt32(dr["CustomerID"].ToString());
                    cvm.BillAddress.Street = dr["Street1"].ToString();

                    if (dr["Street2"] == DBNull.Value)
                    { cvm.BillAddress.Street2 = ""; }
                    else
                    { cvm.BillAddress.Street2 = dr["Street2"].ToString(); }

                    cvm.BillAddress.City = dr["City"].ToString();
                    cvm.BillAddress.State = dr["State"].ToString();
                    cvm.BillAddress.Zip = dr["City"].ToString();
                    cvm.BillAddress.Country = dr["CountryCode"].ToString();

                    cvmList.Add(cvm);
                }
            if (dr != null) { dr.Close(); }
            return cvmList;
        }



        static public void UpDatePartnerID(int CustomerID, long partnerID)
        {
            string sql = @"
            UPDATE Customers
            SET PartnerID = @PartnerID 
            WHERE customerid = @CustomerID   
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, partnerID, CustomerID);
            DBUtil.Exec(sql, mySqlParameters);

        }
    }
}
