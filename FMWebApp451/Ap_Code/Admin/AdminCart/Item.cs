using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace AdminCart
{
    public class Item
    {
        #region Private vars
        private Product lineItemProduct;
        
        private int quantity;
        private decimal lineItemDiscount;
        private decimal lineItemAdjustment; //added for Customer Service to make arbitrary price adjustments
        private decimal lineItemTax;
        private decimal lineItemTotal;
        #endregion

        #region Public vars

        public Product LineItemProduct
        {
            get { return lineItemProduct ; }
            set { lineItemProduct = value; }
        }
        public int Quantity
        {
            get{ return quantity;}
            set { quantity = value; }
        }

        [Display(Name = "Discount")]
        [DataType(DataType.Currency)]
        public decimal LineItemDiscount
        {
            get{ return lineItemDiscount;}
            set{ lineItemDiscount=value;}
        }

        [Display(Name = "Adjust")]
        [DataType(DataType.Currency)]
        public decimal LineItemAdjustment
        {
            get { return lineItemAdjustment; }
            set { lineItemAdjustment = value; }
        }

        [Display(Name = "Tax")]
        [DataType(DataType.Currency)]
        public decimal LineItemTax
        {
            get{ return lineItemTax;}
            set{ lineItemTax=value;}
        }

        [Display(Name = "Price")]
        [DataType(DataType.Currency)]
        public decimal LineItemTotal
        {
            get{ return lineItemTotal;}
            set{ lineItemTotal=value;}
        }
        #endregion

        public Item() 
        {
            lineItemProduct = new Product();
            quantity = 0;
            
            lineItemDiscount = 0;
            lineItemAdjustment = 0;
            lineItemTax = 0;
            lineItemTotal = 0;        
        
        }

        public Item(int ProductID, int Quantity)
        {
            lineItemProduct = new Product(ProductID);
            quantity = Quantity;

            lineItemAdjustment = 0;

            //lineItemDiscount = 0;
            lineItemDiscount = (lineItemProduct.Price - lineItemProduct.SalePrice - lineItemAdjustment);

            lineItemTax = 0;
            lineItemTotal = quantity * (lineItemProduct.Price - lineItemDiscount);

        }


        public Item(int ProductID, int Quantity, decimal Price, decimal Adjustment)
        {
            lineItemProduct = new Product(ProductID);
            LineItemProduct.Price = Price; //the current Product values may not be the same as Order

            quantity = Quantity;

            lineItemAdjustment = Adjustment; //this may not be valid because Adjust = Adjust + Discount

            //lineItemDiscount = 0;
            lineItemDiscount = Adjustment;

            lineItemTax = 0;
            lineItemTotal = quantity * (lineItemProduct.Price - lineItemDiscount);

        }
        
        public override string ToString()
        {
            return string.Format("{0}", lineItemProduct.Name);
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (Object.ReferenceEquals(this, obj)) return true;
            if (this.GetType() != obj.GetType()) return false;

            Item objItem = (Item)obj;
            if (LineItemProduct.ProductID == objItem.LineItemProduct.ProductID) return true;

            return false;
            //return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
