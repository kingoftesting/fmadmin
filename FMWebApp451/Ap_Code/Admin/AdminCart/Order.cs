using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using FM2015.Models;
using FM2015.Helpers;
using FMWebApp451.Interfaces;

namespace AdminCart
{
    

    public class Order
    {

        public OrderDetails details;

        public OrderTransfers orderTransfers;
        public List<UPSShipments> upsShipments;
        public List<FaceMasterScans> scans;
        public List<OrderNote> orderNotes;
        public List<Transaction> transactions;
        public List<Order> orderHistory;

        #region Variables
        private int orderID;
        private int ssorderID;
        private int sourceID;
        private int companyID;
        private bool byPhone;
        private DateTime orderDate;
        private int customerID;
        private int shippingAddressID;
        private int billingAddressID;
        private decimal subTotal;
        private decimal totalShipping;
        private decimal totalTax;
        private decimal totalCoupon;
        private decimal adjust;
        private decimal total;
        private int shipmode;
        private int affiliateOrderDetailID;
        private int siteSettingsID;
        private string status;
        private int orderActionType;
        #endregion

        #region Public Properties
        public int CustomerID { get { return customerID; } set { customerID = value; } }
        public int ShippingAddressID { get { return shippingAddressID; } set { shippingAddressID = value; } }
        public int BillingAddressID { get { return billingAddressID; } set { billingAddressID = value; } }

        public int OrderID
        {
            get { return orderID; }
            set { orderID = value; }
        }
        public int SSOrderID
        {
            get { return ssorderID; }
            set { ssorderID = value; }
        }
        public int SourceID
        {
            get { return sourceID; }
            set { sourceID = value; }
        }

        public int CompanyID
        {
            get { return companyID; }
            set { companyID = value; }
        }
        public bool ByPhone
        {
            get { return byPhone; }
            set { byPhone = value; }
        }
        public DateTime OrderDate
        {
            get { return orderDate; }
            set { orderDate = value; }
        }

        [DataType(DataType.Currency)]
        public decimal SubTotal
        {
            get { return subTotal; }
            set { subTotal=value;}
        }
        [DataType(DataType.Currency)]
        public decimal TotalShipping
        {
            get { return totalShipping; }
            set { totalShipping=value;}
        }
        [DataType(DataType.Currency)]
        public decimal TotalTax
            {
            get { return totalTax; }
            set { totalTax=value;}
        }
        [DataType(DataType.Currency)]
        public decimal TotalCoupon
        {
            get { return totalCoupon; }
            set { totalCoupon = value; }
        }

        [DataType(DataType.Currency)]
        public decimal Adjust
        {
            get { return adjust; }
            set { adjust = value; }
        }

        [DataType(DataType.Currency)]
        public decimal Total
        {
            get { return total; }
            set { total=value;}
        }

        public int ShipMode
        {
            get { return shipmode; }
            set { shipmode = value; }
        }

        public int AffiliateOrderDetailID { get { return affiliateOrderDetailID; } set { affiliateOrderDetailID = value; } }
        public int SiteSettingsID { get { return siteSettingsID; } set { siteSettingsID = value; } }
        public string Status { get { return status; } set { status = value; } }
        public int OrderActionType { get { return orderActionType; } set { orderActionType = value; } }

        #endregion

        #region Constructors
        public Order()
        {
            details = new OrderDetails();
            orderTransfers = new OrderTransfers();
            upsShipments = new List<UPSShipments>();
            scans = new List<FaceMasterScans>();
            orderNotes = new List<OrderNote>();
            transactions = new List<Transaction>();
            orderHistory = new List<Order>();


            orderID = 0;
            ssorderID = 0;
            sourceID = 0; //default source is neither web or real customer ID

            try //kludge to be able to unit test and get around dependency on Config file not being initialized
            {
                companyID = Config.CompanyID(); //default to same companyID as AppName (ie, FM =1, etc.)
            }
            catch
            {
                companyID = 1;
            }

            byPhone = false; //assume a weborder
            customerID = 0;
            billingAddressID = 0;
            shippingAddressID = 0;
            orderDate = DateTime.Parse("1/1/1999");
            subTotal = 0;
            totalShipping = 0;
            totalTax = 0;
            totalCoupon = 0;
            adjust = 0;
            total = 0;
            shipmode = 1; //default = ground
            affiliateOrderDetailID = -1;
            siteSettingsID = -1;

        }

        

        public Order(int FMOrderID)
        {
            
            string sql = @"
                SELECT *
                FROM Orders
                WHERE OrderID = @OrderID";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, FMOrderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read() && dr.HasRows)
            {

                orderID = Convert.ToInt32(dr["OrderID"].ToString());
                if (dr["SuzanneSomersOrderID"] != DBNull.Value) { ssorderID = Convert.ToInt32(dr["SuzanneSomersOrderID"]); }
                else { SSOrderID = 0; }

                sourceID = Convert.ToInt32(dr["SourceID"].ToString());
                companyID = Convert.ToInt32(dr["CompanyID"].ToString());

                if (dr["Phone"] == DBNull.Value) { byPhone = false; }
                else
                {
                    if (dr["Phone"].ToString() == "0") { byPhone = false; }
                    else { byPhone = true; }
                }

                customerID = Convert.ToInt32(dr["CustomerID"].ToString());
                billingAddressID = Convert.ToInt32(dr["BillingAddressID"].ToString());
                shippingAddressID = Convert.ToInt32(dr["ShippingAddressID"].ToString());
                orderDate = Convert.ToDateTime(dr["OrderDate"].ToString());

                totalShipping = Convert.ToDecimal(dr["TotalShipping"].ToString());
                totalTax = Convert.ToDecimal(dr["TotalTax"].ToString());
                totalCoupon = Convert.ToDecimal(dr["TotalCoupon"].ToString());
                adjust = Convert.ToDecimal(dr["Adjust"].ToString());
                total = Convert.ToDecimal(dr["Total"].ToString());
                shipmode = Convert.ToInt32(dr["ShipMode"].ToString());
                affiliateOrderDetailID = Convert.ToInt32(dr["AffiliateOrderDetailID"].ToString());
                siteSettingsID = Convert.ToInt32(dr["SiteSettingsID"].ToString());

                subTotal = total + (totalCoupon + adjust) - totalTax - totalShipping;
            }
            dr.Close();

            OrderDetails od = new OrderDetails();

            foreach(OrderDetail d in OrderDetail.GetOrderDetails(FMOrderID))
            {
                od.AddItem(d);
            }

            details = od;

            orderTransfers = OrderTransfers.GetOrderTransfersByID(FMOrderID);
            upsShipments = UPSShipments.GetUPSShipmentsListByID(FMOrderID);
            scans = FaceMasterScans.GetFaceMasterScansList(FMOrderID);
            orderNotes = OrderNote.GetOrderNoteList(FMOrderID);
            transactions = Transaction.GetTransactionList(FMOrderID);
            orderHistory = Order.FindAllOrders("", "", "", customerID.ToString(), true);
        }

        public Order(string ShopifyOrderName)
        {

            int SSOrderID = 0;
            bool ok = Int32.TryParse(ShopifyOrderName, out SSOrderID);
            if (SSOrderID == 0)
            {
                details = new OrderDetails();
                orderTransfers = new OrderTransfers();
                upsShipments = new List<UPSShipments>();
                scans = new List<FaceMasterScans>();
                orderNotes = new List<OrderNote>();
                transactions = new List<Transaction>();
                orderHistory = new List<Order>();

                orderID = 0;
                ssorderID = 0;
                sourceID = 0; //defaultr source is neither web or real customer ID
                companyID = 0;
                byPhone = false; //assume a weborder
                customerID = 0;
                billingAddressID = 0;
                shippingAddressID = 0;
                orderDate = DateTime.Parse("1/1/1999");
                subTotal = 0;
                totalShipping = 0;
                totalTax = 0;
                totalCoupon = 0;
                adjust = 0;
                total = 0;
                shipmode = 1; //default = ground
                affiliateOrderDetailID = -1;
                siteSettingsID = -1;
            }
            else
            {
                string sql = @" 
                SELECT top 1 * 
                FROM Orders 
                WHERE SuzanneSomersOrderID = @SSOrderID 
                ORDER BY OrderID DESC "; 

                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, SSOrderID);
                SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

                if (dr.Read() && dr.HasRows)
                {

                    orderID = Convert.ToInt32(dr["OrderID"].ToString());
                    if (dr["SuzanneSomersOrderID"] != DBNull.Value) { ssorderID = Convert.ToInt32(dr["SuzanneSomersOrderID"]); }
                    else { SSOrderID = 0; }

                    sourceID = Convert.ToInt32(dr["SourceID"].ToString());
                    companyID = Convert.ToInt32(dr["CompanyID"].ToString());

                    if (dr["Phone"] == DBNull.Value) { byPhone = false; }
                    else
                    {
                        if (dr["Phone"].ToString() == "0") { byPhone = false; }
                        else { byPhone = true; }
                    }

                    customerID = Convert.ToInt32(dr["CustomerID"].ToString());
                    billingAddressID = Convert.ToInt32(dr["BillingAddressID"].ToString());
                    shippingAddressID = Convert.ToInt32(dr["ShippingAddressID"].ToString());
                    orderDate = Convert.ToDateTime(dr["OrderDate"].ToString());

                    totalShipping = Convert.ToDecimal(dr["TotalShipping"].ToString());
                    totalTax = Convert.ToDecimal(dr["TotalTax"].ToString());
                    totalCoupon = Convert.ToDecimal(dr["TotalCoupon"].ToString());
                    adjust = Convert.ToDecimal(dr["Adjust"].ToString());
                    total = Convert.ToDecimal(dr["Total"].ToString());
                    shipmode = Convert.ToInt32(dr["ShipMode"].ToString());
                    affiliateOrderDetailID = Convert.ToInt32(dr["AffiliateOrderDetailID"].ToString());
                    siteSettingsID = Convert.ToInt32(dr["SiteSettingsID"].ToString());

                    subTotal = total + (totalCoupon + adjust) - totalTax - totalShipping;
                }
                dr.Close();

                int FMOrderID = orderID;
                OrderDetails od = new OrderDetails();

                foreach (OrderDetail d in OrderDetail.GetOrderDetails(FMOrderID))
                {
                    od.AddItem(d);
                }

                details = od;

                orderTransfers = OrderTransfers.GetOrderTransfersByID(FMOrderID);
                upsShipments = UPSShipments.GetUPSShipmentsListByID(FMOrderID);
                scans = FaceMasterScans.GetFaceMasterScansList(FMOrderID);
                orderNotes = OrderNote.GetOrderNoteList(FMOrderID);
                transactions = Transaction.GetTransactionList(FMOrderID);
                orderHistory = Order.FindAllOrders(FMOrderID.ToString(), "", "", "", true);
            }

        }

        #endregion

        public int SaveDB()
        {
            CacheHelper.Clear(Config.cachekey_OrderList);
            SqlConnection conn = new SqlConnection(Config.ConnStr());
            string sql = @"
                   SET NOCOUNT ON 
                   IF NOT EXISTS (SELECT * FROM Orders WHERE SuzanneSomersOrderID = @SSOrderID AND SourceID = @SourceID AND CompanyID = @CompanyID)
                   BEGIN
                       INSERT Orders(CartID, CustomerID, OrderDate, Total, Adjust, 
                            TotalShipping, TotalTax,TotalCoupon,Status,
                            ShippingAddressID, BillingAddressID, SourceID, Phone, SuzanneSomersOrderID, 
                            ShipMode, AffiliateOrderDetailID, SiteSettingsID, CompanyID)
                       VALUES(@CartID, @CustomerID, @OrderDate, @Total, @Adjust,
                            @TotalShipping, @TotalTax, @TotalCoupon, 'New',
                            @ShipAddressID, @BillAddressID, @SourceID, @Phone, 
                            @SSOrderID, @ShipMode, @AffiliateOrderDetailID, @SiteSettingsID, @CompanyID); 
                        SELECT OrderID=@@identity; 
                    END
                    ELSE
                    BEGIN
                        SELECT OrderID FROM Orders WHERE SuzanneSomersOrderID = @SSOrderID AND SourceID = @SourceID AND CompanyID = @CompanyID;
                    END
            ";

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlParameter CartIDParam = new SqlParameter("@CartID", SqlDbType.UniqueIdentifier);
            SqlParameter CustomerIDParam = new SqlParameter("@CustomerID", SqlDbType.Int);
            SqlParameter OrderDateParam = new SqlParameter("@OrderDate", SqlDbType.DateTime);
            SqlParameter TotalParam = new SqlParameter("@Total", SqlDbType.Money);

            SqlParameter TotalShippingParam = new SqlParameter("@TotalShipping", SqlDbType.Money);
            SqlParameter TotalTaxParam = new SqlParameter("@TotalTax", SqlDbType.Money);
            SqlParameter TotalCouponParam = new SqlParameter("@TotalCoupon", SqlDbType.Money);
            SqlParameter AdjustParam = new SqlParameter("@Adjust", SqlDbType.Money);
            SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.VarChar);
            SqlParameter ShipAddressIDParam = new SqlParameter("@ShipAddressID", SqlDbType.Int);
            SqlParameter BillAddressIDParam = new SqlParameter("@BillAddressID", SqlDbType.Int);
            SqlParameter SourceIDParam = new SqlParameter("@SourceID", SqlDbType.Int);
            SqlParameter CompanyIDParam = new SqlParameter("@CompanyID", SqlDbType.Int);
            SqlParameter PhoneParam = new SqlParameter("@Phone", SqlDbType.Bit);
            SqlParameter ShipModeParam = new SqlParameter("@ShipMode", SqlDbType.Int);
            SqlParameter AffiliateOrderDetailIDParam = new SqlParameter("@AffiliateOrderDetailID", SqlDbType.Int);
            SqlParameter SiteSettingsIDParam = new SqlParameter("@SiteSettingsID", SqlDbType.Int);
            SqlParameter SSOrderIDParam = new SqlParameter("@SSOrderID", SqlDbType.Int);

            CartIDParam.Value = Guid.NewGuid();
            CustomerIDParam.Value = customerID;
            OrderDateParam.Value = orderDate;
            TotalParam.Value = total;

            TotalShippingParam.Value = totalShipping;
            TotalTaxParam.Value = totalTax;
            TotalCouponParam.Value = totalCoupon;
            AdjustParam.Value = adjust;

            StatusParam.Value = "Uncharged";
            ShipAddressIDParam.Value = shippingAddressID;
            BillAddressIDParam.Value = BillingAddressID;
            SourceIDParam.Value = sourceID;
            CompanyIDParam.Value = companyID;
            PhoneParam.Value = byPhone;
            ShipModeParam.Value = shipmode;
            AffiliateOrderDetailIDParam.Value = affiliateOrderDetailID;
            SiteSettingsIDParam.Value = siteSettingsID;
            SSOrderIDParam.Value = ssorderID;

            cmd.Parameters.Add(CartIDParam);
            cmd.Parameters.Add(CustomerIDParam);
            cmd.Parameters.Add(OrderDateParam);
            cmd.Parameters.Add(TotalParam);
            cmd.Parameters.Add(TotalShippingParam);
            cmd.Parameters.Add(TotalTaxParam);
            cmd.Parameters.Add(TotalCouponParam);
            cmd.Parameters.Add(AdjustParam);
            cmd.Parameters.Add(StatusParam);
            cmd.Parameters.Add(ShipAddressIDParam);
            cmd.Parameters.Add(BillAddressIDParam);
            cmd.Parameters.Add(SourceIDParam);
            cmd.Parameters.Add(CompanyIDParam);
            cmd.Parameters.Add(PhoneParam);
            cmd.Parameters.Add(ShipModeParam);
            cmd.Parameters.Add(AffiliateOrderDetailIDParam);
            cmd.Parameters.Add(SiteSettingsIDParam);
            cmd.Parameters.Add(SSOrderIDParam);

            conn.Open();
            int result = 0;

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                result = Convert.ToInt32(dr["OrderID"]);
            }
            else
            {
                result = -99;
            }
            dr.Close();
            conn.Close();
            return result;
        }

        public int SaveDB(int id)
        {
            CacheHelper.Clear(Config.cachekey_OrderList);
            SqlConnection conn = new SqlConnection(Config.ConnStr());
            string sql = @"
                   SET NOCOUNT ON 
                   IF NOT EXISTS (SELECT * FROM Orders WHERE OrderID = @OrderID AND SourceID = @SourceID AND CompanyID = @CompanyID)
                   BEGIN
                       INSERT Orders(CartID, CustomerID, OrderDate, Total, Adjust, 
                            TotalShipping, TotalTax,TotalCoupon,Status,
                            ShippingAddressID, BillingAddressID, SourceID, Phone, SuzanneSomersOrderID, 
                            ShipMode, AffiliateOrderDetailID, SiteSettingsID, CompanyID)
                       VALUES(@CartID, @CustomerID, @OrderDate, @Total, @Adjust,
                            @TotalShipping, @TotalTax, @TotalCoupon, 'New',
                            @ShipAddressID, @BillAddressID, @SourceID, @Phone, 
                            @SSOrderID, @ShipMode, @AffiliateOrderDetailID, @SiteSettingsID, @CompanyID); 
                        SELECT OrderID=@@identity; 
                    END
                    ELSE
                    BEGIN
                        SELECT OrderID FROM Orders WHERE OrderID = @OrderID AND SourceID = @SourceID AND CompanyID = @CompanyID;
                    END
            ";

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlParameter OrderIDParam = new SqlParameter("@OrderID", SqlDbType.Int);
            SqlParameter CartIDParam = new SqlParameter("@CartID", SqlDbType.UniqueIdentifier);
            SqlParameter CustomerIDParam = new SqlParameter("@CustomerID", SqlDbType.Int);
            SqlParameter OrderDateParam = new SqlParameter("@OrderDate", SqlDbType.DateTime);
            SqlParameter TotalParam = new SqlParameter("@Total", SqlDbType.Money);

            SqlParameter TotalShippingParam = new SqlParameter("@TotalShipping", SqlDbType.Money);
            SqlParameter TotalTaxParam = new SqlParameter("@TotalTax", SqlDbType.Money);
            SqlParameter TotalCouponParam = new SqlParameter("@TotalCoupon", SqlDbType.Money);
            SqlParameter AdjustParam = new SqlParameter("@Adjust", SqlDbType.Money);
            SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.VarChar);
            SqlParameter ShipAddressIDParam = new SqlParameter("@ShipAddressID", SqlDbType.Int);
            SqlParameter BillAddressIDParam = new SqlParameter("@BillAddressID", SqlDbType.Int);
            SqlParameter SourceIDParam = new SqlParameter("@SourceID", SqlDbType.Int);
            SqlParameter CompanyIDParam = new SqlParameter("@CompanyID", SqlDbType.Int);
            SqlParameter PhoneParam = new SqlParameter("@Phone", SqlDbType.Bit);
            SqlParameter ShipModeParam = new SqlParameter("@ShipMode", SqlDbType.Int);
            SqlParameter AffiliateOrderDetailIDParam = new SqlParameter("@AffiliateOrderDetailID", SqlDbType.Int);
            SqlParameter SiteSettingsIDParam = new SqlParameter("@SiteSettingsID", SqlDbType.Int);
            SqlParameter SSOrderIDParam = new SqlParameter("@SSOrderID", SqlDbType.Int);

            OrderIDParam.Value = OrderID;
            CartIDParam.Value = Guid.NewGuid();
            CustomerIDParam.Value = customerID;
            OrderDateParam.Value = orderDate;
            TotalParam.Value = total;

            TotalShippingParam.Value = totalShipping;
            TotalTaxParam.Value = totalTax;
            TotalCouponParam.Value = totalCoupon;
            AdjustParam.Value = adjust;

            StatusParam.Value = "Uncharged";
            ShipAddressIDParam.Value = shippingAddressID;
            BillAddressIDParam.Value = BillingAddressID;
            SourceIDParam.Value = sourceID;
            CompanyIDParam.Value = companyID;
            PhoneParam.Value = byPhone;
            ShipModeParam.Value = shipmode;
            AffiliateOrderDetailIDParam.Value = affiliateOrderDetailID;
            SiteSettingsIDParam.Value = siteSettingsID;
            SSOrderIDParam.Value = ssorderID;

            cmd.Parameters.Add(OrderIDParam);
            cmd.Parameters.Add(CartIDParam);
            cmd.Parameters.Add(CustomerIDParam);
            cmd.Parameters.Add(OrderDateParam);
            cmd.Parameters.Add(TotalParam);
            cmd.Parameters.Add(TotalShippingParam);
            cmd.Parameters.Add(TotalTaxParam);
            cmd.Parameters.Add(TotalCouponParam);
            cmd.Parameters.Add(AdjustParam);
            cmd.Parameters.Add(StatusParam);
            cmd.Parameters.Add(ShipAddressIDParam);
            cmd.Parameters.Add(BillAddressIDParam);
            cmd.Parameters.Add(SourceIDParam);
            cmd.Parameters.Add(CompanyIDParam);
            cmd.Parameters.Add(PhoneParam);
            cmd.Parameters.Add(ShipModeParam);
            cmd.Parameters.Add(AffiliateOrderDetailIDParam);
            cmd.Parameters.Add(SiteSettingsIDParam);
            cmd.Parameters.Add(SSOrderIDParam);

            conn.Open();
            int result = 0;

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                result = Convert.ToInt32(dr["OrderID"]);
            }
            else
            {
                result = -99;
            }
            dr.Close();
            conn.Close();
            return result;
        }

        public int SaveAddressIDs()
        {
            CacheHelper.Clear(Config.cachekey_OrderList);
            SqlConnection conn = new SqlConnection(Config.ConnStr());
            string sql = @"
                   BEGIN
                       UPDATE Orders 
                       SET BillingAddressID = @BillAddressID, ShippingAddressID = @ShipAddressID 
                       WHERE OrderID = @OrderID; 
                       SELECT OrderID FROM Orders WHERE OrderID = @OrderID AND CompanyID = @CompanyID;
                   END
            ";

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlParameter OrderIDParam = new SqlParameter("@OrderID", SqlDbType.Int);
            SqlParameter ShipAddressIDParam = new SqlParameter("@ShipAddressID", SqlDbType.Int);
            SqlParameter BillAddressIDParam = new SqlParameter("@BillAddressID", SqlDbType.Int);
            SqlParameter CompanyIDParam = new SqlParameter("@CompanyID", SqlDbType.Int);

            OrderIDParam.Value = OrderID;
            ShipAddressIDParam.Value = shippingAddressID;
            BillAddressIDParam.Value = BillingAddressID;
            CompanyIDParam.Value = CompanyID;

            cmd.Parameters.Add(OrderIDParam);
            cmd.Parameters.Add(ShipAddressIDParam);
            cmd.Parameters.Add(BillAddressIDParam);
            cmd.Parameters.Add(CompanyIDParam);

            conn.Open();
            int result = 0;

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                result = Convert.ToInt32(dr["OrderID"]);
            }
            else
            {
                result = -99;
            }
            dr.Close();
            conn.Close();
            return result;
        }

        static public DataView FindAllOrders(string fromDate, string toDate, string searchmethod, string searchparameter)
        {
            string sql = "";
            int orderid = -1;
            int customerid = -1;
            string lastname = "%%";
            string email = "%%";
            string phone = "%%";
            string serial = "%%";
            DataSet ds = null;
            SqlParameter[] mySqlParameters = null;

            sql = @"
                SELECT DISTINCT tmp.orderID, tmp.customerID, tmp.lastname, tmp.firstname, tmp.sourceid, tmp.companyid, tmp.suzannesomersorderid, 
	                   CASE WHEN tmp.Phone = 1 THEN 'True' ELSE 'False' END AS Phone, 
	                   CASE WHEN tmp.orderactiontype = 1 THEN 'Placed' ELSE 'Approved' END AS [Action], 
	                   tmp.Total, tmp.orderDate, tmp.city, tmp.state, tmp.countrycode, 
	                   MAX(uploaddate) AS [shipdate], 
	                   trackingnumber = (select top 1 trackingnumber from UPSShipments where orderid = tmp.orderid),  
                       batchid, 
                       serial  = (select top 1 serial from FaceMasterScans where orderid = tmp.orderid) 	   
                FROM ( 
                        SELECT o.orderID, o.customerid, c.lastname, c.firstname, o.phone, o.sourceid, o.companyid, o.suzannesomersorderid,
				                o.total, orderDate = oa.orderactiondate, oa.orderactiontype, 
				                a.city, a.state, a.countrycode, c.email  
                        FROM Orders o, OrderActions oa, Customers c, Addresses a 
                        WHERE oa.orderactiontype = 
				                (SELECT MAX(orderactiontype) 
				                FROM OrderActions 
				                WHERE orderid = o.orderid) 
                            AND (o.orderid = oa.orderid) 
                            AND (o.customerID = c.customerID)           
                            AND (o.shippingaddressID = a.addressID) 
                            AND o.OrderDate BETWEEN @StartDate AND @EndDate 
                            AND c.Email LIKE @Email 
                            AND c.LastName LIKE @LastName 
                            AND c.Phone LIKE @Phone 
                        ) AS tmp 
                LEFT JOIN UPSShipments 
                ON (tmp.OrderID = UPSShipments.orderid) 
                LEFT JOIN OrderTransfers 
                ON (tmp.OrderID = OrderTransfers.orderid) 
                LEFT JOIN FaceMasterScans 
                ON (tmp.OrderID = FaceMasterScans.orderid) 
                WHERE  !paramReplace 
                GROUP BY tmp.orderID, tmp.customerID, tmp.lastname, tmp.firstname, tmp.sourceid, tmp.suzannesomersorderid,
	                   Phone, orderactiontype, 
	                   tmp.Total, tmp.orderDate, tmp.city, tmp.[state], tmp.countrycode, 
	                   trackingnumber, batchid, Serial 
                ORDER BY tmp.orderid ASC 
            ";


            switch (searchmethod)
            {
                case "byOrderID":

                    orderid = Convert.ToInt32(searchparameter);                   
                    fromDate = "1/1/1900" + " 12:00:00AM";
                    toDate = "1/1/2100" + " 12:00:00AM";
                    sql = sql.Replace("!paramReplace", "( tmp.OrderID = @OrderID OR (tmp.SuzanneSomersOrderID = @HDIOrderID) )  ");
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), email, lastname, phone, orderid, orderid);

                    break;
                
                /*
                case "byHDIOrderID":

                    ssorderid = Convert.ToInt32(searchparameter);
                    fromDate = "1/1/1900" + " 12:00:00AM";
                    toDate = "1/1/2100" + " 12:00:00AM";
                    sql = sql.Replace("!paramReplace", "tmp.SuzanneSomersOrderID = @SSOrderID AND tmp.SourceID = -2");
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), email, lastname, phone, ssorderid);

                    break;
                */

                case "byFMCustomerID":

                    customerid = Convert.ToInt32(searchparameter);
                    sql = sql.Replace("!paramReplace", "tmp.CustomerID = @CustomerID ");
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), email, lastname, phone, customerid);
                    break;

                case "byLastName":
                    lastname = searchparameter;
                    sql = sql.Replace("!paramReplace", "1=1");
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), email, lastname, phone);
                    break;

                case "byEmail":
                    email = searchparameter;
                    sql = sql.Replace("!paramReplace", "1=1");
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), email, lastname, phone);
                    break;

                case "byPhone":
                    phone = searchparameter;
                    sql = sql.Replace("!paramReplace", "1=1");
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), email, lastname, phone);
                    break;

                case "bySerialNumber":
                    serial = searchparameter;
                    fromDate = "1/1/2000" + " 12:00:00AM";
                    toDate = "1/1/2100" + " 12:00:00AM";
                    sql = sql.Replace("!paramReplace", "Serial LIKE @Serial ");
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), email, lastname, phone, serial);
                    break;

                default:
                    sql = sql.Replace("!paramReplace", "1=1");
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), email, lastname, phone);
                    break;
            }

            ds = DBUtil.FillDataSet(sql, "orders", mySqlParameters);

            return ds.Tables["orders"].DefaultView;
        }

        static public string GetFMOrderStatus(int orderID)
        {
            string sql = "";

            string orderStatus = "Not Approved"; //default values
            int orderBatchID = 0;
            string orderTrackingNumber = "";
            string orderShipDate = "";
            

            sql = @"
                SELECT tmp.orderID,  
	                   CASE WHEN tmp.orderactiontype = 1 THEN 'Not Approved' ELSE 'Approved' END AS [Action], 	                    
	                   MAX(uploaddate) AS [shipdate], trackingnumber, batchid	   
                FROM ( 
                        SELECT o.orderID, oa.orderactiontype  
                        FROM Orders o, OrderActions oa 
                        WHERE oa.orderactiontype = 
				                (SELECT MAX(orderactiontype) 
				                FROM OrderActions 
				                WHERE orderid = o.orderid) 
                            AND (o.orderid = oa.orderid)                             
                        ) AS tmp 
                LEFT JOIN UPSShipments 
                ON (tmp.OrderID = UPSShipments.orderid) 
                LEFT JOIN OrderTransfers 
                ON (tmp.OrderID = OrderTransfers.orderid) 
                WHERE  tmp.OrderID = @OrderID  
                GROUP BY tmp.orderID, orderactiontype, trackingnumber, batchid 
                ORDER BY tmp.orderid ASC 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read())
            {
                orderStatus = (dr["Action"] != DBNull.Value) ? dr["Action"].ToString() : "Not Approved";
                orderBatchID = (dr["batchid"] != DBNull.Value) ? Convert.ToInt32(dr["batchid"].ToString()) : 0;
                orderTrackingNumber = (dr["trackingnumber"] != DBNull.Value) ? dr["trackingnumber"].ToString() : "";
                orderShipDate = (dr["shipdate"] != DBNull.Value) ? dr["shipdate"].ToString() : "";
                if (!String.IsNullOrEmpty(orderTrackingNumber )) { orderStatus = "Shipped"; }
            }
            HttpContext.Current.Session["orderStatus"] = orderStatus;
            HttpContext.Current.Session["orderBatchID"] = orderBatchID;
            HttpContext.Current.Session["orderTrackingNumber"] = orderTrackingNumber;
            HttpContext.Current.Session["orderShipDate"] = orderShipDate;

            if (dr != null) { dr.Close(); }

            return orderStatus;
        }

        public static List<Order> FindAllOrders(int customerID, bool fullDetails)
        {
            List<Order> orderList = Order.FindAllOrders("", "", "", customerID.ToString(), fullDetails);
            return orderList;
        }

        public static List<Order> FindAllOrders(string orderID, string lastName, string serialNum, string customerID, bool fullDetails)
        {
            List<Order> orderList = new List<Order>();
            SqlParameter[] mySqlParameters = null;

            string sql = @"
                SELECT top 100 * 
                FROM Orders o, Customers c, OrderActions oa  
                WHERE o.orderid = oa.orderid AND o.customerID = c.customerID 
                    AND oa.OrderActionID = (select top 1 orderactionid 
							                from orderactions 
							                where orderid = o.orderid 
							                order by orderactionid desc)
            ";

            //if all params are null or empty then retrieve the last 24hrs of orders
            if (string.IsNullOrEmpty(orderID) && string.IsNullOrEmpty(lastName) && string.IsNullOrEmpty(serialNum) && string.IsNullOrEmpty(customerID))
            {
                sql += "AND o.orderDate BETWEEN @startDate AND @endDate ";
                DateTime endDate = DateTime.Now;
                DateTime startDate = endDate.AddHours(-24);
                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            }
            else
            {

                //fill out rest of Where clause depending on which params were selected

                if (!string.IsNullOrEmpty(orderID))
                {
                    sql += "AND ( (o.OrderID = @OrderID) OR (o.SuzanneSomersOrderID = @OrderID2) ) ";
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID, orderID);
                }

                else if (!string.IsNullOrEmpty(customerID))
                {
                    sql += "AND ( (o.CustomerID = @CustomerID) ) ";
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, customerID);
                }

                else if (!string.IsNullOrEmpty(lastName))
                {
                    sql += "AND c.LastName LIKE @LastName ";
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, "%" + lastName + "%");
                }

                else if (!string.IsNullOrEmpty(serialNum))
                {
                    sql = @"
                        SELECT top 1000 * 
                        FROM Orders o, Customers c, OrderActions oa, FaceMasterScans f 
                        WHERE o.orderid = oa.orderid AND o.customerID = c.customerID AND o.OrderID = f.OrderID 
                            AND f.Serial LIKE @Serial 
                            AND oa.OrderActionID = (select top 1 orderactionid 
							                        from orderactions 
							                        where orderid = o.orderid 
							                        order by orderactionid desc) 
                        ";
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, "%" + serialNum + "%");
                }

            }

            sql += "ORDER BY o.OrderDate DESC ";

            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.HasRows && dr.Read())
            {
                Order o = new Order();

                o.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                if (dr["SuzanneSomersOrderID"] != DBNull.Value) { o.SSOrderID = Convert.ToInt32(dr["SuzanneSomersOrderID"]); }
                else { o.SSOrderID = 0; }

                o.SourceID = Convert.ToInt32(dr["SourceID"].ToString());
                o.CompanyID = Convert.ToInt32(dr["CompanyID"].ToString());

                if (dr["Phone"] == DBNull.Value) { o.ByPhone = false; }
                else
                {
                    if (dr["Phone"].ToString() == "0") { o.ByPhone = false; }
                    else { o.ByPhone = true; }
                }

                o.CustomerID = Convert.ToInt32(dr["CustomerID"].ToString());
                o.BillingAddressID = Convert.ToInt32(dr["BillingAddressID"].ToString());
                o.ShippingAddressID = Convert.ToInt32(dr["ShippingAddressID"].ToString());
                o.OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());

                o.TotalShipping = Convert.ToDecimal(dr["TotalShipping"].ToString());
                o.TotalTax = Convert.ToDecimal(dr["TotalTax"].ToString());
                o.TotalCoupon = Convert.ToDecimal(dr["TotalCoupon"].ToString());
                o.Adjust = Convert.ToDecimal(dr["Adjust"].ToString());
                o.Total = Convert.ToDecimal(dr["Total"].ToString());
                o.ShipMode = Convert.ToInt32(dr["ShipMode"].ToString());
                o.AffiliateOrderDetailID = Convert.ToInt32(dr["AffiliateOrderDetailID"].ToString());
                o.SiteSettingsID = Convert.ToInt32(dr["SiteSettingsID"].ToString());
                if (dr["OrderActionType"] != DBNull.Value) { o.OrderActionType = Convert.ToInt32(dr["OrderActionType"]); }

                o.SubTotal = o.Total + (o.TotalCoupon + o.Adjust) - o.TotalTax - o.TotalShipping;

                orderList.Add(o);
            }
            if (dr != null) { dr.Close(); }

            foreach (Order o in orderList)
            {
                o.upsShipments = UPSShipments.GetUPSShipmentsListByID(o.OrderID);
                if (o.upsShipments.Count > 0)
                {
                    if (o.upsShipments[0].TrackingNumber.Length > 9)
                    {
                        o.Status = "Shipped";
                    }
                    else
                    {
                        OrderActionTypes oat = new OrderActionTypes(o.OrderActionType);
                        o.Status = oat.OrderActionName;
                    }
                }


                if (fullDetails)
                {
                    o.orderNotes = OrderNote.GetOrderNoteList(o.OrderID);
                    o.scans = FaceMasterScans.GetFaceMasterScansList(o.OrderID);
                    o.orderTransfers = OrderTransfers.GetOrderTransfersByID(o.OrderID);
                    o.transactions = Transaction.GetTransactionList(o.OrderID);
                }
            }

            return orderList;
        }

        public static List<Order> FindAllOrders(string lastName, string email, bool fullDetails)
        {
            List<Order> orderList = new List<Order>();
            SqlParameter[] mySqlParameters = null;

            string sql = @"
                SELECT top 100 * 
                FROM Orders o, Customers c, OrderActions oa  
                WHERE o.orderid = oa.orderid AND o.customerID = c.customerID 
                    AND oa.OrderActionID = (select top 1 orderactionid 
							                from orderactions 
							                where orderid = o.orderid 
							                order by orderactionid desc) 
                    AND c.LastName LIKE @LastName 
                    AND c.Email Like @Email 
                ORDER BY c.CustomerID DESC
            ";

            mySqlParameters = DBUtil.BuildParametersFrom(sql, "%" + lastName + "%", "%" + email + "%");
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.HasRows && dr.Read())
            {
                Order o = new Order();

                o.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                if (dr["SuzanneSomersOrderID"] != DBNull.Value) { o.SSOrderID = Convert.ToInt32(dr["SuzanneSomersOrderID"]); }
                else { o.SSOrderID = 0; }

                o.SourceID = Convert.ToInt32(dr["SourceID"].ToString());
                o.CompanyID = Convert.ToInt32(dr["CompanyID"].ToString());

                if (dr["Phone"] == DBNull.Value) { o.ByPhone = false; }
                else
                {
                    if (dr["Phone"].ToString() == "0") { o.ByPhone = false; }
                    else { o.ByPhone = true; }
                }

                o.CustomerID = Convert.ToInt32(dr["CustomerID"].ToString());
                o.BillingAddressID = Convert.ToInt32(dr["BillingAddressID"].ToString());
                o.ShippingAddressID = Convert.ToInt32(dr["ShippingAddressID"].ToString());
                o.OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());

                o.TotalShipping = Convert.ToDecimal(dr["TotalShipping"].ToString());
                o.TotalTax = Convert.ToDecimal(dr["TotalTax"].ToString());
                o.TotalCoupon = Convert.ToDecimal(dr["TotalCoupon"].ToString());
                o.Adjust = Convert.ToDecimal(dr["Adjust"].ToString());
                o.Total = Convert.ToDecimal(dr["Total"].ToString());
                o.ShipMode = Convert.ToInt32(dr["ShipMode"].ToString());
                o.AffiliateOrderDetailID = Convert.ToInt32(dr["AffiliateOrderDetailID"].ToString());
                o.SiteSettingsID = Convert.ToInt32(dr["SiteSettingsID"].ToString());
                if (dr["OrderActionType"] != DBNull.Value) { o.OrderActionType = Convert.ToInt32(dr["OrderActionType"]); }

                o.SubTotal = o.Total + (o.TotalCoupon + o.Adjust) - o.TotalTax - o.TotalShipping;

                orderList.Add(o);
            }
            if (dr != null) { dr.Close(); }

            foreach (Order o in orderList)
            {
                o.upsShipments = UPSShipments.GetUPSShipmentsListByID(o.OrderID);
                if (o.upsShipments.Count > 0)
                {
                    if (o.upsShipments[0].TrackingNumber.Length > 9)
                    {
                        o.Status = "Shipped";
                    }
                    else
                    {
                        OrderActionTypes oat = new OrderActionTypes(o.OrderActionType);
                        o.Status = oat.OrderActionName;
                    }
                }


                if (fullDetails)
                {
                    o.orderNotes = OrderNote.GetOrderNoteList(o.OrderID);
                    o.scans = FaceMasterScans.GetFaceMasterScansList(o.OrderID);
                    o.orderTransfers = OrderTransfers.GetOrderTransfersByID(o.OrderID);
                    o.transactions = Transaction.GetTransactionList(o.OrderID);
                }
            }

            return orderList;
        }

        public static List<Order> FindAllOrders(DateTime startDate, DateTime endDate, bool fullDetails)
        {
            List<Order> orderList = new List<Order>();
            SqlParameter[] mySqlParameters = null;

            string sql = @"
                SELECT  * --top 1000 
                FROM Orders o, OrderActions oa  
                WHERE o.orderid = oa.orderid  
                    AND oa.OrderActionID = (select top 1 orderactionid 
							                from orderactions 
							                where orderid = o.orderid and orderactiontype > 1 
							                order by orderactionid desc) 
                    AND o.orderDate BETWEEN @startDate AND @endDate 
                    ORDER BY o.OrderDate DESC 
            ";

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.HasRows && dr.Read())
            {
                Order o = new Order();

                o.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                if (dr["SuzanneSomersOrderID"] != DBNull.Value) { o.SSOrderID = Convert.ToInt32(dr["SuzanneSomersOrderID"]); }
                else { o.SSOrderID = 0; }

                o.SourceID = Convert.ToInt32(dr["SourceID"].ToString());
                o.CompanyID = Convert.ToInt32(dr["CompanyID"].ToString());

                if (dr["Phone"] == DBNull.Value) { o.ByPhone = false; }
                else
                {
                    if (dr["Phone"].ToString() == "0") { o.ByPhone = false; }
                    else { o.ByPhone = true; }
                }

                o.CustomerID = Convert.ToInt32(dr["CustomerID"].ToString());
                o.BillingAddressID = Convert.ToInt32(dr["BillingAddressID"].ToString());
                o.ShippingAddressID = Convert.ToInt32(dr["ShippingAddressID"].ToString());
                o.OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());

                o.TotalShipping = Convert.ToDecimal(dr["TotalShipping"].ToString());
                o.TotalTax = Convert.ToDecimal(dr["TotalTax"].ToString());
                o.TotalCoupon = Convert.ToDecimal(dr["TotalCoupon"].ToString());
                o.Adjust = Convert.ToDecimal(dr["Adjust"].ToString());
                o.Total = Convert.ToDecimal(dr["Total"].ToString());
                o.ShipMode = Convert.ToInt32(dr["ShipMode"].ToString());
                o.AffiliateOrderDetailID = Convert.ToInt32(dr["AffiliateOrderDetailID"].ToString());
                o.SiteSettingsID = Convert.ToInt32(dr["SiteSettingsID"].ToString());
                if (dr["OrderActionType"] != DBNull.Value) { o.OrderActionType = Convert.ToInt32(dr["OrderActionType"]); }

                o.SubTotal = o.Total + (o.TotalCoupon + o.Adjust) - o.TotalTax - o.TotalShipping;

                orderList.Add(o);
            }
            if (dr != null) { dr.Close(); }

            foreach (Order o in orderList)
            {
                o.upsShipments = UPSShipments.GetUPSShipmentsListByID(o.OrderID);
                if (o.upsShipments.Count > 0)
                {
                    if (o.upsShipments[0].TrackingNumber.Length > 9)
                    {
                        o.Status = "Shipped";
                    }
                    else
                    {
                        OrderActionTypes oat = new OrderActionTypes(o.OrderActionType);
                        o.Status = oat.OrderActionName;
                    }
                }
            }

            return orderList;
        }

        public static List<Order> FindAllOrders(DateTime startDate, DateTime endDate)
        {
            List<Order> orderList = new List<Order>();
            SqlParameter[] mySqlParameters = null;

            string sql = @"
                SELECT  * --top 1000 
                FROM Orders o, OrderActions oa  
                WHERE o.orderid = oa.orderid  
                    AND oa.OrderActionID = (select top 1 orderactionid 
							                from orderactions 
							                where orderid = o.orderid and orderactiontype > 1 
							                order by orderactionid desc) 
                    AND o.orderDate BETWEEN @startDate AND @endDate 
                    ORDER BY o.OrderDate DESC 
            ";

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.HasRows && dr.Read())
            {
                Order o = new Order();

                o.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                if (dr["SuzanneSomersOrderID"] != DBNull.Value) { o.SSOrderID = Convert.ToInt32(dr["SuzanneSomersOrderID"]); }
                else { o.SSOrderID = 0; }

                o.SourceID = Convert.ToInt32(dr["SourceID"].ToString());
                o.CompanyID = Convert.ToInt32(dr["CompanyID"].ToString());

                if (dr["Phone"] == DBNull.Value) { o.ByPhone = false; }
                else
                {
                    if (dr["Phone"].ToString() == "0") { o.ByPhone = false; }
                    else { o.ByPhone = true; }
                }

                o.CustomerID = Convert.ToInt32(dr["CustomerID"].ToString());
                o.BillingAddressID = Convert.ToInt32(dr["BillingAddressID"].ToString());
                o.ShippingAddressID = Convert.ToInt32(dr["ShippingAddressID"].ToString());
                o.OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());

                o.TotalShipping = Convert.ToDecimal(dr["TotalShipping"].ToString());
                o.TotalTax = Convert.ToDecimal(dr["TotalTax"].ToString());
                o.TotalCoupon = Convert.ToDecimal(dr["TotalCoupon"].ToString());
                o.Adjust = Convert.ToDecimal(dr["Adjust"].ToString());
                o.Total = Convert.ToDecimal(dr["Total"].ToString());
                o.ShipMode = Convert.ToInt32(dr["ShipMode"].ToString());
                o.AffiliateOrderDetailID = Convert.ToInt32(dr["AffiliateOrderDetailID"].ToString());
                o.SiteSettingsID = Convert.ToInt32(dr["SiteSettingsID"].ToString());
                if (dr["OrderActionType"] != DBNull.Value) { o.OrderActionType = Convert.ToInt32(dr["OrderActionType"]); }

                o.SubTotal = o.Total + (o.TotalCoupon + o.Adjust) - o.TotalTax - o.TotalShipping;

                orderList.Add(o);
            }
            if (dr != null) { dr.Close(); }

            return orderList;
        }

        static public int GetCustomerID(int orderID)
        {
            int fmOrderID = 0;
            string sql = @"
            SELECT CustomerID 
            FROM Orders 
            WHERE OrderID = @OrderID  
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read())
            { fmOrderID = Convert.ToInt32(dr["CustomerID"]); }
            if (dr != null) { dr.Close(); }

            return fmOrderID;
        }

        static public int OrderExists(int ssOrderID, int companyID)
        {
            int fmOrderID = 0;
            string sql = @"
            SELECT top 1 o.OrderID 
            FROM Orders o, orderdetails od 
            WHERE o.orderid = od.orderid AND o.SuzanneSomersOrderID = @ssOrderID AND o.CompanyID = @CompanyID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, ssOrderID, companyID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read())
            { fmOrderID = Convert.ToInt32(dr["OrderID"]); }
            if (dr != null) { dr.Close(); }

            return fmOrderID;
        }

        static public void UpDatePartnerID(int OrderID, int SSOrderID)
        {
            string sql = @"
            UPDATE Orders
            SET SuzanneSomersOrderID = @SuzanneSomersOrderID 
            WHERE orderid = @OrderID     
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, SSOrderID, OrderID );
            DBUtil.Exec(sql, mySqlParameters);
        }

        static public void UpDateAdjust(int orderID, decimal adjust)
        {
            string sql = @" 
            UPDATE Orders 
            SET Adjust = @Adjust 
            WHERE OrderID = @OrderID   
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, adjust, orderID);
            DBUtil.Exec(sql, mySqlParameters);
        }

        static public List<OrderDetail> GetOrderDetailsList (DateTime startDate, DateTime endDate)
        {
            List<OrderDetail> odList = new List<OrderDetail>();
            string sql = @" 
                SELECT top 10000 od.OrderID, od.ProductID, od.Price, od.Discount, od.Quantity, od.UnitCost 
                FROM Orders o, OrderDetails od, OrderActions oa  
                WHERE o.orderid = oa.orderid AND o.orderid = od.orderid  
                    AND oa.OrderActionID = (select top 1 orderactionid 
							                from orderactions 
							                where orderid = o.orderid and orderactiontype > 1 
							                order by orderactionid desc) 
                    AND o.orderDate BETWEEN @startDate AND @endDate 
                 ORDER BY o.OrderDate DESC    
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.HasRows && dr.Read())
            {
                AdminCart.OrderDetail od = new AdminCart.OrderDetail();
                od.OrderID = Convert.ToInt32(dr["OrderID"]);
                od.ProductID = Convert.ToInt32(dr["ProductID"]);
                od.UnitPrice = Convert.ToDecimal(dr["Price"]);
                od.Discount = Convert.ToDecimal(dr["Discount"]);
                od.Quantity = Convert.ToInt32(dr["Quantity"]);
                od.UnitCost = Convert.ToDecimal(dr["UnitCost"]);
                odList.Add(od);
            }
            if (dr != null) { dr.Close(); }

            return odList;
        }

        public static bool SendEmailReciept(int orderId)
        {

            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(orderId);
            DateTime orderDate = cart.SiteOrder.OrderDate;
            string cardno = cart.Tran.CardNo;
            if (cardno.Length > 11)
                cardno = "**** **** **** " + cardno.Substring(cardno.Length - 4, 4);

            string body = "Thank you for ordering from " + Config.BizName() +  "! \n\n" +
    "Your order information appears below. If you need to get in touch with us about your order, just forward " +
    "this email order confirmation, along with your question to " + Config.CustomerServiceEmail() + "\n\n" +
    "Our charges will appear on your credit card statement as " + Config.StatementDescriptor() + " \n\n" +
    "Again, thank you for shopping at " + Config.BizName() + ".\n\n" +
    "The Customer Service Team at " + Config.BizName() + "\n" +
    "\n\n\n" +
    "YOUR ONLINE ORDER NUMBER IS: " + cart.OrderID.ToString() + "\n\n" +
    "Order Date: " + orderDate + "\n\n" +
    "Credit Card#: " + cardno + "\n\n" +
    "Total Sale: " + cart.SiteOrder.Total.ToString("N") + "\n\n" +
    "========================================================================\n" +
    "CUSTOMER INFORMATION\n" +
    "------------------------------------------------------------------------\n" +
    cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + "\n" +
    cart.BillAddress.Street + "\n";

            if ((string.IsNullOrEmpty(cart.BillAddress.Street2) || (cart.BillAddress.Street2 == " "))) { }
            else { body += cart.BillAddress.Street2 + "\n"; }

            body += cart.BillAddress.City + ", " + cart.BillAddress.State + " " + cart.BillAddress.Zip + "\n" +
            DBUtil.GetCountry(cart.BillAddress.Country) + "\n" +
            "\n" +
            "========================================================================\n" +
            "SHIPPING ADDRESS\n" +
            "------------------------------------------------------------------------\n" +
            cart.ShipAddress.Street + "\n";

            if ((string.IsNullOrEmpty(cart.ShipAddress.Street2) || (cart.ShipAddress.Street2 == " "))) { }
            else { body += cart.ShipAddress.Street2 + "\n"; }

            body += cart.ShipAddress.City + ", " + cart.ShipAddress.State + " " + cart.ShipAddress.Zip + "\n" +
                DBUtil.GetCountry(cart.ShipAddress.Country) + "\n" +
                "Shipping Method: ";
            if (cart.ShipType == 1) { body += "Ground"; }
            if (cart.ShipType == 2) { body += "Expedited (2-Day Air)"; }
            if (cart.ShipType == 3) { body += "Express (Guarenteed Overnight)"; }

            body += "\n" +
                "========================================================================\n" +
                "PRODUCTS ORDERED\n" +
                "------------------------------------------------------------------------\n" +
                "#      Description                            Qty Price   Total\n" +
                "------------------------------------------------------------------------\n";

            int counter = 1;
            string total = "";
            foreach (Item Item in cart.Items)
            {
                total = String.Format("{0:N}", Item.LineItemTotal);
                body += counter + DBUtil.AddBlanks(8 - counter.ToString().Length) +
                    Item.LineItemProduct.Name + DBUtil.AddBlanks(40 - Item.LineItemProduct.Name.Length) +
                    Item.Quantity + DBUtil.AddBlanks(5 - Item.Quantity.ToString().Length) +
                    "$" + String.Format("{0:N}", Item.LineItemProduct.SalePrice) + DBUtil.AddBlanks(8 - String.Format("{0:N}", Item.LineItemProduct.SalePrice).Length) +
                    "$" + total + "\n";
                counter++;
            }

            body += "------------------------------------------------------------------------\n" +
                "                                                     SUBTOTAL: $" + String.Format("{0:N}", cart.SiteOrder.SubTotal) + "\n" +
                "                                                     SHIPPING: $" + String.Format("{0:N}", cart.SiteOrder.TotalShipping) + "\n" +
                "                                                          TAX: $" + String.Format("{0:N}", cart.SiteOrder.TotalTax) + "\n";
            if (cart.SiteOrder.TotalCoupon != 0)
            {
                body += "                                                     DISCOUNT: $" + String.Format("{0:N}", cart.SiteOrder.TotalCoupon) + "\n";

            }
            body += "------------------------------------------------------------------------\n" +
                "                                                        TOTAL: $" + String.Format("{0:N}", cart.SiteOrder.Total) + "\n";


            string emailCopy = "To: " + cart.SiteCustomer.Email + "\n";
            emailCopy += "From: " + AdminCart.Config.MailFrom() + "\n";
            emailCopy += "Subject: Order Confirmation #" + cart.OrderID.ToString() + "\n/n";
            emailCopy += body;
            HttpContext.Current.Session["EmailReceiptCopy"] = emailCopy;

            return mvc_Mail.SendMail(cart.SiteCustomer.Email, AdminCart.Config.MailFrom(), "", "", "Order Confirmation #" +
                             cart.OrderID.ToString(), body);


        }

        public static bool SendEmailRMA(int orderId, string body)
        {
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(orderId);

            string emailCopy = "To: " + cart.SiteCustomer.Email + "\n";
            emailCopy += "From: " + AdminCart.Config.MailFrom() + "\n";
            emailCopy += "Subject: Return Merchandise Authorization (RMA) #" + cart.OrderID.ToString() + "\n/n";
            emailCopy += body;
            HttpContext.Current.Session["EmailRMACopy"] = emailCopy;

            bool ok = mvc_Mail.SendMail(cart.SiteCustomer.Email, AdminCart.Config.MailFrom(), "", "facemaster@slccompanies.com", "Return Merchandise Authorization", body);
            
            if (ok)
            {
                //add a new entry into the CER database
                adminCER.CER cer = new adminCER.CER(cart);
                cer.EventDescription = body;
                int result = cer.Add();
            }

            return ok;
        }

    }
}
