﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using FM2015.Helpers;

/// <summary>
/// Summary description for OrderAction
/// </summary>
namespace AdminCart
{
    public class OrderAction
    {
        //Built with ClassHatch Version Build: 4.0.1  10/2009.  Template file:ExtendedClass.cs.tem

        #region Fields
        private int orderActionID;
        private int orderID;
        private int orderActionType;
        private DateTime orderActionDate;
        private string orderActionReason;
        private string orderActionBy;
        private int sSOrderID;
        #endregion

        #region Properties
        public int OrderActionID { get { return orderActionID; } set { orderActionID = value; } }
        public int OrderID { get { return orderID; } set { orderID = value; } }
        public int OrderActionType { get { return orderActionType; } set { orderActionType = value; } }
        public DateTime OrderActionDate { get { return orderActionDate; } set { orderActionDate = value; } }
        public string OrderActionReason { get { return orderActionReason; } set { orderActionReason = value; } }
        public string OrderActionBy { get { return orderActionBy; } set { orderActionBy = value; } }
        public int SSOrderID { get { return sSOrderID; } set { sSOrderID = value; } }
        #endregion

        #region Constructors
        public OrderAction()
        {
            orderActionID = 0;
            orderID = 0;
            orderActionType = 0;
            orderActionDate = DateTime.Parse("1/1/1900");
            orderActionReason = "";
            orderActionBy = "";
            sSOrderID = 0;
        }
        public OrderAction(int PrimaryKeyValue)
        {
            string sql = @"
			SELECT *
			FROM OrderActions
			WHERE OrderActionID = @PrimaryKeyValue";
            SqlParameter[] parms = new SqlParameter[1];
            parms[0] = new SqlParameter("@PrimaryKeyValue", PrimaryKeyValue);

            SqlDataReader dr = DBUtil.FillDataReader(sql, parms);

            while (dr.Read())
            {
                this.orderActionID = Convert.ToInt32(dr["orderActionID"].ToString());
                this.orderID = Convert.ToInt32(dr["orderID"].ToString());
                this.orderActionType = Convert.ToInt32(dr["orderActionType"].ToString());
                this.orderActionDate = Convert.ToDateTime(dr["orderActionDate"].ToString());
                this.orderActionReason = Convert.ToString(dr["orderActionReason"].ToString());
                this.orderActionBy = Convert.ToString(dr["orderActionBy"].ToString());
                this.sSOrderID = Convert.ToInt32(dr["sSOrderID"].ToString());
            }
            dr.Close();
        }

        #endregion

        #region Methods


        public int SaveDB()
        {
            //db code to write record
            SqlConnection conn = new SqlConnection(Config.ConnStr());

            string sql = "";


            if (OrderActionID == 0)
            {
                //this is an insert
                sql = @"
				INSERT OrderActions(
--OrderActionID,
OrderID,
OrderActionType,
OrderActionDate,
OrderActionReason,
OrderActionBy,
SSOrderID 

				 )VALUES(
--@OrderActionID,
@OrderID,
@OrderActionType,
@OrderActionDate,
@OrderActionReason,
@OrderActionBy,
@SSOrderID 
				)";

            }
            else
            {
                //this is an update
                sql = @"
				UPDATE OrderActions
				SET 
				  orderID = @orderID, 
				  orderActionType = @orderActionType, 
				  orderActionDate = @orderActionDate, 
				  orderActionReason = @orderActionReason, 
				  orderActionBy = @orderActionBy, 
                  SSOrderId = @SSOrderID 
				WHERE OrderActionID = @OrderActionID";
            }

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlParameter orderActionIDParam = new SqlParameter("@orderActionID", SqlDbType.Int);
            SqlParameter orderIDParam = new SqlParameter("@orderID", SqlDbType.Int);
            SqlParameter orderActionTypeParam = new SqlParameter("@orderActionType", SqlDbType.Int);
            SqlParameter orderActionDateParam = new SqlParameter("@orderActionDate", SqlDbType.DateTime);
            SqlParameter orderActionReasonParam = new SqlParameter("@orderActionReason", SqlDbType.VarChar);
            SqlParameter orderActionByParam = new SqlParameter("@orderActionBy", SqlDbType.VarChar);
            SqlParameter ssOrderIdParam = new SqlParameter("@SSOrderID", SqlDbType.Int);

            orderActionIDParam.Value = orderActionID;
            orderIDParam.Value = orderID;
            orderActionTypeParam.Value = orderActionType;
            orderActionDateParam.Value = orderActionDate;
            orderActionReasonParam.Value = orderActionReason;
            orderActionByParam.Value = orderActionBy;
            ssOrderIdParam.Value = SSOrderID;

            cmd.Parameters.Add(orderActionIDParam);
            cmd.Parameters.Add(orderIDParam);
            cmd.Parameters.Add(orderActionTypeParam);
            cmd.Parameters.Add(orderActionDateParam);
            cmd.Parameters.Add(orderActionReasonParam);
            cmd.Parameters.Add(orderActionByParam);
            cmd.Parameters.Add(ssOrderIdParam);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            return 0;
        }

        static public List<OrderAction> GetOrderActionList()
        {
            List<OrderAction> thelist = new List<OrderAction>();

            string sql = "SELECT TOP 100 * FROM OrderActions";

            SqlDataReader dr = DBUtil.FillDataReader(sql);

            while (dr.Read())
            {
                OrderAction obj = new OrderAction();

                obj.orderActionID = Convert.ToInt32(dr["orderActionID"].ToString());
                obj.orderID = Convert.ToInt32(dr["orderID"].ToString());
                obj.orderActionType = Convert.ToInt32(dr["orderActionType"].ToString());
                obj.orderActionDate = Convert.ToDateTime(dr["orderActionDate"].ToString());
                obj.orderActionReason = Convert.ToString(dr["orderActionReason"].ToString());
                obj.orderActionBy = Convert.ToString(dr["orderActionBy"].ToString());
                obj.sSOrderID = Convert.ToInt32(dr["sSOrderID"].ToString());

                thelist.Add(obj);
            }

            return thelist;

        }

        static public List<OrderAction> GetOrderActionList(int OrderID)
        {
            List<OrderAction> thelist = new List<OrderAction>();

            string sql = @"
        SELECT TOP 100 * 
        FROM OrderActions 
        WHERE OrderID = @OrderID
        ";

            SqlParameter[] QueryParameters = new SqlParameter[1];
            QueryParameters[0] = new SqlParameter("@OrderID", OrderID);

            SqlDataReader dr = DBUtil.FillDataReader(sql, QueryParameters);

            while (dr.Read())
            {
                OrderAction obj = new OrderAction();

                obj.orderActionID = Convert.ToInt32(dr["orderActionID"].ToString());
                obj.orderID = Convert.ToInt32(dr["orderID"].ToString());
                obj.orderActionType = Convert.ToInt32(dr["orderActionType"].ToString());
                obj.orderActionDate = Convert.ToDateTime(dr["orderActionDate"].ToString());
                obj.orderActionReason = Convert.ToString(dr["orderActionReason"].ToString());
                obj.orderActionBy = Convert.ToString(dr["orderActionBy"].ToString());
                obj.sSOrderID = Convert.ToInt32(dr["sSOrderID"].ToString());

                thelist.Add(obj);
            }

            return thelist;

        }

        static public List<OrderAction> GetOrderActionList(string fromdate, string todate)
        {
            List<OrderAction> thelist = new List<OrderAction>();

            string sql = @"
        SELECT TOP 1000 * 
        FROM OrderActions 
        WHERE OrderActionDate BETWEEN @fromdate AND @todate 
        ORDER BY OrderActionID DESC 
        ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, fromdate, todate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                OrderAction obj = new OrderAction();

                obj.orderActionID = Convert.ToInt32(dr["orderActionID"].ToString());
                obj.orderID = Convert.ToInt32(dr["orderID"].ToString());
                obj.orderActionType = Convert.ToInt32(dr["orderActionType"].ToString());
                obj.orderActionDate = Convert.ToDateTime(dr["orderActionDate"].ToString());
                obj.orderActionReason = Convert.ToString(dr["orderActionReason"].ToString());
                obj.orderActionBy = Convert.ToString(dr["orderActionBy"].ToString());
                obj.sSOrderID = Convert.ToInt32(dr["sSOrderID"].ToString());

                thelist.Add(obj);
            }

            return thelist;

        }

        static public DataSet GetOrderActionTypes()
        {
            string sql = @"
            SELECT OrderActionTypeID, OrderActionType 
            FROM OrderActionTypes 
            ";
            DataSet ds = DBUtil.FillDataSet(sql, "Locations");
            return ds;
        }

        public static string GetOrderAction(int orderID)
        {
            string orderaction = "N/A";
            string sql = @"
                        SELECT tmp.OrderActionID, tmp.OrderActionType    
                        FROM ( 
		                        SELECT oat.OrderActionType, oa.OrderID, oa.OrderActionID 
		                        FROM OrderActions oa, OrderActionTypes oat 
		                        WHERE oa.OrderActionType = oat.OrderActionTypeID 
		                        AND oat.OrderActionTypeID = 
		                        (Select MAX(OrderActiontypeID) 
		                         From OrderActiontypes 
		                         Where oa.OrderActionType = OrderActionTypeID) 
	                         ) AS tmp
                        WHERE tmp.OrderID = @OrderID 
                        ORDER BY tmp.OrderActionID DESC  
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read()) { orderaction = dr["OrderActionType"].ToString(); }
            if (dr != null) { dr.Close(); }
            return orderaction;
        }

        public static string TranslateOrderAction(int oaTypeID)
        {
            string orderaction = "N/A";
            string sql = @"
                        SELECT OrderActionType    
                        FROM OrderActionTypes  
		                WHERE OrderActionTypeID = @oaTypeID 		                  
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, oaTypeID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read()) { orderaction = dr["OrderActionType"].ToString(); }
            if (dr != null) { dr.Close(); }
            return orderaction;
        }

        public static bool GetFMOrderApproved(int FMOrderID)
        {
            bool approved = false;
            bool cancelled = false;

            string sql = @"
                SELECT TOP 100 * 
                FROM OrderActions 
                WHERE OrderID = @OrderID 
                ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, FMOrderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            int oatype = 0;

            while (dr.Read())
            {
                oatype = Convert.ToInt32(dr["OrderActionType"]);
                if (oatype == 2) { approved = true; }
                if (oatype == 5) { cancelled = true; }
            }

            if (dr != null) { dr.Close(); }
            if (cancelled) { approved = false; }
            return approved;
        }

        #endregion

        #region OverRides

        #endregion
    }
}


