﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace AdminCart
{
    public class OrderActionTypes
    {
        #region Private/Public Vars
        private int id;
        public int ID { get { return id; } set { id = value; } }
        private string orderActionName;
        public string OrderActionName { get { return orderActionName; } set { orderActionName = value; } }
        private string orderActionTypeDescription;
        public string OrderActionTypeDescription { get { return orderActionTypeDescription; } set { orderActionTypeDescription = value; } }
        #endregion

        public OrderActionTypes()
        {
            ID = 0;
            OrderActionName = "n/a";
            OrderActionTypeDescription = "n/a";
        }

        public OrderActionTypes(int ID)
        {
            List<OrderActionTypes> oatList = GetOrderActionTypesList();
            try
            {
                OrderActionTypes oat = oatList.First(x => x.ID == ID);
                ID = oat.ID;
                OrderActionName = oat.OrderActionName;
                OrderActionTypeDescription = oat.OrderActionTypeDescription;
            }
            catch //just ignore any exceptions
            { ; }

        }

        public int Save(int ID)
        {
            int result = 0;
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (ID == 0)
            {
                string sql = @" 
                    INSERT INTO OrderActionTypes 
                    (OrderActionType, OrderActionTypeDescription) 
                    VALUES(@OrderActionType, @OrderActionTypeDescription); 
                    SELECT ID=@@identity;  
                ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderActionName, OrderActionTypeDescription);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ID"]);
                }
                else
                {
                    result = 99;
                }
            }
            else
            {
                string sql = @" 
                    UPDATE OrderActionTypes 
                    SET  
                        OrderActionType = @OrderActionType,  
                        OrderActionTypeDescription = @OrderActionTypeDescription  
                    WHERE OrderActionTypeID = @ID  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderActionName, OrderActionTypeDescription, ID);
                DBUtil.Exec(sql, mySqlParameters);
            }
            return result;
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all OrderActionTypes
        /// </summary>
        public static List<OrderActionTypes> GetOrderActionTypesList()
        {
            List<OrderActionTypes> thelist = new List<OrderActionTypes>();

            if (HttpContext.Current.Session["OrderActionTypesList"] == null)
            {
                string sql = @" 
                SELECT * FROM OrderActionTypes 
            ";

                SqlDataReader dr = DBUtil.FillDataReader(sql);

                while (dr.Read())
                {
                    OrderActionTypes obj = new OrderActionTypes();

                    obj.ID = Convert.ToInt32(dr["OrderActionTypeID"]);
                    obj.OrderActionName = dr["OrderActionType"].ToString();
                    obj.OrderActionTypeDescription = dr["OrderActionDescription"].ToString();

                    thelist.Add(obj);
                }
                if (dr != null) { dr.Close(); }
                HttpContext.Current.Session["OrderActionTypesList"] = thelist;
            }
            else
            {
                thelist = (List<OrderActionTypes>)HttpContext.Current.Session["OrderActionTypesList"];
            }

            return thelist;
        }

        /// <summary>
        /// Returns a className object with the specified ID
        /// </summary>

        public static OrderActionTypes GetOrderActionTypesByID(int id)
        {
            OrderActionTypes obj = new OrderActionTypes(id);
            return obj;
        }

        /// <summary>
        /// Updates an existing OrderActionTypes
        /// </summary>
        public static bool UpdateOrderActionTypes(int id, string orderactiontype, string orderactiontypedescription)
        {
            OrderActionTypes obj = new OrderActionTypes();

            obj.ID = id;
            obj.OrderActionName = orderactiontype;
            obj.OrderActionTypeDescription = orderactiontypedescription;
            int ret = obj.Save(obj.ID);
            return (ret > 0) ? true : false;
        }

        /// <summary>
        /// Creates a new OrderActionTypes
        /// </summary>
        public static int InsertOrderActionTypes(string orderactiontype, string orderactiontypedescription)
        {
            OrderActionTypes obj = new OrderActionTypes();

            obj.OrderActionName = orderactiontype;
            obj.OrderActionTypeDescription = orderactiontypedescription;
            int ret = obj.Save(obj.ID);
            return ret;
        }

    }
}