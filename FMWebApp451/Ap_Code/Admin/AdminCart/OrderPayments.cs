using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace AdminCart
{
    public class OrderPayments: IEnumerable
    {
        private ArrayList items;


        public OrderPayments()
        {
            items = new ArrayList();
        }

        public Payment CreateItem()
        {
            return new Payment();
        }

        public void AddItem(Payment item)
        {
            int index = items.IndexOf(item);

            items.Add(item);
            
        }
        public void UpdateItem(Payment item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
            {
                Payment currentitem = items[index] as Payment;
                currentitem.Complete = item.Complete;

            }
        }
     

        public void DeleteItem(Payment item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
                items.RemoveAt(index);
            else
                throw new ArgumentException(
                    String.Format("Item = {0} is not in the list.", item));

        }
        public int ItemCount()
        {
            int thecount = 0;
            foreach (Payment i in items)
            {
                thecount++;
            }
            return thecount;
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return (items as IEnumerable).GetEnumerator();
        }

        #endregion
    }
}
