using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace AdminCart
{
    public class PaymentTransactions: IEnumerable 
    {
        private ArrayList items;

        public PaymentTransactions()
        {
            items = new ArrayList();
        }

        public Transaction CreateItem()
        {
            return new Transaction();
        }

        public void AddItem(Transaction item)
        {
            int index = items.IndexOf(item);

            //if (index >= 0)
            //{
            //    IncrementItem(item);
           // }
           // else
           // {

                items.Add(item);

            //}
        }
        public void UpdateItem(Transaction item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
            {
                Transaction currentitem = items[index] as Transaction;
                //currentitem.Quantity = item.Quantity;
            }
        }
        

        public void DeleteItem(Transaction item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
                items.RemoveAt(index);
            else
                throw new ArgumentException(
                    String.Format("Item = {0} is not in the collection.", item));

        }
        public int ItemCount()
        {
            int thecount = 0;
            foreach (Transaction i in items)
            {
                thecount++;
            }
            return thecount;
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return (items as IEnumerable).GetEnumerator();
        }

        #endregion
    }
}
