using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using FMWebApp.Helpers;
using FM2015.Helpers;
using System.Web.Mvc;
using System.Linq;

namespace AdminCart
{
    public class Product
    {
        #region PrivateVars
        private int productID;
        private string sku;
        private string name;
        private bool isSubscription;
        private string subscriptionPlan;
        private bool isMultiPay;
        private decimal  price; //SQL money
        private decimal saleprice; //SQL money
        private decimal onePayPrice; //SQL money
        private decimal cost; //SQL money
        private bool taxable; //SQL bit
        private decimal weight; //SQL decimal
        private decimal perunitshipping; //SQL money
        private string shortdescription;
        private string description;
        private string cartimage;
        private string thumbnailimage;
        private string iconimage;
        private string detailsimage;
        private string zoomimage;
        private string originalimage;
        private int stock;
        private bool enabled; //SQL bit
        private bool cSOnly; //SQL bit
        private int sort;
        private bool instock; //SQL bit
        private string outofstockmessage;
        private bool isFMSystem; //SQL bit
        private bool isRefill; //SQL bit
        private bool isBundle;
        private string bundleString;
        private bool isShippable;
        private string countryOfOrigin;
        private long partnerID;
        private string partnerImageURLs;
        private string partnerDescription;
        private string partnerProductType;
        private bool isGiftCard;
        private string category;
        private bool isRefurbished;

        #endregion

        #region Public Properties
        public int ProductID { get { return productID; } set { productID = value; } }

        [Required]
        public string Sku { get { return sku; } set { sku = value; } }

        [Required]
        public string Name { get { return name; } set { name = value; } }

        public bool IsSubscription { get { return isSubscription; } set { isSubscription = value; } }

        [Required]
        public string SubscriptionPlan { get { return subscriptionPlan; } set { subscriptionPlan = value; } }

        public bool IsMultiPay { get { return isMultiPay; } set { isMultiPay = value; } }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get { return price; } set { price = value; } }

        [Required]
        [DataType(DataType.Currency)]
        public decimal SalePrice { get { return saleprice; } set { saleprice = value; } }

        [Required]
        [DataType(DataType.Currency)]
        public decimal OnePayPrice { get { return onePayPrice; } set { onePayPrice = value; } }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Cost { get { return cost; } set { cost = value; } }

        public bool Taxable { get { return taxable; } set { taxable = value; } }

        [Required]
        public decimal Weight { get { return weight; } set { weight = value; } }

        [Required]
        [DataType(DataType.Currency)]
        public decimal PerUnitShipping { get { return perunitshipping; } set { perunitshipping = value; } }

        [Required]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string ShortDescription { get { return shortdescription; } set { shortdescription = value; } }

        [Required]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Description { get { return description; } set { description = value; } }

        [Required]
        public string CartImage { get { return cartimage; } set { cartimage = value; } }

        [Required]
        public string ThumbnailImage { get { return thumbnailimage; } set { thumbnailimage = value; } }

        [Required]
        public string IconImage { get { return iconimage; } set { iconimage = value; } }

        [Required]
        public string DetailsImage { get { return detailsimage; } set { detailsimage = value; } }

        [Required]
        public string ZoomImage { get { return zoomimage; } set { zoomimage = value; } }

        [Required]
        public string OriginalImage { get { return originalimage; } set { originalimage = value; } }

        [Required]
        public int Stock { get { return stock; } set { stock = value; } }

        public bool Enabled { get { return enabled; } set { enabled = value; } }
        public bool CSOnly { get { return cSOnly; } set { cSOnly = value; } }

        [Required]
        public int Sort { get { return sort; } set { sort = value; } }
        public bool InStock { get { return instock; } set { instock = value; } }

        [Required]
        [DataType(DataType.MultilineText)]
        public string OutOfStockMessage { get { return outofstockmessage; } set { outofstockmessage = value; } }

        public bool IsFMSystem { get { return isFMSystem; } set { isFMSystem = value; } }
        public bool IsRefill { get { return isRefill; } set { isRefill = value; } }
        public bool IsBundle { get { return isBundle; } set { isBundle = value; } }

        [Required]
        [DataType(DataType.MultilineText)]
        public string BundleString { get { return bundleString; } set { bundleString = value; } }

        public bool IsShippable { get { return isShippable; } set { isShippable = value; } }

        [Required]
        public string CountryOfOrigin { get { return countryOfOrigin; } set { countryOfOrigin = value; } }

        public long PartnerID { get { return partnerID; } set { partnerID = value; } }

        [DataType(DataType.MultilineText)]
        public string PartnerImageURLs { get { return partnerImageURLs; } set { partnerImageURLs = value; } }

        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string PartnerDescription { get { return partnerDescription; } set { partnerDescription = value; } }

        public string PartnerProductType { get { return partnerProductType; } set { partnerProductType = value; } }

        public bool IsGiftCard { get { return isGiftCard; } set { isGiftCard = value; } }

        public string Category { get { return category; } set { category = value; } }

        public bool IsRefurbished { get { return isRefurbished; } set { isRefurbished = value; } }

        #endregion

        public Product()
        {
            productID = 0;
            sku = "???";
            name = "unknown product";
            isSubscription = false;
            subscriptionPlan = "";
            isMultiPay = false;
            isFMSystem = false;
            isRefill = false;
            price = 0; 
            saleprice = 0;
            onePayPrice = 0;
            cost = 0;
            taxable = false;
            weight = 0;
            perunitshipping = 0;
            shortdescription = "unknown product";
            description = "unknown product";
            cartimage = "ImageNotAvailable.jpg";
            thumbnailimage = "ImageNotAvailable.jpg";
            iconimage = "ImageNotAvailable.jpg";
            detailsimage = "ImageNotAvailable.jpg";
            zoomimage = "ImageNotAvailable.jpg";
            originalimage = "ImageNotAvailable.jpg"; 
            stock = 0;
            enabled = false;
            cSOnly = false;
            sort = 0;
            instock = false;
            outofstockmessage = "Temporarily Out-of-Stock";
            isBundle = false;
            bundleString = "n/a"; //can't be null
            isShippable = true;
            countryOfOrigin = "China";
            partnerID = 0;
            partnerImageURLs = "n/a";
            partnerDescription = "n/a";
            partnerProductType = "n/a";
            isGiftCard = false;
            category = "n/a";
            isRefurbished = false;
        }

        public Product(int FMProductID)
        {
            List<Product> productList = GetProducts();
            Product newP = (from p in productList where p.ProductID == FMProductID select p).FirstOrDefault();
            UpdateProduct(this, newP); //map newP to this
        }

        public Product(string sku)
        {
            List<Product> productList = GetProducts();
            Product newP = (from p in productList where p.Sku == sku select p).FirstOrDefault();
            UpdateProduct(this, newP); //map newP to this
        }

        public Product(long shopifyProductID)
        {
            List<Product> productList = GetProducts();
            Product newP = (from p in productList where p.PartnerID == shopifyProductID select p).FirstOrDefault();
            UpdateProduct(this, newP); //map newP to this
        }

        #region Methods

        public int Save(int ProductID)
        {
            FM2015.Helpers.CacheHelper.Clear(AdminCart.Config.cachekey_ProductList);
            int result = 0;
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (ProductID == 0)
            {
                string sql = @"
                INSERT Products(Sku, Name, Price, SalePrice, OnePayPrice, Taxable, Weight, 
                              PerUnitShipping, ShortDescription, Description, CartImage, ThumbNailImage, IconImage, DetailsImage, 
                              ZoomImage, OriginalImage, Stock, Enabled, CSOnly, Sort, InStock, OutOfStockMessage, 
                              IsFMSystem, IsRefill, IsBundle, BundleString, IsShippable, CountryOfOrigin, IsSubscription, SubscriptionPlan, IsMultiPay,
                              PartnerID, PartnerImageURLs, PartnerDescription, PartnerProductType, IsGiftCard, Category, IsRefurbished) 
                VALUES(@Sku, @Name, @Price, @SalePrice, @OnePayPrice, @Taxable, @Weight, 
                              @PerUnitShipping, @ShortDescription, @Description, @CartImage, @ThumbNailImage, @IconImage, @DetailsImage, 
                              @ZoomImage, @OriginalImage, @Stock, @Enabled, @CSOnly, @Sort, @InStock, @OutOfStockMessage, 
                              @IsFMSystem, @IsRefill, @IsBundle, @BundleString, @IsShippable, @CountryOfOrigin, @IsSubscription, @SubscriptionPlan, @IsMultiPay,
                              @PartnerID, @PartnerImageURLs, @PartnerDescription, @PartnerProductType, @IsGiftCard, @Category, @IsRefurbished); 
                SELECT ProductID=@@identity;";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, sku, name, price, saleprice, onePayPrice, taxable, weight, 
                    perunitshipping, shortdescription, description, cartimage, thumbnailimage, iconimage, detailsimage, 
                    zoomimage, originalimage, stock, enabled, cSOnly, sort, instock, outofstockmessage,
                    isFMSystem, isRefill, isBundle, bundleString, isShippable, countryOfOrigin, isSubscription, subscriptionPlan, isMultiPay,
                    PartnerID, PartnerImageURLs, PartnerDescription, PartnerProductType, IsGiftCard, Category, IsRefurbished); 
                dr = DBUtil.FillDataReader(sql, mySqlParameters);

                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ProductID"]);
                }
                else
                {
                    result = -99;
                }
                if (dr != null) { dr.Close(); }
            }
            else
            {
                string sql = @"
                UPDATE Products
                SET Sku = @Sku,
                    Name = @Name, 
                    Price = @Price, 
                    SalePrice = @SalePrice, 
                    OnePayPrice = @OnePayPrice, 
                    Taxable = @Taxable, 
                    Weight = @Weight, 
                    PerUnitShipping = @PerUnitShipping, 
                    ShortDescription = @ShortDescription, 
                    Description = @Description, 
                    CartImage = @CartImage, 
                    ThumbNailImage = @ThumbNailImage, 
                    IconImage = @IconImage, 
                    DetailsImage = @DetailsImage, 
                    ZoomImage = @ZoomImage, 
                    OriginalImage = @OriginalImage, 
                    Stock = @Stock, 
                    Enabled = @Enabled, 
                    CSOnly = @CSOnly, 
                    Sort = @Sort, 
                    InStock = @InStock, 
                    OutOfStockMessage = @OutOfStockMessage, 
                    IsFMSystem = @IsFMSystem, 
                    IsRefill = @IsRefill, 
                    IsBundle = @IsBundle, 
                    BundleString = @BundleString, 
                    IsShippable = @IsShippable, 
                    CountryOfOrigin = @CountryOfOrigin, 
                    IsSubscription = @IsSubscription, 
                    SubscriptionPlan = @SubscriptionPlan, 
                    IsMultiPay = @IsMultiPay, 
                    PartnerID = @PartnerID, 
                    PartnerImageURLs = @PartnerImageURLs, 
                    PartnerDescription = @PartnerDescription, 
                    PartnerProductType = @PartnerProductType,
                    IsGiftCard = @IsGiftCard, 
                    Category = @Category,  
                    IsRefurbished = @IsRefurbished 
                WHERE ProductID = @ProductID ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, sku, name, price, saleprice, onePayPrice, taxable, weight,
                    perunitshipping, shortdescription, description, cartimage, thumbnailimage, iconimage, detailsimage,
                    zoomimage, originalimage, stock, enabled, cSOnly, sort, instock, outofstockmessage, isFMSystem, isRefill, 
                    isBundle, bundleString, isShippable, countryOfOrigin, isSubscription, subscriptionPlan, isMultiPay, 
                    PartnerID, PartnerImageURLs, PartnerDescription, PartnerProductType, IsGiftCard, Category, IsRefurbished, 
                    ProductID);
                DBUtil.Exec(sql, mySqlParameters);
            }
            return result;
        }
        #endregion

        static public List<Product> GetProducts()
        {
            List<Product> productList = new List<Product>();
            CacheHelper.Get(AdminCart.Config.cachekey_ProductList, out productList);
            if ((productList == null) || (productList.Count == 0))
            {
                productList = new List<Product>();

                string sql = @" 
                SELECT * 
                FROM Products 
                ORDER BY Sort ASC 
            ";
                SqlDataReader dr = null;

                try
                {
                    dr = DBUtil.FillDataReader(sql);
                    while (dr.Read())
                    {
                        Product p = new Product();
                        p.ProductID = Convert.ToInt32(dr["ProductID"]);

                        p.Sku = dr["Sku"].ToString();
                        p.Name = dr["Name"].ToString();

                        p.IsSubscription = Convert.ToBoolean(dr["IsSubscription"]);
                        p.IsMultiPay = Convert.ToBoolean(dr["IsMultiPay"]);

                        p.SubscriptionPlan = dr["SubscriptionPlan"].ToString();

                        p.Price = Convert.ToDecimal(dr["Price"]);
                        p.SalePrice = Convert.ToDecimal(dr["SalePrice"]);
                        p.OnePayPrice = Convert.ToDecimal(dr["OnePayPrice"]);
                        p.Taxable = Convert.ToBoolean(dr["Taxable"]);
                        p.Weight = Convert.ToDecimal(dr["Weight"]);
                        p.PerUnitShipping = Convert.ToDecimal(dr["PerUnitShipping"]);
                        p.ShortDescription = dr["ShortDescription"].ToString();
                        p.Description = dr["Description"].ToString();
                        p.CartImage = dr["CartImage"].ToString();
                        p.ThumbnailImage = dr["ThumbNailImage"].ToString();
                        p.IconImage = dr["IconImage"].ToString();
                        p.DetailsImage = dr["DetailsImage"].ToString();
                        p.ZoomImage = dr["ZoomImage"].ToString();
                        p.OriginalImage = dr["OriginalImage"].ToString();
                        p.Stock = Convert.ToInt32(dr["Stock"]);
                        p.Enabled = Convert.ToBoolean(dr["Enabled"]);
                        p.CSOnly = Convert.ToBoolean(dr["CSOnly"]);
                        p.Sort = Convert.ToInt32(dr["Sort"]);
                        p.InStock = Convert.ToBoolean(dr["InStock"]);
                        p.OutOfStockMessage = dr["OutOfStockMessage"].ToString();

                        p.IsFMSystem = Convert.ToBoolean(dr["IsFMSystem"]);
                        p.IsRefill = Convert.ToBoolean(dr["IsRefill"]);
                        p.IsBundle = Convert.ToBoolean(dr["IsBundle"]);
                        p.BundleString = dr["BundleString"].ToString();
                        p.IsShippable = Convert.ToBoolean(dr["IsShippable"]);
                        p.CountryOfOrigin = dr["CountryOfOrigin"].ToString();

                        p.PartnerID = (dr["PartnerID"] == DBNull.Value) ? 0 : Convert.ToInt64(dr["PartnerID"]);
                        p.PartnerImageURLs = dr["PartnerImageURLs"].ToString();
                        p.PartnerDescription = dr["PartnerDescription"].ToString();
                        p.PartnerProductType = dr["PartnerProductType"].ToString();
                        p.IsGiftCard = Convert.ToBoolean(dr["IsGiftCard"]);
                        p.Category = dr["Category"].ToString();
                        p.IsRefurbished = Convert.ToBoolean(dr["IsRefurbished"]);

                        ProductCost pcost = new ProductCost(p.ProductID); //get most recent cost
                        p.Cost = pcost.Cost;

                        productList.Add(p);
                    }
                }
                catch (Exception ex) //just ignore exceptions
                { string msg = ex.Message; }

                if (dr != null) { dr.Close(); }
                if (productList.Count > 0)
                {
                    FM2015.Helpers.CacheHelper.Add<List<Product>>(productList, Config.cachekey_ProductList, Config.cacheDuration_ProductList);
                }
            }
            if (productList.Count == 0)
            {
                mvc_Mail.SendDiagEmail("Products:GetProducts: count = 0", "");
            }
            return productList;
        }

        public static List<Product> GetProducts(bool customerOnly)
        {
            List<Product> productList = GetProducts();

            if (customerOnly) //show only those products we want customer to see
            {
                productList = (from p in productList where (p.CSOnly = false && p.Enabled == true) select p).ToList();
            }
            else //show all enabled products
            {
                productList = (from p in productList where (p.Enabled == true) select p).ToList();
            }

            return productList;
        }

        public static List<Product> GetProducts(bool customerOnly, bool includeSubscriptions)
        {
            List<Product> productList = GetProducts(customerOnly);

            productList = (from p in productList where (p.IsSubscription == includeSubscriptions) select p).ToList();

            return productList;
        }

        public static List<Product> GetSubProducts()
        {
            List<Product> productList = GetProducts();

            productList = (from p in productList where (p.IsSubscription == true && p.Enabled == true && p.CSOnly == true) select p).ToList();

            return productList;

        }

        public static void PostFMProductToShopify(Product product)
        {
            ShopifyAgent.ShopifyProductSingle shopProduct = new ShopifyAgent.ShopifyProductSingle();
            shopProduct.product.Body_html = product.ShortDescription;
            shopProduct.product.Title = product.Name;
            shopProduct.product.Vendor = "FaceMaster";
            shopProduct.product.Product_type = product.PartnerProductType;

            List<ShopifyAgent.ProductVariant> vList = new List<ShopifyAgent.ProductVariant>();
            ShopifyAgent.ProductVariant variant = new ShopifyAgent.ProductVariant();
            variant.Sku = product.Sku;
            variant.Fulfillment_service = "manual";
            variant.Grams = Convert.ToDouble(product.Weight * Convert.ToDecimal(453.59)); //pounds to grams
            variant.Weight = Convert.ToDouble(product.Weight);
            variant.Weight_unit = "lb";
            variant.Price = product.SalePrice;
            variant.Requires_shipping = product.IsShippable;
            variant.Taxable = true;
            vList.Add(variant);
            shopProduct.product.variants = vList.ToArray();

            List<ShopifyAgent.ProductImage> imgList = new List<ShopifyAgent.ProductImage>();
            ShopifyAgent.ProductImage image = new ShopifyAgent.ProductImage();
            image.Src = "http://www.facemaster.com/images/products/" + product.ThumbnailImage;
            imgList.Add(image);
            shopProduct.product.images = imgList.ToArray();

            shopProduct.product.image.Src = "http://www.facemaster.com/images/products/" + product.ThumbnailImage;

            //Debug... using a static string
            string jsonStr = @"{""product"":{""title"":""#TITLE#"",""body_html"":""#BODY#"",""vendor"":""FaceMaster"",""product_type"":""FaceMaster"",""images"":[{""src"":""#IMAGE-SRC#""}],""variants"":[{""title"":""#TITLE#"",""price"":""#PRICE#"",""sku"":""#SKU#"",""weight"":""#WEIGHT#""}]}}";
            jsonStr = jsonStr.Replace("#TITLE#", product.Name);
            jsonStr = jsonStr.Replace("#BODY#", product.ShortDescription);
            jsonStr = jsonStr.Replace("#IMAGE-SRC#", "http://www.facemaster.com/images/products/" + product.ThumbnailImage);
            jsonStr = jsonStr.Replace("#PRICE#", product.SalePrice.ToString("C"));
            jsonStr = jsonStr.Replace("#SKU#", product.sku);
            jsonStr = jsonStr.Replace("#WEIGHT#", product.Weight.ToString());
            jsonStr = jsonStr.Replace("\r", ""); //get rid of new line stuff; tends to blow the API processor up
            jsonStr = jsonStr.Replace("\n", "");

            //string jsonStr = JsonConvert.SerializeObject(shopProduct);
            string jsonURL = "admin/products.json";
            //response holds new product json from Shopify, if new product was created OK
            string response = JsonHelpers.POSTtoShopify(jsonURL, jsonStr);

            try
            {
                ShopifyAgent.ShopifyProductSingle responseProduct = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyProductSingle>(response);
                product.PartnerID = responseProduct.product.Id;
                product.PartnerImageURLs = responseProduct.product.image.Id.ToString() + "," + responseProduct.product.image.Src;
                product.PartnerDescription = responseProduct.product.Body_html;
                product.Save(product.ProductID);
            }
            catch
            {
                //response wasn't a good Product response
            }
        }

        private static void SendExceptionEmail(string msg, Exception ex)
        {
            dbErrorLogging.LogError(msg, ex);
            //string mailFrom = ConfigurationManager.AppSettings["MailFrom"].ToString();
            //string mailTo = ConfigurationManager.AppSettings["MailTo"].ToString(); 
            //Mail.SendMail(mailFrom, mailTo, "Product Exception", msg + "sysMsg: " + ex.Message + ex.StackTrace + ex.TargetSite + ex.ToString());
            //throw ex;
        }

        private void UpdateProduct(Product p1, Product p2)
        {
            //p1 = p2;
            if (p2 == null)
            {
                p2 = new Product(); //respond with product id = 0 and let caller deal with it
            }
            p1.ProductID = p2.ProductID;
            p1.Sku = p2.Sku;
            p1.Name = p2.Name;

            p1.IsSubscription = p2.IsSubscription;
            p1.IsMultiPay = p2.IsMultiPay;

            p1.SubscriptionPlan = p2.SubscriptionPlan;

            p1.Price = p2.Price;
            p1.SalePrice = p2.SalePrice;
            p1.OnePayPrice = p2.OnePayPrice;
            p1.Taxable = p2.Taxable;
            p1.Weight = p2.Weight;
            p1.PerUnitShipping = p2.PerUnitShipping;
            p1.ShortDescription = p2.ShortDescription;
            p1.Description = p2.Description;
            p1.CartImage = p2.CartImage;
            p1.ThumbnailImage = p2.ThumbnailImage;
            p1.IconImage = p2.IconImage;
            p1.DetailsImage = p2.DetailsImage;
            p1.ZoomImage = p2.ZoomImage;
            p1.OriginalImage = p2.OriginalImage;
            p1.Stock = p2.Stock;
            p1.Enabled = p2.Enabled;
            p1.CSOnly = p2.CSOnly;
            p1.Sort = p2.Sort;
            p1.InStock = p2.InStock;
            p1.OutOfStockMessage = p2.OutOfStockMessage;

            p1.IsFMSystem = p2.IsFMSystem;
            p1.IsRefill = p2.IsRefill;
            p1.IsBundle = p2.IsBundle;
            p1.BundleString = p2.BundleString;
            p1.IsShippable = p2.IsShippable;
            p1.CountryOfOrigin = p2.CountryOfOrigin;

            p1.PartnerID = p2.PartnerID;
            p1.PartnerImageURLs = p2.PartnerImageURLs;
            p1.PartnerDescription = p2.PartnerDescription;
            p1.PartnerProductType = p2.PartnerProductType;
            p1.IsGiftCard = p2.IsGiftCard;
            p1.Category = p2.Category;

            p1.Cost = p2.Cost;
        }


        #region System.Object Overrides

        public override string ToString()
        {
            return string.Format("{0}", productID.ToString());
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (Object.ReferenceEquals(this, obj)) return true;
            if (this.GetType() != obj.GetType()) return false;

            Product objItem = (Product)obj;
            if (productID == objItem.productID) return true;

            return false;
        }

        public override int GetHashCode()
        {
            return productID.GetHashCode();
        }

        #endregion

    }
}
