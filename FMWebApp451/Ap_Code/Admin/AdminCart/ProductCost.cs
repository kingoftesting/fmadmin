using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace AdminCart
{

    public class ProductCost
    {
        #region Variables
        private int productcostID;
        private int productID;
        private DateTime startdate; //SQL DateTime
        private decimal cost; //SQL Money
        #endregion

        #region Public Properties
        public int ProductCostID { get { return productcostID; } set { productcostID = value; } }
        public int ProductID { get { return productID; } set { productID = value; } }
        public DateTime StartDate { get { return startdate; } set { startdate = value; } }
        public decimal Cost { get { return cost; } set { cost = value; } }
        #endregion

        #region Constructors
        public ProductCost()
        {
            productcostID = 0;
            productID = 0;
            startdate = DateTime.Now;
            cost = 0;
        }

        public ProductCost(int ProductID)
        {
            List<ProductCost> productCostList = GetAll();
            ProductCost newP = (from p in productCostList where p.ProductID == ProductID select p).FirstOrDefault();
            if (newP == null)
            {
                this.productcostID = 0;
                this.productID = 0;
                this.startdate = DateTime.Now;
                this.cost = 0;
            }
            else
            {
                this.productcostID = newP.ProductCostID;
                this.productID = newP.ProductID;
                this.startdate = newP.StartDate;
                this.cost = newP.Cost;
            }
        }

        public ProductCost(int ProductID, DateTime AsOfDate)
        {
            List<ProductCost> productCostList = GetAll();
            ProductCost newP = (from p in productCostList
                                where (p.ProductID == ProductID && p.StartDate > AsOfDate)
                                select p).FirstOrDefault();
            if (newP == null)
            {
                this.productcostID = 0;
                this.productID = 0;
                this.startdate = Convert.ToDateTime("1/1/2006");
                this.cost = 0;
            }
            else
            {
                this.productcostID = newP.ProductCostID;
                this.productID = newP.ProductID;
                this.startdate = newP.StartDate;
                this.cost = newP.Cost;
            }
        }
        #endregion


        #region Methods

        public int Save(int ProductID)
        {
            CacheHelper.Clear(Config.cachekey_ProductCostList);
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            string sql = "";
           ProductCost p = new ProductCost(ProductID);
           int result = 0;
           if ((cost != p.Cost) || (startdate != p.startdate)) //no need to update cost if nothing has changed
           {
                sql = @"
                INSERT ProductCosts(ProductID, StartDate, Cost) 
                VALUES(@ProductID, @StartDate, @Cost); SELECT ProductCostID=@@identity;";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, productID, startdate, cost);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);

                if (dr.Read())
                { result = Convert.ToInt32(dr["ProductCostID"]); }
                else
                { result = -99; }
            }
            if (dr != null) { dr.Close(); }

            return result;
        }

        static public List<ProductCost> GetProductCostList(int ProductID)
        {
            List<ProductCost> theList = GetAll();
            theList = (from p in theList where p.ProductID == ProductID select p).ToList();
            return theList;
        }

        public static List<ProductCost> GetAll()
        {
            List<ProductCost> theList = new List<ProductCost>();
            CacheHelper.Get(AdminCart.Config.cachekey_ProductCostList, out theList);
            if ((theList == null) || (theList.Count == 0))
            {
                theList = new List<ProductCost>();
                string sql = @"
                    SELECT * 
                    FROM ProductCosts 
                    ORDER BY StartDate DESC 
                    ";

                SqlDataReader dr = DBUtil.FillDataReader(sql);
                try
                {
                    while (dr.Read())
                    {
                        ProductCost obj = new ProductCost();

                        obj.productcostID = Convert.ToInt32(dr["ProductCostId"]);
                        obj.productID = Convert.ToInt32(dr["ProductId"]);
                        obj.startdate = Convert.ToDateTime(dr["startdate"].ToString());
                        obj.cost = Convert.ToDecimal(dr["cost"]);

                        theList.Add(obj);
                    }
                }
                catch (Exception ex)
                { string msg = ex.Message; }
                
                if (dr != null) { dr.Close(); }
                if (theList.Count > 0)
                {
                    CacheHelper.Add<List<ProductCost>>(theList, Config.cachekey_ProductCostList, Config.cacheDuration_ProductCostList);
                }
            }

            return theList;
        }
        #endregion

    }
}
