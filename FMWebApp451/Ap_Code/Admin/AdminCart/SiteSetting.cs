using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace AdminCart
{
    public class SiteSettings
    {
        //Built with template TemplateBasic.cs.tem
	    #region Fields & Properties

	    private int siteSettingsID;
	    public int SiteSettingsID	{	get{	return siteSettingsID;	} 	set	{siteSettingsID=value;	}}

        //"FreeShipping": site-wide FreeShipping, US-only;
        //some name, like "FaceMakeOver-1" used for a AdWords Campaign
	    private string siteSettingsName;
	    public string SiteSettingsName	{	get{	return siteSettingsName;	} 	set	{siteSettingsName=value;	}}

        //explain something about this sitesetting, so we understand why it was created
        private string siteSettingsDescription;
        public string SiteSettingsDescription { get { return siteSettingsDescription; } set { siteSettingsDescription = value; } }

        //this is sitesetting-specific discount and is the value applied to the sitesetting action
        //for example, Trigger=1, TriggerValue=19; then Product=19 receives a lineitem discount = Value
        private string siteSettingsValue;
	    public string SiteSettingsValue	{	get{	return siteSettingsValue;	} 	set	{siteSettingsValue=value;	}}

        // 0 = No Trigger
        // 1 = subtotal discount, dollar amount; trigger value = n/a; MinProduct = $x (like $50)
        // 2 = subtotal discount, %-off amount; trigger value = n/a; MinProduct = $x (like $50)
        // 3 = lineitem discount, dollar amount; trigger value = productID
        // 4 = lineitem discount, %-off amount; trigger value = productID
        private string siteSettingsTrigger;
	    public string SiteSettingsTrigger	{	get{	return siteSettingsTrigger;	} 	set	{siteSettingsTrigger=value;	}}

        //depends on Trigger above
        private string siteSettingsTriggerValue;
	    public string SiteSettingsTriggerValue	{	get{	return siteSettingsTriggerValue;	} 	set	{siteSettingsTriggerValue=value;	}}

        //used to establish a minimum product subtotal before any discount us applied (ie; 25% off all orders above $75)
        private decimal siteSettingsMinProduct;
	    public decimal SiteSettingsMinProduct	{	get{	return siteSettingsMinProduct;	} 	set	{siteSettingsMinProduct=value;	}}


	    private DateTime siteSettingsStartDate;
	    public DateTime SiteSettingsStartDate	{	get{	return siteSettingsStartDate;	} 	set	{siteSettingsStartDate=value;	}}

        private DateTime siteSettingsEndDate;
	    public DateTime SiteSettingsEndDate	{	get{	return siteSettingsEndDate;	} 	set	{siteSettingsEndDate=value;	}}

        //if true, then FreeShipping US-only; separate from special case where Name = FreeShipping; this is used with a promotion
        private bool siteSettingsFreeShipping;
        public bool SiteSettingsFreeShipping { get { return siteSettingsFreeShipping; } set { siteSettingsFreeShipping = value; } }
        #endregion
	
	
	#region Constructors
	public SiteSettings()
	{
		siteSettingsID = 0;
		siteSettingsName = "";  
		siteSettingsValue = "";
        siteSettingsTrigger = "";
        siteSettingsTriggerValue = "";
        siteSettingsMinProduct = 0;
		siteSettingsStartDate = DateTime.Parse("1/1/1900");
		siteSettingsEndDate = DateTime.Parse("1/1/1900");
        siteSettingsDescription = "";
        siteSettingsFreeShipping = false;

	}

        public SiteSettings(int SiteSettingsID)
        {
            string sql = @" 
                select * 
                from SiteSettings 
                where SiteSettingsID = @SiteSettingsID 
            "; 


            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, SiteSettingsID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read())
            {
                siteSettingsID = Int32.Parse(dr["siteSettingsID"].ToString());
                siteSettingsName = dr["siteSettingsName"].ToString();
                siteSettingsDescription = dr["siteSettingsDescription"].ToString();
                siteSettingsFreeShipping = Convert.ToBoolean(dr["siteSettingsFreeShipping"]);
                siteSettingsValue = dr["siteSettingsValue"].ToString();
                siteSettingsTrigger = dr["siteSettingsTrigger"].ToString();
                siteSettingsTriggerValue = dr["siteSettingsTriggerValue"].ToString();

                //s.siteSettingsMinProduct = (dr["siteSettingsMinProduct"] == null) ? 0 : Decimal.Parse(dr["siteSettingsMinProduct"].ToString());
                if (dr["siteSettingsMinProduct"] == DBNull.Value) { siteSettingsMinProduct = 0; }
                else { siteSettingsMinProduct = Convert.ToDecimal(dr["siteSettingsMinProduct"]); }

                siteSettingsStartDate = DateTime.Parse(dr["siteSettingsStartDate"].ToString());
                siteSettingsEndDate = DateTime.Parse(dr["siteSettingsEndDate"].ToString());
            }
            if (dr != null) dr.Close();
        }
    #endregion
	
	#region Methods
	
        public void Save()
        {
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            if (siteSettingsID == 0)
            {
                sql = @"
                INSERT INTO SiteSettings   
                (siteSettingsName, siteSettingsDescription, siteSettingsValue, siteSettingsFreeShipping, 
                siteSettingsTrigger, siteSettingsTriggerValue, siteSettingsMinProduct, siteSettingsStartDate, siteSettingsEndDate)
  
                VALUES (@siteSettingsName, @siteSettingsDescription, @siteSettingsValue, @siteSettingsFreeShipping, @siteSettingsTrigger, @siteSettingsTriggerValue, 
                @siteSettingsMinProduct, @siteSettingsStartDate, @siteSettingsEndDate)
  
                SELECT siteSettingsID = scope_identity()  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, siteSettingsName, siteSettingsDescription, siteSettingsValue, siteSettingsFreeShipping, siteSettingsTrigger, siteSettingsTriggerValue, siteSettingsMinProduct, siteSettingsStartDate, siteSettingsEndDate);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read()) { siteSettingsID = Convert.ToInt32(dr["siteSettingsID"]); }
            }
            else
            {

                sql = @"
                UPDATE SiteSettings 
                SET 
                    siteSettingsName = @siteSettingsName, 
                    siteSettingsDescription = @siteSettingsDescription, 
                    siteSettingsValue = @siteSettingsValue, 
                    siteSettingsFreeShipping = @siteSettingsFreeShipping, 
                    siteSettingsTrigger = @siteSettingsTrigger, 
                    siteSettingsTriggerValue = @siteSettingsTriggerValue, 
                    siteSettingsMinProduct = @siteSettingsMinProduct, 
                    siteSettingsStartDate = @siteSettingsStartDate, 
                    siteSettingsEndDate = @siteSettingsEndDate 
                WHERE siteSettingsID = @siteSettingsID 
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, siteSettingsName, siteSettingsDescription, siteSettingsValue, siteSettingsFreeShipping, siteSettingsTrigger, siteSettingsTriggerValue, siteSettingsMinProduct, siteSettingsStartDate, siteSettingsEndDate, siteSettingsID);
                DBUtil.Exec(sql, mySqlParameters);
            }
            if (dr != null) { dr.Close(); }
        }

    /***********************************
    * Static methods
    ************************************/
    /// <summary>
        /// Returns a SiteSettings object from an ID
    /// </summary>
        public static SiteSettings GetSiteSettingsByID(int SiteSettingsID)
    {
        SiteSettings obj = new SiteSettings(SiteSettingsID);
        return obj;
    }

    /// <summary>
    /// Creates a new SiteSettings
    /// </summary>
        public static int InsertSiteSettings(string siteSettingsName, string siteSettingsDescription, string siteSettingsValue, bool siteSettingsFreeShipping, string siteSettingsTrigger, string siteSettingsTriggerValue, decimal siteSettingsMinProduct, DateTime siteSettingsStartDate, DateTime siteSettingsEndDate)
    {
        SiteSettings obj = new SiteSettings();
        obj.SiteSettingsID = 0;
        obj.SiteSettingsName = siteSettingsName;
        obj.SiteSettingsDescription = siteSettingsDescription;
        obj.SiteSettingsValue = siteSettingsValue;
        obj.SiteSettingsFreeShipping = siteSettingsFreeShipping;
        obj.SiteSettingsTrigger = siteSettingsTrigger;
        obj.SiteSettingsTriggerValue = siteSettingsTriggerValue;
        obj.SiteSettingsMinProduct = siteSettingsMinProduct;
        obj.SiteSettingsStartDate = siteSettingsStartDate;
        obj.SiteSettingsEndDate = siteSettingsEndDate;
        obj.Save();
        return obj.SiteSettingsID;
    }

    /// <summary>
    /// Updates a Coupon
    /// </summary>
        public static bool UpdateSiteSettings(int siteSettingsID, string siteSettingsName, string siteSettingsDescription, string siteSettingsValue, bool siteSettingsFreeShipping, string siteSettingsTrigger, string siteSettingsTriggerValue, decimal siteSettingsMinProduct, DateTime siteSettingsStartDate, DateTime siteSettingsEndDate)
    {
        SiteSettings obj = new SiteSettings();
        obj.SiteSettingsID = siteSettingsID;
        obj.SiteSettingsName = siteSettingsName;
        obj.SiteSettingsDescription = siteSettingsDescription;
        obj.SiteSettingsValue = siteSettingsValue;
        obj.SiteSettingsFreeShipping = siteSettingsFreeShipping;
        obj.SiteSettingsTrigger = siteSettingsTrigger;
        obj.SiteSettingsTriggerValue = siteSettingsTriggerValue;
        obj.SiteSettingsMinProduct = siteSettingsMinProduct;
        obj.SiteSettingsStartDate = siteSettingsStartDate;
        obj.SiteSettingsEndDate = siteSettingsEndDate;

        obj.Save();
        return true;
    }

        static public List<SiteSettings> GetSiteSettingsList()
	{
        List<SiteSettings> thelist = new List<SiteSettings>();

        string sql = @" 
                select * 
                from SiteSettings 
            "; 
	    
	    SqlDataReader dr = DBUtil.FillDataReader(sql);
	    
	    while(dr.Read())
	    {
            SiteSettings s = new SiteSettings();

            s.siteSettingsID = Int32.Parse(dr["siteSettingsID"].ToString());
            s.siteSettingsName = dr["siteSettingsName"].ToString();
            s.siteSettingsDescription = dr["siteSettingsDescription"].ToString();
            s.siteSettingsFreeShipping = Convert.ToBoolean(dr["siteSettingsFreeShipping"]);
            s.siteSettingsValue = dr["siteSettingsValue"].ToString();
            s.siteSettingsTrigger = dr["siteSettingsTrigger"].ToString();
            s.siteSettingsTriggerValue = dr["siteSettingsTriggerValue"].ToString();

            //s.siteSettingsMinProduct = (dr["siteSettingsMinProduct"] == null) ? 0 : Decimal.Parse(dr["siteSettingsMinProduct"].ToString());
            if (dr["siteSettingsMinProduct"] == DBNull.Value) { s.siteSettingsMinProduct = 0; }
            else { s.siteSettingsMinProduct = Convert.ToDecimal(dr["siteSettingsMinProduct"]); }
 
            s.siteSettingsStartDate = DateTime.Parse(dr["siteSettingsStartDate"].ToString());
            s.siteSettingsEndDate = DateTime.Parse(dr["siteSettingsEndDate"].ToString());

            thelist.Add(s);
	    }
        if (dr != null) { dr.Close(); }
	    
	    return thelist;
	
	}
	#endregion

    }
}
