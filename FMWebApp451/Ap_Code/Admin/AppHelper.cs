﻿using System;
using System.Data;
using System.Configuration;
using System.Web;

using System.IO;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;

/// <summary>
/// Summary description for AppHelper
/// </summary>
public class AppHelper
{
    public Bitmap BitmapFromByteStream(byte[] imageBytes)
    {
        MemoryStream stream = new MemoryStream(imageBytes, false);
        Image image = Image.FromStream(stream);
        Bitmap b = new Bitmap(image);

        return b;
    }


    public void ProcessRequest(HttpContext context, Int32 id)
    {
       //Int32 id;
       //if (context.Request.QueryString["id"] != null)
       //     id = Convert.ToInt32(context.Request.QueryString["id"]);
       //else
       //     throw new ArgumentException("No parameter specified");
 
       Image image = GetImage(id);
       context.Response.ContentType = "image/jpeg";             
        // Save the image to the OutputStream
       if (image != null)
       {
           image.Save(context.Response.OutputStream, ImageFormat.Jpeg);
       }
    }
 
    public Image GetImage(int ID)
    {
        SqlConnection connection = new SqlConnection(AdminCart.Config.ConnStr());
        string sql = @"
SELECT picture 
FROM fmtestimonialpictures 
WHERE PictureID = @ID";
        SqlCommand cmd = new SqlCommand(sql,connection);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@ID", ID);
        connection.Open();
        object img = cmd.ExecuteScalar();
        Image image = null;
        try
        {
            MemoryStream stream =  new MemoryStream((byte[])img);
            image = Image.FromStream(stream);
        }
        catch
        {
            image = null;
        }
        finally
        {
        }
        connection.Close();
        return image;
    }
 
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
 
 
}

