﻿using System;
using System.Data;

using System.Data.SqlClient;
using System.Collections.Generic;
using AdminCart;
using FM2015.Helpers;

/// <summary>
/// Summary description for FMTestimonial
/// </summary>
public class FMTestimonial
{
    //Built with ClassHatch Version Build: 4.0.1  10/2009.  Template file:ExtendedClass.cs.tem

    #region Fields
    private int fMTestimonialID;
    private DateTime submitDate;
    private string submitIP;
    private string testimonial;
    private bool signature;
    private DateTime signatureDate;
    private string email;
    private string firstName;
    private string lastName;
    private string address;
    private string city;
    private string state;
    private string zip;
    private string phone;
    private DateTime birthdate;
    private int beforePicID;
    private int afterPicID;
    #endregion

    #region Properties
    public int FMTestimonialID { get { return fMTestimonialID; } set { fMTestimonialID = value; } }
    public DateTime SubmitDate { get { return submitDate; } set { submitDate = value; } }
    public string SubmitIP { get { return submitIP; } set { submitIP = value; } }
    public string Testimonial { get { return testimonial; } set { testimonial = value; } }
    public bool Signature { get { return signature; } set { signature = value; } }
    public DateTime SignatureDate { get { return signatureDate; } set { signatureDate = value; } }
    public string Email { get { return email; } set { email = value; } }
    public string FirstName { get { return firstName; } set { firstName = value; } }
    public string LastName { get { return lastName; } set { lastName = value; } }
    public string Address { get { return address; } set { address = value; } }
    public string City { get { return city; } set { city = value; } }
    public string State { get { return state; } set { state = value; } }
    public string Zip { get { return zip; } set { zip = value; } }
    public string Phone { get { return phone; } set { phone = value; } }
    public DateTime Birthdate { get { return birthdate; } set { birthdate = value; } }
    public int BeforePicID { get { return beforePicID; } set { beforePicID = value; } }
    public int AfterPicID { get { return afterPicID; } set { afterPicID = value; } }
    #endregion

    #region Constructors
    public FMTestimonial()
		{
			fMTestimonialID = 0;
			submitDate = DateTime.Parse("1/1/1900");
			submitIP = "";
			testimonial = "";
			signature = false;
			signatureDate = DateTime.Parse("1/1/1900");
			email = "";
			firstName = "";
			lastName = "";
			address = "";
			city = "";
			state = "";
			zip = "";
			phone = "";
			birthdate = DateTime.Parse("1/1/1900");
            beforePicID = 0;
            afterPicID = 0;
		}
    public FMTestimonial(int PrimaryKeyValue)
    {
        string sql = @"
			SELECT *
			FROM FMTestimonials
			WHERE FMTestimonialID = @PrimaryKeyValue";
        SqlParameter[] parms = new SqlParameter[1];
        parms[0] = new SqlParameter("@PrimaryKeyValue", PrimaryKeyValue);

        SqlDataReader dr = DBUtil.FillDataReader(sql, parms);

        while (dr.Read())
        {
            this.fMTestimonialID = Convert.ToInt32(dr["fMTestimonialID"].ToString());
            this.submitDate = Convert.ToDateTime(dr["submitDate"].ToString());
            this.submitIP = Convert.ToString(dr["submitIP"].ToString());
            this.testimonial = dr["testimonial"].ToString();
            this.signature = Convert.ToBoolean(dr["signature"].ToString());
            this.signatureDate = Convert.ToDateTime(dr["signatureDate"].ToString());
            this.email = Convert.ToString(dr["email"].ToString());
            this.firstName = Convert.ToString(dr["firstName"].ToString());
            this.lastName = Convert.ToString(dr["lastName"].ToString());
            this.address = Convert.ToString(dr["address"].ToString());
            this.city = Convert.ToString(dr["city"].ToString());
            this.state = Convert.ToString(dr["state"].ToString());
            this.zip = Convert.ToString(dr["zip"].ToString());
            this.phone = Convert.ToString(dr["phone"].ToString());
            this.birthdate = Convert.ToDateTime(dr["birthdate"].ToString());
            this.beforePicID = Convert.ToInt32(dr["beforePicID"].ToString());
            this.afterPicID = Convert.ToInt32(dr["afterPicID"].ToString());
        }
        dr.Close();
    }

    #endregion

    #region Methods
    //Note...not implemented
    public int LoadFromLine(string line)
    {
        /*
        //parses line from file to load into class
        fMTestimonialID = line.Substring(0,); //each line needs a length added - todo
        submitDate = line.Substring(1,); //each line needs a length added - todo
        submitIP = line.Substring(2,15); //each line needs a length added - todo
        testimonial = line.Substring(2,2147483647); //each line needs a length added - todo
        signature = line.Substring(2,); //each line needs a length added - todo
        signatureDate = line.Substring(3,); //each line needs a length added - todo
        email = line.Substring(4,60); //each line needs a length added - todo
        firstName = line.Substring(4,50); //each line needs a length added - todo
        lastName = line.Substring(4,50); //each line needs a length added - todo
        address = line.Substring(4,50); //each line needs a length added - todo
        city = line.Substring(4,50); //each line needs a length added - todo
        state = line.Substring(4,50); //each line needs a length added - todo
        zip = line.Substring(4,50); //each line needs a length added - todo
        phone = line.Substring(4,50); //each line needs a length added - todo
        birthdate = line.Substring(4,); //each line needs a length added - todo
        BeforePicID = line.Substring(5,255); //each line needs a length added - todo
        afterPicID = line.Substring(5,255); //each line needs a length added - todo
        */
        return 0;
    }

    public int SaveDB()
    {
        //db code to write record
        SqlConnection conn = new SqlConnection(Config.ConnStr());

        string sql = "";


        if (FMTestimonialID == 0)
        {
            //this is an insert
            sql = @"
				INSERT FMTestimonials(
--FMTestimonialID,
SubmitDate,
SubmitIP,
Testimonial,
Signature,
SignatureDate,
Email,
FirstName,
LastName,
Address,
City,
State,
Zip,
Phone,
Birthdate,
BeforePicID,
afterPicID

				 )VALUES(
--@FMTestimonialID,
@SubmitDate,
@SubmitIP,
@Testimonial,
@Signature,
@SignatureDate,
@Email,
@FirstName,
@LastName,
@Address,
@City,
@State,
@Zip,
@Phone,
@Birthdate,
@BeforePicID,
@afterPicID

				)";

        }
        else
        {
            //this is an update
            sql = @"
				UPDATE FMTestimonials
				SET 
				  fMTestimonialID = @fMTestimonialID,
				  submitDate = @submitDate,
				  submitIP = @submitIP,
				  testimonial = @testimonial,
				  signature = @signature,
				  signatureDate = @signatureDate,
				  email = @email,
				  firstName = @firstName,
				  lastName = @lastName,
				  address = @address,
				  city = @city,
				  state = @state,
				  zip = @zip,
				  phone = @phone,
				  birthdate = @birthdate,
				  BeforePicID = @BeforePicID,
				  afterPicID = @afterPicID,
				WHERE FMTestimonialID = PrimaryKeyFieldValue";
        }

        SqlCommand cmd = new SqlCommand(sql, conn);

        SqlParameter fMTestimonialIDParam = new SqlParameter("@fMTestimonialID", SqlDbType.Int);
        SqlParameter submitDateParam = new SqlParameter("@submitDate", SqlDbType.DateTime);
        SqlParameter submitIPParam = new SqlParameter("@submitIP", SqlDbType.VarChar);
        SqlParameter testimonialParam = new SqlParameter("@testimonial", SqlDbType.Text);
        SqlParameter signatureParam = new SqlParameter("@signature", SqlDbType.Bit);
        SqlParameter signatureDateParam = new SqlParameter("@signatureDate", SqlDbType.DateTime);
        SqlParameter emailParam = new SqlParameter("@email", SqlDbType.VarChar);
        SqlParameter firstNameParam = new SqlParameter("@firstName", SqlDbType.VarChar);
        SqlParameter lastNameParam = new SqlParameter("@lastName", SqlDbType.VarChar);
        SqlParameter addressParam = new SqlParameter("@address", SqlDbType.VarChar);
        SqlParameter cityParam = new SqlParameter("@city", SqlDbType.VarChar);
        SqlParameter stateParam = new SqlParameter("@state", SqlDbType.VarChar);
        SqlParameter zipParam = new SqlParameter("@zip", SqlDbType.VarChar);
        SqlParameter phoneParam = new SqlParameter("@phone", SqlDbType.VarChar);
        SqlParameter birthdateParam = new SqlParameter("@birthdate", SqlDbType.DateTime);
        SqlParameter BeforePicIDParam = new SqlParameter("@BeforePicID", SqlDbType.VarChar);
        SqlParameter afterPicIDParam = new SqlParameter("@afterPicID", SqlDbType.VarChar);

        fMTestimonialIDParam.Value = fMTestimonialID;
        submitDateParam.Value = submitDate;
        submitIPParam.Value = submitIP;
        testimonialParam.Value = testimonial;
        signatureParam.Value = signature;
        signatureDateParam.Value = signatureDate;
        emailParam.Value = email;
        firstNameParam.Value = firstName;
        lastNameParam.Value = lastName;
        addressParam.Value = address;
        cityParam.Value = city;
        stateParam.Value = state;
        zipParam.Value = zip;
        phoneParam.Value = phone;
        birthdateParam.Value = birthdate;
        BeforePicIDParam.Value = beforePicID;
        afterPicIDParam.Value = afterPicID;

        cmd.Parameters.Add(fMTestimonialIDParam);
        cmd.Parameters.Add(submitDateParam);
        cmd.Parameters.Add(submitIPParam);
        cmd.Parameters.Add(testimonialParam);
        cmd.Parameters.Add(signatureParam);
        cmd.Parameters.Add(signatureDateParam);
        cmd.Parameters.Add(emailParam);
        cmd.Parameters.Add(firstNameParam);
        cmd.Parameters.Add(lastNameParam);
        cmd.Parameters.Add(addressParam);
        cmd.Parameters.Add(cityParam);
        cmd.Parameters.Add(stateParam);
        cmd.Parameters.Add(zipParam);
        cmd.Parameters.Add(phoneParam);
        cmd.Parameters.Add(birthdateParam);
        cmd.Parameters.Add(BeforePicIDParam);
        cmd.Parameters.Add(afterPicIDParam);

        conn.Open();
        cmd.ExecuteNonQuery();
        conn.Close();
        return 0;
    }

    static public List<FMTestimonial> GetFMTestimonialList()
    {
        List<FMTestimonial> thelist = new List<FMTestimonial>();

        string sql = @"
        SELECT * 
        FROM FMTestimonials
        ORDER BY FMTestimonialID DESC
    ";

        SqlDataReader dr = DBUtil.FillDataReader(sql);

        while (dr.Read())
        {
            FMTestimonial obj = new FMTestimonial();

            obj.fMTestimonialID = Convert.ToInt32(dr["fMTestimonialID"].ToString());
            obj.submitDate = Convert.ToDateTime(dr["submitDate"].ToString());
            obj.submitIP = Convert.ToString(dr["submitIP"].ToString());
            obj.testimonial = dr["testimonial"].ToString();
            obj.signature = Convert.ToBoolean(dr["signature"].ToString());
            obj.signatureDate = Convert.ToDateTime(dr["signatureDate"].ToString());
            obj.email = Convert.ToString(dr["email"].ToString());
            obj.firstName = Convert.ToString(dr["firstName"].ToString());
            obj.lastName = Convert.ToString(dr["lastName"].ToString());
            obj.address = Convert.ToString(dr["address"].ToString());
            obj.city = Convert.ToString(dr["city"].ToString());
            obj.state = Convert.ToString(dr["state"].ToString());
            obj.zip = Convert.ToString(dr["zip"].ToString());
            obj.phone = Convert.ToString(dr["phone"].ToString());
            obj.birthdate = Convert.ToDateTime(dr["birthdate"].ToString());
            obj.beforePicID = Convert.ToInt32(dr["beforePicID"].ToString());
            obj.afterPicID = Convert.ToInt32(dr["afterPicID"].ToString());

            thelist.Add(obj);
        }

        return thelist;

    }
    #endregion

    #region Validation
    static public bool IsValid(FMTestimonial obj)
    {
        bool isValid = false;

        if (GetErrors(obj).Count == 0)
        {
            isValid = true;
        }

        return isValid;
    }


    static public List<ValidationError> GetErrors(FMTestimonial obj)
    {
        List<ValidationError> errorlist = new List<ValidationError>();
        /*
        //TODO make this dynamic
        if (ValidateUtil.IsTooLong(20, obj.FirstName))
        {
            ValidationError e = new ValidationError();
            e.Field = "First Name";
            e.ErrorDescription = "20 characters maximium are allowed";
            errorlist.Add(e);
        };
         * */


        return errorlist;
    }
    #endregion

    #region OverRides

    #endregion
}

