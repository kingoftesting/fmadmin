using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using FM2015.Helpers;

/// <summary>
/// Summary description for OrderNote
/// </summary>
public class OrderNote
{
	#region Members
    private int orderNoteID;
    private int orderID;
    private DateTime orderNoteDate;
    private string orderNoteBy;
    private string orderNoteText;
    #endregion

    #region Properties
    public int OrderNoteID { get { return orderNoteID; } set { orderNoteID = value; } }
    public int OrderID { get { return orderID; } set { orderID = value; } }
    public DateTime OrderNoteDate { get { return orderNoteDate; } set { orderNoteDate = value; } }
    public string OrderNoteBy { get { return orderNoteBy; } set { orderNoteBy = value; } }
    public string OrderNoteText { get { return orderNoteText; } set { orderNoteText = value; } }
    #endregion

    public OrderNote()
    {
        OrderNoteID = 0;
        OrderID = 0;
        OrderNoteDate = Convert.ToDateTime("1/1/1900");
        OrderNoteBy = "";
        OrderNoteText = "";
    }

    public OrderNote(int IDfield)
    {
        string sql = @" 
        SELECT * FROM  ordernotes 
        WHERE ID = @ID 
        "; 
        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, IDfield);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
        if (dr.Read())
        {
            OrderNoteID = Convert.ToInt32(dr["OrderNoteID"]);
            OrderID = Convert.ToInt32(dr["OrderID"]);
            OrderNoteDate = Convert.ToDateTime(dr["OrderNoteDate"].ToString());
            OrderNoteBy = dr["OrderNoteBy"].ToString();
            OrderNoteText = dr["OrderNote"].ToString();
        }
        if (dr != null) { dr.Close(); }
    }

    public int Save(int ID)
    {
        int result = 0;
        SqlParameter[] mySqlParameters = null;
        SqlDataReader dr = null;
        if (ID == 0)
        {
            string sql = @" 
            INSERT INTO ordernotes 
            (OrderID, OrderNoteDate, OrderNoteBy, OrderNote) 
            VALUES(@OrderID, @OrderNoteDate, @OrderNoteBy, @OrderNoteText); 
            SELECT ID=@@identity;  
            ";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID, OrderNoteDate, OrderNoteBy, OrderNoteText); 
            dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            { result = Convert.ToInt32(dr["ID"]); }
            else { result = 99; }
        }
        else
        {
            string sql = @" 
            UPDATE ordernotes 
            SET 
                OrderID = @OrderID,   
                OrderNoteDate = @OrderNoteDate,  
                OrderNoteBy = @OrderNoteBy,  
                OrderNote = @OrderNoteText  
            WHERE OrderNoteID = @OrderNoteID  
            ";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID, OrderNoteDate, OrderNoteBy, OrderNoteText, OrderNoteID); 
            DBUtil.Exec(sql, mySqlParameters);
        }
        if (dr != null) { dr.Close(); }
        return result;
    }

    /***********************************
    * Static methods
    ************************************/

    /// <summary>
    /// Returns an OrdeNote object with the specified ID
    /// </summary>

    public static OrderNote GetordernoteByID(int id)
    {
        OrderNote obj = new OrderNote(id);
        return obj;
    }

    /// <summary>
    /// Updates an existing ordernote
    /// </summary>
    public static bool Updateordernote(int id, int orderid, string ordernoteby, string ordernotetext)
    {
        OrderNote obj = new OrderNote();

        obj.OrderNoteID = id;
        obj.OrderID = orderid;
        obj.OrderNoteDate = DateTime.Now;
        obj.OrderNoteBy = ordernoteby;
        obj.OrderNoteText = ordernotetext;
        int ret = obj.Save(obj.OrderNoteID);
        return (ret > 0) ? true : false;
    }

    /// <summary>
    /// Creates a new ordernote
    /// </summary>
    public static int Insertordernote(int orderid, string ordernoteby, string ordernotetext)
    {
        OrderNote obj = new OrderNote();

        obj.OrderID = orderid; 
        obj.OrderNoteDate = DateTime.Now;
        obj.OrderNoteBy = ordernoteby;
        obj.OrderNoteText = ordernotetext;
        int ret = obj.Save(obj.OrderNoteID);
        return ret;
    }

    /// <summary>
    /// Returns a collection with all ordernote
    /// </summary>
    static public List<OrderNote> GetOrderNoteList(int orderID)
    {
        string sql = @"
            SELECT * FROM OrderNotes 
            WHERE OrderID = @OrderID 
            ORDER BY OrderNoteDate DESC 
        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

        List<OrderNote> OrderNoteList = new List<OrderNote>();
        while (dr.Read())
        {
            OrderNote tempON = new OrderNote();
            tempON.OrderNoteID = Convert.ToInt32(dr["OrderNoteID"]);
            tempON.OrderID = Convert.ToInt32(dr["OrderID"]);
            tempON.OrderNoteDate = Convert.ToDateTime(dr["OrderNoteDate"].ToString());
            tempON.orderNoteBy = dr["OrderNoteBy"].ToString();
            tempON.OrderNoteText = dr["OrderNote"].ToString();
            OrderNoteList.Add(tempON);
        }
        if (dr != null) { dr.Close(); }
        return OrderNoteList;
    }
    
}
