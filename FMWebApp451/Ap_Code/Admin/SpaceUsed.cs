﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Summary description for SpaceUsed
/// </summary>
public class SpaceUsed
{
    #region Private/Public Vars
    private int id;
    public int ID { get { return id; } set { id = value; } }
    private string database_name;
    public string Database_name { get { return database_name; } set { database_name = value; } }
    private string database_size;
    public string Database_size { get { return database_size; } set { database_size = value; } }
    private string unallocatedspace;
    public string Unallocatedspace { get { return unallocatedspace; } set { unallocatedspace = value; } }
    private string name;
    public string Name { get { return name; } set { name = value; } }
    private string rows;
    public string Rows { get { return rows; } set { rows = value; } }
    private string reserved;
    public string Reserved { get { return reserved; } set { reserved = value; } }
    private string data;
    public string Data { get { return data; } set { data = value; } }
    private string index_size;
    public string Index_size { get { return index_size; } set { index_size = value; } }
    private string unused;
    public string Unused { get { return unused; } set { unused = value; } }
    #endregion

    public SpaceUsed()
    {
        Database_name = "n/a";
        Database_size = "n/a";
        Unallocatedspace = "n/a";
        Name = "n/a";
        Rows = "n/a";
        Reserved = "n/a";
        Data = "n/a";
        Index_size = "n/a";
        Unused = "n/a";
    }

    public SpaceUsed(string tableName)
    {
        IDataReader dr = null;
        using (SqlConnection cn = new SqlConnection(AdminCart.Config.ConnStr()))
        {
            SqlCommand cmd = new SqlCommand("sp_spaceused", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@objname", SqlDbType.NVarChar).Value = tableName;
            //"@objname", SqlDbType.NVarChar, 776, "mytable "
            cn.Open();
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.Read())
            {
                Database_name = "n/a";
                Database_size = "n/a";
                Unallocatedspace = "n/a";
                Name = dr["Name"].ToString();
                Rows = dr["Rows"].ToString();
                Reserved = dr["Reserved"].ToString();
                Data = dr["Data"].ToString();
                Index_size = dr["Index_size"].ToString();
                Unused = dr["Unused"].ToString();
            }
            if (cn != null) { cn.Close(); }
            if (dr != null) { dr.Close(); }
        }
    }


    /***********************************
    * Static methods
    ************************************/

    /// <summary>
    /// Returns a collection with all SpaceUsed
    /// </summary>
    public static List<SpaceUsed> GetSpaceUsedList()
    {
        List<SpaceUsed> thelist = new List<SpaceUsed>();

        string sql = @" 
SELECT * FROM SpaceUsed 
";

        SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql);

        while (dr.Read())
        {
            SpaceUsed obj = new SpaceUsed();

            obj.ID = Convert.ToInt32(dr["ID"]);
            obj.Database_name = dr["Database_name"].ToString();
            obj.Database_size = dr["Database_size"].ToString();
            obj.Unallocatedspace = dr["Unallocatedspace"].ToString();
            obj.Name = dr["Name"].ToString();
            obj.Rows = dr["Rows"].ToString();
            obj.Reserved = dr["Reserved"].ToString();
            obj.Data = dr["Data"].ToString();
            obj.Index_size = dr["Index_size"].ToString();
            obj.Unused = dr["Unused"].ToString();

            thelist.Add(obj);
        }
        if (dr != null) { dr.Close(); }
        return thelist;
    }

    /// <summary>
    /// Returns a className object with the specified ID
    /// </summary>

    public static SpaceUsed GetSpaceUsedByTable(string tableName)
    {
        SpaceUsed obj = new SpaceUsed(tableName);
        return obj;
    }

    /// <summary>
    /// Returns a className object with the specified ID
    /// </summary>

    public static SpaceUsed GetSpaceUsedByDB()
    {
        SpaceUsed obj = new SpaceUsed();
        return obj;
    }
}
