using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using Certnet;
using FM2015.Helpers;

/// <summary>
/// Summary description for Biz
/// </summary>
public class Biz
{
    public Biz()
    {
        
    }

    static public List<Product> GetProductList()
    {

        return Product.GetProducts(true);
    }

    static public List<Product> GetProductList(bool GetAllProducts)
    {
        List<Product> productlist = new List<Product>();
        if (GetAllProducts)
        {
            productlist = Product.GetProducts(false);
        }
        else
        {
            productlist = Product.GetProducts(true);
        }

        return productlist;
    }

    static public List<AdminCart.Product> GetAdminProductList(bool GetAllProducts)
    {
        List<AdminCart.Product> productlist = new List<AdminCart.Product>();
        if (GetAllProducts)
        {
            productlist = AdminCart.Product.GetProducts(false, false);
        }
        else
        {
            productlist = AdminCart.Product.GetProducts(true, false);
        }

        return productlist;
    }

    static public string GetPageText(int PageID)
    {
        string thetext = "";
        string sql = @"
        SELECT * 
        FROM Paragraphs 
        WHERE PageID = @PageID 
            AND ParagraphVersion = (select max(paragraphversion) from paragraphs where pageid = @PageID2) 
        ORDER BY PagePosition ASC 
        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, PageID, PageID);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters); 
        while (dr.Read())
        {
            if (dr["PagePosition"].ToString() != "5")
            {
                thetext += dr["paragraphtext"].ToString().Replace("#email#", AdminCart.Config.CustomerServiceEmail().Replace("FaceMaster ", "FaceMaster® "));
            }
            else
            {
                thetext += dr["paragraphtext"].ToString();
            }

        }
        if (dr != null) { dr.Close(); }
        return thetext;
    }

    static public string GetPageText(int PageID, int PagePosition)
    {
        string thetext = "";


        string sql = @"
        SELECT * 
        FROM Paragraphs 
        WHERE PageID = @PageID 
            AND ParagraphVersion = (select max(paragraphversion) from paragraphs where pageid = @PageID2) 
            AND PagePosition = @PagePosition 
        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, PageID, PageID, PagePosition);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
        while (dr.Read())
        {
            if (dr["PagePosition"].ToString() == "5")
            {
                thetext += dr["paragraphtext"].ToString().Replace("#email#", AdminCart.Config.CustomerServiceEmail()).Replace("FaceMaster ", "FaceMaster® ");
            }
            else
            {
                thetext += dr["paragraphtext"].ToString();
            }
        }
        if (dr != null) { dr.Close(); }

        return thetext;
    }

}
