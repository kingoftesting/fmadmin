using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using FM2015.Models.mvcCER;
using FM2015.Helpers;
using Newtonsoft.Json;
using System.Linq;
using System.Text.RegularExpressions;

namespace adminCER
{

    public enum complaintStat
    {
        All, Complaint, NonComplaint
    }

    public enum openStat
    {
        All, OpenOnly, ClosedOnly, UnOpened
    }

    public enum medStat
    {
        All, nonMedical, Medical
    }

    public class AJAXFormParams
    {
        [JsonProperty("cerNumber")]
        public string CERNumber { get; set; }

        [JsonProperty("year")]
        public string Year { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("symptom")]
        public int symptomVal { get; set; }

        [JsonProperty("defect")]
        public int defectVal { get; set; }

        [JsonProperty("defectCat")]
        public int defectCatVal { get; set; }

        [JsonProperty("defectLocation")]
        public string defectLocation { get; set; }

        [JsonProperty("serialnumber")]
        public string serialNumber { get; set; }

        [JsonProperty("ordernumber")]
        public string orderNumber { get; set; }
    }


    /// <summary>
    /// Summary description for CER
    /// </summary>
    public class CER
    {
        //static private string CERConnString = AdminCart.Config.ConnStrCER();     

        private string cERYear;
        private int cERIdentifier;
        private string cERSource;
        private DateTime dateReportReceived;
        private string cERType;
        private string receivedBy;
        private int complaintNumber;
        private string complaintDeterminationBy;
        private string customerID;
        private string orderNumber;
        private string pointOfPurchase;
        private string firstName;
        private string lastName;
        private string street;
        private string city;
        private string state;
        private string zip;
        private string country;
        private string email;
        private string phone;
        private string productNumber;
        private string serialNumber;
        private string eventTiming;
        private string eventDescription;
        private string actionExplaination;
        private string statusCs;
        private string statusQa;
        private string statusMed;
        private string medicalCondition;
        private DateTime dateModified;
        private string investigationSummary;
        private bool managerApproved;
        private string insertBy;
        private bool mdr;
        private bool analyze;

        public string CERNumber { get { return CERYear + "000000".Substring(CERIdentifier.ToString().Length) + CERIdentifier.ToString(); } }

        public string CERYear { get { return cERYear; } set { cERYear = value; } }
        public int CERIdentifier { get { return cERIdentifier; } set { cERIdentifier = value; } }
        public string CERSource { get { return cERSource; } set { cERSource = value; } }
        public DateTime DateReportReceived { get { return dateReportReceived; } set { dateReportReceived = value; } }
        public string CERType { get { return cERType; } set { cERType = value; } }
        public string ReceivedBy { get { return receivedBy; } set { receivedBy = value; } }
        public int ComplaintNumber { get { return complaintNumber; } set { complaintNumber = value; } }
        public string ComplaintDeterminationBy { get { return complaintDeterminationBy; } set { complaintDeterminationBy = value; } }
        public string CustomerID { get { return customerID; } set { customerID = value; } }
        public string OrderNumber { get { return orderNumber; } set { orderNumber = value; } }
        public string PointOfPurchase { get { return pointOfPurchase; } set { pointOfPurchase = value; } }
        public string FirstName { get { return firstName; } set { firstName = value; } }
        public string LastName { get { return lastName; } set { lastName = value; } }
        public string Street { get { return street; } set { street = value; } }
        public string City { get { return city; } set { city = value; } }
        public string State { get { return state; } set { state = value; } }
        public string Zip { get { return zip; } set { zip = value; } }
        public string Email { get { return email; } set { email = value; } }
        public string Country { get { return country; } set { country = value; } }
        public string Phone { get { return phone; } set { phone = value; } }
        public string ProductNumber { get { return productNumber; } set { productNumber = value; } }
        public string SerialNumber { get { return serialNumber; } set { serialNumber = value; } }
        public string EventTiming { get { return eventTiming; } set { eventTiming = value; } }
        public string EventDescription { get { return eventDescription; } set { eventDescription = value; } }
        public string ActionExplaination { get { return actionExplaination; } set { actionExplaination = value; } }
        public string StatusCs { get { return statusCs; } set { statusCs = value; } }
        public string StatusQa { get { return statusQa; } set { statusQa = value; } }
        public string StatusMed { get { return statusMed; } set { statusMed = value; } }
        public string MedicalCondition { get { return medicalCondition; } set { medicalCondition = value; } }
        public DateTime DateModified { get { return dateModified; } set { dateModified = value; } }
        public string InvestigationSummary { get { return investigationSummary; } set { investigationSummary = value; } }
        public bool ManagerApproved { get { return managerApproved; } set { managerApproved = value; } }
        public string InsertBy { get { return insertBy; } set { insertBy = value; } }
        public bool Mdr { get { return mdr; } set { mdr = value; } }
        public bool Analyze { get { return analyze; } set { analyze = value; } }

        public CER()
        {
            cERYear = "2007";
            cERIdentifier = 1;
            cERSource = ""; //FM Email, Zanco Order, CER System

            dateReportReceived = DateTime.Now;
            cERType = "";
            receivedBy = "";
            complaintNumber = 0;
            complaintDeterminationBy = "";
            customerID = "0";
            orderNumber = "";
            pointOfPurchase = "";
            firstName = "";
            lastName = "";
            street = "";
            city = "";
            state = "";
            zip = "";
            country = "";
            email = "";
            phone = "";
            productNumber = "";
            serialNumber = "";
            eventTiming = "";
            eventDescription = "";
            actionExplaination = "";
            statusCs = "";
            statusQa = "";
            statusMed = "";
            medicalCondition = "";
            dateModified = DateTime.Now;
            investigationSummary = "N/A";
            insertBy = "";
            managerApproved = false;
            mdr = false;
            analyze = false;
        }

        public CER(string CERNumber)
        {
            //parse CER into two parts: Year and CerID
            string _CERYear = CERNumber.Substring(0, 4);
            string _CERIdentifier = CERNumber.Substring(4);

            string sql = @"
                        SELECT *, PoP = (select PopName from CERPointofPurchase where PopValue = PointofPurchase) 
                        FROM CER2s 
                        WHERE CERYear = @CERYear 
                        AND CERIdentifier = @CERIdentifier ";

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, _CERYear, Convert.ToInt32(_CERIdentifier));
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, mySqlParameters, AdminCart.Config.ConnStrCER());

            if (dr.Read())
            {
                cERYear = _CERYear;
                pointOfPurchase = dr["PointOfPurchase"].ToString();
                cERIdentifier = Convert.ToInt32(_CERIdentifier);
                cERSource = dr["CERSource"].ToString();
                DateReportReceived = DateTime.Parse(dr["DateReportReceived"].ToString());
                cERType = dr["CerType"].ToString().Trim();
                receivedBy = dr["ReceivedBy"].ToString();
                complaintNumber = Convert.ToInt32(dr["ComplaintNumber"]);
                complaintDeterminationBy = dr["ComplaintDeterminationBy"].ToString();
                customerID = dr["CustomerID"].ToString();
                pointOfPurchase = dr["PointOfPurchase"].ToString();

                firstName = dr["FirstName"].ToString();
                lastName = dr["Lastname"].ToString();
                street = dr["Street"].ToString();
                city = dr["City"].ToString();
                state = dr["State"].ToString();
                zip = dr["Zip"].ToString();
                country = dr["country"].ToString();
                email = dr["Email"].ToString();
                phone = dr["Phone"].ToString();
                productNumber = dr["ProductNumber"].ToString();
                serialNumber = dr["SerialNumber"].ToString();
                orderNumber = dr["OrderNumber"].ToString();
                eventTiming = dr["EventTiming"].ToString();
                eventDescription = dr["EventDescription"].ToString();
                actionExplaination = dr["ActionExplaination"].ToString();
                statusCs = dr["StatusCs"].ToString();
                statusQa = dr["StatusQa"].ToString();
                statusMed = dr["StatusMed"].ToString();
                medicalCondition = dr["MedicalCondition"].ToString();
                dateModified = DateTime.Parse(dr["DateModified"].ToString());
                investigationSummary = dr["InvestigationSummary"].ToString();
                managerApproved = bool.Parse(dr["ManagerApproved"].ToString());
                //insertBy = dr["InsertBy"].ToString();
                mdr = bool.Parse(dr["Mdr"].ToString());
                analyze = bool.Parse(dr["Analyze"].ToString());
            }
            else
            {
                DateReportReceived = DateTime.Parse("1/1/1999");
            }
            dr.Close();

        }

        public CER(AdminCart.Cart cart)
        {
            cERYear = "2007";
            cERIdentifier = 1;
            cERSource = "Admin/CER"; //FM Email, Zanco Order, CER System

            dateReportReceived = DateTime.Now;
            cERType = "NC";

            receivedBy = FM2015.Models.User.GetUserName();
            receivedBy = (string.IsNullOrEmpty(receivedBy)) ? "Automated Admin" : receivedBy;
            receivedBy = (receivedBy == " ") ? "Automated Admin via API" : receivedBy;
            insertBy = receivedBy;

            complaintNumber = 0;
            complaintDeterminationBy = "";
            customerID = cart.SiteCustomer.CustomerID.ToString();
            orderNumber = cart.OrderID.ToString();
            pointOfPurchase = "01"; //FaceMaster.Com;
            firstName = cart.SiteCustomer.FirstName;
            lastName = cart.SiteCustomer.LastName;
            street = cart.BillAddress.Street;
            city = cart.BillAddress.City;
            state = cart.BillAddress.State;
            zip = cart.BillAddress.Zip;
            country = cart.BillAddress.Country;
            email = cart.SiteCustomer.Email;
            phone = cart.SiteCustomer.Phone;
            productNumber = "";
            serialNumber = "";
            if (cart.SiteOrder.scans.Count > 0) //check for a serial number...
            {
                serialNumber = (from scan in cart.SiteOrder.scans select scan.Serial).Last().ToString();
            }            
            eventTiming = "";

            eventDescription += "Order ID: " + orderNumber + Environment.NewLine;
            eventDescription += "Products Purchased:" + Environment.NewLine;
            foreach (AdminCart.Item i in cart.Items)
            {
                eventDescription += "qty=" + i.Quantity + " " + i.LineItemProduct.Name + ", productID=" + i.LineItemProduct.ProductID.ToString() +
                    ", price=" + i.LineItemProduct.Price.ToString("C") + ", discount= " + i.LineItemDiscount.ToString("C") + ", cost= " + i.LineItemProduct.Cost.ToString("C");
            }

            actionExplaination = "Product Purchase";
            statusCs = "Opened";
            statusQa = "Unopened";
            statusMed = "Does Not Apply";
            medicalCondition = "n/a";
            dateModified = DateTime.Now;
            investigationSummary = "";
            insertBy = "";
            managerApproved = false;
            mdr = false;
            analyze = false;
        }

        public CER(AdminCart.Customer customer)
        {
            cERYear = "2007";
            cERIdentifier = 1;
            cERSource = "Admin/CER"; //FM Email, Zanco Order, CER System

            dateReportReceived = DateTime.Now;
            cERType = "NC";

            receivedBy = FM2015.Models.User.GetUserName();
            receivedBy = (string.IsNullOrEmpty(receivedBy)) ? "Automated Admin" : receivedBy;
            receivedBy = (receivedBy == " ") ? "Automated Admin via API" : receivedBy;
            insertBy = receivedBy;

            complaintNumber = 0;
            complaintDeterminationBy = "";
            customerID = customer.CustomerID.ToString();
            orderNumber = "0";
            pointOfPurchase = "01"; //FaceMaster.Com;
            firstName = customer.FirstName;
            lastName = customer.LastName;

            AdminCart.Address a = AdminCart.Address.LoadAddress(customer.CustomerID, 1);
            street = a.Street;
            city = a.City;
            state = a.State;
            zip = a.Zip;
            country = a.Country;
            email = customer.Email;
            phone = customer.Phone;
            productNumber = "";
            serialNumber = "";
            eventTiming = "";

            eventDescription = "Customer Service Administrative Action" + Environment.NewLine;


            actionExplaination = "Customer Service Administrative Action";
            statusCs = "Opened";
            statusQa = "Unopened";
            statusMed = "Does Not Apply";
            medicalCondition = "n/a";
            dateModified = DateTime.Now;
            investigationSummary = "";
            insertBy = "";
            managerApproved = false;
            mdr = false;
            analyze = false;
        }

        public int Add()
        {
            FM2015.Helpers.CacheHelper.Clear(AdminCart.Config.cachekey_CERList);
            string CERConnString = GetConnStr();

            if (ConfigurationManager.AppSettings["AllowCERs"].ToString().ToUpperInvariant() == "YES") //don't add a new CER entry if in Test mode
            {
                string sql = @"
            INSERT CER2s( 
                CERSource, DateReportReceived, CerType, ReceivedBy, 
                CustomerID, OrderNumber, PointOfPurchase, 
                FirstName, LastName, Street, City, State, Zip, Country, Email, Phone, 
                ProductNumber, SerialNumber, EventTiming, EventDescription, ActionExplaination,  
                StatusCs, StatusQa, StatusMed, MedicalCondition, 
                ComplaintNumber, ComplaintDeterminationBy,
                DateModified, InvestigationSummary, 
                ManagerApproved, Mdr, Analyze, 
                CERYear, CERIdentifier  
            ) 
            VALUES(
                @CERSource, @DateReportReceived, @CERType, @ReceivedBy, 
                @CustomerId, @OrderNumber, @PointOfPurchase, 
                @FirstName, @LastName, @Street, @City, @State, @Zip, @Country, @Email, @Phone,  
                @ProductNumber, @SerialNumber, @EventTiming, @EventDescription, @ActionExplaination, 
                @StatusCs, @StatusQa, @StatusMed, @MedicalCondition, 
                @ComplaintNumber, @ComplaintDeterminationBy,  
                getDate(), @InvestigationSummary, 
                0, @Mdr, @Analyze, 
                @CERYear, @CERIdentifier 
            )";


                int max = DBUtil.GetScalar("select max(ceridentifier) from cer2s where CERYear = '" + DateTime.Now.Year.ToString() + "'", CERConnString, true);
                max++;

                if (orderNumber == "0") { orderNumber = string.Empty; }

                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql,
                    cERSource, dateReportReceived, cERType, receivedBy,
                    customerID, orderNumber, pointOfPurchase,
                    firstName, lastName, street, city, state, zip, country, email, phone,
                    productNumber, serialNumber, eventTiming, eventDescription, actionExplaination,
                    statusCs, statusQa, statusMed, medicalCondition,
                    complaintNumber, complaintDeterminationBy, investigationSummary,
                    mdr, analyze,
                    DateTime.Now.Year.ToString(), max);

                SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);
                if (dr != null) { dr.Close(); }

                //insert into history table
                sql = @"
            INSERT CER2History( 
                CERSource, DateReportReceived, 
                CerType, CustomerID, ReceivedBy, PointOfPurchase, 
                FirstName, LastName, Street, City, State, Zip, Country, Email, Phone, 
                ProductNumber, SerialNumber, OrderNumber, EventTiming, EventDescription, ActionExplaination,  
                StatusCs, StatusQa, StatusMed, MedicalCondition, 
                ComplaintNumber, ComplaintDeterminationBy,
                InsertDate, InsertBy, Mdr, Analyze, 
                CERYear, CERIdentifier  
            )  
            VALUES( 
                @CERSource, @DateReportReceived, 
                @CERType, @CustomerID, @ReceivedBy, @PointOfPurchase, 
                @FirstName, @LastName, @Street, @City, @State, @Zip, @Country, @Email, @Phone,  
                @ProductNumber, @SerialNumber, @OrderNumber, @EventTiming, @EventDescription, @ActionExplaination,  
                @StatusCs, @StatusQa, @StatusMed, @MedicalCondition, 
                @ComplaintNumber, @ComplaintDeterminationBy, 
                getDate(), @InsertBy, @MDR, @Analyze, 
                @CERYear, @CERIdentifier   
            ) 
        ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql,
                    cERSource, dateReportReceived,
                    cERType, customerID, receivedBy, pointOfPurchase,
                    firstName, lastName, street, city, state, zip, country, email, phone,
                    productNumber, serialNumber, orderNumber, eventTiming, eventDescription, actionExplaination,
                    statusCs, statusQa, statusMed, medicalCondition,
                    complaintNumber, complaintDeterminationBy,
                    insertBy, mdr, analyze,
                    DateTime.Now.Year.ToString(), max);
                dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);
                if (dr != null) { dr.Close(); }
                //HttpContext.Current.Session["CERList"] = null;
                return max;
            }
            else
            {
                return 0;
            }

        }

        public int Update()
        {
            FM2015.Helpers.CacheHelper.Clear(AdminCart.Config.cachekey_CERList);
            //SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrCER());
            string CERConnString = GetConnStr();
            string connStr = CERConnString;
            SqlConnection conn = new SqlConnection(connStr);

            string sql = @"
            UPDATE CER2s SET 
                CERSource = @CERSource, 
                DateReportReceived = @DateReportReceived, 
                CerType = @CerType, 
                CustomerID = @CustomerID, 
                ReceivedBy = @ReceivedBy, 
                PointOfPurchase = @PointOfPurchase, 
                FirstName = @FirstName, 
                LastName = @LastName, 
                Street = @Street, 
                City = @City, 
                State = @State, 
                Zip = @Zip, 
                Country = @Country, 
                Email = @Email, 
                Phone = @Phone, 
                ProductNumber = @ProductNumber, 
                SerialNumber = @SerialNumber,  
                OrderNumber = @OrderNumber, 
                EventTiming = @EventTiming, 
                EventDescription = @EventDescription, 
                ActionExplaination = @ActionExplaination,  
                StatusCs = @StatusCs, 
                StatusQa = @StatusQa, 
                StatusMed = @StatusMed, 
                MedicalCondition = @MedicalCondition, 
                ComplaintNumber = @ComplaintNumber, 
                ComplaintDeterminationBy = @ComplaintDeterminationBy, 
                InvestigationSummary = @InvestigationSummary, 
                DateModified = getDate(), 
                Mdr = @Mdr, 
                Analyze = @Analyze  
            WHERE CERYear = @CERYear AND CERIdentifier = @CERIdentifier 
        
        ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, 
                cERSource, dateReportReceived, 
                cERType, customerID, receivedBy, pointOfPurchase, 
                firstName, lastName, street, city, state, zip, country, email, phone, 
                productNumber, serialNumber, orderNumber, eventTiming, eventDescription, actionExplaination,
                statusCs, statusQa, statusMed, medicalCondition, 
                complaintNumber, complaintDeterminationBy, investigationSummary,
                mdr, Analyze, 
                cERYear, cERIdentifier);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);
            if (dr != null) { dr.Close(); }


            //insert into history table
            insertBy = FM2015.Models.User.GetUserName();

            sql = @"
            INSERT CER2History( 
                CERSource, DateReportReceived, 
                CerType, CustomerID, ReceivedBy, PointOfPurchase, 
                FirstName, LastName, Street, City, State, Zip, Country, Email, Phone, 
                ProductNumber, SerialNumber, OrderNumber, EventTiming, EventDescription, ActionExplaination,  
                StatusCs, StatusQa, StatusMed, MedicalCondition, 
                ComplaintNumber, ComplaintDeterminationBy,
                InsertDate, InsertBy, Mdr, Analyze, 
                CERYear, CERIdentifier  
            )  
            VALUES( 
                @CERSource, @DateReportReceived, 
                @CERType, @CustomerID, @ReceivedBy, @PointOfPurchase, 
                @FirstName, @LastName, @Street, @City, @State, @Zip, @Country, @Email, @Phone,  
                @ProductNumber, @SerialNumber, @OrderNumber, @EventTiming, @EventDescription, @ActionExplaination,  
                @StatusCs, @StatusQa, @StatusMed, @MedicalCondition, 
                @ComplaintNumber, @ComplaintDeterminationBy, 
                getDate(), @InsertBy, @MDR, @Analyze, 
                @CERYear, @CERIdentifier  
            ) 
        ";
            mySqlParameters = DBUtil.BuildParametersFrom(sql,
                cERSource, dateReportReceived,
                cERType, customerID, receivedBy, pointOfPurchase,
                firstName, lastName, street, city, state, zip, country, email, phone,
                productNumber, serialNumber, orderNumber, eventTiming, eventDescription, actionExplaination,
                statusCs, statusQa, statusMed, medicalCondition,
                complaintNumber, complaintDeterminationBy,
                insertBy, mdr, Analyze, 
                cERYear, cERIdentifier); 
            dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);
            if (dr != null) { dr.Close(); }
            //HttpContext.Current.Session["CERList"] = null;

            return 0;
        }

        static public List<CER> GetRelatedCERs(string _CERYear, int _CERIdentifier)
        {
            //SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrCER());
            string CERConnString = GetConnStr();
            string connStr = CERConnString;
            SqlConnection conn = new SqlConnection(GetConnStr());

            string cersql = @"        
                SELECT * 
                FROM RelatedCERs 
                WHERE CERYear = dbo.fn_GetParentCERyear (@CERYear, @CERIdentifier) 
                AND CERIdentifier = dbo.fn_GetParentCERIdentifier (@CERYear1, @CERIdentifier1)  
            "; 

            List<CER> cerList = new List<CER>();

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(cersql, _CERYear, _CERIdentifier, _CERYear, _CERIdentifier);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(cersql, mySqlParameters, AdminCart.Config.ConnStrCER());
            while (dr.Read())
            {
                CER c = null;
                if (_CERYear == dr["RelatedCERYear"].ToString() && _CERIdentifier == Convert.ToInt32(dr["RelatedCERIdentifier"]))
                    c = new CER(dr["CERYear"].ToString() + dr["CERIdentifier"].ToString());
                else
                    c = new CER(dr["RelatedCERYear"].ToString() + dr["RelatedCERIdentifier"].ToString());
                cerList.Add(c);
            }

            if (dr != null) dr.Close();
            return cerList;
        }

        static public List<CER> CERFAQs(string keyword, string resultNum)
        {
            int count = 0;
            if (!int.TryParse(resultNum, out count))
            {
                count = 10; //default
            }
            
            List<CER> cerList = GetCERList();
            cerList = (from cer in cerList
                       where (cer.EventDescription.ToUpper().Contains(keyword.ToUpper()))
                       select cer ).Take(count).ToList();
            
            return cerList;

        }

        public void SaveNote(string note)
        {
            //SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrCER());
            string CERConnString = GetConnStr();

            string sql = @"
            INSERT CERNotes 
            ( 
                CERIdentifier, CERYear, Note, InsertDate, InsertBy 
            ) 
            VALUES 
            (
                @CERIdentifier, @CERYear, @Note, getdate(), '" + System.Web.HttpContext.Current.Session["UserName"].ToString() + @"' 
            ) 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, cERYear, cERIdentifier);
            DBUtil.Exec(sql, CERConnString, mySqlParameters);
        }

        public List<string> GetNoteList()
        {
            //SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrCER());
            string CERConnString = GetConnStr();

            List<string> noteStr = new List<string>();
            string sql = @"
                SELECT Note 
                WHERE CERIdentifier = @CERIdentifier, CERYear = @CERYear 
            ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, cERYear, cERIdentifier);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);

            while (dr.Read())
            {
                string note = dr["Note"].ToString();
                noteStr.Add(note);
            }
            if (dr != null) { dr.Close(); }
            return noteStr;
        }

        public void DefaultStatusQA()
        {
            //SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrCER());
            string CERConnString = GetConnStr();

            string sql = @"
            UPDATE CER2s 
                SET StatusQa = 'Opened' 
                WHERE CERYear = @CERYear 
                AND CERIdentifier = @CERIdentifier;  
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, cERYear, cERIdentifier);
            DBUtil.Exec(sql, CERConnString, mySqlParameters);
            HttpContext.Current.Session["CERList"] = null;
        }

        public static DataTable GetCERTable(bool complaints, medStat medical, openStat stat, DateTime startDate, DateTime endDate)
        {
            //SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrCER());
            string CERConnString = GetConnStr();

            string whereClause = @"
                WHERE  CERType LIKE @CERType AND StatusMed LIKE @StatusMed AND DateReportReceived BETWEEN @startDate and @endDate 
            ";

            if (stat == openStat.All) { } //do nothing
            if (stat == openStat.OpenOnly) { whereClause += " AND StatusQA <> 'Closed'"; }
            if (stat == openStat.ClosedOnly) { whereClause += " AND StatusQA = 'Closed'"; }

            whereClause += " ORDER BY ceryear desc, ceridentifier desc"; 

            string sql = @" 
            SELECT CERNumber= ceryear + '-' + cast(ceridentifier as varchar(10)) 
               ,CERNumberSearchable=ceryear + left('000000', 6-len(ceridentifier) ) + cast(ceridentifier as varchar(10)) 
               ,CERNumberSearchableDash=ceryear + '-' + left('000000', 6-len(ceridentifier) ) + cast(ceridentifier as varchar(10)) 
               ,[DateReportReceived] 
               ,CASE WHEN [CERType] = 'C' THEN 'Complaint' ELSE 'No Complaint' END CERTypes 
               ,[ReceivedBy] 
               ,[ComplaintDeterminationBy] 
               ,[FirstName] 
               ,[LastName] 
               ,[Email] 
               ,[Phone] 
               ,[SerialNumber]      
               ,[StatusQa] 
               ,[EventDescription]         
            FROM Cer2s 
            ";

            sql += whereClause;

            String CERType = "";
            if (complaints) { CERType = "C%"; }
            else { CERType = "NC%"; }

            string StatusMed = "";
            if ((medical == medStat.Medical) && (stat == openStat.ClosedOnly)) { StatusMed = "Closed%"; }
            else if ((medical == medStat.Medical) && (stat == openStat.OpenOnly)) { StatusMed = "Assigned%"; }
            else if ((medical == medStat.Medical) && (stat == openStat.All)) { StatusMed = "%ed%"; } //gets both 'closed' & 'assigned'... just a bit of a kluge...
            else if (medical == medStat.nonMedical) { StatusMed = "Does Not Apply%"; }
            else if (medical == medStat.All) { StatusMed = "%"; }
            else { StatusMed = "%"; }


            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, CERType, StatusMed, startDate, endDate);
            DataTable result = FM2015.Helpers.DBUtil.FillDataSet(sql, "cer2s", mySqlParameters, CERConnString).Tables[0];
            return result;
        }

        public static List<CER> GetCERList(complaintStat complaints, medStat medical, openStat stat, DateTime startDate, DateTime endDate)
        {
            string CERConnString = GetConnStr();

            string whereClause = @"
                WHERE  CERType LIKE @CERType AND StatusMed LIKE @StatusMed AND DateReportReceived BETWEEN @startDate and @endDate 
            ";

            if (stat == openStat.All) { } //do nothing
            else if (stat == openStat.OpenOnly) { whereClause += " AND StatusQA = 'Opened'"; }
            else if (stat == openStat.ClosedOnly) { whereClause += " AND StatusQA = 'Closed'"; }
            else if (stat == openStat.UnOpened) { whereClause += " AND StatusQA = 'UnOpened'"; }

            whereClause += " ORDER BY ceryear desc, ceridentifier desc";

            string sql = @" 
            SELECT top 1000 CERYear, CERIdentifier, [DateReportReceived] 
               ,CASE WHEN [CERType] = 'C' THEN 'Complaint' ELSE 'No Complaint' END CERType 
               ,[ReceivedBy] 
               ,[ComplaintDeterminationBy] 
               ,[FirstName] 
               ,[LastName] 
               ,[Email] 
               ,[Phone] 
               ,[SerialNumber]      
               ,[StatusQa] 
               ,[EventDescription]         
            FROM Cer2s 
            ";

            sql += whereClause;

            String CERType = "";
            if (complaints == complaintStat.All) { CERType = "%"; }
            else if (complaints == complaintStat.Complaint) { CERType = "C%"; }
            else if (complaints == complaintStat.NonComplaint) { CERType = "NC%"; }

            string StatusMed = "";
            if ((medical == medStat.Medical) && (stat == openStat.ClosedOnly)) { StatusMed = "Closed%"; }
            else if ((medical == medStat.Medical) && (stat == openStat.OpenOnly)) { StatusMed = "Assigned%"; }
            else if ((medical == medStat.Medical) && (stat == openStat.All)) { StatusMed = "%ed%"; } //gets both 'closed' & 'assigned'... just a bit of a kluge...
            else if (medical == medStat.nonMedical) { StatusMed = "Does Not Apply%"; }
            else if (medical == medStat.All) { StatusMed = "%"; }
            else { StatusMed = "%"; }


            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, CERType, StatusMed, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);

            List<CER> resultList = new List<CER>();
            while (dr.HasRows && dr.Read())
            {
                CER cer = new CER();
                cer.cERYear = dr["CERYear"].ToString();
                cer.cERIdentifier = Convert.ToInt32(dr["CERIdentifier"]);
                cer.DateReportReceived = DateTime.Parse(dr["DateReportReceived"].ToString());
                cer.cERType = dr["CERType"].ToString();
                cer.receivedBy = dr["ReceivedBy"].ToString();
                cer.complaintDeterminationBy = dr["ComplaintDeterminationBy"].ToString();

                cer.firstName = dr["FirstName"].ToString();
                cer.lastName = dr["Lastname"].ToString();
                cer.email = dr["Email"].ToString();
                cer.phone = dr["Phone"].ToString();
                cer.serialNumber = dr["SerialNumber"].ToString();
                cer.statusQa = dr["StatusQa"].ToString();
                cer.eventDescription = dr["EventDescription"].ToString();

                resultList.Add(cer);
            }
            if (dr != null) { dr.Close(); }

            return resultList;
        }

        public static List<CER> FindCERList(string cerIdentifier, string cerYear, string firstName, string lastName, string eMail, string serialNum, string orderNum)
        {
            string CERConnString = GetConnStr();

            List<CER> cerList = new List<CER>();
            string sql = @"
                        SELECT TOP 1000 CERNumberSearchable=ceryear + left('000000', 6-len(ceridentifier) ) + cast(ceridentifier as varchar(10)) 
                        from Cer2s 
                        WHERE  
                            (CERIdentifier LIKE @CERIdentifier) 
                            AND (CERYear LIKE @CERYear) 
                            AND (FirstName LIKE @FirstName) 
                            AND (LastName LIKE @LastName) 
                            AND (Email LIKE @Email) 
                            AND (SerialNumber LIKE @SerialNumber) 
                            AND (OrderNumber LIKE @OrderNumber) 
                        ORDER BY CERYear Desc, CERIDentifier Desc 
                        ";

            string cerIdentifierParam = "%";
            string cerYearParam = "%";
            string firstNameParam = "%";
            string lastNameParam = "%";
            string emailParam = "%";
            string serialNumberParam = "%";
            string orderNumberParam = "%";

            if (cerIdentifier != "") { cerIdentifierParam = cerIdentifier; }
            if (cerYear != "") { cerYearParam = cerYear; }
            if (firstName != "") { firstNameParam = "%" + firstName + "%"; }
            if (lastName != "") { lastNameParam = "%" + lastName + "%"; }
            if (eMail != "") { emailParam = "%" + eMail + "%"; }
            if (serialNum != "") { serialNumberParam = "%" + serialNum + "%"; }
            if (orderNum != "") { orderNumberParam = "%" + orderNum + "%"; }

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, cerIdentifierParam, cerYearParam, firstNameParam, lastNameParam, emailParam, serialNumberParam, orderNumberParam);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);

            while (dr.Read())
            {
                string cerNum = dr["CERNumberSearchable"].ToString();
                CER cer = new CER(cerNum);
                cerList.Add(cer);
            }
            if (dr != null) { dr.Close(); }

            return cerList;
        }

        public static List<CER> GetCERAnalyzeList()
        {
            List<CER> cerList = GetCERList();
            cerList = ( from cer in cerList
                        where (cer.Analyze = true && cer.CERType == "C" && cer.StatusQa.ToUpper() == "OPENED")
                        select cer
                        ).ToList();
            return cerList;
        }

        public static List<CER> GetOpenComplaints()
        {
            List<CER> cerList = GetCERList();
            cerList = (from cer in cerList
                       where (cer.CERType == "C" && cer.StatusQa.ToUpper() == "OPENED")
                       select cer
                        ).ToList();
            return cerList;
        }

        public static List<CER> GetCERList()
        {
            string CERConnString = GetConnStr();

            List<CER> cerList = new List<CER>();
            if (!CacheHelper.Get(AdminCart.Config.cachekey_CERList, out cerList))
            {
                cerList = new List<CER>(); //recover from cerList being nulled by CacheHelper
                string sql = @"
                        SELECT *                               
                        from Cer2s 
                        ORDER BY ceryear DESC, ceridentifier DESC  
                        ";
                //PoP = (select PopName from CERPointofPurchase where PopValue = PointofPurchase) 
                SqlDataReader dr = DBUtil.FillDataReader(sql, CERConnString);

                while (dr.Read())
                {
                    CER obj = new CER();

                    obj.cERYear = dr["CERYear"].ToString();
                    obj.cERIdentifier = Convert.ToInt32(dr["CERIdentifier"]);
                    obj.pointOfPurchase = dr["PointOfPurchase"].ToString();
                    obj.cERSource = dr["CERSource"].ToString();
                    obj.DateReportReceived = DateTime.Parse(dr["DateReportReceived"].ToString());
                    obj.cERType = dr["CerType"].ToString().Trim();
                    obj.receivedBy = dr["ReceivedBy"].ToString();
                    obj.complaintNumber = Convert.ToInt32(dr["ComplaintNumber"]);
                    obj.complaintDeterminationBy = dr["ComplaintDeterminationBy"].ToString();
                    obj.customerID = dr["CustomerID"].ToString();

                    obj.firstName = dr["FirstName"].ToString();
                    obj.lastName = dr["Lastname"].ToString();
                    obj.street = dr["Street"].ToString();
                    obj.city = dr["City"].ToString();
                    obj.state = dr["State"].ToString();
                    obj.zip = dr["Zip"].ToString();
                    obj.country = dr["country"].ToString();
                    obj.email = dr["Email"].ToString();
                    obj.phone = dr["Phone"].ToString();
                    obj.productNumber = dr["ProductNumber"].ToString();
                    obj.serialNumber = dr["SerialNumber"].ToString();
                    obj.orderNumber = dr["OrderNumber"].ToString();
                    obj.eventTiming = dr["EventTiming"].ToString();
                    obj.eventDescription = dr["EventDescription"].ToString();
                    obj.actionExplaination = dr["ActionExplaination"].ToString();
                    obj.statusCs = dr["StatusCs"].ToString();
                    obj.statusQa = dr["StatusQa"].ToString();
                    obj.statusMed = dr["StatusMed"].ToString();
                    obj.medicalCondition = dr["MedicalCondition"].ToString();
                    obj.dateModified = DateTime.Parse(dr["DateModified"].ToString());
                    obj.investigationSummary = dr["InvestigationSummary"].ToString();
                    obj.managerApproved = bool.Parse(dr["ManagerApproved"].ToString());
                    obj.mdr = bool.Parse(dr["Mdr"].ToString());
                    obj.analyze = bool.Parse(dr["Analyze"].ToString());

                    cerList.Add(obj);
                }
                if (dr != null) { dr.Close(); }
                //HttpContext.Current.Session["CERList"] = cerList;
                FM2015.Helpers.CacheHelper.Add<List<CER>>(cerList, AdminCart.Config.cachekey_CERList, AdminCart.Config.cacheDuration_CERList);
            }

            return cerList;
        }

        public static List<CER> GetCERList(bool nonCachedList)
        {
            List<CER> cerList = new List<CER>();
            if (nonCachedList)
            {
                FM2015.Helpers.CacheHelper.Clear(AdminCart.Config.cachekey_CERList);
            }
            cerList = GetCERList();
            return cerList;
        }


        //returns a list of CERs w/ EventDescription modified to include Messages w/ search keyword included
        public static List<CER> SearchCERs(string searchParam)
        {
            List<adminCER.CER> cerList = adminCER.CER.GetCERList();
            List<CERMessages> cerMessList = new List<CERMessages>();

            if (!string.IsNullOrEmpty(searchParam))
            {
                cerMessList = CERMessages.SearchCERMEssagesList(searchParam);
                foreach (CERMessages cerMess in cerMessList)
                {
                    CER cer = (from c in cerList
                               where ((c.CERYear == cerMess.CERYear) && (c.CERIdentifier == cerMess.CERIdentifier))
                               select c).First();
                    cer.EventDescription += "<p>MESSAGE: " + cerMess.Message + "</p>";
                }

                cerList = (from cer in cerList
                           where (cer.EventDescription.ToUpper().Contains(searchParam.ToUpper()))
                           select cer).Take(100).ToList();

                string hiliteKeyword = "<span style='background-color:yellow'>" + searchParam.ToUpper() + "</span>";
                foreach(CER cer in cerList)
                {
                    cer.EventDescription = Regex.Replace(cer.EventDescription, searchParam, hiliteKeyword, RegexOptions.IgnoreCase);
                    //cer.EventDescription = HttpUtility.HtmlEncode(cer.EventDescription);
                }                                
            }
            else
            {
                cerList = new List<CER>();
            }
            return cerList;
        }

        //
        public static bool AddRelatedCER(string CERNumber, string year, string id)
        {
            string CERConnString = GetConnStr();

            adminCER.CER c1 = new adminCER.CER(CERNumber);
            int seq = Convert.ToInt32(id);
            adminCER.CER c2 = new adminCER.CER(year + seq.ToString("000000"));

            SqlConnection conn = new SqlConnection(CERConnString);

            string sql = @"
                DECLARE @CERyearTemp char(4), @CERIdentifierTemp int
                SET @CERyearTemp = dbo.fn_GetParentCERyear (@CERyear, @CERIdentifier)
                SET @CERIdentifierTemp = dbo.fn_GetParentCERIdentifier (@CERyear, @CERIdentifier)

                IF NOT EXISTS (
                    SELECT * FROM RelatedCERs
                    WHERE CERYear = @CERyearTemp 
                    AND CERIdentifier = @CERIdentifierTemp
                    AND RelatedCERYear = @RelatedCERYear
                    AND RelatedCERIdentifier = @RelatedCERIdentifier
                )
                BEGIN
                    INSERT INTO RelatedCERs
                    SELECT @CERyearTemp, @CERIdentifierTemp, @RelatedCERYear, @RelatedCERIdentifier
                END
            ";
            //todo...convert to using dbutil exec with parameter collection.  see above for example.

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlParameter CERYearParam = new SqlParameter("@CERYear", SqlDbType.VarChar);
            SqlParameter CERIdentifierParam = new SqlParameter("@CERIdentifier", SqlDbType.Int);
            SqlParameter RelatedCERYearParam = new SqlParameter("@RelatedCERYear", SqlDbType.VarChar);
            SqlParameter RelatedCERIdentifierParam = new SqlParameter("@RelatedCERIdentifier", SqlDbType.Int);

            CERYearParam.Value = c1.CERYear;
            CERIdentifierParam.Value = c1.CERIdentifier;
            RelatedCERYearParam.Value = c2.CERYear;
            RelatedCERIdentifierParam.Value = c2.CERIdentifier;

            cmd.Parameters.Add(CERYearParam);
            cmd.Parameters.Add(CERIdentifierParam);
            cmd.Parameters.Add(RelatedCERYearParam);
            cmd.Parameters.Add(RelatedCERIdentifierParam);

            if (c1.CERIdentifier == c2.CERIdentifier)
            {
                return true;
            }
            else
            {
                conn.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    return false;
                }
                conn.Close();

                return true;
            }
        }

        public static bool RemoveRelatedCER(string ParentCERNumber, string RemoveCERNumber)
        {
            string CERConnString = GetConnStr();

            if (RemoveCERNumber != null)
            {
                string sql = @"
                    DECLARE @CERyearTemp char(4), @CERIdentifierTemp int
                    SET @CERyearTemp = dbo.fn_GetParentCERyear (@CERyear, @CERIdentifier)
                    SET @CERIdentifierTemp = dbo.fn_GetParentCERIdentifier (@CERyear, @CERIdentifier)

                    DELETE FROM RelatedCERs
                    WHERE CERYear = @CERyearTemp 
                    AND CERIdentifier = @CERIdentifierTemp
                    AND RelatedCERYear = @RelatedCERYear
                    AND RelatedCERIdentifier = @RelatedCERIdentifier
                ";

                adminCER.CER c1 = new adminCER.CER(ParentCERNumber);
                adminCER.CER c2 = new adminCER.CER(RemoveCERNumber);

                //populate parameters for queries
                SqlParameter[] NamedParameters = new SqlParameter[4];

                NamedParameters[0] = new SqlParameter("@CERYear", c1.CERYear);
                NamedParameters[1] = new SqlParameter("@CERIdentifier", c1.CERIdentifier);
                NamedParameters[2] = new SqlParameter("@RelatedCERYear", c2.CERYear);
                NamedParameters[3] = new SqlParameter("@RelatedCERIdentifier", c2.CERIdentifier);

                DBUtil.Exec(sql, CERConnString, NamedParameters);

                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool AddMessage(string CERNumber, string Message)
        {
            string CERConnString = GetConnStr();

            if ((!string.IsNullOrEmpty(CERNumber)) && (!string.IsNullOrEmpty(Message)))
            {
                string sql = @" 
                    INSERT INTO MessagesCER (CERYear, CERIdentifier, Dated, Type, Message, AddDate, AddBy) 
                    VALUES ( @CERYear, @CERIdentifier, @Dated, @Type, @Message, @AddDate, @AddBy ) 
            ";

                adminCER.CER c1 = new adminCER.CER(CERNumber);
                string name = FM2015.Models.User.GetUserName();

                SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, c1.CERYear, c1.CERIdentifier, DateTime.Now, "message", Message, DateTime.Now, name);
                SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);
                if (dr != null) { dr.Close(); }
            }

            return true;
        }

        public static string GetConnStr()
        {
            string CERConnString = "";
            string aWS_Deploy = ConfigurationManager.AppSettings["AWS-Deploy"].ToString();
            bool deploy = (aWS_Deploy.ToUpper() == "YES") ? true : false;
            if (deploy)
            {
                CERConnString = "SERVER=.;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=FaceMasterComplaintTracking;Application Name=FaceMasterComplaintResolution";
            }
            else
            {
                CERConnString = "SERVER=fmadmin.facemaster.com;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=FaceMasterComplaintTracking;Application Name=FaceMasterComplaintResolution";
            }
            return CERConnString;
        }
    }
}
