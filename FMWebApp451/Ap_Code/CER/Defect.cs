using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Web.Mvc;
using AdminCart;
using FM2015.Helpers;

namespace CER
{
    /// <summary>
    /// Summary description for Defect
    /// </summary>
    public class Defect
    {
        private int id;
        public int Id { get { return id; } set { id = value; } }

        private DateTime date;
        [Display(Name = "Date")]
        public DateTime Date { get { return date; } set { date = value; } }

        private bool isMedical;
        [Display(Name = "Injury?")]
        public bool IsMedical { get { return isMedical; } set { isMedical = value; } }

        private int symptomVal;
        [Display(Name = "Symptom")]
        public int SymptomVal { get { return symptomVal; } set { symptomVal = value; } }

        private int defectVal;
        [Display(Name = "Defect")]
        public int DefectVal { get { return defectVal; } set { defectVal = value; } }

        private int descriptionVal;
        [Display(Name = "Description")]
        public int DescriptionVal { get { return descriptionVal; } set { descriptionVal = value; } }

        private long serialNum;
        [Display(Name = "Serial#")]
        public long SerialNum { get { return serialNum; } set { serialNum = value; } }

        private string storageLocation;
        [Display(Name = "Uxx Location")]
        public string StorageLocation { get { return storageLocation; } set { storageLocation = value; } }

        private int year;
        public int Year { get { return year; } set { year = value; } }

        private int cerNum;
        public int CerNum { get { return cerNum; } set { cerNum = value; } }

        private int scopePicID;
        public int ScopePicID { get { return scopePicID; } set { scopePicID = value; } }

        public Defect()
        {
            id = 0;
            Date = Convert.ToDateTime("1/1/1900");
            IsMedical = false;
            SymptomVal = 0;
            DefectVal = 0;
            DescriptionVal = 0;
            SerialNum = 0;
            StorageLocation = "";
            Year = 0;
            CerNum = 0;
            ScopePicID = 0;
        }

        public Defect(int defectID)
        {

            string sql = @"
                        SELECT * 
                        FROM DefectAnalysis 
                        WHERE ID = @ID 
                ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, defectID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read())
            {
                Id = Convert.ToInt32(dr["ID"]);
                Date = Convert.ToDateTime(dr["Date"]);
                IsMedical = Convert.ToBoolean(dr["IsMedical"]);
                SymptomVal = Convert.ToInt32(dr["SymptomVal"]);
                DefectVal = Convert.ToInt32(dr["DefectVal"]);
                DescriptionVal = Convert.ToInt32(dr["DescriptionVal"]);
                SerialNum = Convert.ToInt64(dr["SerialNum"]);
                StorageLocation = dr["StorageLocation"].ToString();
                Year = Convert.ToInt32(dr["Year"]);
                CerNum = Convert.ToInt32(dr["CerNum"]);
                ScopePicID = Convert.ToInt32(dr["ScopePicID"]);
            }

            if (dr != null) { dr.Close(); }
        }

        public Defect(int year, int cerNum)
        {

            string sql = @"
                        SELECT * 
                        FROM DefectAnalysis 
                        WHERE Year = @Year AND CerNum = @CerNum  
                ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, year, cerNum);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read())
            {
                Id = Convert.ToInt32(dr["ID"]);
                Date = Convert.ToDateTime(dr["Date"]);
                IsMedical = Convert.ToBoolean(dr["IsMedical"]);
                SymptomVal = Convert.ToInt32(dr["SymptomVal"]);
                DefectVal = Convert.ToInt32(dr["DefectVal"]);
                DescriptionVal = Convert.ToInt32(dr["DescriptionVal"]);
                SerialNum = Convert.ToInt64(dr["SerialNum"]);
                StorageLocation = dr["StorageLocation"].ToString();
                Year = Convert.ToInt32(dr["Year"]);
                CerNum = Convert.ToInt32(dr["CerNum"]);
                ScopePicID = Convert.ToInt32(dr["ScopePicID"]);
            }

            if (dr != null) { dr.Close(); }
        }

        public Defect(string CERNumber)
        {
            int year = Convert.ToInt32(CERNumber.Substring(0, 4));
            int cerNum = Convert.ToInt32(CERNumber.Substring(4));

            string sql = @"
                        SELECT * 
                        FROM DefectAnalysis 
                        WHERE Year = @Year AND CerNum = @CerNum  
                ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, year, cerNum);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read())
            {
                Id = Convert.ToInt32(dr["ID"]);
                Date = Convert.ToDateTime(dr["Date"]);
                IsMedical = Convert.ToBoolean(dr["IsMedical"]);
                SymptomVal = Convert.ToInt32(dr["SymptomVal"]);
                DefectVal = Convert.ToInt32(dr["DefectVal"]);
                DescriptionVal = Convert.ToInt32(dr["DescriptionVal"]);
                SerialNum = Convert.ToInt64(dr["SerialNum"]);
                StorageLocation = dr["StorageLocation"].ToString();
                Year = Convert.ToInt32(dr["Year"]);
                CerNum = Convert.ToInt32(dr["CerNum"]);
                ScopePicID = Convert.ToInt32(dr["ScopePicID"]);
            }

            if (dr != null) { dr.Close(); }
        }

        public int Add()
        {
            String sql = @"
        INSERT INTO DefectAnalysis 

        (Date, IsMedical, SymptomVal, DefectVal, DescriptionVal, SerialNum, 
            StorageLocation, Year, CerNum, ScopePicID) 

        VALUES (@Date, @IsMedical, @SymptomVal, @DefectVal, @DescriptionVal, @SerialNum, 
            @StorageLocation, @Year, @CerNum, @ScopePicID) 
        ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, Date, IsMedical, SymptomVal, DefectVal, DescriptionVal, SerialNum, StorageLocation, Year, CerNum, ScopePicID);
            DBUtil.Exec(sql, Config.ConnStrCER(), mySqlParameters);
            return 0;
        }

        public int Update()
        {
            String sql = @"
        UPDATE DefectAnalysis
        SET
            Date = @Date,  
            IsMedical = @IsMedical,  
            SymptomVal = @SymptomVal,  
            DefectVal = @DefectVal,  
            DescriptionVal = @DescriptionVal,  
            SerialNum = @SerialNum,  
            StorageLocation = @StorageLocation,  
            Year = @Year,  
            CerNum = @CerNum,  
            ScopePicID = @ScopePicID 
        WHERE ID = @ID  
        ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, Date, IsMedical, SymptomVal, 
                DefectVal, DescriptionVal, SerialNum, StorageLocation, Year, CerNum, ScopePicID, Id);
            DBUtil.Exec(sql, Config.ConnStrCER(), mySqlParameters);
            return 0;
        }

        static public List<Defect> GetRelatedDefects(int descriptionVal, DateTime startDate, DateTime endDate)
        {
            List<Defect> theList = new List<Defect>();

            string sql = @"
                        SELECT * 
                        FROM DefectAnalysis 
                        WHERE DescriptVal = @DescriptVal AND Date BETWEEN @StartDate AND @EndDate  
                ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, descriptionVal, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            while (dr.Read())
            {
                Defect defect = new Defect();
                defect.Id = Convert.ToInt32(dr["ID"]);
                defect.Date = Convert.ToDateTime(dr["Date"]);
                defect.IsMedical = Convert.ToBoolean(dr["IsMedical"]);
                defect.SymptomVal = Convert.ToInt32(dr["SymptomVal"]);
                defect.DefectVal = Convert.ToInt32(dr["DefectVal"]);
                defect.DescriptionVal = Convert.ToInt32(dr["DescriptionVal"]);
                defect.SerialNum = Convert.ToInt64(dr["SerialNum"]);
                defect.StorageLocation = dr["StorageLocation"].ToString();
                defect.Year = Convert.ToInt32(dr["Year"]);
                defect.CerNum = Convert.ToInt32(dr["CerNum"]);
                defect.ScopePicID = Convert.ToInt32(dr["ScopePicID"]);
                theList.Add(defect);
            }

            if (dr != null) { dr.Close(); }
            return theList;
        }

        static public int GetLastDefectID()
        {

            string sql = @"
            SELECT MAX(ID) FROM DefectAnalysis 
            WHERE Year > '1900' AND DefectVal <> 8 AND StorageLocation <> 'N/A' 
            ";

            string result = DBUtil.GetScalar(sql, Config.ConnStrCER());
            int LastID = 0;
            bool success = Int32.TryParse(result, out LastID);
            return LastID;
        }

        static public string ConvertSymptomValToStr(int symptomVal)
        {
            string symptomName = "n/a";
            string sql = @"
            SELECT Name FROM Symptoms 
            WHERE Value = @Value  
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, symptomVal);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read())
            {
                symptomName = dr["Name"].ToString();
                dr.Close();
            }
            return symptomName;
        }

        static public string ConvertDefectValToStr(int defectVal)
        {
            string defectName = "n/a";
            string sql = @"
            SELECT Name FROM DefectCategory 
            WHERE Value = @Value  
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, defectVal);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read())
            {
                defectName = dr["Name"].ToString();
                dr.Close();
            }
            return defectName;
        }

        static public string ConvertDefectDescValToStr(int defectDescVal)
        {
            string defectDescName = "n/a";
            string sql = @"
            SELECT Name FROM DefectSubCategory 
            WHERE Value = @Value  
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, defectDescVal);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read())
            {
                defectDescName = dr["Name"].ToString();
                dr.Close();
            }
            return defectDescName;
        }
    }
}