﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CER
{
    public class DefectOpts
    {
        private int id;
        public int Id { get { return id; } set { id = value; } }

        private int _value;
        public int Value { get { return _value; } set { _value = value; } }

        private string name;
        public string Name { get { return name; } set { name = value; } }

        public DefectOpts()
        {
            id = 0;
            _value = 0;
            name = "";
        }

        public DefectOpts(int id)
        {
            string sql = @"
                SELECT * 
                FROM DefectCategory 
                WHERE ID = @ID 
            ";

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, id);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, mySqlParameters, AdminCart.Config.ConnStrCER());

            while (dr.Read())
            {
                id = Convert.ToInt32(dr["ID"]);
                _value = Convert.ToInt32(dr["Value"]);
                name = dr["Name"].ToString();
            }
            if (dr != null) { dr.Close(); }

        }

        public static List<DefectOpts> GetDefectOptsList()
        {
            List<DefectOpts> objList = new List<DefectOpts>();

            string sql = @"
                SELECT * 
                FROM DefectCategory 
            ";

            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER());

            while (dr.Read())
            {
                DefectOpts obj = new DefectOpts();
                obj.Id = Convert.ToInt32(dr["ID"]);
                obj.Value = Convert.ToInt32(dr["Value"]);
                obj.Name = dr["Name"].ToString();
                objList.Add(obj);
            }
            if (dr != null) { dr.Close(); }

            return objList;
        }

        public static SelectList GetDefectOptions()
        {
            List<DefectOpts> objList = DefectOpts.GetDefectOptsList();

            // Build a List<SelectListItem>
            var selectListItems = objList.Select(x => new SelectListItem() { Value = x.Value.ToString(), Text = x.Name }).ToList();

            //convert list to SelectList
            SelectList objSelectList = new SelectList(selectListItems, "Value", "Text", "N/A");

            return objSelectList;
        }
    }
}