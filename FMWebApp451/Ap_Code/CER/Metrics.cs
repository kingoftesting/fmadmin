﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using AdminCart;
using FM2015.Helpers;

namespace CERmetrics
{
    /// <summary>
    /// Summary description for Metrics
    /// </summary>
    public class Metrics
    {

        public Metrics()
        {
            //
            // TODO: Add constructor logic here
            //

        }


        public enum Interval
        {
            Daily, Weekly, Monthly, Quarterly, Yearly
        }


        public enum SalesType
        {
            Web, PO, ALL
        }


        /// <summary>
        /// GetRevenue returns a Decimal that holds value of sales (net of tax & shipping) between startDate and endDate
        /// </summary>
        static public Decimal GetRevenue(SalesType salesType, DateTime startDate, DateTime endDate)
        {
            Decimal ret = 0;
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            if ((salesType == SalesType.PO) || (salesType == SalesType.ALL))
            {
                //get PO (non-web) sales
                sql = @"
            SELECT sum(DevRevenue + AccyRevenue) AS Revenue 
            FROM FMPOSales 
            WHERE Date Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);

                if (dr.Read()) { ret += Convert.ToDecimal(dr["Revenue"]); }
                if (dr != null) { dr.Close(); }
            }

            if ((salesType == SalesType.Web) || (salesType == SalesType.ALL))
            {
                //now get web-sales revenue
                sql = @" 
            SELECT sum(o.Total - o.TotalTax - o.TotalShipping) AS Revenue 
            FROM Orders o, OrderActions oa 
            WHERE o.OrderID = oa.OrderID and oa.OrderActionType = 2 and OrderDate Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);

                if (dr.Read()) { ret += Convert.ToDecimal(dr["Revenue"]); }
                if (dr != null) { dr.Close(); }
            }

            return ret;
        }

        /// <summary>
        /// GetUnits returns a Integer value that holds the number of units sold between startDate and endDate
        /// </summary>   
        static public int GetUnits(SalesType salesType, DateTime startDate, DateTime endDate)
        {
            int ret = 0;
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            if ((salesType == SalesType.PO) || (salesType == SalesType.ALL))
            {
                //get PO (non-web) units
                sql = @" 
            SELECT sum(DevUnits) AS Units 
            FROM FMPOSales 
            WHERE Date Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);

                if (dr.Read()) { ret += Convert.ToInt32(dr["Units"]); }
                if (dr != null) { dr.Close(); }
            }

            if ((salesType == SalesType.Web) || (salesType == SalesType.ALL))
            {
                //now get web-sales revenue
                sql = @" 
            SELECT sum(od.quantity) AS Units 
            FROM Orders o, OrderDetails od, OrderActions oa 
            WHERE o.OrderID = od.OrderID and o.OrderID = oa.OrderID and oa.OrderactionType = 2 
                and (od.ProductID = 14 OR od.ProductID = 23 OR od.ProductID = 1 OR od.ProductID = 11) 
                and OrderDate Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);

                if (dr.Read()) { ret += Convert.ToInt32(dr["Units"]); }
                if (dr != null) { dr.Close(); }
            }

            return ret;
        }

        /// <summary>
        /// GeReturns returns a Integer value that holds the number of units returned  between startDate and endDate
        /// </summary>   
        static public int GetReturns(SalesType salesType, DateTime startDate, DateTime endDate)
        {
            int ret = 0;
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            if ((salesType == SalesType.PO) || (salesType == SalesType.ALL))
            {
                //get PO (non-web) units returmed - unofficial data; uses the average of 23% return rate
                sql = @" 
            SELECT (sum(DevUnits) * .23) AS Units 
            FROM FMPOSales 
            WHERE Date Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);

                if (dr.Read()) { ret += Convert.ToInt32(dr["Units"]); }
                if (dr != null) { dr.Close(); }
            }

            if ((salesType == SalesType.Web) || (salesType == SalesType.ALL))
            {
                //now get web-sales revenue
                sql = @" 
            SELECT Count(ID) AS Units 
            FROM returnanalysis 
            WHERE DateReceived Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

                if (dr.Read()) { ret += Convert.ToInt32(dr["Units"]); }
                if (dr != null) { dr.Close(); }
            }

            return ret;
        }

        /// <summary>
        /// GeReturnPercent returns a Decimal value that holds 100 * (number of units returned / number of units sold) between startDate and endDate
        ///     Data is for WebSales only
        /// </summary>   
        static public int GetReturnPercent(DateTime startDate, DateTime endDate)
        {
            int sold = GetUnits(SalesType.Web, startDate, endDate);
            int returned = GetReturns(SalesType.Web, startDate, endDate);

            return 100 * (returned / sold);
        }

        /// <summary>
        /// GetComplaints returns a Integer value that holds the number of complaints received (in the CER System) between startDate and endDate
        /// </summary>   
        static public int GetComplaints(DateTime startDate, DateTime endDate)
        {
            int ret = 0;
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            //now get web-sales revenue
            sql = @" 
            select count(CERIdentifier)
            from CER2s
            where DateReportReceived between @startDate and @endDate 
        ";

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read()) { ret += Convert.ToInt32(dr["Units"]); }
            if (dr != null) { dr.Close(); }

            return ret;
        }

        /// <summary>
        /// GetComplaintPercent returns a Decimal value that holds 100 * (number of units returned / number of units sold) between startDate and endDate
        ///     Data is for all recorded sales (ie, web, HSN, SNBC, etc.)
        /// </summary>   
        static public int GetComplaintPercent(DateTime startDate, DateTime endDate)
        {
            int sold = GetUnits(SalesType.ALL, startDate, endDate);
            int returned = GetComplaints(startDate, endDate);

            return 100 * (returned / sold);
        }

        /// <summary>
        /// GetMetric Set returns a Dataset full of lots of stuff!
        ///     The Dataset includes the following tables:
        ///         "POSales": raw sales data from startDate to endDate from POs; columns = Date, Revenue; grouped and ordered by Date (with gaps)
        ///             - is NULL if SalesType is not ALL or PO
        ///         "POUnits": raw units sold data from startDate to endDate from POs; columns = Date, Revenue; grouped and ordered by Date (with gaps)
        ///             - is NULL if SalesType is not ALL or PO
        ///         "WebSales": raw sales data from startDate to endDate from Web sales; columns = Date, Revenue; grouped and ordered by Date (with gaps)
        ///             - is NULL if SalesType is not ALL or Web
        ///         "WebUnits": raw units sold data from startDate to endDate from Web sales; columns = Date, Revenue; grouped and ordered by Date (with gaps)
        ///             - is NULL if SalesType is not ALL or Web
        ///         "TotalSalesByDay": combined data from "POSales" and "WebSales"; columns = Date, Revenue, 7D MA; grouped and ordered by Date (by day, no gaps)
        ///         "TotalUnitsByDay": combined data from "POUnits" and "WebUnits"; columns = Date, Units, 7D MA; grouped and ordered by Date (by day, no gaps)
        ///         
        ///         "TotalSalesByWeek": combined data from "POSales" and "WebSales"; columns = Date, Revenue; grouped and ordered by Date (by week, no gaps)
        ///         "TotalUnitsByWeek": combined data from "POUnits" and "WebUnits"; columns = Date, Units; grouped and ordered by Date (by week, no gaps)
        ///         
        ///         "TotalSalesByMonth": combined data from "POSales" and "WebSales"; columns = Date, Revenue; grouped and ordered by Date (by month, no gaps)
        ///         "TotalUnitsByMonth": combined data from "POUnits" and "WebUnits"; columns = Date, Units; grouped and ordered by Date (by month, no gaps)
        ///         
        ///         "TotalSalesByQTR": combined data from "POSales" and "WebSales"; columns = Date, Revenue; grouped and ordered by Date (by qtr, no gaps)
        ///         "TotalUnitsByQTR": combined data from "POUnits" and "WebUnits"; columns = Date, Units; grouped and ordered by Date (by qtr, no gaps)
        ///         
        ///         "TotalSalesByYear": combined data from "POSales" and "WebSales"; columns = Date, Revenue; grouped and ordered by Date (by year, no gaps)
        ///         "TotalUnitsByYear": combined data from "POUnits" and "WebUnits"; columns = Date, Units; grouped and ordered by Date (by year, no gaps)
        /// </summary>
        /// <param name="salesType"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        static public DataSet GetMetricSet(Interval interval, SalesType salesType, DateTime startDate, DateTime endDate)
        {
            //DataSet ret = new DataSet();
            //ret.Tables.Add(AddTotalSalesByDay(metrics, startDate, endDate));
            //ret.Tables.Add(AddTotalSalesByWeek(metrics, startDate, endDate));
            //ret.Tables.Add(AddTotalSalesByInterval(interval, salesType, startDate, endDate));

            return AddTotalSalesByInterval(interval, salesType, startDate, endDate); // ds.Tables["Metrics"]
        }

        static private DataSet AddTotalSalesByInterval(Interval interval, SalesType salesType, DateTime startDate, DateTime endDate)
        {
            DateTime startDate0 = new DateTime();
            DateTime endDate0 = new DateTime();
            DateTime startDate1 = new DateTime();
            DateTime endDate1 = new DateTime();
            DateTime startDate2 = new DateTime();
            DateTime endDate2 = new DateTime();
            switch (salesType)
            {
                case SalesType.ALL:
                    startDate0 = startDate;
                    endDate0 = endDate;
                    startDate1 = startDate;
                    endDate1 = endDate;
                    startDate2 = startDate;
                    endDate2 = endDate;
                    break;

                case SalesType.PO:
                    startDate0 = startDate;
                    endDate0 = endDate;
                    startDate1 = startDate.AddYears(100); //ensure 0 records returned for websales
                    endDate1 = endDate.AddYears(100);
                    startDate2 = startDate.AddYears(100);
                    endDate2 = endDate.AddYears(100);
                    break;

                case SalesType.Web:
                    startDate0 = startDate;
                    endDate0 = endDate;
                    startDate0 = startDate.AddYears(100); //ensure 0 records returned for POsales
                    endDate0 = endDate.AddYears(100);
                    startDate1 = startDate;
                    endDate1 = endDate;
                    startDate2 = startDate;
                    endDate2 = endDate;

                    break;

                default:
                    startDate0 = startDate;
                    endDate0 = endDate;
                    startDate1 = startDate;
                    endDate1 = endDate;
                    startDate2 = startDate;
                    endDate2 = endDate;
                    break;

            }

            string sql = GetRevenueSQL(interval);

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate0, endDate0, startDate1, endDate1, startDate2, endDate2);
            DataSet ds = DBUtil.FillDataSet(sql, "Metrics", mySqlParameters, Config.ConnStr());


            ds.Tables["Metrics"].Columns.Add("revMA", typeof(Decimal));
            ds.Tables["Metrics"].Columns.Add("unitsMA", typeof(Decimal));
            ds.Tables["Metrics"].Columns.Add("Date", typeof(string));
            ds.Tables["Metrics"].Columns.Add("Returns", typeof(Int32));
            ds.Tables["Metrics"].Columns.Add("ReturnPercent", typeof(Decimal));
            ds.Tables["Metrics"].Columns.Add("Complaints", typeof(Int32));
            ds.Tables["Metrics"].Columns.Add("ComplaintPercent", typeof(Decimal));
            // create MA values in tables here
            decimal sumRev = 0;
            decimal aveRev = 0;
            decimal sumUnits = 0;
            decimal aveUnits = 0;
            int maCounter = 0;

            switch (interval)
            {
                case Interval.Daily:
                    maCounter = 7; //7 days per week --> 7D MA
                    break;

                case Interval.Weekly:
                    maCounter = 4; //4 weeks per month --> 4W MA
                    break;

                case Interval.Monthly: //3 months per quarter --> 3M MA
                    maCounter = 3;
                    break;

                case Interval.Quarterly: //4 quarters per year --> 4Q MA
                    maCounter = 4;
                    break;

                case Interval.Yearly: //3Y MA
                    maCounter = 3;
                    break;

                default:
                    maCounter = 1;
                    break;
            }

            // create 4W MA values in table
            for (int i = 0; i < ds.Tables["Metrics"].Rows.Count; i++)
            {
                sumRev += Convert.ToDecimal(ds.Tables["Metrics"].Rows[i]["Revenue"]);
                sumUnits += Convert.ToDecimal(ds.Tables["Metrics"].Rows[i]["Units"]);
                if (i < (maCounter - 1))
                {
                    aveRev = sumRev / (i + 1);
                    aveUnits = sumUnits / (i + 1);
                }
                else
                {
                    aveRev = sumRev / maCounter;
                    sumRev = sumRev - Convert.ToDecimal(ds.Tables["Metrics"].Rows[i - (maCounter - 1)]["Revenue"]); //decrement by first term in series, get ready to incrementy by next term
                    aveUnits = sumUnits / maCounter;
                    sumUnits = sumUnits - Convert.ToDecimal(ds.Tables["Metrics"].Rows[i - (maCounter - 1)]["Units"]); //decrement by first term in series, get ready to incrementy by next term
                }
                ds.Tables["Metrics"].Rows[i]["revMA"] = aveRev;
                ds.Tables["Metrics"].Rows[i]["unitsMA"] = aveUnits;

                string year = ds.Tables["Metrics"].Rows[i]["year"].ToString();
                DateTime dt = Convert.ToDateTime("1/1/" + year);
                string dtStr = "";
                switch (interval)
                {
                    case Interval.Daily:
                        int monthNum = Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["monthNum"]);
                        int dayNum = Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["dayNum"]);
                        //dt = dt.AddMonths(monthNum - 1);
                        //dt = dt.AddDays(dayNum - 1);
                        dt = new DateTime(Convert.ToInt32(year), monthNum, dayNum);
                        dtStr = dt.ToShortDateString();
                        break;

                    case Interval.Weekly:
                        int weekNum = Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["weekNum"]);
                        int days = (weekNum - 1) * 7;
                        dt = dt.AddDays(days);
                        dtStr = "wk" + weekNum.ToString() + " " + dt.Year.ToString();
                        break;

                    case Interval.Monthly:
                        monthNum = Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["monthNum"]);
                        dt = new DateTime(Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["year"]), monthNum, 1);
                        System.Globalization.DateTimeFormatInfo info = System.Globalization.DateTimeFormatInfo.GetInstance(System.Globalization.CultureInfo.InvariantCulture);
                        dtStr = info.AbbreviatedMonthNames[monthNum - 1] + year.ToString();
                        break;

                    case Interval.Quarterly:
                        int qtrNum = Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["qtrNum"]);
                        dtStr = qtrNum + "Q" + year.ToString();
                        switch (qtrNum)
                        {
                            case 1:
                                dt = new DateTime(Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["year"]), 1, 1);
                                break;

                            case 2:
                                dt = new DateTime(Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["year"]), 4, 1);
                                break;

                            case 3:
                                dt = new DateTime(Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["year"]), 7, 1);
                                break;

                            case 4:
                                dt = new DateTime(Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["year"]), 10, 1);
                                break;

                        }
                        break;

                    case Interval.Yearly:
                        dtStr = year.ToString();
                        break;

                }

                //ds.Tables["Metrics"].Rows[i]["Date"] = dt;
                ds.Tables["Metrics"].Rows[i]["Date"] = dtStr;
            }

            //now go get return data
            sql = GetReturnWebSQL(interval);

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate1, endDate1);
            DataSet dsRetWeb = DBUtil.FillDataSet(sql, "ReturnsWeb", mySqlParameters, Config.ConnStrCER());

            sql = GetReturnPOSQL(interval);

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate0, endDate0);
            DataSet dsRetPO = DBUtil.FillDataSet(sql, "ReturnsPO", mySqlParameters, Config.ConnStr());

            if ((ds.Tables["Metrics"].Rows.Count != dsRetWeb.Tables["ReturnsWeb"].Rows.Count) || (ds.Tables["Metrics"].Rows.Count != dsRetPO.Tables["ReturnsPO"].Rows.Count) || (dsRetWeb.Tables["ReturnsWeb"].Rows.Count != dsRetPO.Tables["ReturnsPO"].Rows.Count))
            {
                for (int i = 0; i < ds.Tables["Metrics"].Rows.Count; i++)
                {
                    ds.Tables["Metrics"].Rows[i]["Returns"] = 0;
                    ds.Tables["Metrics"].Rows[i]["ReturnPercent"] = 0;
                }
            }
            else
            {
                for (int i = 0; i < ds.Tables["Metrics"].Rows.Count; i++)
                {
                    decimal tempMetricUnits = Convert.ToDecimal(ds.Tables["Metrics"].Rows[i]["Units"]);
                    decimal tempReturnWebUnits = Convert.ToDecimal(dsRetWeb.Tables["ReturnsWeb"].Rows[i]["Units"]);
                    decimal tempReturnPOUnits = Convert.ToDecimal(dsRetPO.Tables["ReturnsPO"].Rows[i]["Units"]);
                    decimal tempReturnUnits = tempReturnWebUnits + tempReturnPOUnits;
                    decimal returnPercent = 0;

                    if (tempMetricUnits != 0) { returnPercent = (tempReturnUnits / tempMetricUnits); }

                    ds.Tables["Metrics"].Rows[i]["Returns"] = tempReturnUnits;
                    ds.Tables["Metrics"].Rows[i]["ReturnPercent"] = returnPercent;
                }
            }

            //now go get return date
            sql = GetComplaintSQL(interval);

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate, endDate);
            DataSet dsComp = DBUtil.FillDataSet(sql, "Complaints", mySqlParameters, Config.ConnStrCER());

            if (ds.Tables["Metrics"].Rows.Count != dsComp.Tables["Complaints"].Rows.Count)
            {
                for (int i = 0; i < ds.Tables["Metrics"].Rows.Count; i++)
                {
                    ds.Tables["Metrics"].Rows[i]["Complaints"] = 0;
                    ds.Tables["Metrics"].Rows[i]["ComplaintPercent"] = 0;
                }
            }
            else
            {
                for (int i = 0; i < ds.Tables["Metrics"].Rows.Count; i++)
                {
                    decimal tempMetricUnits = Convert.ToDecimal(ds.Tables["Metrics"].Rows[i]["UnitsMA"]);
                    decimal tempComplaints = Convert.ToDecimal(dsComp.Tables["Complaints"].Rows[i]["Complaints"]);
                    decimal complaintPercent = 0;

                    // % is valid only if all units are counted
                    if ((salesType == SalesType.ALL) && (tempMetricUnits != 0)) { complaintPercent = (tempComplaints / tempMetricUnits); }

                    ds.Tables["Metrics"].Rows[i]["Complaints"] = tempComplaints;

                    ds.Tables["Metrics"].Rows[i]["ComplaintPercent"] = complaintPercent;
                }

            }

            return ds;
        }

        static private string GetRevenueSQL(Interval interval)
        {
            string sql = "";
            switch (interval)
            {
                case Interval.Daily:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               revenue = sum(Revenue), 
                               devRev = sum(DevRev), 
                               accyRev = sum(AccyRev), 
                               units = sum(units) 
                        FROM ( " + GetRevSQL_FROM() + ") AS tmp" + @" 
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 
                        ";
                    break;

                case Interval.Weekly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(week,[Date]) AS 'weekNum', 
                               revenue = sum(Revenue), 
                               devRev = sum(DevRev), 
                               accyRev = sum(AccyRev), 
                               units = sum(units) 
                        FROM ( " + GetRevSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(week,[Date]), 
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Monthly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
                               revenue = sum(Revenue), 
                               devRev = sum(DevRev), 
                               accyRev = sum(AccyRev), 
                               units = sum(units) 
                        FROM ( " + GetRevSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Quarterly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(quarter,[Date]) AS 'qtrNum', 
                               revenue = sum(Revenue), 
                               devRev = sum(DevRev), 
                               accyRev = sum(AccyRev), 
                               units = sum(units) 
                        FROM ( " + GetRevSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(quarter,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Yearly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
                               revenue = sum(Revenue), 
                               devRev = sum(DevRev), 
                               accyRev = sum(AccyRev), 
                               units = sum(units) 
                        FROM ( " + GetRevSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 

                        ";
                    break;

                default:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               revenue = sum(Revenue), 
                               devRev = sum(DevRev), 
                               accyRev = sum(AccyRev), 
                               units = sum(units)  
                        FROM ( " + GetRevSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 

                        ";
                    break;
            }
            return sql;
        }

        static private string GetRevSQL_FROM()
        {
            string sql = @"
    		SELECT 
			    [Date] = DateFull, 
			    Revenue = 0, 
                DevRev = 0, 
                AccyRev = 0, 
			    units = 0 
		    FROM DateLookup 
		    WHERE DateFull Between @startDate and @endDate 
		    UNION ALL 
            SELECT 
			    [Date], 
			    Revenue = (DevRevenue + AccyRevenue) * .77, 
                DevRev = DevRevenue * .77, 
                AccyRev = AccyRevenue * .77, 
			    units = DevUnits 
		    FROM FMPOSales 
		    WHERE Date Between @startDate0 and @endDate0  
		    UNION ALL 
            SELECT 
                [Date] = o.OrderDate, 
			    revenue = Total-TotalTax-TotalShipping, 
                DevRev = 0, 
                AccyRev = 0, 
			    units = 0 
		    FROM orders o, orderactions oa 
		    WHERE o.orderid = oa.orderid AND oa.OrderActionType = 2 
			    and o.OrderDate BETWEEN @startDate1 and @endDate1 			
		    UNION ALL 
			SELECT 
				[Date] = o.OrderDate, 
				Revenue = 0, 
				CASE 
				WHEN (od.ProductID = 14 OR od.ProductID = 23 OR od.ProductID = 1 OR od.ProductID = 11) 
					THEN od.quantity * (od.price - od.discount) 
					ELSE 0
				END
				DevRev,
				CASE 
				WHEN (od.ProductID = 14 OR od.ProductID = 23 OR od.ProductID = 1 OR od.ProductID = 11) 
					THEN 0 
					ELSE od.quantity * (od.price - od.discount)
				END  
				AccyRev,
				CASE 
				WHEN (od.ProductID = 14 OR od.ProductID = 23 OR od.ProductID = 1 OR od.ProductID = 11) 
					THEN od.quantity 
					ELSE 0 
				END  
				Units
			FROM Orders o, OrderDetails od, OrderActions oa 
			WHERE o.OrderID = od.OrderID and o.OrderID = oa.OrderID and oa.OrderactionType = 2 
				and OrderDate Between @startDate2 and @endDate2   
        ";
            return sql;
        }

        static private string GetReturnWebSQL(Interval interval)
        {
            string sql = "";
            switch (interval)
            {
                case Interval.Daily:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + ") AS tmp" + @" 
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 
                        ";
                    break;

                case Interval.Weekly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(week,[Date]) AS 'weekNum', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(week,[Date]), 
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Monthly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Quarterly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(quarter,[Date]) AS 'qtrNum', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(quarter,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Yearly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(YEAR,[Date]) 
                        ORDER BY 1; 

                        ";
                    break;

                default:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 

                        ";
                    break;
            }
            return sql;
        }

        static private string GetRetWebSQL_FROM()
        {
            string sql = @"
    		SELECT 
			    [Date] = DateFull, 
			    units = 0 
		    FROM DateLookup 
		    WHERE DateFull Between @startDate and @endDate 
		    UNION ALL 
			SELECT 
				[Date],  
				Units = Count(ID) 
			FROM returnanalysis 
			WHERE DateReceived Between @startDate0 and @endDate0  
            GROUP BY [Date] 
        ";
            return sql;
        }

        static private string GetReturnPOSQL(Interval interval)
        {
            string sql = "";
            switch (interval)
            {
                case Interval.Daily:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + ") AS tmp" + @" 
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 
                        ";
                    break;

                case Interval.Weekly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(week,[Date]) AS 'weekNum', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(week,[Date]), 
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Monthly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Quarterly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(quarter,[Date]) AS 'qtrNum', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(quarter,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Yearly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(YEAR,[Date]) 
                        ORDER BY 1; 

                        ";
                    break;

                default:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 

                        ";
                    break;
            }
            return sql;
        }

        static private string GetRetPOSQL_FROM()
        {
            string sql = @"
    		SELECT 
			    [Date] = DateFull, 
			    units = 0 
		    FROM DateLookup 
		    WHERE DateFull Between @startDate and @endDate 
		    UNION ALL 
			SELECT 
				[Date],  
				Units = sum(DevUnits * .23) 
			FROM FMPOSales 
			WHERE Date Between @startDate0 and @endDate0  
            GROUP BY [Date] 
        ";
            return sql;
        }

        static private string GetComplaintSQL(Interval interval)
        {
            string sql = "";
            switch (interval)
            {
                case Interval.Daily:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + ") AS tmp" + @" 
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 
                        ";
                    break;

                case Interval.Weekly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(week,[Date]) AS 'weekNum', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(week,[Date]), 
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Monthly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Quarterly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(quarter,[Date]) AS 'qtrNum', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(quarter,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Yearly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(YEAR,[Date]) 
                        ORDER BY 1; 

                        ";
                    break;

                default:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 

                        ";
                    break;
            }
            return sql;
        }

        static private string GetCompSQL_FROM()
        {
            string sql = @"
    		SELECT 
			    [Date] = DateFull, 
			    complaints = 0 
		    FROM DateLookup 
		    WHERE DateFull Between @startDate and @endDate 
		    UNION ALL 
			SELECT 
	            [Date] = DateReportReceived,  
	            complaints = count(CERIdentifier) 
            FROM CER2s 
            WHERE CERType = 'C' and DateReportReceived Between @startDate0 and @endDate0  
            GROUP BY [DateReportReceived] 
        ";
            return sql;
        }

    }
}