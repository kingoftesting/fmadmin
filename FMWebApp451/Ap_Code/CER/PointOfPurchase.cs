﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using AdminCart;
using FM2015.Helpers;
using System.Linq;

namespace adminCER
{
    /// <summary>
    /// Summary description for CER_PointOfPurchase
    /// </summary>
    public class CER_PointOfPurchase
    {
        #region Private/Public Vars
        private int id;
        public int ID { get { return id; } set { id = value; } }
        private string popValue;
        public string PopValue { get { return popValue; } set { popValue = value; } }
        private string popName;
        public string PopName { get { return popName; } set { popName = value; } }
        private int rank;
        public int Rank { get { return rank; } set { rank = value; } }
        #endregion

        public CER_PointOfPurchase()
        {
            ID = 0;
            PopValue = "n/a";
            PopName = "n/a";
            Rank = 0;
        }

        public CER_PointOfPurchase(int IDfield)
        {
            string sql = @" 
            SELECT * FROM  CERPointOfPurchase 
            WHERE ID = @ID 
        ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, IDfield);
            SqlDataReader dr = DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER(), mySqlParameters);
            if (dr.Read())
            {
                ID = Convert.ToInt32(dr["ID"]);
                PopValue = dr["PopValue"].ToString();
                PopName = dr["PopName"].ToString();
                Rank = Convert.ToInt32(dr["Rank"]);
            }
            if (dr != null) { dr.Close(); }
        }

        public int Save(int ID)
        {
            int result = 0;
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (ID == 0)
            {
                string sql = @" 
                INSERT INTO CERPointOfPurchase 
                (PopValue, PopName, Rank) 
                VALUES(@PopValue, @PopName, @Rank); 
                SELECT ID=@@identity;  
            ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, PopValue, PopName, Rank);
                dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ID"]);
                }
                else
                {
                    result = 99;
                }
            }
            else
            {
                string sql = @" 
                UPDATE CERPointOfPurchase 
                SET  
                    PopValue = @PopValue,  
                    PopName = @PopName,  
                    Rank = @Rank 
                    WHERE ID = @ID  
            ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, PopValue, PopName, Rank, ID);
                DBUtil.Exec(sql, Config.ConnStrCER(), mySqlParameters);
            }
            return result;
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all CER_PointOfPurchase
        /// </summary>
        public static List<CER_PointOfPurchase> GetCER_PointOfPurchaseList()
        {
            List<CER_PointOfPurchase> thelist = new List<CER_PointOfPurchase>();

            string sql = @" 
            SELECT * FROM CERPointOfPurchase 
            ORDER BY Rank ASC 
        ";

            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER());

            while (dr.Read())
            {
                CER_PointOfPurchase obj = new CER_PointOfPurchase();

                obj.ID = Convert.ToInt32(dr["ID"]);
                obj.PopValue = dr["PopValue"].ToString();
                obj.PopName = dr["PopName"].ToString();
                obj.Rank = Convert.ToInt32(dr["Rank"]);

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a className object with the specified ID
        /// </summary>

        public static CER_PointOfPurchase GetCER_PointOfPurchaseByID(int id)
        {
            CER_PointOfPurchase obj = new CER_PointOfPurchase(id);
            return obj;
        }

        /// <summary>
        /// Updates an existing CER_PointOfPurchase
        /// </summary>
        public static bool UpdateCER_PointOfPurchase(int id,
        string popvalue, string popname, int rank)
        {
            CER_PointOfPurchase obj = new CER_PointOfPurchase();

            obj.ID = id;
            obj.PopValue = popvalue;
            obj.PopName = popname;
            obj.Rank = rank;
            int ret = obj.Save(obj.ID);
            return (ret > 0) ? true : false;
        }

        /// <summary>
        /// Creates a new CER_PointOfPurchase
        /// </summary>
        public static int InsertCER_PointOfPurchase(
        string popvalue, string popname, int rank)
        {
            CER_PointOfPurchase obj = new CER_PointOfPurchase();

            obj.PopValue = popvalue;
            obj.PopName = popname;
            obj.Rank = rank;
            int ret = obj.Save(obj.ID);
            return ret;
        }

        /// <summary>
        /// Returns a PoP Name given a PoP Value
        /// </summary>
        public static string ConvertPoPValueToName(string value)
        {
            string popName = "N/A";
            if (!string.IsNullOrEmpty(value))
            {
                List<CER_PointOfPurchase> popList = CER_PointOfPurchase.GetCER_PointOfPurchaseList();
                popName = (from p in popList where Convert.ToInt32(p.popValue) == Convert.ToInt32(value) select p.popName).FirstOrDefault();
            }
            return popName;
        }

    }
}