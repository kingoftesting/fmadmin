using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AdminCart;
using FM2015.Helpers;

namespace CER
{

    /// <summary>
    /// Summary description for Returns
    /// </summary>
    public class Returns
    {
        private int id;
        public int Id { get { return id; } set { id = value; } }

        private DateTime date;
        public DateTime Date { get { return date; } set { date = value; } }

        private DateTime dateReceived;
        public DateTime DateReceived { get { return dateReceived; } set { dateReceived = value; } }

        private int year;
        public int Year { get { return year; } set { year = value; } }

        private int cerNum;
        public int CerNum { get { return cerNum; } set { cerNum = value; } }

        private int reasonVal;
        public int ReasonVal { get { return reasonVal; } set { reasonVal = value; } }

        private int reason2Val;
        public int Reason2Val { get { return reason2Val; } set { reason2Val = value; } }

        public Returns()
        {
            id = 0;
            Date = Convert.ToDateTime("1/1/1900");
            DateReceived = Convert.ToDateTime("1/1/1900");
            Year = 0;
            CerNum = 0;
            ReasonVal = 0;
            Reason2Val = 0;
        }

        public Returns(int returnID)
        {

            string sql = @"
                        SELECT * 
                        FROM ReturnAnalysis 
                        WHERE ID = @ID 
                ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, returnID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read())
            {
                Id = Convert.ToInt32(dr["ID"]);
                Date = Convert.ToDateTime(dr["Date"]);
                DateReceived = Convert.ToDateTime(dr["DateReceived"]);
                Year = Convert.ToInt32(dr["Year"]);
                CerNum = Convert.ToInt32(dr["CerNum"]);
                ReasonVal = Convert.ToInt32(dr["ReasonVal"]);
                Reason2Val = Convert.ToInt32(dr["Reason2Val"]);
            }

            if (dr != null) { dr.Close(); }
        }

        public Returns(int year, int cerNum)
        {

            string sql = @"
                        SELECT * 
                        FROM ReturnAnalysis 
                        WHERE Year = @Year AND CerNum = @CerNum  
                ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, year, cerNum);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read())
            {
                Id = Convert.ToInt32(dr["ID"]);
                Date = Convert.ToDateTime(dr["Date"]);
                DateReceived = Convert.ToDateTime(dr["DateReceived"]);
                Year = Convert.ToInt32(dr["Year"]);
                CerNum = Convert.ToInt32(dr["CerNum"]);
                ReasonVal = Convert.ToInt32(dr["ReasonVal"]);
                Reason2Val = Convert.ToInt32(dr["Reason2Val"]);
            }

            if (dr != null) { dr.Close(); }
        }

        public Returns(string CERNumber)
        {
            int year = Convert.ToInt32(CERNumber.Substring(0, 4));
            int cerNum = Convert.ToInt32(CERNumber.Substring(4));

            string sql = @"
                        SELECT * 
                        FROM ReturnAnalysis 
                        WHERE Year = @Year AND CerNum = @CerNum  
                ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, year, cerNum);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read())
            {
                Id = Convert.ToInt32(dr["ID"]);
                Date = Convert.ToDateTime(dr["Date"]);
                DateReceived = Convert.ToDateTime(dr["DateReceived"]);
                Year = Convert.ToInt32(dr["Year"]);
                CerNum = Convert.ToInt32(dr["CerNum"]);
                ReasonVal = Convert.ToInt32(dr["ReasonVal"]);
                Reason2Val = Convert.ToInt32(dr["Reason2Val"]);
            }

            if (dr != null) { dr.Close(); }
        }

        public int Add()
        {
            String sql = @"
        INSERT INTO ReturnAnalysis 

        (Date, DateReceived, Year, CerNum, ReasonVal, Reason2Val) 

        VALUES (@Date, @DateReceived, @Year, @CerNum, @ReasonVal, @Reason2Val) 
        ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, Date, DateReceived, Year, CerNum, ReasonVal, Reason2Val);
            DBUtil.Exec(sql, Config.ConnStrCER(), mySqlParameters);
            return 0;
        }

        public int Update()
        {
            String sql = @"
        UPDATE DefectsAnalysis
        SET
            Date=@Date,  
            DateReceived=@DateReceived,  
            Year=@Year,  
            CerNum=@CerNum,  
            ReasonVal=@ReasonVal,  
            Reason2Val=@Reason2Val  
        WHERE   ID = @ID  
        ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, Date, DateReceived, Year, CerNum, ReasonVal, Reason2Val, Id);
            DBUtil.Exec(sql, Config.ConnStrCER(), mySqlParameters);
            return 0;
        }

        static public string ConvertReasonValToStr(int reasonVal)
        {
            string reasonName = "n/a";
            string sql = @"
            SELECT Name FROM ReturnReasons 
            WHERE Value = @Value  
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, reasonVal);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read())
            {
                reasonName = dr["Name"].ToString();
                dr.Close();
            }
            return reasonName;
        }

        static public string ConvertReasonVal2ToStr(int reasonVal2)
        {
            string reasonName = "n/a";
            string sql = @"
            SELECT Name FROM ReturnReasons2 
            WHERE Value = @Value  
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, reasonVal2);
            SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

            if (dr.Read())
            {
                reasonName = dr["Name"].ToString();
                dr.Close();
            }
            return reasonName;
        }
    }
}
