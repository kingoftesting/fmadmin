﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMWebApp451.Controllers;

namespace adminCER
{
    public class Service
    {

        public static List<AJAXFormParams> parseAJAXResponse(JSONNameValuePair[] nvArray)
        {
            List<AJAXFormParams> responseList = new List<AJAXFormParams>();
            adminCER.AJAXFormParams cMessage = new adminCER.AJAXFormParams();

            int intVal = 0;
            long longVal = 0;
            string year = "";
            string CERNumber = "";
            bool buildList = true;
            foreach (JSONNameValuePair nvPair in nvArray)
            {
                buildList = true;
                switch (nvPair.Name.ToUpper())
                {
                    case "CERNUMBER":
                        if (long.TryParse(nvPair.Value, out longVal)) //check for string value = long, like "201600001"
                        {
                            CERNumber = nvPair.Value;
                            cMessage.CERNumber = CERNumber;
                        }
                        break;

                    case "YEAR":
                        if (int.TryParse(nvPair.Value, out intVal)) //check for string value = int, like "2016"
                        {
                            nvPair.Value = nvPair.Value.Replace(" ", ""); //get rid any spurious spaces from input, if any
                            year = nvPair.Value;
                        }
                        break;

                    case "ID":
                        if (nvPair.Name.ToUpper() == "ID")
                        {
                            nvPair.Value = nvPair.Value.Replace(" ", ""); //get rid any spurious spaces from input, if any
                            string[] idArray = nvPair.Value.Split(',');
                            for (int i = 0; i < idArray.Length; i++)
                            {
                                adminCER.AJAXFormParams cID = new adminCER.AJAXFormParams();
                                cID.CERNumber = CERNumber;
                                cID.Year = year;
                                if (int.TryParse(idArray[i], out intVal))
                                {
                                    cID.Id = idArray[i];
                                }
                                responseList.Add(cID);
                            }
                            buildList = false;
                        }
                        break;

                    case "MESSAGE":
                        cMessage.Message = nvPair.Value;
                        break;

                    case "DEFECTSYMPTOM":
                        if (int.TryParse(nvPair.Value, out intVal)) //check for string value = int, like "2016"
                        {
                            cMessage.symptomVal = intVal;
                        }
                        break;

                    case "DEFECTVAL":
                        if (int.TryParse(nvPair.Value, out intVal)) //check for string value = int, like "2016"
                        {
                            cMessage.defectVal = intVal;
                        }
                        break;

                    case "DEFECTDESCRIPTION":
                        if (int.TryParse(nvPair.Value, out intVal)) //check for string value = int, like "2016"
                        {
                            cMessage.defectCatVal = intVal;
                        }
                        break;

                    case "DEFECTLOCATION":
                        nvPair.Value = nvPair.Value.Replace(" ", ""); //get rid any spurious spaces from input, if any
                        cMessage.defectLocation = nvPair.Value;
                        break;

                    case "SERIALNUMBER":
                        nvPair.Value = nvPair.Value.Replace(" ", ""); //get rid any spurious spaces from input, if any
                        cMessage.serialNumber = nvPair.Value;
                        break;

                    case "ORDERNUMBER":
                        nvPair.Value = nvPair.Value.Replace(" ", ""); //get rid any spurious spaces from input, if any
                        cMessage.orderNumber = nvPair.Value;
                        break;

                    default:
                        buildList = false; //unrecognized token; don't trust anything that got done before...
                        break;
                } 
            }

            if (buildList)
            {
                responseList.Add(cMessage);
            }

            return responseList;
        }

    }
}