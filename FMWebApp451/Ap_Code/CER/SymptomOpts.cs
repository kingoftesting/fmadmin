﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CER
{
    public class SymptomOpts
    {
        private int id;
        public int Id { get { return id; } set { id = value; } }

        private int _value;
        public int Value { get { return _value; } set { _value = value; } }

        private string name;
        public string Name { get { return name; } set { name = value; } }

        public SymptomOpts()
        {
            id = 0;
            _value = 0;
            name = "";
        }

        public SymptomOpts(int id)
        {
            string sql = @"
                SELECT * 
                FROM Symptoms 
                WHERE ID = @ID 
            ";

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, id);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, mySqlParameters, AdminCart.Config.ConnStrCER());

            while (dr.Read())
            {
                id = Convert.ToInt32(dr["ID"]);
                _value = Convert.ToInt32(dr["Value"]);
                name = dr["Name"].ToString();
            }
            if (dr != null) { dr.Close(); }

        }

        public static List<SymptomOpts> GetSymptomOptsList()
        {
            List<SymptomOpts> objList = new List<SymptomOpts>();

            string sql = @"
                SELECT * 
                FROM Symptoms 
            ";

            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER());

            while (dr.Read())
            {
                SymptomOpts obj = new SymptomOpts();
                obj.Id = Convert.ToInt32(dr["ID"]);
                obj.Value = Convert.ToInt32(dr["Value"]);
                obj.Name = dr["Name"].ToString();
                objList.Add(obj);
            }
            if (dr != null) { dr.Close(); }

            return objList;
        }

        public static SelectList GetSymptomOptions()
        {
            List<SymptomOpts> objList = SymptomOpts.GetSymptomOptsList();

            // Build a List<SelectListItem>
            var selectListItems = objList.Select(x => new SelectListItem() { Value = x.Value.ToString(), Text = x.Name }).ToList();

            //convert list to SelectList
            SelectList objSelectList = new SelectList(selectListItems, "Value", "Text", "N/A");

            return objSelectList;
        }
    }
}