using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace Certnet
{
    public class Address
    {
        #region Private Variables
        private int addressId;
        private int customerID;
        private string street;
        private string street2;
        private string city;
        private string state;
        private string zip;
        private string country;
        #endregion

        #region Public Properties
        public int AddressId
        {
            get { return addressId; }
            set { addressId = value; }
        }
        public int CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }
        public string Street
        {
            get { return street; }
            set { street = value; }
        }
        public string Street2
        {
            get { return street2; }
            set { street2 = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string State
        {
            get { return state; }
            set { state = value; }
        }
        public string Zip
        {
            get { return zip; }
            set { zip = value; }
        }
        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        #endregion

        #region Constructors
        public Address()
        {
            street = "";
            street2 = "";
            city = "";
            state = "";
            zip = "";
            country = "";
        }
        #endregion

        static public Address LoadAddress(int AddressID)
        {
            Address theAddress = new Address();

            string sql = @"
                SELECT AddressID, CustomerID, Street1, Street2, City, State, Zip, CountryCode 
                FROM Addresses 
                WHERE AddressID = @AddressID";

            SqlParameter[] NamedParameters = new SqlParameter[1];
            NamedParameters[0] = new SqlParameter("@AddressID", AddressID);
            
            SqlDataReader dr = DBUtil.FillDataReader(sql, NamedParameters);

            while (dr.Read())
            {
                theAddress.addressId = dr.GetInt32(0);
                theAddress.street = dr.GetString(2);

                if (dr["Street2"] == DBNull.Value)
                { theAddress.street2 = " "; }
                else
                { theAddress.street2 = dr["Street2"].ToString(); }

                theAddress.city = dr.GetString(4);
                theAddress.state = dr.GetString(5);
                theAddress.zip = dr.GetString(6);
                theAddress.country = dr.GetString(7);
            }
            dr.Close();

            return theAddress;
        }

        static public Address LoadAddress(int customerID, int addressType)
        {
            Address theAddress = new Address();

            string sql = @"
                SELECT AddressID, CustomerID, Street1, Street2, City, State, Zip, CountryCode 
                FROM Addresses 
                WHERE customerid = @CustomerID AND addresstype = @AddressType";

            SqlParameter[] NamedParameters = new SqlParameter[2];
            NamedParameters[0] = new SqlParameter("@CustomerID", customerID);
            NamedParameters[1] = new SqlParameter("@AddressType", addressType);

            SqlDataReader dr = DBUtil.FillDataReader(sql, NamedParameters);

            while (dr.Read())
            {
                theAddress.addressId = dr.GetInt32(0);
                theAddress.street = dr.GetString(2);

                if (dr["Street2"] == DBNull.Value)
                { theAddress.street2 = " "; }
                else
                { theAddress.street2 = dr["Street2"].ToString(); }

                theAddress.city = dr.GetString(4);
                theAddress.state = dr.GetString(5);
                theAddress.zip = dr.GetString(6);
                theAddress.country = dr.GetString(7);
            }
            dr.Close();

            return theAddress;
        }


        public void Save(Customer c, int addressType)
        {
           string sql = "";

           //bypass problem saving billing address
           if (addressId == 0)
                sql = @"SET NOCOUNT ON 
                    IF NOT EXISTS (SELECT * FROM Addresses WHERE CustomerID = @CustomerID AND AddressType = @AddressType)
                    INSERT INTO Addresses ( CustomerID, Street1, Street2, City, State, Zip, CountryCode, AddressType ) 
                    VALUES ( @CustomerID, @Street1, @Street2, @City, @State, @Zip, @CountryCode, @AddressType ) ;  
                    SELECT AddressID = @@identity;";
            else
                sql = @"UPDATE Addresses SET 
                        Street1 = @Street1, 
                        Street2 = @Street2, 
                        City = @City,
                        State = @State,
                        Zip = @Zip,
                        CountryCode = @CountryCode
                    WHERE AddressID = @AddressID";

           SqlConnection cn = new SqlConnection(AdminCart.Config.ConnStr());
            SqlCommand cmd = new SqlCommand(sql, cn);

            SqlParameter AddressIDParam = new SqlParameter("@AddressID", SqlDbType.Int);
            AddressIDParam.Value = addressId;
            cmd.Parameters.Add(AddressIDParam);

            SqlParameter CustomerIDParam = new SqlParameter("@CustomerID", SqlDbType.Int);
            CustomerIDParam.Value = c.CustomerID;
            cmd.Parameters.Add(CustomerIDParam);

            SqlParameter AddressTypeParam = new SqlParameter("@AddressType", SqlDbType.Int);
            AddressTypeParam.Value = addressType;
            cmd.Parameters.Add(AddressTypeParam);

            SqlParameter Street1Param = new SqlParameter("@Street1", SqlDbType.VarChar);
            Street1Param.Value = street;
            cmd.Parameters.Add(Street1Param);

            SqlParameter Street2Param = new SqlParameter("@Street2", SqlDbType.VarChar);
            Street2Param.Value = street2;
            cmd.Parameters.Add(Street2Param);

            SqlParameter CityParam = new SqlParameter("@City", SqlDbType.VarChar);
            CityParam.Value = city;
            cmd.Parameters.Add(CityParam);

            SqlParameter StateParam = new SqlParameter("@State", SqlDbType.VarChar);
            StateParam.Value = state;
            cmd.Parameters.Add(StateParam);

            SqlParameter ZipParam = new SqlParameter("@Zip", SqlDbType.VarChar);
            ZipParam.Value = zip;
            cmd.Parameters.Add(ZipParam);

            SqlParameter CountryCodeParam = new SqlParameter("@CountryCode", SqlDbType.VarChar);
            CountryCodeParam.Value = country;
            cmd.Parameters.Add(CountryCodeParam);
              
            cn.Open();
            if (addressId != 0)
                cmd.ExecuteNonQuery();
            else
            {
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read()) addressId = Convert.ToInt32(dr["AddressID"]);
            }
            cn.Close();
        }
    }
}
