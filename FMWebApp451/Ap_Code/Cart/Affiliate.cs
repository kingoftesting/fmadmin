using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FM2015.Helpers;

/// <summary>
/// Summary description for Affiliate
/// </summary>
namespace Certnet
{
    public class Affiliate
    {
        #region Fields & Properties

	    private int affiliateID;
        public int AffiliateID { get { return affiliateID; } set { affiliateID = value; } }

        //from the google analytic param utm_campaign;
        private string campaign;
        public string Campaign { get { return campaign; } set { campaign = value; } }

        //from the google analytic param utm_source; specifies a specific affiliate (name?)
        private string source;
        public string Source { get { return source; } set { source = value; } }

        //specifies the % spiff that is paid to the affiliate
        private decimal compPercent;
        public decimal CompPercent { get { return compPercent; } set { compPercent = value; } }

        //determines when the affiliate program begins
	    private DateTime startDate;
        public DateTime StartDate { get { return startDate; } set { startDate = value; } }

        //determines when the affiliate program begins
        private DateTime endDate;
        public DateTime EndDate { get { return endDate; } set { endDate = value; } }
	    #endregion
    	
    	
	    #region Constructors
	    public Affiliate()
	    {
            affiliateID = 0;
            campaign = "";
            source = "";
            compPercent = 0;
		    startDate = DateTime.Parse("1/1/1900");
            endDate = DateTime.Parse("1/1/1900");
	    }

        public Affiliate(int id)
        {
            string sql = @" 
                select * 
                from Affiliate 
                where AffiliateID = @AffiliateID 
            ";


            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, id);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read())
            {
                affiliateID = Int32.Parse(dr["AffiliateID"].ToString());
                campaign = dr["Campaign"].ToString();
                source = dr["Source"].ToString();
                compPercent = Decimal.Parse(dr["CompPercent"].ToString());
                startDate = DateTime.Parse(dr["StartDate"].ToString());
                endDate = DateTime.Parse(dr["EndDate"].ToString());
            }
            if (dr != null) dr.Close();
        }

        public Affiliate(string _campaign, string _source)
        {
            string sql = @" 
                select * 
                from Affiliate 
                where Campaign = @Campaign AND Source = @Source  
            ";


            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, _campaign, _source);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read())
            {
                affiliateID = Int32.Parse(dr["AffiliateID"].ToString());
                campaign = dr["Campaign"].ToString();
                source = dr["Source"].ToString();
                compPercent = Decimal.Parse(dr["CompPercent"].ToString());
                startDate = DateTime.Parse(dr["StartDate"].ToString());
                endDate = DateTime.Parse(dr["EndDate"].ToString());
            }
            if (dr != null) dr.Close();
        }
    #endregion
	
	#region Methods
	
        public void Save()
        {
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            if (affiliateID == 0)
            {
                sql = @"
                INSERT INTO Affiliate   
                (Campaign, Source, CompPercent, StartDate, EndDate)
  
                VALUES (@Campaign, @Source, @CompPercent, @StartDate, @EndDate)
  
                SELECT AffiliateID = scope_identity()  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, campaign, source, compPercent, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read()) { AffiliateID = Convert.ToInt32(dr["AffiliateID"]); }
            }
            else
            {

                sql = @"
                UPDATE Affiliate 
                SET 
                    Campaign = @Campaign, 
                    Source = @Source, 
                    CompPercent = @CompPercent, 
                    StartDate = @StartDate, 
                    EndDate = @EndDate 
                WHERE affiliateID = @affiliateID  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, campaign, source, compPercent, startDate, endDate, affiliateID);
                DBUtil.Exec(sql, mySqlParameters);
            }
            if (dr != null) { dr.Close(); }
        }

    /***********************************
    * Static methods
    ************************************/
    /// <summary>
        /// Returns an Affiliate object from an ID
    /// </summary>
        public static Affiliate GetAffiliateByID(int AffiliateID)
    {
        Affiliate obj = new Affiliate(AffiliateID);
        return obj;
    }

    /// <summary>
    /// Creates a new Affiliate
    /// </summary>
        public static int InsertAffiliate(string campaign, string source, decimal compPercent, DateTime startDate, DateTime endDate)
    {
        Affiliate obj = new Affiliate();
        obj.AffiliateID = 0;
        obj.Campaign = campaign;
        obj.Source = source;
        obj.CompPercent = compPercent;
        obj.StartDate = startDate;
        obj.EndDate = endDate;
        obj.Save();
        return obj.AffiliateID;
    }

    /// <summary>
    /// Updates a Affiliate
    /// </summary>
        public static bool UpdateAffiliate(int affiliateID, string campaign, string source, decimal compPercent, DateTime startDate, DateTime endDate)
    {
        Affiliate obj = new Affiliate();
        obj.AffiliateID = affiliateID;
        obj.Campaign = campaign;
        obj.Source = source;
        obj.CompPercent = compPercent;
        obj.StartDate = startDate;
        obj.EndDate = endDate;

        obj.Save();
        return true;
    }

        static public List<Affiliate> GetAffiliateList()
	{
        List<Affiliate> thelist = new List<Affiliate>();

        string sql = @" 
                select * 
                from Affiliate 
            "; 
	    
	    SqlDataReader dr = DBUtil.FillDataReader(sql);
	    
	    while(dr.Read())
	    {
            Affiliate obj = new Affiliate();

            obj.affiliateID = Int32.Parse(dr["AffiliateID"].ToString());
            obj.campaign = dr["Campaign"].ToString();
            obj.source = dr["Source"].ToString();
            obj.compPercent = Decimal.Parse(dr["CompPercent"].ToString());
            obj.startDate = DateTime.Parse(dr["StartDate"].ToString());
            obj.endDate = DateTime.Parse(dr["EndDate"].ToString());

            thelist.Add(obj);
	    }
        if (dr != null) { dr.Close(); }
	    
	    return thelist;
	
	}
	#endregion
    }
}
