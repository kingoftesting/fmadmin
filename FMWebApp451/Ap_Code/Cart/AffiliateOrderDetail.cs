using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FM2015.Helpers;

/// <summary>
/// Summary description for AffiliateOrderDetail
/// </summary>
namespace Certnet
{
    public class AffiliateOrderDetail
    {
        #region Fields & Properties

        private int aOrderDetailID;
        public int AOrderDetailID { get { return aOrderDetailID; } set { aOrderDetailID = value; } }

        private int affiliateID;
        public int AffiliateID { get { return affiliateID; } set { affiliateID = value; } }

        private int orderID;
        public int OrderID { get { return orderID; } set { orderID = value; } }

        //amount of order before tax & shipping; used for comp calc
        private decimal orderTotal;
        public decimal OrderTotal { get { return orderTotal; } set { orderTotal = value; } }

        //determines when the affiliate program begins
	    private DateTime orderDate;
        public DateTime OrderDate { get { return orderDate; } set { orderDate = value; } }

        //complete query string from an affiliate request
        private string queryString;
        public string QueryString { get { return queryString; } set { queryString = value; } }

        //from the google analytic param utm_campaign; for instance, SexyForever
        private string campaign;
        public string Campaign { get { return campaign; } set { campaign = value; } }

        //from the google analytic param utm_medium; for instance, AdWords or Affiliate
        private string medium;
        public string Medium { get { return medium; } set { medium = value; } }

        //from the google analytic param utm_source; specifies a specific affiliate (name?)
        private string source;
        public string Source { get { return source; } set { source = value; } }

        //from the google analytic param utm_content; for instance, an direct marketing email version
        private string content;
        public string Content { get { return content; } set { content = value; } }

        #endregion
    	
    	
	    #region Constructors
	    public AffiliateOrderDetail()
	    {
            aOrderDetailID = 0;
            affiliateID = 0;
            orderID = 0;
            orderTotal = 0;
            orderDate = DateTime.Parse("1/1/1900");
            queryString = "";
            campaign = "";
            medium = "";
            source = "";
            content = "";
	    }

        public AffiliateOrderDetail(int _aOrderDetailID)
        {
            string sql = @" 
                select * 
                from AffiliateOrderDetail 
                where AOrderDetailID = @AOrderDetailID 
            ";


            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, _aOrderDetailID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read())
            {
                aOrderDetailID = Int32.Parse(dr["AOrderDetailID"].ToString());
                affiliateID = Int32.Parse(dr["AffiliateID"].ToString());
                orderID = Int32.Parse(dr["OrderID"].ToString());
                orderTotal = Decimal.Parse(dr["OrderTotal"].ToString());
                orderDate = DateTime.Parse(dr["OrderDate"].ToString());
                queryString = dr["QueryString"].ToString();
                campaign = dr["Campaign"].ToString();
                medium = dr["Medium"].ToString();
                source = dr["Source"].ToString();
                content = dr["Content"].ToString();
            }
            if (dr != null) dr.Close();
        }
    #endregion
	
	#region Methods
	
        public void Save()
        {
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            if (aOrderDetailID == 0)
            {
                sql = @"
                INSERT INTO AffiliateOrderDetail   
                (AffiliateID, OrderID, OrderTotal, OrderDate, QueryString, Campaign, Medium, Source, Content) 
  
                VALUES (@AffiliateID, @OrderID, @OrderTotal, @OrderDate, @QueryString, @Campaign, @Medium, @Source, @Content) 
  
                SELECT AOrderDetailID = scope_identity() 
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, affiliateID, orderID, orderTotal, orderDate, queryString, campaign, medium, source, content);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read()) { aOrderDetailID = Convert.ToInt32(dr["AOrderDetailID"]); }
            }
            else
            {

                sql = @"
                UPDATE AffiliateOrderDetail 
                SET 
                    AffiliateID = @AffiliateID, 
                    OrderID = @OrderID, 
                    OrderTotal = @OrderTotal, 
                    OrderDate = @OrderDate,
                    QueryString = @QueryString, 
                    Campaign = @Campaign, 
                    Medium = @Medium, 
                    Source = @Source, 
                    Content = @Content 
                WHERE AOrderDetailID = @AOrderDetailID  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, affiliateID, orderID, orderTotal, orderDate, queryString, campaign, medium, source, content, aOrderDetailID);
                DBUtil.Exec(sql, mySqlParameters);
            }
            if (dr != null) { dr.Close(); }
        }

        /***********************************
        * Static methods
        ************************************/
        /// <summary>
            /// Returns an AffiliateOrderDetail object from an ID
        /// </summary>
            public static AffiliateOrderDetail GetAffiliateOrderDetailByID(int AOrderDetailID)
        {
            AffiliateOrderDetail obj = new AffiliateOrderDetail(AOrderDetailID);
            return obj;
        }

        /// <summary>
        /// Creates a new AffiliateOrderDetail
        /// </summary>
            public static int InsertAffiliateOrderDetail(int AffiliateID, int OrderID, int OrderTotal, DateTime OrderDate, string QueryString, string Campaign, string Medium, string Source, string Content)
        {
            AffiliateOrderDetail obj = new AffiliateOrderDetail();
            obj.AOrderDetailID = 0;
            obj.AffiliateID = AffiliateID;
            obj.OrderID = OrderID;
            obj.OrderTotal = OrderTotal;
            obj.OrderDate = OrderDate;
            obj.QueryString = QueryString;
            obj.Campaign = Campaign;
            obj.Medium = Medium;
            obj.Source = Source;
            obj.Content = Content;
            obj.Save();
            return obj.AffiliateID;
        }

        /// <summary>
        /// Updates a AffiliateOrderDetail
        /// </summary>
        public static bool UpdateAffiliateOrderDetail(int AOrderDetailID, int AffiliateID, int OrderID, int OrderTotal, DateTime OrderDate, string QueryString, string Campaign, string Medium, string Source, string Content)
        {
            AffiliateOrderDetail obj = new AffiliateOrderDetail();
            obj.AOrderDetailID = AOrderDetailID;
            obj.AffiliateID = AffiliateID;
            obj.OrderID = OrderID;
            obj.OrderTotal = OrderTotal;
            obj.OrderDate = OrderDate;
            obj.QueryString = QueryString;
            obj.Campaign = Campaign;
            obj.Medium = Medium;
            obj.Source = Source;
            obj.Content = Content;

            obj.Save();
            return true;
        }

        static public List<AffiliateOrderDetail> GetAffiliateOrderDetailList()
	    {
            List<AffiliateOrderDetail> thelist = new List<AffiliateOrderDetail>();

            string sql = @" 
                    select * 
                    from AffiliateOrderDetail 
                    order by OrderDate DESC 
                "; 
    	    
	        SqlDataReader dr = DBUtil.FillDataReader(sql);
    	    
	        while(dr.Read())
	        {
                AffiliateOrderDetail obj = new AffiliateOrderDetail();

                obj.aOrderDetailID = Int32.Parse(dr["AOrderDetailID"].ToString());
                obj.affiliateID = Int32.Parse(dr["AffiliateID"].ToString());
                obj.orderID = Int32.Parse(dr["OrderID"].ToString());
                obj.orderTotal = Decimal.Parse(dr["OrderTotal"].ToString());
                obj.orderDate = DateTime.Parse(dr["OrderDate"].ToString());
                obj.queryString = dr["QueryString"].ToString();
                obj.campaign = dr["Campaign"].ToString();
                obj.medium = dr["Medium"].ToString();
                obj.source = dr["Source"].ToString();
                obj.content = dr["Content"].ToString();

                thelist.Add(obj);
	        }
            if (dr != null) { dr.Close(); }
    	    
	        return thelist;
    	
	    }

        static public List<AffiliateOrderDetail> GetAffiliateOrderDetailList(int AffiliateID)
        {
            List<AffiliateOrderDetail> thelist = new List<AffiliateOrderDetail>();

            string sql = @" 
                select * 
                from AffiliateOrderDetail 
                where AffiliateID = @AffiliateID 
                order by OrderDate DESC 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, AffiliateID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                AffiliateOrderDetail obj = new AffiliateOrderDetail();

                obj.aOrderDetailID = Int32.Parse(dr["AOrderDetailID"].ToString());
                obj.affiliateID = Int32.Parse(dr["AffiliateID"].ToString());
                obj.orderID = Int32.Parse(dr["OrderID"].ToString());
                obj.orderTotal = Decimal.Parse(dr["OrderTotal"].ToString());
                obj.orderDate = DateTime.Parse(dr["OrderDate"].ToString());
                obj.queryString = dr["QueryString"].ToString();
                obj.campaign = dr["Campaign"].ToString();
                obj.medium = dr["Medium"].ToString();
                obj.source = dr["Source"].ToString();
                obj.content = dr["Content"].ToString();

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }

            return thelist;

        }

        static public List<AffiliateOrderDetail> GetAffiliateOrderDetailList(int AffiliateID, DateTime From, DateTime To)
        {
            List<AffiliateOrderDetail> thelist = new List<AffiliateOrderDetail>();

            string sql = @" 
                select * 
                from AffiliateOrderDetail 
                where AffiliateID = @AffiliateID and OrderDate between @from and @to 
                order by OrderDate DESC 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, AffiliateID, From, To);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                AffiliateOrderDetail obj = new AffiliateOrderDetail();

                obj.aOrderDetailID = Int32.Parse(dr["AOrderDetailID"].ToString());
                obj.affiliateID = Int32.Parse(dr["AffiliateID"].ToString());
                obj.orderID = Int32.Parse(dr["OrderID"].ToString());
                obj.orderTotal = Decimal.Parse(dr["OrderTotal"].ToString());
                obj.orderDate = DateTime.Parse(dr["OrderDate"].ToString());
                obj.queryString = dr["QueryString"].ToString();
                obj.campaign = dr["Campaign"].ToString();
                obj.medium = dr["Medium"].ToString();
                obj.source = dr["Source"].ToString();
                obj.content = dr["Content"].ToString();

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }

            return thelist;

        }
	    #endregion
    }
}
