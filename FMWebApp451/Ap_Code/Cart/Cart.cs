using System;
using System.Data;
using System.Web;
using PayPal.Payments.Common;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;

using System.Data.SqlClient;
using FM2015.Helpers;


namespace Certnet
{
    public class Cart
    {

        #region Member Classes and Collections
        //Member classes
        public Customer SiteCustomer;
        public CartItems Items;
        public Address BillAddress;
        public Address ShipAddress;
        public Order SiteOrder;
        public OrderPayments Payments;
        public ProductItems Products;
        public Transaction Tran;

        public Coupon SiteCoupon;

        public int coupon;
        #endregion

        #region Members
        private Guid cartID;
        private string cartStatus;

        private int orderID;
        private int affiliateOrderID;
        private decimal totalProduct;
        private decimal totalTax;
        private decimal totalShipping;
        private decimal totalDiscounts;
        private decimal total;
        private int shipType;
        private decimal groundShipping;
        private decimal expeditedShipping;
        private decimal expressShipping;
        private decimal csShipping;
        private string cartMode;
        #endregion

        #region Properties
        public int AffiliateOrderID
        {
            get { return affiliateOrderID; }
            set { affiliateOrderID = value; }
        }
        public int OrderID
        {
            get { return orderID; }
            set { orderID = value; }
        }
        public decimal TotalProduct
        {
            get{ return totalProduct;}
            set{ totalProduct=value;}
        }
        public decimal TotalTax
        {
            get{ return totalTax;}
            set{ totalTax=value;}
        }
        public decimal TotalShipping
        {
            get{ return totalShipping;}
            set{ totalShipping =value;}
        }
        public decimal TotalDiscounts
        {
            get{ return totalDiscounts;}
            set{ totalDiscounts=value;}
        }
        public decimal Total
        {
            get { return total; }
            set { total = value; }
        }
        public Guid CartID
        {
            get { return cartID; }
            set { cartID = value; }
        }
        public string CartStatus
        {
            get { return cartStatus; }
            set { cartStatus = value; }
        }
        public int ShipType
        {
            get { return shipType; }
            set { shipType = value; }
        }


        public decimal CSShipping { get { return csShipping; } set { csShipping = value; } }
        public decimal GroundShipping { get { return groundShipping; } set { groundShipping = value; } }
        public decimal ExpeditedShipping { get { return expeditedShipping; } set { expeditedShipping = value; } }
        public decimal ExpressShipping { get { return expressShipping; } set { expressShipping = value; } }

        public string CartMode
        {
            get { return cartMode; }
            set { cartMode = value; }
        }

        
     #endregion


        public Cart()
        {
            orderID = 0;
            affiliateOrderID = 0;
            totalProduct = 0;
            totalTax = 0;
            totalShipping = 0;
            csShipping = -1;
            totalDiscounts = 0;
            total = 0;

            cartID = Guid.NewGuid();
            SiteCustomer = new Customer();
            Items = new CartItems();
            BillAddress = new Address();
            ShipAddress = new Address();
            SiteOrder = new Order();
            //Products = new ProductItems();

            SiteCoupon = new Coupon();

            Tran = new Transaction();

            ShipType = 1;
            coupon = 0;
            cartMode = AdminCart.Config.CartMode;
        }

        public Cart GetCartFromOrder(int _OrderID)
        {
            Cart c = new Cart();
            c.SiteOrder = new Certnet.Order(_OrderID);
            c.SiteCustomer = new Customer(c.SiteOrder.CustomerID);
            c.ShipAddress = Address.LoadAddress(c.SiteOrder.ShippingAddressID);
            c.BillAddress = Address.LoadAddress(c.SiteOrder.BillingAddressID);
            
            c.Tran = Transaction.GetTransaction(_OrderID);

            c.orderID = _OrderID;
            c.Total = c.SiteOrder.Total;
            c.TotalProduct = c.SiteOrder.SubTotal;
            c.TotalShipping = c.SiteOrder.TotalShipping;
            c.TotalTax = c.SiteOrder.TotalTax;
            c.TotalDiscounts = c.SiteOrder.TotalCoupon;
            c.shipType = 1;

            //load items from actual items sold
            foreach(OrderDetail od in OrderDetail.GetOrderDetails(c.orderID))
            {
                Item i = new Item(od.ProductID, od.Quantity);

                i.LineItemTotal = i.Quantity * (od.UnitPrice - i.LineItemAdjustment);

                c.Items.AddItem(i);
            }

            c.SiteCoupon.CouponCodeID = c.SiteCoupon.GetUsageID(c.orderID);
            if (c.SiteCoupon.CouponCodeID != 0) { c.SiteCoupon = new Coupon(c.SiteCoupon.CouponCodeID); }

            c.CalculateTotals();
            return c;
        }

        public void Load(object mycart)
        {
            //Get Cart From session data
            //this = mycart;
            if (mycart == null)
            {
                //add new cart database entry, set cartid value
                cartID = Guid.NewGuid();
                cartStatus = "newly created.";
            }
            else
            {
                Cart tempcart = (Cart)mycart;
                //object o2 = Convert.ChangeType(o1, Type.GetType(sType)); 
                //mycart = Convert.ChangeType(mycart, Type.GetType("Cart"));
                //load card data from db
                this.cartStatus = "reloaded";
                this.cartID= tempcart.CartID;
                //this.cartStatus = tempcart.CartStatus;
                this.SiteCustomer= tempcart.SiteCustomer;
                this.BillAddress = tempcart.BillAddress;
                this.ShipAddress = tempcart.ShipAddress;

                this.Items = tempcart.Items;
                this.SiteCustomer = tempcart.SiteCustomer;
                this.SiteOrder = tempcart.SiteOrder;
                this.ShipAddress = tempcart.ShipAddress;
                this.BillAddress = tempcart.BillAddress;
                this.SiteCoupon = tempcart.SiteCoupon;

                this.ShipType = tempcart.ShipType;
                this.OrderID = tempcart.OrderID;
                this.AffiliateOrderID = tempcart.AffiliateOrderID;

                this.TotalDiscounts = tempcart.TotalDiscounts;
                this.CSShipping = tempcart.CSShipping;
                this.TotalShipping = tempcart.TotalShipping;
                this.TotalProduct = tempcart.TotalProduct;
                this.TotalTax = tempcart.TotalTax;
                this.Total = tempcart.Total;
                
                this.Tran = tempcart.Tran;

                this.cartMode = tempcart.CartMode;
            
            }
        }

        public void CalculateTotals()
        {
            CalculateProductTotal();
            CalculateDiscounts();
            CalculateShipping();
            CalculateTax();
            CalculateTotal();
        }

        private void CalculateProductTotal()
        {
            totalProduct = 0;
            SiteOrder.Adjust = 0;
            foreach (Item i in Items)
            {
                    totalProduct += i.LineItemTotal;
                    //SiteOrder.Adjust += i.LineItemAdjustment;
                    SiteOrder.Adjust += (i.Quantity * i.LineItemDiscount);
                }
        }

        private void CalculateShipping()
        {
            //DEFAULT VALUES
            TotalShipping = 0;

            groundShipping = 0;
            expeditedShipping = 0;
            expressShipping = 0;

            bool NoShipping = true; //assume there is no s&h for any product, anywhere

            decimal expeditedUpcharge = 8;
            decimal expressUpcharge = 16;

            
            if (ShipAddress.Country == "")
            {
                groundShipping = 0;
            } 
            
            else //assume country is US and calculate shipping
            {
                foreach (Item i in Items)
                {
                    int pid = i.LineItemProduct.ProductID;
                    switch (pid)
                    {
                        case 12:
                            groundShipping += 0; //Purple x-waranty shipping = $0 everywhere
                            break;

                        case 19:
                            groundShipping += 0; //Platinum x-waranty shipping = $0 everywhere
                            break;

                        default:
                            NoShipping = false; //OK, now we have s&h $$ to be calculated
                            if (i.LineItemProduct.PerUnitShipping != 0)
                            {
                                groundShipping += i.LineItemProduct.PerUnitShipping * i.Quantity;
                            }
                            else
                            {
                                groundShipping += .1M * (i.Quantity * i.LineItemProduct.SalePrice);
                            };
                            break;
                    }
                }


                //adjust for minshipping . for now exclude fm unit & x-warranty

                bool MinShippingProductTrigger = false;

                foreach (Item i in Items)
                {
                    int pid = i.LineItemProduct.ProductID;
                    switch (pid)
                    {
                        case 1: //no min shipping for FM
                            break;

                        case 12: //no min shipping for purple x-warranty
                            break;

                        case 13: //no min shipping for FM+serum
                            MinShippingProductTrigger = false; 
                            break;

                        case 14: //no min shipping for Platinum
                            MinShippingProductTrigger = false;
                            break;

                        case 19: //no min shipping for platinum x-warranty
                            break;

                        default: 
                            MinShippingProductTrigger = true;
                            break;
                    }
                }

                if (groundShipping < AdminCart.Config.MinimumShipping && MinShippingProductTrigger)
                {
                    groundShipping = AdminCart.Config.MinimumShipping;
                }

            }

            string cid = ShipAddress.Country;

            if (!NoShipping)
            {
                switch (cid)
                {
                    case "":
                        groundShipping = 0; //unknown country
                        ShipType = 1; //groundShipping if unknown country
                        TotalShipping = groundShipping;
                        break;

                    case "CA":
                        groundShipping += 6; //Canada = US + $6; except x-warranty
                        ShipType = 1; // only ground to Canada
                        TotalShipping = groundShipping;
                        break;

                    case "US":
                        groundShipping += 0; //just leave the calc alone
                        //adjust for free shipping
                        if (HttpContext.Current.Session["sset"] != null) //first, check for any site specials
                        {
                            int ssetID = Convert.ToInt32(HttpContext.Current.Session["sset"].ToString());
                            AdminCart.SiteSettings sset = new AdminCart.SiteSettings(ssetID);
                            if (sset.SiteSettingsID > 0) //make sure it's legit
                            {
                                if (sset.SiteSettingsFreeShipping && (TotalProduct >= sset.SiteSettingsMinProduct))
                                { 
                                    groundShipping = 0;
                                    if (SiteOrder.SiteSettingsID <= 0) { SiteOrder.SiteSettingsID = sset.SiteSettingsID; }
                                }
                            }
                        }

                        //now check for global free shipping
                        string sql = @"
                        select * 
                        from sitesettings 
                        where SiteSettingsName = 'FreeShipping' 
                        and getdate() between SiteSettingsStartDate and SiteSettingsEndDate";
                        SqlDataReader dr = DBUtil.FillDataReader(sql);
                        if (dr != null)
                        {
                            while (dr.Read())
                            {
                                decimal minProduct = 0;
                                int ssetID = 0;
                                bool isID = int.TryParse(dr["siteSettingsID"].ToString(), out ssetID);
                                bool isMin = Decimal.TryParse(dr["siteSettingsMinProduct"].ToString(), out minProduct);
                                if (TotalProduct >= minProduct)
                                {
                                    groundShipping = 0;
                                    if (SiteOrder.SiteSettingsID <= 0) { SiteOrder.SiteSettingsID = ssetID; }
                                }
                            }
                        }
                        if (dr != null) { dr.Close(); }

                        //set total based on type

                        if (shipType == 1)
                        {
                            TotalShipping = GroundShipping;
                        }
                        if (ShipType == 2)
                        {
                            TotalShipping = GroundShipping + expeditedUpcharge;
                        }
                        if (ShipType == 3)
                        {
                            TotalShipping = GroundShipping + expressUpcharge;
                        }

                        break;

                    default:
                        groundShipping = 40; //must be non-US and non-CA
                        shipType = 1; //only Ground is available!
                        TotalShipping = groundShipping;
                        break;
                }
            }

            //overide if CS has decided to charge an arbitrary shipping value
            if (CSShipping < 0)
            { }
            else { TotalShipping = Convert.ToDecimal(CSShipping); } 
        }

        //note: extended warranty is taxable as a MANDATORY WARRANTY (available only from FM)
        private void CalculateTax()
        {
            totalTax = 0;
            if (this.ShipAddress.State == "CA")
            {
                /*
                foreach (Item i in Items)
                {
                    i.LineItemTax =  i.LineItemTotal * .0875M;
                    totalTax += i.LineItemTax;
                }
                */

                totalTax = (totalProduct - totalDiscounts) * .0900M;
                
            }
        }

        private void CalculateTotal()
        {
            total = totalProduct - totalDiscounts + totalTax + totalShipping;
        }

        private void CalculateDiscounts()
        {
            totalDiscounts = 0;

            if (SiteCoupon.CouponCodeID != 0)
            {
                if (totalProduct >= SiteCoupon.CouponOrderMinimum)
                {
                    TotalDiscounts = (Decimal.Parse(SiteCoupon.CouponAmount) / 100) * totalProduct;
                }
            }
            else
            {
                //now check for a site discount
                string sql = @"
                        select * 
                        from sitesettings 
                        where ((SiteSettingsName = 'FreeShipping') or (SiteSettingsName = 'siteDiscount')) 
                        and getdate() between SiteSettingsStartDate and SiteSettingsEndDate";
                SqlDataReader dr = DBUtil.FillDataReader(sql);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        int ssID = Convert.ToInt32(dr["SiteSettingsID"]);
                        AdminCart.SiteSettings siteSetting = new AdminCart.SiteSettings(ssID);
                        if (TotalProduct >= siteSetting.SiteSettingsMinProduct)
                        {
                            switch (siteSetting.SiteSettingsTrigger)
                            {
                                case "0":
                                    break;

                                case "1":
                                    totalDiscounts = totalProduct - Convert.ToDecimal(siteSetting.SiteSettingsValue);
                                    break;

                                case "2":
                                    totalDiscounts = totalProduct * (Convert.ToDecimal(siteSetting.SiteSettingsValue) / 100);
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                    dr.Close();
                }
            }
        }

        public string GetVersion()
        {
            return "Certnet/AdminCart Cart Version 4.0.0.0";
        }

        public int Charge()
        {
            int Result = -1;
            //process live

            UserInfo User = new UserInfo(AdminCart.Config.PayPalUser, AdminCart.Config.PayPalVendor,
                AdminCart.Config.PayPalPartner, AdminCart.Config.PayPalPassword);

            string server = AdminCart.Config.PayPalServer;
            if (AdminCart.Config.CartSetting == "Test")
            {
                server = AdminCart.Config.PayPalTestServer;
            }

            Invoice Inv = new Invoice();

            // Set the amount object.

            Currency Amt = new Currency(SiteOrder.Total, "840"); // 840 is US ISO currency code.  If no code passed, 840 is default.
            Amt.NoOfDecimalDigits = 2;
            Amt.Round = true;
            Inv.Amt = Amt;

            //Inv.InvNum = theOrderID.ToString();
            //Inv.CustRef = theOrderID.ToString();
            Inv.Comment2 = this.OrderID.ToString();

            // Create the BillTo object.
            BillTo Bill = new BillTo();

            Bill.FirstName = SiteCustomer.FirstName; //"Joe";
            Bill.LastName = SiteCustomer.LastName; //"Smith";
            Bill.Street = BillAddress.Street;// "123 Main St.";
            Bill.City = BillAddress.City; ;
            Bill.State = BillAddress.State;
            Bill.Zip = BillAddress.Zip;
            Bill.BillToCountry = BillAddress.Country;

            Bill.PhoneNum = SiteCustomer.Phone;
            Bill.Email = SiteCustomer.Email; //"Joe.Smith@anyemail.com";

            // Set the BillTo object into invoice.
            Inv.BillTo = Bill;

            ShipTo Ship = new ShipTo();

            // If billing and shipping details are the same, uncomment the following code.
            Ship = Bill.Copy();
            Inv.ShipTo = Ship;

            CreditCard CC = new CreditCard(Tran.CardNo, Tran.ExpDate);

            CC.Cvv2 = Tran.CvsCode;

            CardTender Card = new CardTender(CC);

            //Using the PayPal SDK example
            string HostAddress = server;
            int HostPort = 443;
            int Timeout = 30;

            //PayflowConnectionData Connection = new PayflowConnectionData(server, 443, 45, "", 0, "", "", @"c:\program files\Payflow SDK for .Net\certs");
            PayflowConnectionData Connection = new PayflowConnectionData(HostAddress, HostPort, Timeout);

            // Create a new Sale Transaction.
            SaleTransaction Trans = new SaleTransaction(
                User, Connection, Inv, Card, PayflowUtility.RequestId);

            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                Result = TrxnResponse.Result;

                string RespMsg = TrxnResponse.RespMsg;

                if (TrxnResponse != null)
                {
                    Tran.TransactionAmount = Total;
                    Tran.Comment2 = OrderID.ToString();
                    Tran.TrxType = "S";
                    Tran.CardType = Tran.GetCardTypeByNumber(Tran.CardNo);

                    Tran.OrderID = OrderID;

                    Tran.CardHolder = SiteCustomer.FirstName + ' ' + SiteCustomer.LastName;
                    Tran.CustomerCode = "FM" + SiteCustomer.CustomerID;
                    Tran.Address = BillAddress;


                    Tran.ResultCode = TrxnResponse.Result;
                    Tran.AuthCode = TrxnResponse.AuthCode;
                    Tran.ResultMsg = TrxnResponse.RespMsg;

                    if (string.IsNullOrEmpty(TrxnResponse.Pnref))
                    {
                        Tran.TransactionID = "Pnref-null";
                        Tran.ResultMsg = "Please try again. " + Tran.ResultMsg; ;
                    }
                    else { Tran.TransactionID = TrxnResponse.Pnref; }

                    Tran.TransactionDate = DateTime.Now;

                    Tran.AvsAddr = TrxnResponse.AVSAddr;
                    Tran.AvsZip = TrxnResponse.AVSZip;
                    Tran.Iavs = TrxnResponse.IAVS;

                    Tran.SaveDB();
                }

                // Get the Transaction Context
                Context TransCtx = Resp.TransactionContext;

                switch (Result)
                {
                    case 0:
                        RespMsg = "Your transaction was approved.";
                        break;
                    case 1: // Transaction Not Completed.
                        RespMsg = "There was an error processing your transaction.  Please contact Customer Service." + Environment.NewLine + "Error: " + Result;
                        break;
                    case 12:
                        RespMsg = "Your transaction was declined.";
                        break;
                    case 13:
                        RespMsg = "Your transaction was declined.";
                        break;
                    case 125:
                        RespMsg = "Your Transactions has been declined. Contact Customer Service.";
                        break;
                    case 126:
                        // Decline transaction if AVS fails.
                        if (TrxnResponse.AVSAddr != "Y" | TrxnResponse.AVSZip != "Y")
                            // Display message that transaction was not accepted.  At this time, you
                            // could redirect user to re-enter STREET and ZIP information.  
                            RespMsg = "Your Billing Information does not match.  Please re-enter.";
                        else
                            RespMsg = "Your Transactions is Under Review. We will notify you via e-mail if accepted.";
                        break;
                    case 127:
                        RespMsg = "Your Transactions is Under Review. We will notify you via e-mail if accepted.";
                        break;
                    default:
                        // Get the Transaction Context and check for any contained SDK specific errors (optional code).
                        if (TransCtx != null & TransCtx.getErrorCount() > 0)
                        {
                            Mail.SendMail(TransCtx.ToString());
                        }
                        //label1.Text += Environment.NewLine + "Transaction Errors = " + TransCtx.ToString();
                        RespMsg = "An unknown error has occured with the credit card processor. Please try again";
                        break;
                }


                // Check the Transaction Context for any contained SDK specific errors (optional code).
                if (TransCtx != null && TransCtx.getErrorCount() > 0)
                {
                    //  label1.Text+="TRANSACTION CONTEXT ERRORS";
                    //  label1.Text+=TransCtx.ToString();
                    //  label1.Text +="------------------------------------------------------";
                }

                return Result;
            }
            else
            {
                return -99;
            }
        }

        public void SaveToOrder()
        {
            SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStr());
            string sql = @"INSERT Orders(CartID, CustomerID, OrderDate, Total, Adjust, 
                    TotalShipping, TotalTax,TotalCoupon,Status,
                    ShippingAddressID, BillingAddressID, SourceID, Phone, SuzanneSomersOrderID, 
                    ShipMode, AffiliateOrderDetailID, SiteSettingsID)
                   VALUES(@CartID, @CustomerID, @OrderDate, @Total, @Adjust,
                    @TotalShipping, @TotalTax, @TotalCoupon, 'New',
                    @ShipAddressID, @BillAddressID, -1, 0, 0, 1, @AffiliateOrderDetailID, @SiteSettingsID);  select OrderID=@@identity;";

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlParameter CartIDParam = new SqlParameter("@CartID", SqlDbType.UniqueIdentifier);
            SqlParameter CustomerIDParam = new SqlParameter("@CustomerID", SqlDbType.Int);
            SqlParameter OrderDateParam = new SqlParameter("@OrderDate", SqlDbType.DateTime);
            SqlParameter TotalParam = new SqlParameter("@Total", SqlDbType.Money);

            SqlParameter TotalShippingParam = new SqlParameter("@TotalShipping", SqlDbType.Money);
            SqlParameter TotalTaxParam = new SqlParameter("@TotalTax", SqlDbType.Money);
            SqlParameter TotalCouponParam = new SqlParameter("@TotalCoupon", SqlDbType.Money);
            SqlParameter AdjustParam = new SqlParameter("@Adjust", SqlDbType.Money);
            SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.VarChar);
            SqlParameter ShipAddressIDParam = new SqlParameter("@ShipAddressID", SqlDbType.Int);
            SqlParameter BillAddressIDParam = new SqlParameter("@BillAddressID", SqlDbType.Int);
            SqlParameter AffiliateOrderDetailIDParam = new SqlParameter("@AffiliateOrderDetailID", SqlDbType.Int);
            SqlParameter SiteSettingsIDParam = new SqlParameter("@SiteSettingsID", SqlDbType.Int);

            CartIDParam.Value = CartID;
            CustomerIDParam.Value = SiteCustomer.CustomerID; 
            OrderDateParam.Value = DateTime.Now;
            TotalParam.Value = Total;

            TotalShippingParam.Value = TotalShipping;
            TotalTaxParam.Value = TotalTax;
            TotalCouponParam.Value = TotalDiscounts;
            AdjustParam.Value = SiteOrder.Adjust;

            StatusParam.Value = "New";
            ShipAddressIDParam.Value = SiteOrder.ShippingAddressID;
            BillAddressIDParam.Value = SiteOrder.BillingAddressID;
            AffiliateOrderDetailIDParam.Value = SiteOrder.AffiliateOrderDetailID;
            SiteSettingsIDParam.Value = SiteOrder.SiteSettingsID;

            cmd.Parameters.Add(CartIDParam);
            cmd.Parameters.Add(CustomerIDParam);
            cmd.Parameters.Add(OrderDateParam);
            cmd.Parameters.Add(TotalParam);
            cmd.Parameters.Add(TotalShippingParam);
            cmd.Parameters.Add(TotalTaxParam);
            cmd.Parameters.Add(TotalCouponParam);
            cmd.Parameters.Add(AdjustParam);
            cmd.Parameters.Add(StatusParam);
            cmd.Parameters.Add(ShipAddressIDParam);
            cmd.Parameters.Add(BillAddressIDParam);
            cmd.Parameters.Add(AffiliateOrderDetailIDParam);
            cmd.Parameters.Add(SiteSettingsIDParam);
            conn.Open();
            int result = 0;

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                result = Convert.ToInt32(dr["OrderID"]);
            }
            else
            {
                result = -99;
            }
            OrderID = result;

            //next, save all order details
            foreach (Item i in Items) 
            {
                OrderDetail od = new OrderDetail();
                od.OrderID = OrderID;
                od.ProductID = i.LineItemProduct.ProductID;

                //od.UnitPrice = i.LineItemProduct.SalePrice;
                od.UnitPrice = i.LineItemProduct.Price;

                //od.Discount = i.LineItemAdjustment;
                od.Discount = (i.LineItemProduct.Price - i.LineItemProduct.SalePrice) + i.LineItemAdjustment;

                AdminCart.ProductCost pcost = new AdminCart.ProductCost(i.LineItemProduct.ProductID);
                od.UnitCost = pcost.Cost;

                od.Quantity = i.Quantity;
                od.SaveToOrderDetail();
            }
        }

        public void Clear()
        {
            Items = null;
        }
    }
}
