using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;


namespace Certnet
{
    public class CartItems: IEnumerable
    {
        private ArrayList items;

        public CartItems()
        {
            items = new ArrayList();
        }

        public Item CreateItem()
        {
            return new Item();
        }

        public void AddItem(Item item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
            {
                IncrementItem(item);
            }
            else
            {

                items.Add(item);
                
            }
        }
        public void UpdateItem(Item item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
            {
                Item currentitem = items[index] as Item;
                currentitem.Quantity = item.Quantity;
            }
        }
        public void IncrementItem(Item item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
            {
                Item currentitem = items[index] as Item;
                currentitem.Quantity += 1;
                currentitem.LineItemTotal = currentitem.Quantity * currentitem.LineItemProduct.SalePrice;
            }
        }

        public void DeleteItem(Item item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
                items.RemoveAt(index);
            else
                throw new ArgumentException(
                    String.Format("Item = {0} is not in the shoppingcart.", item));

        }
        public int ItemCount()
        {
            int thecount = 0;
            foreach(Item i in items)
            {
                thecount++;
            }
            return thecount;
        }
        public decimal ItemSubTotal()
        {
            decimal subtotal = 0;
            foreach (Item i in items)
            {
                subtotal += i.LineItemTotal;
            }
            return subtotal;
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return (items as IEnumerable).GetEnumerator();
        }

        #endregion

        
    }
}
