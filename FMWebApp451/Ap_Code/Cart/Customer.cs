using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace Certnet
{
    public class Customer
    {
        #region private Attributes
        private int customerID;
        private string firstName;
        private string lastName;
        private string email;
        private string password;
        private string phone;
        private DateTime createDate;
        private DateTime lastUpdateDate;
        private DateTime lastLoginDate;
        private int siteID;
        private string referrerDomain;
        private string inBoundQuery;
        private bool faceMasterNewsletter;
        private bool suzanneSomersNewsletter;
        private int suzanneSomersCustomerID;
        #endregion

        #region public Properties
        public int CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        public DateTime CreateDate 
        {
            get { return createDate; }
            set { createDate = value; }
        }

        public int SiteID
        {
            get { return siteID; }
            set { siteID = value; }
        }
        public string ReferrerDomain
        {
            get { return referrerDomain; }
            set { referrerDomain = value; }
        }
        public string InBoundQuery
        {
            get { return inBoundQuery; }
            set { inBoundQuery = value; }
        }
        
        public bool FaceMasterNewsletter
        {
            get { return faceMasterNewsletter; }
            set { faceMasterNewsletter = value; }
        }

        public bool SuzanneSomersNewsletter
        {
            get { return suzanneSomersNewsletter; }
            set { suzanneSomersNewsletter = value; }
        }

        public int SuzanneSomersCustomerID
        {
            get { return suzanneSomersCustomerID; }
            set { suzanneSomersCustomerID = value; }
        }  
        
        #endregion

        public Customer()
        {
            customerID = 0;
            firstName = "";
            lastName = "";
            email = "";
            password = "";
            phone = "";
            createDate = DateTime.Now;
            lastUpdateDate = DateTime.Now;
            lastLoginDate = DateTime.Now;
            siteID = 1;
            referrerDomain = "";
            inBoundQuery = "";
            faceMasterNewsletter = false;
            suzanneSomersNewsletter = false;
            suzanneSomersCustomerID = -1;

        }

        public Customer(int CustomerID)
        {
            customerID = CustomerID;
            LoadCustomerFromDB(customerID);
             
        }

        public int Login(string email, string password)
        {
            string sql = @"SELECT * FROM Customers 
                          WHERE Email = @email and Password = @password";
            SqlConnection cn = new SqlConnection(AdminCart.Config.ConnStr());
            cn.Open();
            SqlCommand cmd = new SqlCommand(sql, cn);

            SqlParameter emailParm = new SqlParameter("@email", SqlDbType.NVarChar, 50);
            emailParm.Value = email;
            SqlParameter passwordParm = new SqlParameter("@password", SqlDbType.NVarChar, 50);
            passwordParm.Value = password;

            cmd.Parameters.Add(emailParm);
            cmd.Parameters.Add(passwordParm);

            SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            while(dr.Read()){
                customerID = Convert.ToInt32(dr["CustomerID"].ToString());
                email = dr["email"].ToString();
                firstName = dr["firstname"].ToString();
                lastName = dr["lastname"].ToString();
                suzanneSomersCustomerID = Convert.ToInt32(dr["SuzanneSomerscustomerID"].ToString());
                UpdateLastLogin(customerID);
            }
            dr.Close();
            cn.Close();
            
            return customerID;

        }

        private void LoadCustomerFromDB(int CustomerID)
        {
            string sql = @"
                SELECT * 
                FROM Customers 
                WHERE CustomerID = @CustomerID";

            //load and set parameter collection
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@CustomerID", CustomerID);
            SqlDataReader dr = null;

            try
            {
                dr = DBUtil.FillDataReader(sql, sqlParameters);
                dr.Read();
                this.customerID = Int32.Parse(dr["CustomerID"].ToString());
                this.firstName = dr["FirstName"].ToString();
                this.lastName = dr["LastName"].ToString();
                
                this.email = dr["Email"].ToString();
                this.password = dr["Password"].ToString();
                this.phone = dr["phone"].ToString();

                this.createDate = DateTime.Parse(dr["CreateDate"].ToString());
                this.lastUpdateDate = DateTime.Parse(dr["LastUpdateDate"].ToString());
                this.lastLoginDate = DateTime.Parse(dr["LastLoginDate"].ToString());

                this.siteID = Int32.Parse(dr["SiteID"].ToString());
                this.referrerDomain = dr["ReferrerDomain"].ToString();
                this.inBoundQuery = dr["inBoundQuery"].ToString();

                this.suzanneSomersCustomerID = Convert.ToInt32(dr["suzannesomerscustomerid"].ToString());

                if (dr["FaceMasterNewsletter"].ToString() == "True") this.faceMasterNewsletter = true; else this.faceMasterNewsletter = false;
                if (dr["SuzanneSomersNewsletter"].ToString() == "True") this.suzanneSomersNewsletter = true; else this.suzanneSomersNewsletter = false;
                
            }
            catch (Exception ex)
            {
                Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "DBUtil Customer Exception", ex.Message + ex.Source + ex.StackTrace);
                throw ex;
            }
            finally
            {
                dr.Close();
            }

        }


        public void SaveCustomerToDB()
        {
            string sql = "";
            
            if (customerID == 0)
                sql = @"
                    SET NOCOUNT ON 
                    
                    INSERT Customers( FirstName, LastName, Email, Phone, Password, 
                        Company, [Group], CreateDate, LastLoginDate, LastUpdateDate, 
                        FaceMasterNewsletter, SuzanneSomersNewsletter, SuzanneSomersCustomerID, 
                        SiteID, ReferrerDomain, InBoundQuery) 
                    VALUES ( 
                        @FirstName, @LastName, @Email, @Phone, @Password,
                        '', '', getdate(), getdate(), getdate(), 
                        @FaceMasterNewsletter, @SuzanneSomersNewsletter, @SuzanneSomersCustomerID,
                        @SiteID, @ReferrerDomain, @InboundQuery) ;  
                    
                    SELECT CustomerId = scope_identity();";
            
            else
            
                sql = @"
                        UPDATE Customers 
                        SET 
                          FirstName = @FirstName, 
                          LastName = @LastName,
                          Email = @Email,
                          Phone = @Phone,
                          Password = @Password,
                          FaceMasterNewsletter = @FaceMasterNewsletter,
                          SuzanneSomersNewsletter = @SuzanneSomersNewsletter,
                          SuzanneSomersCustomerID = @SuzanneSomersCustomerID,
                          SiteID=@SiteID,
                          ReferrerDomain=@ReferrerDomain,
                          InBoundQuery=@InboundQuery, 
                          LastUpdateDate = @LastUpdateDate
                        WHERE 
                          CustomerID = @CustomerID";

            DateTime lastupdatedate = DateTime.Now;

            //establish parameter collection
            int ParameterCount = 13;
            SqlParameter[] sqlparameters = new SqlParameter[ParameterCount];

            sqlparameters[00] = new SqlParameter("@CustomerID", CustomerID);
            sqlparameters[01] = new SqlParameter("@FirstName", FirstName);
            sqlparameters[02] = new SqlParameter("@LastName", LastName);
            sqlparameters[03] = new SqlParameter("@Email", Email);
            sqlparameters[04] = new SqlParameter("@Password", Password);
            sqlparameters[05] = new SqlParameter("@Phone", Phone);
            sqlparameters[06] = new SqlParameter("@FaceMasterNewsletter", FaceMasterNewsletter);
            sqlparameters[07] = new SqlParameter("@SuzanneSomersNewsletter", SuzanneSomersNewsletter);
            sqlparameters[08] = new SqlParameter("@SuzanneSomersCustomerID", SuzanneSomersCustomerID);
            sqlparameters[09] = new SqlParameter("@SiteID", SiteID);
            sqlparameters[10] = new SqlParameter("@ReferrerDomain", ReferrerDomain);
            sqlparameters[11] = new SqlParameter("@InBoundQuery", InBoundQuery);
            sqlparameters[12] = new SqlParameter("@LastUpdateDate", lastupdatedate);

            SqlDataReader dr = null;
            try
            {
                if (customerID != 0)
                {
                    //already a customers, so we don't need the id
                    DBUtil.Exec(sql, sqlparameters);
                }
                else
                {
                    //we need a reader to get the new customerid
                    dr = DBUtil.FillDataReader(sql, sqlparameters);
                    if (dr.Read()) customerID = Convert.ToInt32(dr["CustomerId"]);
                }
            }
            catch (Exception ex)
            {
                Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "Customer Exception", ex.Message + ex.StackTrace + "---------" + sql);
                throw ex;
            }
            finally
            {
                if (dr != null) { dr.Close();}
            }
        }

        private void UpdateLastLogin(int customerid)
        {
            string sql = @"
            UPDATE Customers 
            SET 
              LastLoginDate = @LastLoginDate 
            WHERE 
              CustomerID = @CustomerID ";

            DateTime lastLoginDate = DateTime.Now;

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, lastLoginDate, customerid);
            DBUtil.Exec(sql, mySqlParameters);

        }

    }
}
