using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace Certnet
{
    class Installment
    {
        private int installmentID;  //unique id
        private int paymentPlanID;
        private int paymentNumber;  //1 based index
        private decimal paymentAmount;
        private int daysFromShipment;

        public int InstallmentID
        {
            get{ return installmentID;}
            set{ installmentID=value;}
        }
        public int PaymentPlanID
        {
            get{ return paymentPlanID;}
            set{ paymentPlanID=value;}
        }
        public int PaymentNumber
        {
            get{ return paymentNumber;}
            set{ paymentNumber=value;}
        }
        public decimal PaymentAmount
        {
            get{ return paymentAmount;}
            set{ paymentAmount=value;}
        }
        public int DaysFromShipment
        {
            get{ return daysFromShipment;}
            set{ daysFromShipment=value;}
        }



        public Installment()
        {
            installmentID = 0;
            paymentPlanID = 0;
            paymentNumber = 0;
            paymentAmount = 0;
            daysFromShipment = 0;
        }

        public Installment(int InstallmentID)
        {
            string sql = "SELECT * FROM Installments WHERE InstallmentID = " + InstallmentID;
            SqlDataReader dr =  DBUtil.FillDataReader(sql);
            while (dr.Read())
            {
                installmentID = Convert.ToInt32(dr["InstallmentID"].ToString());
                paymentPlanID = Convert.ToInt32(dr["PaymentPlanID"].ToString());
                paymentNumber=Convert.ToInt32(dr["PaymentNumber"].ToString());
                paymentAmount=Convert.ToInt32(dr["PaymentAmount"].ToString());
                daysFromShipment=Convert.ToInt32(dr["DaysFromShipment"].ToString());
            }
        }
    }
}
