using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace Certnet
{
    public class Mail
    {
        public static void SendMail(string from, string to, string subject, string body)
        {
            MailMessage MyMailMessage = new MailMessage(from, to, subject, body);

            MailAddress rm = new MailAddress("rmohme@sbcglobal.net");
            //MyMailMessage.Bcc.Add(rm);
            
            MyMailMessage.IsBodyHtml = false;

            NetworkCredential mailAuthentication = new NetworkCredential(AdminCart.Config.SMTPLogin, AdminCart.Config.SMTPPassword);

            //SmtpClient mailClient = new SmtpClient(Config.SMTPServer, Config.SMTPPort);
            SmtpClient mailClient = new SmtpClient(AdminCart.Config.SMTPServer, 25);

            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = mailAuthentication;
            mailClient.Send(MyMailMessage);
        }
        public static void SendMail(string body)
        {
            MailMessage MyMailMessage = new MailMessage(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "Cart Message", body);
            MyMailMessage.IsBodyHtml = false;

            NetworkCredential mailAuthentication = new NetworkCredential(AdminCart.Config.SMTPLogin, AdminCart.Config.SMTPPassword);

            //SmtpClient mailClient = new SmtpClient(Config.SMTPServer, Config.SMTPPort);
            SmtpClient mailClient = new SmtpClient(AdminCart.Config.SMTPServer, 25);

            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = mailAuthentication;
            mailClient.Send(MyMailMessage);
        }


    }
}
