using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace Certnet
{
    

    public class Order
    {
        public OrderDetails details;

        #region Variables
        private int orderID;
        private DateTime orderDate;
        private int customerID;
        private int shippingAddressID;
        private int billingAddressID;
        private decimal subTotal;
        private decimal totalShipping;
        private decimal totalTax;
        private decimal totalCoupon;
        private decimal adjust;
        private decimal total;
        private int affiliateOrderDetailID;
        private int siteSettingsID;
        #endregion

        #region Public Properties
        public int CustomerID { get { return customerID; } set { customerID = value; } }
        public int ShippingAddressID { get { return shippingAddressID; } set { shippingAddressID = value; } }
        public int BillingAddressID { get { return billingAddressID; } set { billingAddressID = value; } }

        public int OrderID
        {
            get { return orderID; }
            set { orderID = value; }
        }
        public DateTime OrderDate
        {
            get { return orderDate; }
            set { orderDate = value; }
        }
        public decimal SubTotal
        {
            get { return subTotal; }
            set { subTotal=value;}
        }
        public decimal TotalShipping
        {
            get { return totalShipping; }
            set { totalShipping=value;}
        }
        public decimal TotalTax
            {
            get { return totalTax; }
            set { totalTax=value;}
        }
        public decimal TotalCoupon
        {
            get { return totalCoupon; }
            set { totalCoupon = value; }
        }

        public decimal Adjust
        {
            get { return adjust; }
            set { adjust = value; }
        }

        public decimal Total
        {
            get { return total; }
            set { total=value;}
        }

        public int AffiliateOrderDetailID { get { return affiliateOrderDetailID; } set { affiliateOrderDetailID = value; } }
        public int SiteSettingsID { get { return siteSettingsID; } set { siteSettingsID = value; } }

        #endregion

        #region Constructors
        public Order()
        {
            details = new OrderDetails();

            orderID = 0;
            customerID = 0;
            billingAddressID = 0;
            shippingAddressID = 0;
            orderDate = DateTime.Parse("1/1/1999");
            subTotal = 0;
            totalShipping = 0;
            totalTax = 0;
            totalCoupon = 0;
            adjust = 0;
            total = 0;
            affiliateOrderDetailID = -1;
            siteSettingsID = -1;
        }

        

        public Order(int OrderID)
        {
            
            string sql = @"
                SELECT *
                FROM Orders
                WHERE OrderID = @OrderID";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read() && dr.HasRows)
                {
                
                orderID=Convert.ToInt32(dr["OrderID"].ToString());
                customerID = Convert.ToInt32(dr["CustomerID"].ToString());
                billingAddressID=Convert.ToInt32(dr["BillingAddressID"].ToString());
                shippingAddressID = Convert.ToInt32(dr["ShippingAddressID"].ToString());
                orderDate = Convert.ToDateTime(dr["OrderDate"].ToString());

                totalShipping = Convert.ToDecimal(dr["TotalShipping"].ToString());
                totalTax = Convert.ToDecimal(dr["TotalTax"].ToString());
                totalCoupon = Convert.ToDecimal(dr["TotalCoupon"].ToString());
                adjust = Convert.ToDecimal(dr["Adjust"].ToString());
                total = Convert.ToDecimal(dr["Total"].ToString());
                affiliateOrderDetailID = Convert.ToInt32(dr["AffiliateOrderDetailID"].ToString());
                siteSettingsID = Convert.ToInt32(dr["SiteSettingsID"].ToString());

                subTotal = total + totalCoupon - totalTax - totalShipping;
            }
            dr.Close();

            OrderDetails od = new OrderDetails();

            foreach(OrderDetail d in OrderDetail.GetOrderDetails(orderID))
            {
                od.AddItem(d);
            }

            details = od;

        }

        #endregion

        public int SaveDB()
        {
            SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStr());
            string sql = @"INSERT Orders(CartID, CustomerID, OrderDate, Total, Adjust, 
                    TotalShipping, TotalTax,TotalCoupon,Status,
                    ShipOrderAddressID, BillingAddressID, SourceID, Phone, SuzanneSomersOrderID, 
                    ShipMode, AffiliateOrderDetailID)
                   VALUES(@CartID, @CustomerID, @OrderDate, @Total, @Adjust,  }
                    @TotalShipping, @TotalTax, @TotalCoupon, 'New',
                    @ShipAddressID, @BillingAddressID, -1, 0, 0, 1, @AffiliateOrderDetailID, @SiteSettingsID);select OrderID=@@identity;";

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlParameter CustomerIDParam = new SqlParameter("@CustomerID", SqlDbType.Int);
            SqlParameter OrderDateParam = new SqlParameter("@OrderDate", SqlDbType.DateTime);
            SqlParameter TotalParam = new SqlParameter("@Total", SqlDbType.Money);
            
            SqlParameter TotalShippingParam = new SqlParameter("@TotalShipping", SqlDbType.Money);
            SqlParameter TotalTaxParam = new SqlParameter("@TotalTax", SqlDbType.Money);
            SqlParameter TotalCouponParam = new SqlParameter("@TotalCoupon",SqlDbType.Money);
            SqlParameter AdjustParam = new SqlParameter("@Adjust", SqlDbType.Money);
            SqlParameter StatusParam = new SqlParameter("@Status", SqlDbType.VarChar);
            SqlParameter ShipAddressIDParam = new SqlParameter("@ShipAddressID", SqlDbType.Int);
            SqlParameter BillAddressIDParam = new SqlParameter("@BillAddressID", SqlDbType.Int);
            SqlParameter AffiliateOrderDetailIDParam = new SqlParameter("@AffiliateOrderDetailID", SqlDbType.Int);
            SqlParameter SiteSettingsIDParam = new SqlParameter("@SiteSettingsID", SqlDbType.Int);

            CustomerIDParam.Value = customerID;
            OrderDateParam.Value = orderDate;
            TotalParam.Value = total;

            TotalShippingParam.Value = totalShipping;
            TotalTaxParam.Value = totalTax;
            TotalCouponParam.Value = totalCoupon;
            AdjustParam.Value = adjust;
            
            StatusParam.Value= "Uncharged";
            ShipAddressIDParam.Value = shippingAddressID;
            BillAddressIDParam.Value = billingAddressID;
            AffiliateOrderDetailIDParam.Value = affiliateOrderDetailID;
            SiteSettingsIDParam.Value = siteSettingsID;

            cmd.Parameters.Add(CustomerIDParam);
            cmd.Parameters.Add(OrderDateParam);
            cmd.Parameters.Add(TotalParam);
            cmd.Parameters.Add(TotalShippingParam);
            cmd.Parameters.Add(TotalTaxParam);
            cmd.Parameters.Add(TotalCouponParam);
            cmd.Parameters.Add(AdjustParam);
            cmd.Parameters.Add(StatusParam);
            cmd.Parameters.Add(ShipAddressIDParam);
            cmd.Parameters.Add(BillAddressIDParam);
            cmd.Parameters.Add(AffiliateOrderDetailIDParam);
            cmd.Parameters.Add(SiteSettingsIDParam);

            conn.Open();
            int result = 0;

            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read()){
                result = Convert.ToInt32(dr["OrderID"]);
            } else {
                result = -99;
            }
            dr.Close();
            conn.Close();
            return result;
        }

        /*
        public void Calculate()
        {

        }
        */
    }
}
