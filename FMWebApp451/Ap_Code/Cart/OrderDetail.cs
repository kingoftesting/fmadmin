using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace Certnet
{
    public class OrderDetail
    {
        #region Private Variables
        
        private Product lineItemProduct;

        private int orderDetailID;
        private int orderID;
        private int productID;
        private int quantity;
        private decimal unitPrice;
        private decimal unitCost;
        private decimal discount;
        private decimal lineItemtotal; //calculated, not stored in DB
        
        //not used
        private int addressID;
        #endregion

        #region Public Members

        public Product LineItemProduct
        {
            get { return lineItemProduct; }
            set { lineItemProduct = value; }
        }

        public int OrderDetailID{
            get{ return orderDetailID;}
            set{ orderDetailID = value;}
        }
        
        public int OrderID{
            get{return orderID;}
            set{orderID=value;}
        }
        
        public decimal LineItemTotal 
        { 
            get { return lineItemtotal; } 
            set { lineItemtotal = value; } 
        }
        
        public int ProductID
        {
            get{return productID;}
            set{productID=value;}
        }
        
        public int Quantity
        {
            get{return quantity;}
            set {quantity=value;}
        }
        
        public decimal UnitPrice
        {
            get{return unitPrice;}
            set{unitPrice=value;}
        }

        public decimal UnitCost
        {
            get { return unitCost; }
            set { unitCost = value; }
        }

        public decimal Discount
        {
            get { return discount; }
            set { discount = value; }
        }

        public int AddressID
        {
            set {addressID=value; }
            get { return addressID; }
        }
        #endregion

        #region Constructors
        public OrderDetail()
        {
            Product lineItemProduct = new Product();

            orderDetailID = 0;
            productID=0;
            quantity=0;
            unitPrice=0;
            unitCost = 0;
            lineItemtotal = 0;
            discount = 0;
            //not used
            addressID=0;
        }

        public OrderDetail(int OrderDetailID)
        {
            string sql = @"
            SELECT *
            FROM OrderDetails
            WHERE OrderDetailID = @OrderDetailID";

            SqlParameter[] NamedParameters = new SqlParameter[1];
            NamedParameters[0] = new SqlParameter("@OrderDetailID", OrderDetailID);

            SqlDataReader dr = DBUtil.FillDataReader(sql, NamedParameters);

            while (dr.Read())
            {
                orderDetailID = Convert.ToInt32(dr["OrderDetailID"].ToString());
                orderID = Convert.ToInt32(dr["OrderID"].ToString());
                productID = Convert.ToInt32(dr["ProductID"].ToString());
                quantity = Convert.ToInt32(dr["Quantity"].ToString());
                unitPrice = Convert.ToDecimal(dr["Price"].ToString());

                if (dr["UnitCost"] == DBNull.Value) { unitCost = 0; }
                else { unitCost = Convert.ToDecimal(dr["UnitCost"].ToString()); }

                lineItemProduct = new Product(productID);
            }
            dr.Close();

            lineItemtotal = quantity * (unitPrice - discount);
        }
        #endregion

        #region Methods
        public void SaveToOrderDetail()
        {
            CacheHelper.Clear(AdminCart.Config.cachekey_OrderDetailList);
            SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStr());
            string sql = @"
                INSERT OrderDetails(OrderId, ProductId, Price, Discount, Quantity, UnitCost)
                VALUES(@OrderId, @ProductId, @Price, @Discount, @Quantity, @UnitCost)";

            SqlParameter[] NamedParameters = new SqlParameter[6];

            NamedParameters[0] = new SqlParameter("@OrderId", orderID);
            NamedParameters[1] = new SqlParameter("@ProductId", productID);
            NamedParameters[2] = new SqlParameter("@Price", unitPrice);
            NamedParameters[3] = new SqlParameter("@Discount", discount);
            NamedParameters[4] = new SqlParameter("@Quantity", quantity);
            NamedParameters[5] = new SqlParameter("@UnitCost", unitCost);

            DBUtil.Exec(sql, NamedParameters);
        }

        static public List<OrderDetail> GetOrderDetails(int OrderID)
        {
            List<OrderDetail> details = new List<OrderDetail>();

            string sql = @"
            SELECT *
            FROM OrderDetails
            WHERE OrderID = @OrderID";

            SqlParameter[] NamedParameters = new SqlParameter[1];
            NamedParameters[0] = new SqlParameter("@OrderID", OrderID);

            SqlDataReader dr = DBUtil.FillDataReader(sql, NamedParameters);
            while (dr.Read())
            {
                OrderDetail od = new OrderDetail(Convert.ToInt32(dr["OrderDetailID"].ToString()));
                details.Add(od);
            }
            dr.Close();

            return details;
        }

        #endregion

    }
}
