using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Certnet
{
    public class OrderDetails: IEnumerable
    {
   

    private ArrayList items;

        public OrderDetails()
        {
            items = new ArrayList();
        }

        public OrderDetail CreateItem()
        {
            return new OrderDetail();
        }

        public void AddItem(OrderDetail item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
            {
                IncrementItem(item);
            }
            else
            {

                items.Add(item);
                
            }
        }
        public void UpdateItem(OrderDetail item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
            {
                OrderDetail currentitem = items[index] as OrderDetail;
                currentitem.Quantity = item.Quantity;
            }
        }
        public void IncrementItem(OrderDetail item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
            {
                OrderDetail currentitem = items[index] as OrderDetail;
                Product prod = new Product(currentitem.ProductID);
                currentitem.Quantity += 1;
                currentitem.LineItemTotal = currentitem.Quantity * prod.SalePrice;
            }
        }

        public void DeleteItem(OrderDetail item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
                items.RemoveAt(index);
            else
                throw new ArgumentException(
                    String.Format("Item = {0} is not in the shoppingcart.", item));

        }
        public int ItemCount()
        {
            int thecount = 0;
            foreach(OrderDetail i in items)
            {
                thecount++;
            }
            return thecount;
        }
        public decimal ItemSubTotal()
        {
            decimal subtotal = 0;
            foreach (OrderDetail i in items)
            {

                subtotal += 1;// i.LineItemTotal;
            }
            return subtotal;
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return (items as IEnumerable).GetEnumerator();
        }

        #endregion
         }
}
