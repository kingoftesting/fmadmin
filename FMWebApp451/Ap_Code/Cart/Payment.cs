using System;
using System.Collections.Generic;
using System.Text;

namespace Certnet
{
    public class Payment
    {
        public PaymentTransactions Transactions;

        private string paymentType;
        private decimal totalAmount;
        private decimal discountAmount;
        private string discountReason;
        private bool complete;
        private DateTime dueDate;
        private DateTime completeDate;

        public Payment()
        {
            paymentType = "Credit Card";
        }

        public decimal TotalAmount {
            get { return totalAmount; }
            set { totalAmount=value;}
        }
        public decimal DiscountAmount
        {
            get { return discountAmount; }
            set { discountAmount = value; }

        }
        public string DiscountReason
        {
            get { return discountReason; }
            set { discountReason = value; }

        }
        public bool Complete
        {
            get { return complete; }
            set { complete = value; }
        }
        public DateTime DueDate
        {
            get { return dueDate; }
            set { dueDate = value; }
        }
        public DateTime CompleteDate
        {
            get { return completeDate; }
            set { completeDate = value; }

        }
    }
}
