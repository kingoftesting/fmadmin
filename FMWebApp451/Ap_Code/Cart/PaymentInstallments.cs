using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Certnet
{
    class PaymentInstallments: IEnumerable
    {
        private ArrayList items;

        public PaymentInstallments()
        {
            items = new ArrayList();
        }
        public Installment CreateInstallment()
        {
            return new Installment();
        }
        public void AddItem(Installment installment)
        {
            int index = items.IndexOf(installment);
            items.Add(installment);
        }
        public void DeleteItem(Installment installment)
        {
            int index = items.IndexOf(installment);

            if (index >= 0)
                items.RemoveAt(index);
            else
                throw new ArgumentException(
                    String.Format("Item = {0} is not in the list.", installment));

        }
        public int ItemCount()
        {
            int thecount = 0;
            foreach (Item i in items)
            {
                thecount++;
            }
            return thecount;
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return (items as IEnumerable).GetEnumerator();
        }

        #endregion

    }
}
