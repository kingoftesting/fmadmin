using System;
using System.Collections.Generic;
using System.Text;

namespace Certnet
{
    class PaymentPlan
    {
        private int paymentPlanID;
        private string paymentPlanName;
        private DateTime createDate;
        private bool enabled;

        public PaymentInstallments installments;
        
        
        public int PaymentPlanID
        {
            get{ return paymentPlanID;}
            set{ paymentPlanID=value;}
        }
        public string PaymentPlanName
        {
            get{ return paymentPlanName;}
            set{ paymentPlanName=value;}
        }
        public DateTime CreateDate
        {
            get{ return createDate;}
            set{ createDate=value;}
        }
        public bool Enabled
        {
            get{ return enabled;}
            set{ enabled=value;}
        }

        public PaymentPlan()
        {
            paymentPlanID = 0;
            paymentPlanName = "";
            createDate = DateTime.Now;
            enabled = true;

            installments = new PaymentInstallments();

        }
    }
}
