using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace Certnet
{
    public class Product
    {
        #region PrivateVars
        private int productID;
        private string sku;
        private string name;
        private decimal price;
        private decimal salePrice;
        private bool taxable;
        private decimal weight;
        private decimal perUnitShipping;
        private string description;
        private string shortDescription;
        private string cartImage;
        private string thumbnailImage;
        private string detailsImage;

        //2/2008
        private bool inStock;
        private string outOfStockMessage;

        #endregion

        #region public vars
        public int ProductID
        {
            get { return productID; }
            set { productID = value; }
        }
        public string Sku
        {
            get { return sku; }
            set { sku = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public decimal Price
        {
            get { return price; }
            set { price = value; }
        }
        public decimal SalePrice
        {
            get { return salePrice; }
            set { salePrice=value; }
        }
        public bool Taxable 
        {
            get{ return taxable;}
            set{ taxable=value;}
        }
        public decimal Weight
        {
            get{ return weight;}
            set{ weight=value;}
        }
        public decimal PerUnitShipping
        {
            get { return perUnitShipping; }
            set { perUnitShipping = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string CartImage
        {
            get { return cartImage; }
            set { cartImage = value; }
        }
        public string ThumbnailImage
        {
            get { return thumbnailImage; }
            set { thumbnailImage = value; }
        }
        public string DetailsImage
        {
            get { return detailsImage; }
            set { detailsImage = value; }
        }

        public string ShortDescription { get { return shortDescription; } set { shortDescription = value; } }

        //2/2008
        public bool InStock { get { return inStock; } set { inStock = value; } }
        public string OutOfStockMessage { get { return outOfStockMessage; } set { outOfStockMessage = value; } }
        #endregion

        public Product()
        {
            productID = 0;
            sku = "";
            name = "";
            price = 0;
            salePrice = 0;
            taxable = false;
            weight = 0;
            perUnitShipping = 0;
            description = "";
            shortDescription = "";
            cartImage = "ImageNotAvailable.jpg";
            thumbnailImage = "ImageNotAvailable.jpg";
            detailsImage = "ImageNotAvailable.jpg";
            inStock = true;
            outOfStockMessage = "";
        }

        public Product(int ProductID)
        {
            //Load product from DB
            string sql = @"
                     SELECT 
                       ProductID, sku, name, price, saleprice, 
                       taxable, weight, perunitshipping, shortdescription, description, 
                       cartimage,thumbnailimage,DetailsImage,
                       InStock, OutOfStockMessage
                     FROM Products 
                     WHERE ProductID = @ProductID";
            
            //Create and load parameters collection
            int ParameterCount = 1;
            SqlParameter[] sqlParameters = new SqlParameter[ParameterCount];
            sqlParameters[0] = new SqlParameter("@ProductID", ProductID);

            SqlDataReader dr = null;

            try
            {
                dr = DBUtil.FillDataReader(sql, sqlParameters);

                dr.Read();

                if (dr != null)
                {
                    productID = dr.GetInt32(0);//Convert.ToInt32(dr["ProductID"].ToString());
                    sku = dr.GetString(1);
                    name = dr.GetString(2);
                    price = dr.GetDecimal(3);
                    salePrice = dr.GetDecimal(4);
                    taxable = dr.GetBoolean(5);
                    weight = dr.GetDecimal(6);
                    string pus = dr["PerUnitShipping"].ToString();

                    if (dr["PerUnitShipping"].ToString() != "")
                    {
                        perUnitShipping = decimal.Parse(dr["PerUnitShipping"].ToString());
                    }
                    shortDescription = dr.GetString(8);
                    description = dr.GetString(9);
                    cartImage = dr.GetString(10);
                    thumbnailImage = dr.GetString(11);
                    detailsImage = dr.GetString(12);
                    inStock = dr.GetBoolean(13);
                    outOfStockMessage = dr.GetString(14);
                    dr.Close();

                }
            }
            catch (Exception ex)
            {
                Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "Product Exception", ex.Message + ex.StackTrace + ex.TargetSite + ex.ToString());
                throw ex;
            }
            
        }

        public static List<Product> GetProducts()
        {
            List<Product> productlist = new List<Product>();

            string sql = "SELECT ProductID FROM Products";
            SqlDataReader dr = null;
            
            try
            {
                dr = DBUtil.FillDataReader(sql);
                while (dr.Read())
                {
                    Product p = new Product(Int32.Parse(dr["ProductID"].ToString()));
                    productlist.Add(p);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "Product Exception - GetProducts", ex.Message + ex.StackTrace);
                throw ex;
            }

            return productlist;
        }

        public static List<Product> GetProducts(bool customerOnly)
        {
            List<Product> productlist = new List<Product>();

            string sql = @"
            SELECT ProductID 
            FROM Products 
            ";
            if (customerOnly) {  sql += "WHERE enabled = 1 AND csonly = 0 ";}
            else { sql += "WHERE enabled = 1 "; }

            sql += "ORDER BY Sort ASC";

            //SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, csonly);
            SqlDataReader dr = null;

            try
            {
                dr = DBUtil.FillDataReader(sql);
                while (dr.Read())
                {
                    Product p = new Product(Int32.Parse(dr["ProductID"].ToString()));
                    productlist.Add(p);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "Product Exception - GetProducts", ex.Message + ex.StackTrace);
                throw ex;
            }

            return productlist;
        }


        #region System.Object Overrides

        public override string ToString()
        {
            return string.Format("{0}", productID.ToString());
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (Object.ReferenceEquals(this, obj)) return true;
            if (this.GetType() != obj.GetType()) return false;

            Product objItem = (Product)obj;
            if (productID == objItem.productID) return true;

            return false;
        }

        public override int GetHashCode()
        {
            return productID.GetHashCode();
        }

        #endregion

    }
}
