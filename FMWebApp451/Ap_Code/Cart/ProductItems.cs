using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Data.SqlClient;
using FM2015.Helpers;


namespace Certnet
{
    public class ProductItems: IEnumerable
    {
        private ArrayList items;

        public ProductItems()
        {
            items = new ArrayList();

            try //first, check to see if the list is cached
            {
                if (HttpContext.Current.Session["cache_ProductItems"] != null)
                {
                    items = (ArrayList)HttpContext.Current.Session["cache_ProductItems"];
                }
            }
            catch { }

            if (items.Count == 0)
            {
                string sql = @"
                SELECT * 
                FROM Products
                ORDER BY InStock desc, Name asc";
                SqlDataReader dr = DBUtil.FillDataReader(sql);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Product tempProduct = new Product(Convert.ToInt32(dr["ProductID"].ToString()));
                        items.Add(tempProduct);
                    }
                    dr.Close();
                }
                else
                {
                    Product tempProduct = new Product(1);
                    items.Add(tempProduct);
                }
                try
                {
                    HttpContext.Current.Session["cache_ProductItems"] = items; //this gets called often, so cache it
                }
                catch { }
            }
            
        }

        public Product CreateItem()
        {
            return new Product();
        }

        public void AddItem(Product item)
        {
            
                items.Add(item);
                
            
        }
        public void UpdateItem(Product item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
            {
                Product currentitem = items[index] as Product;
                //currentitem.Quantity = item.Quantity;
            }
        }
        

        public void DeleteItem(Product item)
        {
            int index = items.IndexOf(item);

            if (index >= 0)
                items.RemoveAt(index);
            else
                throw new ArgumentException(
                    String.Format("Item = {0} is not in the productlist.", item));

        }
        public int ItemCount()
        {
            int thecount = 0;
            foreach(Product i in items)
            {
                thecount++;
            }
            return thecount;
        }
        

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return (items as IEnumerable).GetEnumerator();
        }

        #endregion


    }
}
