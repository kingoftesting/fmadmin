using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Certnet;
//using PayFlowPro;
using Microsoft.ApplicationBlocks.Data;
using FM2015.Helpers;

namespace Certnet
{
    public class Transaction
    {
        //transactionType s=sale, c=credit, v=void, a=auth
        //resultCode 0=good, -12=denied, -10x=server error
        //transactionStatus open, auth needed, success, denied, error

        #region Private Variables     
        private int transactionRowID;
        private int orderID;
        private string transactionID;
        private DateTime transactionDate;
        private string trxType;
        private string cardType;
        private string cvsCode;
        private string cardNo;
        private string expDate;
        private decimal transactionAmount;
        private int resultCode;
        private string resultMsg;
        private string comment1;
        private string comment2;
        private string origTranID;
        private string authCode;
        private string cardHolder;
        private Address address;
        private string customerCode;
        private string iavs;
        private string avsZip;
        private string avsAddr;
        #endregion

        #region Public Properties
        public int TransactionRowID
        {
            get { return transactionRowID; }
            set { transactionRowID = value; }
        }
        public int OrderID
        {
            get { return orderID; }
            set { orderID = value; }
        }
        public string TransactionID
        {
            get { return transactionID; }
            set { transactionID = value; }
        }
        public DateTime TransactionDate
        {
            get { return transactionDate; }
            set { transactionDate = value; }
        }
        public string TrxType
        {
            get { return trxType; }
            set { trxType = value; }
        }
        public string CardType
        {
            get { return cardType; }
            set { cardType = value; }
        }
        public string CvsCode { get { return cvsCode; } set { cvsCode = value; } }
        public string CardNo
        {
            get { return cardNo; }
            set { cardNo = value; }
        }
        public string ExpDate
        {
            get { return expDate; }
            set { expDate = value; }
        }
        public decimal TransactionAmount
        {
            get { return transactionAmount; }
            set { transactionAmount = value; }
        }
        public int ResultCode
        {
            get { return resultCode; }
            set { resultCode = value; }
        }
        public string ResultMsg
        {
            get { return resultMsg; }
            set { resultMsg = value; }
        }
        public string Comment1
        {
            get { return comment1; }
            set { comment1 = value; }
        }
        public string Comment2
        {
            get { return comment2; }
            set { comment2 = value; }
        }
        public string OrigTranID
        {
            get { return origTranID; }
            set { origTranID = value; }
        }
        public string AuthCode
        {
            get { return authCode; }
            set { authCode = value; }
        }
        public string CardHolder
        {
            get { return cardHolder; }
            set { cardHolder = value; }
        }
        public Address Address
        {
            get { return address; }
            set { address = value; }
        }
        public string CustomerCode { get { return customerCode; } set { customerCode = value; } }
        public string Iavs
        {
            get { return iavs; }
            set { iavs = value; }
        }
        public string AvsZip
        {
            get { return avsZip; }
            set { avsZip = value; }
        }
        public string AvsAddr
        {
            get { return avsAddr; }
            set { avsAddr = value; }
        }
        #endregion
        

        public Transaction()
        {
            transactionRowID = 0;
            orderID = 0;
            transactionID = "";
            transactionDate = DateTime.Now;
            trxType = "";
            cardType = "";
            cvsCode = "";
            cardNo = "";
            expDate = "";
            transactionAmount = 0.0M;
            resultCode = 0;
            resultMsg = "";
            comment1 = "";
            comment2 = "";
            origTranID = "";
            authCode = "";
            cardHolder = "";
            address = null;
            customerCode = "";
            iavs = "";
            avsZip = "";
            avsAddr = "";

           
            this.Address = new Address();
        }

        static public Transaction GetTransaction(int OrderID)
        {
            Transaction t = new Transaction();

            //HACK - this could have more than one tran, but only returns one
            string sql = @"
                SELECT *
                FROM Transactions
                WHERE OrderID = @OrderID";

            SqlParameter[] NamedParameters = new SqlParameter[1];
            NamedParameters[0] = new SqlParameter("@OrderID", OrderID);

            SqlDataReader dr = DBUtil.FillDataReader(sql, NamedParameters);

            while (dr.Read())
            {
                t.transactionID = dr["TransactionID"].ToString();
                t.transactionDate = Convert.ToDateTime(dr["TransactionDate"].ToString());
                t.trxType = dr["TrxType"].ToString();
                t.cardType = dr["CardType"].ToString();
                t.cardNo = dr["CardNo"].ToString();
                t.expDate = dr["ExpDate"].ToString();
                t.transactionAmount = Convert.ToDecimal(dr["TransactionAmount"].ToString());
                t.resultCode = Convert.ToInt32(dr["ResultCode"].ToString());
                t.resultMsg = dr["ResultMsg"].ToString();
                t.comment1 = dr["Comment1"].ToString();
                t.comment2 = dr["Comment2"].ToString();
                t.cardHolder = dr["CardHolder"].ToString();
                t.address = new Address();
                t.address.City = dr["City"].ToString();
                t.address.State = dr["State"].ToString();
                t.address.Country = dr["Country"].ToString();
                t.customerCode = dr["CustomerCode"].ToString();
                t.iavs = dr["iavs"].ToString();
                t.avsZip = dr["avszip"].ToString();
                t.avsAddr = dr["Avsaddr"].ToString();
                
            }

            return t;

        }
        
         public void SaveDB()
        {
            SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStr());
            
             string sql = @"
                INSERT Transactions(
                OrderID, TransactionID, TransactionDate, TrxType, CardType,
                CardNo, ExpDate, TransactionAmount, ResultCode, ResultMsg, 
                CardHolder, Address, City, State, Zip, Country, CustomerCode, Comment1, Comment2, 
                OrigTranID, AuthCode, IAVS, AVSZIP, AVSADDR
                ) VALUES (
                @OrderID,@TransactionID, @TransactionDate, @TrxType, @CardType,
                @CardNo, @ExpDate, @TransactionAmount, @ResultCode, @ResultMsg, 
                @CardHolder, @Address, @City, @State, @Zip, @Country, @CustomerCode, @Comment1, @Comment2, 
                @OrigTranID, @AuthCode, @IAVS, @AVSZIP, @AVSADDR
                )";
            
             try
            {
                
                


                SqlParameter OrderIDParam = new SqlParameter("@OrderID", SqlDbType.Int);
                SqlParameter TransactionIDParam = new SqlParameter("@TransactionID", SqlDbType.VarChar, 14);
                SqlParameter TransactionDateParam = new SqlParameter("@TransactionDate", SqlDbType.DateTime);
                SqlParameter TrxTypeParam = new SqlParameter("@TrxType", SqlDbType.VarChar, 15);
                SqlParameter CardTypeParam = new SqlParameter("@CardType", SqlDbType.VarChar);
                SqlParameter CardNoParam = new SqlParameter("@CardNo", SqlDbType.VarChar, 16);
                SqlParameter ExpDateParam = new SqlParameter("@ExpDate", SqlDbType.VarChar, 5);
                SqlParameter TransactionAmountParam = new SqlParameter("@TransactionAmount", SqlDbType.Money);
                SqlParameter ResultCodeParam = new SqlParameter("@ResultCode", SqlDbType.Int);
                SqlParameter ResultMsgParam = new SqlParameter("@ResultMsg", SqlDbType.VarChar, 250);
                SqlParameter Comment1Param = new SqlParameter("@Comment1", SqlDbType.VarChar, 128);
                SqlParameter Comment2Param = new SqlParameter("@Comment2", SqlDbType.VarChar, 128);
                SqlParameter OrigTranIDParam = new SqlParameter("@OrigTranID", SqlDbType.VarChar, 14);
                SqlParameter AuthCodeParam = new SqlParameter("@AuthCode", SqlDbType.VarChar, 6);
                SqlParameter CardHolderParam = new SqlParameter("@CardHolder", SqlDbType.VarChar, 80);
                SqlParameter AddressParam = new SqlParameter("@Address", SqlDbType.VarChar, 65);
                SqlParameter cityParam = new SqlParameter("@city", SqlDbType.VarChar, 30);
                SqlParameter stateParam = new SqlParameter("@state", SqlDbType.VarChar, 30);
                SqlParameter zipParam = new SqlParameter("@zip", SqlDbType.VarChar, 18);
                SqlParameter countryParam = new SqlParameter("@country", SqlDbType.VarChar, 4);
                SqlParameter customerCodeParam = new SqlParameter("@CustomerCode", SqlDbType.VarChar, 25);
                SqlParameter IAVSParam = new SqlParameter("@IAVS", SqlDbType.VarChar, 3);
                SqlParameter AVSZIPParam = new SqlParameter("@AVSZIP", SqlDbType.VarChar, 3);
                SqlParameter AVSADDRParam = new SqlParameter("@AVSADDR", SqlDbType.VarChar, 3);

                OrderIDParam.Value = OrderID;
                TransactionIDParam.Value = TransactionID;
                TransactionDateParam.Value = TransactionDate;
                TrxTypeParam.Value = TrxType;
                CardTypeParam.Value = CardType;

                string partcc = CardNo.ToString();
                if (partcc.Length > 11)
                    partcc = partcc.Substring(0, 4) + "XXXXXX" + partcc.Substring(partcc.Length - 4, 4);
                CardNoParam.Value = partcc;

                ExpDateParam.Value = ExpDate;
                TransactionAmountParam.Value = Decimal.Round(TransactionAmount, 2);
                ResultCodeParam.Value = ResultCode;
                ResultMsgParam.Value = ResultMsg;
                Comment1Param.Value = Comment1;
                Comment2Param.Value = Comment2;
                OrigTranIDParam.Value = OrigTranID;
                if (AuthCode == "" || AuthCode == null) { AuthCodeParam.Value = DBNull.Value; } else { AuthCodeParam.Value = AuthCode; }
                CardHolderParam.Value = CardHolder;
                AddressParam.Value = address.Street;
                cityParam.Value = address.City;
                stateParam.Value = address.State;
                zipParam.Value = address.Zip;
                countryParam.Value = address.Country; //.Substring(0, 4);
                customerCodeParam.Value = CustomerCode;
                if (Iavs == "" || Iavs == null) { IAVSParam.Value = "n/a"; } else { IAVSParam.Value = Iavs; }
                if (AvsZip == "" || AvsZip == null) { AVSZIPParam.Value = "n/a"; } else { AVSZIPParam.Value = AvsZip; }
                if (AvsAddr == "" || AvsAddr == null) { AVSADDRParam.Value = "n/a"; } else { AVSADDRParam.Value = AvsAddr; }

                SqlCommand cmd = new SqlCommand(sql, conn);

                //cmd.Parameters.Add(TransactionRowIDParam);
                cmd.Parameters.Add(OrderIDParam);
                cmd.Parameters.Add(TransactionIDParam);
                cmd.Parameters.Add(TransactionDateParam);
                cmd.Parameters.Add(TrxTypeParam);
                cmd.Parameters.Add(CardTypeParam);
                cmd.Parameters.Add(CardNoParam);
                cmd.Parameters.Add(ExpDateParam);
                cmd.Parameters.Add(TransactionAmountParam);
                cmd.Parameters.Add(ResultCodeParam);
                cmd.Parameters.Add(ResultMsgParam);
                cmd.Parameters.Add(Comment1Param);
                cmd.Parameters.Add(Comment2Param);
                cmd.Parameters.Add(OrigTranIDParam);
                cmd.Parameters.Add(AuthCodeParam);
                cmd.Parameters.Add(CardHolderParam);
                cmd.Parameters.Add(AddressParam);
                cmd.Parameters.Add(cityParam);
                cmd.Parameters.Add(stateParam);
                cmd.Parameters.Add(zipParam);
                cmd.Parameters.Add(countryParam);
                cmd.Parameters.Add(customerCodeParam);
                cmd.Parameters.Add(IAVSParam);
                cmd.Parameters.Add(AVSZIPParam);
                cmd.Parameters.Add(AVSADDRParam);


                conn.Open();
                cmd.ExecuteNonQuery();
             
            }
            catch (Exception ex)
            {
                string sender = "CNC-001: ";
                string parms = "OrderID:" + OrderID.ToString() + ": " + Environment.NewLine;
                parms += "TransactionID:" + TransactionID + Environment.NewLine;
                Mail.SendMail(sender + sql + parms + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        public string GetCardTypeByNumber(string cardno)
        {
            string theCardType = "Unknown";

            if (cardno.Length < 14) return theCardType;
            
            if(cardno.Substring(0,1) == "4"){
                return theCardType="Visa";
            } else {
            switch(cardno.Substring(0,2)){
                case "51":
                case "52":
                case "53":
                case "54":
                case "55":
                    theCardType = "M/C";
                    break;
                case "34":
                case "37":
                    theCardType = "Amex";
                    break;
                case "36":
                    theCardType = "Diners";
                    break;
                case "38":
                    theCardType = "Carte Blanche";
                    break;
                default:
                    switch(cardno.Substring(0,4)){
                        case "6011":
                            theCardType="Discover";
                            break;
                        default:
                            break;
                    }
                    break;

            }
            }
            /*
             * Select Case CInt(Left(CardNo, 2))
   Case 34, 37
      CreditCardType = "American Express"
   Case 36
      CreditCardType = "Diners Club"
   Case 38
      CreditCardType = "Carte Blanche"
   Case 51 To 55
      CreditCardType = "Master Card"
   Case Else

      'None of the above - so check the
	  'first four digits collectively
      Select Case CInt(Left(CardNo, 4))
	  
         Case 2014, 2149
            CreditCardType = "EnRoute"
         Case 2131, 1800
            CreditCardType = "JCB"
         Case 6011
            CreditCardType = "Discover"
         Case Else

            'None of the above - so check the
            'first three digits collectively
            Select Case CInt(Left(CardNo, 3))
               Case 300 To 305
                  CreditCardType = "American Diners Club"
               Case Else
         
               'None of the above -
               'so simply check the first digit
               Select Case CInt(Left(CardNo, 1))
                  Case 3
                     CreditCardType = "JCB"
                  Case 4
                    CreditCardType = "Visa"
               End Select

            End Select
			
      End Select
	  
End Select
*/
            return theCardType;
        }

       /*
        * public int ComCharge(object mycart) 
        {
            PayFlowPro.PFPro pfpro = new PayFlowPro.PFPro();

            string  ResponseOut;
            int pCtlx;
            //int rowcount = 0;

            //Security Vars
            string  HostAddress = "test-payflow.verisign.com";
            //string  HostAddress = "payflow.verisign.com";
            string User = "slcwebsales1";
            string Vendor = "centurybank";
            string Partner  = "verisign";
            string Password = "websales2";

            //Don't change these vars
            int Timeout   = 30;
            string ProxyAddress  = "";
            int ProxyPort = 0;
            string ProxyLogon  = "";
            string ProxyPassword = "";
            int HostPort  = 443;
            string Amt = "";
            //string rawAmt = "";
            //string chargeMe = "Yes";

            Amt = "14.95";

            Cart cart = (Cart)mycart;
            string OrderID = cart.OrderID.ToString();
            string ccnum   = cardNo;
            string Name    = cardHolder;
            string Street  = cart.ShipAddress.Street;
            string Zip = cart.ShipAddress.Zip;
            string expdate = expDate;
            string TrxTypeName = "Sale";
            string CCType = cardType;
            string CCTypeName= "";

            switch (CCType)
            {
              case "M":
                CCTypeName = "M/C";
                break;

              case "V":
                CCTypeName = "Visa";
                break;

              case "A":
                CCTypeName = "Amex";
                break;

              case "D":
                CCTypeName = "Disc";
                break;
            }

            string comment1 = OrderID;
            string comment2 = "FaceMaster Payment -" + Name;
            string  ParmList  = "&TRXTYPE=S" +
                "&TENDER=C" +
                "&ACCT=" + ccnum +
                "&EXPDATE=" + expdate +
                "&AMT=" + Amt +
                "&NAME=" + Name +
                "&STREET=" + Street +
                "&ZIP=" + Zip +
                "&CUSTREF=" + OrderID +
                "&COMMENT1=" + comment1  +
                "&COMMENT2=" + comment2;
                          
            string UserAuth = "USER=" + User + "&VENDOR=" + Vendor + "&PARTNER=" + Partner + "&PWD=" + Password;
            ParmList = UserAuth + ParmList;

            pCtlx = pfpro.CreateContext( HostAddress, HostPort, Timeout, ProxyAddress, ProxyPort, ProxyLogon, ProxyPassword );
            ResponseOut = pfpro.SubmitTransaction( pCtlx, ParmList );
            pfpro.DestroyContext( pCtlx );

            string[] split = ResponseOut.Split('&');
            string result = null;
            string pnref = null;
            string respmsg = null;
            string avsaddr = null;
            string avszip = null;
            string iavs = null;
            string authcode = null;
            string ccnumdisplay = null;

            if (ccnum.Length > 12)
            {
            ccnumdisplay = ccnum.Substring(0,4) + "XXXXXXXX" + ccnum.Substring(ccnum.Length-4,4);
            } 
            else 
            {
            ccnumdisplay = "XXXXXXXX";
            } 

            foreach(string stringpair in split)
            {
                string[] spval = stringpair.Split('=');           
                switch(spval[0])
                {
                  case "RESULT":
                    result = spval[1];
                    break;

                  case "PNREF":
                    pnref = spval[1];
                    break;

                  case "RESPMSG":
                    respmsg = spval[1];
                    break;

                  case "AVSADDR":
                    avsaddr = spval[1];
                    break;

                  case "AVSZIP":
                    avszip = spval[1];
                    break;

                  case "IAVS":
                    iavs = spval[1];
                    break;

                  case "AUTHCODE":
                    authcode = spval[1];
                    break;    
                }   
            }

            //log into transactions
            string sql = "INSERT Transactions(OrderID, TransactionID, TransactionDate, TrxType, CardType, CardNo, ExpDate, " +
                "TransactionAmount, ResultCode, ResultMsg, Comment1, Comment2, AuthCode, CardHolder, Address1, Zip, IAVS, AVSZIP, AVSADDR) " +
                "VALUES(" +
                OrderID + ", '" +
                pnref + "', " +
                "getdate() , '" +
                TrxTypeName + "', '" +
                CCTypeName + "', '" +
                ccnumdisplay + "', '" +
                expdate.Substring(0,2) + "/" + expdate.Substring(2,2) + "', " +
                Amt + ", " +
                result + ", '" +
                respmsg + "', '" +
                comment1 + "', '" +
                comment2 + "', '" +
                authcode + "', '" +
                Name + "', '" +
                Street + "', '" +
                Zip + "', '" +
                iavs + "', '" +
                avszip + "', '" +
                avsaddr + "') " ;

            SqlDataReader dr = SqlHelper.ExecuteReader(Config.ConnectionString, sql, "");
            dr.Close();

            return Convert.ToInt32(result);
        }
*/

        public int TestCharge()
        {
            if (AdminCart.Config.CartSetting == "Test")
            {
/*
                this.resultCode = 0;
                transactionID = "DH0000000000000";
                previousTransactionID = "";
                transactionDate = DateTime.Now;
                authCode = "123456";
                transactionStatus = "Charged";
                return resultCode;
*/
                return 0;
            }
            else
            {
                //todo production transaction hit here
                return -999;
            }
        }

    }
}




