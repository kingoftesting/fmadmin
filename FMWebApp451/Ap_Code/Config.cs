using System;
using System.Configuration;
using System.Web;

namespace AdminCart
{
    /// <summary>
    /// Summary description for Config
    /// </summary>
    public static class Config
    {
        //public Config()
        //{
        //}

        //
        //domain names to ip address stuff
        //
        // facemaster adminCER: 
        // crsystlift adminCER: cladmin.mycrystalift.com --> 52.25.186.65
        // sonulase adminCER:
        // rmShop adminCER: 

        //
        // VERSION STUFF
        //
        static public string Version = ConfigurationManager.AppSettings["AppVersion"].ToString();
        static public string WebServiceVersion = ConfigurationManager.AppSettings["WebServiceVersion"].ToString();
        static public string appName = ConfigurationManager.AppSettings["AppName"].ToString();
        static public string PayWhirlApiVersion = ConfigurationManager.AppSettings["PayWhirlApiVersion"].ToString().ToUpper();
        static public bool ApiTest = (ConfigurationManager.AppSettings["ApiTest"].ToString().ToUpper() == "YES");

        public const int rmShopCompanyID = 0;
        public const int fmCompanyID = 1;
        public const int hdiCompanyID = 2;
        public const int liCompanyID = 3;
        public const int ssCompanyID = 4;

        //
        // CHACHE STUFF
        //
        public const bool globalCacheEnable = true; //all caching either on or off
        public const int globalCacheDuration = 120; //minutes to cache expiration
        public const int cacheDuration_CERList = 120; //minutes to cache expiration
        public const string cachekey_CERList = "cerList";
        public const int cacheDuration_ProductList = 120; //minutes to cache expiration
        public const string cachekey_ProductList = "productList";
        public const int cacheDuration_ProductCostList = 120; //minutes to cache expiration
        public const string cachekey_ProductCostList = "productCostList";
        public const int cacheDuration_RiskList = 120; //minutes to cache expiration
        public const string cachekey_RiskList = "RiskList";
        public const int cacheDuration_ShopOrderNoteList = 120; //minutes to cache expiration
        public const string cachekey_ShopOrderNoteList = "ShopOrderNoteList";
        public const int cacheDuration_OrderList = 120; //minutes to cache expiration
        public const string cachekey_OrderList = "OrderList";
        public const int cacheDuration_CustomerList = 120; //minutes to cache expiration
        public const string cachekey_CustomerList = "CustomerList";
        //public const int cacheDuration_OrderDetailList = 120; //minutes to cache expiration
        public const string cachekey_OrderDetailList = "OrderDetailList";
        public const int cacheDuration_SubscriptionPlanList = 120; //minutes to cache expiration
        public const string cachekey_SubscriptionPlanList = "SubscriptionPlanList";
        public const int cacheDuration_SubscriptionsList = 120; //minutes to cache expiration
        public const string cachekey_SubscriptionsList = "SubscriptionsList";




        //
        // TEST & CART SETTIING STUFF
        //
        public static bool SyncShopify = (ConfigurationManager.AppSettings["SyncShopify"].ToString().ToUpper() == "YES") ? true : false;
        public static bool SyncSS = (ConfigurationManager.AppSettings["SyncSS"].ToString().ToUpper() == "YES") ? true : false;
        public static string CartSetting = ConfigurationManager.AppSettings["CartSetting"].ToString();
        //public static string CartSetting = "Live"; //Live Mode
        //public static string CartMode = ConfigurationManager.AppSettings["CartMode"].ToString(); //LiveTransaction
        public const string CartMode = "LiveTransaction";
        //public static decimal MinimumShipping = 5.95M;
        public static decimal MinimumShipping = Convert.ToDecimal(ConfigurationManager.AppSettings["MinimumShipping"]);

        static public bool EasyTestLogin = (ConfigurationManager.AppSettings["EasyTestLogin"].ToString().ToUpperInvariant() == "YES") ? true : false;
        static public string CheckoutLink = ConfigurationManager.AppSettings["CheckoutLink"].ToString();

        //used to switch to https during checkout, if Live
        static public string RedirectMode = ConfigurationManager.AppSettings["RedirectMode"].ToString(); //or "Test"; also change connection strings, and cart configs
        //static public string RedirectMode = "Live"; //also change connection string below, and cart config
        //static public string RedirectMode = "Test"; //also change connection string below, and cart config

        /*******************************************************
        // PACKING SLIP STUFF
        *******************************************************/

        //static public string ReturnStreet1 = "23961 Craftsman Rd.,Suite G";
        //static public string ReturnStreet2 = "Attention: Fulfillment Center";
        //static public string ReturnCity = "Calabasas, CA 91302 USA";

        static public string ReturnStreet1 = "21440 Osborne St.";
        static public string ReturnStreet2 = "Attention: Fulfillment Center";
        static public string ReturnCity = "Canoga Park, CA 91304-1520 USA";

        /*******************************************************
        // Stale Customer Stuff
        *******************************************************/

        // values are in months
        public const int stale_startMonthsBack = -15;
        public const int stale_fmPurchaseWindow = 3; //relative to startMonthsBack
        public const int stale_startAccyPurchaseWindow = 3; //relative to startMonthsBack
        public const int stale_endAccyPurchaseWindow = -6; //relative to DateTime.Now


        /*******************************************************
        // THEME STUFF
        *******************************************************/

        static public string AdminImage()
        {
            string result = "";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "~/images/products/facemaster/PFM_Admin.jpg";
                    break;

                case "CRYSTALIFT":
                    result = "~/images/products/crystalift/crystalift-160.png";
                    break;

                case "SONULASE":
                    result = "~/images/products/sonulase/sonulase.png"; ;
                    break;

                case "RMSHOP":
                    result = "~/images/products/rmshop/Solar2_R_1-400x.jpg";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        static public string AdminURL()
        {
            string result = "";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "https://fmadmin.facemaster.com/admin/default.aspx";
                    break;

                case "CRYSTALIFT":
                    result = "https://52.25.186.65/admin/default.aspx";
                    break;

                case "SONULASE":
                    result = "http://admin.mysonulase.com/admin/default.aspx";
                    break;

                case "RMSHOP":
                    result = "http://52.33.125.114/admin/default.aspx";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        static public string localAdminURL()
        {
            string result = "";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = VirtualPathUtility.ToAbsolute("~/") + "admin/default.aspx";
                    break;

                case "CRYSTALIFT":
                    result = VirtualPathUtility.ToAbsolute("~/") + "admin/default.aspx";
                    break;

                case "SONULASE":
                    result = VirtualPathUtility.ToAbsolute("~/") + "admin/default.aspx";
                    break;

                case "RMSHOP":
                    result = VirtualPathUtility.ToAbsolute("~/") + "admin/default.aspx";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        static public string AdminName()
        {
            string result = "";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "FaceMaster";
                    break;

                case "CRYSTALIFT":
                    result = "Crystalift";
                    break;

                case "SONULASE":
                    result = "SONULASE"; ;
                    break;

                case "RMSHOP":
                    result = "rmShop";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        static public string BizName()
        {
            string result = "";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "FaceMaster of Beverly Hills, Inc.";
                    break;

                case "CRYSTALIFT":
                    result = "Life Innovations, LLC";
                    break;

                case "SONULASE":
                    result = "Life Innovations, LLC"; ;
                    break;

                case "RMSHOP":
                    result = "Capital Momentum";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        static public string StatementDescriptor()
        {
            string result = "";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "WWW.FACEMASTER.COM";
                    break;

                case "CRYSTALIFT":
                    result = "WWW.MYCRYSTALIFT.COM";
                    break;

                case "SONULASE":
                    result = "WWW.MYSONULASE.COM"; ;
                    break;

                case "RMSHOP":
                    result = "Capital Momentum";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        static public string AppCode()
        {
            string result = "";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "FM";
                    break;

                case "CRYSTALIFT":
                    result = "CL";
                    break;

                case "SONULASE":
                    result = "SO"; ;
                    break;

                case "RMSHOP":
                    result = "RM";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        static public string AppPhoneNumber()
        {
            string result = "";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "800-770-2521";
                    break;

                case "CRYSTALIFT":
                    result = "855-880-5252";
                    break;

                case "SONULASE":
                    result = "855-880-5252";
                    break;

                case "RMSHOP":
                    result = "(805) 610-6826";
                    break;

                default:
                    break;
            }
            return result;
        }

        static public int CompanyID()
        {
            int result = 0;

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = fmCompanyID;
                    break;

                case "CRYSTALIFT":
                    result = liCompanyID; //LI = 3
                    break;

                case "SONULASE":
                    result = liCompanyID; //LI = 3
                    break;

                case "RMSHOP":
                    result = rmShopCompanyID; //default sandbox company
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        static public string CompanyIDName(int companyID)
        {
            string result = "";

            switch (companyID)
            {
                case rmShopCompanyID:
                    result = "rmShop";
                    break;

                case fmCompanyID:
                    result = "FM";
                    break;

                case hdiCompanyID:
                    result = "HDI";
                    break;

                case liCompanyID:
                    result = "LI";
                    break;

                case ssCompanyID:
                    result = "SLC";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }


        /*******************************************************
        // CONNECTION STRINGS
        *******************************************************/

        //<add name="FMSqlServer" connectionString="SERVER=.;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb" providerName="System.Data.SqlClient" />
        //static public string ConnectionString = ConfigurationManager.ConnectionStrings["FMSqlServer"].ConnectionString;
        //static public string ConnectionString = "SERVER=.;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb";
        //LOCAL AWS
        //<add name="FMSqlServer" connectionString="SERVER=fmadmin.facemaster.com;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb" providerName="System.Data.SqlClient" />
        //static public string ConnectionString = "SERVER=fmadmin.facemaster.com;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb";
        //static public string ConnectionString = "SERVER=10.10.0.94;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb";
        //TEST
        //<add name="FMSqlServer" connectionString="SERVER=.;UID=web;PWD=hello101;DATABASE=SiteV2Test;Application Name=FaceMasterWeb" providerName="System.Data.SqlClient" />
        //<add name="FMSqlServer" connectionString="SERVER=fmadmin.facemaster.com;UID=web;PWD=hello101;DATABASE=SiteV2Test;Application Name=FaceMasterWeb" providerName="System.Data.SqlClient" />
        //static public string ConnectionString = "SERVER=10.10.0.94;UID=web;PWD=hello101;DATABASE=SiteV2Test;Application Name=FaceMasterWebTest";
        static public string ConnStr()
        {
            string result = "undefined";

            string aWS_Deploy = ConfigurationManager.AppSettings["AWS-Deploy"].ToString();
            bool deploy = (aWS_Deploy.ToUpper() == "YES") ? true : false;

            string useProductionDBs = ConfigurationManager.AppSettings["UseProductionDBs"].ToString();
            bool productionDBs = (useProductionDBs.ToUpper() == "YES") ? true : false;
            
            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    if (deploy)
                    {
                        if (productionDBs) { result = "SERVER=.;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb"; }
                        else { result = "SERVER=.;UID=web;PWD=hello101;DATABASE=SiteV2Test;Application Name=FaceMasterWeb"; }
                    }
                    else
                    {
                        if (productionDBs) { result = "SERVER=fmadmin.facemaster.com;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb"; }
                        else { result = "SERVER=fmadmin.facemaster.com;UID=web;PWD=hello101;DATABASE=SiteV2Test;Application Name=FaceMasterWeb"; }
                    }
                    break;

                case "CRYSTALIFT":
                    if (deploy)
                    {
                        if (productionDBs) { result = "SERVER=.;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb"; }
                        else { result = "SERVER=.;UID=web;PWD=hello101;DATABASE=SiteV2Test;Application Name=FaceMasterWeb"; }
                    }
                    else
                    {
                        if (productionDBs) { result = "SERVER=52.25.186.65;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb"; }
                        else { result = ""; }
                    }
                    break;

                case "SONULASE":
                    if (deploy)
                    {
                        if (productionDBs) { result = "SERVER=.;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb"; }
                        else { result = "SERVER=.;UID=web;PWD=hello101;DATABASE=SiteV2Test;Application Name=FaceMasterWeb"; }
                    }
                    else
                    {
                        if (productionDBs) { result = "SERVER=admin.mysonulase.com;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb"; }
                        else { result = ""; }
                    }
                    break;

                case "RMSHOP":
                    if (deploy)
                    {
                        if (productionDBs) { result = "SERVER=.;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb"; }
                        else { result = ""; }
                    }
                    else
                    {
                        if (productionDBs) { result = "SERVER=52.33.125.114;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb"; }
                        else { result = ""; }
                    }
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        //<add name="CERSqlServer" connectionString="SERVER=.;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=FaceMasterComplaintTracking;Application Name=FaceMasterComplaintResolution" providerName="System.Data.SqlClient" />
        //<add name="CERSqlServer" connectionString="SERVER=fmadmin.facemaster.com;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=FaceMasterComplaintTracking;Application Name=FaceMasterComplaintResolution" providerName="System.Data.SqlClient" />
        //static public string ConnectionStringCER = ConfigurationManager.ConnectionStrings["CERSqlServer"].ConnectionString;
        //static public string ConnectionStringCER = @"SERVER=10.10.0.94;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=FaceMasterComplaintTracking;Application Name=FaceMasterComplaintResolution";
        static public string ConnStrCER()
        {
            string result = "undefined";

            string aWS_Deploy = ConfigurationManager.AppSettings["AWS-Deploy"].ToString();
            bool deploy = (aWS_Deploy.ToUpper() == "YES") ? true : false;

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    if (deploy)
                    {
                        result = "SERVER=.;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=FaceMasterComplaintTracking;Application Name=FaceMasterComplaintResolution";
                    }
                    else
                    {
                        result = "SERVER=fmadmin.facemaster.com;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=FaceMasterComplaintTracking;Application Name=FaceMasterComplaintResolution";
                    }
                    break;

                case "CRYSTALIFT":
                    if (deploy)
                    {
                        result = "SERVER=.;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=CrystaliftComplaintTracking;Application Name=CrystaliftComplaintResolution";
                    }
                    else
                    {
                        result = "SERVER=52.25.186.65;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=CrystaliftComplaintTracking;Application Name=CrystaliftComplaintResolution";
                    }
                    break;

                case "SONULASE":
                    if (deploy)
                    {
                        result = "SERVER=.;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=CrystaliftComplaintTracking;Application Name=CrystaliftComplaintResolution";
                    }
                    else
                    {
                        result = "SERVER=admin.mysonulase.com;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=CrystaliftComplaintTracking;Application Name=CrystaliftComplaintResolution";
                    }
                    break;

                case "RMSHOP":
                    if (deploy)
                    {
                        result = "SERVER=.;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=CrystaliftComplaintTracking;Application Name=CrystaliftComplaintResolution";
                    }
                    else
                    {
                        result = "SERVER=52.33.125.114;UID=ComplaintWeb2;PWD=ComplaintWeb;DATABASE=CrystaliftComplaintTracking;Application Name=CrystaliftComplaintResolution";
                    }
                    break;


                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        //<add name="HDISqlServer" connectionString="SERVER=.;UID=FMInfomercial;PWD=hello101;DATABASE=FMInfomercial;Application Name=FMInfomercialOrders" providerName="System.Data.SqlClient" />
        //<add name="HDISqlServer" connectionString="SERVER=fmadmin.facemaster.com;UID=FMInfomercial;PWD=hello101;DATABASE=FMInfomercial;Application Name=FMInfomercialOrders" providerName="System.Data.SqlClient" />
        //static public string ConnectionStringHDI = ConfigurationManager.ConnectionStrings["HDISqlServer"].ConnectionString;
        //static public string ConnectionStringHDI = @"SERVER=10.10.0.94;UID=FMInfomercial;PWD=hello101;DATABASE=FMInfomercial;Application Name=FMInfomercialOrders";
        static public string ConnStrHDI()
        {
            string result = "undefined";

            string aWS_Deploy = ConfigurationManager.AppSettings["AWS-Deploy"].ToString();
            bool deploy = (aWS_Deploy.ToUpper() == "YES") ? true : false;

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    if (deploy)
                    {
                        result = "SERVER=.;UID=FMInfomercial;PWD=hello101;DATABASE=FMInfomercial;Application Name=FMInfomercialOrders";
                    }
                    else
                    {
                        result = "SERVER=fmadmin.facemaster.com;UID=FMInfomercial;PWD=hello101;DATABASE=FMInfomercial;Application Name=FMInfomercialOrders";
                    }
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        static public string ConnStrSiteV2Restore()
        {
            string result = "undefined";

            string aWS_Deploy = ConfigurationManager.AppSettings["AWS-Deploy"].ToString();
            bool deploy = (aWS_Deploy.ToUpper() == "YES") ? true : false;

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    if (deploy)
                    {
                        result = "SERVER=.;UID=web;PWD=hello101;DATABASE=SiteV2Restore;Application Name=FaceMasterWeb";
                    }
                    else
                    {
                        result = "SERVER=fmadmin.facemaster.com;UID=web;PWD=hello101;DATABASE=SiteV2Restore;Application Name=FaceMasterWeb";
                    }
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        //<add name="ShopifySqlServer" connectionString="SERVER=.;UID=ShopifyFM;PWD=kingoftesting;Database=ShopifyFM;Application Name=ShopifyImport" providerName="System.Data.SqlClient" />
        //<add name="ShopifySqlServer" connectionString="SERVER=fmadmin.facemaster.com;UID=ShopifyFM;PWD=kingoftesting;Database=ShopifyFM;Application Name=ShopifyImport" providerName="System.Data.SqlClient" />
        //static public string ConnectionStringShopify = ConfigurationManager.ConnectionStrings["ShopifySqlServer"].ConnectionString;
        //static public string ConnectionStringShopify = @"SERVER=10.10.0.94;UID=ShopifyFM;PWD=kingoftesting;Database=ShopifyFM;Application Name=ShopifyImport;";
        static public string ConnStrShopify()
        {
            string result = "undefined";

            string aWS_Deploy = ConfigurationManager.AppSettings["AWS-Deploy"].ToString();
            bool deploy = (aWS_Deploy.ToUpper() == "YES") ? true : false;

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    if (deploy)
                    {
                        result = "SERVER=.;UID=ShopifyFM;PWD=kingoftesting;Database=ShopifyFM;Application Name=ShopifyImport";
                    }
                    else
                    {
                        result = "SERVER=fmadmin.facemaster.com;UID=ShopifyFM;PWD=kingoftesting;Database=ShopifyFM;Application Name=ShopifyImport";
                    }
                    break;

                case "CRYSTALIFT":
                    if (deploy)
                    {
                        result = "SERVER=.;UID=ShopifyCL;PWD=kingoftesting;Database=ShopifyCL;Application Name=ShopifyImport";
                    }
                    else
                    {
                        result = "SERVER=52.25.186.65;UID=ShopifyCL;PWD=kingoftesting;Database=ShopifyCL;Application Name=ShopifyImport";
                    }
                    break;

                case "SONULASE":
                    if (deploy)
                    {
                        result = "SERVER=.;UID=ShopifyCL;PWD=kingoftesting;Database=ShopifyCL;Application Name=ShopifyImport";
                    }
                    else
                    {
                        result = "SERVER=admin.mysonulase.com;UID=ShopifyCL;PWD=kingoftesting;Database=ShopifyCL;Application Name=ShopifyImport";
                    }
                    break;

                case "RMSHOP":
                    if (deploy)
                    {
                        result = "SERVER=.;UID=ShopifyCL;PWD=kingoftesting;Database=ShopifyCL;Application Name=ShopifyImport";
                    }
                    else
                    {
                        result = "SERVER=52.33.125.114;UID=ShopifyCL;PWD=kingoftesting;Database=ShopifyCL;Application Name=ShopifyImport";
                    }
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }
         

        /*******************************************************
        // SHOPIFY API STUFF
        *******************************************************/
        //<add key="apikey" value="00976576f3b781bc33f2e4bc1368bab6" />
        static public string ApiKeyShopify()
        {
            string result = "undefined";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "00976576f3b781bc33f2e4bc1368bab6"; // 00976576f3b781bc33f2e4bc1368bab6
                    break;

                case "CRYSTALIFT":
                    result = "d4bf388486b6d692400fe7682f877344";
                    break;

                case "SONULASE":
                    result = "a95e425947049137227a6d89be113591";
                    break;

                case "RMSHOP":
                    result = "6497aeb899af95c538c5e51929f35d94";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        //<add key="pass" value="27e3d1bea70f2df2785b0301f6813074" />
        static public string PassShopify()
        {
            string result = "undefined";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "27e3d1bea70f2df2785b0301f6813074"; // 27e3d1bea70f2df2785b0301f6813074
                    break;

                case "CRYSTALIFT":
                    result = "d435322ac009864700ed51148a8b101c";
                    break;

                case "SONULASE":
                    result = "15766a6f0701b71c439032bdfda1b79e";
                    break;

                case "RMSHOP":
                    result = "608fbd3151e2e29ca2444b0808cb0928";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        //<add key="sharedSecret" value="c404c9809db533255e361aa57e637eee" />
        static public string SharedSecretShopify()
        {
            string result = "undefined";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "c404c9809db533255e361aa57e637eee";
                    break;

                case "CRYSTALIFT":
                    result = "579a509b8892b22c34d726a3a1255177";
                    break;

                case "SONULASE":
                    result = "6d4e8a2f6c1dba8e6c41a9cad455e6ad";
                    break;

                case "RMSHOP":
                    result = "cb554c4a9f7e886504ba871fa2c922b7";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        //<add key="adminroot" value="@facemaster.myshopify.com/" />
        static public string AdminRootShopify()
        {
            string result = "undefined";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "https://facemaster.myshopify.com/";
                    break;

                case "CRYSTALIFT":
                    result = "https://crystalift.myshopify.com/";
                    break;

                case "SONULASE":
                    result = "https://sonulase-2.myshopify.com/";
                    break;

                case "RMSHOP":
                    result = "https://fm-cer.myshopify.com/";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        static public string ShopifySyncPath()
        {
            string result = "undefined";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = @"c:\tmp\fm\shopifyFM\";
                    break;

                case "CRYSTALIFT":
                    result = @"c:\tmp\cl\shopifyCL\";
                    break;

                case "SONULASE":
                    result = @"c:\tmp\so\shopifySO\";
                    break;

                case "RMSHOP":
                    result = @"c:\tmp\rm\shopifyRM\";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }


        /*******************************************************
        // PAYWHIRL API STUFF
        ********************************************************/

        //<add key="PayWhirl_apikey" value="PWB9072BDE668D5950355C90AB313E4E45" />
        static public string ApiKeyPayWhirl()
        {
            string result = "undefined";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    switch (PayWhirlApiVersion)
                    {
                        case "V1":
                            result = "PWB9072BDE668D5950355C90AB313E4E45";
                            break;

                        case "V2":
                            result = "";
                            break;

                        case "test":
                            result = "test";
                            break;

                        default:
                            throw new NotImplementedException();

                    }
                    break;

                case "CRYSTALIFT":
                    switch (PayWhirlApiVersion)
                    {
                        case "V1":
                            result = "PWD7A88AFEF8573C8F09C56AC88D7C82A3"; // original acct: "PW61115F78A6F72298377D624F042FAA4B";
                            break;

                        case "V2":
                            result = "";
                            break;

                        case "test":
                            result = "test";
                            break;

                        default:
                            throw new NotImplementedException();

                    }
                    break;

                case "SONULASE":
                    switch (PayWhirlApiVersion)
                    {
                        case "V1":
                            result = "PWDC431C56C68648FD5367BBD2A0AAB699"; 
                            break;

                        case "V2":
                            result = "";
                            break;

                        case "test":
                            result = "test";
                            break;

                        default:
                            throw new NotImplementedException();

                    }
                    break;

                case "RMSHOP":
                    switch (PayWhirlApiVersion)
                    {
                        case "V1":
                            result = "";
                            break;

                        case "V2":
                            result = "pwpk_56db31cfb9f7756db31cfba01e";
                            break;

                        case "test":
                            result = "test";
                            break;

                        default:
                            throw new NotImplementedException();

                    }
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        //<add key="PayWhirl_secret" value="PWSC7071C9F72223A1BBDEA6E1384D726835" />
        static public string SecretPayWhirl()
        {
            string result = "undefined";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "PWSC7071C9F72223A1BBDEA6E1384D726835";
                    break;

                case "CRYSTALIFT":
                    result = "PWSC4215417D37A6AD255BE5252482915EBE"; //original acct: "PWSC680C0CCB9D09CF7F6C43717084F7037C";
                    break;

                case "SONULASE":
                    result = "PWSC845CC000427AFD6B3F10249EC516DFA8";
                    break;

                case "RMSHOP":
                    result = "pwpsk_56db31cfba0d156db31cfba177";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        //add key="PayWhirlroot" value="www.paywhirl.com" />
        static public string RootPayWhirl()
        {
            return "https://www.paywhirl.com";
        }

        static public string RootPayWhirlApi()
        {
            if (appName.ToUpper() == "RMSHOP")
            {
                return "https://api.paywhirl.com";
            }
            else
            {
                return "https://www.paywhirl.com";
            }
        }

        //add key="PayWhirlroot" value="api.paywhirl.com" />
        static public string RootPayWhirlV2()
        {
            return "https://app.paywhirl.com";
        }

        static public string RootPayWhirlApiV2()
        {
            return "https://api.paywhirl.com";
        }

        /*******************************************************
        // STRIPE API STUFF
        ********************************************************/

        //<add key="StripeApiSecretKey" value="sk_live_C3cQTdtBxVBJztCx1gva5S6r" />
        //<add key="TestStripeApiSecretKey" value="sk_test_chyPPuzAA1M3ETDVHz2vOBVd" />
        static public string ApiSecretKeyStripe()
        {
            string result = "undefined";

            string stripe_Live = ConfigurationManager.AppSettings["StripeLive"].ToString();
            bool live = (stripe_Live.ToUpper() == "YES") ? true : false;


            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    if (live)
                    {
                        result = "sk_live_C3cQTdtBxVBJztCx1gva5S6r";
                    }
                    else
                    {
                        result = "sk_test_chyPPuzAA1M3ETDVHz2vOBVd";
                    }
                    break;

                case "CRYSTALIFT":
                    if (live)
                    {
                        result = "sk_live_1H6a3g795zLOqRzzCpBgFKgD";
                    }
                    else
                    {
                        result = "sk_test_8ZCV8zNdXcYdcVZJyZHg1Tu4";
                    }
                    break;

                case "SONULASE":
                    if (live)
                    {
                        result = "sk_live_iCjkfkNrKAL9hUL7G91eubVU";
                    }
                    else
                    {
                        result = "sk_test_LARdwNWpW2C79HTOs3kemQGv";
                    }
                    break;

                case "RMSHOP":
                    if (live)
                    {
                        result = "sk_live_67eQpnuoCy868PDrEhJyPTSd";
                    }
                    else
                    {
                        result = "sk_test_4KXvPNiVK3EeVKdaUKSP90p2";
                    }
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        //<add key="StripeApiPublishableKey" value="pk_live_ZiT9j0WNf90uOeBCF6qcNYhb" />
        //<add key="TestStripeApiPublishableKey" value="pk_test_IDzMbQZ6ToUjjQKkF1WhgEpl" />
        static public string ApiPublishableKeyStripe()
        {
            string result = "undefined";

            string stripe_Live = ConfigurationManager.AppSettings["StripeLive"].ToString();
            bool live = (stripe_Live.ToUpper() == "YES") ? true : false;


            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    if (live)
                    {
                        result = "pk_live_ZiT9j0WNf90uOeBCF6qcNYhb";
                    }
                    else
                    {
                        result = "pk_test_IDzMbQZ6ToUjjQKkF1WhgEpl";
                    }
                    break;

                case "CRYSTALIFT":
                    if (live)
                    {
                        result = "pk_live_EKtGpkTpe2m5kfTOgRhU3mpT";
                    }
                    else
                    {
                        result = "pk_test_UcuMwzH1yQOn4SNhwcLlacd9";
                    }
                    break;

                case "SONULASE":
                    if (live)
                    {
                        result = "pk_live_3n19x9kLTt6ML67KCbO3cW0b";
                    }
                    else
                    {
                        result = "pk_test_o7h1VtbvSUWw8zcwDMdzmWCx";
                    }
                    break;

                case "RMSHOP":
                    if (live)
                    {
                        //result = "pk_live_Pm5jbgInfwq8pJNnKN7ydqPl";
                        result = "pk_test_9AQaR6uf49IyVz0fogCzPjv2";
                    }
                    else
                    {
                        result = "pk_test_9AQaR6uf49IyVz0fogCzPjv2";
                    }
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        
        static public int SiteID = 1;

        static public string TheUser()
        {
            return "rmohme";
        }

        /*******************************************************
        // MAIL STUFF
        ********************************************************/

        static public string MedicalMailAddress = ConfigurationManager.AppSettings["MedicalMailAddress"].ToString();
        static public string QAMailAddress = ConfigurationManager.AppSettings["QAMailAddress"].ToString();

        static public string SMTPServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();
        static public string SMTPLogin = ConfigurationManager.AppSettings["SMTPLogin"].ToString();
        static public string SMTPPassword = ConfigurationManager.AppSettings["SMTPPassword"].ToString();

        static public string MailTo = ConfigurationManager.AppSettings["MailTo"].ToString(); //default site admin

        //<add key="MailFrom" value="facemaster@slccompanies.com" />
        //static public string MailFrom = ConfigurationManager.AppSettings["MailFrom"].ToString();
        static public string MailFrom()
        {
            string result = "undefined";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "facemaster@slccompanies.com";
                    break;

                case "CRYSTALIFT":
                    //result = "crystalift@lifeinnovationsco.com";
                    result = "facemaster@slccompanies.com";
                    break;

                case "SONULASE":
                    //result = "mycrystalift@slccompanies.com";
                    result = "facemaster@slccompanies.com";
                    break;

                case "RMSHOP":
                    //result = "mycrystalift@slccompanies.com";
                    result = "facemaster@slccompanies.com";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        //static public string CustomerServiceEmail = ConfigurationManager.AppSettings["CustomerServiceEmail"].ToString();
        //<add key="CustomerServiceEmail" value="FaceMaster@slccompanies.com" />
        static public string CustomerServiceEmail()
        {
            string result = "undefined";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "FaceMaster@slccompanies.com";
                    break;

                case "CRYSTALIFT":
                    result = "crystalift@lifeinnovationsco.com";
                    break;

                case "SONULASE":
                    result = "sonulase@lifeinnovationsco.com";
                    break;

                case "RMSHOP":
                    result = "rmohme@gmail.com";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        /*******************************************************
        // FUFILLMENT STUFF
        ********************************************************/
        static public string genPDFPackingList_StoragePath = @"~/App_Data/genPDFs/PackingLists/";
        static public string GetBarCodeFontLocation()
        {
            return HttpContext.Current.Server.MapPath(@"~/Content/fonts/WaspFonts/");
        }

        static public string PackingListPrefix()
        {
            string result = "undefined";

            switch (appName.ToUpper())
            {
                case "FACEMASTER":
                    result = "FM_";
                    break;

                case "CRYSTALIFT":
                    result = "CL_";
                    break;

                case "SONULASE":
                    result = "SU_";
                    break;

                case "RMSHOP":
                    result = "RM_";
                    break;

                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        /*******************************************************
        // TEST HELPER STUFF
        ********************************************************/
        static public string TestWebApi_URL()
        {
            string result = "undefined";

            string aWS_Deploy = ConfigurationManager.AppSettings["AWS-Deploy"].ToString();
            bool deploy = (aWS_Deploy.ToUpper() == "YES") ? true : false;

            if (!deploy)
            {
                result = "http://localhost:56407/api/ShopifyAgent/GetPing";
            }
            else
            {
                switch (appName.ToUpper())
                {
                    case "FACEMASTER":
                        result = "http://fmadmin.facemaster.com/api/ShopifyAgent/GetPing";
                        break;

                    case "CRYSTALIFT":
                        result = "http://52.25.186.65/api/ShopifyAgent/GetPing";
                        break;

                    case "SONULASE":
                        result = "http://admin.mysonulase.com/api/ShopifyAgent/GetPing";
                        break;

                    case "RMSHOP":
                        result = "http://admin.fm-cer.com/api/ShopifyAgent/GetPing";
                        break;

                    default:
                        throw new NotImplementedException();
                }
            }
            return result;
        }

        static public string TestWebApi_ActivateDeferredSubscriptions()
        {
            string result = "undefined";

            string aWS_Deploy = ConfigurationManager.AppSettings["AWS-Deploy"].ToString();
            bool deploy = (aWS_Deploy.ToUpper() == "YES") ? true : false;
            string urlStr = "api/apiSubscriptions/CheckDeferredSubscriptions?isTest=true";
            if (!deploy)
            {
                result = "http://localhost:56407/" + urlStr;
            }
            else
            {
                switch (appName.ToUpper())
                {
                    case "FACEMASTER":
                        result = "https://fmadmin.facemaster.com/" + urlStr;
                        break;

                    case "CRYSTALIFT":
                        result = "https://52.25.186.65/" + urlStr;
                        break;

                    case "SONULASE":
                        result = "http://admin.mysonulase.com/" + urlStr;
                        break;

                    case "RMSHOP":
                        result = "http://admin.fm-cer.com/" + urlStr;
                        break;

                    default:
                        throw new NotImplementedException();
                }
            }
            return result;
        }


        /*******************************************************
        // MISC STUFF
        ********************************************************/
        static public string OrderSearchTip = @"<h4>Search For:</h4>
                <ul>
                    <li><strong>Order#</strong>: type: integer (ex. 91733)</li>
                    <li><strong>Shopify Order#</strong>: type: integer (ex. 801122)</li>
                    <li><strong>Date Range</strong>: type: Date; start Date to now (ex. use single date, like 1/1/2015 to get orders from then to now)</li>
                    <li><strong>Date Range</strong>: type: Date; start Date to end Date (ex. 1/1/2015 6/30/2015 to get orders between the dates)</li>
                    <li><strong>Last Name</strong>: type: string; orders with customers like Last Name (ex. smith)
                        <ul>
                            <li>abbreviations are OK; for example 'smi' will find customers with those letters in the last name</li>
                        </ul>
                    </li>
                    <li><strong>Last Name First Name</strong>:  type: string; orders with customers like Last Name and First Name(ex.smith john)</li>
                    <li><strong>First Name</strong>:  type: * + string; orders with customers like  First Name(ex. * john)
                        <ul>
                            <li>must have an* somehwere in the string</li>
                            <li>abbreviations OK</li>
                        </ul>
                    </li>
                    <li><strong>Email</strong>:  type: string; orders with customers' email (ex. rmohme&#64;alumni.uci.edu)
                        <ul>
                            <li>must have an &#64; somehwere in the string</li>
                            <li>abbreviations OK; for example &#64; will find all customers with email addresses with &#64;alumni in them</li>
                        </ul>
                    </li>
                    <li><strong>Device Serial Number</strong>:  type: &#35; + long integer; orders with serial number (ex. &#35;61312103422)
                        <ul>
                            <li>must have an &#35; as the first character</li>
                            <li>abbreviations OK; for example &#35;103422 will find all orders with '103422' somewhere in the serial number</li>
                        </ul>
                    </li>
                </ul>
        ";

        static public string CustomerSearchTip = @"<h4>Search For:</h4>
                <ul>
                    <li><strong>Order#</strong>: type: integer (ex. 91733)</li>
                    <li><strong>Shopify Order#</strong>: type: integer (ex. 801122)</li>
                    <li><strong>Date Range</strong>: type: Date; start Date to now (ex. use single date, like 1/1/2015 to get orders from then to now)</li>
                    <li><strong>Date Range</strong>: type: Date; start Date to end Date (ex. 1/1/2015 6/30/2015 to get orders between the dates)</li>
                    <li><strong>Last Name</strong>: type: string; orders with customers like Last Name (ex. smith)
                        <ul>
                            <li>abbreviations are OK; for example 'smi' will find customers with those letters in the last name</li>
                        </ul>
                    </li>
                    <li><strong>Last Name First Name</strong>:  type: string; orders with customers like Last Name and First Name(ex.smith john)</li>
                    <li><strong>First Name</strong>:  type: * + string; orders with customers like  First Name(ex. * john)
                        <ul>
                            <li>must have an* somehwere in the string</li>
                            <li>abbreviations OK</li>
                        </ul>
                    </li>
                    <li><strong>Email</strong>:  type: string; orders with customers' email (ex. rmohme&#64;alumni.uci.edu)
                        <ul>
                            <li>must have an &#64; somehwere in the string</li>
                            <li>abbreviations OK; for example &#64; will find all customers with email addresses with &#64;alumni in them</li>
                        </ul>
                    </li>
                    <li><strong>Device Serial Number</strong>:  type: &#35; + long integer; orders with serial number (ex. &#35;61312103422)
                        <ul>
                            <li>must have an &#35; as the first character</li>
                            <li>abbreviations OK; for example &#35;103422 will find all orders with '103422' somewhere in the serial number</li>
                        </ul>
                    </li>
                </ul>
        ";

        static public string SubscriberSearchTip = @"<h4>Search For:</h4>
                <ul>
                    <li><strong>Order#</strong>: type: integer (ex. 91733)</li>
                    <li><strong>Stripe Subscription ID</strong>: type: string (ex. sub_DR6DCHXRvNppds)</li>
                    <li><strong>Stripe Customer ID</strong>: type: string (ex. cus_DR5u9TmJiOQyU7)</li>
                </ul>
        ";

        static public string MPSearchTip = @"<h4>Search For:</h4>
                <ul>
                    <li><strong>Date Range</strong>: type: Date; start Date to now (ex. use single date, like 1/1/2015 to get orders from then to now)</li>
                    <li><strong>Date Range</strong>: type: Date; start Date to end Date (ex. 1/1/2015 6/30/2015 to get orders between the dates)</li>
                </ul>
        ";

        static public string SerNumSearchTip = @"<h4>Search For:</h4>
                <ul>
                    <li><strong>Date Range</strong>: type: Date; start Date to now (ex. use single date, like 1/1/2015 to get orders from then to now)</li>
                    <li><strong>Date Range</strong>: type: Date; start Date to end Date (ex. 1/1/2015 6/30/2015 to get orders between the dates)</li>
                    <li><strong>Device Serial Number</strong>:  type: &#35; + long integer (ex. &#35;61312103422)
                        <ul>
                            <li>must have an &#35; as the first character</li>
                            <li>abbreviations OK; for example &#35;613121 will find all orders with '613121' somewhere in the serial number</li>
                        </ul>
                    </li>
                </ul>
        ";

        static public string FAQSearchTip = @"<h4>Search For:</h4>
                <ul>
                    <li><strong>Order#</strong>: type: integer (ex. 91733)</li>
                    <li><strong>Shopify Order#</strong>: type: integer (ex. 801122)</li>
                    <li><strong>Date Range</strong>: type: Date; start Date to now (ex. use single date, like 1/1/2015 to get orders from then to now)</li>
                    <li><strong>Date Range</strong>: type: Date; start Date to end Date (ex. 1/1/2015 6/30/2015 to get orders between the dates)</li>
                    <li><strong>Last Name</strong>: type: string; orders with customers like Last Name (ex. smith)
                        <ul>
                            <li>abbreviations are OK; for example 'smi' will find customers with those letters in the last name</li>
                        </ul>
                    </li>
                    <li><strong>Last Name First Name</strong>:  type: string; orders with customers like Last Name and First Name(ex.smith john)</li>
                    <li><strong>First Name</strong>:  type: * + string; orders with customers like  First Name(ex. * john)
                        <ul>
                            <li>must have an* somehwere in the string</li>
                            <li>abbreviations OK</li>
                        </ul>
                    </li>
                    <li><strong>Email</strong>:  type: string; orders with customers' email (ex. rmohme&#64;alumni.uci.edu)
                        <ul>
                            <li>must have an &#64; somehwere in the string</li>
                            <li>abbreviations OK; for example &#64; will find all customers with email addresses with &#64;alumni in them</li>
                        </ul>
                    </li>
                    <li><strong>Device Serial Number</strong>:  type: &#35; + long integer; orders with serial number (ex. &#35;61312103422)
                        <ul>
                            <li>must have an &#35; as the first character</li>
                            <li>abbreviations OK; for example &#35;103422 will find all orders with '103422' somewhere in the serial number</li>
                        </ul>
                    </li>
                </ul>
        ";


        static public string MyAccountLinks = @"<br /><br /><br />
            <a class='act' href='MyAccountHome.aspx'>My Account Home</a><br /><br />
            <a class='act' href='MyAccount.aspx'>Edit Profile</a><br /><br />
            <a class='act' href='MyAddress.aspx'>Edit Addresses</a><br /><br />
            <a class='act' href='MyOrders.aspx'>My Orders</a><br /><br />
        </span>
        ";

        static public string ReturnExchangePolicy = @"
In order for us to credit your account, Returns and Exchanges MUST BE accompanied by a Return Merchandise Authorization (RMA) number and received within sixty (60) days from the date of purchase.  To get your RMA Number, please log on to **WEBSITE**, and click on Contact Us.
        ";

        //
        // PAYPAL STUFF
        //        
        //public static string PayPalServer = "payflowpro.paypal.com";
        public static string PayPalServer = ConfigurationManager.AppSettings["PayPalServer"].ToString();

        //public static string PayPalTestServer = "pilot-payflowpro.paypal.com";
        public static string PayPalTestServer = ConfigurationManager.AppSettings["PayPalTestServer"].ToString();

        /* old fm (as of June 23, 2014) */
        //login URL --> manager.paypal.com
        //static public string oldPayPalUser = "Facemaster1";
        //static public string oldPayPalVendor = "Facemaster1";
        //static public string oldPayPalPartner = "centurybank";
        //static public string oldPayPalPassword = "Master13";

        /*new FM (June 23, 2014) * https://manager.paypal.com/ */
        //static public string PayPalUser = "facemaster"; //leave blank
        static public string PayPalUser = ConfigurationManager.AppSettings["PayPalUser"].ToString();
        //static public string PayPalVendor = "facemaster";
        static public string PayPalVendor = ConfigurationManager.AppSettings["PayPalVendor"].ToString();
        //static public string PayPalPartner = "ccindustry";
        static public string PayPalPartner = ConfigurationManager.AppSettings["PayPalPartner"].ToString();
        //static public string PayPalPassword = "Hello101!";
        static public string PayPalPassword = ConfigurationManager.AppSettings["PayPalPassword"].ToString();

        /*
         * 
         * Security Questions:
         * 1. Who is your nearest relative? [Richard]
         * 2. What was the name of your first roommate? [Tom]
         * 3. What was the name of the high school you attended? [San Clemente]
         * 
        From: Codemo, Dave [mailto:dcodemo@paypal.com] 
        Sent: Monday, May 05, 2014 9:42 AM
        To: Sargon Givargis (818) 702-0024
        Subject: New account - LOGIN: facemaster
 
        Here you go. The account is LIVE and will be ready to process in 1 hour. 
 
        https://manager.paypal.com/
        Partner: ccindustry
        Login: facemaster
        User: (Leave blank)
        Password: Welcome1! - you will need to log into the account and reset.
         * Hello101!
  
        Dave Codemo
        Phone: 408.967.0337
        Fax: 928.752.6479
 
        PP_h_logo_30x123
        2211 North First Street
        San Jose, CA 95131
        */

    } 
}
