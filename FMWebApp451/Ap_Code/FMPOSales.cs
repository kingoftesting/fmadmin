using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;


/// <summary>
/// Summary description for FMPOSales
/// </summary>
public class FMPOSales
{
    #region Private/Public Vars
    private int id;
    public int ID { get { return id; } set { id = value; } }
    private DateTime date;
    public DateTime Date { get { return date; } set { date = value; } }
    private string customer;
    public string Customer { get { return customer; } set { customer = value; } }
    private decimal devRevenue;
    public decimal DevRevenue { get { return devRevenue; } set { devRevenue = value; } }
    private int devUnits;
    public int DevUnits { get { return devUnits; } set { devUnits = value; } }
    private decimal accyRevenue;
    public decimal AccyRevenue { get { return accyRevenue; } set { accyRevenue = value; } }
    #endregion

    public FMPOSales()
    {
        ID = 0;
        Date = Convert.ToDateTime("1/1/1900");
        Customer = "n/a";
        DevRevenue = 0;
        DevUnits = 0;
        AccyRevenue = 0;
    }

    public FMPOSales(int IDfield)
    {
        string sql = @"
        SELECT * FROM FMPOSales
        WHERE ID = @ID 
        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, IDfield);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
        if (dr.Read())
        {
            ID = Convert.ToInt32(dr["ID"]);
            Date = Convert.ToDateTime(dr["Date"].ToString());
            Customer = dr["Customer"].ToString();
            DevRevenue = Convert.ToDecimal(dr["DevRevenue"].ToString());
            DevUnits = Convert.ToInt32(dr["DevUnits"].ToString());
            AccyRevenue = Convert.ToDecimal(dr["AccyRevenue"].ToString());
        }
        if (dr != null) { dr.Close(); }
    }

    public int Save(int ID)
    {
        int result = 0;
        SqlParameter[] mySqlParameters = null;
        SqlDataReader dr = null;
        if (ID == 0)
        {
            string sql = @"
            INSERT INTO FMPOSales 
            (Date, Customer, DevRevenue, DevUnits, AccyRevenue) 
            VALUES(@Date, @Customer, @DevRevenue, @DevUnits, @AccyRevenue); 
            SELECT ID=@@identity; 
            ";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, Date, Customer, DevRevenue, DevUnits, AccyRevenue);
            dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                result = Convert.ToInt32(dr["ID"]);
            }
            else
            {
                result = 99;
            }
        }
        else
        {
            string sql = @"
            UPDATE FMPOSales
            SET
                Date = @Date,
                Customer = @Customer, 
                DevRevenue = @DevRevenue, 
                DevUnits = @DevUnits, 
                AccyRevenue = @AccyRevenue 
            WHERE ID = @ID 
            ";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, Date, Customer, DevRevenue, DevUnits, AccyRevenue, ID);
            DBUtil.Exec(sql, mySqlParameters);
        }
        return result;
    }

    /***********************************
    * Static methods
    ************************************/

    /// <summary>
    /// Returns a collection with all FMPOSales
    /// </summary>
    public static List<FMPOSales> GetFMPOSalesList()
    {
        List<FMPOSales> thelist = new List<FMPOSales>();

        string sql = @"
        SELECT * FROM FMPOSales
        ORDER BY DATE DESC 
        ";

        SqlDataReader dr = DBUtil.FillDataReader(sql);

        while (dr.Read())
        {
            FMPOSales obj = new FMPOSales();

            obj.ID = Convert.ToInt32(dr["ID"]);
            obj.Date = Convert.ToDateTime(dr["Date"].ToString());
            obj.Customer = dr["Customer"].ToString();
            obj.DevRevenue = Convert.ToDecimal(dr["DevRevenue"].ToString());
            obj.DevUnits = Convert.ToInt32(dr["DevUnits"].ToString());
            obj.AccyRevenue = Convert.ToDecimal(dr["AccyRevenue"].ToString());

            thelist.Add(obj);
        }
        if (dr != null) { dr.Close(); }
        return thelist;
    }

    /// <summary>
    /// Returns a HarvestResult object with the specified ID
    /// </summary>

    public static FMPOSales GetFMPOSalesByID(int id)
    {
        FMPOSales obj = new FMPOSales(id);
        return obj;
    }

    /// <summary>
    /// Updates an existing FMPOSales
    /// </summary>
    public static bool UpdateFMPOSales(int id,
    DateTime date, string customer, decimal devrevenue, int devunits, decimal accyrevenue)
    {
        FMPOSales obj = new FMPOSales();

        obj.ID = id;
        obj.Date = date;
        obj.Customer = customer;
        obj.DevRevenue = devrevenue;
        obj.DevUnits = devunits;
        obj.AccyRevenue = accyrevenue;
        int ret = obj.Save(obj.ID);
        return (ret > 0) ? true : false;
    }

    /// <summary>
    /// Creates a new FMPOSales
    /// </summary>
    public static int InsertFMPOSales(
    DateTime date, string customer, decimal devrevenue, int devunits, decimal accyrevenue)
    {
        FMPOSales obj = new FMPOSales();

        obj.Date = date;
        obj.Customer = customer;
        obj.DevRevenue = devrevenue;
        obj.DevUnits = devunits;
        obj.AccyRevenue = accyrevenue;
        int ret = obj.Save(obj.ID);
        return ret;
    }

}
