using System;
using System.Data;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Caching;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using AdminCart;
using Stripe;

// *** Helper Methods
//
// string _countries
// String Collection GetCountries()
// SortedList GetCountries(bool insertEmpty)
// string[] GetThemes()
// void SetInputControlsHighlight(Control container, string className, bool onlyTextBoxes)
// string ConvertToHtml(string content)
// void SendCSV(DataView dv)
// string GetTableText(DataView dv)
// string GetMtlIDTableText(DataView dv)
// string SizeString(string s, int length)
// string GetSKUDescription(string sku)
// string GetMtlID(string sku)
// DataView BuildDataTable<T>(List<T>) - convert Generic List to DataTable
// DataTable GetSortedDTfromDV(DataView dv)
// string ConvertDataViewToHTML (DataView dv)
// string ConvertDataViewToHTML(DataView dv, string lastColumn)
// DataTable GetSymptomCollection()
// string GetSymptomName(int value)
// DataTable GetDefectCategoryCollection()
// string GetDefectCategoryName(int value)
// DataTable GetDefectSubCategoryCollection()
// string GetDefectSubCategoryName(int value)
// string GetReasonName(int value)
// string GetReason2Name(int value)
// string GetBatchID(int FMOrderID)
// string GetShipDate(int orderid)
// void AddSerialNum(int FMOrderID, string serialnum, DateTime date)
// void AddTrackNum(int FMOrderID, double weight, double upscharge, string tracknum, string uploadby)
// AddHDITrackNum(int HDIOrderID, string tracknum)
// GetMaxFMBatch()
// RedirectToNewPage(Page page, string redirectURL)
//CancelOrder(int OrderID, string CancelledBy)
//
// ***


namespace Utilities
{
    public static class FMHelpers
    {
        private static string[] _countries = new string[] { 
         "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", 
         "Angola", "Anguilla", "Antarctica", "Antigua And Barbuda", "Argentina", 
         "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan",
		   "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus",
		   "Belgium", "Belize", "Benin", "Bermuda", "Bhutan",
		   "Bolivia", "Bosnia Hercegovina", "Botswana", "Bouvet Island", "Brazil",
		   "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Byelorussian SSR",
		   "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands",
		   "Central African Republic", "Chad", "Chile", "China", "Christmas Island",
		   "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Cook Islands",
		   "Costa Rica", "Cote D'Ivoire", "Croatia", "Cuba", "Cyprus",
		   "Czech Republic", "Czechoslovakia", "Denmark", "Djibouti", "Dominica",
		   "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador",
		   "England", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia",
		   "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France",
		   "Gabon", "Gambia", "Georgia", "Germany", "Ghana",
		   "Gibraltar", "Great Britain", "Greece", "Greenland", "Grenada",
		   "Guadeloupe", "Guam", "Guatemela", "Guernsey", "Guiana",
		   "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Islands",
		   "Honduras", "Hong Kong", "Hungary", "Iceland", "India",
		   "Indonesia", "Iran", "Iraq", "Ireland", "Isle Of Man",
		   "Israel", "Italy", "Jamaica", "Japan", "Jersey",
		   "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, South",
		   "Korea, North", "Kuwait", "Kyrgyzstan", "Lao People's Dem. Rep.", "Latvia",
		   "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein",
		   "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar",
		   "Malawi", "Malaysia", "Maldives", "Mali", "Malta",
		   "Mariana Islands", "Marshall Islands", "Martinique", "Mauritania", "Mauritius",
		   "Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco",
		   "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar",
		   "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles",
		   "Neutral Zone", "New Caledonia", "New Zealand", "Nicaragua", "Niger",
		   "Nigeria", "Niue", "Norfolk Island", "Northern Ireland", "Norway",
		   "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea",
		   "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland",
		   "Polynesia", "Portugal", "Puerto Rico", "Qatar", "Reunion",
		   "Romania", "Russian Federation", "Rwanda", "Saint Helena", "Saint Kitts",
		   "Saint Lucia", "Saint Pierre", "Saint Vincent", "Samoa", "San Marino",
		   "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Seychelles",
		   "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands",
		   "Somalia", "South Africa", "South Georgia", "Spain", "Sri Lanka",
		   "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden",
		   "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikista", "Tanzania",
		   "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago",
		   "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu",
		   "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States",
		   "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City State", "Venezuela",
		   "Vietnam", "Virgin Islands", "Wales", "Western Sahara", "Yemen",
		   "Yugoslavia", "Zaire", "Zambia", "Zimbabwe"};

        /// <summary>
        /// Returns an array with all countries
        /// </summary>     
        public static StringCollection GetCountries()
        {
            StringCollection countries = new StringCollection();
            countries.AddRange(_countries);
            return countries;
        }
        
        public static SortedList GetCountries(bool insertEmpty)
        {
            SortedList countries = new SortedList();
            if (insertEmpty)
                countries.Add("", "Please select one...");
            foreach (String country in _countries)
                countries.Add(country, country);
            return countries;
        }

        public static string MonthLookUp(int monthNum)
        {
            string month = "";
            string[] _monthArray =
            {
                "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
            };

            if ((monthNum > 0) && (monthNum <= 12))
            {
                month = _monthArray[monthNum-1];
            }

            return month;
        }

        /// <summary>
        /// Returns an array with the names of all local Themes
        /// </summary>
        public static string[] GetThemes()
        {
            if (HttpContext.Current.Cache["SiteThemes"] != null)
            {
                return (string[])HttpContext.Current.Cache["SiteThemes"];
            }
            else
            {
                string themesDirPath = HttpContext.Current.Server.MapPath("~/App_Themes");
                // get the array of themes folders under /app_themes
                string[] themes = Directory.GetDirectories(themesDirPath);
                for (int i = 0; i <= themes.Length - 1; i++)
                    themes[i] = Path.GetFileName(themes[i]);
                // cache the array with a dependency to the folder
                CacheDependency dep = new CacheDependency(themesDirPath);
                HttpContext.Current.Cache.Insert("SiteThemes", themes, dep);
                return themes;
            }
        }

        /// <summary>
        /// Adds the onfocus and onblur attributes to all input controls found in the specified parent,
        /// to change their apperance with the control has the focus
        /// </summary>
        public static void SetInputControlsHighlight(Control container, string className, bool onlyTextBoxes)
        {
            foreach (Control ctl in container.Controls)
            {
                if ((onlyTextBoxes && ctl is TextBox) || ctl is TextBox || ctl is DropDownList ||
                    ctl is ListBox || ctl is CheckBox || ctl is RadioButton ||
                    ctl is RadioButtonList || ctl is CheckBoxList)
                {
                    WebControl wctl = ctl as WebControl;
                    wctl.Attributes.Add("onfocus", string.Format("this.className = '{0}';", className));
                    wctl.Attributes.Add("onblur", "this.className = '';");
                }
                else
                {
                    if (ctl.Controls.Count > 0)
                    {
                        ; //SetInputControlsHighlight(ctl, className, onlyTextBoxes);
                    }
                }
            }
        }


        /// <summary>
        /// Converts the input plain-text to HTML version, replacing carriage returns
        /// and spaces with <br /> and &nbsp;
        /// </summary>
        public static string ConvertToHtml(string content)
        {
            content = HttpUtility.HtmlEncode(content);
            content = content.Replace("  ", "&nbsp;&nbsp;").Replace(
               "\t", "&nbsp;&nbsp;&nbsp;").Replace("\n", "<br>");
            return content;
        }

        static public void SendCSV(DataView dv)
        {
            System.Web.HttpContext.Current.Response.Clear();
            DateTime rundate = DateTime.Now;

            string year = rundate.Year.ToString();
            string month = rundate.Month.ToString();
            string day = rundate.Day.ToString();
            string hour = rundate.Hour.ToString();
            string minute = rundate.Minute.ToString();

            string datedfilename = "Response_Data_" + year + "_" + month + "_" + day + "_" + hour + "_" + minute + ".csv";
            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + datedfilename);
            //Response.AddHeader("Content-Length", file.Length.ToString())  
            System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream";

            StringBuilder csvoutput = new StringBuilder("");

            DataTable dt = GetSortedDTfromDV(dv);

            int ColumnCount = dt.Columns.Count;

            StringBuilder columnlist = new StringBuilder("");

            foreach (DataColumn c in dt.Columns)
            {
                columnlist.Append(c.ColumnName + ",");
            }

            csvoutput.AppendLine(columnlist.ToString());

            foreach (DataRow r in dt.Rows)
            {
                StringBuilder line = new StringBuilder("");

                for (int i = 0; i < ColumnCount; i++)
                {
                    //DataColumn c = (DataColumn)r[i];

                    line.Append("\"" + r[i].ToString().Replace(Environment.NewLine, "") + "\",");
                }

                csvoutput.AppendLine(line.ToString());

            }
            System.Web.HttpContext.Current.Response.Write(csvoutput);
            //Response.BinaryWrite();
            System.Web.HttpContext.Current.Response.End();
        }

        static public string GetTableText(DataView dv)
        {
            StringBuilder output = new StringBuilder("");

            DataTable dt = GetSortedDTfromDV(dv);

            int ColumnCount = dt.Columns.Count;

            StringBuilder columnlist = new StringBuilder("");

            string s = "";
            foreach (DataColumn c in dt.Columns)
            {
                s = c.ColumnName;
                s = SizeString(s, 6) + "  ";
                columnlist.Append(s);
            }
            output.AppendLine(columnlist.ToString());

            foreach (DataRow r in dt.Rows)
            {
                StringBuilder line = new StringBuilder("");
                for (int i = 0; i < ColumnCount; i++)
                {
                    s = r[i].ToString();
                    s = SizeString(s, 6) + "  ";
                    line.Append(s);
                }
                line.Append(Environment.NewLine);

                output.AppendLine(line.ToString());

            }
            return output.ToString();
        }

        static public string GetMtlIDTableText(DataView dv)
        {
            StringBuilder output = new StringBuilder("");

            DataTable dt = GetSortedDTfromDV(dv);

            int ColumnCount = dt.Columns.Count;

            StringBuilder columnlist = new StringBuilder("");

            columnlist.Append("MtlID " + " ");
            columnlist.Append("Desc  " + " ");
            columnlist.Append("Orders" + " ");
            columnlist.Append("Qty   " + " ");
            columnlist.Append("Rev     " + " ");
            columnlist.Append("WebOrd" + " ");
            columnlist.Append("WebQty" + " ");
            columnlist.Append("WebRev  " + " ");
            columnlist.Append("PhOrd " + " ");
            columnlist.Append("PhQty " + " ");
            columnlist.Append("PhRev   ");
            output.AppendLine(columnlist.ToString());

            string s = "";
            decimal d1 = 0;
            foreach (DataRow r in dt.Rows)
            {
                StringBuilder line = new StringBuilder("");
                s = r["SKU"].ToString();
                s = FMHelpers.GetMtlID(s);
                s = SizeString(s, 6);
                line.Append(s + " ");

                s = r["SKU"].ToString();
                s = FMHelpers.GetSKUDescription(s);
                s = SizeString(s, 6);
                line.Append(s + " ");

                s = r["Orders"].ToString();
                s = SizeString(s, 6);
                line.Append(s + " ");

                s = r["Units"].ToString();
                s = SizeString(s, 6);
                line.Append(s + " ");

                if (r["Revenue"] == DBNull.Value) { d1 = 0; }
                else { d1 = Convert.ToDecimal(r["Revenue"]); }
                s = string.Format("{0:$#,#}", d1);
                s = SizeString(s, 8);
                line.Append(s + " ");

                s = r["WebOrders"].ToString();
                s = SizeString(s, 6);
                line.Append(s + " ");

                s = r["WebUnits"].ToString();
                s = SizeString(s, 6);
                line.Append(s + " ");

                if (r["WebRevenue"] == DBNull.Value) { d1 = 0; }
                else { d1 = Convert.ToDecimal(r["WebRevenue"]); }
                s = string.Format("{0:$#,#}", d1);
                s = SizeString(s, 8);
                line.Append(s + " ");

                s = r["PhoneOrders"].ToString();
                s = SizeString(s, 6);
                line.Append(s + " ");

                s = r["PhoneUnits"].ToString();
                s = SizeString(s, 6);
                line.Append(s + " ");

                if (r["PhoneRevenue"] == DBNull.Value) { d1 = 0; }
                else { d1 = Convert.ToDecimal(r["PhoneRevenue"]); }
                s = string.Format("{0:$#,#}", d1);
                s = SizeString(s, 8);
                line.Append(s + " ");

                line.Append(Environment.NewLine);

                output.AppendLine(line.ToString());

            }
            return output.ToString();
        }

        static public string SizeString(string s, int length)
        {
            if (s.Length > length) { s = s.Substring(0, length); }
            else
            {
                int t = s.Length; //orig string length
                int i = 0;
                if (t < length) { for (i = 1; i < (length - t + 1); i++) { s += " "; } }
                t = s.Length; //new string length
            }
            return s;
        }

        static private string GetSKUDescription(string sku)
        {
            /*
      * 
 1	1457    FM-2000	        FaceMaster®
 2	1505    FM-21992299	    Conductive Solution and Foam Cap Combo
 3	1502    FM-2199	        Conductive Solution
 4	1548    FM-21992PC	    Conductive Solution - 2 Pack
 5	1503    FM-2299	        Foam Caps
 6	1504    FM-2299-2PC	    Foam Caps - 2 Pack
 7	1506    FM-2DVD	        Step by Step Instructional DVD
 8	1507    FM-2VT	        Step by Step Instructional VHS Tape
 9	1508    FM-2PROBE	    FaceMaster Replacement Probes
 10	1813    FM-2000-3	    FaceMaster® 3-Pay
 11	1812    FM-3001	        FaceMaster Collagen Enhancing Serum with Peptides
 12	1823    FM-4000	        FaceMaster Extended Warranty
 13	1825    FM-2000-3001    FaceMaster PLUS Collagen Enhancing Serum with Peptides
 14	1872    FM-5000         FaceMaster Platinum
 15	1873    FM-5150         Platinum Collagen Enhancing Serum with Peptides
 16	1874    FM-5100         FaceMaster Soothing Conductive Serum
 17	1875    FM-51002PC      FaceMaster Soothing Conductive Serum - 2 Pack
 18	1876    FM-51002199     FaceMaster Soothing Conductive Serum and Foam Cap Combo
 19	1877    FM-5800         FaceMaster Platinum Extended Warranty
 20	1878    FM-5240         FaceMaster Platinum Instructional Kit
 21	1879    FM-5050         FaceMaster Platinum Finger Wands
 22	1880    FM-5060         FaceMaster Platinum Hand Wands
      * */
            string desc = "";
            switch (sku)
            {
                case " ":
                    desc = "TOTALS";
                    break;

                case "FM-2000":
                    desc = "FM";
                    break;

                case "FM-21992299":
                    desc = "SFcomb";
                    break;

                case "FM-2299":
                    desc = "FCaps";
                    break;

                case "FM-2299-2PC":
                    desc = "FC2pk";
                    break;

                case "FM-2DVD":
                    desc = "FMDVD";
                    break;

                case "FM-2VT":
                    desc = "FMVID";
                    break;

                case "FM-2PROBE":
                    desc = "FMWand";
                    break;

                case "FM-2000-3":
                    desc = "FM3pay";
                    break;

                case "FM-3001":
                    desc = "ColSer";
                    break;

                case "FM-4000":
                    desc = "FMXWar";
                    break;

                case "FM-2000-3001":
                    desc = "FM+Col";
                    break;

                case "FM-5000":
                    desc = "PFM";
                    break;

                case "FM-5150":
                    desc = "PColl";
                    break;

                case "FM-5100":
                    desc = "PSerum";
                    break;

                case "FM-51002PC":
                    desc = "PS2pk";
                    break;

                case "FM-51002199":
                    desc = "PSFC2pk";
                    break;

                case "FM-5800":
                    desc = "PXWarr";
                    break;

                case "FM-5240":
                    desc = "PIKit";
                    break;

                case "FM-5050":
                    desc = "PFWand";
                    break;

                case "FM-5060":
                    desc = "PHWand";
                    break;

                default:
                    desc = "????";
                    break;

            }
            return desc;
        }

        static public string GetMtlID(string sku)
        {
            /*
      * 
 1	1457    FM-2000	        FaceMaster®
 2	1505    FM-21992299	    Conductive Solution and Foam Cap Combo
 3	1502    FM-2199	        Conductive Solution
 4	1548    FM-21992PC	    Conductive Solution - 2 Pack
 5	1503    FM-2299	        Foam Caps
 6	1504    FM-2299-2PC	    Foam Caps - 2 Pack
 7	1506    FM-2DVD	        Step by Step Instructional DVD
 8	1507    FM-2VT	        Step by Step Instructional VHS Tape
 9	1508    FM-2PROBE	    FaceMaster Replacement Probes
 10	1813    FM-2000-3	    FaceMaster® 3-Pay
 11	1812    FM-3001	        FaceMaster Collagen Enhancing Serum with Peptides
 12	1823    FM-4000	        FaceMaster Extended Warranty
 13	1825    FM-2000-3001    FaceMaster PLUS Collagen Enhancing Serum with Peptides
 14	1872    FM-5000         FaceMaster Platinum
 15	1873    FM-5150         Platinum Collagen Enhancing Serum with Peptides
 16	1874    FM-5100         FaceMaster Soothing Conductive Serum
 17	1875    FM-51002PC      FaceMaster Soothing Conductive Serum - 2 Pack
 18	1876    FM-51002199     FaceMaster Soothing Conductive Serum and Foam Cap Combo
 19	1877    FM-5800         FaceMaster Platinum Extended Warranty
 20	1878    FM-5240         FaceMaster Platinum Instructional Kit
 21	1879    FM-5050         FaceMaster Platinum Finger Wands
 22	1880    FM-5060         FaceMaster Platinum Hand Wands
      * */
            string desc = "";
            switch (sku)
            {
                case " ":
                    desc = " ";
                    break;

                case "FM-2000":
                    desc = "1457";
                    break;

                case "FM-2199":
                    desc = "1502";
                    break;

                case "FM-21992299":
                    desc = "1505";
                    break;

                case "FM-2299":
                    desc = "1503";
                    break;

                case "FM-21992PC":
                    desc = "1548";
                    break;

                case "FM-2299-2PC":
                    desc = "1504";
                    break;

                case "FM-2DVD":
                    desc = "1506";
                    break;

                case "FM-2VT":
                    desc = "1507";
                    break;

                case "FM-2PROBE":
                    desc = "1508";
                    break;

                case "FM-2000-3":
                    desc = "1813";
                    break;

                case "FM-3001":
                    desc = "1812";
                    break;

                case "FM-4000":
                    desc = "1823";
                    break;

                case "FM-2000-3001":
                    desc = "1825";
                    break;

                case "FM-5000":
                    desc = "1872";
                    break;

                case "FM-5150":
                    desc = "1873";
                    break;

                case "FM-5100":
                    desc = "1874";
                    break;

                case "FM-51002PC":
                    desc = "1875";
                    break;

                case "FM-51002199":
                    desc = "1876";
                    break;

                case "FM-5800":
                    desc = "1877";
                    break;

                case "FM-5240":
                    desc = "1878";
                    break;

                case "FM-5050":
                    desc = "1879";
                    break;

                case "FM-5060":
                    desc = "1880";
                    break;

                default:
                    desc = "????";
                    break;

            }
            return desc;
        }


        // Converts a generic list to a DataTable
        // <T> is the type of data in the list.
        // If you have a List<int>, for example, then call this as follows:
        // List<int> ListOfInt;
        // DataTable ListTable = BuildDataTable<int>(ListOfInt);       
        static public DataTable BuildDataTable<T>(IList<T> lst)
        {
            //create DataTable Structure
            DataTable tbl = CreateTable<T>();
            Type entType = typeof(T);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entType);
            //get the list item and add into the list
            foreach (T item in lst)
            {
                DataRow row = tbl.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }
                tbl.Rows.Add(row);
            }
            return tbl;
        }

        static private DataTable CreateTable<T>()
        {
            //T –> ClassName
            Type entType = typeof(T);
            //set the datatable name as class name
            DataTable tbl = new DataTable(entType.Name);
            //get the property list
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entType);
            foreach (PropertyDescriptor prop in properties)
            {
                //add property as column
                tbl.Columns.Add(prop.Name, prop.PropertyType);
            }
            return tbl;
        }

        /*
         * GetSortedDTfromDV assumes a DataView input using DataView.Sort
         * In order to generate a DataTable that contains the sorted rows, an
         * intermediate GridView is used, with the Dataview as the gridview's source.
        */
        static public DataTable GetSortedDTfromDV(DataView dv)
        {

            GridView gv = new GridView();
            gv.DataSource = dv;
            gv.DataBind();

            DataTable dt = dv.Table.Clone();

            GridViewRow[] Chartarr = new GridViewRow[gv.Rows.Count];
            gv.Rows.CopyTo(Chartarr, 0);

            string s = "";
            foreach (GridViewRow row in Chartarr)
            {
                DataRow datarw;
                datarw = dt.NewRow();
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    s = row.Cells[i].Text;
                    if (string.Equals(s, "&nbsp;"))
                    {
                        Type ptype = datarw[i].GetType();
                        datarw[i] = DBNull.Value;
                    }
                    else { datarw[i] = s; }
                }

                dt.Rows.Add(datarw);
            }

            return dt;
        }

        static public string ConvertDataViewToHTML(DataView dv)
        {
            DataTable dt = Utilities.FMHelpers.GetSortedDTfromDV(dv);

            int ColumnCount = dt.Columns.Count;

            string contactBody = "<table class='ReportHelper'>";
            contactBody += "<tr bgcolor='#CCCCFF'>";

            foreach (DataColumn c in dt.Columns)
            {
                contactBody += "<td align='center'>" + c.ColumnName + "</td>";
            }
            contactBody += "</tr>";
            string bgcolor = "";
            int rowcount = 1;
            string rowelement = "";

            foreach (DataRow r in dt.Rows)
            {
                if (rowcount % 2 == 0) { bgcolor = "bgcolor='#CCCCFF' "; }
                else { bgcolor = "bgcolor='white' "; }
                contactBody += "<tr " + bgcolor + ">";

                for (int i = 0; i < ColumnCount; i++)
                {
                    rowelement = r[i].ToString().Replace(Environment.NewLine, ""); //default value to print
                    if ((r[i].GetType() == System.Type.GetType("System.Decimal")) || (r[i].GetType() == System.Type.GetType("System.Double")))
                    {
                        rowelement = r[i].ToString();
                        Double d = Convert.ToDouble(rowelement);
                        rowelement = d.ToString("#0.00");
                    }
                    contactBody += "<td align='center'>" + rowelement + "</td>";
                }
                contactBody += "</tr>";
                ++rowcount;
            }

            contactBody += "</table>";
            contactBody += "<br /><br />The above is an automated report sent to you from FaceMaster.com <br />";

            return contactBody;

        }

        static public string ConvertDataViewToHTML(DataView dv, string lastColumn)
        {
            DataTable dt = Utilities.FMHelpers.GetSortedDTfromDV(dv);

            int ColumnCount = 0;

            string contactBody = "<table class='ReportHelper'>";
            contactBody += "<tr bgcolor='#CCCCFF'>";

            foreach (DataColumn c in dt.Columns)
            {
                contactBody += "<td align='center'>" + c.ColumnName + "</td>";
                if (c.ColumnName == lastColumn) { break; }
                ++ColumnCount;
            }
            contactBody += "</tr>";
            string bgcolor = "";
            int rowcount = 1;
            string rowelement = "";

            foreach (DataRow r in dt.Rows)
            {
                if (rowcount % 2 == 0) { bgcolor = "bgcolor='#CCCCFF' "; }
                else { bgcolor = "bgcolor='white' "; }
                contactBody += "<tr " + bgcolor + ">";

                for (int i = 0; i <= ColumnCount; i++)
                {
                    rowelement = r[i].ToString().Replace(Environment.NewLine, ""); //default value to print
                    if ((r[i].GetType() == System.Type.GetType("System.Decimal")) || (r[i].GetType() == System.Type.GetType("System.Double")))
                    {
                        rowelement = r[i].ToString();
                        decimal d = Convert.ToDecimal(rowelement);
                        rowelement = d.ToString("C");
                    }
                    contactBody += "<td align='center'>" + rowelement + "</td>";
                }
                contactBody += "</tr>";
                ++rowcount;
            }

            contactBody += "</table>";

            return contactBody;

        }

        static public DataTable GetSymptomCollection()
        {
            string sql = @"
            SELECT * 
            FROM Symptoms  
            ";

            //SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, username);
            DataSet ds = new DataSet();
            try
            {
                ds = FM2015.Helpers.DBUtil.FillDataSet(sql, "table1", Config.ConnStrCER());
            }
            catch (Exception ex)
            {
                string s = "Exception during Helpers.GetSymptoms: " + Environment.NewLine;
                s += "sql: " + sql + Environment.NewLine;
                s += "Exception Message: " + ex.Message;
            }
            finally
            {
            }

            return ds.Tables["table1"];
        }

        static public string GetSymptomName(int value)
        {
            string sql = @"
            SELECT * 
            FROM Symptoms 
            WHERE Value = @Value  
            ";

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, value);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);
            string name = "N/A";
            if (dr.Read()) { name = dr["Name"].ToString(); }
            if (dr != null) { dr.Close(); }
            return name;
        }

        static public DataTable GetDefectCategoryCollection()
        {
            string sql = @"
            SELECT * 
            FROM DefectCategory   
            ";

            //SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, username);
            DataSet ds = new DataSet();
            try
            {
                ds = FM2015.Helpers.DBUtil.FillDataSet(sql, "table1", Config.ConnStrCER());
            }
            catch (Exception ex)
            {
                string s = "Exception during Helpers.GetDefectCategoryCollection: " + Environment.NewLine;
                s += "sql: " + sql + Environment.NewLine;
                s += "Exception Message: " + ex.Message;
            }
            finally
            {
            }

            return ds.Tables["table1"];
        }

        static public string GetDefectCategoryName(int value)
        {
            string sql = @"
            SELECT * 
            FROM DefectCategory 
            WHERE Value = @Value  
            ";

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, value);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);
            string name = "N/A";
            if (dr.Read()) { name = dr["Name"].ToString(); }
            if (dr != null) { dr.Close(); }
            return name;
        }


        static public DataTable GetDefectSubCategoryCollection()
        {
            string sql = @"
            SELECT * 
            FROM DefectSubCategory   
            ";

            //SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, username);
            DataSet ds = new DataSet();
            try
            {
                ds = FM2015.Helpers.DBUtil.FillDataSet(sql, "table1", Config.ConnStrCER());
            }
            catch (Exception ex)
            {
                string s = "Exception during Helpers.GetDefectSubCategoryCollection: " + Environment.NewLine;
                s += "sql: " + sql + Environment.NewLine;
                s += "Exception Message: " + ex.Message;
            }
            finally
            {
            }

            return ds.Tables["table1"];
        }

        static public string GetDefectSubCategoryName(int value)
        {
            string sql = @"
            SELECT * 
            FROM DefectSubCategory 
            WHERE Value = @Value  
            ";

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, value);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);
            string name = "N/A";
            if (dr.Read()) { name = dr["Name"].ToString(); }
            if (dr != null) { dr.Close(); }
            return name;
        }

        static public string GetReasonName(int value)
        {
            string sql = @"
            SELECT * 
            FROM ReturnReasons  
            WHERE Value = @Value  
            ";

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, value);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);
            string name = "N/A";
            if (dr.Read()) { name = dr["Name"].ToString(); }
            if (dr != null) { dr.Close(); }
            return name;
        }

        static public string GetReason2Name(int value)
        {
            string sql = @"
            SELECT * 
            FROM ReturnReasons2  
            WHERE Value = @Value  
            ";

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, value);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);
            string name = "N/A";
            if (dr.Read()) { name = dr["Name"].ToString(); }
            if (dr != null) { dr.Close(); }
            return name;
        }

        public static string GetBatchID(int FMOrderID)
        {
            string batchid = "";
            string sql = @"
                        SELECT * 
                        FROM OrderTransfers 
                        WHERE OrderID = @OrderID 
                        ";

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, FMOrderID);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read()) { batchid = dr["BatchID"].ToString(); }
            if (dr != null) { dr.Close(); }

            return batchid;
        }

        public static string GetShipDate(int orderid)
        {
            string sql = @"
                        SELECT * 
                        FROM UPSShipments 
                        WHERE OrderID = @OrderID 
                        ";

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, orderid);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, mySqlParameters);
            string s = "";
            if (dr.Read()) { s = Convert.ToDateTime(dr["UpLoadDate"]).ToShortDateString(); }
            return s;
        }

        static public void AddSerialNum(int FMOrderID, string serialnum, DateTime date)
        {
            string sql = @"
                    INSERT INTO FaceMasterScans 
                    (OrderID, Serial, ScanDate, SSOrderID)                 
                    VALUES (@OrderID, @Serial, @ScanDate, @SSOrderID) 
                    ";
            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, FMOrderID, serialnum, date, -1);
            FM2015.Helpers.DBUtil.Exec(sql, mySqlParameters);
        }

        static public void AddTrackNum(int FMOrderID, double weight, double upscharge, string tracknum, string uploadby)
        {
            string sql = @"
                    INSERT INTO UPSShipments 
                    (OrderID, Weight, ShipDate, TrackingNumber, UPSCharge, UpLoadDate, UpLoadBy, SSOrderID)                 
                    VALUES (@OrderID, @Weight, @ShipDate, @TrackingNumber, @UPSCharge, @UpLoadDate, @UpLoadBy, @SSOrderID) 
                    ";

            DateTime uploaddate = DateTime.Now;
            string shipdate = uploaddate.ToString();

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, FMOrderID, weight, shipdate, tracknum, upscharge, uploaddate, uploadby, 0);
            FM2015.Helpers.DBUtil.Exec(sql, mySqlParameters);
        }

        static public void AddHDITrackNum(int HDIOrderID, string tracknum)
        {
            string sql = @"
                    INSERT INTO TrackingNumbers 
                    (OrderID, ShippingSystemID, TrackingNumber, ShipDate, UpLoadDate)               
                    VALUES (@OrderID, 0, @TrackingNumber, @ShipDate, @UpLoadDate) 
                    ";

            DateTime uploaddate = DateTime.Now;
            DateTime shipdate = uploaddate;

            SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, HDIOrderID, tracknum, shipdate, uploaddate);
            FM2015.Helpers.DBUtil.Exec(sql, Config.ConnStrHDI(), mySqlParameters);
        }

        static public int GetMaxFMBatch()
        {
            int maxBatch = 1;
            string sql = @"
            SELECT batchid = MAX(batchid) 
            FROM orders o, ordertransfers ot
            WHERE o.orderid = ot.orderid and o.sourceid = -2
        ";
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql);
            if (dr.Read()) { maxBatch = Convert.ToInt32(dr["batchid"]); }
            if (dr != null) { dr.Close(); }

            return maxBatch;
        }

        static public void RedirectToNewPage(Page page, string redirectURL)
        {
            Type clientScriptType = page.GetType();
            string clientScriptName = "redirectToNewPage";

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager clientScript = page.ClientScript;

            // Check to see if the client script is already registered.
            if (!clientScript.IsClientScriptBlockRegistered(clientScriptType, clientScriptName))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<script type='text/javascript'>");
                sb.Append("window.open(' " + redirectURL + ", _blank')"); //URL = where you want to redirect.
                sb.Append("</script>");
                clientScript.RegisterClientScriptBlock(clientScriptType, clientScriptName, sb.ToString());
            }
        }

        static public void CancelOrder(int OrderID, string CancelledBy)
        {
            OrderAction orderaction = new OrderAction();
            orderaction.OrderID = OrderID;
            orderaction.OrderActionType = 5; //Cancelled
            orderaction.OrderActionDate = DateTime.Now;
            orderaction.OrderActionReason = "Order Cancelled";
            orderaction.OrderActionBy = CancelledBy;
            orderaction.SSOrderID = -1;
            orderaction.SaveDB();

            // fill in a bogus tracking # as a hack to fool "unshipped" report
            Utilities.FMHelpers.AddTrackNum(OrderID, 0, 0, "cancelled", CancelledBy);

            //now Cancel @ Shopify if an order exists there
            Order o = new Order(OrderID);
            if (o.SSOrderID > 0)
            {
                ShopifyAgent.ShopifyOrder shopOrder = new ShopifyAgent.ShopifyOrder();
                long shopOrderId = ShopifyAgent.ShopifyOrder.GetShopifyIDFromName(o.SSOrderID.ToString(), o.CompanyID);
                if (shopOrderId > 0) { ShopifyAgent.ShopifyOrder.Cancel(shopOrderId); }
            }

            // leave a paper trail about 
            OrderNote.Insertordernote(OrderID, CancelledBy, "Helpers.CancelOrder: Cancelled Order & Updated Tracking # with 'cancelled'");

        }

        public static List<StripeCharge> stripeChargeList (string email, string callingProc)
        {
            List<StripeCharge> stripeChargeList = new List<StripeCharge>();
            StripeChargeService chargeService = new StripeChargeService();
            StripeChargeListOptions chargeOptions = new StripeChargeListOptions();
            PayWhirl.PayWhirlSubscriber pwSubScriber = new PayWhirl.PayWhirlSubscriber();
                try
                {
                    //pwSubScriber = PayWhirl.PayWhirlSubscriber.FindSubscriberByEmail(email);
                    pwSubScriber = PayWhirl.PayWhirlBiz.FindSubscriberByEmail(email);
            }
            catch (Exception ex)
                {
                    string mailFrom = AdminCart.Config.MailFrom();
                    string mailTo = AdminCart.Config.MailTo;
                    string subject = "Error in ShopifyController: Order Details: No Shopify Transactions & No PayWhirl Subscriber found";
                    string body = "Calling Procedure: " + callingProc + Environment.NewLine;
                    body += "Customer Email: " + email + Environment.NewLine;
                    body += "Ex Message: " + ex.Message + Environment.NewLine;
                    mvc_Mail.SendMail(mailFrom, mailTo, subject, body);
                }

                var customerService = new StripeCustomerService();
                StripeCustomer stripeCustomer = new StripeCustomer();
                if (!string.IsNullOrEmpty(pwSubScriber.Stripe_id))
                {
                    //get the customer from Stripe

                    try
                    {
                        stripeCustomer = customerService.Get(pwSubScriber.Stripe_id);
                    }
                    catch (StripeException ex)
                    {
                        int response = (int)ex.HttpStatusCode;
                        StripeError err = ex.StripeError;
                        string errType = err.ErrorType;
                        string errCode = (string.IsNullOrEmpty(err.Code)) ? "" : err.Code;
                        string errError = (string.IsNullOrEmpty(err.Error)) ? "" : err.Error;
                        string errMessage = (string.IsNullOrEmpty(err.Message)) ? "" : err.Message;
                        string msg = callingProc + ": Get StripeCustomer: Stripe Error: errorType= " + errType + "; errorCode= ";
                        msg += errCode + "; errorError=" + errError + "; errorMessage= " + errMessage;
                        FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                    }
                }

                if (!string.IsNullOrEmpty(stripeCustomer.Id))
                {
                    chargeOptions.CustomerId = stripeCustomer.Id;
                    chargeOptions.Created = new StripeDateFilter();
                    chargeOptions.Created.GreaterThan = DateTime.MinValue;
                    try
                    {
                        stripeChargeList = chargeService.List(chargeOptions).ToList(); // optional StripeChargeListOptions
                    }
                    catch (StripeException ex)
                    {
                        ProcessStripeException(ex, true, callingProc, "Get StripeChargeList: Stripe Error");
                    }
                }

                stripeChargeList = stripeChargeList.OrderByDescending(o => o.Created).ToList();

            return stripeChargeList;
        }

        public static string ProcessStripeException(StripeException ex, bool errLog, string msgPrefix, string subject)
        {
            msgPrefix = (string.IsNullOrEmpty(msgPrefix)) ? "()" : msgPrefix;

            int response = (int)ex.HttpStatusCode;
            StripeError err = ex.StripeError;
            string errType = err.ErrorType;
            string errCode = (string.IsNullOrEmpty(err.Code)) ? "" : err.Code;
            string errError = (string.IsNullOrEmpty(err.Error)) ? "" : err.Error;
            string errMessage = (string.IsNullOrEmpty(err.Message)) ? "" : err.Message;
            string msg = msgPrefix + ": " + subject + ": " + Environment.NewLine; 
            msg += "errType: " + errType  + Environment.NewLine;
            msg += "errCode: " + errCode + Environment.NewLine; 
            msg += "errError: " + errError + Environment.NewLine;
            msg += "errMessage: " + errMessage + Environment.NewLine; 

            if (errLog)
            {
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return msg;
        }


    } 
}