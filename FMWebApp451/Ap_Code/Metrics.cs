﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using AdminCart;
using FM2015.Helpers;

namespace FMmetrics
{
    /// <summary>
    /// Summary description for Metrics
    /// </summary>
    public class Metrics
    {

        public Metrics()
        {
            //
            // TODO: Add constructor logic here
            //

        }


        public enum Interval
        {
            Daily, Weekly, Monthly, Quarterly, Yearly
        }


        public enum SalesType
        {
            Web, PO, HDI, AllWeb, ALL
        }

        public enum TransactionType
        {
            CartSale, NonCartSale, Void, Refund
        }



        /// <summary>
        /// GetRevenue returns a Decimal that holds value of sales (net of tax & shipping) between startDate and endDate
        /// </summary>
        static public Decimal GetRevenue(SalesType salesType, DateTime startDate, DateTime endDate)
        {
            Decimal ret = 0;
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            if ((salesType == SalesType.PO) || (salesType == SalesType.ALL))
            {
                //get PO (non-web) sales
                sql = @"
            SELECT sum(DevRevenue + AccyRevenue) AS Revenue 
            FROM FMPOSales 
            WHERE Date Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);

                if (dr.Read()) { ret += Convert.ToDecimal(dr["Revenue"]); }
                if (dr != null) { dr.Close(); }
            }

            if ((salesType == SalesType.Web) || (salesType == SalesType.ALL))
            {
                //now get web-sales revenue
                sql = @" 
            SELECT sum(o.Total - o.TotalTax - o.TotalShipping) AS Revenue 
            FROM Orders o, Transactions t 
            WHERE o.orderid = t.orderid 
                        and (t.ResultCode = 0) 
                        and (t.Trxtype = 'S') 
                        and OrderDate Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);

                if (dr.Read()) { ret += Convert.ToDecimal(dr["Revenue"]); }
                if (dr != null) { dr.Close(); }
            }

            return ret;
        }

        /// <summary>
        /// GetUnits returns a Integer value that holds the number of units sold between startDate and endDate
        /// </summary>   
        static public int GetUnits(SalesType salesType, DateTime startDate, DateTime endDate)
        {
            int ret = 0;
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            if ((salesType == SalesType.PO) || (salesType == SalesType.ALL))
            {
                //get PO (non-web) units
                sql = @" 
            SELECT sum(DevUnits) AS Units 
            FROM FMPOSales 
            WHERE Date Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);

                if (dr.Read()) { ret += Convert.ToInt32(dr["Units"]); }
                if (dr != null) { dr.Close(); }
            }

            if ((salesType == SalesType.Web) || (salesType == SalesType.ALL))
            {
                //now get web-sales revenue
                sql = @" 
            SELECT sum(od.quantity) AS Units 
            FROM Orders o, OrderDetails od, Transactions t 
            WHERE o.OrderID = od.OrderID 
                and o.orderid = t.orderid 
                and (t.ResultCode = 0) 
                and (t.Trxtype = 'S') 
                and (od.ProductID = 14 OR od.ProductID = 23 OR od.ProductID = 1 OR od.ProductID = 11) 
                and OrderDate Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);

                if (dr.Read()) { ret += Convert.ToInt32(dr["Units"]); }
                if (dr != null) { dr.Close(); }
            }

            return ret;
        }

        /// <summary>
        /// GeReturns returns a Integer value that holds the number of units returned  between startDate and endDate
        /// </summary>   
        static public int GetReturns(SalesType salesType, DateTime startDate, DateTime endDate)
        {
            int ret = 0;
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            if ((salesType == SalesType.PO) || (salesType == SalesType.ALL))
            {
                //get PO (non-web) units returmed - unofficial data; uses the average of 23% return rate
                sql = @" 
            SELECT (sum(DevUnits) * .23) AS Units 
            FROM FMPOSales 
            WHERE Date Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);

                if (dr.Read()) { ret += Convert.ToInt32(dr["Units"]); }
                if (dr != null) { dr.Close(); }
            }

            if ((salesType == SalesType.Web) || (salesType == SalesType.ALL))
            {
                //now get web-sales revenue
                sql = @" 
            SELECT Count(ID) AS Units 
            FROM returnanalysis 
            WHERE DateReceived Between @startDate and @endDate 
        ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);

                if (dr.Read()) { ret += Convert.ToInt32(dr["Units"]); }
                if (dr != null) { dr.Close(); }
            }

            return ret;
        }

        /// <summary>
        /// GeReturnPercent returns a Decimal value that holds 100 * (number of units returned / number of units sold) between startDate and endDate
        ///     Data is for WebSales only
        /// </summary>   
        static public int GetReturnPercent(DateTime startDate, DateTime endDate)
        {
            int sold = GetUnits(SalesType.Web, startDate, endDate);
            int returned = GetReturns(SalesType.Web, startDate, endDate);

            return 100 * (returned / sold);
        }

        /// <summary>
        /// GetComplaints returns a Integer value that holds the number of complaints received (in the CER System) between startDate and endDate
        /// </summary>   
        static public int GetComplaints(DateTime startDate, DateTime endDate)
        {
            int ret = 0;
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            //now get web-sales revenue
            sql = @" 
            select complaints = count(CERIdentifier)
            from CER2s
            where DateReportReceived between @startDate and @endDate
                AND CERType = 'C' 
        ";

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            dr = DBUtil.FillDataReader(sql, mySqlParameters);

            if (dr.Read()) { ret += Convert.ToInt32(dr["complaints"]); }
            if (dr != null) { dr.Close(); }

            return ret;
        }

        /// <summary>
        /// GetComplaintPercent returns a Decimal value that holds 100 * (number of units returned / number of units sold) between startDate and endDate
        ///     Data is for all recorded sales (ie, web, HSN, SNBC, etc.)
        /// </summary>   
        static public int GetComplaintPercent(DateTime startDate, DateTime endDate)
        {
            int sold = GetUnits(SalesType.ALL, startDate, endDate);
            int returned = GetComplaints(startDate, endDate);

            return 100 * (returned / sold);
        }

        /// <summary>
        /// GetUnshippedOrders() returns a DataSet that holds the OrderID, OrderDate, and DayUnshipped over the last 30 days
        /// Based on whether an order has been assigned a tracking number
        /// 
        /// </summary>   
        static public DataSet GetUnshippedOrders()
        {
            DateTime endDate = new DateTime();
            endDate = DateTime.Today;
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 16, 59, 59, 999); //make sure we're at end-of-warehouse-biz-day: 5pm

            //look back 3 business days;
            switch (endDate.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    endDate = endDate.AddDays(-6); //if Monday then account for Sat & Sun and go back to Tue 5pm 
                    break;

                case DayOfWeek.Tuesday:
                    endDate = endDate.AddDays(-6); //if Tues then account for Sat & Sun and go back to Wed 5pm
                    break;

                case DayOfWeek.Wednesday:
                    endDate = endDate.AddDays(-6); //if Wed then account for Sat & Sun and go back to Thu 5pm
                    break;

                case DayOfWeek.Thursday:
                    endDate = endDate.AddDays(-6); //go back to Fri 5pm
                    break;

                case DayOfWeek.Friday:
                    endDate = endDate.AddDays(-4); //go back to Mon 5pm
                    break;

                case DayOfWeek.Saturday:
                    endDate = endDate.AddDays(-4); //go back to Tue 5pm
                    break;

                case DayOfWeek.Sunday:
                    endDate = endDate.AddDays(-5); //if Sun the account for Sat and go back to Wed 5pm
                    break;

                default:
                    endDate = endDate.AddDays(-6); //probably won't get selected, but just in case...
                    break;

            }

            DateTime startDate = endDate.AddMonths(-1); //one month look back
            string sql = @"
            SELECT 
	                row=row_number() over (order by o.orderdate asc), 
	                o.orderid,  
	                o.orderdate,  
	                daysDelayed = DATEDIFF(dd, o.orderDate, GETDATE()), 
	                LastName=dbo.CustomerLastName(o.CustomerID), 
	                City=dbo.OrderAddressCity(o.ShippingAddressID), 
	                State=dbo.OrderAddressState(o.ShippingAddressID), 
	                CountryCode=dbo.OrderAddressCountry(o.ShippingAddressID) 
	 
                FROM orders o, orderactions oa  
                WHERE o.orderid = oa.orderid 
                and oa.orderactiontype = 2  
                and oa.orderactionDate between @startDate and @endDate 
                AND o.orderid not in ( 
	                select orderid 
	                from upsshipments 
				                ) 
                Order By OrderDate ASC 
        ";
                    //where uploaddate > @startDate1 

            DataSet ds = new DataSet();
            bool fromSession = false;
            if (HttpContext.Current.Session["UnshippedReportDate"] != null) 
            {
                DateTime dt = (DateTime)HttpContext.Current.Session["UnshippedReportDate"];
                if (dt.Date == DateTime.Today.Date)
                {
                    if (HttpContext.Current.Session["UnshippedReport"] != null)
                    { 
                        ds = (DataSet)HttpContext.Current.Session["UnshippedReport"];
                        fromSession = true;
                    }

                }
            }

            if (!fromSession)
            {
                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                ds = DBUtil.FillDataSet(sql, "UnshippedOrders", mySqlParameters);
                HttpContext.Current.Session["UnshippedReportDate"] = DateTime.Today.Date;
                HttpContext.Current.Session["UnshippedReport"] = ds;
            }
            return ds;
        }

        /// <summary>
        /// GetUnshippedSerOrders() returns a DataSet that holds the OrderID, OrderDate, and DayUnshipped over the last 30 days
        /// Based on whether a Device order has been scanned for it's serial number
        /// 
        /// </summary>   
        static public DataSet GetUnshippedSerOrders()
        {
            DateTime endDate = new DateTime();
            endDate = DateTime.Today;
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 16, 59, 59, 999); //make sure we're at end-of-warehouse-biz-day: 5pm

            //look back 3 business days;
            switch (endDate.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    endDate = endDate.AddDays(-6); //if Monday then account for Sat & Sun and go back to Tue 5pm 
                    break;

                case DayOfWeek.Tuesday:
                    endDate = endDate.AddDays(-6); //if Tues then account for Sat & Sun and go back to Wed 5pm
                    break;

                case DayOfWeek.Wednesday:
                    endDate = endDate.AddDays(-6); //if Wed then account for Sat & Sun and go back to Thu 5pm
                    break;

                case DayOfWeek.Thursday:
                    endDate = endDate.AddDays(-6); //go back to Fri 5pm
                    break;

                case DayOfWeek.Friday:
                    endDate = endDate.AddDays(-4); //go back to Mon 5pm
                    break;

                case DayOfWeek.Saturday:
                    endDate = endDate.AddDays(-4); //go back to Tue 5pm
                    break;

                case DayOfWeek.Sunday:
                    endDate = endDate.AddDays(-5); //if Sun the account for Sat and go back to Wed 5pm
                    break;

                default:
                    endDate = endDate.AddDays(-6); //probably won't get selected, but just in case...
                    break;

            }

            DateTime startDate = endDate.AddMonths(-1); //one month look back
            string sql = @"
            SELECT 
	                row=row_number() over (order by o.orderdate asc), 
	                o.orderid,  
	                o.orderdate,  
	                daysDelayed = DATEDIFF(dd, o.orderDate, GETDATE()), 
	                LastName=dbo.CustomerLastName(o.CustomerID), 
	                City=dbo.OrderAddressCity(o.ShippingAddressID), 
	                State=dbo.OrderAddressState(o.ShippingAddressID), 
	                CountryCode=dbo.OrderAddressCountry(o.ShippingAddressID) 
	 
                FROM orders o, orderdetails od,  orderactions oa  
                WHERE o.orderid = oa.orderid 
                and o.orderid = od.orderid 
                and (od.ProductID = 26 OR od.ProductID = 27 OR od.ProductID = 28 OR od.ProductID = 29 OR od.ProductID = 41 OR od.ProductID = 42) 
                and oa.orderactiontype = 2  
                and oa.orderactionDate between @startDate and @endDate 
                AND o.orderid not in ( 
	                select orderid 
	                from FaceMasterScans 
				                ) 
                Order By OrderDate ASC 
        ";
            //where uploaddate > @startDate1 

            DataSet ds = new DataSet();
            bool fromSession = false;
            if (HttpContext.Current.Session["UnshippedSerReportDate"] != null)
            {
                DateTime dt = (DateTime)HttpContext.Current.Session["UnshippedSerReportDate"];
                if (dt.Date == DateTime.Today.Date)
                {
                    if (HttpContext.Current.Session["UnshippedSerReport"] != null)
                    {
                        ds = (DataSet)HttpContext.Current.Session["UnshippedSerReport"];
                        fromSession = true;
                    }

                }
            }

            if (!fromSession)
            {
                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
                ds = DBUtil.FillDataSet(sql, "UnshippedSerOrders", mySqlParameters);
                HttpContext.Current.Session["UnshippedSerReportDate"] = DateTime.Today.Date;
                HttpContext.Current.Session["UnshippedSerReport"] = ds;
            }
            return ds;
        }


        /// <summary>
        /// GetRevenueSet(string obrSessionVar, DateTime startDate, DateTime endDate) returns a DataSet that holds the OrderCount, GrossRevenue, OtherCS income, LineItemAdustments, OrderDiscounts, NetRevenue, Tax, and Shipping
        /// <param name="startDate"
        /// <param name="endDate"
        /// </summary> 
        static public DataSet GetRevenueSet(string obrSessionVar, DateTime startDate, DateTime endDate)
        {
            //we want to start the search at midnight of the startDate
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0);
            //we want to finish the search at midnight of the endDate
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59, 999);


            string sql = @"
            SELECT 
                OrderCount = COUNT(tmp.OrderID),
				CASE WHEN SUM(tmp.Total - tmp.TotalShipping - tmp.TotalTax + tmp.Adjust + tmp.TotalCoupon) IS NULL THEN 0 
					 ELSE SUM(tmp.Total - tmp.TotalShipping - tmp.TotalTax + tmp.Adjust + tmp.TotalCoupon) 
				END as GrossRevenue, 
                LineItemDiscounts = 0.00, 
                CASE WHEN sum(tmp.OrderDiscount) IS NULL THEN 0 ELSE sum(tmp.OrderDiscount) END as OrderDiscounts, 
                CASE WHEN SUM(tmp.Total - tmp.TotalShipping - tmp.TotalTax) IS NULL THEN 0 
					 ELSE SUM(tmp.Total - tmp.TotalShipping - tmp.TotalTax) END as NetRevenue, 
                CASE WHEN SUM(tmp.TotalTax) IS NULL THEN 0 ELSE SUM(tmp.TotalTax) END as Tax, 
				CASE WHEN SUM(tmp.TotalShipping) IS NULL THEN 0 ELSE SUM(tmp.TotalShipping) END as Shipping, 
				CASE WHEN SUM(tmp.Total) IS NULL THEN 0 ELSE SUM(tmp.Total) END as Collected  
            FROM ( 
		            SELECT o.orderID, o.orderDate, o.Total, o.TotalTax, o.TotalShipping, o.Adjust, o.TotalCoupon, o.SourceID,  
					    CASE 
						    WHEN o.SourceID < 0 
							THEN o.Adjust 
							ELSE o.TotalCoupon 
					    END as OrderDiscount 
                    FROM Orders o, Transactions t  
                    WHERE o.orderid = t.orderid 
                        and t.ResultCode = 0 
                        and t.Trxtype = 'S' 
                        and t.TransactionDate between @startDate and @endDate 
                        and o.OrderDate between @startDate1 and @endDate1                        
                    ) as tmp  
        ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate, endDate);
            DataSet dsRevStack = DBUtil.FillDataSet(sql, "RevenueStack", mySqlParameters);
            dsRevStack.Tables["RevenueStack"].Columns.Add("AdjGrossRevenue", typeof(Decimal));
            dsRevStack.Tables["RevenueStack"].Columns.Add("OtherCS", typeof(Decimal));
            dsRevStack.Tables["RevenueStack"].Columns.Add("Voids", typeof(Decimal));
            dsRevStack.Tables["RevenueStack"].Columns.Add("Refunds", typeof(Decimal));
            dsRevStack.Tables["RevenueStack"].Columns.Add("Amount", typeof(Decimal));
            dsRevStack.Tables["RevenueStack"].Columns.Add("CollectedCC", typeof(Decimal));

            decimal adjGrossRevenue = GetSKURevenue(startDate, endDate);
            decimal lineItemDiscount = (HttpContext.Current.Session["lineItemDiscount"] == null) ? 0M : (decimal)HttpContext.Current.Session["lineItemDiscount"];
            dsRevStack.Tables["RevenueStack"].Rows[0]["LineItemDiscounts"] = lineItemDiscount;
            //dsRevStack.Tables["RevenueStack"].Rows[0]["AdjGrossRevenue"] = adjGrossRevenue + lineItemDiscount;
            dsRevStack.Tables["RevenueStack"].Rows[0]["AdjGrossRevenue"] = adjGrossRevenue;
            decimal grossRevenue = (decimal)dsRevStack.Tables["RevenueStack"].Rows[0]["GrossRevenue"];
            dsRevStack.Tables["RevenueStack"].Rows[0]["GrossRevenue"] = grossRevenue + lineItemDiscount;


            decimal otherCS = GetTransactionList(TransactionType.NonCartSale, obrSessionVar, startDate, endDate);
            dsRevStack.Tables["RevenueStack"].Rows[0]["OtherCS"] = otherCS;

            decimal voids = GetTransactionsSum(TransactionType.Void, startDate, endDate);
            dsRevStack.Tables["RevenueStack"].Rows[0]["Voids"] = voids;

            decimal refunds = GetTransactionsSum(TransactionType.Refund, startDate, endDate);
            dsRevStack.Tables["RevenueStack"].Rows[0]["Refunds"] = refunds;

            decimal amount = GetTransactionsSum(TransactionType.CartSale, startDate, endDate);
            dsRevStack.Tables["RevenueStack"].Rows[0]["Amount"] = amount;

            dsRevStack.Tables["RevenueStack"].Rows[0]["CollectedCC"] = amount + otherCS - refunds - voids;

            //Don't return any NULL values
            if (dsRevStack.Tables["RevenueStack"].Rows.Count != 0)
            {
                for (int i = 0; i < dsRevStack.Tables["RevenueStack"].Columns.Count; i++)
                {
                    if (dsRevStack.Tables["RevenueStack"].Rows[0][i] == DBNull.Value)
                    { dsRevStack.Tables["RevenueStack"].Rows[0][i] = 0; }
                }
            }
            //return NetRevenue & Collected corrected with noh-cart sales
            decimal netrev = Convert.ToDecimal(dsRevStack.Tables["RevenueStack"].Rows[0]["NetRevenue"]);
            dsRevStack.Tables["RevenueStack"].Rows[0]["NetRevenue"] = netrev + otherCS;
            decimal collected = Convert.ToDecimal(dsRevStack.Tables["RevenueStack"].Rows[0]["Collected"]);
            dsRevStack.Tables["RevenueStack"].Rows[0]["Collected"] = collected + otherCS - refunds - voids;

            return dsRevStack;
        }

        /// <summary>
        /// GetOrderDetailReport(DateTime startDate, DateTime endDate) returns a DataSet that holds the Orderdetails by SKU, Total as well as breaking out PhoneOrders
        /// <param name="startDate"
        /// <param name="endDate"
        /// </summary> 
        static public DataSet GetOrderDetailReport(DateTime startDate, DateTime endDate)
        {
            //we want to start the search at midnight of the startDate
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0);
            //we want to finish the search at midnight of the endDate
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59, 999);


            string sql = @"
            SELECT
	            tmp.SKU, 
	            tmp.Name, 
	            tmp.UnitCost, 
	            CASE WHEN SUM(tmp.Quantity) IS NULL THEN 0 ELSE SUM(tmp.Quantity) END as Units, 
	            --GrossRevenue = SUM(tmp.Quantity * tmp.Price), 
                CASE WHEN SUM(tmp.Quantity * tmp.Price) IS NULL THEN 0 ELSE SUM(tmp.Quantity * tmp.Price) END as GrossRevenue, 
	            --Discount = SUM(tmp.Quantity * tmp.Discount), 
                CASE WHEN SUM(tmp.Quantity * tmp.Discount) IS NULL THEN 0 ELSE SUM(tmp.Quantity * tmp.Discount) END as Discount, 
	            -- Cost = SUM(tmp.Quantity * tmp.UnitCost)
                CASE WHEN SUM(tmp.Quantity * tmp.UnitCost) IS NULL THEN 0 ELSE SUM(tmp.Quantity * tmp.UnitCost) END as Cost
            FROM ( 
		            SELECT p.SKU, p.Name, od.UnitCost, od.Quantity, od.Price, od.Discount  
                    FROM Orders o, OrderDetails od, Products p, OrderActions oa  
                    WHERE o.orderid = oa.orderid 
                        and oa.OrderActionType = 2 
                        --and (t.ResultCode = 0)
                        --and (t.Trxtype = 'S')
                        and o.OrderDate between @startDate and @endDate  
                        and o.OrderID = od.OrderID 
                        and p.ProductID = od.ProductID 
                    ) as tmp 
            GROUP BY tmp.SKU, tmp.Name, tmp.UnitCost 
            ORDER BY GrossRevenue DESC 
        ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            DataSet dsOrderDetails = DBUtil.FillDataSet(sql, "OrderDetails", mySqlParameters);
            dsOrderDetails.Tables["OrderDetails"].Columns.Add("phUnits", typeof(Int32));
            dsOrderDetails.Tables["OrderDetails"].Columns.Add("phGross", typeof(Decimal));
            dsOrderDetails.Tables["OrderDetails"].Columns.Add("phDiscount", typeof(Decimal));
            for (int i = 0; i < dsOrderDetails.Tables["OrderDetails"].Rows.Count; i++)
            {
                dsOrderDetails.Tables["OrderDetails"].Rows[i]["phUnits"] = 0;
                dsOrderDetails.Tables["OrderDetails"].Rows[i]["phGross"] = 0;
                dsOrderDetails.Tables["OrderDetails"].Rows[i]["phDiscount"] = 0;
            }


            sql = @"
            SELECT 
	            -- tmp.SKU, 
                CASE WHEN tmp.SKU is NULL then '' ELSE tmp.SKU END as SKU, 
	            -- Description = tmp.Name, 
                CASE WHEN tmp.Name IS NULL then '' ELSE tmp.Name END as Description, 
	            -- tmp.UnitCost, 
                CASE WHEN tmp.UnitCost IS NULL THEN 0 ELSE tmp.UnitCost END as UnitCost, 
	            -- Units = SUM(tmp.Quantity), 
	            CASE WHEN SUM(tmp.Quantity) IS NULL THEN 0 ELSE SUM(tmp.Quantity) END as Units, 
	            -- GrossRevenue = SUM(tmp.Quantity * tmp.Price), 
                CASE WHEN SUM(tmp.Quantity * tmp.Price) IS NULL THEN 0 ELSE SUM(tmp.Quantity * tmp.Price) END as GrossRevenue, 
	            -- Discount = SUM(tmp.Quantity * tmp.Discount), 
                CASE WHEN SUM(tmp.Quantity * tmp.Discount) IS NULL THEN 0 ELSE SUM(tmp.Quantity * tmp.Discount) END as Discount, 
	            -- Cost = SUM(tmp.Quantity * tmp.UnitCost) 
                CASE WHEN SUM(tmp.Quantity * tmp.UnitCost) IS NULL THEN 0 ELSE SUM(tmp.Quantity * tmp.UnitCost) END as Cost
            FROM ( 
		            SELECT p.SKU, p.Name, od.UnitCost, od.Quantity, od.Price, od.Discount  
                    FROM Orders o, OrderDetails od, Products p, Transactions t  
                    WHERE o.orderid = t.orderid 
                        and (t.ResultCode = 0)
                        and (t.Trxtype = 'S')
                        and t.TransactionDate between @startDate and @endDate 
                        and o.OrderID = od.OrderID 
                        and p.ProductID = od.ProductID 
                        and o.Phone = 1 
                    ) as tmp 
            GROUP BY tmp.SKU, tmp.Name, tmp.UnitCost 
            ORDER BY GrossRevenue DESC 
        ";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            DataSet ds = DBUtil.FillDataSet(sql, "phOrderDetails", mySqlParameters);

            if (ds.Tables["phOrderDetails"].Rows.Count != 0)
            {
                for (int i = 0; i < ds.Tables["phOrderDetails"].Rows.Count; i++)
                {
                    string phSKU = ds.Tables["phOrderDetails"].Rows[i]["SKU"].ToString();
                    for (int j = 0; j < dsOrderDetails.Tables["OrderDetails"].Rows.Count; j++)
                    {
                        string SKU = dsOrderDetails.Tables["OrderDetails"].Rows[j]["SKU"].ToString();
                        if (phSKU == SKU)
                        {
                            int units = Convert.ToInt32(ds.Tables["phOrderDetails"].Rows[i]["Units"]);
                            dsOrderDetails.Tables["OrderDetails"].Rows[j]["phUnits"] = units;

                            Decimal gross = Convert.ToDecimal(ds.Tables["phOrderDetails"].Rows[i]["GrossRevenue"]);
                            dsOrderDetails.Tables["OrderDetails"].Rows[j]["phGross"] = gross;

                            Decimal discount = Convert.ToDecimal(ds.Tables["phOrderDetails"].Rows[i]["Discount"]);
                            dsOrderDetails.Tables["OrderDetails"].Rows[j]["phDiscount"] = discount;
                            break;
                        }
                    }
                }

            }

            //Don't return any NULL values
            if (dsOrderDetails.Tables["OrderDetails"].Rows.Count != 0)
            {
                for (int i = 2; i < dsOrderDetails.Tables["OrderDetails"].Columns.Count; i++)
                {
                    if (dsOrderDetails.Tables["OrderDetails"].Rows[0][i] == DBNull.Value)
                    { dsOrderDetails.Tables["OrderDetails"].Rows[0][i] = 0; }
                }
            }

            return dsOrderDetails;
        }

        /// <summary>
        /// GetTransactionsSum(TransactionType TType, DateTime startDate, DateTime endDate) returns a Decimal value equal to the total amount sold from the cart from startDate to endDate
        /// <param name="TType"
        /// <param name="startDate"
        /// <param name="endDate"
        /// </summary> 
        static public Decimal GetTransactionsSum(TransactionType TType, DateTime startDate, DateTime endDate)
        {
            //we want to start the search at midnight of the startDate
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0);
            //we want to finish the search at midnight of the endDate
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59, 999);
            string trxType = "";

            switch (TType)
            {
                case TransactionType.CartSale:
                    trxType = "S";
                    break;

                case TransactionType.NonCartSale:
                    trxType = "SA";
                    break;

                case TransactionType.Refund:
                    trxType = "C";
                    break;

                case TransactionType.Void:
                    trxType = "V";
                    break;

                default:
                    trxType = "X";
                    break;
            }

            string sql = @"
            SELECT SUM(TransactionAmount) 
            FROM Transactions 
            WHERE ResultCode = 0 
                  and TrxType = @TrxType 
	              and TransactionDate Between @startDate and @endDate 
        ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, trxType, startDate, endDate);
            string retStr = DBUtil.GetScalar(sql, mySqlParameters);
            if (!string.IsNullOrEmpty(retStr)) { return Convert.ToDecimal(retStr); }
            else { return 0; }
        }

        /// <summary>
        /// GetTransactionList(TransactionType TType, string listName, DateTime startDate, DateTime endDate) returns a Decimal value equal to the total amount sold from the cart from startDate to endDate
        /// <param name="TType"
        /// <param name="listName"
        /// <param name="startDate"
        /// <param name="endDate"
        /// </summary> 
        static public Decimal GetTransactionList(TransactionType TType, string listName, DateTime startDate, DateTime endDate)
        {
            //we want to start the search at midnight of the startDate
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0);
            //we want to finish the search at midnight of the endDate
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59, 999);
            string trxType = "";
            switch (TType)
            {
                case TransactionType.CartSale:
                    trxType = "S";
                    break;

                case TransactionType.NonCartSale:
                    trxType = "SA";
                    break;

                case TransactionType.Refund:
                    trxType = "C";
                    break;

                case TransactionType.Void:
                    trxType = "V";
                    break;

                default:
                    trxType = "X";
                    break;
            }

            string sql = @"
            SELECT OrderID, TransactionID, TransactionAmount 
            FROM Transactions 
            WHERE ResultCode = 0 
                  and TrxType = @TrxType 
	              and TransactionDate Between @startDate and @endDate 
        ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, trxType, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            List<AdminCart.Transaction> tList = new List<AdminCart.Transaction>();
            decimal sum = 0;
            while (dr.Read())
            {
                sum += Convert.ToDecimal(dr["TransactionAmount"]);

                AdminCart.Transaction t = new AdminCart.Transaction();
                t.OrderID = Convert.ToInt32(dr["OrderID"]);
                t.TransactionID = dr["TransactionID"].ToString();
                t.TransactionAmount = Convert.ToDecimal(dr["TransactionAmount"].ToString());

                tList.Add(t);
            }
            if (dr != null) { dr.Close(); }
            HttpContext.Current.Session[listName] = tList;
            return sum;
        }

                /// <summary>
        /// GetGetSKURevenue(DateTime startDate, DateTime endDate) returns a Decimal value equal to the total amount sold from the cart by SKU prices from startDate to endDate
        ///
        /// <param name="startDate"
        /// <param name="endDate"
        /// </summary> 
        static public Decimal GetSKURevenue(DateTime startDate, DateTime endDate)
        {
            //we want to start the search at midnight of the startDate
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0);
            //we want to finish the search at midnight of the endDate
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59, 999);


            string sql = @"
            SELECT 
				-- revenue = sum(subTotal), 
                CASE WHEN sum(subTotal) IS NULL THEN 0 ELSE sum(subTotal) END as revenue, 
				-- adjGrossRevenue = sum(tmp.adjPRICE), 
                CASE WHEN sum(tmp.adjPRICE) IS NULL THEN 0 ELSE sum(tmp.adjPRICE) END as adjGrossRevenue, 
				-- delta = sum(tmp.adjPRICE) - sum(subTotal), 
                CASE WHEN sum(tmp.adjPRICE) - sum(subTotal) IS NULL THEN 0 ELSE sum(tmp.adjPRICE) - sum(subTotal) END as delta, 
                -- lineItemDiscount = sum(Discount) 
                CASE WHEN sum(Discount) IS NULL THEN 0 ELSE sum(Discount) END as lineItemDiscount 
            FROM ( 
		            SELECT 
                        discount = (od.Quantity * od.Discount), 
						subTotal = (od.Quantity * od.Price),  
						adjPRICE = od.Quantity * ( 
										( 
										CASE 
											WHEN (select IsMultiPay from products where productID = od.productID) = 1 
											THEN (select OnePayPrice from products where productID = od.productID) 
											ELSE (od.Price) 
										END 
										) 
                                    ) 	            
					FROM Orders o, OrderDetails od, Transactions t  
		            WHERE o.orderid = t.orderid 
                        and (t.ResultCode = 0) 
                        and (t.Trxtype = 'S') 
                        and o.OrderDate between @startDate and @endDate 
                        and t.TransactionDate between @startDate1 and @endDate1 
			            and o.OrderID = od.OrderID  
                    ) as tmp 
            ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            decimal sum = 0;
            decimal revenue = 0;
            decimal delta = 0;
            decimal lineItemDiscount = 0;
            while (dr.Read())
            {                
                bool ok = decimal.TryParse(dr["AdjGrossRevenue"].ToString(), out sum);
                ok = decimal.TryParse(dr["revenue"].ToString(), out revenue);
                ok = decimal.TryParse(dr["delta"].ToString(), out delta);
                ok = decimal.TryParse(dr["lineItemDiscount"].ToString(), out lineItemDiscount);
            }
            if (dr != null) { dr.Close(); }
            HttpContext.Current.Session["lineItemDiscount"] = lineItemDiscount;
            return sum;
        }

        /// <summary>
        /// GetGrossReconcile(string mmSessionVar, DateTime startDate, DateTime endDate) returns a Decimal value equal to the total amount sold from the cart from startDate to endDate
        ///
        /// <param name="mmSessionVar"
        /// <param name="startDate"
        /// <param name="endDate"
        /// </summary> 
        static public Decimal GetGrossReconcile(string mmSessionVar, DateTime startDate, DateTime endDate)
        {
            //we want to start the search at midnight of the startDate
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0);
            //we want to finish the search at midnight of the endDate
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59, 999);

            string sql = @"
            SELECT
	            tmp.*, 
	            -- delta = (tmp.OTotal - tmp.ODTotal) 
                CASE WHEN (tmp.OTotal - tmp.ODTotal) IS NULL THEN 0 ELSE (tmp.OTotal - tmp.ODTotal) END as delta 
            FROM ( 
		            SELECT 
			            o.OrderID, 
                        o.OrderDate, 
                        CASE 
                            WHEN o.SourceID < 0 
                            THEN o.Total - o.TotalTax - o.TotalShipping + o.TotalCoupon + o.Adjust 
                            ELSE o.Total - o.TotalTax - o.TotalShipping + o.TotalCoupon 
                        END AS OTotal,
			            ODTotal = SUM(od.Quantity * (od.Price - od.Discount))  
		            FROM Orders o, OrderDetails od, OrderActions oa   
		            WHERE o.orderid = od.orderid and o.orderid = oa.orderid 
                        and oa.OrderActionType = 2 
			            and o.OrderDate between @startDate and @endDate  
			            and o.OrderID = od.OrderID 
		            GROUP BY o.OrderID, o.OrderDate, o.Total, o.TotalTax, o.TotalShipping, o.TotalCoupon, o.Adjust, o.SourceID 
                    ) as tmp 
            WHERE tmp.OTotal <> tmp.ODTotal 
            ORDER BY tmp.OrderID DESC 
        ";
            // OTotal =  o.Total - o.TotalTax - o.TotalShipping + o.TotalCoupon + o.Adjust, 

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            List<AdminCart.Order> tList = new List<AdminCart.Order>();
            decimal sum = 0;
            while (dr.Read())
            {
                sum += Convert.ToDecimal(dr["delta"]);

                AdminCart.Order o = new AdminCart.Order();
                o.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                o.OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());
                o.Total = Convert.ToDecimal(dr["OTotal"].ToString());
                tList.Add(o);
            }
            if (dr != null) { dr.Close(); }

            HttpContext.Current.Session[mmSessionVar] = tList;
            return sum;
        }

        /// <summary>
        /// GetMetric Set returns a Dataset full of lots of stuff!
        ///     The Dataset includes the following tables:
        ///         "POSales": raw sales data from startDate to endDate from POs; columns = Date, Revenue; grouped and ordered by Date (with gaps)
        ///             - is NULL if SalesType is not ALL or PO
        ///         "POUnits": raw units sold data from startDate to endDate from POs; columns = Date, Revenue; grouped and ordered by Date (with gaps)
        ///             - is NULL if SalesType is not ALL or PO
        ///         "WebSales": raw sales data from startDate to endDate from Web sales; columns = Date, Revenue; grouped and ordered by Date (with gaps)
        ///             - is NULL if SalesType is not ALL or Web
        ///         "WebUnits": raw units sold data from startDate to endDate from Web sales; columns = Date, Revenue; grouped and ordered by Date (with gaps)
        ///             - is NULL if SalesType is not ALL or Web
        ///         "TotalSalesByDay": combined data from "POSales" and "WebSales"; columns = Date, Revenue, 7D MA; grouped and ordered by Date (by day, no gaps)
        ///         "TotalUnitsByDay": combined data from "POUnits" and "WebUnits"; columns = Date, Units, 7D MA; grouped and ordered by Date (by day, no gaps)
        ///         
        ///         "TotalSalesByWeek": combined data from "POSales" and "WebSales"; columns = Date, Revenue; grouped and ordered by Date (by week, no gaps)
        ///         "TotalUnitsByWeek": combined data from "POUnits" and "WebUnits"; columns = Date, Units; grouped and ordered by Date (by week, no gaps)
        ///         
        ///         "TotalSalesByMonth": combined data from "POSales" and "WebSales"; columns = Date, Revenue; grouped and ordered by Date (by month, no gaps)
        ///         "TotalUnitsByMonth": combined data from "POUnits" and "WebUnits"; columns = Date, Units; grouped and ordered by Date (by month, no gaps)
        ///         
        ///         "TotalSalesByQTR": combined data from "POSales" and "WebSales"; columns = Date, Revenue; grouped and ordered by Date (by qtr, no gaps)
        ///         "TotalUnitsByQTR": combined data from "POUnits" and "WebUnits"; columns = Date, Units; grouped and ordered by Date (by qtr, no gaps)
        ///         
        ///         "TotalSalesByYear": combined data from "POSales" and "WebSales"; columns = Date, Revenue; grouped and ordered by Date (by year, no gaps)
        ///         "TotalUnitsByYear": combined data from "POUnits" and "WebUnits"; columns = Date, Units; grouped and ordered by Date (by year, no gaps)
        /// </summary>
        /// <param name="salesType"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        static public DataSet GetMetricSet(Interval interval, SalesType salesType, DateTime startDate, DateTime endDate)
        {
            //DataSet ret = new DataSet();
            //ret.Tables.Add(AddTotalSalesByDay(metrics, startDate, endDate));
            //ret.Tables.Add(AddTotalSalesByWeek(metrics, startDate, endDate));
            //ret.Tables.Add(AddTotalSalesByInterval(interval, salesType, startDate, endDate));

            return AddTotalSalesByInterval(interval, salesType, startDate, endDate); // ds.Tables["Metrics"]
        }

        static private DataSet AddTotalSalesByInterval(Interval interval, SalesType salesType, DateTime startDate, DateTime endDate)
        {
            DateTime startDate0 = new DateTime();
            DateTime endDate0 = new DateTime();
            DateTime startDate1 = new DateTime();
            DateTime endDate1 = new DateTime();
            DateTime startDate2 = new DateTime();
            DateTime endDate2 = new DateTime();
            DateTime startDate3 = new DateTime();
            DateTime endDate3 = new DateTime();
            switch (salesType)
            {
                case SalesType.ALL:
                    startDate0 = startDate;
                    endDate0 = endDate;
                    startDate1 = startDate;
                    endDate1 = endDate;
                    startDate2 = startDate;
                    endDate2 = endDate;
                    startDate3 = startDate;
                    endDate3 = endDate;
                    break;

                case SalesType.PO:
                    startDate0 = startDate;
                    endDate0 = endDate;
                    startDate1 = startDate.AddYears(100); //ensure 0 records returned for websales
                    endDate1 = endDate.AddYears(100);
                    startDate2 = startDate.AddYears(100);
                    endDate2 = endDate.AddYears(100);
                    startDate3 = startDate.AddYears(100);
                    endDate3 = endDate.AddYears(100);
                    break;

                case SalesType.Web:
                    startDate0 = startDate;
                    endDate0 = endDate;
                    startDate0 = startDate.AddYears(100); //ensure 0 records returned for POsales
                    endDate0 = endDate.AddYears(100);
                    startDate1 = startDate;
                    endDate1 = endDate;
                    startDate2 = startDate;
                    endDate2 = endDate;
                    startDate3 = startDate.AddYears(100); //ensure 0 records returned for POsales
                    endDate3 = endDate.AddYears(100);
                    break;

                case SalesType.HDI:
                    startDate0 = startDate;
                    endDate0 = endDate;
                    startDate0 = startDate.AddYears(100); //ensure 0 records returned for POsales
                    endDate0 = endDate.AddYears(100);
                    startDate1 = startDate.AddYears(100);
                    endDate1 = endDate.AddYears(100);
                    startDate2 = startDate.AddYears(100);
                    endDate2 = endDate.AddYears(100);
                    startDate3 = startDate;
                    endDate3 = endDate;
                    break;

                case SalesType.AllWeb:
                    startDate0 = startDate;
                    endDate0 = endDate;
                    startDate0 = startDate.AddYears(100); //ensure 0 records returned for POsales
                    endDate0 = endDate.AddYears(100);
                    startDate1 = startDate;
                    endDate1 = endDate;
                    startDate2 = startDate;
                    endDate2 = endDate;
                    startDate3 = startDate;
                    endDate3 = endDate;
                    break;

                default:
                    startDate0 = startDate;
                    endDate0 = endDate;
                    startDate1 = startDate;
                    endDate1 = endDate;
                    startDate2 = startDate;
                    endDate2 = endDate;
                    startDate3 = startDate;
                    endDate3 = endDate;
                    break;

            }

            string sql = GetRevenueSQL(interval);

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate0, endDate0, startDate1, endDate1, startDate2, endDate2, startDate3, endDate3);
            DataSet ds = DBUtil.FillDataSet(sql, "Metrics", mySqlParameters, Config.ConnStr());

            ds.Tables["Metrics"].Columns.Add("revMA", typeof(decimal));
            ds.Tables["Metrics"].Columns.Add("unitsMA", typeof(decimal));
            ds.Tables["Metrics"].Columns.Add("itemsMA", typeof(decimal));
            ds.Tables["Metrics"].Columns.Add("Date", typeof(string));
            ds.Tables["Metrics"].Columns.Add("Returns", typeof(Int32));
            ds.Tables["Metrics"].Columns.Add("ReturnsNoDay0", typeof(Int32));
            ds.Tables["Metrics"].Columns.Add("ReturnPercent", typeof(decimal));
            ds.Tables["Metrics"].Columns.Add("ReturnPercentNoDay0", typeof(decimal));
            ds.Tables["Metrics"].Columns.Add("TotalCERs", typeof(Int32));
            ds.Tables["Metrics"].Columns.Add("Complaints", typeof(Int32));
            ds.Tables["Metrics"].Columns.Add("ComplaintPercent", typeof(decimal));
            // create MA values in tables here
            decimal sumRev = 0;
            decimal aveRev = 0;
            decimal sumUnits = 0;
            decimal aveUnits = 0;
            decimal sumItems = 0;
            decimal aveItems = 0;

            int maCounter = 0;

            switch (interval)
            {
                case Interval.Daily:
                    maCounter = 7; //7 days per week --> 7D MA
                    break;

                case Interval.Weekly:
                    maCounter = 4; //4 weeks per month --> 4W MA
                    break;

                case Interval.Monthly: //3 months per quarter --> 3M MA
                    maCounter = 3;
                    break;

                case Interval.Quarterly: //4 quarters per year --> 4Q MA
                    maCounter = 4;
                    break;

                case Interval.Yearly: //3Y MA
                    maCounter = 3;
                    break;

                default:
                    maCounter = 1;
                    break;
            }

            // create 4W MA values in table
            for (int i = 0; i < ds.Tables["Metrics"].Rows.Count; i++)
            {
                sumRev += Convert.ToDecimal(ds.Tables["Metrics"].Rows[i]["Revenue"]);
                sumUnits += Convert.ToDecimal(ds.Tables["Metrics"].Rows[i]["Units"]);
                sumItems += Convert.ToDecimal(ds.Tables["Metrics"].Rows[i]["Items"]);
                if (i < (maCounter - 1))
                {
                    aveRev = sumRev / (i + 1);
                    aveUnits = sumUnits / (i + 1);
                    aveItems = sumItems / (i + 1);
                }
                else
                {
                    aveRev = sumRev / maCounter;
                    sumRev = sumRev - Convert.ToDecimal(ds.Tables["Metrics"].Rows[i - (maCounter - 1)]["Revenue"]); //decrement by first term in series, get ready to incrementy by next term
                    aveUnits = sumUnits / maCounter;
                    sumUnits = sumUnits - Convert.ToDecimal(ds.Tables["Metrics"].Rows[i - (maCounter - 1)]["Units"]); //decrement by first term in series, get ready to incrementy by next term
                    aveItems = sumItems / maCounter;
                    sumItems = sumItems - Convert.ToDecimal(ds.Tables["Metrics"].Rows[i - (maCounter - 1)]["Items"]); //decrement by first term in series, get ready to incrementy by next term
                }
                ds.Tables["Metrics"].Rows[i]["revMA"] = aveRev;
                ds.Tables["Metrics"].Rows[i]["unitsMA"] = aveUnits;
                ds.Tables["Metrics"].Rows[i]["itemsMA"] = aveItems;

                string year = ds.Tables["Metrics"].Rows[i]["year"].ToString();
                DateTime dt = Convert.ToDateTime("1/1/" + year);
                string dtStr = "";
                switch (interval)
                {
                    case Interval.Daily:
                        int monthNum = Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["monthNum"]);
                        int dayNum = Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["dayNum"]);
                        //dt = dt.AddMonths(monthNum - 1);
                        //dt = dt.AddDays(dayNum - 1);
                        dt = new DateTime(Convert.ToInt32(year), monthNum, dayNum);
                        dtStr = dt.ToShortDateString();
                        break;

                    case Interval.Weekly:
                        int weekNum = Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["weekNum"]);
                        int days = (weekNum - 1) * 7;
                        dt = dt.AddDays(days);
                        dtStr = "wk" + weekNum.ToString() + " " + dt.Year.ToString();
                        break;

                    case Interval.Monthly:
                        monthNum = Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["monthNum"]);
                        dt = new DateTime(Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["year"]), monthNum, 1);
                        System.Globalization.DateTimeFormatInfo info = System.Globalization.DateTimeFormatInfo.GetInstance(System.Globalization.CultureInfo.InvariantCulture);
                        dtStr = info.AbbreviatedMonthNames[monthNum - 1] + year.ToString();
                        break;

                    case Interval.Quarterly:
                        int qtrNum = Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["qtrNum"]);
                        dtStr = qtrNum + "Q" + year.ToString();
                        switch (qtrNum)
                        {
                            case 1:
                                dt = new DateTime(Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["year"]), 1, 1);
                                break;

                            case 2:
                                dt = new DateTime(Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["year"]), 4, 1);
                                break;

                            case 3:
                                dt = new DateTime(Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["year"]), 7, 1);
                                break;

                            case 4:
                                dt = new DateTime(Convert.ToInt32(ds.Tables["Metrics"].Rows[i]["year"]), 10, 1);
                                break;

                        }
                        break;

                    case Interval.Yearly:
                        dtStr = year.ToString();
                        break;

                }

                //ds.Tables["Metrics"].Rows[i]["Date"] = dt;
                ds.Tables["Metrics"].Rows[i]["Date"] = dtStr;
            }

            //now go get return data
            sql = GetReturnWebSQL(interval);

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate1, endDate1);
            DataSet dsRetWeb = DBUtil.FillDataSet(sql, "ReturnsWeb", mySqlParameters, Config.ConnStrCER());

            //find out how many returns were on Day 0
            sql = GetTTRSQL();

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            DataSet dsRetTTR = DBUtil.FillDataSet(sql, "ReturnsTTR", mySqlParameters, Config.ConnStrCER());
            //int Day0Returns = Convert.ToInt32(dsRetTTR.Tables[0].Rows[0]["NumReturned"]);

            int Day0Returns = 0;
            Int32.TryParse(dsRetTTR.Tables[0].Rows[0]["NumReturned"].ToString(), out Day0Returns);

            sql = GetReturnPOSQL(interval);

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate0, endDate0);
            DataSet dsRetPO = DBUtil.FillDataSet(sql, "ReturnsPO", mySqlParameters, Config.ConnStr());

            if ((ds.Tables["Metrics"].Rows.Count != dsRetWeb.Tables["ReturnsWeb"].Rows.Count) 
                || (ds.Tables["Metrics"].Rows.Count != dsRetPO.Tables["ReturnsPO"].Rows.Count) 
                || (dsRetWeb.Tables["ReturnsWeb"].Rows.Count != dsRetPO.Tables["ReturnsPO"].Rows.Count))
            {
                for (int i = 0; i < ds.Tables["Metrics"].Rows.Count; i++)
                {
                    ds.Tables["Metrics"].Rows[i]["Returns"] = 0;
                    ds.Tables["Metrics"].Rows[i]["ReturnsNoDay0"] = 0;
                    ds.Tables["Metrics"].Rows[i]["ReturnPercent"] = 0;
                    ds.Tables["Metrics"].Rows[i]["ReturnPercentNoDay"] = 0;
                }
            }
            else
            {
                for (int i = 0; i < ds.Tables["Metrics"].Rows.Count; i++)
                {
                    decimal tempMetricUnits = Convert.ToDecimal(ds.Tables["Metrics"].Rows[i]["UnitsMA"]);
                    decimal tempReturnWebUnits = Convert.ToDecimal(dsRetWeb.Tables["ReturnsWeb"].Rows[i]["Units"]);
                    decimal tempReturnWebUnitsNoDay = tempReturnWebUnits;
                    if (i==0)
                    {
                        tempReturnWebUnitsNoDay = tempReturnWebUnits - Day0Returns;
                    }
                    decimal tempReturnPOUnits = Convert.ToDecimal(dsRetPO.Tables["ReturnsPO"].Rows[i]["Units"]);
                    decimal tempReturnUnits = tempReturnWebUnits + tempReturnPOUnits;
                    decimal returnPercent = 0;
                    decimal returnPercentNoDay0 = 0;

                    if (tempMetricUnits != 0)
                    {
                        returnPercent = (tempReturnUnits / tempMetricUnits);
                        returnPercentNoDay0 = (tempReturnWebUnitsNoDay / tempMetricUnits);
                    }

                    ds.Tables["Metrics"].Rows[i]["Returns"] = tempReturnUnits;
                    ds.Tables["Metrics"].Rows[i]["ReturnsNoDay0"] = tempReturnWebUnitsNoDay;
                    ds.Tables["Metrics"].Rows[i]["ReturnPercent"] = returnPercent;
                    ds.Tables["Metrics"].Rows[i]["ReturnPercentNoDay0"] = returnPercentNoDay0;
                }
            }

            //now go get complaint date
            sql = GetTotalCERsSQL(interval);

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate, endDate);
            DataSet dsCERs = DBUtil.FillDataSet(sql, "TotalCERs", mySqlParameters, Config.ConnStrCER());

            //now go get complaint date
            sql = GetComplaintSQL(interval);

            mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate, endDate);
            DataSet dsComp = DBUtil.FillDataSet(sql, "Complaints", mySqlParameters, Config.ConnStrCER());

            if (ds.Tables["Metrics"].Rows.Count != dsComp.Tables["Complaints"].Rows.Count)
            {
                for (int i = 0; i < ds.Tables["Metrics"].Rows.Count; i++)
                {
                    ds.Tables["Metrics"].Rows[i]["Complaints"] = 0;
                    ds.Tables["Metrics"].Rows[i]["ComplaintPercent"] = 0;
                }
            }
            else
            {
                for (int i = 0; i < ds.Tables["Metrics"].Rows.Count; i++)
                {
                    decimal tempMetricUnits = Convert.ToDecimal(ds.Tables["Metrics"].Rows[i]["UnitsMA"]);
                    decimal tempComplaints = Convert.ToDecimal(dsComp.Tables["Complaints"].Rows[i]["Complaints"]);
                    decimal complaintPercent = 0;

                    // % is valid only if all units are counted
                    if ((salesType == SalesType.ALL) && (tempMetricUnits != 0)) { complaintPercent = (tempComplaints / tempMetricUnits); }

                    ds.Tables["Metrics"].Rows[i]["TotalCERS"] = Convert.ToDecimal(dsCERs.Tables["TotalCERs"].Rows[i]["CERs"]);

                    ds.Tables["Metrics"].Rows[i]["Complaints"] = tempComplaints;

                    ds.Tables["Metrics"].Rows[i]["ComplaintPercent"] = complaintPercent;
                }

            }

            return ds;
        }

        static private string GetRevenueSQL(Interval interval)
        {
            string sql = GetRevenueSQLBase();
            string sqlSelectDateGroup = "";
            string sqlGroupBy = "";
            string sqlOrderBy = "";
            switch (interval)
            {
                case Interval.Daily:
                    //sql = GetRevenueSQLBase();
                    sqlSelectDateGroup = @"DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum',";

                    sqlGroupBy = @"DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date])";

                    sqlOrderBy = @"1,2,3";
                    sql = string.Format(sql, sqlSelectDateGroup, sqlGroupBy, sqlOrderBy);
                    break;

                case Interval.Weekly:
                    //sql = GetRevenueSQLBase();
                    sqlSelectDateGroup = @"DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(week,[Date]) AS 'weekNum',";

                    sqlGroupBy = @"DATEPART(week,[Date]), 
                                 DATEPART(year,[Date])";

                    sqlOrderBy = @"1,2";
                    sql = string.Format(sql, sqlSelectDateGroup, sqlGroupBy, sqlOrderBy);
                    break;

                case Interval.Monthly:
                    //sql = GetRevenueSQLBase();
                    sqlSelectDateGroup = @"DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum',";

                    sqlGroupBy = @"DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date])";

                    sqlOrderBy = @"1,2";
                    sql = string.Format(sql, sqlSelectDateGroup, sqlGroupBy, sqlOrderBy);
                    break;

                case Interval.Quarterly:
                    //sql = GetRevenueSQLBase();
                    sqlSelectDateGroup = @"DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(quarter,[Date]) AS 'qtrNum',";

                    sqlGroupBy = @"DATEPART(quarter,[Date]), 
                                 DATEPART(YEAR,[Date])";

                    sqlOrderBy = @"1,2";
                    sql = string.Format(sql, sqlSelectDateGroup, sqlGroupBy, sqlOrderBy);
                    break;

                case Interval.Yearly:
                    //sql = GetRevenueSQLBase();
                    sqlSelectDateGroup = @"DATEPART(YEAR,[Date]) AS 'Year',";
                    sqlGroupBy = @"DATEPART(YEAR,[Date])";
                    sqlOrderBy = @"1,2,3";
                    sql = string.Format(sql, sqlSelectDateGroup, sqlGroupBy, sqlOrderBy);
                    break;

                default:
                    //sql = GetRevenueSQLBase();
                    sqlSelectDateGroup = @"DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum',";
                    sqlGroupBy = @"DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date])";
                    sqlOrderBy = @"1,2,3";
                    sql = string.Format(sql, sqlSelectDateGroup, sqlGroupBy, sqlOrderBy);
                    break;
            }
            return sql;
        }


        static private string GetRevSQL_FROM()
        {
            string sql = @"
            --use a calendar to make sure all days & dates are accounted for
    		SELECT 
			    [Date] = DateFull, 
			    Revenue = 0, 
                Items = 0,  
                DevRev = 0, 
                NewDevRev = 0, 
                RefurbDevRev = 0,
                AccyRev = 0, 
                AccyUnits = 0, 
			    units = 0, 
                NewUnits = 0, 
                RefurbUnits = 0 
		    FROM DateLookup 
		    WHERE DateFull Between @startDate and @endDate 
		    UNION ALL --add in PO Sales (like SNBC, LEI, etc.)
            SELECT 
			    [Date], 
			    Revenue = (DevRevenue + AccyRevenue), 
                Items = DevUnits, 
                DevRev = DevRevenue, 
                NewDevRev = DevRevenue, 
                RefurbDevRev = 0, 
                AccyRev = AccyRevenue, 
                AccyUnits = 0, 
			    units = DevUnits, 
                NewUnits = DevUnits, 
                RefurbUnits = 0 
		    FROM FMPOSales 
		    WHERE Date Between @startDate0 and @endDate0  
		    UNION ALL --here's where FM web sales Revenue is calculated: use line item calculation below
            SELECT 
                [Date] = o.OrderDate, 
			    revenue = 0, 
                Items = 0, 
                DevRev = 0, 
                NewDevRev = 0, 
                RefurbDevRev = 0,   
                AccyRev = 0, 
                AccyUnits = 0, 
			    units = 0, 
                NewUnits = 0, 
                RefurbUnits = 0 
		    FROM orders o, Transactions t 
		    WHERE o.orderid = t.orderid 
                and (t.ResultCode = 0) 
                and (t.Trxtype = 'S') 
			    and o.OrderDate BETWEEN @startDate1 and @endDate1 			
		    UNION ALL --here's where revenue from websales, devices & accessories are calculated & reported
			SELECT 
	[Date] = o.OrderDate, 
	-- o.orderid, od.Quantity, od.Price,  od.Discount, netPrice = (od.Price - od.Discount), o.Total, o.TotalTax, o.TotalShipping, o.Adjust, 
	CASE 
		WHEN (o.Orderdate between '7/1/15' and getdate()) AND p.IsFMSystem = 1 AND p.IsMultiPay = 1 
            -- AND ((select top 1 StripeStatus from Subscribers where orderiId = o.orderid AND StripePlanId = p.SubscriptionPlan) <> 'canceled')
		THEN p.OnePayPrice - ((select Installments from SubscriptionPlans where PlanId = p.SubscriptionPlan)*o.adjust)
        ELSE 
			CASE 
				WHEN (o.Orderdate between '7/1/15' and getdate()) 
				THEN 
					CASE 
						WHEN (o.total + o.adjust - o.totaltax - o.totalshipping) <> 0 
						THEN (od.Quantity *(od.Price - od.Discount)) - (((od.Quantity *(od.Price - od.Discount)) / (o.total + o.adjust - o.totaltax - o.totalshipping)) * o.adjust) 
						ELSE 0 
					END 
				ELSE 
					CASE 
						WHEN (o.Orderdate between '1/1/2000' and '7/1/2015')  --use line item discounts only
						THEN od.Quantity * (od.Price - od.Discount) 
						ELSE 0 
				END 
			END 
	END revenue, 
	--Revenue = 0, 
    items = od.Quantity, 
	CASE 
		WHEN (o.Orderdate between '7/1/15' and getdate()) AND p.IsFMSystem = 1 AND p.IsMultiPay = 1 
            -- AND (select top 1 StripeStatus from Subscribers where StripePlanId = p.SubscriptionPlan) <> 'canceled' 
		THEN p.OnePayPrice - ((select Installments from SubscriptionPlans where PlanId = p.SubscriptionPlan)*o.adjust)
   		ELSE 
			CASE 
				WHEN (o.Orderdate between '7/1/15' and getdate()) AND p.IsFMSystem = 1 AND p.IsMultiPay = 0  
				THEN 
					CASE 
						WHEN (o.total + o.adjust - o.totaltax - o.totalshipping) <> 0 
						THEN (od.Quantity *(od.Price - od.Discount)) - (((od.Quantity *(od.Price - od.Discount)) / (o.total + o.adjust - o.totaltax - o.totalshipping)) * o.adjust) 
						ELSE 0 
					END 
				ELSE 
					CASE 
						WHEN (o.Orderdate between '1/1/2000' and '7/1/2015') AND p.IsFMSystem = 1 --use line item discounts only 
						THEN od.Quantity * (od.Price - od.Discount) 
						ELSE 0 
				    END 
			END 
	END DevRev, 
    CASE 
		    WHEN (o.Orderdate between '7/1/15' and getdate()) AND p.IsFMSystem = 1 AND p.IsMultiPay = 1 AND p.IsRefurbished = 0 
                -- AND (select top 1 StripeStatus from Subscribers where StripePlanId = p.SubscriptionPlan) <> 'canceled' 
		    THEN p.OnePayPrice - ((select Installments from SubscriptionPlans where PlanId = p.SubscriptionPlan)*o.adjust)
   		    ELSE 
			    CASE 
				    WHEN (o.Orderdate between '7/1/15' and getdate()) AND p.IsFMSystem = 1 AND p.IsRefurbished = 0 AND p.IsMultiPay = 0 
				    THEN 
					    CASE 
						    WHEN (o.total + o.adjust - o.totaltax - o.totalshipping) <> 0 
						    THEN (od.Quantity *(od.Price - od.Discount)) - (((od.Quantity *(od.Price - od.Discount)) / (o.total + o.adjust - o.totaltax - o.totalshipping)) * o.adjust) 
						    ELSE 0 
					    END 
				    ELSE 
					    CASE 
						    WHEN (o.Orderdate between '1/1/2000' and '7/1/2015') AND p.IsFMSystem = 1 --use line item discounts only 
						    THEN od.Quantity * (od.Price - od.Discount) 
						    ELSE 0 
				        END 
                END 
    END NewDevRev, 
    CASE 
		    WHEN (o.Orderdate between '7/1/15' and getdate()) AND p.IsFMSystem = 1 AND p.IsMultiPay = 1 AND p.IsRefurbished = 1 
                -- AND (select top 1 StripeStatus from Subscribers where StripePlanId = p.SubscriptionPlan) <> 'canceled'
		    THEN p.OnePayPrice - ((select Installments from SubscriptionPlans where PlanId = p.SubscriptionPlan)*o.adjust)
   		    ELSE 
			    CASE 
				    WHEN (o.Orderdate between '7/1/15' and getdate()) AND p.IsFMSystem = 1 AND p.IsRefurbished = 1 AND p.IsMultiPay = 0  
				    THEN 
					    CASE 
						    WHEN (o.total + o.adjust - o.totaltax - o.totalshipping) <> 0 
						    THEN (od.Quantity *(od.Price - od.Discount)) - (((od.Quantity *(od.Price - od.Discount)) / (o.total + o.adjust - o.totaltax - o.totalshipping)) * o.adjust) 
						    ELSE 0 
					    END 
				    ELSE 
					    CASE 
						    WHEN (o.Orderdate between '1/1/2000' and '7/1/2015') AND p.IsFMSystem = 1 --use line item discounts only 
						    THEN od.Quantity * (od.Price - od.Discount) 
						    ELSE 0 
				    END 
			    END 
	    END RefurbDevRev, 
	CASE 
		WHEN (o.Orderdate between '7/1/15' and getdate()) AND p.IsFMSystem <> 1  --use Shopify discounts as well as line item discounts 
		THEN 
			CASE 
				WHEN (o.total + o.adjust - o.totaltax - o.totalshipping) <> 0 
				THEN (od.Quantity*(od.Price - od.Discount)) - (((od.Quantity*(od.Price - od.Discount)) / (o.total + o.adjust - o.totaltax - o.totalshipping)) * o.adjust) 
				ELSE 0 
			END 
		ELSE 
			CASE 
				WHEN (o.Orderdate between '1/1/2000' and '7/1/2015') AND p.IsFMSystem <> 1 --use line item discounts only 
				THEN od.Quantity * (od.Price - od.Discount) 
				ELSE 0 
			END 
	END AccyRev,
    CASE 
		WHEN p.IsFMSystem <> 1 
		THEN od.Quantity 
		ELSE 0 
	END AccyUnits, 
	CASE 
		WHEN p.IsFMSystem = 1 
		THEN od.quantity 
		ELSE 0 
	END Units, 
    CASE 
		WHEN p.IsFMSystem = 1 AND p.IsRefurbished = 0 
		THEN od.quantity 
		ELSE 0 
	END NewUnits,  
    CASE 
		WHEN p.IsFMSystem = 1 AND p.IsRefurbished = 1 
		THEN od.quantity 
		ELSE 0 
	END RefurbUnits   
FROM Orders o, OrderDetails od, OrderActions oa, Products p  
WHERE o.OrderID = od.OrderID   
	and oa.OrderActionID = (select top 1 orderactionid 
							from orderactions 
							where orderid = o.orderid and orderactiontype > 1 
							order by orderactionid desc) 
	and od.ProductID = p.ProductID 
	and OrderDate Between @startDate2 and @endDate2   
            UNION ALL 
			SELECT 
				[Date] = o.OrderDate, 
				Revenue = 0, 
                Items = od.Quantity, 
				CASE 
				WHEN p.IsFMSystem = 1 
                    THEN od.quantity * (od.price - od.discount) 
					ELSE 0
				END
				DevRev, 
                CASE 
				WHEN p.IsFMSystem = 1 AND p.IsRefurbished = 0 
                    THEN od.quantity * (od.price - od.discount) 
					ELSE 0
				END
				NewDevRev,
                CASE 
				WHEN p.IsFMSystem = 1 AND p.IsRefurbished = 1 
                    THEN od.quantity * (od.price - od.discount) 
					ELSE 0
				END
				RefurbDevRev, 
				CASE 
				WHEN p.IsFMSystem = 1 
					THEN 0 
					ELSE od.quantity * (od.price - od.discount)
				END  
				AccyRev,
                CASE 
				WHEN p.IsFMSystem = 1 
					THEN 0 
					ELSE od.quantity
				END  
				AccyUnits,
				CASE 
				WHEN p.IsFMSystem = 1 
					THEN od.quantity 
					ELSE 0 
				END  
				Units, 
                CASE 
                WHEN p.IsFMSystem = 1 AND p.IsRefurbished = 0 
					THEN od.quantity 
					ELSE 0 
				END  
				NewUnits, 
                CASE 
                WHEN p.IsFMSystem = 1 AND p.IsRefurbished = 1 
					THEN od.quantity 
					ELSE 0 
				END  
				RefurbUnits 
			FROM Orders o, OrderDetails od, Products p  
			WHERE o.OrderID = od.OrderID and o.SourceID = -2 and od.ProductID = p.ProductID   
				and OrderDate Between @startDate3 and @endDate3   
        ";
            return sql;
        }

        static private string GetReturnWebSQL(Interval interval)
        {
            string sql = "";
            switch (interval)
            {
                case Interval.Daily:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + ") AS tmp" + @" 
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 
                        ";
                    break;

                case Interval.Weekly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(week,[Date]) AS 'weekNum', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(week,[Date]), 
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Monthly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Quarterly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(quarter,[Date]) AS 'qtrNum', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(quarter,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Yearly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(YEAR,[Date]) 
                        ORDER BY 1; 

                        ";
                    break;

                default:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               units = sum(units) 
                        FROM ( " + GetRetWebSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 

                        ";
                    break;
            }
            return sql;
        }

        static private string GetRetWebSQL_FROM()
        {
            string sql = @"
    		SELECT 
			    [Date] = DateFull, 
			    units = 0 
		    FROM DateLookup 
		    WHERE DateFull Between @startDate and @endDate 
		    UNION ALL 
			SELECT 
				[Date],  
				Units = Count(ID) 
			FROM returnanalysis 
			WHERE DateReceived Between @startDate0 and @endDate0  
            GROUP BY [Date] 
        ";
            return sql;
        }

        static private string GetReturnPOSQL(Interval interval)
        {
            string sql = "";
            switch (interval)
            {
                case Interval.Daily:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + ") AS tmp" + @" 
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 
                        ";
                    break;

                case Interval.Weekly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(week,[Date]) AS 'weekNum', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(week,[Date]), 
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Monthly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Quarterly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(quarter,[Date]) AS 'qtrNum', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(quarter,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Yearly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(YEAR,[Date]) 
                        ORDER BY 1; 

                        ";
                    break;

                default:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               units = sum(units) 
                        FROM ( " + GetRetPOSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 

                        ";
                    break;
            }
            return sql;
        }

        static private string GetRetPOSQL_FROM()
        {
            string sql = @"
    		SELECT 
			    [Date] = DateFull, 
			    units = 0 
		    FROM DateLookup 
		    WHERE DateFull Between @startDate and @endDate 
		    UNION ALL 
			SELECT 
				[Date],  
				Units = sum(DevUnits * .23) 
			FROM FMPOSales 
			WHERE Date Between @startDate0 and @endDate0  
            GROUP BY [Date] 
        ";
            return sql;
        }

        static private string GetTotalCERsSQL(Interval interval)
        {
            string sql = "";
            switch (interval)
            {
                case Interval.Daily:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               CERs = sum(cers) 
                        FROM ( " + GetTotalCERsSQL_FROM() + ") AS tmp" + @" 
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 
                        ";
                    break;

                case Interval.Weekly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(week,[Date]) AS 'weekNum', 
                               CERs = sum(cers) 
                        FROM ( " + GetTotalCERsSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(week,[Date]), 
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Monthly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
                               CERs = sum(cers) 
                        FROM ( " + GetTotalCERsSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Quarterly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(quarter,[Date]) AS 'qtrNum', 
                               CERs = sum(cers) 
                        FROM ( " + GetTotalCERsSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(quarter,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Yearly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
                               CERs = sum(cers) 
                        FROM ( " + GetTotalCERsSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(YEAR,[Date]) 
                        ORDER BY 1; 

                        ";
                    break;

                default:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               CERs = sum(cers) 
                        FROM ( " + GetTotalCERsSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 

                        ";
                    break;
            }
            return sql;
        }


        static private string GetComplaintSQL(Interval interval)
        {
            string sql = "";
            switch (interval)
            {
                case Interval.Daily:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + ") AS tmp" + @" 
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 
                        ";
                    break;

                case Interval.Weekly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(week,[Date]) AS 'weekNum', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(week,[Date]), 
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Monthly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Quarterly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(quarter,[Date]) AS 'qtrNum', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + " ) AS tmp " + @" 
                        GROUP BY DATEPART(quarter,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2; 

                        ";
                    break;

                case Interval.Yearly:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(YEAR,[Date]) 
                        ORDER BY 1; 

                        ";
                    break;

                default:
                    sql = @"
                        SELECT DATEPART(YEAR,[Date]) AS 'Year', 
	                           DATEPART(month,[Date]) AS 'monthNum', 
	                           DATEPART(day,[Date]) AS 'dayNum', 
                               complaints = sum(complaints) 
                        FROM ( " + GetCompSQL_FROM() + " ) AS tmp " + @"  
                        GROUP BY DATEPART(day,[Date]), 
                                 DATEPART(month,[Date]), 
                                 DATEPART(YEAR,[Date]) 
                        ORDER BY 1,2,3; 

                        ";
                    break;
            }
            return sql;
        }

        static private string GetCompSQL_FROM()
        {
            string sql = @"
    		SELECT 
			    [Date] = DateFull, 
			    complaints = 0 
		    FROM DateLookup 
		    WHERE DateFull Between @startDate and @endDate 
		    UNION ALL 
			SELECT 
	            [Date] = DateReportReceived,  
	            complaints = count(CERIdentifier) 
            FROM CER2s 
            WHERE CERType = 'C' and DateReportReceived Between @startDate0 and @endDate0  
            GROUP BY [DateReportReceived] 
        ";
            return sql;
        }

        static private string GetTotalCERsSQL_FROM()
        {
            string sql = @"
    		SELECT 
			    [Date] = DateFull, 
			    cers = 0 
		    FROM DateLookup 
		    WHERE DateFull Between @startDate and @endDate 
		    UNION ALL 
			SELECT 
	            [Date] = DateReportReceived,  
	            cers = count(CERIdentifier) 
            FROM CER2s 
            WHERE DateReportReceived Between @startDate0 and @endDate0  
            GROUP BY [DateReportReceived] 
        ";
            return sql;
        }

        private static string GetRevenueSQLBase()
        {
            string sql = @"
                        SELECT {0}
                               revenue = sum(Revenue), 
                               items = sum(Items), 
                               devRev = sum(DevRev), 
                               newdevRev = sum(NewDevRev), 
                               refurbdevRev = sum(RefurbDevRev),
                               CASE 
                                    WHEN sum(units) <> 0 
                                    THEN sum(DevRev) / sum(units) 
                                    ELSE 0
                               END 
                               devASP, 
                               CASE 
                                    WHEN sum(NewUnits) <> 0 
                                    THEN sum(NewDevRev) / sum(NewUnits) 
                                    ELSE 0
                               END 
                               newDevASP, 
                               CASE 
                                    WHEN sum(RefurbUnits) <> 0 
                                    THEN sum(RefurbDevRev) / sum(RefurbUnits) 
                                    ELSE 0
                               END 
                               refurbDevASP, 
                               accyRev = sum(AccyRev), 
                               accyUnits = sum(AccyUnits), 
                               CASE 
                                    WHEN sum(AccyUnits) <> 0 
                                    THEN sum(AccyRev) / sum(AccyUnits) 
                                    ELSE 0 
                               END 
                               accyASP, 
                               units = sum(Units), 
                               newunits = sum(NewUnits),
                               refurbunits = sum(RefurbUnits) 
                        FROM ( " + GetRevSQL_FROM() + ") AS tmp" + @" 
                        GROUP BY {1} 
                        ORDER BY {2}; 
                        ";
            return sql;
        }

        private static string GetTTRSQL()
        {
            string sql = @"                       
                SELECT 
                    DATEDIFF(DAY, [DateReceived], [Date] ) AS 'DayCount', 
                    Count(id) AS 'NumReturned',
                    [Year]
                FROM ReturnAnalysis
                WHERE  ([DATE] BETWEEN @startDate AND @endDate) 
                       AND Reason2Val <> 10 
                GROUP BY DATEDIFF(DAY, [DateReceived], [Date]), [Year] 
                ORDER BY DayCount ASC
            ";
            return sql;
        }
    }
}