using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

 public static class ValidateUtil
 {
     static private void ExceptionHandler(Exception ex, string notes)
     {
         //send a mail...

         //log to db or text file...

         //shut off server...

         //turn on sprinklers...
         
         //bubble exception up to the client to handle...
         throw ex;
     }

     public static string maxString(string s, int maxLength)
     {
         if (s == null)
             return string.Empty;

         int count = Math.Min(s.Length, maxLength);
         return s.Substring(0, count);
     }

     public static void TestMaxSize(int maxsize, string input)
     {
         if (IsTooLong(maxsize, input))
         {
             Exception ex = new ArgumentException("you can only enter " + maxsize.ToString() + " chars!  try again..");
             ExceptionHandler(ex, "");
         }
     }


     private static bool IsTooLong(int maxsize, string input)
     {
         //always assume the worst, then correct the result if everything is OK
         bool result = true;

         if (input.Length <= maxsize)
         {
             result = false;
         }

         return result;
     }


     public static void TestMinSize(int minsize, string input)
     {
         if (IsTooShort(minsize, input))
         {
             Exception ex = new ArgumentException("you have to enter at least " + minsize.ToString() + " chars!  try again..");
             ExceptionHandler(ex, "");
         }
     }


     private static bool IsTooShort(int minsize, string input)
     {
         //always assume the worst, then correct the result if everything is OK
         bool result = true;

         if (input.Length >= minsize)
         {
             result = false;
         }

         return result;
     }

     public static void TestDate(string input)
     {
         if (!IsDate(input))
         {
             ArgumentException ex = new ArgumentException("Please enter a valid date.");
             ExceptionHandler(ex, "");
         }
     }

     public static bool IsDate(string input)
     {
         
         //DateTime d = DateTime.Now; //can't have null date in .net...
         DateTime d;
         if (DateTime.TryParse(input, out d)) { return true; }
         else { return false; }

         //return (DateTime.TryParse(input, out d));
         
     }

     public static void TestMoney(string input)
     {
         if (!IsMoney(input))
         {
             ArgumentException ex = new ArgumentException("Please enter valid money value, without $.  Example: 59.99");
             ExceptionHandler(ex, "");
         
         }
     }

     private static bool IsMoney(string input)
     {
         Decimal d = 0.00M;

         return Decimal.TryParse(input, out d);
     }

     /// <summary>
     /// Data Validation - check for reasonable ZIP code.  Should be in separate class.
     /// </summary>
     /// <param name="country"></param>
     /// <param name="zip"></param>
     /// <returns></returns>
     static public string CheckZipCode(string country, string zip)
     {
         string result = "";
         String x1, x2, x3, x4, x5, x6, x7, x8, x9, x10;
         if (country == "US" || country == "CA")
         {
             if (country == "US" && zip.Length == 5)
             {
                 x1 = zip.Substring(0, 1); x2 = zip.Substring(1, 1); x3 = zip.Substring(2, 1); x4 = zip.Substring(3, 1); x5 = zip.Substring(4, 1);

                 if (IsNumeric(x1) && IsNumeric(x2) && IsNumeric(x3) && IsNumeric(x4) && IsNumeric(x5))
                     return result;
                 else
                     return "Zip in USA must be numeric (XXXXX or XXXXX-XXXX)";
             }
             else if (country == "US" && zip.Length == 10)
             {
                 x1 = zip.Substring(0, 1); x2 = zip.Substring(1, 1); x3 = zip.Substring(2, 1); x4 = zip.Substring(3, 1); x5 = zip.Substring(4, 1); x6 = zip.Substring(5, 1); x7 = zip.Substring(6, 1); x8 = zip.Substring(7, 1); x9 = zip.Substring(8, 1); x10 = zip.Substring(9, 1);

                 if (IsNumeric(x1) && IsNumeric(x2) && IsNumeric(x3) && IsNumeric(x4) && IsNumeric(x5) && x6 == "-" && IsNumeric(x7) && IsNumeric(x8) && IsNumeric(x9) && IsNumeric(x10))
                     return result;
                 else
                     return "Zipc in USA must be numeric (XXXXX or XXXXX-XXXX)";
             }
             else if (country == "CA" && zip.Length == 7)
             {
                 x1 = zip.Substring(0, 1); x2 = zip.Substring(1, 1); x3 = zip.Substring(2, 1); x4 = zip.Substring(3, 1); x5 = zip.Substring(4, 1); x6 = zip.Substring(5, 1); x7 = zip.Substring(6, 1);

                 if (!IsNumeric(x1) && IsNumeric(x2) && !IsNumeric(x3) && x4 == " " && IsNumeric(x5) && !IsNumeric(x6) && IsNumeric(x7))
                     return result;
                 else
                     return "Zip in Canada must be of type AXA XAX (where A is a letter and X is a number)";
             }
             else if (country == "US")
             {
                 return "Zipcode in USA must be numeric (XXXXX or XXXXX-XXXX)";
             }
             else if (country == "CA")
             {
                 return "Zip in Canada must be of type AXA XAX (where A is a letter and X is a number)";
             }
             else
                 return result;
         }
         else
             return result;
     }

     public static bool IsNumeric(string s)
     {
         try
         {
             Int32.Parse(s);
         }
         catch
         {
             return false;
         }
         return true;
     }

     static public string CleanUpString(string s)
     {
         string result = s;
         //make sure string isn't capable of sql injection or other bad stuff
         if (!String.IsNullOrEmpty(result))
         {
             //result = result.Replace("'", "''");
             result = result.Replace(@"""", "'");
             //result = result.Replace("/", string.Empty);
             result = result.Replace("\\", string.Empty);
             //result = result.Replace("<", "[");
             //result = result.Replace(">", "]");
             //result = result.Replace("&", "&amp;");
             //result = result.Replace("#", "&#35;");
             //result = result.Replace("'", "&#39;");
             //result = result.Replace(":", "&#58;");
             //result = result.Replace("?", "&#63;");
             //result = result.Replace("[", "&#91;");
             //result = result.Replace("]", "&#93;");
             //result = result.Replace("@", "&#64;");
             //result = result.Replace("*", "&#42;");
             //result = result.Replace(".", "&#46;");
             //result = result.Replace(",", "&#44;");
         }

         return result;
     }

     static public bool IsInRole(int id, string role)
     {
         bool IsRole = false;
         string r = FM2015.Models.Roles.GetRole(id);

         String[] rolelist = r.Split(',');
         foreach (string s in rolelist)
         {
             if (role.ToUpper() == s) { IsRole = true; }
         }
         return IsRole;
     }

 }

