﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// ValidationError Describes any data problems for user feedback
/// </summary>
public class ValidationError
{
	public ValidationError()
	{

	}

    private string field;
    private string errorDescription;

    public string Field { get { return field; } set { field = value; } }
    public string ErrorDescription { get { return errorDescription; } set { errorDescription = value; } }
}
