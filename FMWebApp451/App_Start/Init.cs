﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models;
using AdminCart;

namespace FM2015
{
    public class InitConfig
    {
        public static void RegisterInit()
        {
            User user = new User();
            HttpContext.Current.Session["_currentUser"] = user;

            Customer customer = new Customer();
            HttpContext.Current.Session["_currentCustomer"] = customer;

            Cart cart = new Cart();
            HttpContext.Current.Session["_currentCart"] = cart;
        }
    }
}