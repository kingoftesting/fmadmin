using FM2015.Models;
using FMWebApp451.Interfaces;
using FMWebApp451.Respositories;
using FMWebApp451.Services.PayWhirlServices;
using FMWebApp451.Services.StripeServices;
using FMWebApp451.ViewModels;
using System;

using Unity;

namespace FMWebApp451
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IAddress, AddressProvider>();
            container.RegisterType<ICER, CERProvider>();
            container.RegisterType<ICustomer, CustomerProvider>();
            container.RegisterType<ICreditCard, CreditCard>();
            container.RegisterType<IFaq, Faq>();
            container.RegisterType<IMultiPayRevenueByOrder, MultiPayRevenueByOrder>();
            container.RegisterType<IMultiPayTransactionsByDate, MultiPayTransactionsByDate>();
            container.RegisterType<IOrder, OrderProvider>();
            container.RegisterType<IOrderDetail, OrderDetailProvider>();
            container.RegisterType<IOrdersWithDetailProvider, OrdersWithDetailProvider>();
            container.RegisterType<IPayWhirlService, PayWhirlService>();
            container.RegisterType<IProductCostProvider, ProductCostProvider>();
            container.RegisterType<IProductProvider, ProductProvider>();
            container.RegisterType<IRegistration, RegistrationProvider>();
            container.RegisterType<IRiskProvider, RiskProvider>();
            container.RegisterType<ISalesDetails, SalesDetails>();
            container.RegisterType<IShopOrderNote, ShopOrderNoteProvider>();
            container.RegisterType<IStripeService, StripeServices>();
            container.RegisterType<ISubscriptionPlans, SubscriptionPlanProvider>();
            container.RegisterType<ISubscriptions, SubscriptionProvider>();
            container.RegisterType<ITransaction, TransactionProvider>();
            container.RegisterType<IValidationProvider, ValidationProvider>();
        }
    }
}