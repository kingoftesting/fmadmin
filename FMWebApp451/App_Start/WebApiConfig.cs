﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace FMWebApp451
{
    public static class WebApiConfig
    {
        //http://stackoverflow.com/questions/9594229/accessing-session-using-asp-net-web-api
        public static string UrlPrefix { get { return "api"; } }
        public static string UrlPrefixRelative { get { return "~/api"; } }

        public static void Register(HttpConfiguration config)
        {
        // Web API configuration and services

        // Web API routes
        config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: WebApiConfig.UrlPrefix + "/{controller}/{action}/{id}", //"api/{controller}/{action}/{id}",
                defaults: new { controller = "ShopifyAgent", id = RouteParameter.Optional }
            );
        }
    }
}
