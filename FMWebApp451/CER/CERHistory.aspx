<%@ Page Language="C#" Trace="false" Theme="Admin" MasterPageFile="~/admin/MasterPage3_Blank.master"  AutoEventWireup="true" Inherits="CERHistory" Title="FaceMaster - Complaint Resolution" Codebehind="CERHistory.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <table>
    <tr>
        <td>CER History Information</td>
    </tr>
    <tr>
    <td>Insert Date: <asp:Label ID="labInsertDate" runat="server"></asp:Label></td>
    
    <td>Insert By: <asp:Label ID="labInsertBy" runat="server" /></td>
    </tr>
    <tr>
        <td colspan="4"><hr /></td></tr>    
    <tr>
        <td><asp:Label ID="label5" Text="Email:" runat="server" /></td>
        <td><asp:Label ID="lblEmail" runat="Server" Width="170px" />
    </tr>
    <tr>
        <td><asp:Label ID="labelFirstName" Text="First Name:" runat="server" /></td>
        <td><asp:Label ID="lblFirstName" runat="Server" /></td>
    </tr>
    <tr>
        <td><asp:Label ID="label6" Text="Phone:" runat="server" /></td>
        <td><asp:Label ID="lblPhone" runat="Server" Width="170px" /></td>
    </tr>
    <tr>
        <td><asp:Label ID="labelLastName" Text="Last Name:" runat="server" /></td>
        <td><asp:Label ID="lblLastName" runat="Server" /></td>
    </tr>
    <tr>
        <td><asp:Label ID="label1" Text="Street:" runat="server" /></td>
        <td><asp:Label ID="lblStreet" runat="Server" /></td>
    </tr>
    <tr>
        <td><asp:Label ID="label2" Text="City:" runat="server" /></td>
        <td><asp:Label ID="lblCity" runat="Server" /></td>
    </tr>
    <tr>
        <td><asp:Label ID="label3" Text="State:" runat="server" /></td>
        <td><asp:Label ID="lblState" runat="Server" Width="30px" /></td>
    </tr>
    <tr>
        <td><asp:Label ID="label4" Text="Zip:" runat="server" /></td>
        <td><asp:Label ID="lblZip" runat="Server" Width="50px" /></td>
    </tr> 
    <tr>
        <td><asp:Label ID="label7" Text="Product Number:" runat="server" /></td>
        <td><asp:Label ID="lblProductNumber" runat="Server" Width="170px" /></td>
    </tr>
    <tr>
        <td><asp:Label ID="label9" Text="Serial Number:" runat="server" /></td>
        <td><asp:Label ID="lblSerialNumber" runat="Server" /></td>
    </tr>
    <tr>
        <td><asp:Label ID="label11" Text="Event Description:" runat="server" /></td>
        <td><asp:Label ID="lblEventDescription" runat="Server" /></td>
    </tr>
    <tr>
        <td><asp:Label ID="label13" Text="Action Explaination:" runat="server" /></td>
        <td><asp:Label ID="lblActionExplaination" runat="Server" /></td>
    </tr>
    <tr>
        <td><asp:Label ID="label17" Text="Status Cs:" runat="server" /></td>
        <td><asp:Label ID="lblStatusCs" runat="Server" Width="50px" /></td>
    </tr>     
    <tr>
        <td><asp:Label ID="label8" Text="Status QA:" runat="server" /></td>
        <td><asp:Label ID="lblStatusQa" runat="Server" Width="50px" /></td>
    </tr>     
    <tr>
        <td><asp:Label ID="label12" Text="Status Medical:" runat="server" /></td>
        <td><asp:Label ID="lblStatusMed" runat="Server" Width="50px" /></td>
    </tr>     
     <tr>
        <td><asp:Label ID="label10" Text="Medical Condition:" runat="server" /></td>
        <td><asp:Label ID="lblMedicalCondition" runat="Server" Width="50px" /></td>
    </tr>     
    </table>

</asp:Content>


