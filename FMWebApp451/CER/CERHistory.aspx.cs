using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using adminCER;
using Certnet;
using FM2015.Helpers;

public partial class CERHistory : System.Web.UI.Page
{
    Cart cart;

    void Page_Init(Object sender, EventArgs e)
    {
        cart = new Cart();
        cart.Load(Session["cart"]);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int customerID = cart.SiteCustomer.CustomerID;

        if ((AdminCart.Config.RedirectMode == "Test") && !ValidateUtil.IsInRole(customerID, "ADMIN")) //gotta be an Admin to be in Test mode
        { Response.Redirect("~/admin/Login.aspx"); }

        if (customerID == 0)
        { Response.Redirect("~/admin/Login.aspx"); } //can't get in if not signed-in

        string id = Request["id"].ToString();
        string sql = "SELECT * FROM Cer2History WHERE HistoryId = @ID ";
        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, id);
        SqlDataReader dr = DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER(), mySqlParameters);
        while (dr.Read())
        {
            labInsertDate.Text = dr["InsertDate"].ToString();
            labInsertBy.Text = dr["InsertBy"].ToString();
            lblFirstName.Text = dr["FirstName"].ToString();  
            lblLastName.Text = dr["LastName"].ToString();  
            lblStreet.Text = dr["Street"].ToString();  
            lblCity.Text = dr["City"].ToString();  
            lblState.Text = dr["State"].ToString();  
            lblZip.Text = dr["Zip"].ToString();  
            lblEmail.Text = dr["Email"].ToString();  
            lblPhone.Text = dr["Phone"].ToString();
            lblProductNumber.Text = dr["ProductNumber"].ToString();
            lblSerialNumber.Text = dr["SerialNumber"].ToString();
            lblEventDescription.Text = dr["EventDescription"].ToString();
            lblActionExplaination.Text = dr["ActionExplaination"].ToString();
            lblStatusCs.Text = dr["StatusCs"].ToString();
            lblStatusQa.Text = dr["StatusQa"].ToString();
            lblStatusMed.Text = dr["StatusMed"].ToString();
            lblMedicalCondition.Text = dr["MedicalCondition"].ToString();  
        }
        if (dr != null) { dr.Close(); }
    }
}
