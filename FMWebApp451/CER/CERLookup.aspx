<%@ Page Language="C#" Theme="Admin" MasterPageFile="~/admin/MasterPage3_Blank.master" AutoEventWireup="true" Inherits="CERLookup" Title="FaceMaster - CER - Search" Codebehind="CERLookup.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:Literal id="debuglit" Visible="false" runat="server" />

<div class="alpha four columns">

<div style="padding-left:3px; background-color:#DFDFDF">

    <asp:Panel ID="pnlCERSearch" DefaultButton="ButtonGetCER" runat="server">

        CER Search Shortcuts... 
        <div class="simpleClear"></div>
        <asp:LinkButton ID="btnGet30DComplaints" OnClick="btnGet30DComplaints_Click" runat="server">List Aging Open Complaints</asp:LinkButton>
        <div class="simpleClear"></div>
        <asp:LinkButton ID="btnGetComplaints" OnClick="btnGetComplaints_Click" runat="server">List All Open Complaints</asp:LinkButton>
        <div class="simpleClear"></div>
        <asp:LinkButton ID="btnGetMedComplaints" OnClick="btnGetMedComplaints_Click" runat="server">List All Open Med Complaints</asp:LinkButton>
        <div class="simpleClear"></div>

        <hr style="border: 1px solid gray" />

        CER Advanced Search... 

    <div class="simpleClear"></div>
    <div style="float:left; width:25%;">
        <asp:Label ID="Label4" runat="server" Text="CER #:" /> 
        <asp:TextBox ID="txtCERIdentifier" runat="server"/>
    </div>

    <div style="float:left;">
        <asp:Label ID="Label2" runat="server" Text="&nbsp;&nbsp;" /> 
    </div>

    <div style="float:left; width:25%">
        <asp:Label ID="Label1" runat="server" Text="YEAR:" /> 
        <asp:TextBox ID="txtCERYear" runat="server"  />
    </div>
    <div class="simpleClear"></div>

    <div style="float:left; width:25%">
        <asp:Label ID="Label9" runat="server" Text="Last:" /> 
        <asp:TextBox ID="txtLastName" runat="server" />
    </div>
    <div style="float:left;">
        <asp:Label ID="Label3" runat="server" Text="&nbsp;&nbsp;" /> 
    </div>
    <div style="float:left; width:25%">
        <asp:Label ID="Label13" runat="server" Text="First:" /> 
        <asp:TextBox ID="txtFirstName" runat="server"  />
    </div>
    <div class="simpleClear"></div>
      
    Email: <asp:TextBox ID="txtEmail" runat="server" Width="50%"  />

     <div style="float:left; width:50%">
        Order#: <asp:TextBox ID="txtOrderNumber" runat="server" />
    </div>
    <div class="simpleClear"></div>

   <div style="float:left; width:50%">
        Ser#: <asp:TextBox ID="txtSerialNumber" runat="server" />
    </div>
    <div class="simpleClear"></div>

    <asp:Label ID="Label6" runat="server" Text="Complaint?&nbsp;" />
    <div class="simpleClear"></div>
    <div class="groupHTML">
        <asp:RadioButtonList ID="radBtnListCERComplaint" CssClass="groupHTML" RepeatLayout="UnorderedList" runat="server">
            <asp:ListItem Text="All " Value="0" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Yes" Value="1" Selected="False"></asp:ListItem>
            <asp:ListItem Text="No" Value="2" Selected="False"></asp:ListItem>
        </asp:RadioButtonList>
    </div>

    <div class="simpleClear"></div>
    <asp:Label ID="Label10" runat="server" Text="Open?&nbsp;" />
    <div class="simpleClear"></div>
    <div class="groupHTML">
        <asp:RadioButtonList ID="radBtnListCERStatus" CssClass="groupHTML" RepeatLayout="UnorderedList" runat="server">
            <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Yes" Value="1" Selected="False"></asp:ListItem>
            <asp:ListItem Text="No" Value="2" Selected="False"></asp:ListItem>
            <asp:ListItem Text="Closed" Value="3" Selected="False"></asp:ListItem>
        </asp:RadioButtonList>
    </div>

    <div class="simpleClear"></div>
    <asp:Label ID="Label8" runat="server" Text="Medical?&nbsp;" />
    <div class="simpleClear"></div>
    <div class="groupHTML">
        <asp:RadioButtonList ID="radBtnListMedStatus" CssCLass="groupHTML" RepeatLayout="UnorderedList" runat="server">
            <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Yes" Value="2" Selected="False"></asp:ListItem>
            <asp:ListItem Text="No" Value="1" Selected="False"></asp:ListItem>
        </asp:RadioButtonList>
    </div>
    
    <div class="simpleClear"></div>
    <asp:Label ID="Label7" runat="server" Text="MDR Filed?&nbsp;" />
    <div class="simpleClear"></div>
    <div class="groupHTML">
        <asp:RadioButtonList ID="radBtnListMedCondition" CssClass="groupHTML" RepeatLayout="UnorderedList" runat="server">
            <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
            <asp:ListItem Text="No" Value="1" Selected="False"></asp:ListItem>
            <asp:ListItem Text="Yes" Value="2" Selected="False"></asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div class="simpleClear"></div>

    <asp:Label ID="Label5" runat="server" Text="Output:&nbsp;" />
    <div class="simpleClear"></div>
    <div class="groupHTML">
        <asp:RadioButtonList ID="radBtnListOutput" CssClass="groupHTML" RepeatLayout="UnorderedList" runat="server">
            <asp:ListItem Text="Screen" Value="0" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Spreadsheet" Value="1" Selected="False"></asp:ListItem>
        </asp:RadioButtonList>
    </div>

    <div class="simpleClear"></div>
    <asp:Button ID="ButtonGetCER" runat="server" Text="Search-by-Field" OnClick="ButtonGetCER_Click" OnClientClick="aspnetForm.target ='_self'" />

    </asp:Panel>

    <asp:Panel ID="pnlCERSrch" DefaultButton="btnCerKeyword" Visible="true" runat="server">
        <hr style="border: 1px solid gray" />

        CER Event Description Search... 

        <div class="groupHTML">
            <asp:Label ID="Label11" runat="server" Text="Find the first&nbsp;&nbsp;"></asp:Label>
            <asp:TextBox ID="txtResultNum" Text="10" Width="10%" runat="server"></asp:TextBox>
            <asp:Label ID="Label12" runat="server" Text="&nbsp;&nbsp;results"></asp:Label>
            <div class="simpleClear"></div>

            <asp:Label ID="lblCerKeyword" runat="server" Text="Keyword:&nbsp;"></asp:Label>
            <div class="simpleClear"></div>
            <asp:TextBox ID="txtCerKeyword" Width="50%" runat="server"></asp:TextBox>
        </div>
        <div style="float: left; margin-left: 10px">
            <asp:Button ID="btnCerKeyword" OnClick="btnCerKeyword_Click" OnClientClick="aspnetForm.target ='_blank'" runat="server" Text="Submit" />
        </div>
    </asp:Panel>
    <hr style="border: 1px solid gray" />

    Audit & Inspection Helpers...

    <div class="simpleClear"></div>
    <asp:LinkButton ID="btnGet6moComplaints" OnClick="btnGetLast6moComplaints_Click" runat="server">List Last 6mos non-Med Complaints</asp:LinkButton>
    <div class="simpleClear"></div>
    <asp:LinkButton ID="btnGet6moMedComplaints" OnClick="btnGetLast6moMedComplaints_Click" runat="server">List Last 6mos Med Complaints</asp:LinkButton>
    <div class="simpleClear"></div>
    <asp:LinkButton ID="btnGet6moNonComplaints" OnClick="btnGetLast6moNonComplaints_Click" runat="server">List Last 6mos non-Complaints</asp:LinkButton>


</div>
</div>

<div class="omega twelve columns">

    <asp:Literal ID="litRecordcount" runat="server" />
    <br />

    <asp:Panel ID="pnlCERList" runat="server" Visible="true">
 
    <asp:GridView 
    ID="GridView1"
    SkinID="Admin" 
    runat="server"
    AutoGenerateColumns="false" 
    AllowPaging="false"
    OnPageIndexChanging="GridView1_PageIndexChanging" 
    PageSize="50">
    <Columns>
        <asp:TemplateField HeaderText="CER#">
        <ItemTemplate>
            <a href="EditCER.aspx?CERNumber=<%# DataBinder.Eval(Container.DataItem,"CERNumberSearchable") %>&pageaction=lookup"><%# DataBinder.Eval(Container.DataItem, "CERNumberSearchableDash")%></a>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="Date" DataField="DateReportReceived" DataFormatString="{0:MM-dd-yyyy}" ></asp:BoundField>
        <asp:BoundField HeaderText="Type" DataField="CERTypes" ></asp:BoundField>
        <asp:BoundField HeaderText="First" DataField="Firstname" ></asp:BoundField>
        <asp:BoundField HeaderText="Last" DataField="Lastname" ></asp:BoundField>
        <asp:BoundField HeaderText="Email" DataField="Email" ></asp:BoundField>   
        <asp:BoundField HeaderText="QA" DataField="StatusQA" ></asp:BoundField>
        <asp:HyperLinkField HeaderText="Options" Text="Related" DataNavigateUrlFormatString="CERLookup.aspx?pageaction=related&lookup={0}" DataNavigateUrlFields="CERNumberSearchable" /> 
    </Columns>
        <PagerSettings PageButtonCount="10" />
    </asp:GridView>

    </asp:Panel>

    <asp:Panel ID="pnlAuditList" runat="server" Visible="false">

    <asp:GridView 
    ID="GridView2"
    SkinID="Admin" 
    runat="server"
    AutoGenerateColumns="false" 
    AllowPaging="false">
    <Columns>
        <asp:TemplateField HeaderText="CER#">
        <ItemTemplate>
            <a href="EditCER.aspx?CERNumber=<%# DataBinder.Eval(Container.DataItem,"CERNumberSearchable") %>&pageaction=lookup"><%# DataBinder.Eval(Container.DataItem, "CERNumberSearchableDash")%></a>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="Date" ItemStyle-Width="12%" DataField="DateReportReceived" DataFormatString="{0:MM-dd-yyyy}" ></asp:BoundField>
        <asp:BoundField HeaderText="Last" DataField="Lastname" ></asp:BoundField>
        <asp:BoundField HeaderText="Description" DataField="EventDescription" ></asp:BoundField>
    </Columns>
    </asp:GridView>
    </asp:Panel>


</div>
</asp:Content>

