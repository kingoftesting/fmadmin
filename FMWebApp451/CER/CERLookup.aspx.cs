using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using adminCER;
using Certnet;
using FM2015.Helpers;

public partial class CERLookup : System.Web.UI.Page
{
    Cart cart;

    void Page_Init(Object sender, EventArgs e)
    {
        cart = new Cart();
        cart.Load(Session["cart"]);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Define the name and type of the client scripts on the page.
        String csname1 = "fixform"; //so that only the button controls spec'd generate new windows
        Type cstype = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;

        // Check to see if the startup script is already registered.
        if (!cs.IsStartupScriptRegistered(cstype, csname1))
        {
            StringBuilder cstext1 = new StringBuilder();
            //cstext1.Append("<script type=text/javascript> alert('Hello World!') </");
            //cstext1.Append("script>");

            cstext1.Append("<script type='text/javascript'>");
            cstext1.Append("function fixform() { ");
            cstext1.Append("if (opener.document.getElementById('aspnetForm').target != '_blank') return; ");
            cstext1.Append("opener.document.getElementById('aspnetForm').target = ''; ");
            cstext1.Append("opener.document.getElementById('aspnetForm').action = opener.location.href; ");
            cstext1.Append("} </script>");

            cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
        }

        int customerID = cart.SiteCustomer.CustomerID;

        if ((AdminCart.Config.RedirectMode == "Test") && !ValidateUtil.IsInRole(customerID, "ADMIN")) //gotta be an Admin to be in Test mode
        { Response.Redirect("~/admin/Login.aspx"); }

        if (customerID == 0) 
        { Response.Redirect("~/admin/Login.aspx"); } //can't get in if not signed-in

        string whereClause = "";

        if (!IsPostBack)
        {
            if ((Request["pageaction"] != null) )
            {
                if ((Request["pageaction"] == "related") && (Request["lookup"] != null))
                {
                    string CERNumber = Request["lookup"].ToString();
                    adminCER.CER obj = new adminCER.CER(CERNumber);
                    string tempFirst = txtFirstName.Text;
                    string tempLast = txtLastName.Text;

                    if (!string.IsNullOrEmpty(obj.FirstName))
                    {
                        if (obj.FirstName.Length > 1) { txtFirstName.Text = obj.FirstName.Substring(0, 2); }
                        else { txtFirstName.Text = obj.FirstName.Substring(0, 1); }
                    }
                    txtLastName.Text = obj.LastName.Trim();
                    whereClause = BuildWhereClause();
                    Session["whereClause"] = whereClause;

                    txtFirstName.Text = tempFirst;
                    txtLastName.Text = tempLast;

                }
                else if ((Request["pageaction"] == "find") && (Request["email"] != null))
                {
                    string email = Request["email"].ToString();
                    string tempEmail = txtEmail.Text;

                    //txtFirstName.Text = obj.FirstName.Trim();
                    txtEmail.Text = email.Trim();
                    whereClause = BuildWhereClause();
                    Session["whereClause"] = whereClause;
                    
                    txtEmail.Text = tempEmail;
                }
                else if ((Request["pageaction"] == "find") && (Request["orderNum"] != null))
                {
                    string orderNum = Request["orderNum"].ToString();
                    string tempOrderNum = txtOrderNumber.Text;

                    txtOrderNumber.Text = orderNum.Trim();
                    whereClause = BuildWhereClause();
                    Session["whereClause"] = whereClause;

                    txtOrderNumber.Text = tempOrderNum;
                }
            }
            else
            {
                if (ValidateUtil.IsInRole(customerID, "ADMIN"))
                {
                    //selStatusQa.SelectedValue = "UnOpened";
                    radBtnListCERStatus.SelectedValue = "1";                   
                }
                else
                {
                    //selStatusCs.SelectedValue = "Opened";
                    radBtnListCERStatus.SelectedValue = "2";
                }
                whereClause = BuildWhereClause();
                Session["whereClause"] = whereClause;
                radBtnListCERStatus.SelectedValue = "0"; //keep default search "ALL"
            }

            txtCERYear.Text = DateTime.Now.Year.ToString();
        }

        Session["output"] = (radBtnListOutput.SelectedValue == "0") ? "Screen" : "Excel";
        if (!Page.IsPostBack) { BindData(); }
    }

    protected void BindData()
    {
        //todo - set a new search box called limit on the screen with default value of 100.  subst that value
        //below for the top clause
        string sql = @"
        SELECT TOP 1000 CERNumber= ceryear + '-' + cast(ceridentifier as varchar(10))
           ,CERNumberSearchable=ceryear + left('000000', 6-len(ceridentifier) ) + cast(ceridentifier as varchar(10))
            ,CERNumberSearchableDash=ceryear + '-' + left('000000', 6-len(ceridentifier) ) + cast(ceridentifier as varchar(10))
           ,[DateReportReceived]
           ,CASE WHEN [CERType] = 'C' THEN 'Complaint' ELSE 'No Complaint' END CERTypes
           ,[ReceivedBy]
           ,[ComplaintDeterminationBy]
           ,[FirstName]
           ,[LastName]
           ,[Email]
           ,[Phone]
           ,[SerialNumber]          
           ,[StatusQa]          
        from Cer2s WHERE  0 = 0 ";
        sql += Session["whereClause"];
        sql += " Order By ceryear desc, ceridentifier desc";

        debuglit.Text = "<!--" + sql + "-->";
        debuglit.Visible = true;
        //debuglit.Text =  sql + "<br>";
        string output = "";
        if (Session["output"] != null)
        {
            output = Session["output"].ToString();
        }
        //debuglit.Text = "<!--" + output + "-->";

        if (output == "Excel")
        {
            Response.Clear();
            SendCsv();
        }
        else
        {

            string cERIdentifierParam = Session["cERIdentifierParam"].ToString();
            string cERYearParam = Session["cERYearParam"].ToString();
            string firstNameParam = Session["firstNameParam"].ToString();
            string lastNameParam = Session["lastNameParam"].ToString();
            string emailParam = Session["emailParam"].ToString();
            string serialNumberParam = Session["serialNumberParam"].ToString();
            string orderNumberParam = Session["orderNumberParam"].ToString();

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, cERIdentifierParam, cERYearParam, firstNameParam, lastNameParam, emailParam, serialNumberParam, orderNumberParam);
            DataTable result = FM2015.Helpers.DBUtil.FillDataSet(sql, "cer2s", mySqlParameters, AdminCart.Config.ConnStrCER()).Tables[0];
            int recordcount = result.Rows.Count;
            litRecordcount.Text = recordcount.ToString() + " result(s) found.";
            GridView1.DataSource = result;
            GridView1.DataBind();
        }
    }

    protected void SendCsv()
    {
        Response.Clear();
        DateTime rundate = DateTime.Now;

        string year = rundate.Year.ToString();
        string month = rundate.Month.ToString();
        string day = rundate.Day.ToString();
        string hour = rundate.Hour.ToString();
        string minute = rundate.Minute.ToString();

        string datedfilename = "Response_Data_" + year + "_" + month + "_" + day + "_" + hour + "_" + minute + ".csv";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + datedfilename);
        //Response.AddHeader("Content-Length", file.Length.ToString())  
        Response.ContentType = "application/octet-stream";
        string sql = @"select TOP 1000 
[CERYear]
      ,[CERIdentifier]
      ,[CERSource]
      ,[DateReportReceived]
      ,[CERType]
      ,[ReceivedBy]
      ,[ComplaintNumber]
      ,[ComplaintDeterminationBy]
      ,[CustomerID]
      ,[OrderNumber]
      ,[PointOfPurchase]
      ,[FirstName]
      ,[LastName]
      ,[Street]
      ,[City]
      ,[State]
      ,[Zip]
      ,[Country]
      ,[Email]
      ,[Phone]
      ,[ProductNumber]
      ,[SerialNumber]
      ,[EventTiming]
      ,[EventDescription]
      ,[ActionExplaination]
      ,[StatusCs]
      ,[StatusQa]
      ,[StatusMed]
      ,[DateModified]
      ,[InvestigationSummary]
      ,[ManagerApproved]
      ,[MedicalCondition]
from cer2s where 0=0 " + Session["whereclause"];

        string cERIdentifierParam = Session["cERIdentifierParam"].ToString();
        string cERYearParam = Session["cERYearParam"].ToString();
        string firstNameParam = Session["firstNameParam"].ToString();
        string lastNameParam = Session["lastNameParam"].ToString();
        string emailParam = Session["emailParam"].ToString();
        string serialNumberParam = Session["serialNumberParam"].ToString();
        string orderNumberParam = Session["orderNumberParam"].ToString();

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, cERIdentifierParam, cERYearParam, firstNameParam, lastNameParam, emailParam, serialNumberParam, orderNumberParam);
        DataTable dt = FM2015.Helpers.DBUtil.FillDataSet(sql, "cer2s", mySqlParameters, AdminCart.Config.ConnStrCER()).Tables[0];

        StringBuilder csvoutput = new StringBuilder("");
        int ColumnCount = dt.Columns.Count;

        StringBuilder columnlist = new StringBuilder("");

        foreach (DataColumn c in dt.Columns)
        {
            columnlist.Append(c.ColumnName + ",");
        }
        csvoutput.AppendLine(columnlist.ToString());

        foreach (DataRow r in dt.Rows)
        {
            StringBuilder line = new StringBuilder("");

            for (int i = 0; i < ColumnCount; i++)
            {
                //DataColumn c = (DataColumn)r[i];
                
                line.Append("\"" + r[i].ToString().Replace(Environment.NewLine,"") + "\",");
            }
            csvoutput.AppendLine(line.ToString());

        }
        Response.Write(csvoutput);
        //Response.BinaryWrite();
        Response.End();
    }

    protected void Selections_SelectedIndexChanged(object sender, EventArgs e)
    {
        string whereClause = BuildWhereClause(); 
        Session["whereClause"] = whereClause;
        Session["output"] = (radBtnListOutput.SelectedValue == "0") ? "Screen" : "Excel";
        BindData();
    }

    private string BuildWhereClause()
    {
        string whereClause = "";
        string cERIdentifierParam = "%";
        string cERYearParam = "%";
        string firstNameParam = "%";
        string lastNameParam = "%";
        string emailParam = "%";
        string serialNumberParam = "%";
        string orderNumberParam = "%";


        if (txtCERIdentifier.Text != "")
        {
            cERIdentifierParam = txtCERIdentifier.Text;
            if (txtCERYear.Text != "") { cERYearParam = txtCERYear.Text; }
            radBtnListCERStatus.SelectedValue = "0"; //default to "ALL"
            radBtnListMedStatus.SelectedValue = "0"; //default to "ALL"
            radBtnListCERComplaint.SelectedValue = "0"; //default to "ALL"
            radBtnListMedCondition.SelectedValue = "0"; //default to "ALL"

        }
        else
        {
            if (txtCERYear.Text != "") { cERYearParam = txtCERYear.Text; }
            if (txtFirstName.Text != "") { firstNameParam = "%" + txtFirstName.Text + "%"; }
            if (txtLastName.Text != "") { lastNameParam = "%" + txtLastName.Text + "%"; }
            if (txtEmail.Text != "") { emailParam = "%" + txtEmail.Text + "%"; }
            if (txtSerialNumber.Text != "") { serialNumberParam = "%" + txtSerialNumber.Text + "%"; }
            if (txtOrderNumber.Text != "") { orderNumberParam = "%" + txtOrderNumber.Text + "%"; }
        }

        whereClause = @" 
            AND (CERIdentifier LIKE @CERIdentifier)
            AND (CERYear LIKE @CERYear) 
            AND (FirstName LIKE @FirstName) 
            AND (LastName LIKE @LastName) 
            AND (Email LIKE @Email) 
            AND (SerialNumber LIKE @SerialNumber) 
            AND (OrderNumber LIKE @OrderNumber) 
        ";

        //CS OR QA OPEN/CLOSE: if "ANY" is selected then don't update the WHERE clause
        if (radBtnListCERStatus.SelectedValue == "1") //Search Open CERs
        { whereClause += " AND (StatusQa = 'UnOpened') "; }
        else if (radBtnListCERStatus.SelectedValue == "2") //Search Open CERs
        { whereClause += " AND (StatusCs = 'Opened' OR StatusQa = 'Opened') "; }
        else if (radBtnListCERStatus.SelectedValue == "3") //Search Closed CERs
        { whereClause += " AND StatusQa = 'Closed' "; }
        //MedStatus OPEN/CLOSE: if "ANY" is selected then don't update the WHERE clause
        if (radBtnListMedStatus.SelectedValue == "1") //Search Open CERs
        { whereClause += " AND StatusMed = 'Does Not Apply' "; }
        else if (radBtnListMedStatus.SelectedValue == "2") //Search Closed CERs
        { whereClause += " AND StatusMed IN ('Assigned','Closed') "; }

        //Complaint: if "ALL" is selected then don't update the WHERE clause
        if (radBtnListCERComplaint.SelectedValue == "1") whereClause += " AND CERType = 'C' ";
        if (radBtnListCERComplaint.SelectedValue == "2") whereClause += " AND CERType = 'NC' ";

        //Med Condition: if "ALL" is selected then don't update the WHERE clause
        if (radBtnListMedCondition.SelectedValue == "1") whereClause += " AND (MDR = 0)";
        if (radBtnListMedCondition.SelectedValue == "2") whereClause += " AND (MDR = 1)";


        Session["cERIdentifierParam"] = cERIdentifierParam;
        Session["cERYearParam"] = cERYearParam;
        Session["firstNameParam"] = firstNameParam;
        Session["lastNameParam"] = lastNameParam;
        Session["emailParam"] = emailParam;
        Session["serialNumberParam"] = serialNumberParam;
        Session["orderNumberParam"] = orderNumberParam;

        Session["whereClause"] = whereClause;   
        return whereClause;
    }

    protected void btnCerKeyword_Click(object sender, EventArgs e)
    {
        string keyword = ValidateUtil.CleanUpString(txtCerKeyword.Text);
        string resultNum = ValidateUtil.CleanUpString(txtResultNum.Text);
        if (!ValidateUtil.IsNumeric(resultNum)) { resultNum = "10"; }
        Response.Redirect("../admin/GetCerED.aspx?Keyword=" + keyword + "&resultNum=" + resultNum);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void ButtonGetCER_Click(object sender, EventArgs e)
    {

        string whereClause = BuildWhereClause();
        BindData();

    }

    protected void btnGetComplaints_Click(object sender, EventArgs e)
    {
        DataTable result = new DataTable();
        result = adminCER.CER.GetCERTable(true, medStat.All, openStat.OpenOnly, Convert.ToDateTime("1/1/1900"), DateTime.Now);
        SetGridView1(result);
        
    }

    protected void btnGet30DComplaints_Click(object sender, EventArgs e)
    {

        DataTable result = new DataTable();
        result = adminCER.CER.GetCERTable(true, medStat.All, openStat.OpenOnly, Convert.ToDateTime("1/1/1900"), DateTime.Now.AddDays(-30));
        SetGridView1(result);

    }

    protected void btnGetMedComplaints_Click(object sender, EventArgs e)
    {

        DataTable result = new DataTable();
        result = adminCER.CER.GetCERTable(true, medStat.Medical, openStat.OpenOnly, Convert.ToDateTime("1/1/1900"), DateTime.Now);
        SetGridView1(result);

    }

    protected void btnGetLast6moComplaints_Click(object sender, EventArgs e)
    {
        DataTable result = new DataTable();
        DateTime startDate = DateTime.Now.AddMonths(-6);
        result = adminCER.CER.GetCERTable(true, medStat.nonMedical, openStat.All, startDate, DateTime.Now);
        SetGridView2(result);

    }

    protected void btnGetLast6moMedComplaints_Click(object sender, EventArgs e)
    {
        DataTable result = new DataTable();
        DateTime startDate = DateTime.Now.AddMonths(-6);
        result = adminCER.CER.GetCERTable(true, medStat.Medical, openStat.All, startDate, DateTime.Now);
        SetGridView2(result);

    }

    protected void btnGetLast6moNonComplaints_Click(object sender, EventArgs e)
    {
        DataTable result = new DataTable();
        DateTime startDate = DateTime.Now.AddMonths(-6);
        result = adminCER.CER.GetCERTable(false, medStat.nonMedical, openStat.All, startDate, DateTime.Now);
        SetGridView2(result);

    }

    protected void SetGridView1(DataTable result)
    {
        pnlCERList.Visible = true;
        pnlAuditList.Visible = false;
        int recordcount = result.Rows.Count;
        litRecordcount.Text = recordcount.ToString() + " result(s) found."; litRecordcount.Text = recordcount.ToString() + " result(s) found.";
        GridView1.DataSource = result;
        GridView1.DataBind();
    }

    protected void SetGridView2(DataTable result)
    {
        pnlCERList.Visible = false;
        pnlAuditList.Visible = true;
        int recordcount = result.Rows.Count;
        litRecordcount.Text = recordcount.ToString() + " result(s) found."; litRecordcount.Text = recordcount.ToString() + " result(s) found.";
        GridView2.DataSource = result;
        GridView2.DataBind();
    }
}
