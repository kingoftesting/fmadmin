﻿<%@ Page Title="" Language="C#" Theme="Admin" MasterPageFile="~/admin/MasterPage3_Blank.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" Inherits="Charts" Codebehind="Charts.aspx.cs" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="groupHTML">
    <asp:Label runat="server">Start Month: &nbsp;</asp:Label>
    <asp:DropDownList ID="ddlSalesMonth" CssClass="groupHTML" Width="10%" AutoPostBack="true" runat="server">
        <asp:ListItem Value="1">Jan</asp:ListItem>
        <asp:ListItem Value="2">Feb</asp:ListItem>
        <asp:ListItem Value="3">Mar</asp:ListItem>
        <asp:ListItem Value="4">Apr</asp:ListItem>
        <asp:ListItem Value="5">May</asp:ListItem>
        <asp:ListItem Value="6">Jun</asp:ListItem>
        <asp:ListItem Value="7">Jul</asp:ListItem>
        <asp:ListItem Value="8">Aug</asp:ListItem>
        <asp:ListItem Value="9">Sep</asp:ListItem>
        <asp:ListItem Value="10">Oct</asp:ListItem>
        <asp:ListItem Value="11">Nov</asp:ListItem>
        <asp:ListItem Value="12">Dec</asp:ListItem>
    </asp:DropDownList>         
    <asp:Label runat="server">&nbsp;&nbsp;&nbsp;&nbsp;</asp:Label>
    <asp:Label  runat="server">Select Start Year: &nbsp;</asp:Label>
    <asp:DropDownList ID="ddlStartYear" Width="15%" AutoPostBack="true" runat="server"></asp:DropDownList>
    <asp:Label runat="server">&nbsp;&nbsp;&nbsp;&nbsp;</asp:Label>
</div>
<div class="groupHTML">
    <asp:CheckBox ID="chkYOYComp" Text="Select for YOY Comparison" Width="100%" AutoPostBack="true" runat="server" />
</div>

<div class="simpleClear"></div>
<div class="groupHTML">
<asp:Label ID="Label3" runat="server">Source: &nbsp;</asp:Label>
<asp:RadioButtonList id="radlistSalesSource" CssClass="groupHTML" RepeatDirection="Horizontal" AutoPostBack="true" runat="server">
    <asp:ListItem>ALL</asp:ListItem>
    <asp:ListItem>POSales</asp:ListItem>
    <asp:ListItem selected="true">FM-WebSales</asp:ListItem>
    <asp:ListItem>HDI-WebSales</asp:ListItem>
    <asp:ListItem>All-WebSales</asp:ListItem>
</asp:RadioButtonList>
</div>

<div class="simpleClear"></div>

<div class="groupHTML">
<asp:Label ID="Label4" runat="server">Interval: &nbsp;</asp:Label>
<asp:RadioButtonList id="radlistSalesInterval" RepeatDirection="Horizontal" AutoPostBack="true" runat="server">
    <asp:ListItem >Day</asp:ListItem>
    <asp:ListItem>Week</asp:ListItem>
    <asp:ListItem selected="true">Month</asp:ListItem>
    <asp:ListItem>Quarter</asp:ListItem>
    <asp:ListItem>Year</asp:ListItem>
</asp:RadioButtonList>
</div>

<div class="simpleClear"></div>
<!--
<asp:Button ID="btnPDF" runat="server" Text="Export to PDF" OnClick="btnPDF_Click"/>
-->

<hr />

<div id="divMedPareto" style="margin: 0 0 0 110px; height:550px">

    <asp:Panel ID="pnlMedPareto" Visible="true" runat="server">
 
            <asp:Chart ID="chrtMedPareto" Height="480px" Width="760px" runat="server" 
                        Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                        BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
                <Series>
                </Series>
                <Titles>
                    <asp:Title Name="Title1" Text="Medical Complaint Pareto" Font="Arial, 18">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                        BorderDashStyle="Solid" BorderWidth="1">
                        <Position Auto="true" />
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX Title="Complaint" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                            <MajorGrid Interval="5" />
                        </AxisX>
                        <AxisY Title="# of Incidents" TitleFont="Arial, 18" TitleForeColor="Black">
                            <LabelStyle Format="N0" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>

    </asp:Panel>
</div>

<hr />

<div id="divFailurePareto" style="margin: 0 0 0 110px; height:550px">

    <asp:Panel ID="pnlFailurePareto" Visible="true" runat="server">

            <asp:Chart ID="chrtFailurePareto" Height="480px" Width="760px" runat="server" 
                        Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                        BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
                <Series>
                </Series>
                <Titles>
                    <asp:Title Name="Title1" Text="Device Defect Pareto" Font="Arial, 18">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                        BorderDashStyle="Solid" BorderWidth="1">
                        <Position Auto="true" />
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX Title="Defect" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                            <MajorGrid Interval="5" />
                        </AxisX>
                        <AxisY Title="# of Incidents" TitleFont="Arial, 18" TitleForeColor="Black">
                            <LabelStyle Format="N0" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>

    </asp:Panel>

</div>

<hr />

<div id="divPCBPareto" style="margin: 0 0 0 110px; height:550px">

    <asp:Panel ID="pnlPCBPareto" Visible="true" runat="server">

        <asp:Chart ID="chrtPCBPareto" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="PCB Defect Pareto" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Defect" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="# of Incidents" TitleFont="Arial, 18" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

    </asp:Panel>
</div>

<hr />

<div id="divReturnPareto" style="margin: 0 0 0 110px; height:550px">

    <asp:Panel ID="pnlReturnPareto" Visible="true" runat="server">

        <asp:Chart ID="chrtReturnPareto" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Returns Pareto" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="return Reason" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="# of Incidents" TitleFont="Arial, 18" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

    </asp:Panel>
</div>

<hr />

<div id="divTTR" style="margin: 0 0 0 110px; height:550px">

    <asp:Panel ID="pnlTTR" Visible="true" runat="server">
        <div class="groupHTML">            
            <asp:Label runat="server">Select Reason: &nbsp;</asp:Label>
            <asp:DropDownList ID="ddlReason" CssClass="groupHTML"  Width="10%" AutoPostBack="true" runat="server"></asp:DropDownList>
        </div>
        
        <div>
            <asp:Chart ID="chrtTTR" Height="480px" Width="760px" runat="server" 
                        Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                        BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
                <Series>
                </Series>
                <Titles>
                    <asp:Title Name="Title1" Text="Days To Return" Font="Arial, 18">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                        BorderDashStyle="Solid" BorderWidth="1">
                        <Position Auto="true" />
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX Title="# of Days" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                            <MajorGrid Interval="5" />
                        </AxisX>
                        <AxisY Title="# of Returns" TitleFont="Arial, 18" TitleForeColor="Black">
                            <LabelStyle Format="N0" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
    </asp:Panel>
</div>

<hr />

<div id="divSales" style="margin: 0 0 0 110px;">

    <asp:Panel ID="pnlSales" Width="100%" Visible="true" runat="server">

        <asp:Chart ID="chrtTotalItems" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />

        <div class="simpleClear"></div>


        <asp:Chart ID="chrtUnits" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />

        <div class="simpleClear"></div>

        <asp:Chart ID="chrtNewUnits" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />

        <div class="simpleClear"></div>


        <asp:Chart ID="chrtRefurbUnits" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />

        <div class="simpleClear"></div>

        <asp:Chart ID="chrtSales" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtDevRev" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtNewDevRev" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

         <asp:Chart ID="chrtRefurbDevRev" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtDevASP" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Line" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtNewDevASP" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Line" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtRefurbDevASP" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Line" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
                <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                    BorderDashStyle="Solid" BorderWidth="1">
                    <Position Auto="true" />
                </asp:Legend>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtAccyRev" Height="480px" Width="760px" runat="server" 
                Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
        <Series>
            <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                    IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
            </asp:Series>
            <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
            </asp:Title>
        </Titles>
        <Legends>
            <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                BorderDashStyle="Solid" BorderWidth="1">
                <Position Auto="true" />
            </asp:Legend>
        </Legends>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                    <MajorGrid Interval="5" />
                </AxisX>
                <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                    <LabelStyle Format="N0" />
                </AxisY>
            </asp:ChartArea>
        </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtAccyUnits" Height="480px" Width="760px" runat="server" 
                Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
        <Series>
            <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                    IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
            </asp:Series>
            <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
            </asp:Title>
        </Titles>
        <Legends>
            <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                BorderDashStyle="Solid" BorderWidth="1">
                <Position Auto="true" />
            </asp:Legend>
        </Legends>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                    <MajorGrid Interval="5" />
                </AxisX>
                <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                    <LabelStyle Format="N0" />
                </AxisY>
            </asp:ChartArea>
        </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtAccyASP" Height="480px" Width="760px" runat="server" 
                Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
        <Series>
            <asp:Series Name="Series1" ChartType="Line" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                    IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
            </asp:Series>
            <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
            </asp:Title>
        </Titles>
        <Legends>
            <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                BorderDashStyle="Solid" BorderWidth="1">
                <Position Auto="true" />
            </asp:Legend>
        </Legends>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                    <MajorGrid Interval="5" />
                </AxisX>
                <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                    <LabelStyle Format="N0" />
                </AxisY>
            </asp:ChartArea>
        </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtReturns" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtReturnsNoDay0" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtReturnPer" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtReturnPerNoDay0" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtTotalCERs" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtComplaints" Height="480px" Width="760px" runat="server" 
                    Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                    BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
            <Series>
                <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                        IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
                </asp:Title>
            </Titles>
            <Legends>
            </Legends>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                        <MajorGrid Interval="5" />
                    </AxisX>
                    <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                        <LabelStyle Format="N0" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtComplaintPer" Height="480px" Width="760px" runat="server" 
                Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
        <Series>
            <asp:Series Name="Series1" ChartType="Column" MarkerStyle="Square" MarkerSize="5" MarkerColor="Black"
                    IsValueShownAsLabel="true" Font="Arial, 8" LabelAngle="45" LabelForeColor="Blue">
            </asp:Series>
            <asp:Series Name="Series2" ChartType="Line" Color="Black"></asp:Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Daily Sales" Font="Arial, 18">
            </asp:Title>
        </Titles>
        <Legends>
        </Legends>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisX Title="Date" Interval="1" TitleFont="Arial, 14" TitleForeColor="Black">
                    <MajorGrid Interval="5" />
                </AxisX>
                <AxisY Title="Revenue" TitleFont="Arial, 14" TitleForeColor="Black">
                    <LabelStyle Format="N0" />
                </AxisY>
            </asp:ChartArea>
        </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtTTS" Height="480px" Width="760px" runat="server" 
                Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
        <Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Days-To-Tracking# from Purchase (6 month view)" Font="Arial, 18">
            </asp:Title>
        </Titles>
        <Legends>
        </Legends>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisX Title="Date" Interval="7" TitleFont="Arial, 14" TitleForeColor="Black">
                    <MajorGrid Interval="28" />
                </AxisX>
                <AxisY Title="Average # Days to Trk#" TitleFont="Arial, 14" TitleForeColor="Black">
                    <LabelStyle />
               </AxisY>
            </asp:ChartArea>
        </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtTTS2" Height="480px" Width="760px" runat="server" 
                Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
        <Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="Days-To-Serial# from Purchase (6 month view)" Font="Arial, 18">
            </asp:Title>
        </Titles>
        <Legends>
        </Legends>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisX Title="Date" Interval="7" TitleFont="Arial, 14" TitleForeColor="Black">
                    <MajorGrid Interval="28" />
                </AxisX>
                <AxisY Title="Average # Days to Serial#" TitleFont="Arial, 14" TitleForeColor="Black">
                    <LabelStyle />
               </AxisY>
            </asp:ChartArea>
        </ChartAreas>
        </asp:Chart>

                <hr />
        <div class="simpleClear"></div>

        <asp:Chart ID="chrtNCRs" Height="480px" Width="760px" runat="server" 
                Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
        <Series>
        </Series>
        <Titles>
            <asp:Title Name="Title1" Text="NCRs (3mo moving average)" Font="Arial, 18">
            </asp:Title>
        </Titles>
        <Legends>
        </Legends>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisX Title="Date" Interval="120" TitleFont="Arial, 14" TitleForeColor="Black">
                    <MajorGrid Interval="30" />
                </AxisX>
                <AxisY Title="Average #NCRs" TitleFont="Arial, 14" TitleForeColor="Black">
                    <LabelStyle />
               </AxisY>
            </asp:ChartArea>
        </ChartAreas>
        </asp:Chart>

        <hr />
        <div class="simpleClear"></div>

    </asp:Panel>
</div>

</asp:Content>

