﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.DataVisualization.Charting;
using System.Data.SqlClient;
using FMmetrics;
using AdminCart;
using FM2015.Helpers;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

public partial class Charts : System.Web.UI.Page
{
    public class DateValue
    {
        public int Year { get; set; }
        public DateTime Date { get; set; }
        public double Value { get; set; }

        public DateValue()
        {
            Year = 1900;
            Date = Convert.ToDateTime("1/1/1900");
            Value = 0;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            SortedList yearList = new SortedList();
            int year = DateTime.Now.Year;
            while (year > 2005)
            {
                yearList.Add(year.ToString(), year.ToString());
                year--;
            }
            ddlStartYear.DataSource = yearList;
            ddlStartYear.DataTextField = "Key";
            ddlStartYear.DataValueField = "Value";
            ddlStartYear.DataBind();
            ddlStartYear.SelectedValue = DateTime.Now.Year.ToString();
            
            ddlSalesMonth.SelectedValue = DateTime.Now.Month.ToString();

            string sql = @"                       
                SELECT * 
                FROM ReturnReasons
                ORDER BY ID ASC 
            ";

            DataSet ds = DBUtil.FillDataSet(sql, "Reasons", AdminCart.Config.ConnStrCER()); 
            if (ds != null)
            {
                ddlReason.DataSource = ds;
                ddlReason.DataTextField = "Name";
                ddlReason.DataValueField = "Value";
                ddlReason.DataBind();
                ddlReason.SelectedValue = "14"; //default to ALL reasons
            }
        }

        initMedPareto();
        initFailurePareto();
        initPCBPareto();
        initReturnPareto();
        initTTR();
        initSales();
        initTTS();
        initTTS2();
        initNCRs();
    }

    protected void initMedPareto()
    {

        int startYear = Convert.ToInt32(ddlStartYear.SelectedValue);
        int thisYear = DateTime.Now.Year;

        //string startDate = Convert.ToDateTime("1/1/" + (startYear - 1).ToString()).ToString();
        //string endDate = Convert.ToDateTime("12/31/" + (startYear).ToString()).ToString();
        string sql = @"                       
                        SELECT TOP 50 (SELECT Name FROM Symptoms WHERE Value = SymptomVal) AS Symptom, 
			                          [YEAR], 
			                          Count(ID) AS TOTAL 
                        FROM DefectAnalysis 
                        WHERE IsMedical = 1 and SymptomVal <> 0 and ([YEAR] Between @startDate and @endDate) 
                        GROUP BY SymptomVal, [YEAR] 
                        HAVING Count(ID) > 0 
                        ORDER BY Count(ID) DESC, SymptomVal ASC 
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, (startYear-1).ToString(), startYear.ToString());
        SqlDataReader dr = DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER(), mySqlParameters);


        //chrtMedPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        chrtMedPareto.DataBindCrossTable(dr, "Year", "Symptom", "Total", "Label=Total{N0}", PointSortOrder.Descending);
        foreach (Series series in chrtMedPareto.Series)
        { series.ChartType = SeriesChartType.Column; }

    }

    protected void initFailurePareto()
    {

        int startYear = Convert.ToInt32(ddlStartYear.SelectedValue);
        int thisYear = DateTime.Now.Year;

        //string startDate = Convert.ToDateTime("1/1/" + (startYear - 1).ToString()).ToString();
        //string endDate = Convert.ToDateTime("12/31/" + (startYear).ToString()).ToString();
        string sql = @"                       
                        SELECT TOP 50 (SELECT Name FROM DefectCategory WHERE Value = DefectVal) AS Defect, 
			                          [YEAR], 
			                          Count(ID) AS TOTAL 
                        FROM DefectAnalysis 
                        WHERE DefectVal <> 0 and DefectVal <> 8 and ([YEAR] Between @startDate and @endDate) 
                        GROUP BY DefectVal, [YEAR] 
                        HAVING Count(ID) > 0 
                        ORDER BY Count(ID) DESC, DefectVal ASC 
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, (startYear - 1).ToString(), startYear.ToString());
        SqlDataReader dr = DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER(), mySqlParameters);


        //chrtMedPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        chrtFailurePareto.DataBindCrossTable(dr, "Year", "Defect", "Total", "Label=Total{N0}", PointSortOrder.Descending);
        foreach (Series series in chrtFailurePareto.Series)
        { series.ChartType = SeriesChartType.Column; }

    }

    protected void initPCBPareto()
    {

        int startYear = Convert.ToInt32(ddlStartYear.SelectedValue);
        int endYear = startYear;
        //int thisYear = DateTime.Now.Year;

        if (chkYOYComp.Checked) { startYear = startYear - 1; }
        //string startDate = Convert.ToDateTime("1/1/" + (startYear).ToString()).ToString();
        string sql = @"                       
                        SELECT TOP 50 (SELECT Name FROM DefectSubCategory WHERE Value = DescriptionVal) AS Defect, 
                                      [YEAR], 
                                      Count(ID) AS TOTAL 
                        FROM DefectAnalysis 
                        WHERE DefectVal = 3 and ([YEAR] Between @startYear and @endYear) 
                        GROUP BY DescriptionVal, [YEAR] 
                        HAVING Count(ID) > 0 
                        ORDER BY Count(ID) DESC, DescriptionVal ASC 
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startYear.ToString(), endYear.ToString());
        SqlDataReader dr = DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER(), mySqlParameters);


        //chrtPCBPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        chrtPCBPareto.DataBindCrossTable(dr, "Year", "Defect", "Total", "Label=Total{N0}", PointSortOrder.Descending);
        foreach (Series series in chrtPCBPareto.Series)
        { series.ChartType = SeriesChartType.Column; }

    }

    protected void initReturnPareto()
    {

        int startYear = Convert.ToInt32(ddlStartYear.SelectedValue);
        int endYear = startYear;
        //int thisYear = DateTime.Now.Year;

        if (chkYOYComp.Checked) { startYear = startYear - 1; }
        //string startDate = Convert.ToDateTime("1/1/" + (startYear).ToString()).ToString();
        string sql = @"                       
                        SELECT TOP 50 (SELECT Name FROM ReturnReasons WHERE Value = ReasonVal) AS Reason, 
                                      [YEAR], 
                                      Count(ID) AS TOTAL 
                        FROM ReturnAnalysis 
                        WHERE (DateReceived Between @startDate and @endDate) 
                        GROUP BY ReasonVal, [YEAR] 
                        HAVING Count(ID) > 0 
                        ORDER BY Count(ID) DESC, ReasonVal ASC 
                    ";

        DateTime startDate = Convert.ToDateTime("1/1/" + startYear.ToString());
        DateTime endDate = Convert.ToDateTime("12/1/" + endYear.ToString() + " 23:59:59.997");

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate.ToString(), endDate.ToString());
        SqlDataReader dr = DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER(), mySqlParameters);


        //chrtPCBPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        chrtReturnPareto.DataBindCrossTable(dr, "Year", "Reason", "Total", "Label=Total{N0}", PointSortOrder.Descending);
        foreach (Series series in chrtReturnPareto.Series)
        { series.ChartType = SeriesChartType.Column; }

    }

    protected void initTTR()
    {

        int startYear = Convert.ToInt32(ddlStartYear.SelectedValue);
        int endYear = startYear;
        //int thisYear = DateTime.Now.Year;

        if (chkYOYComp.Checked) { startYear = startYear - 1; }
        string reasonStr = ddlReason.SelectedItem.Text;
        
        string sql = @"                       
                SELECT 
                    DATEDIFF(DAY, [DateReceived], [Date] ) AS 'DayCount', 
                    Count(id) AS 'NumReturned',
                    [Year]
                FROM ReturnAnalysis
                WHERE  ([DATE] BETWEEN @startDate AND @endDate) 
                       AND Reason2Val <> 10 
       ";

        Boolean needReason = false;
        if (ddlReason.SelectedItem.Text != "ALL")
        {
            sql += @"
                 AND ReasonVal = @Reason 
           ";
            needReason = true;
        }

        sql += @"
            GROUP BY DATEDIFF(DAY, [DateReceived], [Date]), [Year] 
            ORDER BY DayCount ASC 
        ";

        DateTime startDate = Convert.ToDateTime("1/1/" + startYear.ToString());
        DateTime endDate = Convert.ToDateTime("12/1/" + endYear.ToString() + " 23:59:59.997");

        //DataSet ds = new DataSet();
        SqlDataReader dr = null;

        if (needReason) 
        {
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate.ToString(), endDate.ToString(), ddlReason.SelectedValue.ToString());
            //ds = DBUtil.FillDataSet(sql, "TTR", mySqlParameters, Config.ConnectionString); 
            dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);
        }
        else 
        {
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate.ToString(), endDate.ToString());
            //ds = DBUtil.FillDataSet(sql, "TTR", mySqlParameters, Config.ConnectionString);
            dr = DBUtil.FillDataReader(sql, Config.ConnStrCER(), mySqlParameters);

        }

        //add chart series data here
        chrtTTR.DataBindCrossTable(dr, "Year", "DayCount", "NumReturned", "", PointSortOrder.Descending);
        if (dr != null) { dr.Close(); }

        foreach (Series series in chrtTTR.Series)
        { series.ChartType = SeriesChartType.Column; }

    }

    protected void initTTS()
    {
        //Time-to-Ship based on when a Tracking # was entered
        //line chart

        int startYear = Convert.ToInt32(ddlStartYear.SelectedValue);
        int thisYear = DateTime.Now.Year;

        DateTime end = DateTime.Now;
        DateTime start = end.AddMonths(-6);

        //string startDate = Convert.ToDateTime("1/1/" + (startYear - 1).ToString()).ToString();
        //string endDate = Convert.ToDateTime("12/31/" + (startYear).ToString()).ToString();
        string sql = @"                       
                        SELECT
                            DATEPART(YEAR,o.orderdate) AS 'Year',  
	                        DATEADD(dd, DATEDIFF(dd, 0, o.orderdate), 0) as [OrderDate], 
	                        avg(datediff(d, o.orderdate, u.uploaddate )) AS TTS 
                        FROM Orders o, UPSShipments u, orderactions oa 
                        WHERE o.orderid = u.orderid AND o.orderid = oa.orderid AND oa.orderactiontype = 2 
	                        AND orderdate BETWEEN @start and @end 
                        GROUP BY 
                            DATEPART(YEAR,o.orderdate),
	                        DATEADD(dd, DATEDIFF(dd, 0, o.orderdate), 0) 
                        ORDER BY DATEADD(dd, DATEDIFF(dd, 0, o.orderdate), 0) desc  
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, start.ToString(), end.ToString());
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);


        //chrtMedPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        //chrtTTS.DataBindCrossTable(dr, "Year", "OrderDate", "TTS", "Label=TTS{N0}", PointSortOrder.Descending);
        chrtTTS.DataBindCrossTable(dr, "Year", "OrderDate", "TTS", "", PointSortOrder.Descending);
        foreach (Series series in chrtTTS.Series)
        { 
            series.ChartType = SeriesChartType.Line;
            series.BorderWidth = 3;
        }

    }

    protected void initTTS2()
    {
        //Time-to-Ship based on when a Serial # was scanned
        //line chart

        int startYear = Convert.ToInt32(ddlStartYear.SelectedValue);
        int thisYear = DateTime.Now.Year;

        DateTime end = DateTime.Now;
        DateTime start = end.AddMonths(-6);

        //string startDate = Convert.ToDateTime("1/1/" + (startYear - 1).ToString()).ToString();
        //string endDate = Convert.ToDateTime("12/31/" + (startYear).ToString()).ToString();
        string sql = @"                       
                        SELECT
                            DATEPART(YEAR,o.orderdate) AS 'Year',  
	                        DATEADD(dd, DATEDIFF(dd, 0, o.orderdate), 0) as [OrderDate], 
	                        avg(datediff(d, o.orderdate, s.scandate )) AS TTS 
                        FROM Orders o, FaceMasterScans s, orderactions oa 
                        WHERE o.orderid = s.orderid AND o.orderid = oa.orderid AND oa.orderactiontype = 2 
	                        AND orderdate BETWEEN @start and @end 
                        GROUP BY 
                            DATEPART(YEAR,o.orderdate),
	                        DATEADD(dd, DATEDIFF(dd, 0, o.orderdate), 0) 
                        ORDER BY DATEADD(dd, DATEDIFF(dd, 0, o.orderdate), 0) desc  
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, start.ToString(), end.ToString());
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);


        //add chart series data here
        //chrtTTS.DataBindCrossTable(dr, "Year", "OrderDate", "TTS", "Label=TTS{N0}", PointSortOrder.Descending);
        chrtTTS2.DataBindCrossTable(dr, "Year", "OrderDate", "TTS", "", PointSortOrder.Descending);
        foreach (Series series in chrtTTS2.Series)
        {
            series.ChartType = SeriesChartType.Line;
            series.BorderWidth = 3;
        }

    }

    protected void initNCRs()
    {
        //# of NCRs opened per unit of time
        //line chart
        List<DateValue> dvList = new List<DateValue>();
        int periodLength = 3; //use a 12mo simple moving average


        int startYear = Convert.ToInt32(ddlStartYear.SelectedValue);
        int thisYear = DateTime.Now.Year;

        DateTime end = DateTime.Now;
        DateTime start = end.AddMonths(-36);
        start = new DateTime(start.Year, start.Month, 1); //set to 1st day of month

        //string startDate = Convert.ToDateTime("1/1/" + (startYear - 1).ToString()).ToString();
        //string endDate = Convert.ToDateTime("12/31/" + (startYear).ToString()).ToString();
        string sql = @"                       
                        SELECT Value = NCRNumber, Date = OpenDate 
                        FROM NCRTracking                             
                    ";

        SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStrCER());
        while (dr.Read())
        {
            DateValue dv = new DateValue();
            dv.Date = Convert.ToDateTime(dr["Date"]);
            dv.Year = dv.Date.Year;
            if (dr["Value"] != DBNull.Value)
            {
                dv.Value = 1;
            }
            else
            {
                dv.Value = 0;
            }
            dvList.Add(dv);

        }
        if (dr != null) { dr.Close(); }

        //build list of all months and total all NCRs within those months
        List<DateValue> dvAllList = new List<DateValue>();
        int months = ((end.Year - start.Year) * 12) + end.Month - start.Month;
        for (int i = 0; i < months; i++ )
        {
            DateValue dvObj = new DateValue();
            dvObj.Date = start.AddMonths(i);
            dvObj.Year = dvObj.Date.Year;
            dvObj.Value = (from dv in dvList where (dv.Date.Month == dvObj.Date.Month) && (dv.Date.Year == dvObj.Date.Year) select dv.Value).Sum();
            dvAllList.Add(dvObj);
        }

        //now build a list of moving averages that will be displayed in chart
        List<DateValue> dvChartList = new List<DateValue>();

        for (int i = 0; i < dvAllList.Count(); i++)
        {
            DateValue dvObj = new DateValue();
            double sma = 0;
            dvObj.Date = dvAllList[dvAllList.Count() - i - 1].Date;
            dvObj.Year = dvObj.Date.Year;

            if (i < periodLength)
            {
                if (dvAllList.GetRange(dvAllList.Count() - i - 1, i).Count() > 0)
                {
                    sma = (from p in dvAllList.GetRange(dvAllList.Count() - i - 1, i) select p.Value).Average();
                }
            }
            else
            {
                sma = (from p in dvAllList.GetRange(dvAllList.Count() - i - 1, periodLength) select p.Value).Average();
            }
            dvObj.Value = sma;
            dvChartList.Add(dvObj);
        }

        //add chart series data here
        //chrtTTS.DataBindCrossTable(dr, "Year", "OrderDate", "TTS", "Label=TTS{N0}", PointSortOrder.Descending);
        chrtNCRs.DataBindCrossTable(dvChartList, "Year", "Date", "Value", "", PointSortOrder.Descending);
        foreach (Series series in chrtNCRs.Series)
        {
            series.ChartType = SeriesChartType.Line;
            series.BorderWidth = 3;
        }

    }

    protected void initSales()
    {

        int startYear = Convert.ToInt32(ddlStartYear.SelectedValue);
        int startMonth = Convert.ToInt32(ddlSalesMonth.SelectedValue);
        DateTime startDate = new DateTime(startYear,startMonth, 1);
        //DateTime endDate = new DateTime(DateTime.Today.Year, 12, 31,23,59,59,997);
        DateTime lastDayofMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1);
        //DateTime endDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, lastDayofMonth.Day, 23, 59, 59, 997);
        DateTime endDate = DateTime.Today.Date.AddDays(1);

        Metrics.SalesType salesType = Metrics.SalesType.ALL; //default to all revenue sources
        switch (radlistSalesSource.SelectedValue)
        {
            case "ALL":
                salesType = Metrics.SalesType.ALL;
                break;

            case "FM-WebSales":
                salesType = Metrics.SalesType.Web;
                break;

            case "POSales":
                salesType = Metrics.SalesType.PO;
                break;

            case "HDI-WebSales":
                salesType = Metrics.SalesType.HDI;
                break;

            case "All-WebSales":
                salesType = Metrics.SalesType.AllWeb;
                break;

            default:
                salesType = Metrics.SalesType.ALL;
                break;
        }

        Metrics.Interval interval = Metrics.Interval.Monthly; //default to all revenue sources

        switch (radlistSalesInterval.SelectedValue)
        {
            case "Day":
                interval = Metrics.Interval.Daily;
                startDate = startDate.Date;
                SetChartTitles(chrtSales, "Revenue By Day (1-Pay Equivalent)", "Daily Revenue", "7D MA", 5, 5);
                SetChartTitles(chrtDevRev, "Device Revenue By Day (1-Pay Equivalent)", "Daily Revenue", "", 5, 5);
                SetChartTitles(chrtNewDevRev, "New Device Revenue By Day (1-Pay Equivalent)", "Daily Revenue", "", 5, 5);
                SetChartTitles(chrtRefurbDevRev, "Refurb Device Revenue By Day (1-Pay Equivalent)", "Daily Revenue", "", 5, 5);
                SetChartTitles(chrtDevASP, "Device ASP By Day (1-Pay Equivalent)", "Daily ASP", "", 5, 5);
                SetChartTitles(chrtNewDevASP, "New Device ASP By Day (1-Pay Equivalent)", "Daily ASP", "", 5, 5);
                SetChartTitles(chrtRefurbDevASP, "Refurb Device ASP By Day (1-Pay Equivalent)", "Daily ASP", "", 5, 5);
                SetChartTitles(chrtAccyRev, "Accy Revenue By Day", "Daily Revenue", "", 5, 5);
                SetChartTitles(chrtAccyASP, "Accy ASP By Day", "Daily Revenue", "", 5, 5);
                SetChartTitles(chrtAccyUnits, "Accessory Units By Day", "Daily Units", "", 5, 5);
                SetChartTitles(chrtTotalItems, "Total Items Sold By Day", "Daily Items", "7D MA", 5, 5);
                SetChartTitles(chrtUnits, "Total FM Devices By Day", "Daily Units", "7D MA", 5, 5);
                SetChartTitles(chrtNewUnits, "New FM Devices By Day", "Daily Units", "7D MA", 5, 5);
                SetChartTitles(chrtRefurbUnits, "Refurbished FM Devices By Day", "Daily Units", "7D MA", 5, 5);
                SetChartTitles(chrtReturns, "Returns By Day", "Daily Returns", "", 5, 5);
                SetChartTitles(chrtReturnsNoDay0, "Returns By Day w/o Day 0", "Daily Returns", "", 5, 5);
                SetChartTitles(chrtReturnPer, "Returns/units (7D MA)", "Daily Return %", "7D MA", 5, 5);
                SetChartTitles(chrtReturnPerNoDay0, "Returns/units (No Day 0, 7D MA)", "Daily Return %", "7D MA", 5, 5);
                SetChartTitles(chrtTotalCERs, "Total CER Entries By Day", "Daily CERs", "", 5, 5);
                SetChartTitles(chrtComplaints, "Complaints By Day", "Daily Complaints", "", 5, 5);
                SetChartTitles(chrtComplaintPer, "Complaints/units (7D MA) (valid for all sales only)", "Daily Complaint % (all sales only)", "", 5, 5);
                break;


            case "Week":
                interval = Metrics.Interval.Weekly;
                int diff = startDate.DayOfWeek - DayOfWeek.Monday;
                if (diff < 0) { diff += 7; }
                startDate = startDate.AddDays(-1 * diff).Date;
                SetChartTitles(chrtSales, "Revenue By Week (1-Pay Equivalent)", "Weekly Revenue", "4W MA", 5, 1);
                SetChartTitles(chrtDevRev, "Device Revenue By Week (1-Pay Equivalent)", "Weekly Revenue", "", 5, 1);
                SetChartTitles(chrtNewDevRev, "New Device Revenue By Week (1-Pay Equivalent)", "Weekly Revenue", "", 5, 1);
                SetChartTitles(chrtRefurbDevRev, "Refurb Device Revenue By Week (1-Pay Equivalent)", "Weekly Revenue", "", 5, 1);
                SetChartTitles(chrtDevASP, "Device ASP By Week (1-Pay Equivalent)", "Weekly ASP", "", 5, 1);
                SetChartTitles(chrtNewDevASP, "New Device ASP By Week (1-Pay Equivalent)", "Weekly ASP", "", 5, 1);
                SetChartTitles(chrtRefurbDevASP, "Refurb Device ASP By Week (1-Pay Equivalent)", "Weekly ASP", "", 5, 1);
                SetChartTitles(chrtAccyRev, "Accy Revenue By Week", "Weekly Revenue", "", 5, 1);
                SetChartTitles(chrtAccyASP, "Accy ASP By Week", "Weekly Revenue", "", 5, 1);
                SetChartTitles(chrtAccyUnits, "Accessory Units By Week", "Weekly Units", "", 5, 1);
                SetChartTitles(chrtTotalItems, "Total Items Sold By Week", "Weekly Items", "4W MA", 5, 1);
                SetChartTitles(chrtUnits, "Total FM Devices By Week", "Weekly Units", "4W MA", 5, 1);
                SetChartTitles(chrtNewUnits, "New FM Devices By Week", "Weekly Units", "4W MA", 5, 1);
                SetChartTitles(chrtRefurbUnits, "Refurbished FM Devices By Week", "Weekly Units", "4W MA", 5, 1);
                SetChartTitles(chrtReturns, "Returns By Week", "Weekly Returns", "", 5, 1);
                SetChartTitles(chrtReturnsNoDay0, "Returns (no Day 0) By Week", "Weekly Returns", "", 5, 1);
                SetChartTitles(chrtReturnPer, "Returns/units (4W MA)", "Weekly Return %", "4W MA", 5, 1);
                SetChartTitles(chrtReturnPerNoDay0, "Returns/units (No Day 0, 4W MA)", "Weekly Return %", "4W MA", 5, 1);
                SetChartTitles(chrtTotalCERs, "Total CER Entries By Week", "Weeky CERs", "", 5, 1);
                SetChartTitles(chrtComplaints, "Complaints By Week", "Weekly Complaints", "", 5, 1);
                SetChartTitles(chrtComplaintPer, "Complaints/units (4W MA) (valid for all sales only)", "Weekly Complaint % (all sales only)", "", 5, 1);
                break;

            case "Month":
                interval = Metrics.Interval.Monthly;
                startDate = new DateTime(startDate.Year, startDate.Month, 1);
                SetChartTitles(chrtSales, "Revenue By Month (1-Pay Equivalent)", "Monthly Revenue", "3M MA", 5, 1);
                SetChartTitles(chrtDevRev, "Device Revenue By Month (1-Pay Equivalent)", "Monthly Revenue", "", 5, 1);
                SetChartTitles(chrtNewDevRev, "New Device Revenue By Month (1-Pay Equivalent)", "Monthly Revenue", "", 5, 1);
                SetChartTitles(chrtRefurbDevRev, "Refurb Device Revenue By Month (1-Pay Equivalent)", "Monthly Revenue", "", 5, 1);
                SetChartTitles(chrtDevASP, "Device ASP By Month (1-Pay Equivalent)", "Monthly ASP", "", 5, 1);
                SetChartTitles(chrtNewDevASP, "New Device ASP By Month (1-Pay Equivalent)", "Monthly ASP", "", 5, 1);
                SetChartTitles(chrtRefurbDevASP, "Refurb Device ASP By Month (1-Pay Equivalent)", "Monthly ASP", "", 5, 1);
                SetChartTitles(chrtAccyRev, "Accy Revenue By Month", "Monthly Revenue", "", 5, 1);
                SetChartTitles(chrtAccyASP, "Accy ASP By Month", "Monthly Revenue", "", 5, 1);
                SetChartTitles(chrtAccyUnits, "Accessory Units By Month", "Monthly Units", "", 5, 1);
                SetChartTitles(chrtTotalItems, "Total Items Sold By Month", "Monthly Items", "3M MA", 5, 1);
                SetChartTitles(chrtUnits, "Total FM Devices By Month", "Monthly Units", "3M MA", 5, 1);
                SetChartTitles(chrtNewUnits, "New FM Devices By Month", "Monthly Units", "3M MA", 5, 1);
                SetChartTitles(chrtRefurbUnits, "Refurbished FM Devices By Month", "Monthly Units", "4W MA", 5, 1);
                SetChartTitles(chrtReturns, "Returns By Month", "Monthly Returns", "", 5, 1);
                SetChartTitles(chrtReturnsNoDay0, "Returns (no Day 0) By Month", "Monthly Returns", "", 5, 1);
                SetChartTitles(chrtReturnPer, "Returns/units (3M MA)", "Monthly Return %", "3M MA", 5, 1);
                SetChartTitles(chrtReturnPerNoDay0, "Returns/units (No Day 0, 3M MA)", "Monthly Return %", "3M MA", 5, 1);
                SetChartTitles(chrtTotalCERs, "Total CER Entries By Month", "Monthly CERs", "", 5, 1);
                SetChartTitles(chrtComplaints, "Complaints By Month", "Monthly Complaints", "", 5, 1);
                SetChartTitles(chrtComplaintPer, "Complaints/units (3M MA) (valid for all sales only)", "Monthly Complaint % (all sales only)", "", 5, 1);
                break;

            case "Quarter":
                interval = Metrics.Interval.Quarterly;
                int quarterNumber = ((startDate.Month - 1) / 3) + 1;
                startDate = new DateTime(startDate.Year, (quarterNumber - 1) * 3 + 1, 1);
                SetChartTitles(chrtSales, "Revenue By Quarter (1-Pay Equivalent)", "Quarterly Revenue", "4Q MA", 5, 1);
                SetChartTitles(chrtDevRev, "Device Revenue By Quarter (1-Pay Equivalent)", "Quarterly Revenue", "", 5, 1);
                SetChartTitles(chrtNewDevRev, "New Device Revenue By Quarter (1-Pay Equivalent)", "Quarterly Revenue", "", 5, 1);
                SetChartTitles(chrtRefurbDevRev, "Refurb Device Revenue By Quarter (1-Pay Equivalent)", "Quarterly Revenue", "", 5, 1);
                SetChartTitles(chrtDevASP, "Device ASP By Quarter (1-Pay Equivalent)", "Quarterly ASP", "", 5, 1);
                SetChartTitles(chrtNewDevASP, "New Device ASP By Quarter (1-Pay Equivalent)", "Quarterly ASP", "", 5, 1);
                SetChartTitles(chrtRefurbDevASP, "Refurb Device ASP By Quarter (1-Pay Equivalent)", "Quarterly ASP", "", 5, 1);
                SetChartTitles(chrtAccyRev, "Accy Revenue By Quarter", "Quarterly Revenue", "", 5, 1);
                SetChartTitles(chrtAccyASP, "Accy ASP By Quarter", "Quarterly Revenue", "", 5, 1);
                SetChartTitles(chrtAccyUnits, "Accessory Units By Quarter", "Quarterly Units", "", 5, 1);
                SetChartTitles(chrtTotalItems, "Total Items Sold By Quarter", "Quarterly Items", "4Q MA", 5, 1);
                SetChartTitles(chrtUnits, "Total FM Devices By Quarter", "Quarterly Units", "4Q MA", 5, 1);
                SetChartTitles(chrtNewUnits, "New FM Devices By Quarter", "Quarterly Units", "4Q MA", 5, 1);
                SetChartTitles(chrtRefurbUnits, "Refurbished FM Devices By Quarter", "Quarterly Units", "4Q MA", 5, 1);
                SetChartTitles(chrtReturns, "Returns By Quarter", "Quarterly Returns", "", 5, 1);
                SetChartTitles(chrtReturnsNoDay0, "Returns (no Day 0) By Quarter", "Quarterly Returns", "", 5, 1);
                SetChartTitles(chrtReturnPer, "Returns/units (4Q MA)", "Quarterly Return %", "4Q MA", 5, 1);
                SetChartTitles(chrtReturnPerNoDay0, "Returns/units (No Day 0, 4Q MA)", "Quarterly Return %", "4Q MA", 5, 1);
                SetChartTitles(chrtTotalCERs, "Total CER Entries By Quarter", "Quarterly CERs", "", 5, 1);
                SetChartTitles(chrtComplaints, "Complaints By Quarter", "Quarterly Complaints", "", 5, 1);
                SetChartTitles(chrtComplaintPer, "Complaints/units (4Q MA) (valid for all sales only)", "Quarterly Complaint % (all sales only)", "", 5, 1);
                break;

            case "Year":
                interval = Metrics.Interval.Yearly;
                startDate = new DateTime(startDate.Year, 1, 1);
                SetChartTitles(chrtSales, "Revenue By Year (1-Pay Equivalent)", "Yearly Revenue", "2Y MA", 1, 1);
                SetChartTitles(chrtDevRev, "Device Revenue By Year (1-Pay Equivalent)", "Yearly Revenue", "", 1, 1);
                SetChartTitles(chrtNewDevRev, "New Device Revenue By Year (1-Pay Equivalent)", "Yearly Revenue", "", 5, 1);
                SetChartTitles(chrtRefurbDevRev, "Refurb Device Revenue By Year (1-Pay Equivalent)", "Yearly Revenue", "", 5, 1);
                SetChartTitles(chrtAccyRev, "Accy Revenue By Year", "Yearly Revenue", "", 5, 1);
                SetChartTitles(chrtAccyASP, "Accy ASP By Year", "Yearly Revenue", "", 5, 1);
                SetChartTitles(chrtAccyUnits, "Accessory Units By Year", "Yearly Units", "", 1, 1);
                SetChartTitles(chrtTotalItems, "Total Items Sold By Year", "Yearly Items", "2Y MA", 5, 1);
                SetChartTitles(chrtUnits, "Total FM Devices By Year", "Yearly Units", "2Y MA", 1, 1);
                SetChartTitles(chrtNewUnits, "New FM Devices By Year", "Yearly Units", "2Y MA", 1, 1);
                SetChartTitles(chrtRefurbUnits, "Refurbished FM Devices By Year", "Yearly Units", "2Y MA", 1, 1);
                SetChartTitles(chrtReturns, "Returns By Year", "Yearly Returns", "", 1, 1);
                SetChartTitles(chrtReturnsNoDay0, "Returns (No Day 0) By Year", "Yearly Returns", "", 1, 1);
                SetChartTitles(chrtReturnPer, "Returns/units (2Y MA)", "Yearly Return %", "2Y MA", 1, 1);
                SetChartTitles(chrtReturnPerNoDay0, "Returns/units (No Day 0, 2Y MA)", "Yearly Return %", "2Y MA", 1, 1);
                SetChartTitles(chrtTotalCERs, "Total CER Entries By Year", "Yearly CERs", "", 5, 1);
                SetChartTitles(chrtComplaints, "Complaints By Year", "Yearly Complaints", "", 1, 1);
                SetChartTitles(chrtComplaintPer, "Complaints/units (2Y MA) (valid for all sales only)", "Yearly Complaint % (all sales only)", "", 1, 1);
                break;

            default:
                interval = Metrics.Interval.Monthly;
                startDate = new DateTime(startDate.Year, startDate.Month, 1);
                SetChartTitles(chrtSales, "Revenue By Month (1-Pay Equivalent)", "Monthly Revenue", "3M MA", 5, 1);
                SetChartTitles(chrtDevRev, "Device Revenue By Month (1-Pay Equivalent)", "Monthly Revenue", "", 5, 1);
                SetChartTitles(chrtNewDevRev, "New Device Revenue By Month (1-Pay Equivalent)", "Monthly Revenue", "", 5, 1);
                SetChartTitles(chrtRefurbDevRev, "Refurb Device Revenue By Month (1-Pay Equivalent)", "Monthly Revenue", "", 5, 1);
                SetChartTitles(chrtDevASP, "Device ASP By Month (1-Pay Equivalent)", "Monthly ASP", "", 5, 1);
                SetChartTitles(chrtNewDevASP, "New Device ASP By Month (1-Pay Equivalent)", "Monthly ASP", "", 5, 1);
                SetChartTitles(chrtRefurbDevASP, "Refurb Device ASP By Month (1-Pay Equivalent)", "Monthly ASP", "", 5, 1);
                SetChartTitles(chrtAccyRev, "Accy Revenue By Month", "Monthly Revenue", "", 5, 1);
                SetChartTitles(chrtAccyASP, "Accy ASP By Month", "Monthly Revenue", "", 5, 1);
                SetChartTitles(chrtAccyUnits, "Accessory Units By Month", "Monthly Units", "", 5, 1);
                SetChartTitles(chrtTotalItems, "Total Items Sold By Month", "Monthly Items", "3M MA", 5, 1);
                SetChartTitles(chrtUnits, "Total FM Devices By Month", "Monthly Units", "3M MA", 5, 1);
                SetChartTitles(chrtNewUnits, "New FM Devices By Month", "Monthly Units", "3M MA", 5, 1);
                SetChartTitles(chrtRefurbUnits, "Refurbished FM Devices By Month", "Monthly Units", "4W MA", 5, 1);
                SetChartTitles(chrtReturns, "Returns By Month", "Monthly Returns", "", 5, 1);
                SetChartTitles(chrtReturnsNoDay0, "Returns (No Day 0) By Month", "Monthly Returns", "", 5, 1);
                SetChartTitles(chrtReturnPer, "Returns/units (3M MA)", "Monthly Return %", "3M MA", 5, 1);
                SetChartTitles(chrtReturnPerNoDay0, "Returns/units (No Day 0, 3M MA)", "Monthly Return %", "3M MA", 5, 1);
                SetChartTitles(chrtTotalCERs, "Total CER Entries By Month", "Monthly CERs", "", 5, 1);
                SetChartTitles(chrtComplaints, "Complaints By Month", "Monthly Complaints", "", 5, 1);
                SetChartTitles(chrtComplaintPer, "Complaints/units (3M MA) (valid for all sales only)", "Monthly Complaint % (all sales only)", "", 5, 1);
                break;
        }

        DataSet ds = Metrics.GetMetricSet(interval, salesType, startDate, endDate);

        SetSalesChartsParameters(chrtSales, ds.Tables["Metrics"], "Revenue", "Date", "revMA", "Date", "Date", "Revenue", "C0", "C0");
        SetSalesChartsParameters(chrtDevRev, ds.Tables["Metrics"], "DevRev", "Date", "", "", "Date", "Revenue", "C0", "C0");
        SetSalesChartsParameters(chrtNewDevRev, ds.Tables["Metrics"], "NewDevRev", "Date", "", "", "Date", "Revenue", "C0", "C0");
        SetSalesChartsParameters(chrtRefurbDevRev, ds.Tables["Metrics"], "RefurbDevRev", "Date", "", "", "Date", "Revenue", "C0", "C0");
        SetSalesChartsParameters(chrtDevASP, ds.Tables["Metrics"], "DevASP", "Date", "", "", "Date", "Average Selling Price", "C0", "C0");
        SetSalesChartsParameters(chrtNewDevASP, ds.Tables["Metrics"], "NewDevASP", "Date", "", "", "Date", "Average Selling Price", "C0", "C0");
        SetSalesChartsParameters(chrtRefurbDevASP, ds.Tables["Metrics"], "RefurbDevASP", "Date", "", "", "Date", "Average Selling Price", "C0", "C0");
        SetSalesChartsParameters(chrtAccyRev, ds.Tables["Metrics"], "AccyRev", "Date", "", "", "Date", "Revenue", "C0", "C0");
        SetSalesChartsParameters(chrtAccyASP, ds.Tables["Metrics"], "AccyASP", "Date", "", "", "Date", "Average Selling Price", "C0", "C0");
        SetSalesChartsParameters(chrtAccyUnits, ds.Tables["Metrics"], "AccyUnits", "Date", "", "", "Date", "TotalAccy Units", "N0", "N0");
        SetSalesChartsParameters(chrtTotalItems, ds.Tables["Metrics"], "Items", "Date", "ItemsMA", "Date", "Date", "Items", "N0", "N0");
        SetSalesChartsParameters(chrtUnits, ds.Tables["Metrics"], "Units", "Date", "UnitsMA", "Date", "Date", "Devices", "N0", "N0");
        SetSalesChartsParameters(chrtNewUnits, ds.Tables["Metrics"], "NewUnits", "Date", "", "", "Date", "Devices", "N0", "N0");
        SetSalesChartsParameters(chrtRefurbUnits, ds.Tables["Metrics"], "RefurbUnits", "Date", "", "", "Date", "Devices", "N0", "N0");
        SetSalesChartsParameters(chrtReturns, ds.Tables["Metrics"], "Returns", "Date", "", "", "Date", "Returns", "N0", "N0");
        SetSalesChartsParameters(chrtReturnsNoDay0, ds.Tables["Metrics"], "ReturnsNoDay0", "Date", "", "", "Date", "Returns", "N0", "N0");
        SetSalesChartsParameters(chrtReturnPer, ds.Tables["Metrics"], "ReturnPercent", "Date", "", "", "Date", "% Returns", "P2", "P2");
        SetSalesChartsParameters(chrtReturnPerNoDay0, ds.Tables["Metrics"], "ReturnPercentNoDay0", "Date", "", "", "Date", "% Returns", "P2", "P2");
        SetSalesChartsParameters(chrtTotalCERs, ds.Tables["Metrics"], "TotalCERs", "Date", "", "", "Date", "# Received", "N0", "N0");
        SetSalesChartsParameters(chrtComplaints, ds.Tables["Metrics"], "Complaints", "Date", "", "", "Date", "# Received", "N0", "N0");
        SetSalesChartsParameters(chrtComplaintPer, ds.Tables["Metrics"], "ComplaintPercent", "Date", "", "", "Date", "% Complaints", "P2", "P2");

        // Data bind to the selected data source
        chrtSales.DataBind();
    }

    public void SetSalesChartsParameters(Chart chartControl, DataTable dataSource, string Series1_YValueMember, string Series1_XValueMember,
      string Series2_YValueMember, string Series2_XValueMember, string ChartArea_AxisX_Title, string ChartArea_AxisY_Title,
      string Series1_LabelFormat, string ChartArea_AXisY_LabelStyle_Format)
    {
        chartControl.DataSource = dataSource;
        chartControl.Series["Series1"]["labelStyle"] = "Top";
        chartControl.Series["Series1"].YValueMembers = Series1_YValueMember;
        chartControl.Series["Series1"].XValueMember = Series1_XValueMember;
        chartControl.Series["Series2"].YValueMembers = Series2_YValueMember;
        chartControl.Series["Series2"].XValueMember = Series2_XValueMember;
        chartControl.ChartAreas["ChartArea1"].AxisX.Title = ChartArea_AxisX_Title;
        chartControl.ChartAreas["ChartArea1"].AxisY.Title = ChartArea_AxisY_Title;
        // Set labels format
        chartControl.Series["Series1"].LabelFormat = Series1_LabelFormat;
        chartControl.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = ChartArea_AXisY_LabelStyle_Format;
    }

    public void SetChartTitles(Chart chartControl, string Title1, string Series1_LegendText, string Series2_LegendText,
        int ChartArea_AxisX_MajorGrid_Interval, int ChartArea_AxisX_Interval)
    {
        chartControl.Titles["Title1"].Text = Title1;
        chartControl.Series["Series1"].LegendText = Series1_LegendText;
        chartControl.Series["Series2"].LegendText = Series2_LegendText;
        chartControl.ChartAreas["ChartArea1"].AxisX.MajorGrid.Interval = ChartArea_AxisX_MajorGrid_Interval;
        chartControl.ChartAreas["ChartArea1"].AxisX.Interval = ChartArea_AxisX_Interval;
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        this.Page.RenderControl(hw);
        StringReader sr = new StringReader(sw.ToString());
        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0.0f);
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }

}