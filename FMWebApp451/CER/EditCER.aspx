<%@ Page Language="C#" EnableViewState="true" EnableEventValidation="false" MaintainScrollPositionOnPostback="true" Trace="false" Theme="Admin" MasterPageFile="~/admin/MasterPage3_Blank.master" AutoEventWireup="true" Inherits="AddComplaint" Title="FaceMaster - Complaint Resolution" ValidateRequest="false" Codebehind="EditCER.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<div class="alpha eight columns">

<asp:Literal ID="LookupPopup" runat="server" />
<asp:Label ID="lblAlert" runat="server" Font-Bold="true" ForeColor="red" Font-Size="14px" />

   <div>
        <strong>Customer Experience Report</strong>
        &nbsp;&nbsp;
        <asp:HyperLink ID="lnkPrint" runat="server" Text="Print Form" Visible="false" Target="_blank" />
        &nbsp;&nbsp;
        <asp:HyperLink ID="lnkLookUp" runat="server" Text="LookUp" Visible="false" Target="_blank" />
        &nbsp;&nbsp;
       <!--
        <asp:LinkButton ID="lnkUploadPics" runat="server" Text="Upload Pics" Visible="false" OnCommand="changeLinkButton_OnCommand" CommandArgument="upload" CausesValidation="false" />
        &nbsp;&nbsp;
        <asp:LinkButton ID="lnkViewPics" runat="server" Text="View Pics" Visible="false" OnCommand="changeLinkButton_OnCommand" CommandArgument="view" CausesValidation="false" />
       -->
    </div>

    <div style="clear:both"></div>
    <div class="groupHTML">
        <asp:Label ID="lblReceivedBy" runat="server" Text="Received by:&nbsp;"></asp:Label>
        <asp:Label ID="literalReceivedBy" runat="server" Font-Bold="true" />
    </div>

</div>

<div class="omega eight columns">

    <div class="groupHTML">
        <asp:Label ID="LabelCerNumber" runat="server" Text="CER:&nbsp;" />
        <asp:Label ID="lblCerNumber_Year" runat="server" Width="7%"></asp:Label>
        <asp:Label ID="lblCerNumberSpacer" runat="server" Text="&nbsp;- &nbsp;" />
        <asp:Label ID="lblCerNumber_Sequence" runat="server" Width="10%"></asp:Label>
    </div>

    <div style="clear:both"></div>
    <div class="groupHTML">
        <asp:Label ID="LabelDateReceived" runat="server" Text="Date Received:&nbsp;"></asp:Label>
        <asp:Label ID="TxtDateReceived" runat="server"/>
    </div>
</div>


<div class="alpha omega sixteen columns">
    <asp:Label ID="PageMessage" runat="server" Font-Bold="true" ForeColor="red" />
    <hr />

</div>


<div class="alpha eight columns">

    <div style="margin-left:0%; margin-bottom:10px">
        <strong>CER General Information</strong>
    </div>
    
        <div class="groupHTML">
            <asp:Label ID="LabelSource" runat="Server" Text="Point of Purchase:&nbsp;"/>
            <asp:DropDownList ID="selPointOfPurchase" runat="server" TabIndex="1" Width="30%">
            </asp:DropDownList>        
        </div>

        <div style="clear:both">&nbsp;</div>
        <div class="groupHTML">
            <div class="groupHTML">
                <asp:Label ID="lblcompType" runat="server" Text="CER Type: &nbsp;" />
                <asp:RadioButton ID="radioCerTypeC" text="Complaint" CssClass="groupHTML" runat="server" GroupName="CERType" AutoPostBack="true" TabIndex="2" />
            </div>
            <asp:Label ID="Label7" runat="server" Text="&nbsp; &nbsp;" />
            <asp:RadioButton ID="radioCerTypeNC" text="Non-Complaint" CssClass="groupHTML" runat="server" Checked="true" GroupName="CERType" AutoPostBack="true" />
            <div style="clear:both"></div>
            <asp:Label ID="label6" runat="server" Text="Complaint determined by: &nbsp;" /> 
            <asp:Label ID="literalComplaintDeterminationBy" runat="server" Font-Bold="true" />
        </div>

</div>

<div class="omega eight columns">

    <asp:Panel ID="pnlAddRelated" DefaultButton="btnAdd" runat="server">
    <div class="groupHTML">
        <asp:Label ID="lblAddRelated" runat="server" Text="Add Related CER:&nbsp;" />
        <asp:TextBox ID="RelatedCERNumber_Year" runat="server" Width="10%" />
        <asp:Label ID="lblCERnumspc2" runat="server" Text="&nbsp;-&nbsp;" />
        <asp:TextBox ID="RelatedCERNumber_Sequence" runat="server" Width="10%" />
        <asp:Label ID="lblCERnumspc3" runat="server" Text="&nbsp; &nbsp;" />
        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" OnClientClick="aspnetForm.target ='_self'" CausesValidation="false" />
    </div>
    <div style="clear:both"></div>
    </asp:Panel>
    Related CERs: <asp:Literal ID="literalRelatedCERList" runat="server" />
    <div style="clear:both"></div>

</div>

<div class="alpha omega sixteen columns">
    <hr />
</div>

<asp:Panel ID="pnlDetails" DefaultButton="btnDetailsUpdate" runat="server">
<div class="alpha eight columns">
    <div class="groupHTML">
        <asp:Label ID="Label22" Text="<strong>Reporting Party</strong>" runat="server" />
        <div class="simpleClear"></div>
        <asp:Label ID="labelCustomerID" Text="Customer ID:&nbsp;" runat="server" />
        <!--
        <asp:Label ID="lblCustomerID" Text="" runat="server" />
        -->
        <asp:HyperLink ID="lnkCustomerID" runat="server" Text="" Visible="true" Target="_blank" />
        <div class="simpleClear"></div>
        <asp:Label ID="labelFirstName" Text="First Name:&nbsp;" runat="server" />
        <asp:Label ID="txtFirstName" runat="Server" ></asp:Label>
        <div class="simpleClear"></div>
        <asp:Label ID="labelLastName" Text="Last Name:&nbsp;" runat="server" />
        <asp:Label ID="txtLastName" runat="Server" />    
        <div class="simpleClear">&nbsp;</div>
        <asp:Button ID="btnDetailsUpdate" runat="server" Text="Update Details" OnClick="btnDetailsUpdate_Click" Visible="true" CausesValidation="false"/>            
    </div>
</div>

<div class="omega eight columns">
    <div class="formHTML">
        <asp:Label ID="label1" Text="Street:&nbsp;" runat="server" />
        <asp:Label ID="txtStreet" style="width:50%; text-align:left" runat="Server" />
        <div class="simpleClear"></div>
        <asp:Label ID="label2" Text="City:&nbsp;" runat="server" />
        <asp:Label ID="txtCity" style="width:50%; text-align:left" runat="Server" />
        <div class="simpleClear"></div>
        <asp:Label ID="label3" Text="State:&nbsp;" runat="server" />
        <asp:Label id="txtState" style="width:50%; text-align:left" runat="server" />
        <div class="simpleClear"></div>
        <asp:Label ID="label4" Text="Zip:&nbsp;" runat="server" />
        <asp:Label ID="txtZip" style="width:50%; text-align:left" runat="Server" />   
        <div class="simpleClear"></div>
        <asp:Label ID="label5" Text="Country:&nbsp;" runat="server" />
        <asp:Label ID="txtCountry" style="width:50%; text-align:left" runat="server" />
        <div class="simpleClear"></div>
        <asp:Label ID="label8" Text="Email:&nbsp;" runat="server" />
        <asp:Label ID="txtEmail" runat="Server" />
        <div class="simpleClear"></div>
        <asp:Label ID="label9" Text="Phone:&nbsp;" runat="server" />
        <asp:Label ID="txtPhone" style="width:50%; text-align:left" runat="Server" />     
    </div>
</div>
</asp:Panel>

<div class="alpha omega sixteen columns">
    <hr />
    <div style="margin-left:0%; margin-bottom:10px">
        <strong>Event Information</strong>
    </div>
</div>

<div class="alpha eight columns">
    <div class="formHTML">
        <asp:Label ID="label12" runat="server" Text="Product:" />
        <asp:DropDownList ID="selProductNumber" runat="server" TabIndex="13">
                <asp:ListItem>FaceMaster</asp:ListItem>
                <asp:ListItem>Conductive Solution</asp:ListItem>
                <asp:ListItem>Conductive Solution - 2 Pack</asp:ListItem>
                <asp:ListItem>Conductive Solution and Foam Cap Combo</asp:ListItem>
                <asp:ListItem>FaceMaster Replacement Probes</asp:ListItem>
                <asp:ListItem>Foam Caps</asp:ListItem>
                <asp:ListItem>Foam Caps - 2 Pack</asp:ListItem>
                <asp:ListItem>Step by Step Instructional DVD</asp:ListItem>
                <asp:ListItem>Step by Step Instructional VHS Tape</asp:ListItem>
        </asp:DropDownList>

        <p>&nbsp;</p>

        <asp:Label ID="label14" runat="server" Text="Order No:&nbsp;"></asp:Label>
        <asp:TextBox ID="txtOrderNumber" runat="server" width="12%" TabIndex="15" />
        &nbsp;&nbsp;
        <asp:HyperLink ID="lnkReviewOrder" runat="server" Text="Review Order" NavigateUrl="~/admin/ReviewOrder.aspx?OrderID=" Target="_blank" Visible="false" ></asp:HyperLink>
        <asp:RangeValidator id="checkOrder" controlToValidate="txtOrderNumber" errorMessage="* must be numeric" type="Double" minimumValue="0" maximumValue="100000000" runat="server" Display="Dynamic"  />

    </div>
</div>

<div class="omega eight columns">
    <div class="groupHTML">
        <asp:Label ID="label13" runat="server" Text="Serial No:&nbsp;"></asp:Label>
        <asp:TextBox ID="txtSerialNumber" runat="server" TabIndex="14" />
        
        <p>&nbsp;</p>

        <asp:Label ID="label15" runat="server" Text="Timing:&nbsp;"></asp:Label>
        <asp:RadioButton id="radEOAsUnpackaged" CssClass="groupHTML" Text="Packaged" GroupName="EO" runat="server" TabIndex="16" />
        <asp:Label ID="Label19" runat="server" Text="&nbsp;" />
        <asp:RadioButton id="radEOPrior" CssClass="groupHTML" Text="Prior to Use" GroupName="EO" runat="server" /> 
        <asp:Label ID="Label20" runat="server" Text="&nbsp;" />
        <asp:RadioButton id="radEODuring" CssClass="groupHTML" Text="During Use" GroupName="EO" runat="server" />  
        <asp:Label ID="Label21" runat="server" Text="&nbsp;" />
        <asp:RadioButton id="radEOAfter" CssClass="groupHTML" Text="After Use" GroupName="EO" runat="server" />        
    </div>

    <div style="clear:both"></div>
    <div class="groupHTML">
        <asp:Label ID="lblCondition" runat="server" Text="Medical Condition:&nbsp;" Visible="false" />
        <asp:RadioButtonList ID="radCondition" runat="server" RepeatDirection="Horizontal" Visible="false">
            <asp:ListItem Enabled="false">Serious</asp:ListItem>
            <asp:ListItem Enabled="false">Death</asp:ListItem>
            <asp:ListItem Enabled="false">n/a</asp:ListItem>
            <asp:ListItem Enabled="false">Temp Injury</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ControlToValidate="radCondition" ErrorMessage="* select one<br />" ForeColor="Red" id="Requiredfieldvalidator1" runat="server" Display="Dynamic" />
    </div>

</div>

<div class="alpha omega sixteen columns">
    <hr />
    <div>
        <strong>FM ADMIN Order Notes</strong>
       <asp:GridView ID="GridFMOrderNotes" SkinID="Admin" runat="server" AutoGenerateColumns="false" Width="100%" AlternatingRowStyle-BackColor="#fff8c6">
        <Columns>
            <asp:BoundField HeaderStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" HeaderText="Date" DataField="OrderNoteDate" HeaderStyle-Width="15%" />    
            <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderText="Note" DataField="OrderNote" /> 
            <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderText="By" DataField="OrderNoteBy" /> 
        </Columns>
        <EmptyDataTemplate><strong>There are no order notes for this order</strong></EmptyDataTemplate>
        </asp:GridView>    
    </div>
</div>

<div class="alpha omega sixteen columns">
    <hr />
</div>

<div class="alpha eight columns">         
    <strong>Event&nbsp;Description:&nbsp;</strong>
    <asp:TextBox ID="txtEventDescription" runat="server" width="100%" Rows="15" TextMode="MultiLine" TabIndex="17" />
    <asp:Label ID="lblEventDescription" runat="server" Visible="false" />
</div>

<div class="omega eight columns"> 
    <asp:Panel ID="pnlActionTaken" runat="server">

    <div>
        <strong>Action Taken</strong>
    </div>
    <div class="groupHTML">
        <asp:Label ID="Label23" runat="server" Text="CS:&nbsp;" />
        <asp:RadioButtonList ID="radStatusCs" runat="server" RepeatDirection="Horizontal" TabIndex="22" AutoPostBack="true"
                OnSelectedIndexChanged="radStatusCs_SelectedIndexChanged" >
            <asp:ListItem Enabled="false">Opened</asp:ListItem>
            <asp:ListItem Enabled="false">Closed</asp:ListItem>
        </asp:RadioButtonList>
    </div>

    <div style="clear:both">&nbsp;</div>
    <div class="groupHTML">
        <asp:Label ID="Label24" runat="server" Text="QA:&nbsp;" />             
        <asp:RadioButtonList ID="radStatusQa" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
        OnSelectedIndexChanged="radStatusQa_SelectedIndexChanged" TabIndex="23">
            <asp:ListItem Enabled="false">Unopened</asp:ListItem>
            <asp:ListItem Enabled="false">Opened</asp:ListItem>
            <asp:ListItem Enabled="false">Closed</asp:ListItem>
        </asp:RadioButtonList>
    </div>

    <div style="clear:both">&nbsp;</div>
    <div class="groupHTML">
        <asp:Label ID="Label25" runat="server" Text="Medical:&nbsp;" />
        <asp:RadioButtonList ID="radStatusMed" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
        OnSelectedIndexChanged="radStatusMed_SelectedIndexChanged" TabIndex="24">          
            <asp:ListItem Enabled="false">Does Not Apply</asp:ListItem>
            <asp:ListItem Enabled="false">Assigned</asp:ListItem>
            <asp:ListItem Enabled="false">Closed</asp:ListItem>
        </asp:RadioButtonList>
    </div>

    <div class="groupHTML">
        <div style="clear:both">&nbsp;</div>

        <asp:Label ID="label27" runat="server" Text="MDR:&nbsp;"></asp:Label>
        <asp:RadioButtonList ID="radListMDR" runat="server" RepeatDirection="Horizontal" Visible="true">
            <asp:ListItem Enabled="false">Yes</asp:ListItem>
            <asp:ListItem Enabled="false">No</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ControlToValidate="radListMDR" ErrorMessage="* select one<br />" ForeColor="Red" id="Requiredfieldvalidator3" runat="server" Display="Dynamic" />
    </div>

    <div class="groupHTML">
        <div style="clear:both">&nbsp;</div>

        <asp:Label ID="label28" runat="server" Text="Analyze:&nbsp;"></asp:Label>
        <asp:RadioButtonList ID="radListAnalyze" runat="server" RepeatDirection="Horizontal" Visible="true">
            <asp:ListItem Enabled="false">Yes</asp:ListItem>
            <asp:ListItem Enabled="false">No</asp:ListItem>
        </asp:RadioButtonList>
        <asp:RequiredFieldValidator ControlToValidate="radListAnalyze" ErrorMessage="* select one<br />" ForeColor="Red" id="Requiredfieldvalidator4" runat="server" Display="Dynamic" />
    </div>

    <asp:Panel ID="pnlUpdate" DefaultButton="ButtonUpdate" runat="server">
    <div style="clear:both">&nbsp;</div>
    <div class="groupHTML">
        <asp:Label ID="label18" runat="server" Text="Reason:&nbsp;"></asp:Label>
        <asp:RequiredFieldValidator ControlToValidate="txtActionExplaination" ErrorMessage="<br />* fill in " ForeColor="Red" id="Requiredfieldvalidator2" runat="server" Display="Dynamic" ValidationGroup="updateGroup" />
        <asp:TextBox ID="txtActionExplaination" runat="server" width="40%" Rows="1" TabIndex="25" />
        <asp:Label ID="label26" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
        <asp:Button ID="ButtonUpdate" runat="server" Text="Update Record" OnClick="ButtonUpdate_Click" Visible="false" OnClientClick="aspnetForm.target ='_self'" />            
        
        <div style="clear:both"></div>
        <asp:Panel ID="pnlCloseActionShortCuts" runat="server" Visible="false">
            <asp:LinkButton ID="lnkBtnReviewed" OnClick="lnkBtnReviewed_Click" OnClientClick="aspnetForm.target ='_self'" Visible="false" CausesValidation="false" runat="server">NC(close)</asp:LinkButton>
            &nbsp; &nbsp; 
            <asp:LinkButton ID="lnkBtnComplaint" OnClick="lnkBtnComplaint_Click" OnClientClick="aspnetForm.target ='_self'" Visible="false" CausesValidation="false" runat="server">C(open)</asp:LinkButton>
            &nbsp; &nbsp; 
            <asp:LinkButton ID="lnkBtnOpenMedComplaint" OnClick="lnkBtnMedComplaint_Click" OnClientClick="aspnetForm.target ='_self'" Visible="false" CausesValidation="false" runat="server">MedC(open)</asp:LinkButton>
            &nbsp; &nbsp; 
            <asp:LinkButton ID="lnkBtnDNR" OnClick="lnkBtnDNR_Click" OnClientClick="aspnetForm.target ='_self'" Visible="false" CausesValidation="false" runat="server">DNR(close)</asp:LinkButton>
            &nbsp; &nbsp; 
            <asp:LinkButton ID="lnkBtnNTF" OnClick="lnkBtnNTF_Click" OnClientClick="aspnetForm.target ='_self'" Visible="false" CausesValidation="false" runat="server">NTF(close)</asp:LinkButton>
        </asp:Panel>
        
    </div>
    </asp:Panel>

    </asp:Panel>

</div> 

<div class="alpha omega sixteen columns">
    <hr />
</div> 

<div class="alpha omega sixteen columns">
    <asp:Panel ID="pnlMessage" DefaultButton="ButtonAddMessage" runat="server">
    <strong>New Message:</strong>
    <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="5" Width="100%" TabIndex="18" />
    <div class="simpleClear"></div>
    <div class="groupHTML">
        <asp:Label ID="label11" runat="server" Text="Date:&nbsp;" /> 
        <asp:TextBox ID="txtDate" runat="server" width="15%" TabIndex="19" />
        <asp:Label ID="label16" runat="server" Text="&nbsp;&nbsp;Time:&nbsp;" /> 
        <asp:TextBox ID="txtTime" runat="server" width="15%" TabIndex="20" />
        <asp:Label ID="label17" runat="server" Text="&nbsp;&nbsp;Type:&nbsp;" /> 
        <asp:DropDownList ID="selType" runat="server" TabIndex="21" Width="10%">
            <asp:ListItem>Call</asp:ListItem>
            <asp:ListItem>Email</asp:ListItem>
        </asp:DropDownList>
        <asp:Label ID="label10" runat="server" Text="&nbsp;&nbsp;" />
        <asp:Button ID="ButtonAddMessage" runat="server" Text="Add Message" OnClick="ButtonAddMessage_Click" OnClientClick="aspnetForm.target ='_self'" CausesValidation="false" />
    </div>
    </asp:Panel>
</div> 

<div class="alpha omega sixteen columns">
    <div>
        <strong>Related&nbsp;CER&nbsp;Messages</strong>
        <asp:GridView ID="gridMessages" SkinID="Admin" runat="server" AutoGenerateColumns="false" Width="100%" AlternatingRowStyle-BackColor="#fff8c6">
        <Columns>
            <asp:BoundField HeaderStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" HeaderText="Dated" DataField="Dated" />    
            <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderText="Message" DataField="Message" /> 
            <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderText="AddBy" DataField="AddBy" /> 
        </Columns>
        <EmptyDataTemplate><strong>There are no messages for this CER</strong></EmptyDataTemplate>        
        </asp:GridView>
    </div>
</div>    
   
<div class="alpha omega sixteen columns">
    <hr />
    <div>
        <strong>CER History</strong>
    </div>
    <asp:GridView ID="gridHistory" SkinID="Admin" runat="server" AutoGenerateColumns="false" Width="100%">
    <Columns>
        <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderText="Date Updated" DataField="InsertDate" />
        <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderText="Updated by" DataField="InsertBy" />
        <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderText="Reason for Update" DataField="ActionExplaination" />        
        
        <asp:TemplateField>
        <ItemTemplate>
            <a target="_blank" href="CERHistory.aspx?id=<%# DataBinder.Eval(Container.DataItem,"HistoryId") %>">View This Update</a>
        </ItemTemplate>
        </asp:TemplateField>        
    </Columns>
    <EmptyDataTemplate><strong>There is no history for this CER</strong></EmptyDataTemplate>        
    </asp:GridView>
</div>

<div class="alpha omega sixteen columns">
    <hr />
</div> 

<div class="alpha eight columns">   

    <asp:Panel ID="pnlDefectAnalysis" runat="server">

        <strong>CER Defect Analysis</strong>
        <asp:DetailsView 
        ID="dtlDefectAnalysis"
        SkinID="dtlsAdmin" 
        AutoGenerateRows="false"
        DefaultMode="ReadOnly"
        AllowPaging="false"
        autogenerateinsertbutton="true"
        autogenerateeditbutton="true" 
        OnItemUpdating="dtlDefectAnalysis_Updating" 
        OnItemInserting="dtlDefectAnalysis_Inserting"
        datakeynames="ID"
        DataSourceID="sqlDefectsDetails"
        runat="server">   
        <FieldHeaderStyle />

        <Fields>

          <asp:TemplateField HeaderText="ID">
            <ItemTemplate>
               <asp:Label ID="lblID" Visible="true" Text='<%# Eval("ID") %>' runat="server"></asp:Label>
            </ItemTemplate>
         </asp:TemplateField>
                         
          <asp:TemplateField HeaderText="Date">
            <ItemTemplate>
               <asp:Label ID="lblDate" Visible="true" Text='<%# Eval("Date") %>' runat="server"></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtDate" runat="server" Text='<%# Bind("Date") %>' Width="45%" MaxLength="100"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequireDate" runat="server" ControlToValidate="edittxtDate" SetFocusOnError="true" ValidationGroup="DefectAnalysis"
                  Text="The Date field is required." ToolTip="The Date field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:CustomValidator ID="valeditDate" runat="server" ControlToValidate="edittxtDate" ValidationGroup="DefectAnalysis" 
                ErrorMessage="<br />The format of the Date is not valid." OnServerValidate="valeditDate_ServerValidate"></asp:CustomValidator>
           </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtDate" runat="server" OnLoad="inserttxtDate_OnLoad" Text='<%# Bind("Date") %>' Width="45%" MaxLength="100"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequireDate" runat="server" ControlToValidate="InserttxtDate" SetFocusOnError="true" ValidationGroup="DefectAnalysis"
                  Text="The Date field is required." ToolTip="The Date field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:CustomValidator ID="valinsertDate" runat="server" ControlToValidate="inserttxtDate" ValidationGroup="DefectAnalysis" 
                ErrorMessage="<br />The format of the Date is not valid." OnServerValidate="valinsertDate_ServerValidate"></asp:CustomValidator>
            </InsertItemTemplate>
         </asp:TemplateField>

          <asp:TemplateField HeaderText="IsMedical">
            <ItemTemplate>
               <asp:RadioButton ID="rdbtnIsMedical" Checked='<%# Eval("IsMedical") %>' runat="server"></asp:RadioButton>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:RadioButton ID="editrdbtnIsMedical" Checked='<%# Bind("IsMedical") %>' runat="server" ></asp:RadioButton>
           </EditItemTemplate>
            <InsertItemTemplate>
               <asp:RadioButton ID="insertrdbtnIsMedical" Checked='<%# Bind("IsMedical") %>' runat="server" ></asp:RadioButton>
            </InsertItemTemplate>
         </asp:TemplateField>

        <asp:TemplateField HeaderText="Symptom">
            <ItemTemplate>
               <asp:Label ID="Symptom" runat="server" Text='<%# Eval("Symptom") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
              <asp:DropDownList id="editddlSymptom" datasourceid="SymptomSqlDataSource"
			        datatextfield="Name" 	DataValueField="Value"  
			        SelectedValue='<%# Bind("SymptomVal") %>'   runat="server"/>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:DropDownList ID="insertddlSymptom" datasourceid="SymptomSqlDataSource"
			        datatextfield="Name" 	DataValueField="Value"  
			        SelectedValue='<%# Bind("SymptomVal") %>' runat="server">
               </asp:DropDownList>
           </insertItemTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="Defect">
            <ItemTemplate>
               <asp:Label ID="lblDefect" runat="server" Text='<%# Eval("Defect") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:DropDownList ID="editddlDefect" datasourceid="DefectSqlDataSource"
			        datatextfield="Name" 	DataValueField="Value"  
			        SelectedValue='<%# Bind("DefectVal") %>'  runat="server">
               </asp:DropDownList>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:DropDownList ID="insertddlDefect" datasourceid="DefectSqlDataSource"
			        datatextfield="Name" 	DataValueField="Value"  
			        SelectedValue='<%# Bind("DefectVal") %>'  runat="server">
               </asp:DropDownList>
            </InsertItemTemplate>
         </asp:TemplateField>
 
          <asp:TemplateField HeaderText="Description">
            <ItemTemplate>
               <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:DropDownList ID="editddlDescription" datasourceid="DefectSubSqlDataSource"
			        datatextfield="Name" 	DataValueField="Value"  
			        SelectedValue='<%# Bind("DescriptionVal") %>'  runat="server">
               </asp:DropDownList>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:DropDownList ID="insertddlDescription" datasourceid="DefectSubSqlDataSource"
			        datatextfield="Name" 	DataValueField="Value"  
			        SelectedValue='<%# Bind("DescriptionVal") %>'  runat="server">
               </asp:DropDownList>
           </insertItemTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="SerialNum">
            <ItemTemplate>
               <asp:Label ID="lblSerialNum" runat="server" Text='<%# Eval("SerialNum") %>'></asp:Label>
            </ItemTemplate>
         </asp:TemplateField>

          <asp:TemplateField HeaderText="StorageLocation">
            <ItemTemplate>
               <asp:Label ID="lblStorageLocation" runat="server" Text='<%# Eval("StorageLocation") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtStorageLocation" runat="server" Text='<%# Bind("StorageLocation") %>' Width="40%" MaxLength="30"></asp:TextBox>
                <asp:RequiredFieldValidator ID="valRequireStorageLocation" runat="server" ControlToValidate="edittxtStorageLocation" SetFocusOnError="true" ValidationGroup="DefectAnalysis"
                  Text="The StorageLocation field is required." ToolTip="The StorageLocation field is required." Display="Dynamic"></asp:RequiredFieldValidator>
           </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtStorageLocation" runat="server" Text='<%# Bind("StorageLocation") %>' Width="40%" MaxLength="30"></asp:TextBox>
                <asp:RequiredFieldValidator ID="valRequireStorageLocation" runat="server" ControlToValidate="inserttxtStorageLocation" SetFocusOnError="true" ValidationGroup="DefectAnalysis"
                  Text="The StorageLocation field is required." ToolTip="The StroageLocation field is required." Display="Dynamic"></asp:RequiredFieldValidator>
           </InsertItemTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="Year">
            <ItemTemplate>
               <asp:Label ID="lblYear" runat="server" Text='<%# Eval("Year") %>'></asp:Label>
            </ItemTemplate>
         </asp:TemplateField>

        <asp:TemplateField HeaderText="CerNum">
            <ItemTemplate>
               <asp:Label ID="lblCerNum" runat="server" Text='<%# Eval("CerNum") %>'></asp:Label>
            </ItemTemplate>
         </asp:TemplateField>
         
         <asp:TemplateField HeaderText="ScopePicID">
            <ItemTemplate>
               <asp:Label ID="lblScopePicID" runat="server" Text='<%# Eval("ScopePicID") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtScopePicID" runat="server" Text='<%# Bind("ScopePicID") %>' Width="40%" MaxLength="30"></asp:TextBox>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtScopePicID" runat="server" Text='<%# Bind("ScopePicID") %>' Width="40%" MaxLength="30"></asp:TextBox>
            </InsertItemTemplate>
         </asp:TemplateField>
         
        </Fields>

    </asp:DetailsView>
    
    <asp:SqlDataSource 
    ID="sqlDefectsDetails"
    ConnectionString=""
    SelectCommand="
        SELECT ID, Date, IsMedical,
            SymptomVal,
            DefectVal,
            DescriptionVal, 
            Symptom=(select name from Symptoms where value = SymptomVal), 
			Defect=(select name from DefectCategory where value = DefectVal), 
            [Description]=(select name from DefectSubCategory where value = DescriptionVal),  
            SerialNum, StorageLocation, [Year], CerNum, ScopePicID
        FROM DefectAnalysis
        WHERE [Year]=@Year AND CerNum=@CerNum "
    UpdateCommand="
        UPDATE DefectAnalysis
        SET
            Date=@Date, 
            IsMedical=@IsMedical, 
            SymptomVal=@SymptomVal, 
            DefectVal=@DefectVal, 
            DescriptionVal=@DescriptionVal, 
            SerialNum=@SerialNum, 
            StorageLocation=@StorageLocation, 
            Year=@Year, 
            CerNum=@CerNum, 
            ScopePicID=@ScopePicID
        WHERE   ID=@ID"   
    InsertCommand="
        INSERT INTO DefectAnalysis
        (Date, IsMedical, SymptomVal, DefectVal, DescriptionVal, SerialNum, 
            StorageLocation, Year, CerNum, ScopePicID)
        VALUES
        (@Date, @IsMedical, @SymptomVal, @DefectVal, @DescriptionVal, @SerialNum, 
            @StorageLocation, @Year, @CerNum, @ScopePicID)"
   
    runat="server">
    <SelectParameters>
        <asp:Parameter Name="Year" Type="Int32" />
        <asp:Parameter Name="CerNum" Type="Int32" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="ID" Type="Int32" />
        <asp:Parameter Name="Date" Type="DateTime" />
        <asp:Parameter Name="IsMedical" Type="Boolean" />
        <asp:Parameter Name="SymptomVal" Type="Int32" />
        <asp:Parameter Name="DefectVal" Type="Int32" />
        <asp:Parameter Name="DescriptionVal" Type="Int32" />
        <asp:Parameter Name="SerialNum" Type="Int64" />
        <asp:Parameter Name="StorageLocation" Type="String" />
        <asp:Parameter Name="Year" Type="Int32" />
        <asp:Parameter Name="CerNum" Type="Int32" />
        <asp:Parameter Name="ScopePicID" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Name="Date" Type="DateTime" />
        <asp:Parameter Name="IsMedical" Type="Boolean" />
        <asp:Parameter Name="SymptomVal" Type="Int32" />
        <asp:Parameter Name="DefectVal" Type="Int32" />
        <asp:Parameter Name="DescriptionVal" Type="Int32" />
        <asp:Parameter Name="SerialNum" Type="Int64" />
        <asp:Parameter Name="StorageLocation" Type="String" />
        <asp:Parameter Name="Year" Type="Int32" />
        <asp:Parameter Name="CerNum" Type="Int32" />
        <asp:Parameter Name="ScopePicID" Type="Int32" />
    </InsertParameters>
         
</asp:SqlDataSource>

<asp:SqlDataSource ID="SymptomSqlDataSource"  ConnectionString=""
SelectCommand="SELECT Value,Name FROM Symptoms"  runat="server"/> 

<asp:SqlDataSource ID="DefectSqlDataSource"  ConnectionString=""
SelectCommand="SELECT Value,Name FROM DefectCategory"  runat="server"/> 

<asp:SqlDataSource ID="DefectSubSqlDataSource"  ConnectionString=""
SelectCommand="SELECT Value,Name FROM DefectSubCategory"  runat="server"/> 

    </asp:Panel>

</div>

<div class="omega eight columns"> 

        <strong>CER Returns Entry</strong>
        <asp:DetailsView 
        ID="dtlReturnEntry"
        SkinID="dtlsAdmin"
        AutoGenerateRows="false"
        DefaultMode="ReadOnly"
        AllowPaging="True"
        autogenerateinsertbutton="true"
        autogenerateeditbutton="true" 
        OnItemUpdating="dtlReturnEntry_Updating" 
        OnItemInserting="dtlReturnEntry_Inserting"
        datakeynames="ID"
        DataSourceID="sqlReturnDetails"
        runat="server">
    
    
    <FieldHeaderStyle />
        <Fields>

          <asp:TemplateField HeaderText="ID">
            <ItemTemplate>
               <asp:Label ID="lblID" Visible="true" Text='<%# Eval("ID") %>' runat="server"></asp:Label>
            </ItemTemplate>
         </asp:TemplateField>
                         
          <asp:TemplateField HeaderText="Date">
            <ItemTemplate>
               <asp:Label ID="lblDate" Visible="true" Text='<%# Eval("Date") %>' runat="server"></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtDate" runat="server" Text='<%# Bind("Date") %>' Width="45%" MaxLength="100"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequireDate" runat="server" ControlToValidate="edittxtDate" SetFocusOnError="true" ValidationGroup="ReturnEntry"
                  Text="The Date field is required." ToolTip="The Date field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:CustomValidator ID="valeditDate" runat="server" ControlToValidate="edittxtDate"  ValidationGroup="ReturnEntry"
                ErrorMessage="<br />The format of the Date is not valid." OnServerValidate="valeditDate_Returns_ServerValidate"></asp:CustomValidator>
           </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtDate" runat="server" OnLoad="inserttxtDate_Returns_OnLoad" Text='<%# Bind("Date") %>' Width="45%" MaxLength="100"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequireDate" runat="server" ControlToValidate="InserttxtDate" SetFocusOnError="true" ValidationGroup="ReturnEntry"
                  Text="The Date field is required." ToolTip="The Date field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:CustomValidator ID="valinsertDate" runat="server" ControlToValidate="inserttxtDate" ValidationGroup="ReturnEntry"
                ErrorMessage="<br />The format of the Date is not valid." OnServerValidate="valinsertDate_Returns_ServerValidate"></asp:CustomValidator>
            </InsertItemTemplate>
         </asp:TemplateField>
         
          <asp:TemplateField HeaderText="DateReceived">
            <ItemTemplate>
               <asp:Label ID="lblDateReceived" Visible="true" Text='<%# Eval("DateReceived") %>' runat="server"></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:TextBox ID="edittxtDateReceived" runat="server" Text='<%# Bind("DateReceived") %>' Width="40%" MaxLength="100"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequireDateReceived" runat="server" ControlToValidate="edittxtDateReceived" SetFocusOnError="true" ValidationGroup="ReturnEntry"
                  Text="The DateReceived field is required." ToolTip="The DateReceived field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:CustomValidator ID="valeditDateReceived" runat="server" ControlToValidate="edittxtDateReceived" ValidationGroup="ReturnEntry"
                ErrorMessage="<br />The format of the DateReceived is not valid." OnServerValidate="valeditDateReceived_Returns_ServerValidate"></asp:CustomValidator>
           </EditItemTemplate>
            <InsertItemTemplate>
               <asp:TextBox ID="inserttxtDateReceived" runat="server" OnLoad="inserttxtDateReceived_Returns_OnLoad" Text='<%# Bind("DateReceived") %>' Width="40%" MaxLength="100"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valRequireDateReceived" runat="server" ControlToValidate="InserttxtDateReceived" SetFocusOnError="true" ValidationGroup="ReturnEntry"
                  Text="The DateReceived field is required." ToolTip="The DateReceived field is required." Display="Dynamic"></asp:RequiredFieldValidator>
               <asp:CustomValidator ID="valinsertDateReceived" runat="server" ControlToValidate="inserttxtDateReceived" ValidationGroup="ReturnEntry"
                ErrorMessage="<br />The format of the DateReceived is not valid." OnServerValidate="valinsertDateReceived_Returns_ServerValidate"></asp:CustomValidator>
            </InsertItemTemplate>
         </asp:TemplateField>         

        <asp:TemplateField HeaderText="Reason">
            <ItemTemplate>
               <asp:Label ID="lblReason" runat="server" Text='<%# Eval("Reason") %>' ></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
              <asp:DropDownList id="editddlReason" datasourceid="ReasonSqlDataSource"
			        datatextfield="Name" 	DataValueField="Value"  
			        SelectedValue='<%# Bind("ReasonVal") %>'  runat="server"/>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:DropDownList ID="insertddlReason" datasourceid="ReasonSqlDataSource"
			        datatextfield="Name" 	DataValueField="Value"  
			        SelectedValue='<%# Bind("ReasonVal") %>' runat="server">
               </asp:DropDownList>
           </insertItemTemplate>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="Reason2">
            <ItemTemplate>
               <asp:Label ID="lblReason2" runat="server" Text='<%# Eval("Reason2") %>' ></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
               <asp:DropDownList ID="editddlReason2" datasourceid="Reason2SqlDataSource"
			        datatextfield="Name" 	DataValueField="Value"  
			        SelectedValue='<%# Bind("Reason2Val") %>'  runat="server">
               </asp:DropDownList>
            </EditItemTemplate>
            <InsertItemTemplate>
               <asp:DropDownList ID="insertddlReason2" datasourceid="Reason2SqlDataSource"
			        datatextfield="Name" 	DataValueField="Value"  
			        SelectedValue='<%# Bind("Reason2Val") %>'  runat="server">
               </asp:DropDownList>
            </InsertItemTemplate>
         </asp:TemplateField>
 
         <asp:TemplateField HeaderText="Year">
            <ItemTemplate>
               <asp:Label ID="lblYear" runat="server" Text='<%# Eval("Year") %>' Width="20%" ></asp:Label>
            </ItemTemplate>
         </asp:TemplateField>

        <asp:TemplateField HeaderText="CerNum">
            <ItemTemplate>
               <asp:Label ID="lblCerNum" runat="server" Text='<%# Eval("CerNum") %>' Width="20%" ></asp:Label>
            </ItemTemplate>
         </asp:TemplateField>
         
        </Fields>

    </asp:DetailsView>
    
    <asp:SqlDataSource 
    ID="sqlReturnDetails"
    ConnectionString=""
    SelectCommand="
        SELECT ID, Date, DateReceived, Year, CerNum, ReasonVal, Reason2Val, 
            Reason= (select name from returnreasons where value = ReasonVal), 
            Reason2= (select name from returnreasons2 where value = Reason2Val) 
        FROM ReturnAnalysis
        WHERE Year=@Year AND CerNum=@CerNum "
    UpdateCommand="
        UPDATE ReturnAnalysis
        SET
            Date=@Date,  
            DateReceived=@DateReceived,  
            Year=@Year,  
            CerNum=@CerNum,  
            ReasonVal=@ReasonVal,  
            Reason2Val=@Reason2Val 
        WHERE   ID=@ID"   
    InsertCommand="
        INSERT INTO ReturnAnalysis 
        (Date, DateReceived, Year, CerNum, ReasonVal, Reason2Val) 
        VALUES (@Date, @DateReceived, @Year, @CerNum, @ReasonVal, @Reason2Val) "
   
    runat="server">
    <SelectParameters>
        <asp:Parameter Name="Year" Type="Int32" />
        <asp:Parameter Name="CerNum" Type="Int32" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="ID" Type="Int32" />
        <asp:Parameter Name="Date" Type="DateTime" />
        <asp:Parameter Name="DateReceived" Type="DateTime" />
        <asp:Parameter Name="ReasonVal" Type="Int32" />
        <asp:Parameter Name="Reason2Val" Type="Int32" />
        <asp:Parameter Name="Year" Type="Int32" />
        <asp:Parameter Name="CerNum" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Name="Date" Type="DateTime" />
        <asp:Parameter Name="DateReceived" Type="DateTime" />
        <asp:Parameter Name="ReasonVal" Type="Int32" />
        <asp:Parameter Name="Reason2Val" Type="Int32" />
        <asp:Parameter Name="Year" Type="Int32" />
        <asp:Parameter Name="CerNum" Type="Int32" />
    </InsertParameters>
         
</asp:SqlDataSource>

<asp:SqlDataSource ID="ReasonSqlDataSource"  ConnectionString=""
SelectCommand="SELECT Value,Name FROM ReturnReasons"  runat="server"/> 

<asp:SqlDataSource ID="Reason2SqlDataSource"  ConnectionString=""
SelectCommand="SELECT Value,Name FROM ReturnReasons2"  runat="server"/>

</div>

</asp:Content>

