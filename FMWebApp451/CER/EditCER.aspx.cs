using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CER;
using adminCER;
using Certnet;
using FM2015.Helpers;

public partial class AddComplaint : AdminBasePage
{
  
    protected void Page_Load(object sender, EventArgs e)
    {
        string connCER = AdminCart.Config.ConnStrCER();
        SymptomSqlDataSource.ConnectionString = connCER;
        DefectSqlDataSource.ConnectionString = connCER;
        DefectSubSqlDataSource.ConnectionString = connCER;
        ReasonSqlDataSource.ConnectionString = connCER;
        Reason2SqlDataSource.ConnectionString = connCER;
        sqlDefectsDetails.ConnectionString = connCER;
        sqlReturnDetails.ConnectionString = connCER;

        if (!Page.IsPostBack)
        {
            CanUserProceed(new string[4] { "ADMIN", "CUSTOMERSERVICE", "ISQA", "DOCTOR" });

            bool IsAdmin = Convert.ToBoolean(Session["isAdmin"]);
            bool IsDoctor = Convert.ToBoolean(Session["isDoctor"]);
            bool IsQA = Convert.ToBoolean(Session["isQA"]);
            bool IsCS = Convert.ToBoolean(Session["isCS"]);

            //initialize Point of Purchase dropdown
            List<adminCER.CER_PointOfPurchase> popList = new List<CER_PointOfPurchase>();
            popList = adminCER.CER_PointOfPurchase.GetCER_PointOfPurchaseList();
            selPointOfPurchase.DataSource = popList;
            selPointOfPurchase.DataTextField = "popName";
            selPointOfPurchase.DataValueField = "popValue";
            selPointOfPurchase.DataBind();

            //when cs, disable allow select death /serious injury, ect...
            if (!IsAdmin) radStatusMed.AutoPostBack = false;
            if (IsAdmin) pnlCloseActionShortCuts.Visible = true;


            //on edit dont allow edit complaint/no complaint
            if ( (Request["CERNumber"] != null) && (!IsAdmin) )
            {
                radioCerTypeC.Enabled = false;
                radioCerTypeNC.Enabled = false;
            }

            RelatedCER();

            if (Request["print"] != null)
            {
                lblEventDescription.Visible = true;
                txtEventDescription.Visible = false;
            }
            else
            {
                lblEventDescription.Visible = false;
                txtEventDescription.Visible = true;
            }

            RelatedCERNumber_Year.Text = DateTime.Now.Year.ToString();

            if (IsDoctor) { radStatusMed.Items.FindByValue("Closed").Enabled = true; }

            if (IsAdmin || IsQA)
            {
                radStatusCs.Items.FindByValue("Opened").Enabled = true;
                radStatusCs.Items.FindByValue("Closed").Enabled = true;
                radStatusQa.Items.FindByValue("Unopened").Enabled = true;
                radStatusQa.Items.FindByValue("Opened").Enabled = true;
                radStatusQa.Items.FindByValue("Closed").Enabled = true;
                radStatusMed.Items.FindByValue("Does Not Apply").Enabled = true;
                radStatusMed.Items.FindByValue("Assigned").Enabled = true;
                radStatusMed.Items.FindByValue("Closed").Enabled = true;

                radListMDR.Enabled = true;
                radListMDR.Items.FindByValue("Yes").Enabled = true;
                radListMDR.Items.FindByValue("No").Enabled = true;

                radListAnalyze.Enabled = true;
                radListAnalyze.Items.FindByValue("Yes").Enabled = true;
                radListAnalyze.Items.FindByValue("No").Enabled = true;

                lnkPrint.Visible = true;
                string CERNumber = Request["CERNumber"].ToString();
                lnkPrint.NavigateUrl = "EditCER.aspx?CERNumber=" + CERNumber + "&pageaction=lookup&print=1";
                lnkLookUp.Visible = true;
                lnkLookUp.NavigateUrl = "CERLookup.aspx";
                lnkUploadPics.Visible = true;
                lnkViewPics.Visible = true;
            }

            txtDate.Text = DateTime.Now.ToShortDateString();
            txtTime.Text = DateTime.Now.ToShortTimeString();

            if (Request["pageaction"] != null) { if (Request["pageaction"] == "lookup") { LookupCER(); } }
            else if (Request["CERNumber"] != null) { LookupCER(); }
        }
    }

    protected void btnDetailsUpdate_Click(object sender, EventArgs e)
    {
        string CERNumber = Request["CERNumber"].ToString();
        adminCER.CER obj = new adminCER.CER(CERNumber);
        int customerID = Convert.ToInt32(obj.CustomerID);
        string url = "~/cer/editCER.aspx?CERNumber=" + CERNumber;
        string redirect = "~/admin/EditUser.aspx?CustomerID=" + customerID.ToString() + "&ReturnURL=" + url;

        Response.Redirect(redirect);
    }

    protected void BindRelatedCERS(string _CERYear, int _CERIdentifier)
    {
        literalRelatedCERList.Text = "";
        string CERNumber = "";
        if (Request["CERNumber"] != null)
        { CERNumber = Request["CERNumber"]; }
        else
        { CERNumber = Session["lblCerNumber_Year"].ToString() + Convert.ToInt32(Session["lblCerNumber_Sequence"]).ToString("000000"); }

        foreach (adminCER.CER c in adminCER.CER.GetRelatedCERs(_CERYear, _CERIdentifier))
        {
            string cNum = c.CERNumber;
            if (c.StatusMed != "Does Not Apply") 
            { cNum = "<span style='color:red'>" + cNum + "-MedC </span>"; }
            else if (c.CERType == "C") { cNum = "<span style='color:red'>" + cNum + "-C </span>"; }
            literalRelatedCERList.Text += "<a href=EditCER.aspx?CERNumber=" + c.CERNumber + "&pageaction=lookup>" + cNum + "</a> " +
                "(<a href=EditCER.aspx?CERNumber=" + CERNumber + "&RelatedCER=" + c.CERNumber + "&pageaction=lookup>del</a>) - ";
        }
        //literalRelatedCERList.Text = "";
    }

    protected void BindOrders(string orderid)
    {
    }

    protected void ButtonUpdate_Click(object sender, EventArgs e)
    {
        UpdateCER();
    }

    protected void lnkBtnReviewed_Click(object sender, EventArgs e)
    {
        radioCerTypeNC.Checked = true;
        radioCerTypeC.Checked = false;
        txtActionExplaination.Text = "Reviewed & Closed";
        radStatusCs.SelectedValue = "Closed";
        radStatusQa.SelectedValue = "Closed";
        radStatusMed.SelectedValue = "Does Not Apply";
        radCondition.SelectedValue = "n/a";
        radioCerTypeC.Checked = false;
        UpdateCER();
        Response.Redirect("CERLookup.aspx"); 
    }

    protected void lnkBtnDNR_Click(object sender, EventArgs e)
    {
        txtMessage.Text += " Customer did not return device. Will close DNR.";
        AddMessage();

        txtActionExplaination.Text = "closed - DNR";
        radStatusCs.SelectedValue = "Closed";
        radStatusQa.SelectedValue = "Closed";

        if (radStatusMed.SelectedValue == "Assigned") { radStatusMed.SelectedValue = "Closed"; }
        else { radStatusMed.SelectedValue = "Does Not Apply"; }

        radCondition.SelectedValue = "n/a";
        //radioCerTypeC.Checked = false;
        UpdateCER();

        Response.Redirect("CERLookup.aspx");
    }

    protected void lnkBtnNTF_Click(object sender, EventArgs e)
    {
        txtMessage.Text += " Device was No Trouble Found. Will close NTF.";
        AddMessage();

        txtActionExplaination.Text = "closed - NTF";
        radStatusCs.SelectedValue = "Closed";
        radStatusQa.SelectedValue = "Closed";

        if (radStatusMed.SelectedValue == "Assigned") { radStatusMed.SelectedValue = "Closed"; }
        else { radStatusMed.SelectedValue = "Does Not Apply"; }

        radCondition.SelectedValue = "n/a";
        UpdateCER();

        Response.Redirect("CERLookup.aspx");
    }

    protected void lnkBtnComplaint_Click(object sender, EventArgs e)
    {
        radioCerTypeC.Checked = true;
        ListItem itemAnalyze_Yes = radListAnalyze.Items.FindByValue("Yes");
        if (itemAnalyze_Yes != null) { itemAnalyze_Yes.Selected = true; }

        radioCerTypeNC.Checked = false;
        txtActionExplaination.Text = "QA To Investigate; non-Medical";
        radStatusCs.SelectedValue = "Opened";
        radStatusQa.SelectedValue = "Opened";
        radStatusMed.SelectedValue = "Does Not Apply";
        radCondition.SelectedValue = "n/a";
        UpdateCER();
        Response.Redirect("CERLookup.aspx");
    }

    protected void lnkBtnMedComplaint_Click(object sender, EventArgs e)
    {
        radioCerTypeC.Checked = true;
        ListItem itemAnalyze_Yes = radListAnalyze.Items.FindByValue("Yes");
        if (itemAnalyze_Yes != null) { itemAnalyze_Yes.Selected = true; }

        radioCerTypeNC.Checked = false;
        txtActionExplaination.Text = "QA To Investigate; Medical";
        radStatusCs.SelectedValue = "Opened";
        radStatusQa.SelectedValue = "Opened";
        radStatusMed.SelectedValue = "Assigned";
        radCondition.SelectedValue = "n/a";
        UpdateCER();
        Response.Redirect("CERLookup.aspx");
    }


    private void UpdateCER()
    {
        adminCER.CER obj = new adminCER.CER();
        obj.CERYear = Session["lblCerNumber_Year"].ToString();
        //obj.CERPointOfPurchase = TextCerNumber_Source.Text;
        obj.CERIdentifier = Convert.ToInt32(Session["lblCerNumber_Sequence"]);

        if (Int32.TryParse(lblCustomerID.Text.Trim(), out int id)) { obj.CustomerID = id.ToString(); }
        else { obj.CustomerID = "0"; }

        obj.DateReportReceived = DateTime.Parse(TxtDateReceived.Text);
        obj.PointOfPurchase = selPointOfPurchase.SelectedValue;        
        obj.FirstName = ValidateUtil.CleanUpString(txtFirstName.Text);
        obj.LastName = ValidateUtil.CleanUpString(txtLastName.Text);
        obj.Email = ValidateUtil.CleanUpString(txtEmail.Text);
        obj.Phone = ValidateUtil.CleanUpString(txtPhone.Text);
        obj.Street = ValidateUtil.CleanUpString(txtStreet.Text);
        obj.City = ValidateUtil.CleanUpString(txtCity.Text);
        obj.State = ValidateUtil.CleanUpString(txtState.Text);
        obj.Zip = ValidateUtil.CleanUpString(txtZip.Text);
        obj.Country = txtCountry.Text;
        obj.ProductNumber = selProductNumber.SelectedValue;
        obj.SerialNumber = ValidateUtil.CleanUpString(txtSerialNumber.Text);
        obj.OrderNumber = ValidateUtil.CleanUpString(txtOrderNumber.Text);
        obj.EventDescription = ValidateUtil.CleanUpString(txtEventDescription.Text);
        obj.ActionExplaination = ValidateUtil.CleanUpString(txtActionExplaination.Text);
        obj.StatusCs = radStatusCs.SelectedValue;
        obj.StatusQa = radStatusQa.SelectedValue;
        obj.StatusMed = radStatusMed.SelectedValue;
        obj.MedicalCondition = radCondition.SelectedValue;
        obj.InsertBy = Session["UserLastName"].ToString() + ", " + Session["UserFirstName"].ToString();

        obj.CERType = "NC"; //default is not a complaint
        //if ((obj.StatusQa == "C-Opened") || (radioCerTypeC.Checked))
        if (radioCerTypeC.Checked)
        {
            obj.CERType = "C";
            obj.ComplaintNumber = GetNewComplaintNumber();
        }

        obj.Mdr = false; //default is no MDR report
        ListItem Mdr_Yes = radListMDR.Items.FindByValue("Yes");
        if (Mdr_Yes != null) { if (Mdr_Yes.Selected) { obj.Mdr = true; } }

        obj.Analyze = false; //default is no MDR report
        ListItem Analyze_Yes = radListAnalyze.Items.FindByValue("Yes");
        if (Analyze_Yes != null) { if (Analyze_Yes.Selected) { obj.Analyze = true; } }

        obj.ComplaintDeterminationBy = literalComplaintDeterminationBy.Text;
        obj.ReceivedBy = literalReceivedBy.Text;

        if ((obj.ComplaintDeterminationBy == "") && (Convert.ToBoolean(Session["IsQA"])) || Convert.ToBoolean(Session["IsAdmin"]))
        {
            obj.ComplaintDeterminationBy = Session["UserLastName"].ToString() + ", " + Session["UserFirstName"].ToString();
            literalComplaintDeterminationBy.Text = obj.ComplaintDeterminationBy;
        }

        //if QA + select close QA, close auto CS and MED
        if ((Convert.ToBoolean(Session["IsAdmin"])) && (radStatusQa.SelectedValue == "Closed"))
        {
            obj.StatusCs = "Closed";
            radStatusCs.SelectedValue = "Closed";
            if ((radStatusMed.SelectedValue == "Assigned") || (radStatusMed.SelectedValue == "Closed"))
            { 
                radStatusMed.SelectedValue = "Closed";
                obj.StatusMed = "Closed";
            }
            else
            {
                radStatusMed.SelectedValue = "Does Not Apply";
                obj.StatusMed = "Does Not Apply";
            }
        }
        obj.Update();

        //add new note!!!
        //obj.SaveNote(txtNote.Text);
        
        //Response.Redirect("CERLookUp.aspx");
        PageMessage.Text = "&nbsp;&nbsp;&nbsp;The CER has been updated.";
        BindHistory();
        BindMessages();
        BindOrderNotes();
        //BindNotes();
        txtActionExplaination.Text = "";
        //Response.Redirect("EditCER.aspx"); 

        //if complaint + medical and new record, email asap!
        //&& Session["UserRights"].ToString() == "IsQA"
        if (radioCerTypeC.Checked && obj.StatusMed == "Assigned")
        {
            //Mail.SendMail("customerservice@facemaster.com", Config.MedicalMailAddress, "FaceMaster CER - Medical Complaint Autonotification", "Check CER #:" + obj.CERYear + "-" + obj.CERIdentifier);
        }
    }

    protected void ButtonAddMessage_Click(object sender, EventArgs e)
    {
        AddMessage();
    }

    protected void AddMessage()
    {

        string CERNumber = "";
        if (Request["CERNumber"] != null)
        { CERNumber = Request["CERNumber"]; }
        else
        { CERNumber = Session["lblCerNumber_Year"].ToString() + Convert.ToInt32(Session["lblCerNumber_Sequence"]).ToString("000000"); }

        adminCER.CER c1 = new adminCER.CER(CERNumber);

        string sql = @"
                    INSERT INTO MessagesCER
                    SELECT @CERYear, @CERIdentifier, @Dated, @Type, @Message, @AddDate, @AddBy
            ";

        SqlParameter[] NamedParameters = new SqlParameter[7];

        NamedParameters[0] = new SqlParameter("@CERYear", c1.CERYear);
        NamedParameters[1] = new SqlParameter("@CERIdentifier", c1.CERIdentifier);
        NamedParameters[2] = new SqlParameter("@Dated", txtDate.Text + " " + txtTime.Text);
        NamedParameters[3] = new SqlParameter("@Type", selType.SelectedValue);
        NamedParameters[4] = new SqlParameter("@Message", txtMessage.Text);
        NamedParameters[5] = new SqlParameter("@AddDate", DateTime.Now);
        NamedParameters[6] = new SqlParameter("@AddBy", Session["UserLastName"].ToString());

        DBUtil.Exec(sql, AdminCart.Config.ConnStrCER(), NamedParameters);

        BindMessages();
    }

    protected void BindHistory()
    {
        string sql = @"
        SELECT InsertDate, InsertBy, HistoryId, 
        CASE WHEN [CERType] = 'C' THEN 'Complaint' ELSE 'No Complaint' END CERTypes,
        [EventDescription], ActionExplaination, StatusCs, StatusQa, StatusMed
        FROM Cer2History
        WHERE CERYear = '" + Session["lblCerNumber_Year"].ToString() + @"' 
        AND CERIdentifier = '" + Session["lblCerNumber_Sequence"].ToString() + @"' 
        ORDER BY InsertDate DESC";

        gridHistory.DataSource = DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER());
        gridHistory.DataBind();
    }

    protected void BindMessages()
    {
        string sql = @"
        SELECT MessageId, CERYear, CERIdentifier, Dated, Type, Message, AddDate, AddBy
        FROM MessagesCER
        WHERE CERYear = '" + Session["lblCerNumber_Year"].ToString() + @"' 
        AND CERIdentifier = '" + Session["lblCerNumber_Sequence"].ToString() + @"' 
        ORDER BY Dated DESC";

        gridMessages.DataSource = DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER());
        gridMessages.DataBind();
    }

    protected void BindOrderNotes()
    {
        string sql = @"
            SELECT *
            FROM OrderNotes 
            WHERE OrderID = @OrderID 
            ORDER BY OrderNoteDate DESC 
        ";

        string orderID = "-1"; //assume a bogus orderID to get a null set back from query if txtOrderNumber is bogus or null
        if (ValidateUtil.IsNumeric(txtOrderNumber.Text)) 
        { 
            orderID = txtOrderNumber.Text;
            lnkReviewOrder.NavigateUrl += txtOrderNumber.Text;
            lnkReviewOrder.Visible = true;
        }

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID);
        DataSet ds = FM2015.Helpers.DBUtil.FillDataSet(sql, "FMOrderNotes", mySqlParameters, AdminCart.Config.ConnStr()); //pull from FM order DB

        GridFMOrderNotes.DataSource = ds.Tables["FMOrderNotes"];
        GridFMOrderNotes.DataBind();
    }

    protected int GetNewComplaintNumber()
    {
        //todo ... this is trouble if two people open a new one at once
        int max = 0;
        string sql = "SELECT MAX(ComplaintNumber) FROM Cer2s";
        SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER());
        while (dr.Read())
        {
            if (!dr.IsDBNull(0))
                max = dr.GetInt32(0);
        }
        dr.Close();
        return max += 1;
    }

    protected void radStatusCs_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ValidateUtil.IsInRole(cart.SiteCustomer.CustomerID, "ADMIN")) radStatusMed.ClearSelection();
    }

    protected void radStatusQa_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void radStatusMed_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ValidateUtil.IsInRole(cart.SiteCustomer.CustomerID, "ADMIN")) radStatusCs.ClearSelection();

        if (radStatusMed.SelectedValue == "Assigned")
        {
            lblCondition.Visible = true;
            radCondition.Visible = true;
            radCondition.ClearSelection();

            DisplayMedicalCondition();
        }
    }

    protected void DisplayMedicalCondition()
    {
        if ((Convert.ToBoolean(Session["IsAdmin"])) && (radStatusMed.SelectedValue != ""))
        {
            lblCondition.Visible = true;
            radCondition.Visible = true;
            radCondition.Items.FindByValue("Serious").Enabled = true;
            radCondition.Items.FindByValue("Death").Enabled = true;
            radCondition.Items.FindByValue("n/a").Enabled = true;
            radCondition.Items.FindByValue("Temp Injury").Enabled = true;
        }
        else
        {
            lblCondition.Visible = false;
            radCondition.Visible = false;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (DBUtil.IsNumeric(RelatedCERNumber_Year.Text) && DBUtil.IsNumeric(RelatedCERNumber_Sequence.Text))
        {
            if (1 == 1)
            {
                string CERNumber = "";
                if (Request["CERNumber"] != null)
                    CERNumber = Request["CERNumber"];
                //no..this sets the cer to ref itself
                //else
                //    CERNumber = textCerNumber_Year.Text + Convert.ToInt32(TextCerNumber_Sequence.Text).ToString("000000");

                SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrCER());

                string sql = @"
                DECLARE @CERyearTemp char(4), @CERIdentifierTemp int
                SET @CERyearTemp = dbo.fn_GetParentCERyear (@CERyear, @CERIdentifier)
                SET @CERIdentifierTemp = dbo.fn_GetParentCERIdentifier (@CERyear, @CERIdentifier)

                IF NOT EXISTS (
                    SELECT * FROM RelatedCERs
                    WHERE CERYear = @CERyearTemp 
                    AND CERIdentifier = @CERIdentifierTemp
                    AND RelatedCERYear = @RelatedCERYear
                    AND RelatedCERIdentifier = @RelatedCERIdentifier
                )
                BEGIN
                    INSERT INTO RelatedCERs
                    SELECT @CERyearTemp, @CERIdentifierTemp, @RelatedCERYear, @RelatedCERIdentifier
                END
            ";
                //todo...convert to using dbutil exec with parameter collection.  see above for example.

                SqlCommand cmd = new SqlCommand(sql, conn);

                SqlParameter CERYearParam = new SqlParameter("@CERYear", SqlDbType.VarChar);
                SqlParameter CERIdentifierParam = new SqlParameter("@CERIdentifier", SqlDbType.Int);
                SqlParameter RelatedCERYearParam = new SqlParameter("@RelatedCERYear", SqlDbType.VarChar);
                SqlParameter RelatedCERIdentifierParam = new SqlParameter("@RelatedCERIdentifier", SqlDbType.Int);

                adminCER.CER c1 = new adminCER.CER(CERNumber);
                int seq = Convert.ToInt32(RelatedCERNumber_Sequence.Text);
                adminCER.CER c2 = new adminCER.CER(RelatedCERNumber_Year.Text + seq.ToString("000000"));

                CERYearParam.Value = c1.CERYear;
                CERIdentifierParam.Value = c1.CERIdentifier;
                RelatedCERYearParam.Value = c2.CERYear;
                RelatedCERIdentifierParam.Value = c2.CERIdentifier;

                cmd.Parameters.Add(CERYearParam);
                cmd.Parameters.Add(CERIdentifierParam);
                cmd.Parameters.Add(RelatedCERYearParam);
                cmd.Parameters.Add(RelatedCERIdentifierParam);

                if (c1.CERIdentifier == c2.CERIdentifier)
                {
                    PageMessage.Text = "No action taken.  This would have resulted in a cer related to itself!";
                }
                else
                {
                    conn.Open();
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        StringBuilder errorMessages = new StringBuilder();
                        for (int i = 0; i < ex.Errors.Count; i++)
                        {
                            errorMessages.Append(ex.Errors[i].Message + "<br>");
                        }
                        PageMessage.Text = errorMessages.ToString(); //"Invalid CER No."; // 
                    }
                    conn.Close();

                    BindRelatedCERS(c1.CERYear, c1.CERIdentifier);
                }
            }

            string CERnumber = Request["CERNumber"].ToString();
            Response.Redirect("EditCER.aspx?CERNumber=" + CERnumber + "&pageaction=lookup");
        }
        else
        {
            PageMessage.Text = "To add a related CER, the data must be numeric.";
        }
    }
    protected void changeLinkButton_OnCommand(object source, CommandEventArgs e)
    {
          
        if (e.CommandArgument.ToString() == "upload")
Response.Write(@"
<script type='text/javascript'>
    window.open('UploadImage.aspx?cer=" + Session["lblCerNumber_Year"].ToString() + "&seq=" + Session["lblCerNumber_Sequence"].ToString() + "','Upload','width=300,height=200,scrollbars=0,toolbars=0');</script>"
);

        else if (e.CommandArgument.ToString() == "view")
    Response.Write(@"
<script type='text/javascript'>
    window.open('ShowImages.aspx?cer=" + Session["lblCerNumber_Year"].ToString() + "&seq=" + Session["lblCerNumber_Sequence"].ToString() + "','View','width=800,height=800,scrollbars=1,toolbars=1');</script>"
    );
    }

    protected void dtlDefectAnalysis_Updating(object sender, EventArgs e)
    {
        //DetailsView dtlDefectAnalysis = (DetailsView)pnlDefectAnalysis.FindControl("dtlDefectAnalysis") as DetailsView;
        if (dtlDefectAnalysis != null)
        {
            //sqlDefectsDetails.UpdateParameters["ID"].DefaultValue = ddlS.SelectedItem.Text;
            Label lblYear = dtlDefectAnalysis.FindControl("lblYear") as Label;
            if (lblYear != null) { sqlDefectsDetails.UpdateParameters["Year"].DefaultValue = lblYear.Text; }
            else { sqlDefectsDetails.UpdateParameters["Year"].DefaultValue = "1900"; }

            Label lblCerNum = dtlDefectAnalysis.FindControl("lblCerNum") as Label;
            if (lblCerNum != null) { sqlDefectsDetails.UpdateParameters["CerNum"].DefaultValue = lblCerNum.Text; }
            else { sqlDefectsDetails.UpdateParameters["CerNum"].DefaultValue = "0"; }

            CheckBox IsMedical = dtlDefectAnalysis.FindControl("editrdbtnIsMedical") as CheckBox;
            if (IsMedical != null) { sqlDefectsDetails.UpdateParameters["IsMedical"].DefaultValue = IsMedical.Checked.ToString(); }
            else { sqlDefectsDetails.UpdateParameters["IsMedical"].DefaultValue = "false"; }

            DropDownList Symptom = dtlDefectAnalysis.FindControl("editddlSymptom") as DropDownList;
            if (Symptom != null) { sqlDefectsDetails.UpdateParameters["SymptomVal"].DefaultValue = Symptom.SelectedValue.ToString(); }
            else { sqlDefectsDetails.UpdateParameters["SymptomVal"].DefaultValue = "0"; }

            DropDownList Defect = dtlDefectAnalysis.FindControl("editddlDefect") as DropDownList;
            if (Defect != null) { sqlDefectsDetails.UpdateParameters["DefectVal"].DefaultValue = Defect.SelectedValue.ToString(); }
            else { sqlDefectsDetails.UpdateParameters["DefectVal"].DefaultValue = "0"; }

            DropDownList Description = dtlDefectAnalysis.FindControl("editddlDescription") as DropDownList;
            if (Description != null) { sqlDefectsDetails.UpdateParameters["DescriptionVal"].DefaultValue = Description.SelectedValue.ToString(); }
            else { sqlDefectsDetails.UpdateParameters["DescriptionVal"].DefaultValue = "0"; }

            Label SerialNum = dtlDefectAnalysis.FindControl("lblSerialNum") as Label;
            if (SerialNum != null) { sqlDefectsDetails.UpdateParameters["SerialNum"].DefaultValue = SerialNum.Text; }
            else { sqlDefectsDetails.UpdateParameters["SerialNum"].DefaultValue = "-1"; }

            TextBox StorageLocation = dtlDefectAnalysis.FindControl("edittxtStorageLocation") as TextBox;
            if (StorageLocation != null) { sqlDefectsDetails.UpdateParameters["StorageLocation"].DefaultValue = ValidateUtil.CleanUpString(StorageLocation.Text); }
            else { sqlDefectsDetails.UpdateParameters["StorageLocation"].DefaultValue = "N/A"; }

            TextBox ScopePicID = dtlDefectAnalysis.FindControl("edittxtScopePicID") as TextBox;
            if (ScopePicID != null) { sqlDefectsDetails.UpdateParameters["ScopePicID"].DefaultValue = ValidateUtil.CleanUpString(ScopePicID.Text); }
            else { sqlDefectsDetails.UpdateParameters["ScopePicID"].DefaultValue = "0"; }
        }
    }

    protected void dtlDefectAnalysis_Inserting(object sender, DetailsViewInsertEventArgs e)
    {
        //DetailsView dtlDefectAnalysis = (DetailsView)pnlDefectAnalysis.FindControl("dtlDefectAnalysis") as DetailsView;
        if (dtlDefectAnalysis != null)
        {
            Label lblYear = dtlDefectAnalysis.FindControl("lblYear") as Label;
            if (lblYear != null) { sqlDefectsDetails.InsertParameters["Year"].DefaultValue = lblYear.Text; }
            else { sqlDefectsDetails.InsertParameters["Year"].DefaultValue = "1900"; }

            Label lblCerNum = dtlDefectAnalysis.FindControl("lblCerNum") as Label;
            if (lblCerNum != null) { sqlDefectsDetails.InsertParameters["CerNum"].DefaultValue = lblCerNum.Text; }
            else { sqlDefectsDetails.InsertParameters["CerNum"].DefaultValue = "0"; }

            CheckBox IsMedical = dtlDefectAnalysis.FindControl("insertrdbtnIsMedical") as CheckBox;
            if (IsMedical != null) { sqlDefectsDetails.InsertParameters["IsMedical"].DefaultValue = IsMedical.Checked.ToString(); }
            else { sqlDefectsDetails.InsertParameters["IsMedical"].DefaultValue = "false"; }

            DropDownList Symptom = dtlDefectAnalysis.FindControl("insertddlSymptom") as DropDownList;
            if (Symptom != null) { sqlDefectsDetails.InsertParameters["SymptomVal"].DefaultValue = Symptom.SelectedValue.ToString(); }
            else { sqlDefectsDetails.InsertParameters["SymptomVal"].DefaultValue = "0"; }

            DropDownList Defect = dtlDefectAnalysis.FindControl("insertddlDefect") as DropDownList;
            if (Defect != null) { sqlDefectsDetails.InsertParameters["DefectVal"].DefaultValue = Defect.SelectedValue.ToString(); }
            else { sqlDefectsDetails.InsertParameters["DefectVal"].DefaultValue = "0"; }

            DropDownList Description = dtlDefectAnalysis.FindControl("insertddlDescription") as DropDownList;
            if (Description != null) { sqlDefectsDetails.InsertParameters["DescriptionVal"].DefaultValue = Description.SelectedValue.ToString(); }
            else { sqlDefectsDetails.InsertParameters["DescriptionVal"].DefaultValue = "0"; }

            Label SerialNum = dtlDefectAnalysis.FindControl("lblSerialNum") as Label;
            if (SerialNum != null) { sqlDefectsDetails.InsertParameters["SerialNum"].DefaultValue = SerialNum.Text; }
            else { sqlDefectsDetails.InsertParameters["SerialNum"].DefaultValue = "-1"; }

            TextBox StorageLocation = dtlDefectAnalysis.FindControl("inserttxtStorageLocation") as TextBox;
            if (StorageLocation != null) { sqlDefectsDetails.InsertParameters["StorageLocation"].DefaultValue = ValidateUtil.CleanUpString(StorageLocation.Text); }
            else { sqlDefectsDetails.InsertParameters["StorageLocation"].DefaultValue = "N/A"; }

            TextBox ScopePicID = dtlDefectAnalysis.FindControl("inserttxtScopePicID") as TextBox;
            if (ScopePicID != null) { sqlDefectsDetails.InsertParameters["ScopePicID"].DefaultValue = ValidateUtil.CleanUpString(ScopePicID.Text); }
            else { sqlDefectsDetails.InsertParameters["ScopePicID"].DefaultValue = "0"; }
        }
    }

    protected void dtlReturnEntry_Updating(object sender, EventArgs e)
    {
        //DetailsView dtlReturnEntry = (DetailsView)pnlReturnEntry.FindControl("dtlReturnEntry") as DetailsView;
        if (dtlReturnEntry != null)
        {
            //sqlDefectsDetails.UpdateParameters["ID"].DefaultValue = ddlS.SelectedItem.Text;
            Label lblYear = dtlReturnEntry.FindControl("lblYear") as Label;
            if (lblYear != null) { sqlReturnDetails.UpdateParameters["Year"].DefaultValue = lblYear.Text; }
            else { sqlReturnDetails.UpdateParameters["Year"].DefaultValue = "1900"; }

            Label lblCerNum = dtlReturnEntry.FindControl("lblCerNum") as Label;
            if (lblCerNum != null) { sqlReturnDetails.UpdateParameters["CerNum"].DefaultValue = lblCerNum.Text; }
            else { sqlReturnDetails.UpdateParameters["CerNum"].DefaultValue = "0"; }

            DropDownList Reason = dtlReturnEntry.FindControl("editddlReason") as DropDownList;
            if (Reason != null) { sqlReturnDetails.UpdateParameters["ReasonVal"].DefaultValue = Reason.SelectedValue.ToString(); }
            else { sqlReturnDetails.UpdateParameters["ReasonVal"].DefaultValue = "0"; }

            DropDownList Reason2 = dtlReturnEntry.FindControl("editddlReason2") as DropDownList;
            if (Reason2 != null) { sqlReturnDetails.UpdateParameters["Reason2Val"].DefaultValue = Reason2.SelectedValue.ToString(); }
            else { sqlReturnDetails.UpdateParameters["Reason2Val"].DefaultValue = "0"; }
        }
    }

    protected void dtlReturnEntry_Inserting(object sender, DetailsViewInsertEventArgs e)
    {
        //DetailsView dtlReturnEntry = (DetailsView)pnlReturnEntry.FindControl("dtlReturnEntry") as DetailsView;
        if (dtlReturnEntry != null)
        {
            Label lblYear = dtlReturnEntry.FindControl("lblYear") as Label;
            if (lblYear != null) { sqlReturnDetails.InsertParameters["Year"].DefaultValue = lblYear.Text; }
            else { sqlReturnDetails.InsertParameters["Year"].DefaultValue = "1900"; }

            Label lblCerNum = dtlReturnEntry.FindControl("lblCerNum") as Label;
            if (lblCerNum != null) { sqlReturnDetails.InsertParameters["CerNum"].DefaultValue = lblCerNum.Text; }
            else { sqlReturnDetails.InsertParameters["CerNum"].DefaultValue = "0"; }

            DropDownList Reason = dtlReturnEntry.FindControl("insertddlReason") as DropDownList;
            if (Reason != null) { sqlReturnDetails.InsertParameters["ReasonVal"].DefaultValue = Reason.SelectedValue.ToString(); }
            else { sqlReturnDetails.InsertParameters["ReasonVal"].DefaultValue = "0"; }

            DropDownList Reason2 = dtlReturnEntry.FindControl("insertddlReason2") as DropDownList;
            if (Reason2 != null) { sqlReturnDetails.InsertParameters["Reason2Val"].DefaultValue = Reason2.SelectedValue.ToString(); }
            else { sqlReturnDetails.InsertParameters["Reason2Val"].DefaultValue = "0"; }
        }
    }


    protected void valeditDate_ServerValidate(object source, ServerValidateEventArgs args)
    {
        //DetailsView dtlDefectAnalysis = (DetailsView)pnlDefectAnalysis.FindControl("dtlDefectAnalysis") as DetailsView;
        if (dtlDefectAnalysis != null)
        {
            TextBox t = dtlDefectAnalysis.FindControl("edittxtDate") as TextBox;
            args.IsValid = ValidateUtil.IsDate(t.Text);
        }
    }

    protected void valinsertDate_ServerValidate(object source, ServerValidateEventArgs args)
    {
        //DetailsView dtlDefectAnalysis = (DetailsView)pnlDefectAnalysis.FindControl("dtlDefectAnalysis") as DetailsView;
        if (dtlDefectAnalysis != null)
        {
            TextBox t = dtlDefectAnalysis.FindControl("inserttxtDate") as TextBox;
            args.IsValid = ValidateUtil.IsDate(t.Text);
        }
    }

    protected void inserttxtDate_OnLoad(object sender, EventArgs e)
    {
        //DetailsView dtlDefectAnalysis = (DetailsView)pnlDefectAnalysis.FindControl("dtlDefectAnalysis") as DetailsView;
        if (dtlDefectAnalysis != null)
        {
            TextBox t = dtlDefectAnalysis.FindControl("inserttxtDate") as TextBox;
            if ((t != null) && String.IsNullOrEmpty(t.Text)) t.Text = DateTime.Now.ToString();
        }
    }

    protected void valeditDate_Returns_ServerValidate(object source, ServerValidateEventArgs args)
    {
        //DetailsView dtlReturnEntry = (DetailsView)pnlReturnEntry.FindControl("dtlReturnEntry") as DetailsView;
        if (dtlReturnEntry != null)
        {
            TextBox t = dtlReturnEntry.FindControl("edittxtDate") as TextBox;
            args.IsValid = ValidateUtil.IsDate(t.Text);
        }
    }

    protected void valinsertDate_Returns_ServerValidate(object source, ServerValidateEventArgs args)
    {
        //DetailsView dtlReturnEntry = (DetailsView)pnlReturnEntry.FindControl("dtlReturnEntry") as DetailsView;
        if (dtlReturnEntry != null)
        {
            TextBox t = dtlReturnEntry.FindControl("inserttxtDate") as TextBox;
            args.IsValid = ValidateUtil.IsDate(t.Text);
        }
    }

    protected void inserttxtDate_Returns_OnLoad(object sender, EventArgs e)
    {
        //DetailsView dtlReturnEntry = (DetailsView)pnlReturnEntry.FindControl("dtlReturnEntry") as DetailsView;
        if (dtlReturnEntry != null)
        {
            TextBox t = dtlReturnEntry.FindControl("inserttxtDate") as TextBox;
            if ((t != null) && String.IsNullOrEmpty(t.Text)) t.Text = DateTime.Now.ToString();
        }
    }

    protected void valeditDateReceived_Returns_ServerValidate(object source, ServerValidateEventArgs args)
    {
        //DetailsView dtlReturnEntry = (DetailsView)pnlReturnEntry.FindControl("dtlReturnEntry") as DetailsView;
        if (dtlReturnEntry != null)
        {
            TextBox t = dtlReturnEntry.FindControl("edittxtDate") as TextBox;
            args.IsValid = ValidateUtil.IsDate(t.Text);
        }
    }

    protected void valinsertDateReceived_Returns_ServerValidate(object source, ServerValidateEventArgs args)
    {
        //DetailsView dtlReturnEntry = (DetailsView)pnlReturnEntry.FindControl("dtlReturnEntry") as DetailsView;
        if (dtlReturnEntry != null)
        {
            TextBox t = dtlReturnEntry.FindControl("inserttxtDate") as TextBox;
            args.IsValid = ValidateUtil.IsDate(t.Text);
        }
    }

    protected void inserttxtDateReceived_Returns_OnLoad(object sender, EventArgs e)
    {
        //DetailsView dtlReturnEntry = (DetailsView)pnlReturnEntry.FindControl("dtlReturnEntry") as DetailsView;
        if (dtlReturnEntry != null)
        {
            TextBox t = dtlReturnEntry.FindControl("inserttxtDate") as TextBox;
            if ((t != null) && String.IsNullOrEmpty(t.Text)) t.Text = DateTime.Now.ToString();
        }
    }

    static public string GetDefectReasonName(int value)
    {
        string sql = @"
            SELECT * 
            FROM ReturnReasons  
            WHERE Value = @Value  
            ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, value);
        SqlDataReader dr = DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrCER(), mySqlParameters);
        string name = "N/A";
        if (dr.Read()) { name = dr["Name"].ToString(); }
        if (dr != null) { dr.Close(); }
        return name;
    }

    protected void InitInsertMode()
    {
        sqlDefectsDetails.InsertParameters["Year"].DefaultValue = ValidateUtil.CleanUpString(Session["lblCerNumber_Year"].ToString());
        sqlDefectsDetails.InsertParameters["CerNum"].DefaultValue = ValidateUtil.CleanUpString(Session["lblCerNumber_Sequence"].ToString());
        sqlDefectsDetails.InsertParameters["SerialNum"].DefaultValue = "-1";

        //DetailsView dtlDefectAnalysis = this.Master.FindControl("MainContent").FindControl("dtlDefectAnalysis") as DetailsView;
        //DetailsView dtl = (DetailsView)FindControlRecursive(pnlDefectAnalysis, "dtlDefectAnalysis");

        DetailsView dtl = dtlDefectAnalysis;
        if (dtl.CurrentMode == DetailsViewMode.Insert)
        {

            Label lblYear = dtl.FindControl("lblYear") as Label; 
            //Label lblYear = (Label)FindControlRecursive(dtl, "lblYear");
            if (lblYear != null) { lblYear.Text = ValidateUtil.CleanUpString(Session["lblCerNumber_Year"].ToString()); }

            Label lblCerNum = dtl.FindControl("lblCerNum") as Label;
            if (lblCerNum != null) { lblCerNum.Text = ValidateUtil.CleanUpString(Session["lblCerNumber_Sequence"].ToString()); }

            RadioButton radBtnIsMedical = (RadioButton)dtl.FindControl("insertrdbtnIsMedical") as RadioButton;
            if ((radBtnIsMedical != null) && (radStatusMed.SelectedItem.ToString() == "Assigned")) { radBtnIsMedical.Checked = true; }

            Label lblSerialNum = dtl.FindControl("lblSerialNum") as Label;
            if (lblSerialNum != null)
            {
                if (String.IsNullOrEmpty(txtSerialNumber.Text)) { lblSerialNum.Text = "-1"; }
                else
                {

                    long ser = 0;
                    if (txtSerialNumber.Text.Length >= 11)
                    {
                        string testSer = (ValidateUtil.CleanUpString(txtSerialNumber.Text)).Substring(0, 11);
                        if (long.TryParse(testSer, out ser)) { lblSerialNum.Text = testSer; }
                        else { lblSerialNum.Text = "-1"; }
                    }
                    else { lblSerialNum.Text = "-1"; }
                }
                sqlDefectsDetails.InsertParameters["SerialNum"].DefaultValue = lblSerialNum.Text;
            }

            TextBox StorageLocation = (TextBox)dtl.FindControl("inserttxtStorageLocation") as TextBox;
            if (StorageLocation != null)
            {
                Defect defect = new Defect(Defect.GetLastDefectID());
                if (defect.Id > 0) { StorageLocation.Text = defect.StorageLocation; }
                else { StorageLocation.Text = "N/A"; }
            }

            TextBox ScopePicID = (TextBox)dtl.FindControl("inserttxtScopePicID") as TextBox;
            if (ScopePicID != null) { ScopePicID.Text = "0"; }
        }
    }

    protected void InitReturnInsertMode()
    {
        sqlReturnDetails.InsertParameters["Year"].DefaultValue = ValidateUtil.CleanUpString(Session["lblCerNumber_Year"].ToString());
        sqlReturnDetails.InsertParameters["CerNum"].DefaultValue = ValidateUtil.CleanUpString(Session["lblCerNumber_Sequence"].ToString());

        //DetailsView dtlReturnEntry = pnlReturnEntry.FindControl("dtlReturnEntry") as DetailsView;
        if (dtlReturnEntry != null)
        {
            Label lblYear = dtlReturnEntry.FindControl("lblYear") as Label;
            if (lblYear != null) { lblYear.Text = ValidateUtil.CleanUpString(Session["lblCerNumber_Year"].ToString()); }
            Label lblCerNum = dtlReturnEntry.FindControl("lblCerNum") as Label;
            if (lblCerNum != null) { lblCerNum.Text = ValidateUtil.CleanUpString(Session["lblCerNumber_Sequence"].ToString()); }

            TextBox txtDate = dtlReturnEntry.FindControl("inserttxtDate") as TextBox;
            if (txtDate != null) { txtDate.Text = DateTime.Now.ToString(); }

            TextBox txtDateReceived = dtlReturnEntry.FindControl("inserttxtDateReceived") as TextBox;
            if (txtDateReceived != null) { txtDateReceived.Text = DateTime.Now.ToString(); }
        }
    }

    protected void InitUpdateMode()
    {
    }

    protected void InitReadOnlyMode()
    {
    }

    protected string CreateNewCustomer(adminCER.CER obj)
    {
        AdminCart.Customer c = new AdminCart.Customer();
        AdminCart.Address a = new AdminCart.Address();

        string first = "";
        if (obj.FirstName == "") { first = "unknown"; }
        else { first = obj.FirstName; }

        string last = "";
        if (obj.LastName == "") { last = "unknown"; }
        else { last = obj.LastName; }

        string newEmail = "CER.";
        if (obj.Email == "")
        {
            string year = DateTime.Now.Year.ToString();
            string month = DateTime.Now.Month.ToString();
            string day = DateTime.Now.Day.ToString();
            Random r = new Random();
            newEmail += year + month + day + r.Next(100).ToString() + "@FaceMaster.com";
        }
        else { newEmail += obj.Email; }

        string password = "FM12345!";

        string phone = "";
        if (obj.Phone == "") { phone = "n/a"; }
        else { phone = obj.Phone; }

        string street = "";
        if (obj.Street == "") { street = "n/a"; }
        else { street = obj.Street; }

        string city = "";
        if (obj.City == "") { city = "n/a"; }
        else { city = obj.City; }

        string state = "";
        if (obj.State == "") { state = "CA"; }
        else { state = obj.State; }

        string country = "";
        if (obj.Country == "") { country = "US"; }
        else { state = obj.Country; }

        string zip = "";
        if (obj.Zip == "") { zip = "00000"; }
        else { zip = obj.Zip; }

        //first, see if the customer existed before IDs were included
        
        c = AdminCart.Customer.FindCustomer(obj.FirstName, obj.LastName, obj.Email, obj.Phone);
        if (c.CustomerID == 0) //then create a new customer
        {
            c.FirstName = first;
            c.LastName = last;
            c.Email = newEmail;
            c.Password = password;
            c.Phone = obj.Phone;
            a.Street = street;
            a.Street2 = "";
            a.City = city;
            a.State = state;
            a.Country = country;
            a.Zip = zip;


            int customerID = 0;
            int emailinc = 0;
            while (customerID <= 0)
            {
                customerID = AdminCart.Customer.EnterCustomer(c, a); //OK should be new customerID
                if (customerID > 0)
                {
                    //new customer entered OK
                    a.Save(c, 1);
                    a.Save(c, 2);
                    return customerID.ToString();
                }
                else
                {
                    c.Email = emailinc.ToString() + c.Email;
                    emailinc++;
                }
            }           
        }
        
        return c.CustomerID.ToString();
    }

    private void RelatedCER()
    {
        if (Request["RelatedCER"] != null)
        {
            string sql = @"
                    DECLARE @CERyearTemp char(4), @CERIdentifierTemp int
                    SET @CERyearTemp = dbo.fn_GetParentCERyear (@CERyear, @CERIdentifier)
                    SET @CERIdentifierTemp = dbo.fn_GetParentCERIdentifier (@CERyear, @CERIdentifier)

                    DELETE FROM RelatedCERs
                    WHERE CERYear = @CERyearTemp 
                    AND CERIdentifier = @CERIdentifierTemp
                    AND RelatedCERYear = @RelatedCERYear
                    AND RelatedCERIdentifier = @RelatedCERIdentifier
                ";

            adminCER.CER c1 = new adminCER.CER(Request["CERNumber"]);
            adminCER.CER c2 = new adminCER.CER(Request["RelatedCER"]);

            //populate parameters for queries
            SqlParameter[] NamedParameters = new SqlParameter[4];

            NamedParameters[0] = new SqlParameter("@CERYear", c1.CERYear);
            NamedParameters[1] = new SqlParameter("@CERIdentifier", c1.CERIdentifier);
            NamedParameters[2] = new SqlParameter("@RelatedCERYear", c2.CERYear);
            NamedParameters[3] = new SqlParameter("@RelatedCERIdentifier", c2.CERIdentifier);

            DBUtil.Exec(sql, AdminCart.Config.ConnStrCER(), NamedParameters);

            string CERnumber = Request["CERNumber"].ToString();
            Response.Redirect("EditCER.aspx?CERNumber=" + CERnumber + "&pageaction=lookup");
        }

    }

    private void LookupCER()
    {
        ButtonUpdate.Visible = true;
        lnkBtnReviewed.Visible = true;
        lnkBtnComplaint.Visible = true;
        lnkBtnOpenMedComplaint.Visible = true;
        lnkBtnDNR.Visible = true;
        lnkBtnNTF.Visible = true;
        if (Request["CERNumber"] != null)
        {

            //pull data through the CER class from the database
            string CERNumber = Request["CERNumber"].ToString();

            adminCER.CER obj = new adminCER.CER(CERNumber);

            lblCerNumber_Year.Text = obj.CERYear;
            lblCerNumber_Sequence.Text = obj.CERIdentifier.ToString("000000");
            Session["lblCerNumber_Year"] = lblCerNumber_Year.Text;
            Session["lblCerNumber_Sequence"] = lblCerNumber_Sequence.Text;

            if (Convert.ToInt32(obj.CustomerID) == 0)
            {
                obj.CustomerID = CreateNewCustomer(obj);

                if (Convert.ToInt32(obj.CustomerID) > 0)
                {
                    obj.InsertBy = "CER System";
                    obj.ActionExplaination = "Updated Customer Details";
                    obj.Update(); //update the CER w/ the new CustomerID
                }
            }

            if (Request["Print"] != null)
            {
                Panel pnl = (Panel)Master.FindControl("pnlHeader");
                if (pnl != null) { pnl.Visible = false; }

                lnkPrint.Visible = false;
                lnkUploadPics.Visible = false;
                lnkViewPics.Visible = false;

                radioCerTypeC.Enabled = false;
                radioCerTypeNC.Enabled = false;

                radListMDR.Enabled = false;
                radListAnalyze.Enabled = false;

                selPointOfPurchase.Enabled = false;
                selProductNumber.Enabled = false;
                radEOAsUnpackaged.Enabled = false;
                radEOPrior.Enabled = false;
                radEODuring.Enabled = false;
                radEOAfter.Enabled = false;
                pnlUpdate.Visible = false;
                //pnlActionTaken.Visible = false;
                radStatusCs.Enabled = false;
                radStatusQa.Enabled = false;
                radStatusMed.Enabled = false;
                radListMDR.Enabled = false;
                radListAnalyze.Enabled = false;

                pnlMessage.Visible = false;
                pnlAddRelated.Visible = false;
                RelatedCERNumber_Sequence.Visible = false;
                txtOrderNumber.Enabled = false;
                txtSerialNumber.Enabled = false;
                btnDetailsUpdate.Visible = false;
                dtlDefectAnalysis.Visible = false;
                dtlReturnEntry.Visible = false;
            }
            else
            {
                dtlReturnEntry.Visible = true;
                Returns ret = new Returns(Convert.ToInt32(Session["lblCerNumber_Year"].ToString()), Convert.ToInt32(Session["lblCerNumber_Sequence"]));
                if (ret.Id == 0) //default mode for detailview is ReadOnly
                {
                    dtlReturnEntry.ChangeMode(DetailsViewMode.Insert);
                    dtlReturnEntry.DataBind();
                    InitReturnInsertMode();
                }
                else
                {
                    sqlReturnDetails.SelectParameters["Year"].DefaultValue = ValidateUtil.CleanUpString(Session["lblCerNumber_Year"].ToString());
                    sqlReturnDetails.SelectParameters["CerNum"].DefaultValue = ValidateUtil.CleanUpString(Session["lblCerNumber_Sequence"].ToString());
                    //InitReadOnlyMode();
                    dtlReturnEntry.ChangeMode(DetailsViewMode.ReadOnly);
                }
            }

            //lblCustomerID.Text = obj.CustomerID;
            lnkCustomerID.NavigateUrl = "~/Customers/Details/" + obj.CustomerID;
            lnkCustomerID.Text = obj.CustomerID;
            int cID = Convert.ToInt32(obj.CustomerID);
            AdminCart.Customer c = new AdminCart.Customer(cID);
            AdminCart.Address a = new AdminCart.Address();
            a = AdminCart.Address.LoadAddress(cID, 1);

            literalReceivedBy.Text = obj.ReceivedBy;
            literalComplaintDeterminationBy.Text = obj.ComplaintDeterminationBy;
            TxtDateReceived.Text = obj.DateReportReceived.ToString();
            txtFirstName.Text = obj.FirstName;

            obj.LastName = obj.LastName.Replace("''", "'");

            txtLastName.Text = c.LastName.Replace("''", "'");
            txtStreet.Text = a.Street;
            txtCity.Text = a.City;
            txtState.Text = a.State;
            txtZip.Text = a.Zip;
            txtCountry.Text = a.Country;
            txtEmail.Text = c.Email;
            txtPhone.Text = c.Phone;

            lblCondition.Visible = true;
            radCondition.Visible = true;
            DisplayMedicalCondition();

            txtSerialNumber.Text = obj.SerialNumber;
            obj.EventDescription = obj.EventDescription.Replace("''", "'");
            txtEventDescription.Text = obj.EventDescription;
            lblEventDescription.Text = obj.EventDescription;
            txtOrderNumber.Text = obj.OrderNumber.ToString();

            ListItem item3 = selPointOfPurchase.Items.FindByValue(obj.PointOfPurchase); if (item3 != null) item3.Selected = true;
            ListItem item5 = radCondition.Items.FindByValue(obj.MedicalCondition); if (item5 != null) item5.Selected = true;
            ListItem item4 = selProductNumber.Items.FindByValue(obj.ProductNumber); if (item4 != null) item4.Selected = true;

            ListItem itemCs = radStatusCs.Items.FindByValue(obj.StatusCs); if (itemCs != null) itemCs.Selected = true;
            ListItem itemQa = radStatusQa.Items.FindByValue(obj.StatusQa); if (itemQa != null) itemQa.Selected = true;
            ListItem itemMed = radStatusMed.Items.FindByValue(obj.StatusMed); if (itemMed != null) itemMed.Selected = true;

            if (obj.Mdr)
            {
                ListItem itemMDR_Yes = radListMDR.Items.FindByValue("Yes");
                if (itemMDR_Yes != null) { itemMDR_Yes.Selected = true; }
            }
            else
            {
                ListItem itemMDR_No = radListMDR.Items.FindByValue("No");
                if (itemMDR_No != null) { itemMDR_No.Selected = true; }
            }

            if (obj.Analyze)
            {
                ListItem itemAnalyze_Yes = radListAnalyze.Items.FindByValue("Yes");
                if (itemAnalyze_Yes != null) { itemAnalyze_Yes.Selected = true; }
            }
            else
            {
                ListItem itemAnalyze_No = radListAnalyze.Items.FindByValue("No");
                if (itemAnalyze_No != null) { itemAnalyze_No.Selected = true; }
            }

            if (obj.CERType == "NC")
            {
                radioCerTypeNC.Checked = true;
                radioCerTypeC.Checked = false;
            }
            else
            {
                radioCerTypeNC.Checked = false;
                radioCerTypeC.Checked = true;
            }

            BindHistory();
            BindMessages();
            BindOrderNotes();
            BindOrders(obj.OrderNumber);
            BindRelatedCERS(obj.CERYear, obj.CERIdentifier);
            //BindNotes(obj);

            //initialize the Defect Resolution section
            if (Convert.ToBoolean(Session["IsAdmin"]))
            {
                if ((radioCerTypeC.Checked) && (Request["Print"] == null))
                {
                    dtlDefectAnalysis.Visible = true;

                    Defect defect = new Defect(Convert.ToInt32(Session["lblCerNumber_Year"]), Convert.ToInt32(Session["lblCerNumber_Sequence"]));
                    if (defect.Id == 0) //default mode for detailview is ReadOnly
                    {
                        dtlDefectAnalysis.ChangeMode(DetailsViewMode.Insert);
                        dtlDefectAnalysis.DataBind();
                        InitInsertMode();
                    }
                    else
                    {
                        sqlDefectsDetails.SelectParameters["Year"].DefaultValue = ValidateUtil.CleanUpString(Session["lblCerNumber_Year"].ToString());
                        sqlDefectsDetails.SelectParameters["CerNum"].DefaultValue = ValidateUtil.CleanUpString(Session["lblCerNumber_Sequence"].ToString());
                        //dtlDefectAnalysis.ChangeMode(DetailsViewMode.ReadOnly);
                        InitReadOnlyMode();
                    }
                }
            }

            //if medical and complaint, show alert
            if (obj.CERType == "C" && (obj.StatusMed == "Assigned" || obj.StatusMed == "Closed"))
                lblAlert.Text = "Attention - Medical Complaint!";
        }
    }

    private Control FindControlRecursive(Control ctlRoot, string sControlId)
    {
        // if this control is the one we are looking for, break from the recursion
        // and return the control.
        if (ctlRoot.ID == sControlId)
        {
            return ctlRoot;
        }

        // loop the child controls of this parent control and call recursively.
        foreach (Control ctl in ctlRoot.Controls)
        {
            Control ctlFound = FindControlRecursive(ctl, sControlId);

            // if we found the control, return it.
            if (ctlFound != null)
            {
                return ctlFound;
            }
        }

        // we never found the control so just return null.
        return null;
    } 


}
