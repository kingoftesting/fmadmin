﻿<%@ Page Title="" Language="C#" Theme="Admin" MasterPageFile="~/admin/MasterPage3_Blank.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" Inherits="HDIReport" Codebehind="HDI-Report.aspx.cs" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

<div id="divUnitsMOM_DEC">

    <asp:Panel ID="pnlUnitsMOM_DEC" Visible="true" runat="server">

       <div>
            <asp:Chart ID="chrtUnitsMOM_DEC" Height="480px" Width="760px" runat="server" 
                        Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                        BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
                <Series>
                </Series>
                <Titles>
                    <asp:Title Name="Title1" Text="FM.com Units Sold (DEC vs. NOV)" Font="Arial, 18">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                        BorderDashStyle="Solid" BorderWidth="1">
                        <Position Auto="true" />
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX Title="Day" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                            <MajorGrid Interval="5" />
                        </AxisX>
                        <AxisY Title="Units" TitleFont="Arial, 18" TitleForeColor="Black">
                            <LabelStyle Format="N0" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
    </asp:Panel>
</div>

<hr />

<div id="divRevMOM_DEC">

    <asp:Panel ID="pnlRevMOM_DEC" Visible="true" runat="server">

       <div>
            <asp:Chart ID="chrtRevMOM_DEC" Height="480px" Width="760px" runat="server" 
                        Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                        BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
                <Series>
                </Series>
                <Titles>
                    <asp:Title Name="Title1" Text="FM.com Net Revenue (DEC vs. NOV)" Font="Arial, 18">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                        BorderDashStyle="Solid" BorderWidth="1">
                        <Position Auto="true" />
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX Title="Day" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                            <MajorGrid Interval="5" />
                        </AxisX>
                        <AxisY Title="Net Revenue" TitleFont="Arial, 18" TitleForeColor="Black">
                            <LabelStyle Format="C0" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
    </asp:Panel>
</div>

<hr />

<div id="divUnitsYOY_DEC">

    <asp:Panel ID="pnlUnitsYOY_DEC" Visible="true" runat="server">

       <div>
            <asp:Chart ID="chrtUnitsYOY_DEC" Height="480px" Width="760px" runat="server" 
                        Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                        BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
                <Series>
                </Series>
                <Titles>
                    <asp:Title Name="Title1" Text="FM.com Units Sold (DEC 2012 vs. DEC 2011)" Font="Arial, 18">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                        BorderDashStyle="Solid" BorderWidth="1">
                        <Position Auto="true" />
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX Title="Day" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                            <MajorGrid Interval="5" />
                        </AxisX>
                        <AxisY Title="Units" TitleFont="Arial, 18" TitleForeColor="Black">
                            <LabelStyle Format="N0" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
    </asp:Panel>
</div>

<hr />

<div id="divRevYOY_DEC">

    <asp:Panel ID="pnlRevYOY_DEC" Visible="true" runat="server">

       <div>
            <asp:Chart ID="chrtRevYOY_DEC" Height="480px" Width="760px" runat="server" 
                        Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                        BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
                <Series>
                </Series>
                <Titles>
                    <asp:Title Name="Title1" Text="FM.com Daily Net Revenue (DEC 2012 vs. DEC 2011)" Font="Arial, 18">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                        BorderDashStyle="Solid" BorderWidth="1">
                        <Position Auto="true" />
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX Title="Day" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                            <MajorGrid Interval="5" />
                        </AxisX>
                        <AxisY Title="Net Revenue" TitleFont="Arial, 18" TitleForeColor="Black">
                            <LabelStyle Format="C0" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
    </asp:Panel>
</div>

<hr />

<div id="divUnitsYOY_NOV">

    <asp:Panel ID="pnlUnitsYOY_NOV" Visible="true" runat="server">

       <div>
            <asp:Chart ID="chrtUnitsYOY_NOV" Height="480px" Width="760px" runat="server" 
                        Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                        BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
                <Series>
                </Series>
                <Titles>
                    <asp:Title Name="Title1" Text="FM.com Units Sold (NOV 2012 vs. NOV 2011)" Font="Arial, 18">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                        BorderDashStyle="Solid" BorderWidth="1">
                        <Position Auto="true" />
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX Title="Day" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                            <MajorGrid Interval="5" />
                        </AxisX>
                        <AxisY Title="Units" TitleFont="Arial, 18" TitleForeColor="Black">
                            <LabelStyle Format="N0" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
    </asp:Panel>
</div>

<hr />

<div id="divRevYOY_NOV">

    <asp:Panel ID="pnlRevYOY_NOV" Visible="true" runat="server">

       <div>
            <asp:Chart ID="chrtRevYOY_NOV" Height="480px" Width="760px" runat="server" 
                        Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                        BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
                <Series>
                </Series>
                <Titles>
                    <asp:Title Name="Title1" Text="FM.com Daily Net Revenue (NOV 2012 vs. NOV 2011)" Font="Arial, 18">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                        BorderDashStyle="Solid" BorderWidth="1">
                        <Position Auto="true" />
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX Title="Day" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                            <MajorGrid Interval="5" />
                        </AxisX>
                        <AxisY Title="Net Revenue" TitleFont="Arial, 18" TitleForeColor="Black">
                            <LabelStyle Format="C0" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
    </asp:Panel>
</div>

<hr />



<div id="divUnitsMOM_NOV">

    <asp:Panel ID="pnlUnitsMOM_NOV" Visible="true" runat="server">

       <div>
            <asp:Chart ID="chrtUnitsMOM_NOV" Height="480px" Width="760px" runat="server" 
                        Palette="BrightPastel" BackColor="AliceBlue" BackGradientStyle="TopBottom" BackSecondaryColor="White"
                        BorderlineColor="Black" BorderlineWidth="2" BorderlineDashStyle="Solid" AntiAliasing="All">
                <Series>
                </Series>
                <Titles>
                    <asp:Title Name="Title1" Text="FM.com Units Sold (NOV 2012 vs. OCT 2012)" Font="Arial, 18">
                    </asp:Title>
                </Titles>
                <Legends>
                    <asp:Legend Name="Legend1" Docking="Bottom" Alignment="Center" BorderColor="Black" 
                        BorderDashStyle="Solid" BorderWidth="1">
                        <Position Auto="true" />
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX Title="Day" Interval="1" TitleFont="Arial, 18" TitleForeColor="Black">
                            <MajorGrid Interval="5" />
                        </AxisX>
                        <AxisY Title="Units" TitleFont="Arial, 18" TitleForeColor="Black">
                            <LabelStyle Format="N0" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
    </asp:Panel>
</div>

<hr />



</asp:Content>

