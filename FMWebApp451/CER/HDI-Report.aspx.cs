﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Data.SqlClient;
using CERmetrics;
using AdminCart;
using FM2015.Helpers;


public partial class HDIReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        initUnitsYOY_DEC();
        initRevYOY_DEC();
        initUnitsYOY_NOV();
        initRevYOY_NOV();
        initUnitsMOM_DEC();
        initRevMOM_DEC();
        initUnitsMOM_NOV();

    }

    protected void initUnitsMOM_NOV()
    {
        string endDate = "11/30/2012" + " 23:59:59.997";

        string startDate = "10/1/2012" + " 0:0:0";

        string sql = @"                       
                        SELECT DATEPART(month,[Date]) AS 'Month', 
                               DATEPART(day,[Date]) AS 'Day', 
                               units = sum(units) 
                        FROM ( 
		                        SELECT 
			                        [DATE] = o.orderdate, 
			                        CASE 
				                        WHEN (od.ProductID = 14) 
				                        THEN od.quantity 
				                        ELSE 0 
			                        END  
			                        Units 
		                        FROM Orders o, OrderDetails od, OrderActions oa 
		                        WHERE o.OrderID = od.OrderID and o.OrderID = oa.OrderID and oa.OrderactionType = 2 
			                        and o.OrderDate Between @startDate and @endDate 
	                          ) 
	                          AS tmp 
                        GROUP BY DATEPART(day,[Date]),  
                                 DATEPART(month,[Date]) 
                        ORDER BY 1,2; 
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
        SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);


        //chrtMedPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        chrtUnitsMOM_NOV.DataBindCrossTable(dr, "Month", "Day", "Units", "Label=Units{N0}", PointSortOrder.Descending);
        foreach (Series series in chrtUnitsMOM_NOV.Series)
        { series.ChartType = SeriesChartType.Column; }

    }

    protected void initUnitsYOY_NOV()
    {
        string endDate = "11/30/2012" + " 23:59:59.997";

        string startDate = "11/1/2011" + " 0:0:0";

        string sql = @"                       
                        SELECT DATEPART(year,[Date]) AS 'Year', 
                               DATEPART(day,[Date]) AS 'Day', 
                               units = sum(units) 
                        FROM ( 
		                        SELECT 
			                        [DATE] = o.orderdate, 
			                        CASE 
				                        WHEN (od.ProductID = 14) 
				                        THEN od.quantity 
				                        ELSE 0 
			                        END  
			                        Units 
		                        FROM Orders o, OrderDetails od, OrderActions oa 
		                        WHERE o.OrderID = od.OrderID and o.OrderID = oa.OrderID and oa.OrderactionType = 2 
			                        and o.OrderDate Between @startDate and @endDate 
                                    and month(orderDate) = 11 
	                          ) 
	                          AS tmp 
                        GROUP BY DATEPART(day,[Date]),  
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
        SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);


        //chrtMedPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        chrtUnitsYOY_NOV.DataBindCrossTable(dr, "Year", "Day", "Units", "Label=Units{N0}", PointSortOrder.Descending);
        foreach (Series series in chrtUnitsYOY_NOV.Series)
        { series.ChartType = SeriesChartType.Column; }

    }

    protected void initRevYOY_DEC()
    {
        string endDate = "12/31/2012" + " 23:59:59.997";

        string startDate = "12/1/2011" + " 0:0:0";

        string sql = @"                       
                        SELECT DATEPART(year,[Date]) AS 'Year', 
                               DATEPART(day,[Date]) AS 'Day', 
                               NetRevenue = sum(NetRevenue) 
                        FROM ( 
		                        SELECT 
			                        [DATE] = o.orderdate, 			                          
			                        NetRevenue = o.Total - o.TotalTax - o.TotalShipping 
		                        FROM Orders o,  OrderActions oa 
		                        WHERE o.OrderID = oa.OrderID and oa.OrderactionType = 2 
			                        and o.OrderDate Between @startDate and @endDate 
                                    and month(orderDate) = 12 
	                          ) 
	                          AS tmp 
                        GROUP BY DATEPART(day,[Date]),  
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
        SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);


        //chrtMedPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        chrtRevYOY_DEC.DataBindCrossTable(dr, "Year", "Day", "NetRevenue", "Label=NetRevenue{C0}", PointSortOrder.Descending);
        foreach (Series series in chrtRevYOY_DEC.Series)
        { series.ChartType = SeriesChartType.Column; }

    }

    protected void initRevYOY_NOV()
    {
        string endDate = "11/30/2012" + " 23:59:59.997";

        string startDate = "11/1/2011" + " 0:0:0";

        string sql = @"                       
                        SELECT DATEPART(year,[Date]) AS 'Year', 
                               DATEPART(day,[Date]) AS 'Day', 
                               NetRevenue = sum(NetRevenue) 
                        FROM ( 
		                        SELECT 
			                        [DATE] = o.orderdate, 			                          
			                        NetRevenue = o.Total - o.TotalTax - o.TotalShipping 
		                        FROM Orders o,  OrderActions oa 
		                        WHERE o.OrderID = oa.OrderID and oa.OrderactionType = 2 
			                        and o.OrderDate Between @startDate and @endDate 
                                    and month(orderDate) = 11 
	                          ) 
	                          AS tmp 
                        GROUP BY DATEPART(day,[Date]),  
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
        SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);


        //chrtMedPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        chrtRevYOY_NOV.DataBindCrossTable(dr, "Year", "Day", "NetRevenue", "Label=NetRevenue{C0}", PointSortOrder.Descending);
        foreach (Series series in chrtRevYOY_NOV.Series)
        { series.ChartType = SeriesChartType.Column; }

    }

    protected void initUnitsMOM_DEC()
    {
        string endDate = "12/31/2012" + " 23:59:59.997";

        string startDate = "11/1/2012" + " 0:0:0";

        string sql = @"                       
                        SELECT DATEPART(month,[Date]) AS 'Month', 
                               DATEPART(day,[Date]) AS 'Day', 
                               units = sum(units) 
                        FROM ( 
		                        SELECT 
			                        [DATE] = o.orderdate, 
			                        CASE 
				                        WHEN (od.ProductID = 14) 
				                        THEN od.quantity 
				                        ELSE 0 
			                        END  
			                        Units 
		                        FROM Orders o, OrderDetails od, OrderActions oa 
		                        WHERE o.OrderID = od.OrderID and o.OrderID = oa.OrderID and oa.OrderactionType = 2 
			                        and o.OrderDate Between @startDate and @endDate 
	                          ) 
	                          AS tmp 
                        GROUP BY DATEPART(day,[Date]),  
                                 DATEPART(month,[Date]) 
                        ORDER BY 1,2; 
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
        SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);


        //chrtMedPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        chrtUnitsMOM_DEC.DataBindCrossTable(dr, "Month", "Day", "Units", "Label=Units{N0}", PointSortOrder.Descending);
        foreach (Series series in chrtUnitsMOM_DEC.Series)
        { series.ChartType = SeriesChartType.Column; }

    }

    protected void initRevMOM_DEC()
    {
        string endDate = "12/31/2012" + " 23:59:59.997";

        string startDate = "11/1/2012" + " 0:0:0";

        string sql = @"                       
                        SELECT DATEPART(month,[Date]) AS 'Month', 
                               DATEPART(day,[Date]) AS 'Day', 
                               NetRevenue = sum(NetRevenue) 
                        FROM ( 
		                        SELECT 
			                        [DATE] = o.orderdate, 			                          
			                        NetRevenue = o.Total - o.TotalTax - o.TotalShipping 
		                        FROM Orders o,  OrderActions oa 
		                        WHERE o.OrderID = oa.OrderID and oa.OrderactionType = 2 
			                        and o.OrderDate Between @startDate and @endDate  
	                          ) 
	                          AS tmp 
                        GROUP BY DATEPART(day,[Date]),  
                                 DATEPART(month,[Date]) 
                        ORDER BY 1,2; 
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
        SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);


        //chrtMedPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        chrtRevMOM_DEC.DataBindCrossTable(dr, "Month", "Day", "NetRevenue", "Label=NetRevenue{C0}", PointSortOrder.Descending);
        foreach (Series series in chrtRevMOM_DEC.Series)
        { series.ChartType = SeriesChartType.Column; }

    }

    protected void initUnitsYOY_DEC()
    {
        string endDate = "12/31/2012" + " 23:59:59.997";

        string startDate = "12/1/2011" + " 0:0:0";

        string sql = @"                       
                        SELECT DATEPART(year,[Date]) AS 'Year', 
                               DATEPART(day,[Date]) AS 'Day', 
                               units = sum(units) 
                        FROM ( 
		                        SELECT 
			                        [DATE] = o.orderdate, 
			                        CASE 
				                        WHEN (od.ProductID = 14) 
				                        THEN od.quantity 
				                        ELSE 0 
			                        END  
			                        Units 
		                        FROM Orders o, OrderDetails od, OrderActions oa 
		                        WHERE o.OrderID = od.OrderID and o.OrderID = oa.OrderID and oa.OrderactionType = 2 
			                        and o.OrderDate Between @startDate and @endDate 
                                    and month(orderDate) = 12 
	                          ) 
	                          AS tmp 
                        GROUP BY DATEPART(day,[Date]),  
                                 DATEPART(year,[Date]) 
                        ORDER BY 1,2; 
                    ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
        SqlDataReader dr = DBUtil.FillDataReader(sql, Config.ConnStr(), mySqlParameters);


        //chrtMedPareto.LoadTemplate("~/ChartTemplates/SkyBlue.xml");

        //add chart series data here
        chrtUnitsYOY_DEC.DataBindCrossTable(dr, "Year", "Day", "Units", "Label=Units{N0}", PointSortOrder.Descending);
        foreach (Series series in chrtUnitsYOY_DEC.Series)
        { series.ChartType = SeriesChartType.Column; }

    }
}