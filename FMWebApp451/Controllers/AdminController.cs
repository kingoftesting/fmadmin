﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminCart;

namespace FM2015.Controllers
{
    public class AdminMVCController : Controller
    {
        // GET: Index
        public ActionResult Index()
        {
            
            List<FM2015.FMmetrics.UnProcessedOrders> orderList = new List<FM2015.FMmetrics.UnProcessedOrders>();
            orderList = FM2015.FMmetrics.mvc_Metrics.GetUnshippedOrdersList();
            if (orderList.Count() == 0)
            {
                ViewBag.NoTrackingCount = "0";
                ViewBag.NoTrackingDelay = "0";
            }
            else
            {
                ViewBag.NoTrackingCount = orderList.Count().ToString();
                ViewBag.NoTrackingDelay = orderList[0].DaysDelayed.ToString();
            }

            List<FM2015.FMmetrics.UnProcessedOrders> orderList2 = new List<FM2015.FMmetrics.UnProcessedOrders>();
            orderList2 = FM2015.FMmetrics.mvc_Metrics.GetUnshippedSerOrdersList();
            if (orderList2.Count() == 0)
            { ViewBag.NoSerialCount = "0"; }
            else
            { ViewBag.NoSerialCount = orderList2.Count().ToString(); }

            List<adminCER.CER> cerList = adminCER.CER.GetCERAnalyzeList();
            if (cerList.Count() == 0)
            { ViewBag.cerAnalyzeCount = "0"; }
            else
            { ViewBag.cerAnalyzeCount = cerList.Count().ToString(); }

            cerList = adminCER.CER.GetOpenComplaints();
            cerList = (from cer in cerList where (cer.DateReportReceived < DateTime.Now.AddDays(-30)) select cer).ToList();
            if (cerList.Count() == 0)
            { ViewBag.cerAgingCount = "0"; }
            else
            { ViewBag.cerAgingCount = cerList.Count().ToString(); }

            return View();
        }

        //GET: FindCustomers
        public ActionResult FindCustomers(string paramCustomer)
        {
            if (string.IsNullOrEmpty(paramCustomer))
            {
                return Redirect("~/Customers/index/");
            }
            else
            {
                return Redirect("~/Customers/index/?paramCustomer=" + paramCustomer);
            }
            
        }

        //GET: FindOrders
        public ActionResult FindOrders(string paramOrder)
        {
            if (string.IsNullOrEmpty(paramOrder))
            {
                return Redirect("~/Orders/index/");
            }
            else
            {
                return Redirect("~/Orders/index/?paramOrder=" + @HttpUtility.UrlEncode(paramOrder));
            }
        }

        //GET: FindOrders
        public ActionResult FindSubscribers(string param)
        {
            if (string.IsNullOrEmpty(param))
            {
                return Redirect("~/subscribers/index/");
            }
            else
            {
                return Redirect("~/subscribers/index/?param="+ param);
            }
        }

        //GET: GetShippingZoones
        public ActionResult GetShippingZones()
        {
            List<ShopifyAgent.ShopifyShippingZone> shippingZones = ShopifyAgent.ShopifyShippingZone.GetShippingZonesViaAPI();
            return View(shippingZones);
        }

        //GET: Test Web Api access
        public ActionResult TestWebApi()
        {
            string testURL = Config.TestWebApi_URL();
            string response = FMWebApp.Helpers.JsonHelpers.GETfromWebAPI(testURL);
            ViewBag.What = "Test WebApi: GetPing";
            ViewBag.URL = testURL;
            ViewBag.Response = response;
            return View();

        }
    }
}