﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminCart;
using FM2015.Models;

namespace FM2015.Controllers
{
    public class BulkReturnsIBNUController : Controller
    {
        // GET: BulkReturnsIBNU
        public ActionResult Index(string param)
        {
            List<BulkReturnsIBNU> theList = new List<BulkReturnsIBNU>();

            string serNumStr = "";
            long serNum = 0;
            string date1 = "";
            string date2 = "";
            bool useDateParams = true;
            DateTime endDate = DateTime.Now;
            DateTime startDate = endDate.AddDays(-7);

            if (!string.IsNullOrEmpty(param))
            {
                if (param[0] == '#')
                {
                    serNumStr = param.Replace("#", ""); //looks like a serial number
                    if (!long.TryParse(serNumStr, out serNum))
                    {
                        ViewBag.ErrMsg = "Serial Number must contain only integers (like: 61312103422)";
                        return View(theList);
                    }
                    useDateParams = false;
                }
                else
                {
                    string[] name = param.Split(' '); // see if theres a startDate and endDate
                    if (name.Length > 0)
                    {
                        date1 = name[0];
                    }
                    if (name.Length > 1)
                    {
                        date2 = name[1];
                    }
                    // check for a date range
                    if (DateTime.TryParse(date1, out startDate))
                    {
                        useDateParams = true; //must be a startDate
                        if (!string.IsNullOrEmpty(date2)) //now check for an endDate
                        {
                            if (!DateTime.TryParse(date2, out endDate))
                            {
                                ViewBag.ErrMsg = "endDate must be a Date (like: 1/1/2015)";
                                return View(theList);
                            }
                            else
                            {
                                endDate = endDate.Date.AddDays(1).AddTicks(-1); //get just before midnight
                            }
                        }
                        if (endDate < startDate)
                        {
                            ViewBag.ErrMsg = "startDate must be before EndDate";
                            return View(theList);
                        }
                    }
                }
            }
            else
            {
                useDateParams = false; //is orderID
            }

            theList = BulkReturnsIBNU.GetBulkReturnsIBNUList();
            if (!useDateParams)
            {
                if (serNum > 0)
                {
                    string serStr = serNum.ToString();
                    theList = (from b in theList where (b.Serial.ToString().Contains(serStr)) select b).ToList();
                }
            }
            else
            {
                theList = (from b in theList where (b.AddDate >= startDate && b.AddDate <= endDate) select b).ToList();
            }
            return View(theList);
        }

        // GET: BulkReturnsIBNU/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: BulkReturnsIBNU/Create
        public ActionResult Create()
        {
            BulkReturnsIBNU br = new BulkReturnsIBNU();
            //br.Test = true;
            br.Test = false; //go live

            return View(br);
        }

        // POST: BulkReturnsIBNU/Create
        [HttpPost]
        public ActionResult Create(BulkReturnsIBNU br)
        {
            List<BulkReturnsIBNU> theList = new List<BulkReturnsIBNU>();
            ViewBag.ErrMsg = "";
            ViewBag.OKMsg = "";
            bool userOK = FM2015.Models.User.HasRole("returns");
            if (userOK)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        br.Id = 0; //make sure this is a new insert
                        if (br.Serial > 0)
                        {
                            theList = BulkReturnsIBNU.GetBulkReturnsIBNUList();
                            switch (AdminCart.Config.appName.ToUpper())
                            {
                                case "FACEMASTER":
                                    if ((br.Serial < 10601000000) || (br.Serial > 62000000000))
                                    {
                                        ViewBag.ErrMsg = "Error: Failed To Save SerNum: Serial Number Invalid: " + br.Serial.ToString();
                                        return View(br);
                                    }
                                    List<BulkReturnsIBNU> objList = (from b in theList where (b.Serial == br.Serial) && (b.AddDate > DateTime.Today.AddDays(-7)) select b).ToList();
                                    if (objList.Count > 0)
                                    {
                                        ViewBag.ErrMsg = "Error: Serial Number Has Been Entered Within Last 7 Days: " + br.Serial.ToString();
                                        return View(br);
                                    }
                                    break;

                                case "CRYSTALIFT":
                                    break;

                                case "SONULASE":
                                    break;

                                default:
                                    ViewBag.ErrMsg = "Error: Failed To Save SerNum: Unrecognized Company (not FM, CL or SU): " + br.Serial.ToString();
                                    return View(br);
                            }
                            br.Save();
                            ViewBag.OKMsg = "Success! Return Device Entered OK; Serial Number: " + br.Serial.ToString();
                            return View(br);
                        }
                        else
                        {
                            ViewBag.ErrMsg = "Error: Failed To Save SerNum: Serial Number Invalid: " + br.Serial.ToString();
                            return View(br);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.ErrMsg = "Error: Failed To Save SerNum: " + ex.Message;
                    return View(br);
                }
            }
            else
            {
                ViewBag.ErrMsg = "Error: Failed To Save SerNum: User Not Authorized";
                return View(br);
            }
            ViewBag.ErrMsg = "Error: Failed To Save SerNum: User Not Authorized";
            return View(br);
        }

        // GET: BulkReturnsIBNU/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BulkReturnsIBNU/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BulkReturnsIBNU/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: BulkReturnsIBNU/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
