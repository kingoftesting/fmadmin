﻿using System;
using System.Web.Mvc;
using AdminCart;
using FMWebApp451.ViewModels.Stripe;
using FMWebApp451.Interfaces;
using Stripe;

namespace FM2015.Controllers
{
    public class CCUpdateController : Controller
    {
        private readonly ICreditCard creditCardService;
        private readonly ICustomer customerProvider;

        public CCUpdateController(ICreditCard creditCardService, ICustomer customerProvider)
        {
            this.creditCardService = creditCardService;
            this.customerProvider = customerProvider;
        }

        // GET: CCUpdate
        public ActionResult Index(int? customerId)
        {
            CreditCardViewModel ccvm = new CreditCardViewModel();
            if ((customerId == null) || (customerId == 0))
            {
                ViewBag.Name = "Name: Unknown";
                ViewBag.ErrorMsg = @"Customer information is unavailable. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";
                return View(ccvm);
            }
            int id = Convert.ToInt32(customerId);
            Customer c = customerProvider.GetById(id);
            ccvm.CreditCardViewModelKey = id;
            ccvm.Name = c.FirstName + " " + c.LastName;
            return View(ccvm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(CreditCardViewModel ccvm)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(ccvm.CardNumber))
                {
                    ViewBag.ErrorMsg = "Card Number is required";
                    return View(ccvm);
                }
                if (ccvm.CardNumber.Length < 15)
                {
                    ViewBag.ErrorMsg = "Card is less than 15 characters.  Please correct, then try again.";
                    return View(ccvm);
                }
                if (string.IsNullOrEmpty(ccvm.Name))
                {
                    ViewBag.ErrorMsg = "Name is required";
                    return View(ccvm);
                }
                if (string.IsNullOrEmpty(ccvm.ExpirationMonth))
                {
                    ViewBag.ErrorMsg = "Expiration Month is required";
                    return View(ccvm);
                }
                if (string.IsNullOrEmpty(ccvm.ExpirationYear))
                {
                    ViewBag.ErrorMsg = "Expiration Year is required";
                    return View(ccvm);
                }
                int temp = 0;
                if (!Int32.TryParse(ccvm.ExpirationMonth, out temp))
                {
                    ViewBag.ErrorMsg = "Expiration month must be a number from 1 to 12 (for instance, 5)";
                    return View(ccvm);
                }
                if ((Convert.ToInt32(ccvm.ExpirationMonth) < 1) || (Convert.ToInt32(ccvm.ExpirationMonth) > 12))
                {
                    ViewBag.ErrorMsg = "Expiration month must be a number from 1 to 12 (for instance, 6)";
                    return View(ccvm);
                }
                temp = 0;
                if (!Int32.TryParse(ccvm.ExpirationYear, out temp))
                {
                    ViewBag.ErrorMsg = "Expiration year must be a number (for instance, 2016)";
                    return View(ccvm);
                }
                if (string.IsNullOrEmpty(ccvm.Cvc))
                {
                    ViewBag.ErrorMsg = "CVS is required";
                    return View(ccvm);
                }
                temp = 0;
                if (!Int32.TryParse(ccvm.Cvc, out temp))
                {
                    ViewBag.ErrorMsg = "CVS must be all numbers (for instance, 123)";
                    return View(ccvm);
                }

                ccvm.CardNumber = ccvm.CardNumber.Replace(" ", ""); //get rid of any spaces customer may have used

                ViewBag.ErrorMsg = creditCardService.AddNewCard(ccvm);
            }
            
            return View(ccvm);
        }

        /********** SUPPORT METHODS **********/


    }
}