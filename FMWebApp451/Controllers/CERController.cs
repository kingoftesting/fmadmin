﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FM2015.Models.mvcCER;
using FM2015.ViewModels;
using AdminCart;
using adminCER;

namespace FM2015.Controllers
{
    public class mvcCERController : Controller
    {
        // GET: CER
        public ActionResult Index(string cerYear, string cerNum, string lastName, string firstName, string customerID, 
            string searchEV, string searchSerial, string ddlComplaint, string ddlDateRange, string ddlPop, string ddlOpenClose)
        {
            List<adminCER.CER> cerList = adminCER.CER.GetCERList(true); //get a non-cached version of the CER list
            DateTime endDate = DateTime.Now;
            int tmpInt = 0;
            ViewBag.ErrMsg = "";
            ViewBag.IsSearchEV = false;

            if ((string.IsNullOrEmpty(cerYear)) && (string.IsNullOrEmpty(cerNum)) && (string.IsNullOrEmpty(lastName)) 
                && (string.IsNullOrEmpty(firstName)) && (string.IsNullOrEmpty(customerID)) && (string.IsNullOrEmpty(searchEV)) && (string.IsNullOrEmpty(searchSerial))
                && (string.IsNullOrEmpty(ddlComplaint)) && (string.IsNullOrEmpty(ddlPop)) && (string.IsNullOrEmpty(ddlDateRange)))
            {
                //cerList = adminCER.CER.GetCERList(complaintStat.All, medStat.All, openStat.UnOpened, DateTime.Now.AddMonths(-12), DateTime.Now);
                cerList = (from cer in cerList where (cer.StatusQa.ToUpper() == "UNOPENED") select cer).ToList();
            }
            else
            {
                if (!string.IsNullOrEmpty(searchSerial))
                {
                    cerList = (from cer in cerList where (cer.SerialNumber.ToLower().Contains(searchSerial.ToLower())) select cer).ToList();
                }

                if (!string.IsNullOrEmpty(searchEV))
                {
                    cerList = (from cer in cerList where (cer.EventDescription.ToLower().Contains(searchEV.ToLower())) select cer).ToList();
                }

                if (!string.IsNullOrEmpty(ddlDateRange))
                {
                    switch (ddlDateRange)
                    {
                        case "0":
                            endDate = Convert.ToDateTime("1/1/2006"); //include the entire database
                            break;

                        case "1":
                            endDate = endDate.AddMonths(-6); //go back only 6 months
                            break;

                        case "2":
                            endDate = endDate.AddMonths(-12); //go back only 12 months
                            break;

                        case "3":
                            endDate = endDate.AddMonths(-24); //go back only 24 months
                            break;

                        default:
                            endDate = Convert.ToDateTime("1/1/2006"); //include the entire database
                            break;
                    }
                    cerList = (from cer in cerList where (cer.DateReportReceived >= endDate) select cer).ToList();
                }

                if (!string.IsNullOrEmpty(ddlComplaint))
                {
                    switch (ddlComplaint)
                    {
                        case "0": //do nothing; use entire CER list
                            break;

                        case "1": //non-complaint
                            cerList = (from cer in cerList where (cer.CERType == "NC") select cer).ToList();
                            break;

                        case "2": //all complaints
                            cerList = (from cer in cerList where (cer.CERType == "C") select cer).ToList();
                            break;

                        case "3": //just injuries
                            cerList = (from cer in cerList where (cer.CERType == "C" && (cer.StatusMed == "Assigned" || cer.StatusMed == "Closed")) select cer).ToList();
                            break;

                        case "4": //just non-injuries
                            cerList = (from cer in cerList where (cer.CERType == "C" && cer.StatusMed == "Does Not Apply") select cer).ToList();
                            break;

                        default: //do nothing; use entire CER list
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(ddlOpenClose))
                {
                    switch (ddlOpenClose)
                    {
                        case "0": //do nothing; use entire CER list
                            break;

                        case "1": //only Opens
                            cerList = (from cer in cerList where (cer.StatusQa != "Closed" || cer.StatusCs != "Closed" || cer.StatusMed == "Assigned") select cer).ToList();
                            break;

                        case "2":
                            cerList = (from cer in cerList where (cer.StatusQa == "Closed" && cer.StatusCs == "Closed" && cer.StatusMed != "Assigned") select cer).ToList();
                            break;

                        default: //do nothing; use entire CER list
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(ddlPop) && (ddlPop != "0"))
                {
                    cerList = (from cer in cerList where (cer.PointOfPurchase == ddlPop) select cer).ToList();
                }

                if (!string.IsNullOrEmpty(customerID))
                {
                    if (!Int32.TryParse(customerID, out tmpInt))
                    {
                        ViewBag.ErrMsg = "CustomerID must be a 4-dn integer (like: 1 or 512 or 40719, etc.)";
                        cerList = new List<adminCER.CER>();
                        return View(cerList);
                    }
                    cerList = (from cer in cerList where (cer.CustomerID == customerID) select cer).ToList();
                }
                if (!string.IsNullOrEmpty(cerYear))
                {
                    if (!Int32.TryParse(cerYear, out tmpInt))
                    {
                        ViewBag.ErrMsg = "CER Year must be a 4-digit year integer (like: 2016)";
                        cerList = new List<adminCER.CER>();
                        return View(cerList);
                    }
                    cerList = (from cer in cerList where (cer.CERYear == cerYear) select cer).ToList();

                }
                if (!string.IsNullOrEmpty(cerNum))
                {
                    if (!Int32.TryParse(cerNum, out tmpInt))
                    {
                        ViewBag.ErrMsg = "CER ID must be an integer (like: 1 or 12 or 123, etc.)";
                        cerList = new List<adminCER.CER>();
                        return View(cerList);
                    }
                    cerList = (from cer in cerList where (cer.CERIdentifier.ToString() == cerNum) select cer).ToList();
                }
                if (!string.IsNullOrEmpty(firstName))
                {
                    cerList = (from cer in cerList where (cer.FirstName.ToUpper().Contains(firstName.ToUpper())) select cer).ToList();
                }

                if (!string.IsNullOrEmpty(lastName))
                {
                    cerList = (from cer in cerList where (cer.LastName.ToUpper().Contains(lastName.ToUpper())) select cer).ToList();
                }
            }

            int recordcount = cerList.Count;
            ViewBag.RecordCount = recordcount.ToString();

            return View(cerList);
        }

        public ActionResult Search (string search)
        {
            List<adminCER.CER> cerList = adminCER.CER.SearchCERs(search);
            return View(cerList);
        }

        public ActionResult Details(string CERNumber)
        {
            CERViewModel cvm = new CERViewModel();
            cvm.cer = new adminCER.CER(CERNumber);
            cvm.cerMessageList = CERMessages.GetCERMessagesList(CERNumber);
            cvm.relatedCERList = adminCER.CER.GetRelatedCERs(cvm.cer.CERYear, cvm.cer.CERIdentifier);
            cvm.relatedCERList = cvm.relatedCERList.OrderByDescending(x => x.CERNumber).ToList();
            int orderId = 0;
            if (Int32.TryParse(cvm.cer.OrderNumber, out orderId))
            {
                if (orderId > 20000) 
                {
                    cvm.orderNoteList = OrderNote.GetOrderNoteList(Convert.ToInt32(cvm.cer.OrderNumber));
                    cvm.cart = cvm.cart.GetCartFromOrder(Convert.ToInt32(cvm.cer.OrderNumber));
                }
            }
            cvm.cerHistoryList = FM2015.Models.mvcCER.CERHistory.GetHistoryList(CERNumber);
            cvm.defectAnalysis = new CER.Defect(cvm.cer.CERNumber);
            cvm.returnAnalysis = new CER.Returns(cvm.cer.CERNumber);
            return View(cvm);
        }

        //GET: CER/Create
        public ActionResult Create(string customerID, string orderID)
        {
            CERCreateViewModel ccer = new CERCreateViewModel();
            Order o = new Order();
            Customer customer = new Customer();
            int cID = 0;
            int oID = 0;
            if (!string.IsNullOrEmpty(customerID))
            {
                if (Int32.TryParse(customerID, out cID))
                {
                    customer = new Customer(cID);
                }
                if (customer.CustomerID == 0)
                {
                    ViewBag.ErrorMessage = "New CER Error: no customer ID";
                    return View(ccer);
                }
                else
                {
                    ccer.CustomerID = customer.CustomerID;
                }
            }

            if (!string.IsNullOrEmpty(orderID))
            {
                if (Int32.TryParse(orderID, out oID))
                {
                    o = new Order(oID);
                }
                if (o.OrderID == 0)
                {
                    ViewBag.ErrorMessage = "New CER Error: no order ID";
                    return View(ccer);
                }
                else
                {
                    ccer.OrderID = o.OrderID;
                }
            }            
            return View(ccer);
        }

        // POST: CER/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CERCreateViewModel ccer)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            if (string.IsNullOrEmpty(ccer.ActionExplaination))
            {
                return RedirectToAction("Index");
            }

            if (ccer.ActionExplaination.Length < 4)
            {
                return RedirectToAction("Index");
            }

            adminCER.CER cer = new adminCER.CER();
            Customer c = new Customer();
            Cart cart = new Cart();

            if (ccer.OrderID > 0)
            {
                
                cart = cart.GetCartFromOrder(ccer.OrderID);
                cer = new adminCER.CER(cart);
            }
            else
            {
                c = new Customer(ccer.CustomerID);
                cer = new adminCER.CER(c);
            }

            cer.EventDescription = ccer.EventDescription;
            cer.ActionExplaination = ccer.ActionExplaination;
            cer.Add();

            return RedirectToAction("Index");
        }

        public ActionResult Related(string CERNumber)
        {
            List<adminCER.CER> cerList = adminCER.CER.GetCERList();

            adminCER.CER cer = new adminCER.CER(CERNumber);
            string firstName = "";
            if (cer.FirstName.Length > 1) { firstName = cer.FirstName.Substring(0, 2); }
            else { firstName = cer.FirstName.Substring(0, 1); }

            //cerList = adminCER.CER.FindCERList("", "", firstName, cer.LastName, "");
            cerList = (from c in cerList where ((c.FirstName.ToUpper().Contains(firstName.ToUpper())) && (c.LastName.ToUpper().Contains(cer.LastName.ToUpper()))) select c).ToList();
            List<adminCER.CER> newList = cerList.OrderBy(x => x.DateReportReceived).Reverse().ToList();

            ViewBag.ErrMsg = "";
            ViewBag.IsSearchEV = false;
            ViewBag.RecordCount = newList.Count.ToString();
            return View("Index", newList);
        }

        public ActionResult RemoveRelated(string CERNumber, string remCERNumber)
        {
            adminCER.CER.RemoveRelatedCER(CERNumber, remCERNumber);
            return RedirectToAction("Details", "mvcCER", new { CERNumber = CERNumber });
        }

        public ActionResult AllOpens()
        {
            //List<adminCER.CER> cerList = adminCER.CER.GetCERList(complaintStat.All, medStat.All, openStat.OpenOnly, DateTime.Now.AddYears(-2), DateTime.Now);
            List<adminCER.CER> cerList = adminCER.CER.GetCERList();
            cerList = (from cer in cerList where (cer.StatusQa.ToUpper() == "OPENED") select cer).ToList();
            ViewBag.ErrMsg = "";
            ViewBag.IsSearchEV = false;
            ViewBag.RecordCount = cerList.Count.ToString();
            return View("Index", cerList);
        }

        public ActionResult All30Opens()
        {
            List<adminCER.CER> cerList = adminCER.CER.GetCERList();
            //cerList = adminCER.CER.GetCERList(complaintStat.All, medStat.All, openStat.OpenOnly, DateTime.Now.AddYears(-2), DateTime.Now.AddDays(-30));
            cerList = (from cer in cerList where ((cer.StatusQa.ToUpper() == "OPENED")) && (cer.DateReportReceived <= DateTime.Now.AddDays(-30)) select cer).ToList();
            ViewBag.ErrMsg = "";
            ViewBag.IsSearchEV = false;
            ViewBag.RecordCount = cerList.Count.ToString();
            return View("Index", cerList);
        }

        public ActionResult AllUnOpens()
        {
            List<adminCER.CER> cerList = adminCER.CER.GetCERList();
            //List<adminCER.CER> cerList = adminCER.CER.GetCERList(complaintStat.All, medStat.All, openStat.UnOpened, DateTime.Now.AddYears(-2), DateTime.Now);
            cerList = (from cer in cerList where ((cer.StatusQa.ToUpper() == "UNOPENED")) && (cer.DateReportReceived > DateTime.Now.AddDays(-30)) select cer).ToList();
            ViewBag.ErrMsg = "";
            ViewBag.IsSearchEV = false;
            ViewBag.RecordCount = cerList.Count.ToString();
            return View("Index", cerList);
        }

        public ActionResult CloseNonComplaint(string cerNumber)
        {
            adminCER.CER cer = new adminCER.CER(cerNumber);
            cer.CERType = "NC";
            cer.ActionExplaination = "Reviewed & Closed";
            cer.StatusCs = "Closed";
            cer.StatusQa = "Closed";
            cer.StatusMed = "Does Not Apply";
            cer.MedicalCondition = "n/a";
            cer.ComplaintDeterminationBy = FM2015.Models.User.GetUserName();
            cer.Update();
            return RedirectToAction("Details", "mvcCER", new { CERNumber = cerNumber });
        }

        public ActionResult CloseNTF(string cerNumber)
        {
            adminCER.CER cer = new adminCER.CER(cerNumber);
            //cer.CERType = "NC";
            cer.ActionExplaination = "closed: no trouble found";
            cer.StatusCs = "Closed";
            cer.StatusQa = "Closed";
            cer.StatusMed = "Does Not Apply";
            if (cer.StatusMed == "Assigned")
            {
                cer.StatusMed = "Closed";
            }
            cer.StatusMed = "Does Not Apply";
            cer.MedicalCondition = "n/a";
            cer.ComplaintDeterminationBy = FM2015.Models.User.GetUserName();
            cer.Update();
            return RedirectToAction("Details", "mvcCER", new { CERNumber = cerNumber });
        }

        public ActionResult CloseDNR(string cerNumber)
        {
            adminCER.CER cer = new adminCER.CER(cerNumber);
            //cer.CERType = "NC";
            cer.ActionExplaination = "closed: did not return device";
            cer.StatusCs = "Closed";
            cer.StatusQa = "Closed";
            cer.StatusMed = "Does Not Apply";
            if (cer.StatusMed == "Assigned")
            {
                cer.StatusMed = "Closed";
            }
            cer.MedicalCondition = "n/a";
            cer.ComplaintDeterminationBy = FM2015.Models.User.GetUserName();
            cer.Update();
            return RedirectToAction("Details", "mvcCER", new { CERNumber = cerNumber });
        }

        public ActionResult OpenComplaint(string cerNumber)
        {
            adminCER.CER cer = new adminCER.CER(cerNumber);
            cer.CERType = "C";
            cer.ActionExplaination = "open: investigate complaint";
            cer.StatusCs = "Opened";
            cer.StatusQa = "Opened";
            cer.StatusMed = "Does Not Apply";
            cer.MedicalCondition = "n/a";
            cer.Analyze = true;
            cer.ComplaintDeterminationBy = FM2015.Models.User.GetUserName();
            cer.Update();
            return RedirectToAction("Details", "mvcCER", new { CERNumber = cerNumber });
        }

        public ActionResult OpenMedComplaint(string cerNumber)
        {
            adminCER.CER cer = new adminCER.CER(cerNumber);
            cer.CERType = "C";
            cer.ActionExplaination = "open: investigate injury/medical complaint";
            cer.StatusCs = "Opened";
            cer.StatusQa = "Opened";
            cer.StatusMed = "Assigned";
            cer.MedicalCondition = "n/a";
            cer.Analyze = true;
            cer.ComplaintDeterminationBy = FM2015.Models.User.GetUserName();
            cer.Update();
            return RedirectToAction("Details", "mvcCER", new { CERNumber = cerNumber });
        }

        public ActionResult Print()
        {
            adminCER.CER cer = new adminCER.CER();
            List<adminCER.CER> cerList = adminCER.CER.GetCERList(complaintStat.All, medStat.All, openStat.UnOpened, DateTime.Now.AddYears(-2), DateTime.Now);
            ViewBag.RecordCount = cerList.Count.ToString();
            return View("Index", cerList);
        }

        public ActionResult RedirectToLegacy(string cerNumber)
        {
            //redirect to old view...
            return Redirect("~/CER/EditCER.aspx?CERNumber=" + cerNumber + "&pageaction=lookup");
        }
    }
}