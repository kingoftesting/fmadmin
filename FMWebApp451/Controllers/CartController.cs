﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models;
using AdminCart;

namespace FM2015.Controllers
{
    public class CartController : Controller
    {
        // GET: View Cart
        public ActionResult Index()
        {

            Cart cart = new Cart();
            if (HttpContext.Session["cart"] == null)
            {
                HttpContext.Session["cart"] = cart;
            }
            else
            {
                cart = (Cart)HttpContext.Session["cart"]; 
            }
            cart.CalculateTotals();
            return View(cart);
        }

        // POST: AddToCart
        [HttpPost]
        public JsonResult AddProductTo_cart(int id)
        {

            Cart cart = new Cart();
            if (HttpContext.Session["cart"] == null)
            {
                HttpContext.Session["cart"] = cart;
            }
            else
            {
                cart = (Cart)HttpContext.Session["cart"];
            }
            if (id <= 0)
            {
                Response.Redirect("http://www.phrack.com?From=DumbHacker&Response=GoAway");
            }
            Item item = new Item(id, 1);
            cart.Items.AddItem(item);
            HttpContext.Session["cart"] = cart;
            //Response.Redirect("/cart/view");
            return Json(new
            {
                redirectUrl = Url.Action("View", "Cart"),
                isRedirect = true
            });
        }

        // POST: DeleteProductFromCart
        [HttpPost]
        public JsonResult DeleteProductFrom_cart(int id)
        {

            Cart cart = new Cart();
            if (HttpContext.Session["cart"] == null)
            {
                HttpContext.Session["cart"] = cart;
            }
            else
            {
                cart = (Cart)HttpContext.Session["cart"];
            }
            if (id <= 0)
            {
                Response.Redirect("http://www.phrack.com?From=DumbHacker&Response=GoAway");
            }
            Item item = new Item(id, 1);
            cart.Items.DeleteItem(item);
            cart.CalculateTotals();
            HttpContext.Session["cart"] = cart;
            return Json(new { success = "True", message = "" }, JsonRequestBehavior.AllowGet);
        }

        // POST: UpdateproductQuantity
        [HttpPost]
        public JsonResult UpdateproductQuantity_cart(int id)
        {

            Cart cart = new Cart();
            if (HttpContext.Session["cart"] == null)
            {
                HttpContext.Session["cart"] = cart;
            }
            else
            {
                cart = (Cart)HttpContext.Session["cart"];
            }
            if (id <= 0)
            {
                Response.Redirect("http://www.phrack.com?From=DumbHacker&Response=GoAway");
            }

            int quantity = 0;
            if (Int32.TryParse(Request["quantity"], out quantity)) { ; }
            if (quantity < 0) { quantity = 0; }

            foreach (Item i in cart.Items)
            {
                if (i.LineItemProduct.ProductID == id)
                {
                    i.Quantity = quantity;
                    //i.LineItemTotal = i.Quantity * (i.LineItemProduct.SalePrice - i.LineItemAdjustment);
                    i.LineItemTotal = i.Quantity * (i.LineItemProduct.Price - i.LineItemDiscount);
                    cart.CalculateTotals();
                    HttpContext.Session["cart"] = cart;
                    return Json(new { success = "True", message = "" }, JsonRequestBehavior.AllowGet);
                }
                HttpContext.Session["cart"] = cart;
            }
            return Json(new { success = "False", message = "" }, JsonRequestBehavior.AllowGet); }

        // GET: CheckOut Cart
        public ActionResult CheckOut_cart()
        {

            Cart cart = new Cart();
            if (HttpContext.Session["cart"] == null)
            {
                HttpContext.Session["cart"] = cart;
                 return Redirect("/cart/view");
            }
            else
            {
                cart = (Cart)HttpContext.Session["cart"];
            }
            //if (cart.SiteCustomer.CustomerID <= 0) { cart = GetCustomerSignIn(cart); }
            cart.CalculateTotals();
            return View();
        }

    }
}