﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using FM2015.Models;
using FM2015.Helpers;
using AdminCart;
using Stripe;
using System.Configuration;
using FMWebApp.ViewModels.PayWhirl;
using PayWhirl;

namespace FM2015.ViewModels
{
    public class CustomersController : Controller
    {
         
        // GET: Customers
        public ActionResult Index(string paramCustomer)
        {
            
            int cID = 0;
            string email = "";
            string lastName = "";
            string firstName = "";

            if (!string.IsNullOrEmpty(paramCustomer))
            {
                if (!Int32.TryParse(paramCustomer, out cID))
                {
                    if (paramCustomer.Contains("@"))
                    {
                        email = paramCustomer; //looks like an email address
                    }
                    else
                    {
                        string[] name = paramCustomer.Split(' ');
                        if (name.Length > 0) { lastName = name[0]; }
                        if (name.Length > 1) { firstName = name[1]; }
                    }
                }
            }

            List<CustomerViewModel> cvmList = Customer.GetCvmList(firstName, lastName, email, cID);
            if (cvmList.Count == 1)
            {
                cID = cvmList[0].Customer.CustomerID;
                return RedirectToAction("Details", "Customers", new { id =cID.ToString() });
            }
            return View(cvmList);
        }

        // GET: Customers/Details/5 where 5 = customerID = customerViewModelID
        public ActionResult Details(int id)
        {
            if (id == 0)
            {
                return HttpNotFound();
            } 
            
            Customer customer = new Customer(id);
            CustomerViewModel cvm = new CustomerViewModel(customer);
            if (cvm.Customer.CustomerID == 0)
            {
                return HttpNotFound();
            }
            if (Subscriptions.SubscriberStatus(customer.Email) == "unpaid")
            {
                ViewBag.deadBeat = true;
                ViewBag.AmountDue = 0;
                ViewBag.StripeCustomerId = "";
                ViewBag.StripeSubscriptionId = "";

                //Stripe.StripeInvoice invoice = Subscriptions.GetLastInvoice(ovm.cart.SiteCustomer.Email);
                //decimal accountBalance = Subscriptions.GetAccountBalance(ovm.cart.SiteCustomer.Email);
                StripeCustomer stripeCustomer = Subscriptions.GetStripeCustomerFromEmail(customer.Email);
                StripeInvoice stripeInvoice = new StripeInvoice();
                stripeInvoice = Subscriptions.GetLastInvoice(stripeCustomer.Id);

                if (stripeInvoice != null)
                {
                    if (stripeInvoice.AmountDue > 0)
                    {
                        decimal balance = Convert.ToDecimal(stripeInvoice.AmountDue) / 100;
                        ViewBag.AmountDue = balance.ToString("C");
                        ViewBag.StripeCustomerId = stripeCustomer.Id;
                    }
                }
            }
            else
            {
                ViewBag.deadBeat = false;
            }
            return View(cvm);
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            CustomerViewModel cvm = new CustomerViewModel();
            cvm.ShipAddress.Street = "n/a";
            cvm.ShipAddress.City = "n/a";
            cvm.ShipAddress.Zip = "00000";
            cvm.Customer.Password = Config.AppCode() + "123456";
            cvm.Customer.ConfirmPassword = cvm.Customer.Password;
            return View(cvm);
        }

        // POST: Customers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerViewModel cvm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    cvm.Customer.Email = cvm.Customer.Email.TrimStart().TrimEnd();
                    cvm.Customer.Phone = cvm.Customer.Phone.TrimStart().TrimEnd();

                    string billState = Request["txtBillState"];
                    string shipState = Request["txtShipState"];
                    cvm = FM2015.Helpers.Helpers.UpdateStateCountrySelectLists(cvm, billState, shipState);

                    cvm.BillAddress.Zip = cvm.BillAddress.Zip.TrimStart().TrimEnd();
                    cvm.ShipAddress.Zip = cvm.ShipAddress.Zip.TrimStart().TrimEnd();
                    string chkZip = DBUtil.CheckZipCode(cvm.SelectBillCountryCode, cvm.BillAddress.Zip);
                    if (chkZip != "")
                    {
                        ViewBag.ErrMsg = "Unacceptable Billing Address Zip: " + chkZip;
                        return View(cvm);
                    }

                    if ((cvm.ShipAddress.Street.ToUpper() != cvm.BillAddress.Street.ToUpper()) && ((cvm.ShipAddress.Street.ToUpper() == "N/A")))
                    {
                        cvm.ShipAddress.Street = cvm.BillAddress.Street;
                        cvm.ShipAddress.Street2 = cvm.BillAddress.Street2;
                        cvm.ShipAddress.City = cvm.BillAddress.City;
                        cvm.ShipAddress.State = cvm.BillAddress.State;
                        cvm.ShipAddress.Zip = cvm.BillAddress.Zip;
                        cvm.ShipAddress.Country = cvm.BillAddress.Country;
                    }

                    if ((cvm.ShipAddress.City.ToUpper() != cvm.BillAddress.City.ToUpper()) && ((cvm.ShipAddress.City.ToUpper() == "N/A")))
                    {
                        cvm.ShipAddress.City = cvm.BillAddress.City;
                    }

                    chkZip = DBUtil.CheckZipCode(cvm.SelectShipCountryCode, cvm.ShipAddress.Zip);
                    if (chkZip != "")
                    {
                        ViewBag.ErrMsg = "Unacceptable Shipping Address Zip: " + chkZip;
                        return View(cvm);
                    }

                    if (string.IsNullOrEmpty(cvm.Customer.Phone))
                    {
                        ViewBag.ErrMsg = "Phone # can't be empty; if none is available use n/a";
                        return View(cvm);
                    }

                    int result = Customer.CreateNewCustomer(cvm.Customer, cvm.BillAddress, cvm.ShipAddress);
                    if (result <= 0)
                    {
                        ViewBag.ErrMsg = "Create New Customer Failed. CreatNewCustomer ID Result: " + result.ToString();
                        return View(cvm);
                    }
                    else
                    {
                        cvm.Customer = new Customer(result);
                        if (cvm.Customer.CustomerID > 0) //if a valid customer id then update the Shopify side
                        {
                            long PartnerID = ShopifyCustomer.UpdateShopifyCustomer(cvm.Customer);
                            if ((cvm.Customer.PartnerID == 0) && PartnerID > 0) { Customer.UpDatePartnerID(cvm.Customer.CustomerID, PartnerID); }
                        }
                    }

                    return RedirectToAction("Details", "Customers", new { id = cvm.Customer.CustomerID.ToString() } );
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrMsg = "Create New Customer Failed." + ex.Message;
                return View(cvm);
            }

            ViewBag.ErrMsg = "Create New Customer Failed.";
            return View(cvm);
        }

        // GET: Customers/CreateFromPayWhirl
        public ActionResult CreateFromPayWhirl(string paramCustomer)
        {
            if (string.IsNullOrEmpty(paramCustomer))
            {
                ViewBag.ErrMsg = "Invalid email address: NULL or Empty";
                return View();
            }

            string email = paramCustomer;
            if (!email.Contains("@"))
            {
                ViewBag.ErrMsg = "Invalid email address: " + email.ToString();
                return View();
            }

            email = email.TrimStart().TrimEnd();
            Customer c = Customer.FindCustomerByEmail(email);
            if (c.CustomerID != 0)
            {
                ViewBag.ErrMsg = "Customer already exists: " + email.ToString();
                return View();
            }

            PayWhirlCustomerViewModel pwvm = new PayWhirlCustomerViewModel();
            //pwvm.payWhirlCustomer = PayWhirlSubscriber.GetSubscriber(id);
            pwvm.payWhirlCustomer = PayWhirlBiz.FindSubscriberByEmail(email);
            if (pwvm.payWhirlCustomer.Id == 0)
            {
                ViewBag.ErrMsg = "PayWhirl could not find customer account from email: " + email.ToString();
                return View();
            }

            if (email.ToUpper() != pwvm.payWhirlCustomer.Email.TrimStart().TrimEnd().ToUpper())
            {
                ViewBag.ErrMsg = "PayWhirl email does not match email: " + "PayWhirl: " + pwvm.payWhirlCustomer.Email + " vs. " + email.ToString();
                return View();
            }

            if (string.IsNullOrEmpty(pwvm.payWhirlCustomer.Phone))
            {
                pwvm.payWhirlCustomer.Phone = "n/a";
            }

            //valid PayWhirl customer details have been found

            c = new Customer();
            Address a = new Address();

            c.FirstName = pwvm.payWhirlCustomer.First_name;
            c.LastName = pwvm.payWhirlCustomer.Last_name;
            c.Email = pwvm.payWhirlCustomer.Email;
            c.Phone = pwvm.payWhirlCustomer.Phone;
            c.Password = "PayWhirl";
            c.FaceMasterNewsletter = true;

            a.Street = pwvm.payWhirlCustomer.Address_1;
            a.Street2 = pwvm.payWhirlCustomer.Address_2;
            a.City = pwvm.payWhirlCustomer.City;
            a.State = pwvm.payWhirlCustomer.State;
            a.Country = pwvm.payWhirlCustomer.Country;
            a.Zip = pwvm.payWhirlCustomer.Zip;

            int result = Customer.CreateNewCustomer(c, a, a);
            //int result = 152; //this is here as a test
            c.CustomerID = result;

            if (result <= 0)
            {
                ViewBag.ErrMsg = "Create New Customer Failed. CreatNewCustomer ID Result: " + result.ToString();
                return View();
            }

            return RedirectToAction("Details", "Customers", new { id = c.CustomerID.ToString() });
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(int id)
        {
            Customer c = new Customer(id);
            CustomerViewModel cvm = new CustomerViewModel(c);
            cvm = FM2015.Helpers.Helpers.UpdateStateCountrySelectLists(cvm, cvm.BillAddress.State, cvm.ShipAddress.State);

            if (cvm == null)
            {
                return HttpNotFound();
            }

            cvm.Customer.ConfirmPassword = cvm.Customer.Password;
            return View(cvm);
        }

        // POST: Customers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, CustomerViewModel cvm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    cvm.Customer.CustomerID = id;
                    string billState = Request["txtBillState"];
                    string shipState = Request["txtShipState"];
                    cvm = FM2015.Helpers.Helpers.UpdateStateCountrySelectLists(cvm, billState, shipState);

                    string chkZip = DBUtil.CheckZipCode(cvm.SelectBillCountryCode, cvm.BillAddress.Zip);
                    if (chkZip != "")
                    {
                        ViewBag.ErrMsg = "Bill Adr: " + chkZip;
                        return View(cvm);
                    }

                    if ((cvm.ShipAddress.Street.ToUpper() != cvm.BillAddress.Street.ToUpper()) && ((cvm.ShipAddress.Street.ToUpper() == "N/A")))
                    {
                        ViewBag.ErrMsg = "Unacceptable Shipping Street: " + cvm.ShipAddress.Street;
                        return View(cvm);
                    }

                    if ((cvm.ShipAddress.City.ToUpper() != cvm.BillAddress.City.ToUpper()) && ((cvm.ShipAddress.City.ToUpper() == "N/A")))
                    {
                        ViewBag.ErrMsg = "Unacceptable Shipping City: " + cvm.ShipAddress.City;
                        return View(cvm);
                    }

                    chkZip = DBUtil.CheckZipCode(cvm.SelectShipCountryCode, cvm.ShipAddress.Zip);
                    if (chkZip != "")
                    {
                        ViewBag.ErrMsg = "Unacceptable Shipping Address Zip: " + chkZip;
                        return View(cvm);
                    }

                    if (string.IsNullOrEmpty(cvm.Customer.Phone))
                    {
                        ViewBag.ErrMsg = "Phone # can't be empty; if no phone# is available use n/a";
                        return View(cvm);
                    }
                    cvm.Customer.Phone.TrimStart().TrimEnd();
                    if (string.IsNullOrEmpty(cvm.Customer.Phone))
                    {
                        ViewBag.ErrMsg = "Phone # can't be empty or spaces; if no phone# is available use n/a";
                        return View(cvm);
                    }

                    Customer cTemp = new Customer(id);
                    if (cTemp.Email != cvm.Customer.Email) //customer must be updating their email address
                    {
                        Customer cTemp2 = Customer.FindCustomerByEmail(cvm.Customer.Email);
                        if (cTemp2.CustomerID != 0) //new email address not already in use
                        {
                            ViewBag.ErrMsg = "Email address: " + cvm.Customer.Email + " is already in use; try another";
                            return View(cvm);
                        }
                    }

                    cvm.Customer.SaveCustomerToDB();
                    cvm.BillAddress.Save(cvm.Customer, 1);
                    cvm.ShipAddress.Save(cvm.Customer, 2);

                    return RedirectToAction("Details", "Customers", new { id = cvm.Customer.CustomerID.ToString() } );
                }
                return View(cvm);

            }
            catch
            {
                ViewBag.ErrMsg = "Some unknown error occured - please try again and/or alert Rodger";
                return View(cvm);
            }
        }

        // GET: Customers/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Customers/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public JsonResult ddlStateList(string id)
        {
            string i = Request.Url.PathAndQuery;
            CustomerViewModel cvm = new CustomerViewModel();
            List<Location> lList = Location.Countries();
            Location l = lList.Find(x => x.Code == id);

            //could be either a bill or ship address that's requesting; doesn't really matter for the result
            cvm.BillStateSelectList = Location.GetStateSelectList(l.ID); 

            return Json(cvm.BillStateSelectList, JsonRequestBehavior.AllowGet);
        }

        // GET: CustomerViewModel/UnSubscribe/5?subscriptionID = id
        public ActionResult UnSubscribe(int id, string stripeCustomerID, string stripeSubscriptionID)
        {
            ViewBag.ErrMsg = "";
            if (FM2015.Models.Subscriptions.Stripe_UnSubscribe(stripeCustomerID, stripeSubscriptionID) != 0)
            {
                ViewBag.ErrMsg = Session["Stripe_UnSubscribe_Msg"].ToString();
            }
            FMWebApp.ViewModels.Stripe.StripeCustomerViewModel cvm = new FMWebApp.ViewModels.Stripe.StripeCustomerViewModel(stripeCustomerID, stripeSubscriptionID);
            return View(cvm);
        }

        public ActionResult DeleteCard(string id, string stripeCustomerID, int customerID)
        {
            var cardService = new StripeCardService();
            try
            {
                cardService.Delete(stripeCustomerID, id); // optional isRecipient
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "ManageSubscriptions: DeleteCard: Stripe Error");
            }
            return RedirectToAction("Details", "Customers", new { id = customerID });
        }

        public ActionResult EditCard(string id, string stripeCustomerID, int customerID)
        {
            var cardService = new StripeCardService();
            try
            {
                //cardService.Delete(stripeCustomerID, id); // optional isRecipient
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Edit Card: Stripe Error");
            }
            return RedirectToAction("Details", "Customers", new { id = customerID });
        }

        // GET: Add Card in Stripe
        public ActionResult AddCard(string stripeCustomerID, int customerID)
        {
            FMWebApp451.ViewModels.Stripe.CreditCardViewModel ccvm = new FMWebApp451.ViewModels.Stripe.CreditCardViewModel();
            ccvm.customer = new Customer(customerID);
            ccvm.StripeCustomerID = stripeCustomerID;
            ccvm.CreditCardViewModelKey = customerID;
            return View(ccvm);
        }

        // POST:  Add Card in Stripe
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCard(FMWebApp451.ViewModels.Stripe.CreditCardViewModel ccvm)
        {
            if (ModelState.IsValid)
            {

                int customerID = ccvm.CreditCardViewModelKey;
                Customer c = new Customer(customerID);
                Address a = Address.LoadAddress(c.CustomerID, 1);
                StripeCustomer stripeCustomer = new StripeCustomer();
                ccvm.customer = c;

                //get a credit card token for this subscription purchase
                var myCard = new StripeCardCreateOptions(); //create a token from the customers card
                myCard.SourceCard = new SourceCard(); //*****
                myCard.SourceCard.Name = ccvm.Name;
                myCard.SourceCard.Number = ccvm.CardNumber;
                myCard.SourceCard.ExpirationYear = Convert.ToInt32(ccvm.ExpirationYear);
                myCard.SourceCard.ExpirationMonth = Convert.ToInt32(ccvm.ExpirationMonth);
                myCard.SourceCard.Cvc = ccvm.Cvc;
                myCard.SourceCard.AddressLine1 = a.Street;
                myCard.SourceCard.AddressCity = a.City;
                myCard.SourceCard.AddressState = a.State;
                myCard.SourceCard.AddressCountry = a.Country;
                myCard.SourceCard.AddressZip = a.Zip;

                var cardService = new StripeCardService();
                StripeCard stripeCard = new StripeCard();
                try
                {
                    stripeCard = cardService.Create(ccvm.StripeCustomerID, myCard);
                }
                catch (StripeException ex)
                {
                    ViewBag.ErrMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Add Card: Stripe Error");
                    return View(ccvm);
                }

                //begin creating CER
                adminCER.CER cer = new adminCER.CER(c);
                cer.EventDescription = "Added new credit card to Stripe customer account";

                var customerService = new StripeCustomerService();

                try
                {
                    stripeCustomer = customerService.Get(ccvm.StripeCustomerID);
                }
                catch (StripeException ex)
                {
                    ViewBag.ErrMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Add Card: Stripe Error");
                    return View(ccvm);
                }

                var myCustomer = new StripeCustomerUpdateOptions();
                myCustomer.DefaultSource = stripeCard.Id;

                try
                {
                    stripeCustomer = customerService.Update(ccvm.StripeCustomerID, myCustomer);
                    ViewBag.ErrMsg = "New Default Card Successfully Added!";
                }
                catch (StripeException ex)
                {
                    ViewBag.ErrMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Add Card: Stripe Error");
                    return View(ccvm);
                }
                cer.EventDescription += " Successfully made new card the default card";
                int result = cer.Add();
            }
            else
            {
                ViewBag.ErrMsg = "information Missing: Please Update!";
            }
            
            return View(ccvm);
        }

        // GET: CustomerViewModel/NewOrder  // add an order to a an existing customer
        public ActionResult CreateOrder(int id)
        {
            AdminCreateOrderViewModel acovm = new AdminCreateOrderViewModel
            {
                CustomerId = id,
                OrderDate = DateTime.Now,
                TransactionDate = DateTime.Now,
            };

            Customer customer = new Customer(id);
            ViewBag.Name = customer.FirstName + " " + customer.LastName;
            ViewBag.CustomerID = customer.CustomerID.ToString();

            return View(model: acovm);
        }

        // POST:  CustomerViewModel/NewOrder  // add an order to a an existing customer
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOrder(AdminCreateOrderViewModel acovm)
        {
            if (ModelState.IsValid)
            {
                Customer customer = new Customer(acovm.CustomerId);
                Address shipAddress = Address.LoadAddress(acovm.CustomerId, 2);
                Address billAddress = Address.LoadAddress(acovm.CustomerId, 1);
                List<Item> itemList = GenItemList(acovm.ProductIDQuantityPairs);

                Order order = new Order()
                {
                    OrderID = 0,
                    CustomerID = acovm.CustomerId,
                    OrderDate = acovm.OrderDate,
                    ShippingAddressID = shipAddress.AddressId,
                    BillingAddressID = billAddress.AddressId,
                    Total = acovm.Total,
                    Adjust = acovm.Adjust,
                    TotalTax = acovm.TotalTax,
                    TotalShipping = acovm.TotalShipping,
                    SourceID = Models.User.GetUserID(),
                    CompanyID = Config.CompanyID(),
                };
                order.OrderID = order.SaveDB(order.OrderID);

                foreach (Item item in itemList)
                {
                    OrderDetail orderDetail = new OrderDetail()
                    {
                        OrderDetailID = 0,
                        OrderDate = DateTime.Now,
                        OrderID = order.OrderID,
                        ProductID = item.LineItemProduct.ProductID,
                        UnitPrice = item.LineItemProduct.SalePrice,
                        Discount = 0,
                        Quantity = item.Quantity,
                        UnitCost = item.LineItemProduct.Cost,
                    };
                    orderDetail.SaveToOrderDetail();
                }

                Transaction transaction = new Transaction()
                {
                    TransactionRowID = 0,
                    OrderID = order.OrderID,
                    TransactionID = acovm.TransactionId,
                    TransactionDate = acovm.TransactionDate,
                    TrxType = "S",
                    CardType = acovm.CardType,
                    CardNo = "**** " + acovm.CardLast4,
                    ExpDate = acovm.CardExp,
                    TransactionAmount = order.Total,
                    ResultCode = 0,
                    ResultMsg = "Approved(Stripe/RM Manual)",
                    Comment1 = "",
                    Comment2 = order.OrderID.ToString(),
                    OrigTranID = "",
                    AuthCode = "0",
                    CardHolder = customer.FirstName + " " + customer.LastName,
                    Address = billAddress,
                    CustomerCode = "FM"+order.OrderID.ToString(),
                    Iavs = "N",
                    AvsAddr = "Y",
                    AvsZip = "Y",
                };
                transaction.SaveDB();

                OrderAction orderAction = new OrderAction()
                {
                    OrderActionID = 0,
                    OrderID = order.OrderID,
                    OrderActionBy = Models.User.GetUserName(),
                    OrderActionDate = DateTime.Now,
                    OrderActionReason = "new Order, entered manually by Admin",
                    OrderActionType = 2,
                };
                orderAction.SaveDB();

                OrderNote orderNote = new OrderNote()
                {
                    OrderNoteID = 0,
                    OrderNoteBy = Models.User.GetUserName(),
                    OrderNoteDate = DateTime.Now,
                    OrderNoteText = "order was not generated by PayWhirl/Stripe/Shopify/Admin; had to enter the order manually",
                    OrderID = order.OrderID,
                };
                orderNote.Save(orderNote.OrderNoteID);
            }
            else
            {
                ViewBag.ErrMsg = "information Missing: Please Update!";
            }

            return RedirectToAction("Details", "Customers", new { id = acovm.CustomerId });
        }

        private List<Item> GenItemList(string productIDQuantityPairs)
        {
            List<Item> itemList = new List<Item>();
            string[] itemPairs = productIDQuantityPairs.Split(';');
            foreach (string itemPair in itemPairs)
            {
                string[] itemPairSpecs = itemPair.Split(',');
                if ((itemPairSpecs != null) && (itemPairSpecs.Length == 2))
                {
                    Item item = new Item();
                    Product p = new Product(Convert.ToInt32(itemPairSpecs[0]));
                    ProductCost pc = new ProductCost(p.ProductID);
                    item.LineItemProduct = p;
                    item.LineItemProduct.Cost = pc.Cost;
                    item.Quantity = Convert.ToInt32(itemPairSpecs[1]);
                    itemList.Add(item);
                }
            }
            return itemList;
        }
    }

}
