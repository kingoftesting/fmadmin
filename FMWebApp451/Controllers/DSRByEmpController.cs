﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AdminCart;
using FM2015.Models;
using FM2015.ViewModels;
using FMWebApp451.Interfaces;

namespace FM2015.Controllers
{
    public class DSRByEmpController : Controller
    {
        private readonly IOrder orderProvider;
        private readonly IOrderDetail orderDetailProvider;
        private readonly IProductProvider productProvider;
        private readonly ITransaction transactionProvider;
        private readonly ISalesDetails salesDetailsProvider;
        private readonly IMultiPayRevenueByOrder mpRevenueByOrderProvider;
        private readonly IMultiPayTransactionsByDate mpTransactionsByDateProvider;

        public DSRByEmpController(IOrder orderProvider, IOrderDetail orderDetailProvider, IProductProvider productProvider,
            ITransaction transactionProvider, ISalesDetails salesDetailsProvider, IMultiPayRevenueByOrder mpRevenueByOrderProvider,
            IMultiPayTransactionsByDate mpTransactionsByDateProvider)
        {
            this.orderProvider = orderProvider;
            this.orderDetailProvider = orderDetailProvider;
            this.productProvider = productProvider;
            this.transactionProvider = transactionProvider;
            this.salesDetailsProvider = salesDetailsProvider;
            this.mpRevenueByOrderProvider = mpRevenueByOrderProvider;
            this.mpTransactionsByDateProvider = mpTransactionsByDateProvider;
        }

        // GET: DSRByEmployee
        public ActionResult Index(string SelectMonth, string SelectYear)
        {
            DailySalesByPerson dailySales = new DailySalesByPerson();
            int theMonth = 0;
            int theYear = 0;
            string parseResult = GetMonthYear(SelectMonth, SelectYear, out theMonth, out theYear);
            if (!string.IsNullOrEmpty(parseResult))
            {
                ViewBag.ErrMsg = parseResult;
                return View(dailySales);
            }

            dailySales.sales = GetDailySales(theMonth, theYear);
            return View(dailySales);
        }

        /********** SUPPORT METHODS **********/

        public string GetMonthYear(string SelectMonth, string SelectYear, out int theMonth, out int theYear)
        {
            theMonth = 0;
            theYear = 0;

            if (string.IsNullOrEmpty(SelectYear)) { theYear = DateTime.Today.Year; }
            if (string.IsNullOrEmpty(SelectMonth)) { theMonth = DateTime.Today.Month; }
            if (!int.TryParse(SelectYear, out theYear)) { theYear = DateTime.Today.Year; }
            if (!int.TryParse(SelectMonth, out theMonth)) { theMonth = DateTime.Today.Month; }

            return string.Empty;
        }

        public List<DayOfWeekSales> GetDailySales(int theMonth, int theYear)
        {
            List<DayOfWeekSales> salesList = new List<DayOfWeekSales>();

            // get all sales in date range
            DateTime startDate = new DateTime(theYear, theMonth, 1);
            DateTime endDate = startDate.AddMonths(1).AddTicks(-1);
            List<Order> orderList = orderProvider.GetByDate(startDate, endDate);
            List<OrderDetail> orderDetailList = orderDetailProvider.GetByDate(startDate, endDate);
            List<Product> pList = productProvider.GetAll();

            for (int day = 1; day <= DateTime.DaysInMonth(startDate.Year, startDate.Month); day++)
            {
                DayOfWeekSales dailySales = new DayOfWeekSales();
                DateTime date = new DateTime(theYear, theMonth, day);
                dailySales.Date = date;
                decimal gcRevenue = 0;

                //get all the order for each day of the month
                var dayList = from o in orderList
                              where (o.OrderDate.Day == day)
                              select o;

                dailySales.TotalRevenue = (from o in dayList select (o.SubTotal- o.TotalCoupon- o.Adjust)).Sum();

                List<OrderDetail> odList = (from od in orderDetailList where (od.OrderID == (from o in dayList select o.OrderID).FirstOrDefault()) select od).ToList();

                //account for gift card sales - subtract from revenue
                foreach (OrderDetail od in odList)
                {
                    Product p = (from p2 in pList where p2.ProductID == od.ProductID select p2).FirstOrDefault();
                    if (p.IsMultiPay)
                    {

                    }
                    else
                    {

                    }
                    if (p.IsGiftCard)
                    {
                        gcRevenue = od.Quantity * od.UnitPrice;
                    }
                }

                dailySales.TotalRevenue = dailySales.TotalRevenue - gcRevenue;

                dailySales.PhoneRevenue = (from o in dayList where (o.ByPhone == true) select o.SubTotal).Sum() -
                    (from o in dayList where (o.ByPhone == true) select o.TotalCoupon).Sum() -
                    (from o in dayList where (o.ByPhone == true) select o.Adjust).Sum();

                dailySales.PhoneRevenue = dailySales.PhoneRevenue - gcRevenue;

                dailySales.WebRevenue = dailySales.TotalRevenue - dailySales.PhoneRevenue;

                var byPhoneList = (from o in dayList
                                   where (o.ByPhone == true)
                                   group o by o.SourceID into newList
                                   orderby newList.Key
                                   select newList).ToList();

                foreach (var group in byPhoneList)
                {
                    SalesMan salesman = new SalesMan();
                    foreach (var item in group)
                    {
                        if (!dailySales.SalesTeamList.Exists(x => x.user.UserID == item.SourceID))
                        {
                            salesman.user.UserID = item.SourceID;
                            dailySales.SalesTeamList.Add(salesman);
                        }

                        int index = dailySales.SalesTeamList.FindIndex(x => x.user.UserID == item.SourceID);
                        decimal revenue = item.SubTotal - item.TotalCoupon - item.Adjust;
                        salesman.Sales = salesman.Sales + revenue;
                        dailySales.SalesTeamList[index].Sales = salesman.Sales;
                    }
                }
                salesList.Add(dailySales);
            }

            return salesList;
        }
    }
}