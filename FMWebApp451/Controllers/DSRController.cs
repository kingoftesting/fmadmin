﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AdminCart;
using FM2015.Helpers;
using FM2015.Models;
using FM2015.ViewModels;
using FMWebApp451.Interfaces;

namespace FM2015.Controllers
{
    public class DSRController : Controller
    { 
        private readonly IOrder orderProvider;
        private readonly IOrderDetail orderDetailProvider;
        private readonly IProductProvider productProvider;
        private readonly ITransaction transactionProvider;
        private readonly ISalesDetails salesDetailsProvider;
        private readonly ISubscriptions subscriptionProvider;
        private readonly ISubscriptionPlans subscriptionPlanProvider;
        private readonly IMultiPayRevenueByOrder mpRevenueByOrderProvider;
        private readonly IMultiPayTransactionsByDate mpTransactionsByDateProvider;

        public DSRController(IOrder orderProvider, IOrderDetail orderDetailProvider, IProductProvider productProvider, 
            ITransaction transactionProvider, ISalesDetails salesDetailsProvider, ISubscriptions subscriptionProvider, ISubscriptionPlans subscriptionPlanProvider,
            IMultiPayRevenueByOrder mpRevenueByOrderProvider, IMultiPayTransactionsByDate mpTransactionsByDateProvider)
        {
            this.orderProvider = orderProvider;
            this.orderDetailProvider = orderDetailProvider;
            this.productProvider = productProvider;
            this.transactionProvider = transactionProvider;
            this.salesDetailsProvider = salesDetailsProvider;
            this.subscriptionProvider = subscriptionProvider;
            this.subscriptionPlanProvider = subscriptionPlanProvider;
            this.mpRevenueByOrderProvider = mpRevenueByOrderProvider;
            this.mpTransactionsByDateProvider = mpTransactionsByDateProvider;
        }

        // GET: DSR
        public ActionResult Index(string param)
        {
           SalesViewModel SalesVM = new SalesViewModel();

            DateTime endDateTmp = DateTime.Now;
            DateTime startDateTmp = DateTime.Today;
            string startStr = "";
            string endStr = "";
            string errMsg = "";

            errMsg = IndexParamParser(param, out startStr, out endStr);
            if (!String.IsNullOrEmpty(errMsg))
            {
                ViewBag.ErrMsg = errMsg;
                return View(SalesVM);
            }
            SalesVM.SalesPrimary = GetSalesStats(Convert.ToDateTime(startStr), Convert.ToDateTime(endStr));

            return View(SalesVM);
        }

        // GET: GrossMismtachList & display it
        public ActionResult GrossMisMatchDisplay(string startStr, string endStr)
        {
            SalesViewModel SalesVM = new SalesViewModel();
            //CacheHelper.ClearAll(); //make sure you get all orders and not just those cached
            SalesVM.SalesPrimary = GetSalesStats(Convert.ToDateTime(startStr), Convert.ToDateTime(endStr));
            return View(SalesVM.SalesPrimary.GrossMismatchList);
        }

        // GET: LineItemDiscountList & display it
        public ActionResult LineItemDiscountDisplay(string startStr, string endStr)
        {
            SalesViewModel SalesVM = new SalesViewModel();
            //CacheHelper.ClearAll(); //make sure you get all orders and not just those cached
            SalesVM.SalesPrimary = GetSalesStats(Convert.ToDateTime(startStr), Convert.ToDateTime(endStr));
            return View(SalesVM.SalesPrimary.liDiscountList);
        }

        /********** SUPPORT METHODS **********/

        public string IndexParamParser(string param, out string startStr, out string endStr)
        {
            DateTime startDateTmp = DateTime.Today.Date;
            DateTime endDateTmp = startDateTmp;
            startStr = startDateTmp.ToString();
            endStr = string.Empty;

            if (!string.IsNullOrEmpty(param))
            {
                if (!param.Contains("..."))
                {
                    string[] dateArray = param.Split(' '); // could be a start and end date range
                    if (dateArray.Length > 0)
                    {
                        startStr = dateArray[0];
                    }
                    if (dateArray.Length > 1)
                    {
                        endStr = dateArray[1];
                    }
                    else
                    {
                        switch (param.Trim().ToUpper())
                        {
                            case "Y":
                                {
                                    startStr = DateTime.Today.AddDays(-1).ToString();
                                    endStr = DateTime.Today.AddDays(-1).ToString();
                                }
                                break;

                            case "W":
                                {
                                    startStr = DateTime.Today.AddDays(-7).ToString();
                                    endStr = DateTime.Today.AddDays(-1).Subtract(DateTime.Today.TimeOfDay).ToString();
                                }
                                break;

                            case "M":
                                {
                                    DateTime tempDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                                    DateTime tempDate1 = tempDate.AddMonths(1).AddTicks(-1); //should be the end of the month
                                    startStr = tempDate.ToString();
                                    endStr = tempDate1.ToString();
                                }
                                break;

                            default:
                                break;

                        }
                    }

                    // check for a date range
                    if (DateTime.TryParse(startStr, out startDateTmp))
                    {
                        if (!string.IsNullOrEmpty(endStr)) //now check for an endDate
                        {
                            if (!DateTime.TryParse(endStr, out endDateTmp))
                            {
                                return "endDate must be a Date (like: 1/1/2015)";
                            }
                            if (endDateTmp.TimeOfDay.Ticks + 1 != 0)
                            {
                                endDateTmp = endDateTmp.Date.AddDays(1).AddTicks(-1);
                                //endStr = endDateTmp.AddDays(1).AddTicks(-1).ToString(); //should be just before midnight
                                endStr = endDateTmp.ToString();
                            }
                        }
                        else
                        {
                            endDateTmp = Convert.ToDateTime(startStr);
                            endStr = endDateTmp.Date.AddDays(1).AddTicks(-1).ToString();
                        }
                        startStr = startDateTmp.Date.ToString();
                        if (endDateTmp < startDateTmp)
                        {
                            return "End Date must be AFTER Start Date (date range error: end: " + endDateTmp.ToShortDateString() + "; start: " + startDateTmp.ToShortDateString() + ")";
                        }
                    }
                    else
                    {
                        return "startDate must be a Date (like: 1/1/2015)";
                    }
                }
            }
            else
            {
                endDateTmp = Convert.ToDateTime(startStr);
                endStr = endDateTmp.Date.AddDays(1).AddTicks(-1).ToString();
            }

            return string.Empty;
        }

        public Sales GetSalesStats(DateTime startDate, DateTime endDate)
        {
            Sales sales = new Sales();
            decimal sumSalesTransaction = 0;
            List<Product> productList = productProvider.GetAll();
            sales.TotalCost = 0;
            sales.TotalUnits = 0;
            sales.StartDate = startDate;
            sales.EndDate = endDate;

            //CacheHelper.ClearAll(); //make sure you get all orders and not just those cached

            Order order = new Order();
            //total Orders
            List<Order> orderList = orderProvider.GetByDate(startDate, endDate);
            sales.OrderList = orderList;
            sales.TotalOrders = (from o in orderList select o.OrderID).Count();

            //total items ordered
            List<OrderDetail> orderDetailList = orderDetailProvider.GetByDate(startDate, endDate);
            sales.OrderDetailList = orderDetailList;
            sales.TotalUnits = (from od in orderDetailList select od.Quantity).Sum();

            //get subscriptions
            List<Subscriptions> subscriptionList = subscriptionProvider.GetAll();
            List<SubscriptionPlans> subscriptionPlanList = subscriptionPlanProvider.GetAll();

            //total product-related stats generated by OrderDetails
            foreach (OrderDetail od in orderDetailList)
            {
                Product p = (from prod in productList where prod.ProductID == od.ProductID select prod).FirstOrDefault();

                if (p == null)
                {
                    continue;
                }
                sales.TotalLineItemDiscounts += (od.Quantity * od.Discount);
                sales.TotalCost += (od.UnitCost * od.Quantity);

                if (p.IsFMSystem)
                {
                    //total devices ordered
                    sales.TotalDevices += od.Quantity;
                }
                if (p.IsMultiPay)
                {
                    decimal mpCashDiscount = (from o in orderList where o.OrderID == od.OrderID select o.Adjust).FirstOrDefault();
                    Subscriptions sub = (from s in subscriptionList where s.OrderID == od.OrderID select s).FirstOrDefault();
                    if (sub != null)
                    {                       
                        sales.MpCashDiscounts += mpCashDiscount;
                        sales.TotalMPDevices += od.Quantity;
                        sales.Mp1PayRevenue += (od.Quantity * p.OnePayPrice);
                        sales.MpRevenue += (od.Quantity * od.UnitPrice) - mpCashDiscount;
                    }
                    if ((sub != null) && (sub.StripeStatus == "canceled"))
                    {
                        int? installments = (from s in subscriptionPlanList where s.PlanId == sub.StripePlanID select s.Installments).FirstOrDefault();
                        if (installments != null)
                        {
                            sales.MpCancelled = ((od.Quantity * od.UnitPrice) - mpCashDiscount) * (int)installments;
                        }
                    }
                }
                if (p.IsGiftCard)
                {
                    sales.TotalGiftCardRevenue = sales.TotalGiftCardRevenue + (od.Quantity * od.UnitPrice);
                }
                if (p.IsRefill)
                {
                    sales.TotalRefills += od.Quantity;
                }

            }

            //total transactions
            List<Transaction> tList = transactionProvider.GetAll(startDate, endDate);
            sales.TransactionList = tList;

            sumSalesTransaction = (from t in tList where (t.TrxType == "S" && t.ResultCode == 0) select t.TransactionAmount).Sum();
            sales.TotalOtherCS = (from t in tList where (t.TrxType == "SA" && t.ResultCode == 0) select t.TransactionAmount).Sum();
            sales.TotalRefunds = (from t in tList where (t.TrxType == "C" && t.ResultCode == 0) select t.TransactionAmount).Sum();
            sales.TotalVoids = (from t in tList where (t.TrxType == "V" && t.ResultCode == 0) select t.TransactionAmount).Sum();

            sales.SalesDetailsList = salesDetailsProvider.GetAll(startDate, endDate); //gets all order details where IsMultiPay = true

            List<FMWebApp451.ViewModels.MultiPayRevenueByOrder> mpList = mpRevenueByOrderProvider.GetAll(startDate, endDate);
            sales.mpRevenueByOrder = mpList;

            //remaining stats...

            sales.tstTotalLineItemDiscounts = (from od in orderDetailList select (od.Quantity * od.Discount)).Sum();
            sales.liDiscountList = (from od in orderDetailList where (od.Discount != 0) select od).ToList();
            if (sales.liDiscountList == null)
            {
                sales.liDiscountList = new List<OrderDetail>();
            }

            sales.tstTotalGrossRevenue = (from od in orderDetailList select ((od.UnitPrice - od.Discount) * od.Quantity)).Sum() - sales.TotalGiftCardRevenue; //- sales.MpCashDiscounts 

            sales.MpDiscounts = (from l in mpList select l.PromoCodeDiscount).Sum();
            sales.MpNetRevenue = sales.Mp1PayRevenue - sales.MpDiscounts;
            sales.MpCollected = (from l in mpList select l.Collected).Sum();
            //sales.MpCancelled = (from l in mpList select l.Cancelled).Sum();
            sales.MpUnCollectable = (from l in mpList select l.UnCollectable).Sum();
            sales.MpWrittenOff = (from l in mpList select l.WrittenOff).Sum();
            sales.MpToBeCollected = (from l in mpList select l.ToBeCollected).Sum(); // - sales.MpCancelled;

            sales.TotalGrossRevenue = (from o in orderList select (o.Total - o.TotalTax - o.TotalShipping + o.Adjust + o.TotalCoupon)).Sum() + sales.TotalLineItemDiscounts - sales.TotalGiftCardRevenue; // - sales.MpCashDiscounts 
            sales.TotalSinglePayGrossRevenue = sales.TotalGrossRevenue + (sales.Mp1PayRevenue - sales.MpRevenue) + sales.MpCashDiscounts + sales.TotalGiftCardRevenue; //inflate TotalGrossRevenue to account for multi-pay sales at OnePayPrice
            sales.TotalSinglePayGrossRevenueCash = sales.TotalSinglePayGrossRevenue; // - sales.MpCashDiscounts;

            sales.TotalDiscounts = (from o in orderList select (o.Adjust)).Sum(); // - sales.MpCashDiscounts;
            sales.TotalDiscounts += (from o in orderList select (o.TotalCoupon)).Sum();

            sales.TotalNetRevenue = sales.TotalSinglePayGrossRevenue - sales.MpCancelled - sales.MpDiscounts - sales.TotalGiftCardRevenue + 
                sales.TotalOtherCS - sales.TotalLineItemDiscounts - sales.TotalDiscounts;

            sales.TotalGrossCash = sales.TotalGrossRevenue;
            sales.TotalNetCash = sales.TotalGrossCash - sales.TotalLineItemDiscounts - sales.TotalDiscounts + sales.TotalOtherCS;
            sales.TotalMargin = sales.TotalNetCash - sales.TotalCost;
            sales.TotalSalesTax = (from o in orderList select (o.TotalTax)).Sum();
            sales.TotalShipping = (from o in orderList select (o.TotalShipping)).Sum();
            sales.TotalCollectedWebSite = sales.TotalNetCash + sales.TotalSalesTax + sales.TotalShipping - sales.TotalRefunds - sales.TotalVoids + sales.TotalGiftCardRevenue;
            sales.TotalCollectedCC = sumSalesTransaction + sales.TotalOtherCS - sales.TotalRefunds - sales.TotalVoids;

            sales.LineItemDiscountReconcile = sales.tstTotalLineItemDiscounts - sales.TotalLineItemDiscounts;
            sales.adjGrossRevRevenueReconcile = sales.tstTotalGrossRevenue - (sales.TotalGrossRevenue - sales.TotalLineItemDiscounts);

            //CASH reconcile @ Gross Revenue Level
            //sales.GrossReconcile = mvc_Metrics.GetGrossReconcile("GrossMismatchList", startDate, endDate);//order total vs, orderdetail total: mismatches by orderID in Session["GrossMisMatch"]
            //sales.GrossMismatchList = (List<AdminCart.Order>)System.Web.HttpContext.Current.Session["GrossMismatchList"];

            decimal tmpGrossReconcile = 0;
            sales.GrossMismatchList = GetGrossRevenueMisMatch(orderList, orderDetailList, out tmpGrossReconcile);
            sales.GrossReconcile = tmpGrossReconcile;

            sales.mpTransactionList = mpTransactionsByDateProvider.GetAll(startDate, endDate);

            decimal tmpTotal = 0;
            for (int i = 0; i < 6; i++)
            {
                sales.TransAmountsFromPriorMonths[i] = (from t in sales.mpTransactionList
                                                        where t.OrderDate.Month == endDate.AddMonths(-(i+1)).Month
                                                        select t.TransactionAmount).Sum();
                tmpTotal += sales.TransAmountsFromPriorMonths[i];
            }

            sales.CollectedReconcile = sales.TotalCollectedCC - sales.TotalCollectedWebSite - tmpTotal;

            return sales;
        }

        public List<Order> GetGrossRevenueMisMatch(List<Order> orderList, List<OrderDetail> orderDetailList, out decimal tmpGrossReconcile)
        {
            List<Order> theList = new List<Order>();
            tmpGrossReconcile = 0;

            foreach (Order o in orderList)
            {
                List<OrderDetail> tmpODList = (from od in orderDetailList where o.OrderID == od.OrderID select od).ToList();
                if (tmpODList == null)
                {
                    tmpODList = new List<OrderDetail>();
                }
                decimal gmOrder = o.Total - o.TotalTax - o.TotalShipping + o.TotalCoupon + o.Adjust;
                decimal gmOrderDetail = (from tmpOD in tmpODList select tmpOD.Quantity * (tmpOD.UnitPrice - tmpOD.Discount)).Sum();
                if (gmOrder != gmOrderDetail)
                {
                    theList.Add(o);
                    tmpGrossReconcile += gmOrder - gmOrderDetail;
                }
            }
            return theList;
        }
    }
}
 