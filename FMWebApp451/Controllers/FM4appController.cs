﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models;

namespace FM2015.Controllers
{
    public class FM4appController : Controller
    {
        // GET: FM4app
        public ActionResult Index()
        {
            FM4AppDownload fm4 = new FM4AppDownload();

            return View(fm4);
        }
    }
}