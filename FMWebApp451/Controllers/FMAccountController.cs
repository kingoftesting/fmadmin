﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models;
using FM2015.ViewModels;
using FM2015.Helpers;
using AdminCart;

namespace FM2015.Controllers
{
    public class FMAccountController : Controller
    {
        // GET: FMAccount/SignIn
        public ActionResult SignIn(string returnUrl)
        {
            CustomerViewModel cvm = new CustomerViewModel();
            ViewBag.ReturnUrl = returnUrl;
            return View(cvm);
        }

        // POST: FMAccount/SignIn
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignIn(CustomerViewModel cvm, string returnUrl)
        {
            Certnet.Cart cart = new Certnet.Cart();
            Session["cart"] = null;
            Session["CScart"] = null;
            Session["CustomerID"] = 0;
            Session["cache_ProductItems"] = null;
            Session["promo"] = null;
            Session["UnshippedReportList"] = null;
            Session["UnshippedSerReport"] = null;

            Session["IsAdmin"] = false;
            Session["IsDoctor"] = false;
            Session["IsCS"] = false;
            Session["IsQA"] = false;
            Session["IsREPORTS"] = false;
            Session["IsSTORE"] = false;
            Session["IsUSER"] = false;
            Session["IsMARKETING"] = false;
            
            try
            {
                string email = cvm.Customer.Email;
                string password = cvm.Customer.Password;
                int id = cvm.Customer.Login(email, password);
                if (id > 0) 
                {

                    FM2015.Models.Roles role = new FM2015.Models.Roles(id); //get user roles, if any
                    if (!String.IsNullOrEmpty(role.Role))
                    {
                        string[] roles = role.Role.Split(',');
                        for (int i = 0; i < roles.Length; i++)
                        {
                            if (roles[i].ToUpper() == "ADMIN") { Session["IsAdmin"] = true; }
                            if (roles[i].ToUpper() == "DOCTOR") { Session["IsDoctor"] = true; }
                            if (roles[i].ToUpper() == "CUSTOMERSERVICE") { Session["IsCS"] = true; }
                            if (roles[i].ToUpper() == "ISQA") { Session["IsQA"] = true; }
                            if (roles[i].ToUpper() == "REPORTS") { Session["IsREPORTS"] = true; }
                            if (roles[i].ToUpper() == "STORE") { Session["IsSTORE"] = true; }
                            if (roles[i].ToUpper() == "USER") { Session["IsUSER"] = true; }
                            if (roles[i].ToUpper() == "MARKETING") { Session["IsMARKETING"] = true; }
                        }
                    }
                    cart.SiteCustomer = new Certnet.Customer(id);
                    cart.BillAddress = Certnet.Address.LoadAddress(id, 1);
                    cart.ShipAddress = Certnet.Address.LoadAddress(id, 2);
                    Session["UserLastName"] = cart.SiteCustomer.LastName;
                    Session["UserFirstName"] = cart.SiteCustomer.FirstName;
                    Session["UserID"] = id;

                    FM2015.Models.User user = new FM2015.Models.User(id);
                    Session["_currentUser"] = user;

                    Session["Cart"] = cart; 
                }

                if (returnUrl != null)
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return Redirect("/adminMVC");
                }
            }
            catch
            {
                ModelState.AddModelError("", "Invalid Sign-In Attempt.");
                return View(cvm);
            }
        }

        // GET: FMAccount/Register
        public ActionResult Register(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            CustomerViewModel cvm = new CustomerViewModel();
            return View(cvm);
        }

        // POST: FMAccount/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(CustomerViewModel cvm, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string billState = Request["txtBillState"];
                    string shipState = Request["txtShipState"];
                    cvm = FM2015.Helpers.Helpers.UpdateStateCountrySelectLists(cvm, billState, shipState);
                    int result = Customer.EnterCustomer(cvm.Customer, cvm.BillAddress, cvm.ShipAddress);
                    if (result <=0)
                    {
                        ViewBag.ErrorMessage = "Registration Failed. The email address used is already associated with an existing FaceMaster account. Please use another.";
                        return View(cvm);
                    }
                    else
                    {
                        FM2015.Models.User user = new FM2015.Models.User(result); //result hold new CustomerID
                        HttpContext.Session["_currentUser"] = user;
                    }

                    if (returnUrl != null)
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return Redirect("/MyAccount");
                    }
                }
            }
            catch
            {
                ModelState.AddModelError("", "Invalid Registration attempt.");
                return View(cvm);
            }
            ModelState.AddModelError("", "Invalid Registration attempt.");
            return View(cvm);
        }

        // POST: FMAccount/SignOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignOff()
        {
            Models.User user = new Models.User();
            HttpContext.Session["_currentUser"] = user;
            return Redirect("/");
        }
    }
}