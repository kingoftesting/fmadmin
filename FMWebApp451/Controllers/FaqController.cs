﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FM2015.Models;

namespace FM2015.Controllers
{
    public class FaqController : Controller
    {
        // GET: Faq
        public ActionResult Index(string paramFaq)
        {
            List<Faq> faqList = new List<Faq>();
            Faq faq = new Faq();
            faqList = faq.GetAll(paramFaq);

            return View(faqList);
        }
    }
}