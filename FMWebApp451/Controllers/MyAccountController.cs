﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models;
using FM2015.ViewModels;
using AdminCart;

namespace FM2015.Controllers
{
    public class MyAccountController : Controller
    {
        // GET: MyAccount
        public ActionResult Index()
        {
            int userID = FM2015.Models.User.GetUserID();
            if (userID <= 0)
            {
                return Redirect("/FMAccount/SignIn");
            }
            else
            {
                Customer customer = new Customer(FM2015.Models.User.GetUserID());
                CustomerViewModel cvm = new CustomerViewModel(customer);
                cvm.Customer.ConfirmPassword = cvm.Customer.Password;
                return View(cvm);
            }
        }

        //POST: MyProfile
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(CustomerViewModel cvm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string billState = Request["txtBillState"];
                    string shipState = Request["txtShipState"];
                    cvm = FM2015.Helpers.Helpers.UpdateStateCountrySelectLists(cvm, billState, shipState);
                    cvm.Customer.SaveCustomerToDB();
                    cvm.BillAddress.Save(cvm.Customer, 1);
                    cvm.BillAddress.Save(cvm.Customer, 2);
                    return View(cvm);
                }
            }
            catch
            {
                ModelState.AddModelError("", "Invalid Registration attempt.");
                return View(cvm);
            }
            ModelState.AddModelError("", "Invalid Registration attempt.");
            return View(cvm);
        }

        //GET: MyOrders
        public ActionResult MyOrders()
        {
            int userID = FM2015.Models.User.GetUserID();
            List<Order> orderList = Order.FindAllOrders(userID, true);
            return View(orderList);
        }

        //GET: OrderDetail
        public ActionResult MyOrderDetail(int orderID)
        {
            //Order order = new Order(orderID);
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(orderID);
            return View(cart);
        }
    }
}