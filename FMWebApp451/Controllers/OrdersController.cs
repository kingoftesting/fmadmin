﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models;
using FM2015.ViewModels;
using AdminCart;
using PayWhirl;
using Stripe;
using System.IO;
using Newtonsoft.Json;

namespace FM2015.Controllers
{
    public class OrdersController : Controller
    {
        // GET: Orders
        public ActionResult Index(string paramOrder)
        {
            Cart cart = new Cart();
            List<Cart> cartList = new List<Cart>();
            OrderViewModel ovm = new OrderViewModel();

            int orderID = 0;
            string email = "";
            string orderIDStr = "";
            long serNum = 0;
            string serNumStr = "";
            string lastName = "";
            string firstName = "";
            string nameStr = "";
            DateTime endDate = DateTime.Now;
            DateTime startDate = endDate.AddDays(-7);
            bool useDateParams = true;

            if (!string.IsNullOrEmpty(paramOrder))
            {
                if (!Int32.TryParse(paramOrder, out orderID))
                {
                    if (paramOrder.Contains("@"))
                    {
                        email = paramOrder; //looks like an email address
                        useDateParams = false;
                    }
                    else if (paramOrder[0] == '#')
                    {
                        serNumStr = paramOrder.Replace("#", ""); //looks like a serial number
                        if (!long.TryParse(serNumStr, out serNum))
                        {
                            ViewBag.ErrMsg = "Serial Number must contain only integers (like: 61312103422)";
                            return View(ovm);
                        }
                        useDateParams = false;
                    }
                    else
                    {
                        nameStr = paramOrder;
                        useDateParams = false; //assume this isn't a date
                        if (!nameStr.Contains("..."))
                        {
                            string[] name = nameStr.Split(' '); // could be a first or last name
                            if (name.Length > 0)
                            {
                                lastName = name[0];
                            }
                            if (name.Length > 1)
                            {
                                firstName = name[1];
                            }

                            // check for a date range
                            if (DateTime.TryParse(lastName, out startDate))
                            {
                                useDateParams = true; //must be a startDate
                                if (!string.IsNullOrEmpty(firstName)) //now check for an endDate
                                {
                                    if (!DateTime.TryParse(firstName, out endDate))
                                    {
                                        ViewBag.ErrMsg = "endDate must be a Date (like: 1/1/2015)";
                                        return View(ovm);
                                    }
                                    else
                                    {
                                        endDate = endDate.Date.AddDays(1).AddTicks(-1); //get just before midnight
                                    }
                                }
                                if (endDate < startDate)
                                {
                                    ViewBag.ErrMsg = "startDate must be before EndDate";
                                    return View(ovm);
                                }
                            }
                        }
                    }
                }
                else
                {
                    useDateParams = false; //is orderID
                }
            }

            orderIDStr = (orderID == 0) ?  orderIDStr = "" : orderIDStr = orderID.ToString();
            serNumStr = (serNum == 0) ? serNumStr = "" : serNumStr = serNum.ToString();

            List<int> list = ShopifyAgent.ShopifyAgent.SyncShopify(); //sync orders from Shopify

            if (useDateParams)
            {
                ovm.cartList = cart.FindAllCarts(startDate, endDate, false);
            }
            else
            {
                if (!string.IsNullOrEmpty(email))
                {
                    ovm.cartList = cart.FindAllCarts("", email, false);
                }
                else
                {
                    ovm.cartList = cart.FindAllCarts(orderIDStr, lastName, serNumStr, "", false);
                }
            }

            if (ovm.cartList.Count == 1)
            {
                orderID = ovm.cartList[0].SiteOrder.OrderID;
                return RedirectToAction("Details", "Orders", new { id = orderID.ToString() });
            }

            return View(ovm);
        }

        //GET: Details
        public ActionResult Details(int id)
        {
            OrderViewModel ovm = new OrderViewModel();
            bool hasSubscriptionItem = false; 
            ovm.cart = ovm.cart.GetCartFromOrder(id, false);

            foreach (Item item in ovm.cart.Items)
            {
                Product p = new Product(item.LineItemProduct.ProductID);
                if (p.IsMultiPay)
                {
                    hasSubscriptionItem = true;
                    break;
                }
            }

            if (ovm.cart.SiteOrder.SourceID == -1) { ovm.SourceID = ovm.cart.SiteOrder.SourceID + " (" + Config.AppCode() + " website)"; }
            if (ovm.cart.SiteOrder.SourceID == -2) { ovm.SourceID = ovm.cart.SiteOrder.SourceID + " (HDI website)"; }
            if (ovm.cart.SiteOrder.SourceID == -3) { ovm.SourceID = ovm.cart.SiteOrder.SourceID + " (Shopify website)"; }
            if (ovm.cart.SiteOrder.SourceID < -3) { ovm.SourceID = ovm.cart.SiteOrder.SourceID + " (Shopify website)"; }
            if (ovm.cart.SiteOrder.SourceID == 0)
            {
                if (ovm.cart.SiteOrder.ByPhone) { ovm.SourceID = ovm.cart.SiteOrder.SourceID + " (unknown: possibly Saul)"; }
                else { ovm.SourceID = ovm.cart.SiteOrder.SourceID + " (unknown: dhenson)"; }
            }
            if (ovm.cart.SiteOrder.SourceID > 0)
            {
                Customer c = new Customer(ovm.cart.SiteOrder.SourceID);
                ovm.SourceID = ovm.cart.SiteOrder.SourceID.ToString() + " (" + c.FirstName + " " + c.LastName + ")";
            }

            ovm.CompanyID = ovm.cart.SiteOrder.CompanyID.ToString() + " (" + Config.CompanyIDName(ovm.cart.SiteOrder.CompanyID) + ")";

            if (ovm.cart.SiteOrder.SiteSettingsID > 0)
            {
                //make this into a link to the affiliate order detail report
                SiteSettings sset = new SiteSettings(ovm.cart.SiteOrder.SiteSettingsID);
                ovm.SiteSpecial = ovm.cart.SiteOrder.SiteSettingsID.ToString() + " (" + sset.SiteSettingsName + ")";
            }
            else
            {
                ovm.SiteSpecial = ovm.cart.SiteOrder.SiteSettingsID.ToString() + " (none)";
            }

            string trkNumURL = "";
            if (ovm.cart.SiteOrder.upsShipments.Count() > 0)
            {
                string trkNum = ovm.cart.SiteOrder.upsShipments[0].TrackingNumber;
                string carrierType = trkNum.Substring(0, 1);
                if (carrierType == "1") //if not "1" then assume USPS   
                {
                    //trkNum = "<a href=" + "'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums='" + trkNum + ">" + trkNum + "</a>";
                    string url = "http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=" + trkNum;
                    trkNumURL = String.Format("<a href=\"{0}\">{1}</a>", url, trkNum);
                    ViewBag.trkNumURL = trkNumURL;
                }
                else 
                { 
                    //trkNum = "<a href" + "'https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1='" + trkNum + ">" + trkNum + "</a>";
                    string url = "https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=" + trkNum;
                    trkNumURL = String.Format("<a href=\"{0}\">{1}</a>", url, trkNum);
                    ViewBag.trkNumURL = trkNumURL;
                }
                //ovm.cart.SiteOrder.upsShipments[0].TrackingNumber = trkNum;
            }

            long shopOrderId = ShopifyAgent.ShopifyOrder.GetShopifyIDFromName(ovm.cart.SiteOrder.SSOrderID.ToString(), ovm.cart.SiteOrder.CompanyID);
            if (shopOrderId > 800000)
            {
                ovm.RiskList = ShopifyAgent.ShopOrderRisk.GetOrderRiskList(shopOrderId, ovm.cart.SiteOrder.CompanyID);
            }
            ovm.subscription = new Subscriptions(ovm.cart.SiteOrder.OrderID.ToString());
            if ((ovm.subscription.ID == 0) && hasSubscriptionItem)
            {
                ovm.HoldingForSubscription = true;
            }
            ovm.delayedSubscriptionList = FM2015.Models.DelayedPlanChanges.GetDelayedPlanChangesList(ovm.cart.SiteCustomer.CustomerID);
            ovm.cerList = adminCER.CER.FindCERList("", "", "", "", "", "", ovm.cart.SiteOrder.OrderID.ToString());
            //ViewBag.subStatus = Subscriptions.SubscriberStatus(ovm.cart.SiteCustomer.Email);
            if (ovm.subscription.StripeStatus == "unpaid")
            {
                ViewBag.deadBeat = true;
                ViewBag.AmountDue = 0;
                ViewBag.StripeCustomerId = "";
                ViewBag.StripeSubscriptionId = "";

                //Stripe.StripeInvoice invoice = Subscriptions.GetLastInvoice(ovm.cart.SiteCustomer.Email);
                //decimal accountBalance = Subscriptions.GetAccountBalance(ovm.cart.SiteCustomer.Email);
                StripeCustomer stripeCustomer = Subscriptions.GetStripeCustomer(ovm.subscription.StripeCusID);
                StripeInvoice stripeInvoice = new StripeInvoice();
                stripeInvoice = Subscriptions.GetLastInvoice(stripeCustomer.Id);

                if (stripeInvoice != null)
                {
                    if (stripeInvoice.AmountDue > 0)
                    {
                        decimal balance = Convert.ToDecimal(stripeInvoice.AmountDue) / 100;
                        ViewBag.AmountDue = balance.ToString("C");
                        ViewBag.StripeCustomerId = stripeCustomer.Id;
                    }
                }
            }
            else
            {
                ViewBag.deadBeat = false;
            }

            return View(ovm);
        }

        public ActionResult UpdateOrderAddresses(int orderID)
        {
            Order order = new Order(orderID);
            Customer customer = new Customer(order.CustomerID);
            CustomerViewModel cvm = new CustomerViewModel(customer);

            if (cvm == null)
            {
                ViewBag.ErrMsg = "Error attempting to locate Order details. Please try again. If the problem persists please contact IT";
                return View(cvm);
            }

            return View(cvm);
        }

        // POST: CustomerViewModel/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateOrderAddresses(int orderID, CustomerViewModel cvm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Order order = new Order(orderID);
                    Customer customer = new Customer(order.CustomerID);
                    cvm.Customer = customer;
                    //cvm.BillAddress.AddressId = order.BillingAddressID;
                    //cvm.ShipAddress.AddressId = order.ShippingAddressID;
                    cvm.BillAddress.AddressId = 0; //create new address entries in database
                    cvm.ShipAddress.AddressId = 0;
                    string billState = Request["txtBillState"];
                    string shipState = Request["txtShipState"];
                    cvm = FM2015.Helpers.Helpers.UpdateStateCountrySelectLists(cvm, billState, shipState);
                    //cvm.Customer.SaveCustomerToDB();
                    cvm.BillAddress.Save(cvm.Customer, 1);
                    cvm.ShipAddress.Save(cvm.Customer, 2);
                    order.BillingAddressID = cvm.BillAddress.AddressId;
                    order.ShippingAddressID = cvm.ShipAddress.AddressId;
                    order.SaveAddressIDs();

                    Cart cart = new Cart();
                    cart = cart.GetCartFromOrder(orderID);
                    adminCER.CER cer = new adminCER.CER(cart);
                    cer.EventDescription = "Order Addresses Updated" + Environment.NewLine;
                    cer.EventDescription += "Order ID: " + orderID.ToString() + Environment.NewLine;
                    cer.EventDescription += "Change By: " + cer.ReceivedBy + Environment.NewLine;
                    cer.ActionExplaination = "Order Addresses Updated";
                    int result = cer.Add();

                    return RedirectToAction("Details", "Orders", new { id = orderID.ToString() });
                }
                return View();

            }
            catch
            {
                return View();
            }
        }

        public ActionResult AddSerialNumber(int orderID)
        {
            return View();
        }

        // POST: 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSerialNumber(int orderID, string serNum)
        {
            long serialNum = 0;
            if (!Int64.TryParse(serNum, out serialNum))
            {
                ViewBag.ErrMsg = "You Must Enter a Device Serial Number";
                return View();
            }

            Utilities.FMHelpers.AddSerialNum(orderID, serNum, DateTime.Now);
            OrderNote.Insertordernote(orderID, FM2015.Models.User.GetUserName(), "Added New Serial = " + serNum);
            return RedirectToAction("Details", "Orders", new { id = orderID.ToString() });
        }


        public ActionResult AddTrackingNumber(int orderID)
        {
            return View();
        }

        // POST: 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddTrackingNumber(int orderID, string trkNum, string weightStr, string chargeStr)
        {

            double weight = 0;
            double upscharge = 0;
            string trackingnumber = ValidateUtil.CleanUpString(trkNum);

            if (!double.TryParse(weightStr, out weight))
            {
                ViewBag.ErrMsg = "You Must Enter a weight (form: x.yz)";
                return View();
            }

            chargeStr = chargeStr.Replace("$", "");
            if (!double.TryParse(chargeStr, out upscharge))
            {
                ViewBag.ErrMsg = "You Must Enter a delivery charge (form: x.yz)";
                return View();
            }

            Utilities.FMHelpers.AddTrackNum(orderID, weight, upscharge, trackingnumber, FM2015.Models.User.GetUserName());
            OrderNote.Insertordernote(orderID, FM2015.Models.User.GetUserName(), "Added New Tracking Number = " + trkNum);

            string trkNumURL = "";
            string carrierType = trackingnumber.Substring(0, 1);
            if (carrierType == "1") //if not "1" then assume USPS   
            { trkNumURL = "http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=" + trackingnumber; }
            else
            { trkNumURL = "https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=" + trackingnumber; }

            trkNumURL = String.Format("<a href=\"{0}\">{1}</a>", trkNumURL, trackingnumber);

            //now check to see if we need to fulfill an order at Shopify
            //SSOrderID > 0
            //SourceID <> -2 (HDI)
            Order order = new Order(orderID);
            if ((order.SSOrderID != 0) &&
                (order.OrderDate > Convert.ToDateTime("1/1/2015")) &&
                (order.SourceID != -2))
            {
                ShopifyAgent.ShopifyFulfillment.SendShopifyTracking(order.SSOrderID, trackingnumber, trkNumURL, order.CompanyID);
            }

            return RedirectToAction("Details", "Orders", new { id = orderID.ToString() });
        }

        public ActionResult AddOrderNote(int orderID)
        {
            return View();
        }

        // POST: 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddOrderNote(int orderID, string orderNote)
        {
            OrderNote.Insertordernote(orderID, FM2015.Models.User.GetUserName(), ValidateUtil.CleanUpString(orderNote));
            return RedirectToAction("Details", "Orders", new { id = orderID.ToString() });
        }

        public ActionResult Refund(int orderID, string chargeID)
        {
            Transaction tran = Transaction.GetTransaction(orderID, chargeID);
            ViewBag.ShowForm = true;
            return View(tran);
        }

        // POST: 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Refund(int orderID, string chargeID, string amountStr, string orderNote)
        {
            Transaction tran = new Transaction();
            decimal amount = 0;
            chargeID = ValidateUtil.CleanUpString(chargeID);

            if (!decimal.TryParse(amountStr, out amount))
            {
                ViewBag.ShowForm = true;
                ViewBag.ErrMsg = "You Must Enter a refund dollar amount (form: xx.yz)";
                return View(tran);
            }

            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(orderID);
            cart.SiteOrder.Total = amount;
            if (cart.Credit(chargeID, cart.SiteOrder.SourceID) != 0)
            {
                ViewBag.ShowForm = true;
                ViewBag.ErrMsg = "Refund Transaction Failed: " + cart.Tran.ResultMsg;
                return View(cart.Tran);
            }

            OrderNote.Insertordernote(orderID, FM2015.Models.User.GetUserName(), "Refund Transaction = " + cart.Tran.TransactionID + "; " + ValidateUtil.CleanUpString(orderNote));
            adminCER.CER cer = new adminCER.CER(cart);
            cer.EventDescription = "REFUND TRANSACTION: Refund amount=" + cart.Tran.TransactionAmount.ToString() + Environment.NewLine + cer.EventDescription;
            int result = cer.Add();

            ViewBag.ShowForm = false;
            ViewBag.ErrMsg = "Refund was successful: refund transactionID = " + cart.Tran.TransactionID;
            return View(cart.Tran);
        }

        public ActionResult UpdateTransaction(int transactionRowId, int orderId)
        {
            Order o = new Order(orderId); //get Shopify Name
            long shopId = ShopifyAgent.ShopifyOrder.GetShopifyIDFromName(o.SSOrderID.ToString(), Config.CompanyID());
            ShopifyAgent.ShopifyOrder shopOrder = new ShopifyAgent.ShopifyOrder(shopId, Config.CompanyID());
            string apiResult = ShopifyAgent.ShopifyAgent.DownloadOrderTransactions(shopOrder);
            ShopifyAgent.ShopifyTransactions sos = new ShopifyAgent.ShopifyTransactions();
            try
            {
                sos = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyTransactions>(apiResult);
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                string msg = "error in Orders:Details: DownLoadTransactions </br>";
                msg += "JSON response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            List<ShopifyAgent.ShopifyTransaction> shopTransList = sos.Transactions.ToList();

            ViewBag.TransactionRowId = transactionRowId;
            ViewBag.OrderId = orderId;
            return View(shopTransList);
        }

        public ActionResult SelectUpdateTransaction(int orderId, int transactionRowId, long shopOrderId, long shopTranId)
        {
            string apiResult = ShopifyAgent.ShopifyAgent.DownloadOrderTransaction(shopOrderId, shopTranId);
            ShopifyAgent.ShopifyTransactionSingle shopTran = new ShopifyAgent.ShopifyTransactionSingle();
            try
            {
                shopTran = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyTransactionSingle>(apiResult);
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                string msg = "error in Orders:Details: DownLoadTransaction (single transaction) </br>";
                msg += "JSON response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            Transaction newTran = new Transaction();

            newTran.TransactionRowID = transactionRowId;
            newTran.OrderID = orderId;
            newTran.TransactionID = shopTran.transaction.Authorization_key;
            newTran.AuthCode = (newTran.TransactionID.Length > 8) ? newTran.TransactionID.Substring(0, 8) : "ch_" + newTran.OrderID.ToString();
            newTran.OrigTranID = "0";
            newTran.TransactionAmount = shopTran.transaction.Amount;
            newTran.TrxType = "S";
            newTran.OrderID = newTran.OrderID;
            newTran.TransactionDate = (DateTime)shopTran.transaction.Created_at;
            newTran.CardType = shopTran.transaction.Payment_Details.Credit_card_company;
            newTran.CardHolder = shopTran.transaction.Payment_Details.Credit_card_company;
            newTran.CardNo = "************ " + shopTran.transaction.Payment_Details.Credit_card_number.Substring(shopTran.transaction.Payment_Details.Credit_card_number.Length-4, 4);
            newTran.ExpDate = "0000";
            newTran.ResultCode = (shopTran.transaction.Status == "success") ? 0 : -1;
            if (newTran.ResultCode == 0)
            {
                newTran.ResultMsg = "Approved (Shopify)";
            }
            else
            {
                newTran.ResultMsg = (shopTran.transaction.Message == null) ? "NOT Approved (Shopify)" : (shopTran.transaction.Message);
            }            
            newTran.AuthCode = "0";
            newTran.Comment2 = "FM" + newTran.OrderID;
            newTran.CardHolder = newTran.CardHolder;
            newTran.Address = newTran.Address;
            return View(newTran);
        }

        // POST: 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SelectUpdateTransaction(Transaction transaction)
        {
            if (transaction.UpdateDB())
            {
                OrderNote.Insertordernote(transaction.OrderID, FM2015.Models.User.GetUserName(), "Update Transaction: TranRowId = " + transaction.TransactionRowID.ToString() +  "; " + transaction.TransactionID);
            }
            return RedirectToAction("Details", "Orders", new { id = transaction.OrderID });

        }

        public ActionResult CancelOrder(int orderID)
        {
            return View();
        }

        // POST: 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelOrder(int orderID, string orderNote)
        {
            if (orderNote.Length == 0)
            {
                ViewBag.ErrMsg = "You must leave an order note explaining the cancelation";
                return View();
            }

            Utilities.FMHelpers.CancelOrder(orderID, FM2015.Models.User.GetUserName());
            return RedirectToAction("Details", "Orders", new { id = orderID.ToString() });
        }

        public ActionResult ViewNoTrackOrders()
        {
            List<FM2015.FMmetrics.UnProcessedOrders> orderList = new List<FM2015.FMmetrics.UnProcessedOrders>();
            orderList = FM2015.FMmetrics.mvc_Metrics.GetUnshippedOrdersList();

            return View(orderList);
        }

        public ActionResult ViewNoSerialOrders()
        {
            List<FM2015.FMmetrics.UnProcessedOrders> orderList = new List<FM2015.FMmetrics.UnProcessedOrders>();
            orderList = FM2015.FMmetrics.mvc_Metrics.GetUnshippedSerOrdersList();

            return View("ViewNoTrackOrders", orderList);
        }

        public ActionResult ViewCERsToAnalyze()
        {
            List<adminCER.CER> cerList = adminCER.CER.GetCERAnalyzeList();
            return View(cerList);
        }

        public ActionResult ViewAgingCERs()
        {
            List<adminCER.CER> cerList = adminCER.CER.GetOpenComplaints();
            cerList = (from cer in cerList where (cer.DateReportReceived < DateTime.Now.AddDays(-30)) select cer).ToList();
            return View("ViewCERsToAnalyze", cerList);
        }

        public ActionResult NewCharge(int orderID)
        {
            //Response.Redirect("~/admin/AddNewCharge.aspx?OrderID=" + orderID.ToString());
            return Redirect("~/admin/AddNewCharge.aspx?OrderID=" + orderID.ToString());
        }

        public ActionResult SendEmailReceipt(int id)
        {
            if (!Order.SendEmailReciept(id))
            {
                ViewBag.ErrMsg = "<h4 style='color: red'>Failure: Email Receipt was not sent successfully.</h4>";
                ViewBag.ErrMsg += "Check the email address and order number then try again. If the problem persists contact IT" + Environment.NewLine;
                ViewBag.ErrMsg += Session["EmailReceiptCopy"].ToString();
                string str = ViewBag.ErrMsg;
                str = str.Replace("\n", "<br />");
                str = str.Replace("\r", "<br />");
                ViewBag.ErrMsg = str;
            }
            else
            {
                ViewBag.ErrMsg = "<h4 style='color: green'>Success: Email Receipt was sent successfully.</h4>";
                ViewBag.ErrMsg += Session["EmailReceiptCopy"].ToString();
                string str = ViewBag.ErrMsg;
                str = str.Replace("\n", "<br />");
                str = str.Replace("\r", "<br />");
                ViewBag.ErrMsg = str;
            }

            return View();
        }

        public ActionResult ReturnAuthorization(int id)
        {
            RMAViewModel rma = new RMAViewModel(id);
            rma.RMABody = GetRMABody(id);
            return View(rma);
        }

        // POST: ReturnAuthorization
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReturnAuthorization(RMAViewModel rma)
        {
            if (ModelState.IsValid)
            {
                string body = rma.RMABody + Environment.NewLine + "-----------------------------------" + Environment.NewLine + Environment.NewLine + rma.ActionBody;
                if (Order.SendEmailRMA(rma.OrderId, body))
                {
                    return RedirectToAction("Details", "Orders", new { id = rma.OrderId.ToString() });
                }
            }

            ViewBag.ErrMsg = "<h4 style='color: red'>Failure: Email RMA was not sent successfully.</h4>";
            ViewBag.ErrMsg += "Check the email address and order number then try again. If the problem persists contact IT";
            return View(rma);
        }

        public ActionResult SendToShopify(int id)
        {
            FMShopifyOrderTranslator.CreateShopifyOrder(id);
            return RedirectToAction("Index");
        }

        public FileResult PackingSlip(int id)
        {
            /*
            OrderViewModel ovm = new OrderViewModel();
            AdminCart.Cart cart = new AdminCart.Cart();
            ovm.cart = cart.GetCartFromOrder(id);
            return View(ovm);
            */
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(id);

            List<FM2015.FMmetrics.UnProcessedOrders> orderList = new List<FM2015.FMmetrics.UnProcessedOrders>();
            FM2015.FMmetrics.UnProcessedOrders upOrder = new FMmetrics.UnProcessedOrders();
            upOrder.Row = 1;
            upOrder.OrderId = id;
            upOrder.OrderDate = cart.SiteOrder.OrderDate;
            upOrder.LastName = cart.SiteCustomer.LastName;
            upOrder.City = cart.ShipAddress.City;
            upOrder.Country = cart.ShipAddress.Country;
            upOrder.DaysDelayed = 0;
            orderList.Add(upOrder);

            MemoryStream pdf = Reports.genPackingListPDF(orderList);
            pdf.Flush(); //Always catches me out
            pdf.Position = 0; //Not sure if this is required

            return File(pdf, "application/pdf");
        }

        public string GetRMABody (int orderId)
        {
            string rmaBody = "Dear {2}," + Environment.NewLine + Environment.NewLine +
    "We regret that your FaceMaster experience has been less than completely satisfying. " + Environment.NewLine + Environment.NewLine +
    "RMA INSTRUCTIONS" + Environment.NewLine + Environment.NewLine +
    "It would be very helpful, and greatly appreciated, if you could send us a response to this email with a very short description of the reason why you were unsatisfied with our product. " + Environment.NewLine + Environment.NewLine +
    "Please return your FaceMaster order# {0}, and all accessories, to our Product Fulfillment Center: " + Environment.NewLine + Environment.NewLine +
    "FaceMaster of Beverly Hills, Inc." + Environment.NewLine +
    "21440 Osborne St." + Environment.NewLine +
    "Canoga Park, CA 91304 - 1520" + Environment.NewLine +
    "(800) 770 - 2521" + Environment.NewLine + Environment.NewLine +
    "Please include your sales invoice, packing slip, or a copy of this email with the following information:" + Environment.NewLine + Environment.NewLine +
    "1.indicate that you are returning your FaceMaster for a refund." + Environment.NewLine + Environment.NewLine +
    "2.include your RMA# {1}." + Environment.NewLine + Environment.NewLine +
    "Please keep a copy of this information and reference it in any future correspondence on this subject." + Environment.NewLine + Environment.NewLine +
    "Also, if possible, please send us a short response to this email with the delivery service and tracking number you use to ship your product back to us.If there is a delivery problem with your return this information will be helpful to us in trying to locate it." + Environment.NewLine + Environment.NewLine +
    "Your refund should be processed and credited to your credit card within 24-48 hours after we receive your returned product." + Environment.NewLine + Environment.NewLine +
    "Thank You," + Environment.NewLine + 
    "FaceMaster Customer Service";

            //create RMA number
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(orderId);
            int userId = FM2015.Models.User.GetUserID();
            string RMAstr = userId.ToString() + "-";
            string orderNumStr = cart.OrderID.ToString() + " / " + cart.SiteOrder.SSOrderID.ToString();
            RMAstr += DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString();
            rmaBody = string.Format(rmaBody, orderNumStr, RMAstr, cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName );

            return rmaBody;
        }

        // GET: Orders/UpdateOrder  // update an existing order
        public ActionResult UpdateOrder(int id)
        {
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(id, false);

            AdminUpdateOrderViewModel acovm = new AdminUpdateOrderViewModel
            {
                CustomerId = cart.SiteCustomer.CustomerID,
                OrderId = cart.SiteOrder.OrderID,
                OrderDate = cart.SiteOrder.OrderDate,
                Total = cart.SiteOrder.Total,
                Adjust = cart.SiteOrder.Adjust,
                Coupon = cart.SiteOrder.TotalCoupon,
                TotalTax = cart.SiteOrder.TotalTax,
                TotalShipping = cart.SiteOrder.TotalShipping,
            };

            acovm.odList = OrderDetail.GetOrderDetails(acovm.OrderId);
            acovm.oaList = OrderAction.GetOrderActionList(acovm.OrderId);
            acovm.tranList = Transaction.GetTransactionList(acovm.OrderId);

            ViewBag.Name = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName;
            ViewBag.OrderId = acovm.OrderId.ToString();

            return View(model: acovm);
        }

        // POST:  Orders/UpdateOrder  // update an existing order
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateOrder(AdminUpdateOrderViewModel acovm)
        {
            if (acovm == null)
            {
                throw new ArgumentNullException(nameof(acovm));
            }

            if (acovm.OrderId <= 0)
            {
                throw new ArgumentNullException(nameof(acovm.OrderId));
            }

            if (ModelState.IsValid)
            {
                Cart cart = new Cart();
                cart = cart.GetCartFromOrder(acovm.OrderId);
                cart.Total = acovm.Total;
                cart.TotalDiscounts = acovm.Adjust + acovm.Coupon;
                cart.TotalTax = acovm.TotalTax;
                cart.TotalShipping = acovm.TotalShipping;
                cart.SiteOrder.OrderDate = acovm.OrderDate;
                cart.SiteOrder.Total = acovm.Total;
                cart.SiteOrder.Adjust = acovm.Adjust;
                cart.SiteOrder.TotalCoupon = acovm.Coupon;
                cart.SiteOrder.TotalTax = acovm.TotalTax;
                cart.SiteOrder.TotalShipping = acovm.TotalShipping;
                cart.UpdateOrder();

                OrderNote orderNote = new OrderNote()
                {
                    OrderNoteID = 0,
                    OrderNoteBy = Models.User.GetUserName(),
                    OrderNoteDate = DateTime.Now,
                    OrderNoteText = "order was manually modified",
                    OrderID = cart.SiteOrder.OrderID,
                };
                orderNote.Save(orderNote.OrderNoteID);
            }
            else
            {
                ViewBag.ErrMsg = "information Missing: Please Update!";
            }

            return RedirectToAction("Details", "Orders", new { id = acovm.OrderId });
        }

        // GET: Orders/UpdateOrder  // update an existing order
        public ActionResult NullifyOrder(int id)
        {
            Order o = new Order(id);
            List<OrderDetail> odList = OrderDetail.GetOrderDetails(id);
            List<OrderAction> oaList = OrderAction.GetOrderActionList(id);
            List<Transaction> tList = Transaction.GetTransactionList(id);

            o.Total = 0;
            o.Adjust = 0;
            o.TotalTax = 0;
            o.TotalShipping = 0;
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(id);
            cart.Total = 0;
            cart.TotalTax = 0;
            cart.TotalShipping = 0;
            cart.SiteOrder = o;
            cart.UpdateOrder();

            foreach (OrderDetail od in odList)
            {
                od.OrderID = -od.OrderID;
                od.UpdateDB();
            }

            foreach (OrderAction oa in oaList)
            {
                oa.OrderActionType = 1;
                oa.OrderActionReason = "order error - nullified";
                oa.OrderActionBy = Models.User.GetUserName();
                oa.SaveDB();
            }

            foreach (Transaction t in tList)
            {
                t.TransactionID = "rm_Null";
                t.CardType = "n/a";
                t.CardNo = "****0000";
                t.ExpDate = "0000";
                t.ResultCode = -1;
                t.ResultMsg = "order error";
                t.UpdateDB();
            }

            OrderNote note = new OrderNote()
            {
                OrderNoteDate = DateTime.Now,
                OrderID = o.OrderID,
                OrderNoteBy = Models.User.GetUserName(),
                OrderNoteText = "Order Error. Order has been Nulled."
            };
            note.Save(0); 

            return RedirectToAction("Details", "Orders", new { id });
        }

        // GET: Orders/UpdateDetail  // update an existing orderdetail
        public ActionResult UpdateDetail(int id)
        {
            OrderDetail od = new OrderDetail(id);

            return View(od);
        }

        // POST:  Orders/UpdateDetail  // update existing orderdetails
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateDetail(OrderDetail od)
        {
            if (ModelState.IsValid)
            {
                od.LineItemTotal = od.Quantity * (od.UnitPrice - od.Discount);
                od.UpdateDB();
            }
            else
            {
                ViewBag.ErrMsg = "information Missing: Please Update!";
            }

            return RedirectToAction("Details", "Orders", new { id = od.OrderID });
        }

        // GET: Orders/CreateDetail  // create a new existing orderdetail lineitem
        public ActionResult CreateDetail(int FMOrderID)
        {
            OrderDetail od = new OrderDetail()
            {
                OrderID = FMOrderID,
                Quantity = 1,
            };

            return View(od);
        }

        // POST:  Orders/CreateDetail  // create a new existing orderdetail lineitem
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDetail(OrderDetail od)
        {
            if (ModelState.IsValid)
            {
                Product p = new Product(od.ProductID);
                ProductCost pc = new ProductCost(od.ProductID);
                od.UnitPrice = p.SalePrice;
                od.UnitCost = pc.Cost;
                od.LineItemTotal = od.Quantity * (od.UnitPrice - od.Discount);
                od.SaveToOrderDetail();
            }
            else
            {
                ViewBag.ErrMsg = "information Missing: Please Update!";
            }

            return RedirectToAction("Details", "Orders", new { id = od.OrderID });
        }

        // GET: Orders/DeleteDetail  // create a new existing orderdetail lineitem
        public ActionResult DeleteDetail(int id)
        {
            OrderDetail od = new OrderDetail(id);
            int orderId = od.OrderID;
            od.OrderID = -od.OrderID;
            od.UpdateDB();

            return RedirectToAction("Details", "Orders", new { id = orderId });
        }

        // GET: Orders/UpdateDetail  // update existing orderActions
        public ActionResult UpdateAction(int id)
        {
            OrderAction oa = new OrderAction(id);

            return View(model: oa);
        }

        // POST:  Orders/UpdateAction  // update existing orderActions
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateAction(OrderAction oa)
        {
            if (ModelState.IsValid)
            {
                oa.SaveDB();
            }
            else
            {
                ViewBag.ErrMsg = "information Missing: Please Update!";
            }

            return RedirectToAction("Details", "Orders", new { id = oa.OrderID });
        }

        // GET: Orders/UpdateOrderTransaction  // update existing Transaction
        public ActionResult UpdateOrderTransaction(int FMOrderID, int transactionRowId)
        {
            Transaction t = Transaction.GetTransaction(FMOrderID, transactionRowId);
            if (String.IsNullOrEmpty(t.Comment1))
            {
                t.Comment1 = t.Comment2;
            }
            if (String.IsNullOrEmpty(t.AuthCode))
            {
                t.AuthCode = "0000";
            }
            if (String.IsNullOrEmpty(t.OrigTranID))
            {
                t.OrigTranID = "0000";
            }
            if (String.IsNullOrEmpty(t.CvsCode))
            {
                t.CvsCode = "000";
            }

            return View(model: t);
        }

        // POST:  Orders/UpdateOrderTransaction  // update existing Transaction
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateOrderTransaction(Transaction t)
        {
            if (ModelState.IsValid)
            {
                t.UpdateDB();
            }
            else
            {
                ViewBag.ErrMsg = "information Missing: Please Update!";
            }

            return RedirectToAction("Details", "Orders", new { id = t.OrderID });
        }

        // GET: Orders/CreateDetail  // create a new existing orderdetail lineitem
        public ActionResult CreateTransaction(int FMOrderID)
        {
            Order o = new Order(FMOrderID);
            Customer c = new Customer(o.CustomerID);
            Address a = Address.LoadAddress(o.BillingAddressID, 1);
            Transaction t = new Transaction()
            {
                OrderID = FMOrderID,
                TransactionAmount = o.Total,
                TransactionDate = o.OrderDate,
                TransactionID = "rm_null",
                TrxType = "S",
                CardType = "n/a",
                CvsCode = "000",
                CardNo = "****1234",
                ExpDate = "0000",
                ResultCode = 0,
                ResultMsg = "Approved(RM manual)",
                AuthCode = "0000",
                CardHolder = c.FirstName + " " + c.LastName,
                Address = a,
                CustomerCode = "FM" + o.OrderID.ToString(),
                Comment1 = o.OrderID.ToString(),
                Comment2 = o.OrderID.ToString(),
                OrigTranID = "n/a",
                Iavs = "n/a",
                AvsAddr = "n/a",
                AvsZip = "n/a",
            };

            return View(t);
        }

        // POST:  Orders/CreateDetail  // create a new existing orderdetail lineitem
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTransaction(Transaction t)
        {
            if (ModelState.IsValid)
            {
                t.TransactionID.TrimStart().TrimEnd();
                t.SaveDB();
            }
            else
            {
                ViewBag.ErrMsg = "information Missing: Please Update!";
            }

            return RedirectToAction("Details", "Orders", new { id = t.OrderID });
        }
    }
}