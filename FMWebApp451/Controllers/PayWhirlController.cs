﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PayWhirl;
using Stripe;
using FMWebApp.ViewModels.PayWhirl;
using FMWebApp451.Services.StripeServices;

namespace FMWebApp401.Controllers
{
    public class PayWhirlController : Controller
    {
        //
        // GET: /PayWhirl/
        //Get all plans in the authenticated account.
        //PayWhirl API call: GET /api/plans

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CurrentUser()
        {
            //PayWhirlUser user = PayWhirlUser.GetCurrentUser();
            PayWhirlUser user = PayWhirlBiz.GetCurrentUser();
            return View(user);
        }

        public ActionResult SubscriberList(string param)
        {
            //List<PayWhirlSubscriber> subs = PayWhirlSubscriber.GetSubscriberList();
            List<PayWhirlSubscriber> subs = PayWhirlBiz.GetSubscriberList(param);
            return View(subs);
        }

        public ActionResult SubscriberDetails(int id)
        {
            PayWhirlCustomerViewModel pwvm = new PayWhirlCustomerViewModel();
            //pwvm.payWhirlCustomer = PayWhirlSubscriber.GetSubscriber(id);
            pwvm.payWhirlCustomer = PayWhirlBiz.GetSubscriber(id);
            StripeServices stripeServices = new StripeServices(); 
            pwvm.stripeCustomerSubscriptionList = stripeServices.GetCustomerSubscriptionList(pwvm.payWhirlCustomer.Stripe_id, out string errMsg);

            return View(pwvm);
        }

        public ActionResult SubscriberDetailsByEmail(string email)
        {
            PayWhirlCustomerViewModel pwvm = new PayWhirlCustomerViewModel();
            //pwvm.payWhirlCustomer = PayWhirlSubscriber.GetSubscriber(id);
            pwvm.payWhirlCustomer = PayWhirlBiz.FindSubscriberByEmail(email);
            StripeServices stripeServices = new StripeServices();
            pwvm.stripeCustomerSubscriptionList = stripeServices.GetCustomerSubscriptionList(pwvm.payWhirlCustomer.Stripe_id, out string errMsg);

            /*
            var subscriptionService = new Stripe.StripeSubscriptionService(); //get list of all subscriptions for customer
            try
            {
                pwvm.stripeCustomerSubscriptionList = subscriptionService.List(pwvm.payWhirlCustomer.Stripe_id);
            }
            catch (StripeException ex)
            {
                FM2015.Helpers.Helpers.ProcessStripeException(ex, "SubscriberDetailsByEmail: subscriptionService.List())");
            }
            */

            return View("SubscriberDetails", pwvm);
        }

        public ActionResult SubscriberEdit(int id)
        {
            //PayWhirlSubscriber sub = PayWhirlSubscriber.GetSubscriber(id);
            PayWhirlSubscriber sub = PayWhirlBiz.GetSubscriber(id);

            return View(sub);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubscriberEdit(PayWhirlSubscriber subscriber)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PayWhirlSubscriber.UpdateSubscriber(subscriber);
                    return RedirectToAction("SubscriberList");
                }
            }
            catch
            {
                return View(subscriber);
            }
            return View(subscriber);
        }

        public ActionResult SubscriberCreate()
        {
            PayWhirlSubscriber sub = new PayWhirlSubscriber();
            return View(sub);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubscriberCreate(PayWhirlSubscriber subscriber)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PayWhirlSubscriber.CreateSubscriber(subscriber);
                    return RedirectToAction("SubscriberList");
                }
            }
            catch
            {
                return View(subscriber);
            }
            return View(subscriber);
        }

        public ActionResult PlanList()
        {
            List<PayWhirlPlan> plans = PayWhirlPlan.GetPlanList();
            return View(plans);
        }

        public ActionResult PlanDetails(int id)
        {
            PayWhirlPlan plan = PayWhirlPlan.GetPlan(id);
            return View(plan);
        }

    }
}
