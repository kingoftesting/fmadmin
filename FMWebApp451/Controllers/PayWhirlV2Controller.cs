﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PayWhirlV2;

namespace FM2015.Controllers
{
    public class PayWhirlV2Controller : Controller
    {
        // GET: PayWhirlV2
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CurrentUser()
        {
            pwAccountV2 user = pwAccountV2.GetAccount();
            return View(user);
        }

        public ActionResult GetCustomerList()
        {
            List<pwCustomerV2> customerList = pwCustomerV2.GetCustomerListV2();
            return View(customerList);
        }

        public ActionResult GetCustomer(int id)
        {
            pwCustomerV2 customer = pwCustomerV2.GetCustomer(id);
            return View(customer);
        }

        public ActionResult updateCustomer(int id)
        {
            pwCustomerV2 customer = pwCustomerV2.GetCustomer(id);
            return View(customer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult updateCustomer(pwCustomerV2 customer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    pwCustomerV2.UpdateCustomer(customer);
                    return RedirectToAction("GetCustomerList");
                }
            }
            catch
            {
                return View(customer);
            }
            return View(customer);
        }

        public ActionResult GetEmailTemplate(int id)
        {
            pwEmailV2 email = pwEmailV2.GetEmailTemplate(id);
            return View(email);
        }
    }
}