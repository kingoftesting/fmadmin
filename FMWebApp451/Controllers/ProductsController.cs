﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FM2015.Helpers;
using FM2015.ViewModels;
using FM2015.Models;
using FMWebApp451.Interfaces;
using AdminCart;

namespace FM2015.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductProvider productProvider;
        private readonly IProductCostProvider productCostProvider;

        public ProductsController(IProductProvider productProvider, IProductCostProvider productCostProvider)
        {
            this.productProvider = productProvider;
            this.productCostProvider = productCostProvider;
        }

        // GET: Products
        public ActionResult Index()
        {
            List<Product> productList = productProvider.GetAll();
            productList = (from p in productList where p.CSOnly == true select p).ToList();
            return View(productList);
        }

        // GET: Products
        public ActionResult AdminIndex()
        {


            List<Product> productList = productProvider.GetAll();
            //sort based on enabled, then productId
            productList = productList.OrderByDescending(p => p.Enabled).ThenBy(p => p.Sort).ToList();
            return View(productList);
        }

        // GET: Product/Details/5 where 5 = ProductID
        public ActionResult Details(int id)
        {
            Product product = productProvider.GetById(id);
            return View(product);
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            ProductViewModel pvm = new ProductViewModel
            {
                Categories = GenCategorySelectList()
            };
            return View(pvm);
        }


        // POST: Product/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel pvm)
        {
            if (ModelState.IsValid)
            {
                int result = productProvider.Save(pvm.Product); //update DB entry
                result = productCostProvider.Save(pvm.ProductCost); //update DB entry
                return RedirectToAction("Details", "Products", new { id = result });
            }
            return View(pvm);
        }

        // GET: Product/Edit
        public ActionResult Edit(int id)
        {
            //https://www.asp.net/mvc/overview/older-versions/mvc-music-store/mvc-music-store-part-5

            Product product = productProvider.GetById(id);
            ProductCost productCost = productCostProvider.GetByProduct(product);
            ProductViewModel pvm = new ProductViewModel
            {
                Product = product,
                ProductCost = productCost,
                Categories = GenCategorySelectList()
            };
            return View(pvm);
        }


        // POST: Product/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ProductViewModel pvm)
        {
            //http://www.c-sharpcorner.com/uploadfile/cd3310/createeditdelete-operation-on-a-table-in-asp-net-mvc-appli/

            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException("Products(post): Edit: Id = 0");
            }
            //string ddlCategory = Request.Form["Product_Category"].ToString();
            //pvm.Product.Category = ddlCategory;
            if (ModelState.IsValid)
            {
                pvm.Product.ProductID = id;
                pvm.ProductCost.ProductID = id;
                int result = productProvider.Save(pvm.Product); //update DB entry; if (result <= 0) { dbErrorLogging.LogError(); }

                result = productCostProvider.Save(pvm.ProductCost); //update DB entry

                return RedirectToAction("Details", "Products", new { id });
            }

            return View(pvm);
        }

        // GET: Product/Clone
        public ActionResult Clone(int id)
        {

            Product product = productProvider.GetById(id);
            ProductCost productCost = productCostProvider.GetByProduct(product);
            product.ProductID = 0; //make sure we insert a new product 
            productCost.ProductCostID = 0;

            int newId = 0;
            newId = productProvider.Save(product); //use id=0 to be sure to create new DB entry
            if (newId <= 0)
            {
                return RedirectToAction("Edit", "Products", new { id });
            }
            newId = productCostProvider.Save(productCost); //use id=0 to be sure to create new DB entry
            if (newId <= 0)
            {
                return RedirectToAction("Edit", "Products", new { id });
            }
            return RedirectToAction("Edit", "Products", new { id = newId });
        }

        // GET: Product/SendAllProductsToShopift
        public ActionResult SendAllProductsToShopify()
        {
            bool customerOnly = true;
            List<Product> pList = productProvider.GetAll(customerOnly);
            foreach (Product p in pList)
            {
                Product.PostFMProductToShopify(p);
            }

            pList = productProvider.GetAll();
            return View("AdminIndex", pList);
        }

        // GET: Product/ProductToShopify
        public ActionResult SendProductToShopify(int id)
        {
            Product product = new Product(id);
            Product.PostFMProductToShopify(product);

            List<Product> pList = productProvider.GetAll();
            return View("AdminIndex", pList);
        }

        public List<SelectListItem> GenCategorySelectList()
        {
            List<SelectListItem> selectList = new List<SelectListItem>()
            {
                new SelectListItem { Text = "n/a", Value = "n/a" },
                new SelectListItem { Text = "device", Value = "device" },
                new SelectListItem { Text = "multipay", Value = "multipay" },
                new SelectListItem { Text = "club", Value = "club" },
                new SelectListItem { Text = "accessory", Value = "accessory" }
            };
            return selectList;
        }


    }
}