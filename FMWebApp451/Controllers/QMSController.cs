﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models.QMS;

namespace FM2015.Controllers
{
    public class QMSController : Controller
    {
        // GET: QMS
        public ActionResult Index()
        {
            return View();
        }

        // GET: QMS/NCRs
        public ActionResult NCRs()
        {
            List<NCR> ncrList = NCR.GetNCRList();
            return View(ncrList);
        }
    }
}