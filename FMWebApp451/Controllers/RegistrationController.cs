﻿using System;
using System.Web.Mvc;
using AdminCart;
using FMWebApp451.Interfaces;

namespace FMWebApp451.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly IRegistration registrationProvider;

        public RegistrationController(IRegistration registrationProvider)
        {
            this.registrationProvider = registrationProvider;
        }

        // GET: Registration
        public ActionResult Index()
        {
            return View();
        }
    }
}