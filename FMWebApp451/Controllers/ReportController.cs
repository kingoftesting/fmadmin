﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models;
using FM2015.ViewModels;
using FM2015.FMmetrics;
using AdminCart;
using Stripe;
using System.IO;
using FMWebApp451.Interfaces;

namespace FM2015.Controllers
{
    public class ReportController : Controller
    {
        private readonly IOrdersWithDetailProvider odProvider;

        public ReportController(IOrdersWithDetailProvider odProvider)
        {
            this.odProvider = odProvider;
        }

        // GET: Sales Report
        public ActionResult Index(string param)
        {

            SalesViewModel SalesVM = new SalesViewModel();

            DateTime endDateTmp = DateTime.Now;
            DateTime startDateTmp = DateTime.Today;
            string startStr = "";
            string endStr = "";

            if (!string.IsNullOrEmpty(param))
            {
                if (!param.Contains("..."))
                {
                    string[] dateArray = param.Split(' '); // could be a start and end date range
                    if (dateArray.Length > 0)
                    {
                        startStr = dateArray[0];
                    }
                    if (dateArray.Length > 1)
                    {
                        endStr = dateArray[1];
                    }
                    else if (startStr.ToUpper() == "Y")
                    {
                        startStr = DateTime.Now.AddDays(-1).Date.ToString();
                        endStr = startStr;
                    }
                    else if (startStr.ToUpper() == "W")
                    {
                        startStr = DateTime.Now.AddDays(-7).Date.ToString();
                        endStr = DateTime.Now.ToString();
                    }
                    else if (startStr.ToUpper() == "M")
                    {
                        DateTime tempDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        startStr = tempDate.ToString();
                        tempDate = tempDate.AddMonths(1).AddTicks(-1); //should be the end of the month
                        endStr = tempDate.ToString();
                    }

                    // check for a date range
                    if (DateTime.TryParse(startStr, out startDateTmp))
                    {
                        if (!string.IsNullOrEmpty(endStr)) //now check for an endDate
                        {
                            if (!DateTime.TryParse(endStr, out endDateTmp))
                            {
                                ViewBag.ErrMsg = "endDate must be a Date (like: 1/1/2015)";
                                return View(SalesVM);
                            }
                        }
                    }
                    else
                    {
                        ViewBag.ErrMsg = "startDate must be a Date (like: 1/1/2015)";
                        return View(SalesVM);
                    }
                }
            }


            endDateTmp = (endDateTmp < Convert.ToDateTime("1/1/2000")) ? DateTime.Now : endDateTmp;
            endDateTmp = endDateTmp.Date.AddDays(1).AddTicks(-1); //get just before midnight
            startDateTmp = (startDateTmp < Convert.ToDateTime("1/1/2000")) ? DateTime.Today : startDateTmp;

            if (startDateTmp > endDateTmp)
            {
                ViewBag.ErrMsg = "End Date must be AFTER Start Date (date range error: end: " + endDateTmp.ToShortDateString() + "; start: " + startDateTmp.ToShortDateString() + ")";
                return View(SalesVM);
            }


            //SalesVM.SalesPrimary = FindAllWebSales(startDateTmp.ToString(), endDateTmp.ToString());
            SalesVM.SalesPrimary = FindAllWebSales(startDateTmp, endDateTmp);

            SalesVM.SalesPrimary.adjGrossRevRevenueReconcile = SalesVM.SalesPrimary.adjGrossRevRevenueReconcile - SalesVM.SalesPrimary.TotalGiftCardRevenue; //gift card $$ were subtracted out of revenue, but still show up in orderdetails $$

            ViewBag.MpCancelledPayments = SalesVM.SalesPrimary.MpCancelled.ToString("C");
            ViewBag.MpPastDuePayments = SalesVM.SalesPrimary.MpUnCollectable.ToString("C");
            ViewBag.MpWrittenOffPayments = SalesVM.SalesPrimary.MpWrittenOff.ToString("C");

            ViewBag.NumberOfMonths = endDateTmp.Subtract(startDateTmp).Days / (365.25 / 12);

            List<FMWebApp451.ViewModels.MultiPayTransactionsByDate> tList = new List<FMWebApp451.ViewModels.MultiPayTransactionsByDate>();
            tList = FMWebApp451.ViewModels.MultiPayTransactionsByDate.GetList(startDateTmp, endDateTmp);

            List<FMWebApp451.ViewModels.MultiPayTransactionsByDate> newtList = (from t in tList
                                                 where t.OrderDate.Month == endDateTmp.AddMonths(-1).Month
                                                 select t).ToList();

            decimal ccMonthMinus1 = (from t in tList
                                     where t.OrderDate.Month == endDateTmp.AddMonths(-1).Month
                                     select t.TransactionAmount).Sum();
            ViewBag.ccMonthMinus1 = ccMonthMinus1.ToString("C");
            ViewBag.MonthMinus1 = Utilities.FMHelpers.MonthLookUp(endDateTmp.AddMonths(-1).Month);
            

            decimal ccMonthMinus2 = (from t in tList
                                     where t.OrderDate.Month == endDateTmp.AddMonths(-2).Month
                                     select t.TransactionAmount).Sum();
            ViewBag.ccMonthMinus2 = ccMonthMinus2.ToString("C");
            ViewBag.MonthMinus2 = Utilities.FMHelpers.MonthLookUp(endDateTmp.AddMonths(-2).Month);

            decimal ccMonthMinus3 = (from t in tList
                                     where t.OrderDate.Month == endDateTmp.AddMonths(-3).Month
                                     select t.TransactionAmount).Sum();
            ViewBag.ccMonthMinus3 = ccMonthMinus3.ToString("C");
            ViewBag.MonthMinus3 = Utilities.FMHelpers.MonthLookUp(endDateTmp.AddMonths(-3).Month);

            SalesVM.SalesPrimary.CollectedReconcile = (SalesVM.SalesPrimary.TotalCollectedWebSite + ccMonthMinus1 + ccMonthMinus2 + ccMonthMinus3) - SalesVM.SalesPrimary.TotalCollectedCC;
            SalesVM.SalesPrimary.GrossRevRevenueReconcile = SalesVM.SalesPrimary.TotalSinglePayGrossRevenue - (SalesVM.SalesPrimary.Mp1PayRevenue - SalesVM.SalesPrimary.MpRevenue + SalesVM.SalesPrimary.TotalGrossRevenue);

            //ViewBag.TotalGiftCardRevenue = SalesVM.SalesPrimary.TotalGiftCardRevenue;

            return View(SalesVM);
        }

    // GET: Sales Report
        public ActionResult DailySales(string SelectMonth, string SelectYear)
        {
            DailySalesByPerson dailySales = new DailySalesByPerson();
            if (String.IsNullOrEmpty(SelectMonth)) { SelectMonth = DateTime.Now.Month.ToString(); }
            if (String.IsNullOrEmpty(SelectYear)) { SelectYear = DateTime.Now.Year.ToString(); }
            dailySales.sales = DayOfWeekSales.GetDailySales(Convert.ToInt32(SelectMonth), Convert.ToInt32(SelectYear));

            List<SalesMan> theTeam = new List<SalesMan>();
            foreach (DayOfWeekSales itemList in dailySales.sales) //itemList == List<DayOfWeekSales>
            {
                dailySales.TotalGrossRevenue = dailySales.TotalGrossRevenue + itemList.TotalRevenue;
                foreach (SalesMan item in itemList.SalesTeamList) //item == SalesMan
                {
                    if(!theTeam.Exists(x => x.user.UserID == item.user.UserID))
                    {
                        SalesMan s = new SalesMan();
                        s.Sales = item.Sales;
                        s.user.UserID = item.user.UserID;
                        theTeam.Add(s);
                    }
                    else
                    {
                        int index = theTeam.FindIndex(x => x.user.UserID == item.user.UserID);
                        theTeam[index].Sales = theTeam[index].Sales + item.Sales;
                    }
                   
                    dailySales.TotalPhoneRevenue = dailySales.TotalPhoneRevenue + item.Sales;
                }
            }
            dailySales.TotalWebRevenue = dailySales.TotalGrossRevenue - dailySales.TotalPhoneRevenue;
            foreach(SalesMan item in theTeam)
            {
                User u = new User(item.user.UserID);
                item.user.FirstName = u.FirstName;
                item.user.LastName = u.LastName;
            }
            ViewBag.theTeam = theTeam;

            return View(dailySales);
        }

        public static Sales FindAllWebSales(string fromDate, string toDate)
        {
            Sales sales = new Sales();

            List<AdminCart.Order> GrossMismatchList = new List<AdminCart.Order>(); //get ready to fill a list of orderIDs from order exceptions

            DateTime startDate = Convert.ToDateTime(fromDate);
            sales.StartDate = startDate;

            DateTime stopDate = Convert.ToDateTime(toDate);
            sales.EndDate = stopDate;

            DataSet dsRev = mvc_Metrics.GetRevenueSet("ObrTransactionList", startDate, stopDate); //also sets Session["ObrTransactionList"]
            sales.OtherCSTransactions = (List<AdminCart.Transaction>)System.Web.HttpContext.Current.Session["ObrTransactionList"];

            sales.SalesDetailsList = FMmetrics.mvc_Metrics.GetOrderDetailReport(startDate, stopDate);
            sales.GrossReconcile = mvc_Metrics.GetGrossReconcile("GrossMismatchList", startDate, stopDate);//order total vs, orderdetail total: mismatches by orderID in Session["GrossMisMatch"]
            sales.GrossMismatchList = (List<AdminCart.Order>)System.Web.HttpContext.Current.Session["GrossMismatchList"];

            sales.TotalCost = 0;
            sales.TotalUnits = 0;
            sales.TotalLineItemDiscounts = 0;
            foreach (var item in sales.SalesDetailsList)
            {
                sales.TotalCost += item.TotalCost;
                sales.TotalUnits += item.TotalUnits;
                sales.TotalLineItemDiscounts += item.TotalDiscount;
            }

            sales.TotalOrders = Convert.ToInt32(dsRev.Tables[0].Rows[0]["OrderCount"]);
            sales.TotalGrossRevenue = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["GrossRevenue"]);
            sales.TotalDiscounts = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["LineItemDiscounts"]);
            sales.TotalOtherCS = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["OtherCS"]);
            sales.TotalNetRevenue = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["NetRevenue"]);
            sales.TotalSalesTax = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Tax"]);
            sales.TotalShipping = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Shipping"]);
            sales.TotalCollectedWebSite = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Collected"]);
            sales.TotalRefunds = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Refunds"]);
            sales.TotalVoids = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["Voids"]);
            sales.TotalCollectedCC = Convert.ToDecimal(dsRev.Tables[0].Rows[0]["CollectedCC"]);

            return sales;
        }

        public static Sales FindAllWebSales(DateTime startDate, DateTime endDate)
        {
            Sales sales = new Sales();

            List<AdminCart.Order> GrossMismatchList = new List<AdminCart.Order>(); //get ready to fill a list of orderIDs from order exceptions

            sales.StartDate = startDate;
            sales.EndDate = endDate;

            List<AdminCart.Order> orderList = new List<AdminCart.Order>();
            orderList = AdminCart.Order.FindAllOrders(startDate, endDate, false);

            List<AdminCart.OrderDetail> orderDetailList = new List<AdminCart.OrderDetail>();
            orderDetailList = AdminCart.Order.GetOrderDetailsList(startDate, endDate);

            List<AdminCart.Transaction> salesTransactionsList = new List<AdminCart.Transaction>();
            decimal sumSalesTransaction = mvc_Metrics.GetTransactionList(mvc_Metrics.TransactionType.CartSale, "SalesTransactionsList", startDate, endDate);
            salesTransactionsList = (List<AdminCart.Transaction>)System.Web.HttpContext.Current.Session["SalesTransactionsList"];

            List<AdminCart.Transaction> csTransactionsList = new List<AdminCart.Transaction>();
            sales.TotalOtherCS = mvc_Metrics.GetTransactionList(mvc_Metrics.TransactionType.NonCartSale, "CSTransactionsList", startDate, endDate);
            csTransactionsList = (List<AdminCart.Transaction>)System.Web.HttpContext.Current.Session["CSTransactionsList"];

            List<AdminCart.Transaction> refundTransactionsList = new List<AdminCart.Transaction>();
            sales.TotalRefunds = mvc_Metrics.GetTransactionList(mvc_Metrics.TransactionType.Refund, "RefundTransactionsList", startDate, endDate);
            refundTransactionsList = (List<AdminCart.Transaction>)System.Web.HttpContext.Current.Session["RefundTransactionsList"];

            List<AdminCart.Transaction> voidTransactionsList = new List<AdminCart.Transaction>();
            sales.TotalVoids = mvc_Metrics.GetTransactionList(mvc_Metrics.TransactionType.Void, "VoidTransactionsList", startDate, endDate);
            voidTransactionsList = (List<AdminCart.Transaction>)System.Web.HttpContext.Current.Session["voidTransactionsList"];

            sales.SalesDetailsList = FMmetrics.mvc_Metrics.GetOrderDetailReport(startDate, endDate);
            //sales.GrossReconcile = mvc_Metrics.GetGrossReconcile("GrossMismatchList", startDate, endDate);//order total vs, orderdetail total: mismatches by orderID in Session["GrossMisMatch"]
            //sales.GrossMismatchList = (List<AdminCart.Order>)System.Web.HttpContext.Current.Session["GrossMismatchList"];

            sales.TotalCost = 0;
            sales.TotalUnits = 0;

            sales.TotalOrders = (from o in orderList select o.OrderID).Count();
            //decimal tstLineItemDiscounts = (from od in orderDetailList select new decimal { sum (od.UnitPrice * od.Quantity) }

            List<FMWebApp451.ViewModels.MultiPayRevenueByOrder> mpList = new List<FMWebApp451.ViewModels.MultiPayRevenueByOrder>();
            mpList = FMWebApp451.ViewModels.MultiPayRevenueByOrder.GetList(startDate, endDate);
            sales.TotalMPDevices = (from l in mpList select l.OrderId).Count();

            decimal mpGrossRev = 0;
            decimal spGrossRev = 0;
            foreach (SalesDetails sd in sales.SalesDetailsList)
            {
                sales.TotalCost += sd.TotalCost;
                sales.TotalUnits += sd.TotalUnits;
                sales.TotalLineItemDiscounts += sd.TotalDiscount;

                AdminCart.Product p = new AdminCart.Product(sd.ProductID);
                if (p.IsMultiPay)
                {
                    mpGrossRev = mpGrossRev + (sd.TotalGrossRevenue); //running total of revenue generated by all multi-pay products
                    spGrossRev = spGrossRev + (sd.TotalUnits * p.OnePayPrice); 
                }
                if (p.IsFMSystem)
                {
                    sales.TotalDevices = sales.TotalDevices + sd.TotalUnits;
                }
                if (p.IsRefill)
                {
                    sales.TotalRefills = sales.TotalRefills + sd.TotalUnits;
                }
            }

            List<Product> pList = Product.GetProducts();
            foreach (OrderDetail od in orderDetailList)
            {
                Product p = (from prod in pList where prod.ProductID == od.ProductID select prod).FirstOrDefault();
                if (p.IsMultiPay)
                {
                    sales.MpCashDiscounts = sales.MpCashDiscounts + (from o in orderList where o.OrderID == od.OrderID select o.Adjust).FirstOrDefault();
                }
                if (p.IsGiftCard)
                {
                    sales.TotalGiftCardRevenue = sales.TotalGiftCardRevenue + (od.Quantity * od.UnitPrice);
                }
            }

            decimal tstLineItemDiscounts = orderDetailList.Sum(s => (s.Discount * s.Quantity));
            decimal tstLineItemDiscounts2 = (from od in orderDetailList select (od.Quantity * od.Discount)).Sum();
            decimal tstgrossRevenue = orderDetailList.Sum(s => ((s.UnitPrice - s.Discount) * s.Quantity));
            decimal tstgrossRevenue2 = (from od in orderDetailList select ((od.UnitPrice - od.Discount) * od.Quantity)).Sum();

            sales.Mp1PayRevenue = spGrossRev;
            sales.MpDiscounts = (from l in mpList select l.PromoCodeDiscount).Sum();
            sales.MpRevenue = mpGrossRev - sales.MpCashDiscounts;
            sales.MpNetRevenue = sales.Mp1PayRevenue - sales.MpDiscounts;
            sales.MpCollected = (from l in mpList select l.Collected).Sum();
            sales.MpCancelled = (from l in mpList select l.Cancelled).Sum();
            sales.MpUnCollectable = (from l in mpList select l.UnCollectable).Sum();
            sales.MpWrittenOff = (from l in mpList select l.WrittenOff).Sum();
            sales.MpToBeCollected = sales.MpNetRevenue - sales.MpCollected - sales.MpCancelled - sales.MpWrittenOff;

            sales.TotalGrossRevenue = (from o in orderList select (o.Total - o.TotalTax - o.TotalShipping + o.Adjust)).Sum() + sales.TotalLineItemDiscounts - sales.TotalGiftCardRevenue;
            sales.TotalSinglePayGrossRevenue = sales.TotalGrossRevenue + (spGrossRev - mpGrossRev) + sales.TotalGiftCardRevenue; //inflate TotalGrossRevenue to account for multi-pay sales at OnePayPrice

            sales.TotalDiscounts = (from o in orderList select (o.Adjust)).Sum(); // - sales.MpCashDiscounts;
            sales.TotalNetRevenue = sales.TotalGrossRevenue - sales.TotalLineItemDiscounts - sales.TotalDiscounts + sales.TotalOtherCS;
            sales.TotalMargin = sales.TotalNetRevenue - sales.TotalCost;
            sales.TotalSalesTax = (from o in orderList select (o.TotalTax)).Sum();
            sales.TotalShipping = (from o in orderList select (o.TotalShipping)).Sum();
            sales.TotalCollectedWebSite = sales.TotalNetRevenue + sales.TotalSalesTax + sales.TotalShipping - sales.TotalRefunds - sales.TotalVoids + sales.TotalGiftCardRevenue;
            sales.TotalCollectedCC = sumSalesTransaction + sales.TotalOtherCS - sales.TotalRefunds - sales.TotalVoids;

            sales.LineItemDiscountReconcile = tstLineItemDiscounts - sales.TotalLineItemDiscounts;
            sales.adjGrossRevRevenueReconcile = tstgrossRevenue - (sales.TotalGrossRevenue - sales.TotalLineItemDiscounts);

            return sales;
        }

        public ActionResult MultiPayStatus()
        {
            List<FMWebApp451.ViewModels.MultiPayStatus> list = new List<FMWebApp451.ViewModels.MultiPayStatus>();
            list = FMWebApp451.ViewModels.MultiPayStatus.GetStatusList();
            return View(list);
        }

        public ActionResult PromoCodeMismatches()
        {
            List<FMWebApp451.ViewModels.PromoCodeMismatches> list = new List<FMWebApp451.ViewModels.PromoCodeMismatches>();
            list = FMWebApp451.ViewModels.PromoCodeMismatches.GetMismatchList();
            return View(list);
        }

        public ActionResult MultiPayRevenueByOrder(string paramSearch)
        {
            List<FMWebApp451.ViewModels.MultiPayRevenueByOrder> list = new List<FMWebApp451.ViewModels.MultiPayRevenueByOrder>();

            DateTime endDateTmp = DateTime.Now;
            DateTime startDateTmp = Convert.ToDateTime("7/6/15");
            string str0 = "";
            string str1 = "";

            //check for startDate
            if (!String.IsNullOrEmpty(paramSearch))
            {
                string paramStr = paramSearch;
                if (!paramStr.Contains("..."))
                {
                    paramStr = paramStr.Replace("-", " ");
                    paramStr = System.Text.RegularExpressions.Regex.Replace(paramStr, @"\s+", " ");
                    string[] strList = paramStr.Split(' '); // could be start & end dates
                    if (strList.Length > 0)
                    {
                        str0 = strList[0];
                    }
                    if (strList.Length > 1)
                    {
                        str1 = strList[1];
                    }

                    // check for a date range
                    if (!DateTime.TryParse(str0, out startDateTmp))
                    {
                        ViewBag.ErrMsg = "startDate must be of the form mm/dd/yyyy";
                        return View();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(str1)) //now check for an endDate
                        {
                            if (!DateTime.TryParse(str1, out endDateTmp))
                            {
                                ViewBag.ErrMsg = "endDate must be of the form mm/dd/yyyy";
                                return View();
                            }
                            else
                            {
                                endDateTmp = endDateTmp.Date.AddDays(1).AddTicks(-1); //get just before midnight
                            }
                        }
                    }
                }
            }

            if (endDateTmp < startDateTmp)
            {
                ViewBag.ErrMsg = "startDate must be before EndDate";
                return View();
            }

            list = FMWebApp451.ViewModels.MultiPayRevenueByOrder.GetList(startDateTmp, endDateTmp);
            list = list.OrderByDescending(x => x.Status).ToList();
            ViewBag.StartDate = startDateTmp.ToString("D");
            ViewBag.EndDate = endDateTmp.ToString("D");

            int totalOrders = (from l in list select l.OrderId).Count();
            int totalDevices = 0;
            try
            {
                totalDevices = FMmetrics.mvc_Metrics.GetUnits(mvc_Metrics.SalesType.Web, startDateTmp, endDateTmp);
            }
            catch
            {
                ; //do nothing in the case of an exception (forcing a NULL into an int?)s
            }
            decimal mpPercent = (totalDevices == 0) ? 0 : Convert.ToDecimal(totalOrders) / Convert.ToDecimal(totalDevices);
            ViewBag.TotalOrders = totalOrders.ToString();
            ViewBag.SinglePayOrders = totalDevices;
            ViewBag.MPPercent = (totalDevices == 0) ? "n/a" : mpPercent.ToString("P1");

            int totalPastDue = (from l in list 
                                where l.UnCollectable != 0 
                                select l).Count();
            ViewBag.TotalPastDue = totalPastDue.ToString();

            int totalWrittenOff = (from l in list
                                where l.WrittenOff != 0
                                select l).Count();
            decimal totalWrittenOffPercent = (totalOrders == 0) ? 0 : Convert.ToDecimal(totalWrittenOff) / Convert.ToDecimal(totalOrders);
            ViewBag.TotalWrittenOff =totalWrittenOff.ToString();
            ViewBag.TotalWrittenOffPercent = (totalOrders == 0) ? "n/a" : totalWrittenOffPercent.ToString("P1");

            int total4Pay = (from l in list
                             where l.ttlPaymentCount == 4
                             select l).Count();
            decimal FourPayPercent = (totalOrders == 0) ? 0 : Convert.ToDecimal(total4Pay) / Convert.ToDecimal(totalOrders);
            ViewBag.Total4Pay = total4Pay.ToString();
            ViewBag.FourPayPercent = (totalOrders == 0) ? "n/a" : FourPayPercent.ToString("P1");

            int total3Pay = (from l in list
                             where l.ttlPaymentCount == 3
                             select l).Count();
            decimal threePayPercent = (totalOrders == 0) ? 0 : Convert.ToDecimal(total3Pay) / Convert.ToDecimal(totalOrders);
            ViewBag.Total3Pay = total3Pay.ToString();
            ViewBag.ThreePayPercent = (totalOrders == 0) ? "n/a" : threePayPercent.ToString("P1");

            decimal percentPastDue = (totalOrders == 0) ? 0 : Convert.ToDecimal(totalPastDue) / Convert.ToDecimal(totalOrders);
            ViewBag.PastDuePercent = (totalOrders == 0) ? "n/a" : percentPastDue.ToString("P1");

            int active = (from l in list where l.Status == "active" select l).Count();
            decimal activePercent = (totalOrders == 0) ? 0 : Convert.ToDecimal(active) / Convert.ToDecimal(totalOrders);
            ViewBag.Active = active.ToString();
            ViewBag.ActivePercent = (totalOrders == 0) ? "n/a" : activePercent.ToString("P1");

            int completed = (from l in list where l.Status == "completed" select l).Count();
            decimal completedPercent = (totalOrders == 0) ? 0 : Convert.ToDecimal(completed) / Convert.ToDecimal(totalOrders);
            ViewBag.Completed = completed.ToString();
            ViewBag.CompletedPercent = (totalOrders == 0) ? "n/a" : completedPercent.ToString("P1");

            int cancelled = (from l in list where l.Status == "canceled" select l).Count();
            decimal cancelledPercent = (totalOrders == 0) ? 0 : Convert.ToDecimal(cancelled) / Convert.ToDecimal(totalOrders);
            ViewBag.Cancelled = cancelled.ToString();
            ViewBag.CancelledPercent = (totalOrders == 0) ? "n/a" : cancelledPercent.ToString("P1");

            decimal grossRevenue = (from l in list select l.GrossRevenue).Sum();
            ViewBag.GrossRevenue = grossRevenue.ToString("C");

            decimal promoDiscount = (from l in list select l.PromoCodeDiscount).Sum();
            ViewBag.Discount = promoDiscount.ToString("C");

            decimal promoPercent = (grossRevenue == 0) ? 0 : promoDiscount / grossRevenue;
            ViewBag.Percent = (grossRevenue == 0) ? "n/a" : promoPercent.ToString("P1");

            decimal revenue = (from l in list select l.Revenue).Sum();
            ViewBag.Revenue = revenue.ToString("C");

            decimal averageSellingPrice = (totalOrders == 0) ? 0 : revenue / Convert.ToDecimal(totalOrders);
            ViewBag.ASP = averageSellingPrice.ToString("C");

            decimal collected = (from l in list select l.Collected).Sum();
            decimal collectedPercent = (revenue == 0) ? 0 : collected / revenue;
            ViewBag.Collected = collected.ToString("C");
            ViewBag.CollectedPercent = (revenue == 0) ? "n/a" : collectedPercent.ToString("P1");

            decimal cancelledPmnts = (from l in list select l.Cancelled).Sum();
            decimal cancelledPmntsPercent = (revenue == 0) ? 0 : cancelledPmnts / revenue;
            ViewBag.cancelledPmnts = cancelledPmnts.ToString("C");
            ViewBag.cancelledPmntsPercent = (revenue == 0) ? "n/a" : cancelledPmntsPercent.ToString("P1");

            decimal writtenOff = (from l in list select l.WrittenOff).Sum();
            decimal writtenOffPercent = (revenue == 0) ? 0 : writtenOff / revenue;
            ViewBag.WrittenOff = writtenOff.ToString("C");
            ViewBag.WrittenOffPercent = (revenue == 0) ? "n/a" : writtenOffPercent.ToString("P1");

            decimal unCollectable = (from l in list select l.UnCollectable).Sum();
            decimal unCollectablePercent = (revenue == 0) ? 0 : unCollectable / revenue;
            ViewBag.UnCollectable = unCollectable.ToString("C");
            ViewBag.UnCollectablePercent = (revenue == 0) ? "n/a" : unCollectablePercent.ToString("P1");

            ViewBag.NumberOfMonths = endDateTmp.Subtract(startDateTmp).Days / (365.25 / 12);             

            List<FMWebApp451.ViewModels.MultiPayTransactionsByOrder> listT = new List<FMWebApp451.ViewModels.MultiPayTransactionsByOrder>();
            listT = FMWebApp451.ViewModels.MultiPayTransactionsByOrder.GetList(startDateTmp, endDateTmp);

            List<FMWebApp451.ViewModels.MultiPayTransactionsByOrder> newtList = (from t in listT
                                                                                 where t.TransactionDate.Month == startDateTmp.AddMonths(0).Month
                                                                                select t).ToList();
            ViewBag.Month0 = Utilities.FMHelpers.MonthLookUp(startDateTmp.AddMonths(0).Month);
            ViewBag.Month1 = Utilities.FMHelpers.MonthLookUp(startDateTmp.AddMonths(1).Month);
            ViewBag.Month2 = Utilities.FMHelpers.MonthLookUp(startDateTmp.AddMonths(2).Month);
            ViewBag.Month3 = Utilities.FMHelpers.MonthLookUp(startDateTmp.AddMonths(3).Month);

            ViewBag.CollectedMM0 = (from t in listT
                                          where t.TransactionDate.Month == startDateTmp.AddMonths(0).Month
                                          select t.TransactionAmount).Sum().ToString("C");
            ViewBag.CancelledMM0 = (from l in list
                                      where l.OrderDate.Month == startDateTmp.AddMonths(0).Month
                                      select l.Cancelled).Sum().ToString("C");
            ViewBag.UncollectableMM0 = (from l in list
                                      where l.OrderDate.Month == startDateTmp.AddMonths(0).Month
                                      select l.UnCollectable).Sum().ToString("C");

            ViewBag.WrittenOffMM0 = (from l in list
                                        where l.OrderDate.Month == startDateTmp.AddMonths(0).Month
                                        select l.WrittenOff).Sum().ToString("C");

            ViewBag.CollectedMM1 = (from t in listT
                                    where t.TransactionDate.Month == startDateTmp.AddMonths(1).Month
                                    select t.TransactionAmount).Sum().ToString("C");
            ViewBag.CancelledMM1 = (from l in list
                                    where l.OrderDate.Month == startDateTmp.AddMonths(1).Month
                                    select l.Cancelled).Sum().ToString("C");
            ViewBag.UncollectableMM1 = (from l in list
                                        where l.OrderDate.Month == startDateTmp.AddMonths(1).Month
                                        select l.UnCollectable).Sum().ToString("C");
            ViewBag.WrittenOffMM1 = (from l in list
                                     where l.OrderDate.Month == startDateTmp.AddMonths(1).Month
                                     select l.WrittenOff).Sum().ToString("C");

            ViewBag.CollectedMM2 = (from t in listT
                                    where t.TransactionDate.Month == startDateTmp.AddMonths(2).Month
                                    select t.TransactionAmount).Sum().ToString("C");
            ViewBag.CancelledMM2 = (from l in list
                                    where l.OrderDate.Month == startDateTmp.AddMonths(2).Month
                                    select l.Cancelled).Sum().ToString("C");
            ViewBag.UncollectableMM2 = (from l in list
                                        where l.OrderDate.Month == startDateTmp.AddMonths(2).Month
                                        select l.UnCollectable).Sum().ToString("C");
            ViewBag.WrittenOffMM2 = (from l in list
                                     where l.OrderDate.Month == startDateTmp.AddMonths(2).Month
                                     select l.WrittenOff).Sum().ToString("C");

            ViewBag.CollectedMM3 = (from t in listT
                                    where t.TransactionDate.Month == startDateTmp.AddMonths(3).Month
                                    select t.TransactionAmount).Sum().ToString("C");
            ViewBag.CancelledMM3 = (from l in list
                                    where l.OrderDate.Month == startDateTmp.AddMonths(3).Month
                                    select l.Cancelled).Sum().ToString("C");
            ViewBag.UncollectableMM3 = (from l in list
                                        where l.OrderDate.Month == startDateTmp.AddMonths(3).Month
                                        select l.UnCollectable).Sum().ToString("C");
            ViewBag.WrittenOffMM3 = (from l in list
                                     where l.OrderDate.Month == startDateTmp.AddMonths(3).Month
                                     select l.WrittenOff).Sum().ToString("C");


            List<FMWebApp451.ViewModels.MultiPayRefundsByOrder> listR = new List<FMWebApp451.ViewModels.MultiPayRefundsByOrder>();
            listR = FMWebApp451.ViewModels.MultiPayRefundsByOrder.GetList(startDateTmp, endDateTmp);

            decimal totalRefunds = (from r in listR select r.TransactionAmount).Sum();
            ViewBag.Refunded = totalRefunds.ToString("C");
            decimal refundedPercent = (revenue == 0) ? 0 : totalRefunds / revenue;
            ViewBag.RefundPercent = refundedPercent.ToString("P1");

            //decimal toBeCollected = (from l in list select l.ToBeCollected).Sum();
            decimal toBeCollected = revenue - collected - cancelledPmnts - writtenOff;
            decimal tobeCollectedPercent = (revenue == 0) ? 0 : toBeCollected / revenue;
            ViewBag.ToBeCollected = toBeCollected.ToString("C");
            ViewBag.ToBeCollectedPercent = (revenue == 0) ? "n/a" : tobeCollectedPercent.ToString("P1");

            //update the view list with refund info
            foreach (FMWebApp451.ViewModels.MultiPayRefundsByOrder r in listR)
            {
                FMWebApp451.ViewModels.MultiPayRevenueByOrder t = list.Where(x => x.OrderId == r.OrderId).FirstOrDefault();
                if (t != null) { t.RefundAmount = r.TransactionAmount; }
            }

            List<FMWebApp451.ViewModels.MultiPayRefundsByOrder> newRList = (from r in listR
                                                                            where r.TransactionDate.Month == startDateTmp.AddMonths(0).Month
                                                                            select r).ToList();

            decimal refundsMonth0 = (from r in listR
                                          where r.TransactionDate.Month == startDateTmp.AddMonths(0).Month
                                          select r.TransactionAmount).Sum();
            ViewBag.RefundedMM0 = refundsMonth0.ToString("C");

            decimal refundsMonth1 = (from r in listR
                                     where r.TransactionDate.Month == startDateTmp.AddMonths(1).Month
                                     select r.TransactionAmount).Sum();
            ViewBag.RefundedMM1 = refundsMonth1.ToString("C");

            decimal refundsMonth2 = (from r in listR
                                     where r.TransactionDate.Month == startDateTmp.AddMonths(2).Month
                                     select r.TransactionAmount).Sum();
            ViewBag.RefundedMM2 = refundsMonth2.ToString("C");

            decimal refundsMonth3 = (from r in listR
                                     where r.TransactionDate.Month == startDateTmp.AddMonths(3).Month
                                     select r.TransactionAmount).Sum();
            ViewBag.RefundedMM3 = refundsMonth3.ToString("C");

            /* ** support for the various Tabs (unpaid, completed, etc.)
        <td><span>@ViewBag.GrossRevenue</span></td>
        <td><span>@ViewBag.Percent</span></td>
        <td><span>@ViewBag.Discount</span></td>
        <td><!--promoCode--></td>
        <td><span>@ViewBag.Revenue</span></td>
        <td><!--payment--></td>
        <td><!--ttlPaymentCount--></td>
        <td><!--PaymentsMade--></td>
        <td><span>@ViewBag.Collected</span></td>
        <td><span>@ViewBag.Refunded</span></td>
        <td><span>@ViewBag.ToBeCollected</span></td>
        <td><span>@ViewBag.UnCollectable</span></td>
        <td><span>@ViewBag.WrittenOff</span></td>
*/

            // #tabUnPaid
            decimal grossRevenue_unpaid = (from l in list where l.Status == "unpaid" select l.GrossRevenue).Sum();
            ViewBag.GrossRevenue_unpaid = grossRevenue_unpaid.ToString("C");

            decimal promoDiscount_unpaid = (from l in list where l.Status == "unpaid" select l.PromoCodeDiscount).Sum();
            ViewBag.Discount_unpaid = promoDiscount_unpaid.ToString("C");

            decimal promoPercent_unpaid = (grossRevenue == 0) ? 0 : promoDiscount_unpaid / grossRevenue;
            ViewBag.Percent_unpaid = (grossRevenue == 0) ? "n/a" : promoPercent_unpaid.ToString("P1");

            decimal revenue_unpaid = (from l in list where l.Status == "unpaid" select l.Revenue).Sum();
            ViewBag.Revenue_unpaid = revenue_unpaid.ToString("C");

            decimal collected_unpaid = (from l in list where l.Status == "unpaid" select l.Collected).Sum();
            ViewBag.Collected_unpaid = collected_unpaid.ToString("C");

            decimal toBeCollected_unpaid = (from l in list where l.Status == "unpaid" select l.ToBeCollected).Sum();
            ViewBag.ToBeCollected_unpaid = toBeCollected_unpaid.ToString("C");

            decimal unCollectable_unpaid = (from l in list where l.Status == "unpaid" select l.UnCollectable).Sum();
            ViewBag.UnCollectable_unpaid = unCollectable_unpaid.ToString("C");

            decimal writtenOff_unpaid = (from l in list where l.Status == "unpaid" select l.WrittenOff).Sum();
            ViewBag.WrittenOff_unpaid = writtenOff_unpaid.ToString("C");

            // #tabActive
            decimal grossRevenue_active = (from l in list where l.Status == "active" select l.GrossRevenue).Sum();
            ViewBag.GrossRevenue_active = grossRevenue_active.ToString("C");

            decimal promoDiscount_active = (from l in list where l.Status == "active" select l.PromoCodeDiscount).Sum();
            ViewBag.Discount_active = promoDiscount_active.ToString("C");

            decimal promoPercent_active = (grossRevenue == 0) ? 0 : promoDiscount_active / grossRevenue;
            ViewBag.Percent_active = (grossRevenue == 0) ? "n/a" : promoPercent_active.ToString("P1");

            decimal revenue_active = (from l in list where l.Status == "active" select l.Revenue).Sum();
            ViewBag.Revenue_active = revenue_active.ToString("C");

            decimal collected_active = (from l in list where l.Status == "active" select l.Collected).Sum();
            ViewBag.Collected_active = collected_active.ToString("C");

            decimal toBeCollected_active = (from l in list where l.Status == "active" select l.ToBeCollected).Sum();
            ViewBag.ToBeCollected_active = toBeCollected_active.ToString("C");

            decimal unCollectable_active = (from l in list where l.Status == "active" select l.UnCollectable).Sum();
            ViewBag.UnCollectable_active = unCollectable_active.ToString("C");

            decimal writtenOff_active = (from l in list where l.Status == "active" select l.WrittenOff).Sum();
            ViewBag.WrittenOff_active = writtenOff_active.ToString("C");

            // #tabCompleted
            decimal grossRevenue_completed = (from l in list where l.Status == "completed" select l.GrossRevenue).Sum();
            ViewBag.GrossRevenue_completed = grossRevenue_completed.ToString("C");

            decimal promoDiscount_completed = (from l in list where l.Status == "completed" select l.PromoCodeDiscount).Sum();
            ViewBag.Discount_completed = promoDiscount_completed.ToString("C");

            decimal promoPercent_completed = (grossRevenue == 0) ? 0 : promoDiscount_completed / grossRevenue;
            ViewBag.Percent_completed = (grossRevenue == 0) ? "n/a" : promoPercent_completed.ToString("P1");

            decimal revenue_completed = (from l in list where l.Status == "completed" select l.Revenue).Sum();
            ViewBag.Revenue_completed = revenue_completed.ToString("C");

            decimal collected_completed = (from l in list where l.Status == "completed" select l.Collected).Sum();
            ViewBag.Collected_completed = collected_completed.ToString("C");

            decimal toBeCollected_completed = (from l in list where l.Status == "completed" select l.ToBeCollected).Sum();
            ViewBag.ToBeCollected_completed = toBeCollected_completed.ToString("C");

            decimal unCollectable_completed = (from l in list where l.Status == "completed" select l.UnCollectable).Sum();
            ViewBag.UnCollectable_completed = unCollectable_completed.ToString("C");

            decimal writtenOff_completed = (from l in list where l.Status == "completed" select l.WrittenOff).Sum();
            ViewBag.WrittenOff_completed = writtenOff_completed.ToString("C");

            // #tabPastDue
            decimal grossRevenue_past_due = (from l in list where l.Status == "past_due" select l.GrossRevenue).Sum();
            ViewBag.GrossRevenue_past_due = grossRevenue_past_due.ToString("C");

            decimal promoDiscount_past_due = (from l in list where l.Status == "past_due" select l.PromoCodeDiscount).Sum();
            ViewBag.Discount_past_due = promoDiscount_past_due.ToString("C");

            decimal promoPercent_past_due = (grossRevenue == 0) ? 0 : promoDiscount_past_due / grossRevenue;
            ViewBag.Percent_past_due = (grossRevenue == 0) ? "n/a" : promoPercent_past_due.ToString("P1");

            decimal revenue_past_due = (from l in list where l.Status == "past_due" select l.Revenue).Sum();
            ViewBag.Revenue_past_due = revenue_past_due.ToString("C");

            decimal collected_past_due = (from l in list where l.Status == "past_due" select l.Collected).Sum();
            ViewBag.Collected_past_due = collected_past_due.ToString("C");

            decimal toBeCollected_past_due = (from l in list where l.Status == "past_due" select l.ToBeCollected).Sum();
            ViewBag.ToBeCollected_past_due = toBeCollected_past_due.ToString("C");

            decimal unCollectable_past_due = (from l in list where l.Status == "past_due" select l.UnCollectable).Sum();
            ViewBag.UnCollectable_past_due = unCollectable_past_due.ToString("C");

            decimal writtenOff_past_due = (from l in list where l.Status == "past_due" select l.WrittenOff).Sum();
            ViewBag.WrittenOff_past_due = writtenOff_past_due.ToString("C");

            // #tabCancelled
            decimal grossRevenue_cancelled = (from l in list where l.Status == "canceled" select l.GrossRevenue).Sum();
            ViewBag.GrossRevenue_cancelled = grossRevenue_cancelled.ToString("C");

            decimal promoDiscount_cancelled = (from l in list where l.Status == "canceled" select l.PromoCodeDiscount).Sum();
            ViewBag.Discount_cancelled = promoDiscount_cancelled.ToString("C");

            decimal promoPercent_cancelled = (grossRevenue == 0) ? 0 : promoDiscount_cancelled / grossRevenue;
            ViewBag.Percent_cancelled = (grossRevenue == 0) ? "n/a" : promoPercent_cancelled.ToString("P1");

            decimal revenue_cancelled = (from l in list where l.Status == "canceled" select l.Revenue).Sum();
            ViewBag.Revenue_cancelled = revenue_cancelled.ToString("C");

            decimal collected_cancelled = (from l in list where l.Status == "canceled" select l.Collected).Sum();
            ViewBag.Collected_cancelled = collected_cancelled.ToString("C");

            decimal toBeCollected_cancelled = (from l in list where l.Status == "canceled" select l.ToBeCollected).Sum();
            ViewBag.ToBeCollected_cancelled = toBeCollected_cancelled.ToString("C");

            decimal unCollectable_cancelled = (from l in list where l.Status == "canceled" select l.UnCollectable).Sum();
            ViewBag.UnCollectable_cancelled = unCollectable_cancelled.ToString("C");

            decimal writtenOff_cancelled = (from l in list where l.Status == "canceled" select l.WrittenOff).Sum();
            ViewBag.WrittenOff_cancelled = writtenOff_cancelled.ToString("C");

            // #tabWrittenOff
            decimal grossRevenue_writtenoff = (from l in list where l.Status == "writtenoff" select l.GrossRevenue).Sum();
            ViewBag.GrossRevenue_writtenoff = grossRevenue_writtenoff.ToString("C");

            decimal promoDiscount_writtenoff = (from l in list where l.Status == "writtenoff" select l.PromoCodeDiscount).Sum();
            ViewBag.Discount_writtenoff = promoDiscount_writtenoff.ToString("C");

            decimal promoPercent_writtenoff = (grossRevenue == 0) ? 0 : promoDiscount_writtenoff / grossRevenue;
            ViewBag.Percent_writtenoff = (grossRevenue == 0) ? "n/a" : promoPercent_writtenoff.ToString("P1");

            decimal revenue_writtenoff = (from l in list where l.Status == "writtenoff" select l.Revenue).Sum();
            ViewBag.Revenue_writtenoff = revenue_writtenoff.ToString("C");

            decimal collected_writtenoff = (from l in list where l.Status == "writtenoff" select l.Collected).Sum();
            ViewBag.Collected_writtenoff = collected_writtenoff.ToString("C");

            decimal toBeCollected_writtenoff = (from l in list where l.Status == "writtenoff" select l.ToBeCollected).Sum();
            ViewBag.ToBeCollected_writtenoff = toBeCollected_writtenoff.ToString("C");

            decimal unCollectable_writtenoff = (from l in list where l.Status == "writtenoff" select l.UnCollectable).Sum();
            ViewBag.UnCollectable_writtenoff = unCollectable_writtenoff.ToString("C");

            decimal writtenOff_writtenoff = (from l in list where l.Status == "writtenoff" select l.WrittenOff).Sum();
            ViewBag.WrittenOff_writtenoff = writtenOff_writtenoff.ToString("C");

            return View(list);
        }

        public ActionResult ClubRevenueByOrder(string paramSearch)
        {
            List<FMWebApp451.ViewModels.ClubRevenueByOrder> list = new List<FMWebApp451.ViewModels.ClubRevenueByOrder>();

            DateTime endDateTmp = DateTime.Now;
            DateTime startDateTmp = Convert.ToDateTime("7/6/15");
            string str0 = "";
            string str1 = "";

            //check for startDate
            if (!String.IsNullOrEmpty(paramSearch))
            {
                string paramStr = paramSearch;
                if (!paramStr.Contains("..."))
                {
                    paramStr = paramStr.Replace("-", " ");
                    paramStr = System.Text.RegularExpressions.Regex.Replace(paramStr, @"\s+", " ");
                    string[] strList = paramStr.Split(' '); // could be start & end dates
                    if (strList.Length > 0)
                    {
                        str0 = strList[0];
                    }
                    if (strList.Length > 1)
                    {
                        str1 = strList[1];
                    }

                    // check for a date range
                    if (!DateTime.TryParse(str0, out startDateTmp))
                    {
                        ViewBag.ErrMsg = "startDate must be of the form mm/dd/yyyy";
                        return View();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(str1)) //now check for an endDate
                        {
                            if (!DateTime.TryParse(str1, out endDateTmp))
                            {
                                ViewBag.ErrMsg = "endDate must be of the form mm/dd/yyyy";
                                return View();
                            }
                            else
                            {
                                endDateTmp = endDateTmp.Date.AddDays(1).AddTicks(-1); //get just before midnight
                            }
                        }
                    }
                }
            }

            if (endDateTmp < startDateTmp)
            {
                ViewBag.ErrMsg = "startDate must be before EndDate";
                return View();
            }

            list = FMWebApp451.ViewModels.ClubRevenueByOrder.GetList(startDateTmp, endDateTmp);
            list = list.OrderByDescending(x => x.Status).ToList();
            ViewBag.StartDate = startDateTmp.ToString("D");
            ViewBag.EndDate = endDateTmp.ToString("D");

            int totalOrders = (from l in list select l.OrderId).Count();
            ViewBag.TotalOrders = totalOrders.ToString();

            int totalPastDue = (from l in list
                                where l.UnCollectable != 0
                                select l).Count();
            ViewBag.TotalPastDue = totalPastDue.ToString();

            decimal percentPastDue = (totalOrders == 0) ? 0 : Convert.ToDecimal(totalPastDue) / Convert.ToDecimal(totalOrders);
            ViewBag.PastDuePercent = (totalOrders == 0) ? "n/a" : percentPastDue.ToString("P1");

            int completed = (from l in list where l.Status == "completed" select l).Count();
            decimal completedPercent = (totalOrders == 0) ? 0 : Convert.ToDecimal(completed) / Convert.ToDecimal(totalOrders);
            ViewBag.Completed = completed.ToString();
            ViewBag.CompletedPercent = (totalOrders == 0) ? "n/a" : completedPercent.ToString("P1");

            int active = (from l in list where l.Status == "active" select l).Count();
            decimal activePercent = (totalOrders == 0) ? 0 : Convert.ToDecimal(active) / Convert.ToDecimal(totalOrders);
            ViewBag.Active = active.ToString();
            ViewBag.ActivePercent = (totalOrders == 0) ? "n/a" : activePercent.ToString("P1");

            //average time in club for active members
            if (active > 0)
            {
                double averageMonths = (from l in list where l.Status == "active" select l).Average(x => DateTime.Now.Subtract(x.OrderDate).Days / (365.25 / 12));
                ViewBag.ATime = averageMonths.ToString("F2");
            }
            else
            {
                ViewBag.ATime = "N/A (no active subscribers)";
            }

            List<DelayedPlanChanges> dpcList = new List<Models.DelayedPlanChanges>();
            dpcList = FM2015.Models.DelayedPlanChanges.GetDelayedPlanChangesList();
            int deferredTotal = (from dpc in dpcList where (!dpc.Activated && !dpc.Deleted) select dpc).Count();
            /*
            int deferredTotal = 0;
            foreach (FMWebApp451.ViewModels.ClubRevenueByOrder clubOrder in list)
            {
                int customerID = Order.GetCustomerID(clubOrder.OrderId);
                int count = (from dpc in dpcList where dpc.CustomerID == customerID && !dpc.Activated select dpc).Count();
                if (count > 0)
                { deferredTotal++; }
            }
            */
            ViewBag.Deferred = deferredTotal.ToString();
            decimal DeferredPercent = (totalOrders == 0) ? 0 : Convert.ToDecimal(deferredTotal) / Convert.ToDecimal(totalOrders);
            ViewBag.DeferredPercent = (totalOrders == 0) ? "n/a" : DeferredPercent.ToString("P1");

            int cancelled = (from l in list where l.Status == "canceled" select l).Count();
            cancelled = cancelled - deferredTotal;
            decimal cancelledPercent = (totalOrders == 0) ? 0 : Convert.ToDecimal(cancelled) / Convert.ToDecimal(totalOrders);
            ViewBag.Cancelled = cancelled.ToString();
            ViewBag.CancelledPercent = (totalOrders == 0) ? "n/a" : cancelledPercent.ToString("P1");

            decimal grossRevenue = (from l in list select l.GrossRevenue).Sum();
            ViewBag.GrossRevenue = grossRevenue.ToString("C");

            decimal promoDiscount = (from l in list select l.PromoCodeDiscount).Sum();
            ViewBag.Discount = promoDiscount.ToString("C");

            decimal promoPercent = (grossRevenue == 0) ? 0 : promoDiscount / grossRevenue;
            ViewBag.Percent = (grossRevenue == 0) ? "n/a" : promoPercent.ToString("P1");

            decimal revenue = (from l in list select l.Revenue).Sum();
            ViewBag.Revenue = revenue.ToString("C");

            decimal averageSellingPrice = (totalOrders == 0) ? 0 : revenue / Convert.ToDecimal(totalOrders);
            ViewBag.ASP = averageSellingPrice.ToString("C");

            decimal collected = (from l in list select l.Collected).Sum();
            decimal collectedPercent = (revenue == 0) ? 0 : collected / revenue;
            ViewBag.Collected = collected.ToString("C");
            ViewBag.CollectedPercent = (revenue == 0) ? "n/a" : collectedPercent.ToString("P1");

            decimal toBeCollected = (from l in list select l.ToBeCollected).Sum();
            decimal tobeCollectedPercent = (revenue == 0) ? 0 : toBeCollected / revenue;
            ViewBag.ToBeCollected = toBeCollected.ToString("C");
            ViewBag.ToBeCollectedPercent = (revenue == 0) ? "n/a" : tobeCollectedPercent.ToString("P1");

            decimal unCollectable = (from l in list select l.UnCollectable).Sum();
            decimal unCollectablePercent = (revenue == 0) ? 0 : unCollectable / revenue;
            ViewBag.UnCollectable = unCollectable.ToString("C");
            ViewBag.UnCollectablePercent = (revenue == 0) ? "n/a" : unCollectablePercent.ToString("P1");

            return View(list);
        }

        public ActionResult DelayedPlanChanges(string startDate, string endDate)
        {
            List<FM2015.Models.DelayedPlanChanges> dpcList = FM2015.Models.DelayedPlanChanges.GetDelayedPlanChangesList();
            return View(dpcList);
        }

        public ActionResult DisplayPromoCodes()
        {
            IEnumerable<StripeCoupon> stripeCouponList = new List<StripeCoupon>();
            StripeCouponListOptions stripeCouponListOptions = new StripeCouponListOptions(); //*******
            stripeCouponListOptions.Limit = 40;
            var couponService = new StripeCouponService();
            try
            {
                stripeCouponList = couponService.List(stripeCouponListOptions);  // optional StripeListOptions
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Report Controller: Promo Code Lookup Error: Coupon list can't be found");
            }
            return View(stripeCouponList);
        }

        public ActionResult SendDunning(int id, bool test)
        {
            decimal amountDue = 0;
            //add CER
            //add OrderNote
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(id, false);
            StripeCustomer stripeCustomer = Subscriptions.GetStripeCustomerFromEmail(cart.SiteCustomer.Email);
            StripeInvoice stripeInvoice = new StripeInvoice();
            stripeInvoice = Subscriptions.GetLastInvoice(stripeCustomer.Id);
            if (stripeInvoice != null)
            {
                if (stripeInvoice.AmountDue > 0)
                {
                    amountDue = Convert.ToDecimal(stripeInvoice.AmountDue) / 100;
                }
            }
            if (!mvc_Mail.Send2DeclineEmail2(cart.SiteOrder, amountDue, cart.Tran.CardNo, DateTime.Now, test))
            {
                ViewBag.ErrMsg = "<h4 style='color: red'>Failure: Email Receipt was not sent successfully.</h4>";
                ViewBag.ErrMsg += "Check the email address and order number then try again. If the problem persists contact IT" + Environment.NewLine;
                ViewBag.ErrMsg += Session["EmailDunningCopy"].ToString();
                string str = ViewBag.ErrMsg;
                str = str.Replace("\n", "<br />");
                str = str.Replace("\r", "<br />");
                ViewBag.ErrMsg = str;
            }
            else
            {
                ViewBag.ErrMsg = "<h4 style='color: green'>Success: Email was sent OK!</h4>";
                ViewBag.ErrMsg += Session["EmailDunningCopy"].ToString();
                string str = ViewBag.ErrMsg;
                str = str.Replace("\n", "<br />");
                str = str.Replace("\r", "<br />");
                ViewBag.ErrMsg = str;

                adminCER.CER cer = new adminCER.CER(cart);
                cer.EventDescription = "Sent Dunning Letter" + Environment.NewLine;
                cer.EventDescription += Session["EmailDunningCopy"].ToString();
                cer.ActionExplaination = "Sent Dunning Letter";
                int result = cer.Add();

            }
            return View();
        }

        public ActionResult SendFinalDunning(int id)
        {
            DunningViewModel dunning = new DunningViewModel(id);
            dunning.DunningBody = GetDunningBody(id);
            return View(dunning);
        }

        // POST: SendFinalDunnin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendFinalDunning(DunningViewModel dunning)
        {
            if (ModelState.IsValid)
            {
                string emailBody = dunning.DunningBody;
                string mailFrom = AdminCart.Config.MailFrom();
                string mailTo = "rmohme@sbcglobal.net"; //assume test
                string subject = "FINAL NOTICE: PAST DUE";
                if (mvc_Mail.SendMail(mailTo, mailFrom, AdminCart.Config.appName, "", subject, emailBody, true))
                {
                    Cart cart = new Cart();
                    cart = cart.GetCartFromOrder(dunning.OrderId, false);
                    adminCER.CER cer = new adminCER.CER(cart);
                    cer.EventDescription = "Sent FINAL Dunning Letter" + Environment.NewLine;
                    cer.EventDescription += emailBody;
                    cer.ActionExplaination = "Sent FINAL Dunning Letter";
                    int result = cer.Add();

                    return RedirectToAction("MultiPayRevenueByOrder", "Report", null );
                }
            }

            ViewBag.ErrMsg = "<h4 style='color: red'>Failure: Email FINAL DUNNING was not sent successfully.</h4>";
            ViewBag.ErrMsg += "Check the email address, order number, etc. and then try again. If the problem persists contact IT";
            return View(dunning);
        }

        public string GetDunningBody(int orderId)
        {
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(orderId, false);
            Order order = cart.SiteOrder;
            Customer c = cart.SiteCustomer;
            Address a = cart.BillAddress;
            string addressStr = a.Street + " <br />" + a.City + " " + a.State + " " + a.Zip;
            string responseDateStr = DateTime.Now.AddDays(14).ToString("D");
            string cardNumStr = cart.Tran.CardNo.Substring(cart.Tran.CardNo.Length-4, 4);
            string orderStr = order.OrderID.ToString() + " / " + order.SSOrderID.ToString();

            string emailBody = CustomerEmailFinalDeclineFMUpdate;
            emailBody = emailBody.Replace("%NAME%", c.FirstName + " " + c.LastName);
            emailBody = emailBody.Replace("%ADDRESS%", addressStr);
            emailBody = emailBody.Replace("%COMPANY%", AdminCart.Config.appName);
            emailBody = emailBody.Replace("%ORDERNUMBER%", orderStr);
            decimal amount = order.Total;
            emailBody = emailBody.Replace("%AMOUNTDUE%", amount.ToString("C"));
            emailBody = emailBody.Replace("%RESPONSEDATE%", responseDateStr);
            emailBody = emailBody.Replace("%CARDNUMBER%", cardNumStr);
            emailBody = emailBody.Replace("%COMPANYPHONE%", AdminCart.Config.AppPhoneNumber());
            string url = "https://fmadmin.facemaster.com/ccupdate/index?customerid=" + order.CustomerID.ToString();
            emailBody = emailBody.Replace("%UPDATEURL%", url);

            return emailBody;
        }

        public static bool SendEmailDunning(int orderId, string body)
        {
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(orderId);

            string emailCopy = "To: " + cart.SiteCustomer.Email + "\n";
            emailCopy += "From: " + AdminCart.Config.MailFrom() + "\n";
            emailCopy += "Subject: Return Merchandise Authorization (RMA) #" + cart.OrderID.ToString() + "\n/n";
            emailCopy += body;
            //HttpContext.Current.Session["EmailDunningCopy"] = emailCopy;

            bool ok = mvc_Mail.SendMail(cart.SiteCustomer.Email, AdminCart.Config.MailFrom(), "", "facemaster@slccompanies.com", "Return Merchandise Authorization", body);

            if (ok)
            {
                //add a new entry into the CER database
                adminCER.CER cer = new adminCER.CER(cart);
                cer.EventDescription = body;
                int result = cer.Add();
            }

            return ok;
        }

        public FileResult genPackingLists()
        {
            //http://stackoverflow.com/questions/1510451/how-to-return-pdf-to-browser-in-mvc
            //http://www.dotnetspider.com/resources/45903-PDF-Generation-of-MVC-VIew-Using-iTextSharp.aspx
            //http://www.4guysfromrolla.com/articles/030911-1.aspx
            //https://www.simple-talk.com/dotnet/asp.net/asp.net-mvc-action-results-and-pdf-content/
            //http://developers.itextpdf.com/examples

            //get unshipped orders
            List<FM2015.FMmetrics.UnProcessedOrders> orderList = new List<FM2015.FMmetrics.UnProcessedOrders>();
            orderList = FM2015.FMmetrics.mvc_Metrics.GetALLUnshippedOrdersList();
            MemoryStream pdf = Reports.genPackingListPDF(orderList);
            //string filePath = Reports.genPackingListPDF(orderList);

            pdf.Flush(); //Always catches me out
            pdf.Position = 0; //Not sure if this is required

            return File(pdf, "application/pdf");
        }

        public ActionResult DisplayStaleCustomers()
        {
            List<OrdersWithDetail> odList = odProvider.GetMany(DateTime.Now.AddMonths(Config.stale_startMonthsBack));

            List<OrdersWithDetail> targetList = StaleCustomers.GetList(odList);

            List<StaleCustomerViewModel> theList = new List<StaleCustomerViewModel>();
            foreach (OrdersWithDetail od in targetList)
            {
                StaleCustomerViewModel obj = new StaleCustomerViewModel();
                obj.customerId = od.CustomerId;
                obj.OrderDate = od.OrderDate;
                obj.OrderId = od.OrdersWithDetailsId;
                obj.email = od.Email;
                obj.firstName = od.FirstName;
                obj.lastName = od.LastName;
                theList.Add(obj);
            }
            return View(theList);
        }

        public static string CustomerEmailFinalDeclineFMUpdate = @"
<p>
    %NAME% <br />
    %ADDRESS%
</p>

<p>Dear %NAME%,</p>

<p>re: %COMPANY% order# %ORDERNUMBER%</p>

<p>Amount Due: %AMOUNTDUE%</p>

<p>The credit card number being declined is (last 4 digits: %CARDNUMBER%)</p>

<p>
Recently, your attention was called upon regarding the above referenced invoice via several emails, 
the last one dated %LASTEMAILDATE%. The amount of %AMOUNTDUE% is now considerably past due and we are concerned that, 
to-date, we have not heard back from you.
</p>

<p>Your immediate attention is sought to this urgent matter.</p>

<p>
We must receive payment immediately to keep your credit in good standing with us. 
Please use either of these two options to provide your payment to us:
</p>

<ul>
<li>You can click here <a href='%UPDATEURL%' target='_blank'>FaceMaster Payment Method Update</a> 
and update your credit card information online</li>
<li>Give us a call at %COMPANYPHONE% during our normal business hours (M-F 9:30am - 5:30pm Pacific / 12:30pm - 8:30pm Eastern; 
Holidays excluded) and one of our customer service team members will be happy to assist you making your payment</li>
</ul>

<p>
We wish to hear from you and resolve this matter within ten (10) business days (no later than %RESPONSEDATE%). 
Failure to do so will cause us to pass-on your seriously delinquent account to a third party collection agency. 
This could further jeopardize your credit rating.
</p>

<p>
We trust that this will not be necessary.
</p>

<p>
Once again, we remind you that this is our Final Notice.
</p>

<p>
Yours Truly,<br /><br />
Mr. H.T. Schmidt <br />
Accounts Receivable, Finance <br />
FaceMaster of Beverly Hills, Inc. <br />
21440 Osborne St. <br />
Canoga Park, CA 91304-1520 <br />
phone: (800) 770-2521 (M-F, 9:30pm-5:30pm Pacific Time, holidays excluded)<br />
email: facemaster@slccompanies.com <br />
</p>
";

    }
}