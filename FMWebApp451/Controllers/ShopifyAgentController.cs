﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FM2015.Models;
using Stripe;
using System.IO;
using System.Web;
using AdminCart;

namespace FMWebApp.Controllers
{
    public class ShopifyAgentController : ApiController
    {
        private class ShopID
        {
            [JsonProperty("ShopifyOrderID")]
            public string id { get; set; }
        }

        public string Index()
        {
            string response = "(" + AdminCart.Config.appName + ") Current AppVersion: " + ConfigurationManager.AppSettings["AppVersion"].ToString();

            return response;
        }

        //GET version
        //[HttpGet]
        public string GetPing()
        {
            string response = "(" + AdminCart.Config.appName + ") Current AppVersion: " + ConfigurationManager.AppSettings["AppVersion"].ToString();
            return response;
        }

        //GET version
        //[HttpGet]
        public string GetAssyVersion()
        {
            string response = ConfigurationManager.AppSettings["AppName"].ToString() + ": ";
            response += "Current Assembly Version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            return response;
        }

        //public HttpResponseMessage TranslateOrder([FromBody]dynamic value)
        public HttpResponseMessage TranslateOrder(string id, int companyID)
        {

            //var buffer = JObject.Parse(value.ToString());
            //string id = buffer.ToObject<ShopID>().id;

            ShopifyFMOrderTranslator.MergeOrders(id, companyID);

            string content = Request.Content.ToString();

            if (content == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid order id");
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, "translated/inserted order id =" + id);
            return response;
        }

        [HttpPost]
        public HttpResponseMessage ShopifyWHOrderCreation([FromBody]dynamic value)
        {

            ShopifyAgent.ShopifyOrder order = new ShopifyAgent.ShopifyOrder();
            string rqstIP = "";
            string msg = "";
            rqstIP = (HttpContext.Current.Request.UserHostAddress != null) ? HttpContext.Current.Request.UserHostAddress : String.Empty;
 
            if (string.IsNullOrEmpty(value.ToString()))
            {
                msg = "Shopify WebHook Event: value is Null or Empty" + Environment.NewLine;
                msg += "Sent FROM IP: " + rqstIP + Environment.NewLine;
                mvc_Mail.SendDiagEmail("api: ShopifyAgent: Shopify WebHooks: Order Creation", msg);

                return Request.CreateResponse(HttpStatusCode.BadRequest, "Shopify WebHook Json Content = null");
            }

            try
            {
                order = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyOrder>(value.ToString());

                msg = "Shopify WebHook Event" + Environment.NewLine;
                msg += "Event Type: " + "Order Creation" + Environment.NewLine;
                msg += "Sent FROM IP: " + rqstIP + Environment.NewLine;
                msg += "Time Created: " + DateTime.Now.ToString() + Environment.NewLine;
                msg += "Order: Name: " + order.Name + Environment.NewLine;
                msg += "Order: Email: " + order.Email + Environment.NewLine;
                msg += "Order: Number: " + order.Number + Environment.NewLine;
                msg += "Order: Total: " + order.Total_price + Environment.NewLine;
                msg += "Order: ID: " + order.Id + Environment.NewLine;
                msg += "raw json data: " + value.ToString() + Environment.NewLine;
                mvc_Mail.SendDiagEmail("api: ShopifyAgent: Shopify WebHooks: Order Creation", msg);

            }
            catch (Exception ex)
            {
                //put some exception handling code here
                rqstIP = (HttpContext.Current.Request.UserHostAddress != null) ? HttpContext.Current.Request.UserHostAddress : String.Empty;
                msg = "error in ShopifyWHOrderCreation" + Environment.NewLine;
                msg += "Sent FROM IP: " + rqstIP + Environment.NewLine;
                msg += "Time Created: " + DateTime.Now.ToString() + Environment.NewLine;
                msg += "raw json data: " + value.ToString() + Environment.NewLine;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            return Request.CreateResponse(HttpStatusCode.Created, "Shopify WebHook Processed OK");
        }

        [HttpGet]
        public HttpResponseMessage StripeWebHooks()
        {
            string response = "StripeWebHook: GET (" + AdminCart.Config.appName + ") Current AppVersion: " + ConfigurationManager.AppSettings["AppVersion"].ToString();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage ShopifyWebHooks()
        {
            string response = "ShopifyWebHook: GET (" + AdminCart.Config.appName + ") Current AppVersion: " + ConfigurationManager.AppSettings["AppVersion"].ToString();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        public HttpResponseMessage ShopifyWebHooks_OrderCreation([FromBody]dynamic value)
        {
            //https://github.com/nozzlegear/ShopifySharp

            if (string.IsNullOrEmpty(value.ToString()))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Shopify WebHook Json Content = null");
            }

            ShopifyAgent.ShopifyOrder order = FMWebApp.Helpers.ShopifyWebHookHelpers.GetOrderCreationFromWebHook(value.ToString());
            if (order.Id == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Shopify WebHook JSON Order Conversion Failed: Id = 0");
            }

            return Request.CreateResponse(HttpStatusCode.OK, "Shopify WebHook Parsed: Order Creation");
        }

        [HttpPost]
        public HttpResponseMessage ShopifyWebHooks_OrderUpdate([FromBody]dynamic value)
        {
            //https://github.com/nozzlegear/ShopifySharp

            if (string.IsNullOrEmpty(value.ToString()))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Shopify WebHook Json Content = null");
            }

            ShopifyAgent.ShopifyOrder order = FMWebApp.Helpers.ShopifyWebHookHelpers.GetOrderUpdateFromWebHook(value.ToString());
            if (order.Id == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Shopify WebHook JSON Order Conversion Failed: Id = 0");
            }

            return Request.CreateResponse(HttpStatusCode.OK, "Shopify WebHook Parsed: Order Update");
        }

        [HttpPost]
        public HttpResponseMessage ShopifyWebHooks_ProductCreation([FromBody]dynamic value)
        {
            //https://github.com/nozzlegear/ShopifySharp

            if (string.IsNullOrEmpty(value.ToString()))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Shopify WebHook Json Content = null");
            }

            ShopifyAgent.ShopifyProduct product = FMWebApp.Helpers.ShopifyWebHookHelpers.GetProductCreationFromWebHook(value.ToString());
            if (product.Id == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Shopify WebHook JSON Product Conversion Failed: Id = 0");
            }

            return Request.CreateResponse(HttpStatusCode.OK, "Shopify WebHook Parsed: Product Creations");
        }

        [HttpPost]
        public HttpResponseMessage ShopifyWebHooks_ProductUpdate([FromBody]dynamic value)
        {
            //https://github.com/nozzlegear/ShopifySharp

            if (string.IsNullOrEmpty(value.ToString()))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Shopify WebHook Json Content = null");
            }

            ShopifyAgent.ShopifyProduct product = FMWebApp.Helpers.ShopifyWebHookHelpers.GetProductUpdateFromWebHook(value.ToString());
            if (product.Id == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Shopify WebHook JSON Product Conversion Failed: Id = 0");
            }

            return Request.CreateResponse(HttpStatusCode.OK, "Shopify WebHook Parsed: Product Creations");
        }

        [HttpPost]
        public HttpResponseMessage ShopifyWebHooks_RefundCreation([FromBody]dynamic value)
        {
            //https://github.com/nozzlegear/ShopifySharp

            if (string.IsNullOrEmpty(value.ToString()))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Shopify WebHook Json Content = null");
            }

            ShopifyAgent.ShopifyRefund refund = FMWebApp.Helpers.ShopifyWebHookHelpers.GetRefundCreationFromWebHook(value.ToString());
            if (refund.Id == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Shopify WebHook JSON Order Refund Conversion Failed: Id = 0");
            }

            return Request.CreateResponse(HttpStatusCode.OK, "Shopify WebHook Parsed: Refund Creation");
        }

        [HttpPost]
        public HttpResponseMessage StripeWebHooks([FromBody]dynamic value)
        {

            if (string.IsNullOrEmpty(value.ToString()))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Stripe WebHook Json Content = null");
            } 
            
            StripeEvent stripeEvent = new StripeEvent();
            try
            {
                stripeEvent = StripeEventUtility.ParseEvent(value.ToString());
            }
            catch (StripeException ex)
            {
                FM2015.Helpers.Helpers.ProcessStripeException(ex, "EventUtility.ParseEvent(value.ToString())");
                return Request.CreateResponse(HttpStatusCode.NotFound, "Stripe WebHook: StripeEvent Not Parsed Correctly");                
            }

            bool isTest = false; //assume live mode
            if ((stripeEvent.LiveMode == null) || (stripeEvent.LiveMode == false) || (stripeEvent.Id == "evt_00000000000000"))
            { isTest = true; }

            string testStr = (isTest) ? "TEST" : string.Empty;

            List<int> list = ShopifyAgent.ShopifyAgent.SyncShopify(); //sync orders from Shopify

            AdminCart.Cart cart = new AdminCart.Cart();
            bool isMultiPay = false;
            bool isSubscription = false;

            StripeCharge stripeCharge = new StripeCharge();
            StripeCard stripeCard = new StripeCard();
            StripeSubscription stripeSubscription = new StripeSubscription();
            StripeInvoice stripeInvoice = new StripeInvoice();
            Subscriptions newSubUpdate = new Subscriptions();

            if (isTest)
            {
                FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook");
                FM2015.Models.StripeWebHooks.ProcessWebHook(stripeEvent, value.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: Subscription Parsed");
            }

            switch (stripeEvent.Type)
            {
                // all of the types available are listed in StripeEvents
                case StripeEvents.CustomerSubscriptionUpdated:

                    try
                    {
                        stripeSubscription = Stripe.Mapper<StripeSubscription>.MapFromJson(stripeEvent.Data.Object.ToString());
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, "Subscription.MapFrom():" + testStr + "SubscriptionUpdated");
                        if (isTest)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: Subscription Parsed");
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, "Stripe " + testStr + " WebHook: StripeSubscription Not Parsed Correctly");
                    }
                    newSubUpdate = new Subscriptions(stripeSubscription.Id, stripeSubscription.CustomerId);
                    if (newSubUpdate.ID == 0)
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "WebHook: Subscription/Customer pair not found in Subscriptions Table");
                        if (isTest)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: Subscriptions table error");
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, "Stripe" + testStr + "WebHook: Subscription/Customer pair not found in Subscriptions Table");
                    }
                    newSubUpdate.StripeStatus = stripeSubscription.Status;
                    if (!isTest)
                    {
                        newSubUpdate.Save(newSubUpdate.ID);
                    }
                    if ((stripeSubscription.Status == "past_due") || (stripeSubscription.Status == "unpaid"))
                    {
                        decimal amountDue = Convert.ToDecimal(stripeSubscription.StripePlan.Amount) / 100;
                        // AdminCart.Order order = new AdminCart.Order(90954);
                        AdminCart.Order order = new AdminCart.Order(newSubUpdate.OrderID);
                        AdminCart.Cart pastDueCart = new AdminCart.Cart();
                        pastDueCart = pastDueCart.GetCartFromOrder(newSubUpdate.OrderID, false);
                        if (mvc_Mail.Send2DeclineEmail2(order, amountDue, pastDueCart.Tran.CardNo, DateTime.Now, false))
                        {
                            adminCER.CER cer = new adminCER.CER(pastDueCart)
                            {
                                EventDescription = "Send Dunning Letter" + Environment.NewLine
                            };
                            cer.EventDescription += HttpContext.Current.Session["EmailDunningCopy"].ToString();
                            cer.ActionExplaination = "Sent Dunning Letter";

                            int result = cer.Add();                           
                        }
                    }
                    //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Successfull Subscription Status Update");
                    break;

                case StripeEvents.ChargeRefunded: 
                    try
                    {
                        stripeCharge = Stripe.Mapper<StripeCharge>.MapFromJson(stripeEvent.Data.Object.ToString());
                        //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: ChargeRefunded:unimplemented");
                    }
                    catch(StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "Charge.MapFrom(): Refund");
                        if (isTest)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Test Modein Live Mode: StripeStripeCharge.MapFrom(): Refund");
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeCharge:Refund Not Parsed Correctly");
                    }
                    break;

                case StripeEvents.CustomerSubscriptionDeleted:
                    try
                    {
                        stripeSubscription = Stripe.Mapper<StripeSubscription>.MapFromJson(stripeEvent.Data.Object.ToString());
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, "Subscription.MapFrom():" + testStr + "    ");
                        if (isTest)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Test Modein Live Mode: StripeStripeCharge.MapFrom(): subscription deleted");                       
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, "Stripe " + testStr + " WebHook: StripeSubscription Not Parsed Correctly");
                    }
                    newSubUpdate = new Subscriptions(stripeSubscription.Id, stripeSubscription.CustomerId);
                    if (newSubUpdate.ID == 0)
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Subscription_Deleted: Multi-Pay Subscription Not Found");
                        return Request.CreateResponse(HttpStatusCode.NotFound, "Stripe" + testStr + "WebHook: StripeSubscription Not Parsed Correctly");
                    }
                    newSubUpdate.StripeStatus = stripeSubscription.Status;
                    if (!isTest)
                    {
                        newSubUpdate.Save(newSubUpdate.ID);
                    }

                    //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: Subscription Deleted");
                    break;

                case StripeEvents.ChargeSucceeded:
                    try
                    {
                        stripeCharge = Stripe.Mapper<StripeCharge>.MapFromJson(stripeEvent.Data.Object.ToString());
                    }
                    catch(StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "ChargeSucceeded: Failed StripeCharge.MapFrom())");
                        if (isTest)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: ChargeSucceeded: Failed StripeCharge.MapFrom()");
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeCharge:Succeeded: StripeCharge Not Parsed Correctly");                
                    }

                    StripeInvoiceService invoiceService = new StripeInvoiceService();
                    if (stripeCharge.InvoiceId != null)
                    {
                        try
                        {
                            stripeInvoice = invoiceService.Get(stripeCharge.InvoiceId);
                        }
                        catch (StripeException ex)
                        {
                            FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "ChargeSucceeded: Failed InvoiceService.GET");
                            if (isTest)
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: ChargeSucceeded: Failed InvoiceService.GET");
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeCharge:Succeeded Failed InvoiceService.GET");
                        }
                    }

                    string subscriptionID = "";
                    try
                    {
                        subscriptionID = stripeInvoice.SubscriptionId;
                    }
                    catch
                    { ; } //ignore exception if not a subscription

                    if (!string.IsNullOrEmpty(subscriptionID))
                    {
                        Subscriptions sub = new Subscriptions(subscriptionID, stripeInvoice.CustomerId);
                        if (sub.ID == 0)
                        {
                            FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: ChargeSucceeded: Subscription; failed to find subscription entry");
                            break;
                        }
                        else
                        {
                            //get transaction info from Stripe, store it where the order can find it (Transactions)
                            cart = cart.GetCartFromOrder(sub.OrderID, false);
                            foreach (AdminCart.Item item in cart.Items)
                            {
                                isSubscription = item.LineItemProduct.IsSubscription;
                                isMultiPay = item.LineItemProduct.IsMultiPay;
                                if (isMultiPay)
                                {
                                    break;
                                }
                            }

                            if (isMultiPay)
                            {
                                List<AdminCart.Transaction> tList = AdminCart.Transaction.GetTransactionList(sub.OrderID);
                                if (tList.Count > 0)
                                {
                                    // check if transaction has already been recorded
                                    List<AdminCart.Transaction> tranList = (from t in tList
                                                                            where t.TransactionID == stripeCharge.Id
                                                                            select t).ToList();

                                    if (tranList.Count() == 0) // if not then record it
                                    {
                                        List<AdminCart.Transaction> tFromStripeList = ShopifyFMOrderTranslator.Get24HRStripeTransactions(stripeCharge.CustomerId, cart.SiteOrder);
                                        if (tFromStripeList.Count() > 0)
                                        {
                                            tFromStripeList.OrderByDescending(x => x.TransactionDate);
                                            AdminCart.Transaction tran = tFromStripeList.First();
                                            if (!isTest) //save the transaction if live
                                            {
                                                tran.SaveDB();
                                            }

                                        }
                                        else
                                        {
                                            FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: ChargeSucceeded: Failed to Find StripeTransaction: OrderID =" + cart.SiteOrder.OrderID.ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: ChargeSucceeded: Failed to Find Transaction: OrderID =" + cart.SiteOrder.OrderID.ToString());
                                }
                            } //end subscription charge processing
                        }

                    } //not a multi-pay charge
                    //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: ChargeSucceeded");
                    break;

                case StripeEvents.CustomerSubscriptionCreated:

                    try
                    {
                        stripeSubscription = Stripe.Mapper<StripeSubscription>.MapFromJson(stripeEvent.Data.Object.ToString());
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "SubscriptionCreated: StripeSubscription.MapFrom())");
                        if (isTest)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: SubscriptionCreated: StripeSubscription.MapFrom()");
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeSubscription Not Parsed Correctly");
                    }
                    Subscriptions newSub = new Subscriptions
                    {
                        StripeSubID = stripeSubscription.Id,
                        StripeCusID = stripeSubscription.CustomerId,
                        StripePlanID = stripeSubscription.StripePlan.Id
                    };
                    try
                    {
                        newSub.StripePromoCode = stripeSubscription.StripeDiscount.StripeCoupon.Id;
                    }
                    catch
                    { ; } //if no coupon then ignore

                    StripeCustomer stripeCustomer = new StripeCustomer();
                    try
                    {
                        var customerService = new StripeCustomerService();
                        stripeCustomer = customerService.Get(newSub.StripeCusID);       
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "SubscriptionCreated: customerService.Get())");
                        if (isTest)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: subscription created: customerService.Get()");
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeCustomer Not Parsed Correctly");
                    }

                    AdminCart.Customer c = AdminCart.Customer.FindCustomerByEmail(stripeCustomer.Email);
                    if (c.CustomerID == 0)
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Find-Customer-By-Email failed: Email=" + stripeCustomer.Email);
                        if (isTest)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Stripe WebHook: Test Mode in Live Mode: subscription created: Customer Not Found");
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Customer Not Found");
                    }

                    List<AdminCart.Order> oList = AdminCart.Order.FindAllOrders(c.CustomerID, false);
                    if (oList.Count == 0)
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Find-All-Orders Failed: customerID=" + c.CustomerID);
                        return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Order Not Found");
                    }

                    //AdminCart.Order o = oList.OrderByDescending(x => x.OrderDate).ToList().First();
                    try
                    {
                        DateTime dtSubStart = (DateTime)stripeSubscription.Start;
                        dtSubStart = dtSubStart.AddHours(-8); //account for time conversion at Stripe
                        oList = (from order in oList where order.OrderDate > dtSubStart.AddDays(-1) select order).ToList();
                        if (oList.Count == 0) //this is here to catch repeat subscribers; an older purchase may get used by mistake
                        {
                            string subStart = (stripeSubscription.Start == null) ? string.Empty : stripeSubscription.Start.ToString();
                            string msg = testStr + "Find-All-Orders failed: Not Created by Shopify yet? Email=" + stripeCustomer.Email + "Sub.Start: " + subStart;
                            FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), msg);
                            return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Order Not Found: Not Created by Shopify yet?");
                        }
                    }
                    catch
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Find-All-Orders failed: Subscription.Start == null? Email=" + stripeCustomer.Email);
                        return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Order Not Found: Subscription.Start == null?");
                    }

                    if (!FindSubscriptionOrderMatch(oList, stripeSubscription, out AdminCart.Order o, out isSubscription, out isMultiPay))
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Subscription Item in Order Not Found: OrderID=" + o.OrderID);
                        if (isTest)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: subscription created: Subscription Item in Order Not Found");
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Subscription Order Not Found");
                    }
                    else //if (isMultiPay): allow all subscriptions, not just multipay
                    {
                        newSub.OrderID = o.OrderID;
                        newSub.StripeSubDate = o.OrderDate;
                        newSub.StripeStatus = "active";
                        int id = 0;
                        if (!isTest)
                        {
                            id = newSub.Save(0); // insert new subscription if not a test event
                        }
                        
                        if (id == 0)
                        {
                            FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Insert to Subscribe Table Not Successful");
                            if (isTest)
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: subscription created: Insert to Subscribe Table Not Successful");
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Subscription Table Not Updated");
                        }
                    }
                    //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: Successfull Subscription Update");
                    break;

                case StripeEvents.InvoicePaymentSucceeded:
                    try
                    {
                        stripeInvoice = Stripe.Mapper<StripeInvoice>.MapFromJson(stripeEvent.Data.Object.ToString());
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "InvoicePaymentSucceeded: Failed StripeInvoice.MapFrom())");
                        if (isTest)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: InvoicePaymentSucceeded: Failed StripeInvoice.MapFrom()");
                        }
                        return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeInvoice:Succeeded: StripeInvoice Not Parsed Correctly");
                    }

                    invoiceService = new StripeInvoiceService();
                    if (stripeInvoice.Id != null)
                    {
                        try
                        {
                            stripeInvoice = invoiceService.Get(stripeInvoice.Id);
                        }
                        catch (StripeException ex)
                        {
                            FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "InvoicePaymentSucceeded: Failed InvoiceService.GET");
                            if (isTest)
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: InvoicePaymentSucceeded: Failed InvoiceService.GET");
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: InvoicePaymentSucceeded Failed InvoiceService.GET");
                        }
                    }



                    subscriptionID = "";
                    try
                    {
                        subscriptionID = stripeInvoice.SubscriptionId;
                    }
                    catch
                    { ; } //ignore exception if not a subscription

                    if (!string.IsNullOrEmpty(subscriptionID))
                    {
                        Subscriptions sub = new Subscriptions(subscriptionID, stripeInvoice.CustomerId);
                        if (sub.ID == 0)
                        {
                            FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: InvoicePaymentSucceeded: Subscription; failed to find subscription entry");
                            break;
                        }
                        else
                        {
                            //get transaction info from Stripe, store it where the order can find it (Transactions)
                            cart = cart.GetCartFromOrder(sub.OrderID, false);
                            foreach (AdminCart.Item item in cart.Items)
                            {
                                isSubscription = item.LineItemProduct.IsSubscription;
                                isMultiPay = item.LineItemProduct.IsMultiPay;
                                if (isMultiPay)
                                {
                                    break;
                                }
                            }

                            if (isMultiPay)
                            {
                                List<AdminCart.Transaction> tList = AdminCart.Transaction.GetTransactionList(sub.OrderID);
                                if ((tList.Count() >= 3) && (cart.SiteOrder.SourceID > 0))
                                {
                                    int installements = PayWhirl.PayWhirlPlan.GetPlanInstallments(stripeInvoice.CustomerId, stripeInvoice.SubscriptionId);
                                    string msg = "OrderID = " + cart.SiteOrder.OrderID.ToString() + Environment.NewLine;
                                    msg += "Installments = " + installements.ToString() + Environment.NewLine;
                                    msg += "Transaction Count = " + tList.Count().ToString();
                                    FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "ACTION ALERT!!: InvoicePayment: Succeeded: Check for Subscription Completion: " + msg);
                                }
                
                            } //end invoice payment succeeded processing
                        }
                    } 
                    break;

                default:
                    //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Unidentified StripeEvent");
                    break;
            }

            return Request.CreateResponse(HttpStatusCode.OK, "Stripe WebHook Processed OK");
        }

        private bool FindSubscriptionOrderMatch(List<Order> oList, StripeSubscription stripeSubscription, out Order o, out bool isSubscription, out bool isMultiPay)
        {
            bool isOrderMatch = false;
            isSubscription = false;
            isMultiPay = false;
            o = new Order();
            Cart cart = new Cart();
            foreach (AdminCart.Order order in oList)
            {                
                cart = cart.GetCartFromOrder(order.OrderID, false);
                foreach (AdminCart.Item item in cart.Items)
                {
                    isSubscription = item.LineItemProduct.IsSubscription;
                    isMultiPay = item.LineItemProduct.IsMultiPay;
                    isOrderMatch = (item.LineItemProduct.SubscriptionPlan.ToLower() == stripeSubscription.StripePlan.Id.ToLower()) ? true : false;
                    if (isOrderMatch)
                    {
                        break;
                    }
                }
                if (isOrderMatch)
                {
                    o = order;
                    break;
                }
            }
            return isOrderMatch;
        }

        [HttpPost]
        public HttpResponseMessage StripeTestWebHooks([FromBody]dynamic value)
        {
            if (string.IsNullOrEmpty(value.ToString()))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Stripe Test WebHook Json Content = null");
            }

            StripeEvent stripeEvent = new StripeEvent();
            try
            {
                stripeEvent = StripeEventUtility.ParseEvent(value.ToString());
            }
            catch (StripeException ex)
            {
                FM2015.Helpers.Helpers.ProcessStripeException(ex, "StripeEventUtility.ParseEvent(value.ToString())");
                return Request.CreateResponse(HttpStatusCode.NotFound, "Stripe Test WebHook: StripeEvent Not Parsed Correctly");
            }

            FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, value.ToString(), "TEST WEBHOOKS: Deprecated; Use LIVE Webhooks w/ evt_0000000");
            return Request.CreateResponse(HttpStatusCode.OK, "Stripe Test WebHook Processed OK");
        }
    }
}
