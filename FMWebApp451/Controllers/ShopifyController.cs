﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Configuration;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FMWebApp.Helpers;
using Stripe;
using AdminCart;
using FM2015.ViewModels;
using FM2015.Models;
using ShopifyAgent;

namespace FMWebApp.Controllers
{
    public class ShopifyController : Controller
    {
        // GET: Shopify
        public ActionResult Index()
        {
            return View();
        }

        // GET: Shopify Complete Order list
        public ActionResult GetOrders(string shopifyOrderID)
        {
            string url = "";
            string apiResult = "";
            List<ShopifyAgent.ShopifyOrder> orderList = new List<ShopifyAgent.ShopifyOrder>();

            
            if (!string.IsNullOrEmpty(shopifyOrderID))
            {
                if (int.TryParse(shopifyOrderID, out int tempInt))
                {
                    //doesn't look like a long was sent in for shopifyOrderID; assume it's the order name
                    long tempLong = ShopifyAgent.ShopifyOrder.GetShopifyIDFromName(shopifyOrderID, 1);
                    shopifyOrderID = tempLong.ToString();
                }
                //ShopifyAgent.ShopifyOrder shopifyOrder = new ShopifyAgent.ShopifyOrder();
                //long id = ShopifyAgent.ShopifyOrder.GetShopifyIDFromName(shopifyOrderID, 0); //only our own Shopify orders
                long id = Convert.ToInt64(shopifyOrderID);
                return RedirectToAction("OrderDetails", new { id = id });

                //url = "admin/orders/" + id.ToString() + ".json";
                //apiResult = JsonHelpers.GETfromShopify(url);

                //var buffer = JObject.Parse(apiResult);
                //ShopifyAgent.ShopifyOrder order = buffer.ToObject<ShopifyAgent.ShopifyOrderSingle>().order;
                //orderList.Add(order);
            }
            else
            {
                url = "admin/orders.json";
                apiResult = JsonHelpers.GETfromShopify(url);

                ShopifyAgent.ShopifyOrders sos = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyOrders>(apiResult);
                for (int i = 0; i < sos.Orders.Length; i++)
                { orderList.Add(sos.Orders[i]); }
            }

            return View("OrderList", orderList); 
        }

        // Test Running a console app from this controller
        public ActionResult RunShopifyAgentTest()
        {
            ShopifyFMOrderTranslator s = new ShopifyFMOrderTranslator();
            s.RunShopifyAgentTest();
            return RedirectToAction("Index", "AdminMVC");
        }

        // Test Running a console app from this controller
        public ActionResult SendTestEmail()
        {
            mvc_Mail.SendDiagEmail("Test Email from " + AdminCart.Config.AdminName(), "Test email: time sent: " + DateTime.Now.ToString());
            return RedirectToAction("Index", "AdminMVC");
        }


        // GET: Shopify Single Order Details
        public ActionResult OrderDetails(long id)
        {
            string uri = "admin/orders/" + id.ToString() + ".json";
            string apiResult = JsonHelpers.GETfromShopify(uri);
            var buffer = JObject.Parse(apiResult);
            ShopifyAgent.ShopifyOrder order = buffer.ToObject<ShopifyAgent.ShopifyOrderSingle>().order;

            //get Transactions for this order
            uri = "admin/orders/" + id.ToString() + "/transactions.json";
            apiResult = JsonHelpers.GETfromShopify(uri);
            ShopifyAgent.ShopifyTransactions trans = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyTransactions>(apiResult);
            
            if ((trans.Transactions.Length == 0) && (order.Gateway == "cash")) // go look in Stripe for transactions
            {
                List<StripeCharge> stripeChargeList = Utilities.FMHelpers.stripeChargeList(order.Email, "ShopifyController: OrderDetails: GetTransactions");

                    //convert tList to ShopifyTransactions array
                    List<ShopifyAgent.ShopifyTransaction> shopTransList = new List<ShopifyAgent.ShopifyTransaction>();
                    foreach (StripeCharge charge in stripeChargeList)
                    {
                        ShopifyAgent.ShopifyTransaction shopTran = new ShopifyAgent.ShopifyTransaction();

                        shopTran.Amount = Decimal.Divide(charge.Amount, 100); //Stripe does stuff in cents
                        shopTran.Authorization_key = charge.Id;
                        shopTran.Created_at = charge.Created;
                        shopTran.Currency = "USD";
                        shopTran.Device_id = "";
                        shopTran.Error_code = (charge.FailureCode == null) ? "0" : "-1";
                        shopTran.Gateway = "cash";
                        shopTran.Id = 0;
                        shopTran.Kind = (charge.Id.Substring(0, 2).ToUpper() == "CH") ? "sale" : "refund";
                        shopTran.Location_id = "";
                        shopTran.Message = (charge.FailureCode == null) ? (charge.FailureCode + ": " + charge.FailureMessage) : "Approved";
                        shopTran.Order_id = order.Id;
                        shopTran.Parent_id = "";
                        shopTran.ShopifyImportDate = order.ShopifyImportDate;
                        shopTran.ShopifyOrderID = order.ShopifyOrderID;
                        shopTran.ShopifyTransactionID = 0;
                        shopTran.Source_name = "web";
                        shopTran.Status = "success";
                        shopTran.Test = false;
                        shopTran.User_id = "";

                        shopTransList.Add(shopTran);
                    }
                    trans.Transactions = shopTransList.ToArray();
            }

            order.Transactions = trans.Transactions;

            List<ShopifyAgent.ShopOrderRisk> RiskList = ShopifyAgent.ShopOrderRisk.GetOrderRiskList(id, order.CompanyID);
            ViewBag.RiskList = RiskList;

            return View("OrderDetails", order);
        }

        // GET: Shopify Single Order Details
        public ActionResult OrderCancel(long id)
        {
            ShopifyAgent.ShopifyOrder.Cancel(id);
            return RedirectToAction("GetOrders");
        }

        // GET: Shopify Edit Order
        public ActionResult OrderEdit(long id)
        {
            string uri = "admin/orders/" + id.ToString() + ".json";
            string apiResult = JsonHelpers.GETfromShopify(uri);
            var buffer = JObject.Parse(apiResult);
            ShopifyAgent.ShopifyOrder order = buffer.ToObject<ShopifyAgent.ShopifyOrderSingle>().order;

            return View("OrderEdit", order);
        }

        // PUT: Shopify Edit Order
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OrderEdit(ShopifyAgent.ShopifyOrder updatedOrder)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    long result = ShopifyAgent.ShopifyOrder.PutShopifyOrderViaAPI(updatedOrder); //use id=0 to be sure to create new DB entry
                    //if (result <= 0) { dbErrorLogging.LogError(); }
                    return RedirectToAction("GetOrders");
                }
            }
            catch
            {
                return View(updatedOrder);
            }
            return View(updatedOrder);
        }

        // GET: Shopify Complete Customer List
        public ActionResult GetCustomers(string lastName)
        {
            string url = "";
            string apiResult = "";
            List<ShopifyCustomer> customerList = new List<ShopifyCustomer>();

            if(!string.IsNullOrEmpty(lastName))
            {
                url = "admin/customers/search.json?query=last_name:" + lastName;
            }
            else
            {
                url = "admin/customers.json";
            }

            apiResult = JsonHelpers.GETfromShopify(url);
            ShopifyCustomers sos = JsonConvert.DeserializeObject<ShopifyCustomers>(apiResult);
            for (int i = 0; i < sos.Customers.Length; i++)
            { customerList.Add(sos.Customers[i]); }

            return View("CustomerList", customerList);
        }

        // GET: Shopify Single Customer Details
        public ActionResult CustomerDetails(long Id)
        {

            string uri = "admin/customers/" + Id.ToString() + ".json";
            string apiResult = JsonHelpers.GETfromShopify(uri);
            ShopifyCustomer customer = JsonConvert.DeserializeObject<ShopifyCustomerSingle>(apiResult).customer;

            return View("CustomerDetails", customer);

        }

        // GET: Shopify Customer Edit
        public ActionResult CustomerEdit(long Id)
        {
            string uri = "admin/customers/" + Id.ToString() + ".json";
            string apiResult = JsonHelpers.GETfromShopify(uri);
            ShopifyCustomer customer = JsonConvert.DeserializeObject<ShopifyCustomerSingle>(apiResult).customer;

            return View("CustomerEdit", customer);
        }

        // PUT: Shopify Edit Customer
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CustomerEdit(ShopifyCustomer updatedCustomer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    long result = ShopifyCustomer.PutShopifyCustomerViaAPI(updatedCustomer); //use id=0 to be sure to create new DB entry

                    return RedirectToAction("GetCustomers");
                }
            }
            catch
            {
                return View("CustomerEdit", updatedCustomer);
            }
            return View("CustomerEdit", updatedCustomer);
        }

        // GET: Shopify Customer Create
        public ActionResult CustomerCreate()
        {
            return View();
        }

        // PUT: Shopify Customer Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CustomerCreate(ShopifyCustomer customer)
        {
            try
            {                
                if (ModelState.IsValid)
                {
                    long id = ShopifyCustomer.PostShopifyCustomerViaAPI(customer);
                    return RedirectToAction("GetCustomers");
                }
            }
            catch
            {
                return View(customer);
            }
            return View(customer);
        }

        // GET: Shopify Complete Product List
        public ActionResult GetProducts(string param)
        {
            string uri = "admin/products.json";
            string apiResult = JsonHelpers.GETfromShopify(uri);

            List<ShopifyAgent.ShopifyProduct> productList = new List<ShopifyAgent.ShopifyProduct>();
            ShopifyAgent.ShopifyProducts sos = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyProducts>(apiResult);
            for (int i = 0; i < sos.products.Length; i++)
            { productList.Add(sos.products[i]); }

            if (param != null)
            {
                List<ShopifyAgent.ShopifyProduct> searchProductList = (from p in productList where p.Title.ToUpper().Contains(param.ToUpper()) select p).ToList();
                if (searchProductList.Count() == 0)
                {
                    searchProductList = (from p in productList where p.Body_html.ToUpper().Contains(param.ToUpper()) select p).ToList();
                }
                productList = searchProductList;
            }

            productList = productList.OrderByDescending(x => x.Created_at).OrderBy(x => x.Product_type).ToList();
            return View("GetProducts", productList);
        }

        // GET: Shopify Single Customer Details
        public ActionResult ProductDetails(long Id)
        {

            string uri = "admin/products/" + Id.ToString() + ".json";
            string apiResult = JsonHelpers.GETfromShopify(uri);
            var buffer = JObject.Parse(apiResult);
            ShopifyAgent.ShopifyProduct product = buffer.ToObject<ShopifyAgent.ShopifyProductSingle>().product;

            return View("ProductDetails", product);

        }

        // GET: Pull new Shopify Orders into ShopifyFM, sync w/ FM Admin
        public ActionResult ManualShopifyAgent()
        {
            List<int> oList = ShopifyAgent.ShopifyAgent.SyncShopify();
            ViewBag.oList = oList;
            return View();
        }

        // GET: sync a single Shopify order with FM Admin; assumes that order is already in ShopifyFM
        public ActionResult SendToFaceMaster(long id)
        {
            ShopifyFMOrderTranslator.MergeOrders(id.ToString(), Config.CompanyID());
            return Redirect("~/admin/default.aspx");
        }

        // GET: sync a single Shopify order with Crstalift Admin; assumes that order is already in ShopifyFM
        public ActionResult SendToCrystalift(long id)
        {
            ShopifyFMOrderTranslator.MergeOrders(id.ToString(), Config.CompanyID());
            return Redirect("~/admin/default.aspx");
        }

        // Matches ShopifyFM order & orderdetails to FM order & orderdetails; updates FM orderdetails if approved
        public ActionResult RebuildFMOrderDetailsFromShopifyFM(DateTime? startDate, int iterationLimit)
        {
            ViewBag.ErrorMsg = string.Empty;
            ViewBag.StartOrder = string.Empty;
            List<RebuildFMDetailsvm> rebuildFMDetailsvmList = new List<RebuildFMDetailsvm>();
            RebuildFMDetailsReport report = new RebuildFMDetailsReport();


            int userID = FM2015.Models.User.GetUserID();
            FM2015.Models.Roles roles = new FM2015.Models.Roles(userID);
            if (!roles.Role.ToUpper().Contains("ADMIN"))
            {
                ViewBag.ErrorMsg = "Admin credentials are required to enter this page.";
                return View(report);
            }
            if (startDate != null)
            {
                if ((startDate > DateTime.Now) || (startDate < DateTime.Now.AddYears(-3)))
                {
                    ViewBag.ErrorMsg = "StartDate must be between NOW and NOW minus 3 years";
                    return View(report);
                }
            }
            if ((iterationLimit <= 0) || (iterationLimit > 100))
            {
                ViewBag.ErrorMsg = "Update iterations must be between and 100";
                return View(report);
            }

            List<Product> products = Product.GetProducts();
            List<ShopifyOrder> shopifyOrders = ShopifyOrder.GetShopifyOrderList();
            shopifyOrders = (from shop in shopifyOrders where shop.Created_at <= startDate select shop).ToList();
            shopifyOrders = GetDistinctOrders(shopifyOrders);

            //List<ShopifyLineItem> shopifyLineItems = ShopifyLineItem.GetShopifyLineItemList();

            OrderProvider op = new OrderProvider();
            List<AdminCart.Order> orders = op.GetAll();
            List<AdminCart.Order> ordersWithShopifyNames = (from o in orders where o.SSOrderID >= 800000 select o).ToList();
            OrderDetailProvider odp = new OrderDetailProvider();
            List<OrderDetail> orderDetails = odp.GetAll();

            //foreach (ShopifyOrder shopifyOrder in shopifyOrders; start at the most recent and work down)
            for (int i = 0; i < shopifyOrders.Count; i++)
            {
                AdminCart.Order order = null;

                ShopifyOrder shopifyOrder = shopifyOrders[i];
                //find corresponding FM Order ID from ShopifyLineItem
                int orderId = (from o in ordersWithShopifyNames where o.SSOrderID.ToString() == shopifyOrder.Name select o.OrderID).FirstOrDefault();
                if (orderId == 0)
                {
                    RebuildFMDetailsErrors error = new RebuildFMDetailsErrors()
                    {
                        OrderId = orderId,
                        ErrorMsg = "could not find an order with matching Shopify Name & SSOrderId: Shopify Name =" + shopifyOrder.Name,
                        ErrorCode = 0,
                    };
                    report.UnMatchedOrders.Add(error);
                    continue;
                }
                else
                {
                    order = (from o in orders where o.OrderID == orderId select o).FirstOrDefault();
                }
                if ((order == null) || (order.OrderID != orderId) || (order.OrderID == 0))
                {
                    RebuildFMDetailsErrors error = new RebuildFMDetailsErrors()
                    {
                        OrderId = orderId,
                        ErrorMsg = "could not find an order with matching OrderID: OrderID =" + orderId.ToString(),
                        ErrorCode = 0,
                    };
                    report.UnMatchedOrders.Add(error);
                    continue;
                }

                //List<OrderDetail> theFMOrderDetailList = OrderDetail.GetOrderDetails(orderId);
                List<OrderDetail> theFMOrderDetailList = (from od in orderDetails where od.OrderID == orderId select od).ToList();

                if (theFMOrderDetailList.Count != 0)
                {
                    continue; //this order does not need it's od updated
                }

                //get corresponding order detail list
                //List<ShopifyLineItem> theShopifyOrderDetailList = (from shopifyOD in shopifyLineItems where shopifyOD.Name == shopifyOrder.Name select shopifyOD).ToList();
                List<ShopifyLineItem> theShopifyOrderDetailList = GetShopifyOrderLineItems(shopifyOrder.Id);
                //attemp to match order details; results in a list
                theFMOrderDetailList = new List<OrderDetail>();
                bool goodODList = false;
                foreach (ShopifyLineItem item in theShopifyOrderDetailList)
                {
                    OrderDetail od = null;
                    int productID = (from p in products where p.PartnerID == item.Product_id select p.ProductID).FirstOrDefault();
                    if (productID == 0)
                    {
                        //try to find the product from SKU, Name & Price
                        productID = (from p in products where (p.Sku == item.Sku) && (p.Price == item.Price) select p.ProductID).FirstOrDefault();

                    }
                    if (productID == 0)
                    {
                        RebuildFMDetailsErrors error = new RebuildFMDetailsErrors()
                        {
                            OrderId = orderId,
                            ErrorMsg = "ProductID = 0: Can't find matching FM Product; ShopifyLineItemID = " + item.ShopifyLineItemID.ToString() +
                                "; Shopify Product Id = " + item.Product_id.ToString() +
                                " item.Sku = " + item.Sku + " item.Name = " + item.Name + " item.Price = " + item.Price.ToString(),
                            ErrorCode = 0,
                        };
                        report.UnMatchedOrders.Add(error);
                        break;
                    }
                    else
                    {
                        od = (from detail in orderDetails
                              where (detail.ProductID == productID)
                              && (detail.Quantity == item.Quantity)
                              && (detail.UnitPrice == item.Price)
                              && (detail.Discount == 0) //shopify orders don't have lineitem discounts
                              && (detail.OrderID == -113113) // all details that need to be recovered have this orderid
                              select detail).LastOrDefault();
                    }
                    if ((od == null) || (od.OrderDetailID == 0))
                    {
                        Product p = new Product(productID);
                        ProductCost pc = new ProductCost(productID);
                        od = new OrderDetail()
                        {
                            OrderDetailID = 0,
                            OrderID = -113113, //fake the updater out...
                            ProductID = productID,
                            Quantity = item.Quantity,
                            Discount = 0,
                            UnitCost = pc.Cost,
                            UnitPrice = p.Price,
                        };
                        //od.SaveToOrderDetail();
                        theFMOrderDetailList.Add(od); //only looking to sort out oderdetails that were assigned a bogus orderid
                        goodODList = true;
                    }
                    else if (od.OrderID == -113113)
                    {
                        theFMOrderDetailList.Add(od); //only looking to sort out oderdetails that were assigned a bogus orderid
                        orderDetails.Remove(od); //use each orderdetail just once
                        goodODList = true;
                    }
                }

                if (goodODList) //validate; if validated then update
                {
                    // we should now have a candidate for the orderdetails associated with the FM order
                    RebuildFMDetailsvm rebuildFMDetailsvm = new RebuildFMDetailsvm
                    {
                        FMOrder = order,
                        FMOrderDetails = theFMOrderDetailList,
                        ShopifyOrder = shopifyOrder,
                        ShopifyLineItems = theShopifyOrderDetailList
                    };
                    int validateErrorCode = ValidateDetails(order, rebuildFMDetailsvm);
                    if ((validateErrorCode == 8) && (order.Adjust == 0))  //&& (order.SourceID > 0) 
                    {
                        order.Adjust = CalAdjust(order, rebuildFMDetailsvm.FMOrderDetails);
                        Cart cart = new Cart();
                        cart = cart.GetCartFromOrder(order.OrderID, false);
                        cart.SiteOrder.Adjust = order.Adjust;
                        cart.TotalTax = order.TotalTax;
                        cart.TotalShipping = order.TotalShipping;
                        cart.UpdateOrder();
                        validateErrorCode = 0; //allow this order to go through
                    }
                    if (validateErrorCode == 0) // return code 0 means all tests passed
                    {
                        foreach (OrderDetail od in rebuildFMDetailsvm.FMOrderDetails)
                        {
                            try
                            {
                                od.OrderID = rebuildFMDetailsvm.FMOrder.OrderID; //update the orderid to get things back in sync...
                                if (od.OrderDetailID == 0)
                                {
                                    od.SaveToOrderDetail();
                                }
                                else
                                {
                                    bool ok = od.UpdateDB();
                                    if (!ok)
                                    {
                                        string msg = string.Empty;
                                        throw new Exception(message: msg);
                                    }
                                }                                
                            }
                            catch (Exception ex)
                            {
                                string msg1 = ex.Message;
                                string msg2 = string.Empty;
                                throw new Exception(message: msg2);
                            }

                        }
                        report.MatchedOrders.Add(rebuildFMDetailsvm.FMOrder.OrderID);
                        if (report.MatchedOrders.Count >= iterationLimit)
                        {
                            break;
                        }
                    }
                    else
                    {
                        RebuildFMDetailsErrors error = new RebuildFMDetailsErrors()
                        {
                            OrderId = orderId,
                            ErrorMsg = "Failed Validation: " + ErrorCodeDetail(validateErrorCode),
                            ErrorCode = validateErrorCode,
                        };
                        report.UnMatchedOrders.Add(error);
                    }
                }
            }
            ViewBag.UpdatedCount = report.MatchedOrders.Count.ToString();
            ViewBag.UnUpdatedCount = report.UnMatchedOrders.Count.ToString();
            return View(report);
        }

        // Matches ShopifyFM order & orderdetails to FM order & orderdetails; updates FM orderdetails if approved
        public ActionResult RebuildFMOrderDetailsFromODRestore(DateTime? startDate, int iterationLimit)
        {
            ViewBag.ErrorMsg = string.Empty;
            ViewBag.StartOrder = string.Empty;
            List<RebuildFMDetailsvm> rebuildFMDetailsvmList = new List<RebuildFMDetailsvm>();
            RebuildFMDetailsReport report = new RebuildFMDetailsReport();


            int userID = FM2015.Models.User.GetUserID();
            FM2015.Models.Roles roles = new FM2015.Models.Roles(userID);
            if (!roles.Role.ToUpper().Contains("ADMIN"))
            {
                ViewBag.ErrorMsg = "Admin credentials are required to enter this page.";
                return View(report);
            }
            if (startDate != null)
            {
                if ((startDate > DateTime.Now) || (startDate > Convert.ToDateTime("6/23/15")))
                {
                    ViewBag.ErrorMsg = "StartDate must be earlier than 6/23/15";
                    return View(report);
                }
            }
            if ((iterationLimit <= 0) || (iterationLimit > 1000))
            {
                ViewBag.ErrorMsg = "Update iterations must be between and 1000";
                return View(report);
            }

            List<Product> products = Product.GetProducts();

            OrderProvider op = new OrderProvider();
            List<AdminCart.Order> orders = op.GetAll();
            orders = (from o in orders where o.OrderDate < startDate select o).ToList();

            OrderDetailProvider odp = new OrderDetailProvider();
            List<OrderDetail> orderDetails = odp.GetAll();

            OrderDetailRestoreProvider odRestore = new OrderDetailRestoreProvider();
            List<OrderDetail> orderDetailsRestore = odRestore.GetAll();

            for (int i = 0; i < orders.Count; i++)
            {
                AdminCart.Order order = orders[i];

                if ((order == null) || (order.OrderID == 0))
                {
                    RebuildFMDetailsErrors error = new RebuildFMDetailsErrors()
                    {
                        OrderId = 0,
                        ErrorMsg = "OrderID = 0 while indexing into order list; index =" + i.ToString(),
                        ErrorCode = 0,
                    };
                    report.UnMatchedOrders.Add(error);
                    continue;
                }

                //List<OrderDetail> theFMOrderDetailList = OrderDetail.GetOrderDetails(orderId);
                List<OrderDetail> theFMOrderDetailList = (from od in orderDetails where od.OrderID == order.OrderID select od).ToList();
                if (theFMOrderDetailList.Count != 0)
                {
                    continue; //this order does not need it's od updated
                }

                //get corresponding order detail list from SiteV2Restore
                theFMOrderDetailList = (from od in orderDetailsRestore where od.OrderID == order.OrderID select od).ToList();

                if (theFMOrderDetailList.Count > 0) //validate; if validated then update
                {
                    // we should now have a candidate for the orderdetails associated with the FM order
                    RebuildFMDetailsvm rebuildFMDetailsvm = new RebuildFMDetailsvm
                    {
                        FMOrder = order,
                        FMOrderDetails = theFMOrderDetailList,
                        ShopifyOrder = null,
                        ShopifyLineItems = null
                    };
                    int validateErrorCode = ValidateDetailsRestore(order, rebuildFMDetailsvm);
                    //if ((validateErrorCode == 8) && (order.Adjust == 0))  //&& (order.SourceID > 0) 
                    //{
                    //    order.Adjust = CalAdjust(order, rebuildFMDetailsvm.FMOrderDetails);
                    //    Cart cart = new Cart();
                    //    cart = cart.GetCartFromOrder(order.OrderID, false);
                    //    cart.SiteOrder.Adjust = order.Adjust;
                    //    cart.TotalTax = order.TotalTax;
                    //    cart.TotalShipping = order.TotalShipping;
                    //    cart.UpdateOrder();
                    //    validateErrorCode = 0; //allow this order to go through
                    //}
                    if (validateErrorCode == 0) // return code 0 means all tests passed
                    {
                        foreach (OrderDetail od in rebuildFMDetailsvm.FMOrderDetails)
                        {
                            if (od.Discount > 0) //this set of orders has od.discount mirrored in o.Adjust
                            {
                                od.Discount = 0;
                            }
                            try
                            {
                                od.OrderID = rebuildFMDetailsvm.FMOrder.OrderID; //update the orderid to get things back in sync...
                                if (od.OrderDetailID == 0)
                                {
                                    od.SaveToOrderDetail();
                                }
                                else
                                {
                                    bool ok = od.UpdateDB();
                                    if (!ok)
                                    {
                                        string msg = string.Empty;
                                        throw new Exception(message: msg);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                string msg1 = ex.Message;
                                string msg2 = string.Empty;
                                throw new Exception(message: msg2);
                            }

                        }
                        report.MatchedOrders.Add(rebuildFMDetailsvm.FMOrder.OrderID);
                        if (report.MatchedOrders.Count >= iterationLimit)
                        {
                            break;
                        }
                    }
                    else
                    {
                        RebuildFMDetailsErrors error = new RebuildFMDetailsErrors()
                        {
                            OrderId = order.OrderID,
                            ErrorMsg = "Failed Validation: " + ErrorCodeDetail(validateErrorCode),
                            ErrorCode = validateErrorCode,
                        };
                        report.UnMatchedOrders.Add(error);
                    }
                }
            }
            ViewBag.UpdatedCount = report.MatchedOrders.Count.ToString();
            ViewBag.UnUpdatedCount = report.UnMatchedOrders.Count.ToString();
            return View(report);
        }

        private List<ShopifyOrder> GetDistinctOrders(List<ShopifyOrder> shopifyOrders)
        {
            List<ShopifyOrder> distinctOrders = new List<ShopifyOrder>();
            foreach (ShopifyOrder shop in shopifyOrders)
            {
                ShopifyOrder shop2 = (from s in distinctOrders where s.Name == shop.Name select s).FirstOrDefault();
                if (shop2 == null)
                {
                    distinctOrders.Add(shop);
                }
                else
                {
                    ; //just here for debugging
                }
            }
            distinctOrders = distinctOrders.OrderByDescending(x => x.Name).ToList();
            return distinctOrders;
        }

        private decimal CalAdjust(AdminCart.Order order, List<OrderDetail> fMOrderDetails)
        {
            decimal lineItemTotal = 0;
            foreach (OrderDetail od in fMOrderDetails)
            {
                lineItemTotal = lineItemTotal + ((od.UnitPrice - od.Discount) * od.Quantity);
            }
            decimal subtotal = order.Total + order.Adjust - order.TotalTax - order.TotalShipping;
            decimal adjust = lineItemTotal - subtotal;
            if (adjust < 0)
            {
                return 0;
            }
            else
            {
                return adjust;
            }

        }

        private List<ShopifyLineItem> GetShopifyOrderLineItems(long id)
        {
            //wait .5 sec before making the API call to ensure we stay under the throttle limit
            //turns out the db activity keeps things pretty well throttled!
            //System.Threading.Thread.Sleep(100);

            string uri = "admin/orders/" + id.ToString() + ".json";
            string apiResult = JsonHelpers.GETfromShopify(uri);
            var buffer = JObject.Parse(apiResult);
            ShopifyAgent.ShopifyOrder order = buffer.ToObject<ShopifyAgent.ShopifyOrderSingle>().order;

            return order.LineItems.ToList();
        }

        

        private int ValidateDetails(AdminCart.Order order, RebuildFMDetailsvm rebuildFMDetailsvm)
        {
            RebuildFMDetailsvm re = rebuildFMDetailsvm;

            if (re.ShopifyLineItems.Count != re.FMOrderDetails.Count)
            {
                return 1;
            }

            if (re.FMOrder.SSOrderID.ToString() != re.ShopifyOrder.Name)
            {
                return 2;
            }

            decimal lineItemTotal = 0;
            int numberOfSubscriptions = 0;
            for (int i = 0; i < re.FMOrderDetails.Count; i++)
            {
                if (re.FMOrderDetails[i].OrderID != -113113)
                {
                    return 3;
                }

                Product p = new Product(re.FMOrderDetails[i].ProductID);
                if (p.ProductID == 0)
                {
                    return 4;
                }
                if (re.ShopifyLineItems[i].Product_id != p.PartnerID)
                {
                    if (re.ShopifyLineItems[i].Sku != p.Sku)
                    {
                        return 5;
                    }
                    if (re.ShopifyLineItems[i].Price != p.Price)
                    {
                        return 5;
                    }

                    //if (order.SourceID < 0) //FM customer service orders won't have any partner IDs but could still be legit
                    //{
                    //    return 5;
                    //}
                }
                if (re.ShopifyLineItems[i].Quantity != re.FMOrderDetails[i].Quantity)
                {
                    return 6;
                }
                if (re.FMOrderDetails[i].Discount != 0)
                {
                    return 7;
                }
                lineItemTotal = lineItemTotal + (re.FMOrderDetails[i].UnitPrice * re.FMOrderDetails[i].Quantity);
                if ((p.IsMultiPay) || (p.IsSubscription))
                {
                    numberOfSubscriptions++;
                }
            }
            if (numberOfSubscriptions > 1)
            {
                return 9;
            }
            decimal adjust = re.FMOrder.Adjust;
            decimal coupon = re.FMOrder.TotalCoupon;
            decimal tax = re.FMOrder.TotalTax;
            decimal shipping = re.FMOrder.TotalShipping;
            decimal total = lineItemTotal - adjust - coupon + tax + shipping;
            if (re.FMOrder.Total != total)
            {
                return 8;
            }

            return 0;
        }

        private int ValidateDetailsRestore(AdminCart.Order order, RebuildFMDetailsvm rebuildFMDetailsvm)
        {
            RebuildFMDetailsvm re = rebuildFMDetailsvm;

            decimal lineItemTotal = 0;
            int numberOfSubscriptions = 0;
            for (int i = 0; i < re.FMOrderDetails.Count; i++)
            {
                Product p = new Product(re.FMOrderDetails[i].ProductID);
                if (p.ProductID == 0)
                {
                    return 10;
                }
                lineItemTotal = lineItemTotal + (re.FMOrderDetails[i].UnitPrice * re.FMOrderDetails[i].Quantity);
                if ((p.IsMultiPay) || (p.IsSubscription))
                {
                    numberOfSubscriptions++;
                }
            }
            if (numberOfSubscriptions > 1)
            {
                return 9;
            }
            decimal adjust = re.FMOrder.Adjust;
            decimal coupon = re.FMOrder.TotalCoupon;
            decimal tax = re.FMOrder.TotalTax;
            decimal shipping = re.FMOrder.TotalShipping;
            decimal total = lineItemTotal - adjust - coupon + tax + shipping;
            if (re.FMOrder.Total != total)
            {
                return 8;
            }

            return 0;
        }


        private string ErrorCodeDetail(int errCode)
        {
            string message = string.Empty;
            switch (errCode)
            {
                case 1:
                    message = "ShopifyLineItem count not the same as FM OrderDetail count";
                    break;

                case 2:
                    message = "ShopifyLineItem Name not the same as FM Order SSOrderId";
                    break;

                case 3:
                    message = "FM ORderID != -113113";
                    break;

                case 4:
                    message = "ProductID = 0; unable to translate ShopifyLineItem product_id?";
                    break;

                case 5:
                    message = "Product partnerID, SKU or Price not the same as Shopify LineItem product_id, sku or price";
                    break;

                case 6:
                    message = "Product quantity not the same as ShopifyLineItem quantity";
                    break;

                case 7:
                    message = "FM OrderDetail discount != 0";
                    break;

                case 8:
                    message = "FM OrderDetail discount != 0";
                    break;

                case 9:
                    message = "Order can not have more than one subscription/multipay";
                    break;

                case 10:
                    message = "ProductID = 0";
                    break;

                default:
                    message = "unknown error code";
                    break;
            }
            return message;
        }
    }
}