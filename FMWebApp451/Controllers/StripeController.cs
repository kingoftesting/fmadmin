﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminCart;
using FM2015.Models;
using FM2015.ViewModels;
using FMWebApp.ViewModels.Stripe;
using FMWebApp451.Services.StripeServices;
using Stripe;

namespace FMWebApp.Controllers
{
    public class StripeController : Controller
    {
        // GET: Stripe
        public ActionResult Index()
        {
            return View();
        }

        // GET: Stripe/GetCustomerList
        public ActionResult GetCustomerList(string param)
        {
            string errMsg = string.Empty;
            StripeServices stripeServices = new StripeServices();
            IEnumerable<StripeCustomer> stripeCustomerList = stripeServices.GetStripeCustomerList(param, out errMsg);
            ViewBag.ErrMsg = errMsg;
            return View(stripeCustomerList);
        }

        // GET: Stripe/CustomerDetails/5
        public ActionResult CustomerDetails(string id)
        {
            string errMsg = string.Empty;
            StripeServices stripeServices = new StripeServices();
            StripeCustomerViewModel cvm = new StripeCustomerViewModel
            {
                stripeCustomerId = id
            };
            ViewBag.Error = string.Empty;

            cvm.stripeCustomer = stripeServices.GetStripeCustomer(id, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.Error = errMsg;
                return View(cvm);
            }

            cvm.customer = Customer.FindCustomerByEmail(cvm.stripeCustomer.Email);

            cvm.stripeCustomerSubscriptionList = stripeServices.GetCustomerSubscriptionList(id, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.Error = errMsg;
                return View(cvm);
            }

            var productService = new StripeProductService();
            foreach (StripeSubscription subscription in cvm.stripeCustomerSubscriptionList)
            {
                subscription.StripePlan.Product = stripeServices.GetStripeProduct(subscription.StripePlan.ProductId, out errMsg);
                if (!string.IsNullOrEmpty(errMsg))
                {
                    ViewBag.Error = errMsg;
                    return View(cvm);
                }
            }

            cvm.stripeCustomerCardList = stripeServices.GetStripeCustomerCards(id, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.Error = errMsg;
                return View(cvm);
            }

            cvm.stripeCustomerChargeList = stripeServices.GetStripeCustomerCharges(id, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.Error = errMsg;
                return View(cvm);
            }

            cvm.stripeCustomerInvoiceList = stripeServices.GetStripeCustomerInvoices(id, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.Error = errMsg;
                return View(cvm);
            }
            
            return View(cvm);
        }

        // GET: Stripe/CustomerEdit/5
        public ActionResult CustomerEdit(string id)
        {
            StripeCustomerViewModel cvm = new StripeCustomerViewModel();

            var customerService = new StripeCustomerService();
            cvm.stripeCustomer = customerService.Get(id);

            return View(cvm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CustomerEdit(string id, StripeCustomerViewModel cvm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var myCustomer = new StripeCustomerUpdateOptions();
                    myCustomer.Description = cvm.stripeCustomer.Description;
                    myCustomer.Email = cvm.stripeCustomer.Email;

                    var customerService = new StripeCustomerService();
                    StripeCustomer stripeCustomer = customerService.Update(cvm.stripeCustomer.Id, myCustomer);
                    return RedirectToAction("GetCustomerList");
                }
            }
            catch
            {
                return View(cvm);
            }
            return View(cvm);
        }

        // GET: Stripe/GetChargeList
        public ActionResult GetChargeList()
        {
            var chargeService = new StripeChargeService();
            StripeChargeListOptions chargeOptions = new StripeChargeListOptions();
            //chargeOptions.CustomerId = "cus_6TxHUxUVDTfVCp"; //Rodger's Stripe Customer ID
            DateTime tempDate = DateTime.Now.AddDays(-30);
            //System.DateTime utc = tempDate.ToUniversalTime();
            chargeOptions.Created = new StripeDateFilter();
            chargeOptions.Created.GreaterThan = tempDate;
            //chargeOptions.Created.EqualTo = utc;
            IEnumerable<StripeCharge> stripeChargeList = chargeService.List(chargeOptions); // optional StripeChargeListOptions
            return View(stripeChargeList);
        }

        // GET: Stripe/GetProductList
        public ActionResult GetProductList()
        {
            var productService = new StripeProductService();
            IEnumerable<StripeProduct> stripeProductList = productService.List(); // optional StripeListOptions
            return View(stripeProductList);
        }

        // GET: Stripe/PlanDetails/5
        public ActionResult ProductDetails(string id)
        {
            var productService = new StripeProductService();
            StripeProduct response = productService.Get(id);

            return View(response);
        }

        // GET: Stripe/GetPlanList
        public ActionResult GetPlanList()
        {
            var planService = new StripePlanService();
            var productService = new StripeProductService();
            IEnumerable<StripePlan> stripePlanList = planService.List(); // optional StripeListOptions
            foreach (StripePlan stripePlan in stripePlanList)
            {
                StripeProduct stripeProduct = productService.Get(stripePlan.ProductId);
                stripePlan.Product = stripeProduct;
            }
            return View(stripePlanList);
        }

        // GET: Stripe/PlanDetails/5
        public ActionResult PlanDetails(string id)
        {
            var planService = new StripePlanService();
            var productService = new StripeProductService();
            StripePlan stripePlan = planService.Get(id);
            StripeProduct stripeProduct = productService.Get(stripePlan.ProductId);
            stripePlan.Product = stripeProduct;

            return View(stripePlan);
        }

        // GET: Stripe/PayInvoice/in_1234
        public ActionResult PayInvoice(string id, string stripeCustomerID)
        {
            StripeCustomer stripeCustomer = new StripeCustomer();
            var customerService = new StripeCustomerService();
            try
            {
                stripeCustomer = customerService.Get(stripeCustomerID);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "PayInvoice: Subscriber Lookup Error: Stripe customer can't be found: CustomerID = " + stripeCustomerID);
            }
            StripeInvoice stripeInvoice = new StripeInvoice();
            var invoiceService = new StripeInvoiceService();
            //StripeInvoicePayOptions stripeInvoicePayOptions = new StripeInvoicePayOptions()
            //{
            //    SourceId = stripeCustomer.DefaultSource.Id,
            //};

            try
            {
                stripeInvoice = invoiceService.Pay(id); //v17.9.0 worked like this
                //stripeInvoice = invoiceService.Pay(id, stripeInvoicePayOptions, null);
            }
            catch(StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeController: Details: invoice payment failed; customer: "
                    + stripeCustomerID + "; Invoice: " + id);
            }

            return RedirectToAction("CustomerDetails", new { id = stripeCustomerID });
        }

        // GET: Stripe/OpenInvoice/in_1234
        public ActionResult OpenInvoice(string id, string stripeCustomerID)
        {
            StripeInvoice stripeInvoice = new StripeInvoice();
            var invoiceService = new StripeInvoiceService();
            var stripeInvoiceUpdateOptions = new StripeInvoiceUpdateOptions();

            try
            {
                stripeInvoice = invoiceService.Get(id);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeController: Details: invoice GET failed");
            }

            try
            {
                stripeInvoiceUpdateOptions.Closed = false;
                stripeInvoice = invoiceService.Update(id, stripeInvoiceUpdateOptions);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeController: Details: invoice open failed");
            }

            return RedirectToAction("CustomerDetails", new { id = stripeCustomerID });
        }


    }
}
