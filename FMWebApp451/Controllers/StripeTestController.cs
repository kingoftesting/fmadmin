﻿using FMWebApp.ViewModels.Stripe;
using FMWebApp451.Services.StripeServices;
using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMWebApp451.Interfaces;

namespace FMWebApp451.Controllers
{
    //NOTE: Strong Dependency on the version of StripeServices that is to be tested
    //NOTE: These tests are meant to be used with Stripe account in "test mode"
    //NOTE: Stripe test response webhook sent to "live" webhook receiver which should send an email

    public class StripeTestController : Controller
    {
        private readonly IStripeService stripeServices;

        public StripeTestController(IStripeService stripeServices)
        {
            this.stripeServices = stripeServices;
        }

        public const string testPlanName = "stripe test plan";
        public const string initTestPlanNickName = "foo bar";
        public const string TestPlanNickName = "barfargle-bargle";
        public const string testCardName = "stripe test card";

        public const string stripeCustomerId = "cus_6JZvlUZNFc13bq"; // "good" id
        //public const string stripeCustomerId = "cus_7JZvlUZNFc13bq"; // "bad" id


        StripeCustomerViewModel scvm;
        private string errMsg;

        public StripeCustomerViewModel Scvm { get => scvm; set => scvm = value; }
        public string ErrMsg { get => errMsg; set => errMsg = value; }

        // GET: StripeTest
        public ActionResult Index()
        {
            ViewBag.errMsg = string.Empty;
            string errMsg = string.Empty;
            scvm = new StripeCustomerViewModel();

            if (!TestMode())
            {
                ViewBag.errMsg = "UNABLE TO RUN TEST: Stripe not configured in TEST MODE!!";
                return View(scvm);
            }

            bool ok = TestCustomerStuff(); 
            if (ok) { ok = TestPlanStuff(); }
            if (ok) { ok = TestTokenStuff(); }
            if (ok) { ok = TestCardStuff(); }
            //if (ok) { ok = TestChargeStuff(); } 
            if (ok) { ok = TestSubscriptionStuff(); } 
            if (ok) { ok = TestInvoiceStuff(); } 

            return View(scvm);
        }

        public ActionResult TestReport()
        {
            ViewBag.errMsg = string.Empty;
            string errMsg = string.Empty;
            scvm = new StripeCustomerViewModel();

            if (!TestMode())
            {
                ViewBag.errMsg = "UNABLE TO RUN TEST: Stripe not configured in TEST MODE!!";
                return View(scvm);
            }

            bool ok = TestCustomerStuff();
            if (!ok)
            {
                ViewBag.ErrMsg = "FAILED: could initialize Test Customer";
                return View(scvm);
            }

            scvm.stripeCustomerPlanList = stripeServices.GetStripePlanList(out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.ErrMsg = errMsg;
                return View(scvm);
            }

            scvm.stripeCustomerCardList = stripeServices.GetStripeCustomerCards(scvm.stripeCustomerId, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.ErrMsg = errMsg;
                return View(scvm);
            }

            scvm.stripeCustomerInvoiceList = stripeServices.GetStripeCustomerInvoices(scvm.stripeCustomerId, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.ErrMsg = errMsg;
                return View(scvm);
            }

            scvm.stripeCustomerChargeList = stripeServices.GetStripeCustomerCharges(scvm.stripeCustomerId, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.ErrMsg = errMsg;
                return View(scvm);
            }

            scvm.stripeCustomerSubscriptionList = stripeServices.GetCustomerSubscriptionList(scvm.stripeCustomerId, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.ErrMsg = errMsg;
                return View(scvm);
            }

            return View(scvm);

        }

        public ActionResult CancelSubscription(string subscriptionId, string stripeCustomerId)
        {
            ViewBag.errMsg = string.Empty;
            string errMsg = string.Empty;
            string subscriptionErrMsg = "Stripe Test Reort: Delete subscription FAILED: subscriptionId = " + subscriptionId;

            bool ok = stripeServices.DeleteStripeSubscription(stripeCustomerId, subscriptionId, subscriptionErrMsg, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.ErrMsg = errMsg;
                return View();
            }
            ViewBag.ErrMsg = "Cancel Subscription SUCCEEDED: subscriptionId = " + subscriptionId;
            return View();
        }

            private bool TestTokenStuff()
        {
            // create a new Stripe Card
            StripeTokenCreateOptions tokenOptions = new StripeTokenCreateOptions()
            {
                Card = new StripeCreditCardOptions()
                {
                    Number = "4242424242424242",
                    Name = testCardName,
                    ExpirationYear = 2019,
                    ExpirationMonth = 10,
                    Cvc = "123"
                }
            };

            StripeToken stripeToken = stripeServices.CreateStripeToken(tokenOptions, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            };

            stripeToken = stripeServices.GetStripeToken(stripeToken.Id, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            };

            if (stripeToken == null)
            {
                ViewBag.errMsg = "Get Stripe Token FAILED: tokenId = " + stripeToken.Id;
                return false;
            };
            return true;
        }

        private bool TestInvoiceStuff()
        {
            List<StripeInvoice> stripeInvoices = stripeServices.GetStripeCustomerInvoices(scvm.stripeCustomerId, out string errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }
            else
            {
                scvm.stripeCustomerInvoiceList = stripeInvoices;
            }
            return true;
        }

        private bool TestChargeStuff()
        {
            StripeCharge stripeCharge = new StripeCharge();

            //get list of charges (test)
            List<StripeCharge> stripeCharges = stripeServices.GetStripeCustomerCharges(scvm.stripeCustomer.Id, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }

            //create a charge
            StripeChargeCreateOptions chargeOptions = new StripeChargeCreateOptions()
            {
                Amount = 2000,
                CustomerId = scvm.stripeCustomer.Id,
                Currency = "usd",
                Description = "Charge for facemaster@example.com",
                SourceTokenOrExistingSourceId = scvm.stripeCustomer.DefaultSourceId,
            };
            StripeChargeService chargeService = new StripeChargeService();
            stripeCharge = stripeServices.CreateStripeCharge(chargeOptions, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }

            //get that charge


            //get list of charges & add to viewmodel for view reporting
            stripeCharges = stripeServices.GetStripeCustomerCharges(scvm.stripeCustomer.Id, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }
            scvm.stripeCustomerChargeList = stripeCharges;
            return true;
        }

        private bool TestSubscriptionStuff()
        {
            //get a plan to subscribe to (select from list of plans)
            List<StripePlan> stripePlans = stripeServices.GetStripePlanList(out string errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }

            StripePlan stripePlan = (from plan in stripePlans where plan.Product.Name == testPlanName select plan).FirstOrDefault();
            if (stripePlan == null)
            {
                ViewBag.errMsg = "TestSubscriptionStuff: Could not find test plan to subscribe to: name = " + testPlanName;
                return false;
            }
            StripeCoupon stripeCoupon = new StripeCoupon()
            {
                Id = string.Empty,
            };
            string stripeSubscriptionErrMsg = "TestSubscriptionStuff: subscriptionService.Create failed";
            StripeSubscription stripeSubscription = stripeServices.CreateStripeSubscription(scvm.stripeCustomerId, stripePlan.Id, stripeCoupon.Id, stripeSubscriptionErrMsg, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }

            List<StripeSubscription> stripeSubscriptions = stripeServices.GetCustomerSubscriptionList(scvm.stripeCustomerId, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }
            else
            {
                scvm.stripeCustomerSubscriptionList = stripeSubscriptions;
            }
            return true;
        }

        private bool TestCardStuff()
        {
            //Get the test card, but via a test of the service
            StripeCard stripeCard = GetTestStripeCard(scvm.stripeCustomer.Id, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }

            //update stripe customer object default_source with new stripeCard
            StripeCustomerUpdateOptions stripeCustomerUpdateOptions = new StripeCustomerUpdateOptions
            {
                DefaultSource = stripeCard.Id  //****************
            };
            StripeCustomer stripeCustomer = stripeServices.UpdateStripeCustomer(scvm.stripeCustomerId, stripeCustomerUpdateOptions, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }
            scvm.stripeCustomer = stripeCustomer; //reflect the update in the view model global (for use by TestCharge)

            //Get list of cards for ViewModel
            List<StripeCard> stripeCards = stripeServices.GetStripeCustomerCards(scvm.stripeCustomer.Id, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }
            scvm.stripeCustomerCardList = stripeCards;
            return true;
        }

        private bool TestPlanStuff()
        {
            //Get the test plan, but via a test of the service
            StripePlan stripePlan = GetTestStripePlan(out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }

            //Get list of plans for ViewModel
            List<StripePlan> stripePlans = stripeServices.GetStripePlanList(out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }
            scvm.stripeCustomerPlanList = stripePlans;
            return true;
        }

        private bool TestCustomerStuff()
        {
            //Get a Customer
            errMsg = string.Empty;
            StripeCustomer stripeCustomer = stripeServices.GetStripeCustomer(stripeCustomerId, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return false;
            }
            else
            {
                scvm.stripeCustomer = stripeCustomer;
                scvm.stripeCustomerId = stripeCustomer.Id;
                return true;
            }
        }

        private StripeCard GetTestStripeCard(string stripeId, out string errMsg)
        {
            errMsg = string.Empty;
            StripeCard stripeCard = new StripeCard();
            List<StripeCard> stripeCards = stripeServices.GetStripeCustomerCards(stripeId, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return stripeCard;
            }

            stripeCard = (from card in stripeCards where card.Name == testCardName select card).FirstOrDefault();
            if (stripeCard != null)
            {
                if (!stripeServices.DeleteStripeCard(scvm.stripeCustomer.Id, stripeCard.Id, out errMsg))
                {
                    ViewBag.errMsg = errMsg;
                    return stripeCard;
                }
            }

            // create a new Stripe Card
            StripeCardCreateOptions cardCreateOptions = new StripeCardCreateOptions()
            {
                SourceCard = new SourceCard()
                {
                    Number = "4242424242424242",
                    Name = testCardName + "Test1",
                    ExpirationYear = 2019,
                    ExpirationMonth = 10,
                    Cvc = "123"
                }

            };

            stripeCard = stripeServices.CreateStripeCard(stripeCustomerId, cardCreateOptions, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return stripeCard;
            }

            stripeCards = stripeServices.GetStripeCustomerCards(stripeId, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return stripeCard;
            }

            stripeCard = (from card in stripeCards where card.Name == testCardName + "Test1" select card).FirstOrDefault();
            if (stripeCard == null)
            {
                ViewBag.errMsg = "Get Stripe Test Card FAILED";
                return stripeCard;
            }

            StripeCardUpdateOptions stripeCardUpdateOptions = new StripeCardUpdateOptions()
            {
                Name = testCardName,
            };
            stripeCard = stripeServices.UpdateStripeCard(stripeCustomerId, stripeCard.Id, stripeCardUpdateOptions, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return stripeCard;
            }

            return stripeCard;
        }

        private StripePlan GetTestStripePlan(out string errMsg)
        {
            errMsg = string.Empty;
            StripePlan stripePlan = new StripePlan();
            List<StripePlan> stripePlans = stripeServices.GetStripePlanList(out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return stripePlan;
            }
            stripePlan = (from plan in stripePlans where plan.Product.Name == testPlanName select plan).FirstOrDefault();
            if (stripePlan != null)
            {
                if (!stripeServices.DeleteStripePlan(stripePlan.Id, out errMsg))
                {
                    ViewBag.errMsg = errMsg;
                    return stripePlan;
                }
            }

            //create a new Stripe plan
            StripePlanCreateOptions planCreateOptions = InitPlanCreateOptions();
            stripePlan = stripeServices.CreateStripePlan(planCreateOptions, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return stripePlan;
            }

            stripePlan = stripeServices.GetStripePlanById(stripePlan.Id, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return stripePlan;
            }

            stripePlan = stripeServices.GetStripePlanByName(testPlanName, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return stripePlan;
            }
            if (stripePlan.Nickname != initTestPlanNickName)
            {
                ViewBag.errMsg = "StripeService: UpDateStripePlan: Stripe Error: plan name = " + stripePlan.Product.Name;
                return stripePlan;
            }

            //create a new Stripe plan
            StripePlanUpdateOptions planUpdateOptions = InitPlanUpdateOptions();
            stripePlan = stripeServices.UpdateStripePlan(stripePlan.Id, planUpdateOptions, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.errMsg = errMsg;
                return stripePlan;
            }
            return stripePlan;
        }

        private StripePlanUpdateOptions InitPlanUpdateOptions()
        {
            StripePlanUpdateOptions planUpdateOptions = new StripePlanUpdateOptions()
            {
                Nickname = initTestPlanNickName,
            };
            return planUpdateOptions;
        }

        private StripePlanCreateOptions InitPlanCreateOptions()
        {
            StripePlanCreateOptions planCreateOptions = new StripePlanCreateOptions()
            {
                Product = new StripePlanProductCreateOptions()
                {
                    Name = testPlanName,
                },
                Nickname = initTestPlanNickName,
                Amount = 100,
                Currency = "usd",
                Interval = "month",
            };
            return planCreateOptions;
        }

        private bool TestMode()
        {
            string stripe_Live = ConfigurationManager.AppSettings["StripeLive"].ToString();
            return (stripe_Live.ToUpper() == "NO") ? true : false;
        }
    }
}