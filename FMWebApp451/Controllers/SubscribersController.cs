﻿using FM2015.Models;
using FMWebApp451.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMWebApp451.Controllers
{
    public class SubscribersController : Controller
    {
        private readonly ISubscriptions subscriptionProvider;

        public SubscribersController(ISubscriptions subscriptionProvider)
        {
            this.subscriptionProvider = subscriptionProvider;
        }

        // GET: Subscribers
        public ActionResult Index(string param)
        {
            List<Subscriptions> subscriptions = new List<Subscriptions>();
            Subscriptions sub = new Subscriptions();

            subscriptions = subscriptionProvider.GetAll();
            subscriptions = subscriptions.OrderByDescending(x => x.OrderID).ToList();

            if (string.IsNullOrEmpty(param))
            {
                ; // list all subscriptions if param is null
            }
            else
            {
                if (Int32.TryParse(param, out int orderId))
                {
                    subscriptions = (from s in subscriptions where s.OrderID == orderId select s).ToList();
                }
                else
                {
                    if (param.Contains("sub_"))
                    {
                        subscriptions = (from s in subscriptions where s.StripeSubID.Contains(param) select s).ToList();
                    }
                    else
                    {
                        if (param.Contains("cus_"))
                            {
                                subscriptions = (from s in subscriptions where s.StripeCusID.Contains(param) select s).ToList();
                            }
                    }
                }
            }
            return View(subscriptions);
        }

        // GET: Subscribers/Details/5
        public ActionResult Details(int id)
        {
            List<Subscriptions> subscriptions = new List<Subscriptions>();
            Subscriptions sub = new Subscriptions();

            subscriptions = subscriptionProvider.GetAll();
            sub = (from s in subscriptions where s.ID == id select s).FirstOrDefault();

            return View(sub);
        }

        // GET: Subscribers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Subscribers/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Subscribers/Edit/5
        public ActionResult Edit(int id)
        {
            List<Subscriptions> subscriptions = new List<Subscriptions>();
            Subscriptions sub = new Subscriptions();

            subscriptions = subscriptionProvider.GetAll();
            sub = (from s in subscriptions where s.ID == id select s).FirstOrDefault();

            return View(sub);
        }

        // POST: Subscribers/Edit/5
        [HttpPost]
        public ActionResult Edit(Subscriptions sub)
        {
            if (ModelState.IsValid)
            {
                subscriptionProvider.Update(sub);
                return RedirectToAction("Index", new {param = sub.OrderID.ToString() });
            }
            return View();
        }

        // GET: Subscribers/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Subscribers/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
