﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models;
using AdminCart;
using PayWhirl;
using Stripe;
using FMWebApp.ViewModels.Stripe;

namespace FMWebApp451.Controllers
{
    public class SubscriptionsController : Controller
    {
        // GET: Subscriptions
        public ActionResult Index(string id)
        {

            Int32.TryParse(id, out int cID);

            Customer customer = new Customer();
            PayWhirlSubscriber sub = new PayWhirlSubscriber();
            StripeCustomerViewModel cvm = new StripeCustomerViewModel();
            ViewBag.ErrMsg = "";

            if (cID > 0)
            {
                customer = new Customer(cID);
            }
            else
            {
                ViewBag.ErrMsg = "Customer ID not in correct format: " + id;
                return View(cvm.stripeCustomerSubscriptionList);
            }
            try
            {
                //sub = PayWhirlSubscriber.FindSubscriberByEmail(customer.Email);
                sub = PayWhirl.PayWhirlBiz.FindSubscriberByEmail(customer.Email);
            }
            catch
            {
                sub = new PayWhirlSubscriber(); //go to to default if problem with PayWhirl api
            }
            if (sub.Id > 0)
            {
                var subscriptionService = new StripeSubscriptionService(); //get list of all subscriptions for customer
                StripeSubscriptionListOptions stripeSubscriptionListOptions = new StripeSubscriptionListOptions()
                {
                    CustomerId = sub.Stripe_id,
                };
                try
                {
                    //cvm.stripeCustomerSubscriptionList = subscriptionService.List(sub.Stripe_id);
                    cvm.stripeCustomerSubscriptionList = subscriptionService.List(stripeSubscriptionListOptions);
                }
                catch (StripeException ex)
                {
                    Utilities.FMHelpers.ProcessStripeException(ex, true, "", "ManageSubscriptions: BindCustomers: Stripe Error");
                }
            }
            ViewBag.Name = customer.FirstName + " " + customer.LastName;
            ViewBag.cusID = sub.Stripe_id;

            return View(cvm.stripeCustomerSubscriptionList);
        }

        // GET: Subscriptions
        public ActionResult ChangePlan(string id, string stripeCustomerID, string stripeSubscriptionID)
        {
            StripeCustomerViewModel cvm = new StripeCustomerViewModel(stripeCustomerID, stripeSubscriptionID);
            //modify plan list to eliminate plans that can't be used (ie, can't change multi-pay, or switch multi-pay to club, etc.)
            List<StripePlan> newPlanList = new List<StripePlan>();
            foreach (StripePlan plan in cvm.stripeCustomerPlanList)
            {
                if (!plan.Id.Contains("pay"))
                {
                    newPlanList.Add(plan);
                }
            }
            cvm.stripeCustomerPlanList = newPlanList;

            List<StripeCoupon> newCouponList = new List<StripeCoupon>();
            foreach (StripeCoupon coupon in cvm.stripeCustomerCouponList)
            {
                if (coupon.Valid)
                {
                    newCouponList.Add(coupon);
                }
            }
            cvm.stripeCustomerCouponList = newCouponList;

            Session["cpCVM"] = cvm;
            return View(cvm);
        }

        // POST: Subscriptions
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePlan(DateTime txtTrialEndDate)
        {
            StripeCustomerViewModel cvm = new StripeCustomerViewModel();
            cvm = (StripeCustomerViewModel)Session["cpCVM"];

            string newStripePlan = Request["ddlStripePlan"];
            if (string.IsNullOrEmpty(newStripePlan))
            {
                ViewBag.ErrMsg = "You must select a New Plan!";
                return View(cvm);
            }

            if (txtTrialEndDate < DateTime.Now)
            {
                ViewBag.ErrMsg = "You must select a Start Time after Today!";
                return View(cvm);
            }

            string newStripePromoCode = Request["ddlStripePromo"];

            if (!string.IsNullOrEmpty(newStripePromoCode))
            {
                cvm.currSubscription.StripePromoCode = newStripePromoCode;
            }

            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(cvm.orderId);

            int dpcID = DelayedPlanChanges.InsertDelayedPlanChanges(cvm.customerId, cvm.customer.LastName, cvm.stripeCustomer.Id, newStripePlan, cvm.currStripePlanId, txtTrialEndDate, cvm.currSubscription.StripePromoCode, cart, false);

            if (dpcID <= 0)
            {
                ViewBag.ErrMsg = "Insert new delayed subscription failed";
                return View(cvm);
            }
            else
            {
                string msg = "New Delayed Subscription: OrderID = " + cart.SiteOrder.OrderID.ToString() + "; old plan: " + cvm.currStripePlanId + "; new plan: " + newStripePlan + "; startDate: " + txtTrialEndDate;
                OrderNote.Insertordernote(cvm.orderId, FM2015.Models.User.GetUserName(), msg);

                //OK, successful new plan was set up OK. Now delete the old plan
                //FM2015.Models.Subscriptions.Stripe_UnSubscribe(cvm.stripeCustomer.Id, cvm.currStripeSubscriptionId);
                return RedirectToAction("UnSubscribe", "Customers", new { id = cvm.customerId, stripeCustomerID = cvm.stripeCustomer.Id, stripeSubscriptionID = cvm.currStripeSubscriptionId });
            }

        }

        // GET: ActivateDeferred
        public ActionResult ActivateDeferred(int id, bool isTest)
        {
            DelayedPlanChanges dpc = new DelayedPlanChanges(id);
            Customer c = new Customer(dpc.CustomerID);

            string response = DelayedPlanChanges.Activate(dpc, isTest);
            if (response == "success") //if successful the display the customer's details
            {
                return RedirectToAction("Details", "Customers", new { id = dpc.CustomerID.ToString() });
            }

            ViewBag.Message = "Activate Deferred Subscription: " + response;
            return View();
        }

        // GET: DeleteDeferred
        public ActionResult DeleteDeferred(int id, bool isTest)
        {
            DelayedPlanChanges dpc = new DelayedPlanChanges(id);
            Customer c = new Customer(dpc.CustomerID);
            DelayedPlanChanges.Delete(dpc, isTest);

            return RedirectToAction("Details", "Customers", new { id = dpc.CustomerID.ToString() });
        }

        // GET: Subscriptions
        public ActionResult Delete(string id, string subId)
        {
            StripeCustomerViewModel cvm = new StripeCustomerViewModel();
            cvm = (StripeCustomerViewModel)Session["cpCVM"];

            Subscriptions sub = new Subscriptions(id, subId);
            if (sub.ID == 0)
            {
                ViewBag.ErrMsg = "Subscription Delete: subscription can't be found; StripeCustomerID = " + id + "; " + "StripeSubscriptionID = " + subId;
                return View(cvm);
            }

            Order o = new Order(sub.OrderID);
            if (o.OrderID == 0)
            {
                ViewBag.ErrMsg = "Subscription Delete: order can't be found; StripeCustomerID = " + id + "; " + "StripeSubscriptionID = " + subId + "; " + "OrderID = " + sub.OrderID.ToString();
                return View(cvm);
            }

            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(o.OrderID);

            try
            {
                FM2015.Models.Subscriptions.Stripe_UnSubscribe(id, subId);
                adminCER.CER cer = new adminCER.CER(cart);
                cer.EventDescription = "Subscription Delete" + Environment.NewLine;
                cer.EventDescription += "Subscription ID: " + sub.ID.ToString() + Environment.NewLine;
                cer.EventDescription += "Stripe Customer ID: " + id + Environment.NewLine;
                cer.EventDescription += "Stripe Subscriber ID: " + subId + Environment.NewLine;
                cer.EventDescription += "OrderID:" + o.OrderID.ToString() + Environment.NewLine;
                cer.EventDescription += "Unsubscribed By: " + cer.ReceivedBy + Environment.NewLine;
                cer.ActionExplaination = "Unsubscribe Subscription";
                int result = cer.Add();
            }
            catch (StripeException ex)
            {
                ViewBag.ErrMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Subscriptions: Unsubscribe: Stripe Error");
                return View(cvm);
            }

            return View("Index/" + cvm.customerId);
        }

        public ActionResult CustomerPayment(string id, string stripeCustomerID, string stripeSubscriptionID)
        {
            StripeCustomer stripeCus = new StripeCustomer();
            StripeSubscription stripeSub = new StripeSubscription();
            StripeCustomerViewModel cvm = new StripeCustomerViewModel(stripeCustomerID, stripeSubscriptionID);

            ViewBag.ErrMsg = "";

            Subscriptions sub = new Subscriptions(stripeSubscriptionID, stripeCustomerID);
            if (sub.ID == 0)
            {
                ViewBag.ErrMsg = "Subscription Payment: subscription can't be found; StripeCustomerID = " + id + "; " + "StripeSubscriptionID = " + stripeSubscriptionID;
                return View(cvm);
            }

            if (cvm.orderId == 0)
            {
                ViewBag.ErrMsg = "Subscription Payment: order can't be found; StripeCustomerID = " + id + "; " + "StripeSubscriptionID = " + stripeSubscriptionID + "; " + "OrderID = " + sub.OrderID.ToString();
                return View(cvm);
            }

            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(cvm.orderId);

            var subscriptionService = new StripeSubscriptionService();
            
            try
            {
                //stripeSub = subscriptionService.Get(stripeCustomerID, stripeSubscriptionID);
                stripeSub = subscriptionService.Get(stripeSubscriptionID, null);
            }
            catch (StripeException ex)
            {
                ViewBag.ErrMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Subscription Payment: Get StripeSubscription: Stripe Error");
                return View(cvm);
            }

            ViewBag.Plan = stripeSub.StripePlan.Id;

            StripeInvoice stripeInvoice = new StripeInvoice();
            stripeInvoice = Subscriptions.GetLastInvoice(stripeCustomerID);
            if (stripeInvoice.Closed != true)
            {
                cart.Total = Convert.ToDecimal(stripeInvoice.AmountDue) / 100;
                ViewBag.Total = cart.Total.ToString("C");
            }
            else
            {
                cart.Total = 0;
                ViewBag.Total = "Currently $0.00 (most recent invoice has been successfully paid)";
            }

            Session["CScart"] = cart;
            Session["cpCVM"] = cvm;
            Session["StripeInvoiceID"] = stripeInvoice.Id;
            return View(cart.Tran);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CustomerPayment(Transaction tran)
        {
            StripeCustomerViewModel cvm = (StripeCustomerViewModel)Session["cpCVM"];
            //string stripeCustomerID = Session["StripeCustomerID"].ToString();
            Cart cart = new Cart();
            cart = (Cart)Session["CScart"];
            cart.Tran = tran;
            Session["CScart"] = cart;
            ViewBag.Total = cart.Total.ToString("C");

            if (string.IsNullOrEmpty(tran.CardNo))
            {
                ViewBag.ErrorMsg = "Card Number is required";
                return View(tran);
            }
            if (tran.CardNo.Length < 15)
            {
                ViewBag.ErrorMsg = "Card is less than 15 characters.  Please correct, then try again.";
                return View(tran);
            }
            if (string.IsNullOrEmpty(tran.CardHolder))
            {
                ViewBag.ErrorMsg = "Name is required";
                return View(tran);
            }
            if (string.IsNullOrEmpty(tran.ExpDate))
            {
                ViewBag.ErrorMsg = "Expiration is required";
                return View(tran);
            }
            int temp = 0;
            if (!Int32.TryParse(tran.ExpDate.Replace("/", ""), out temp))
            {
                ViewBag.ErrorMsg = "Expiration date must be in the form of MM/YY (for instance, 06/15)";
                return View(tran);
            }
            if (string.IsNullOrEmpty(tran.CvsCode))
            {
                ViewBag.ErrorMsg = "CVS is required";
                return View(tran);
            }
            temp = 0;
            if (!Int32.TryParse(tran.CvsCode, out temp))
            {
                ViewBag.ErrorMsg = "CVS must be all numbers";
                return View(tran);
            }

            tran.CardNo = tran.CardNo.Replace(" ", ""); //get rid of any spaces customer may have used
            tran.ExpDate = tran.ExpDate.Replace("/", ""); //get rid of possible / in MM/YY 
            cart.Tran = tran;

            Session["RequestIsSubscribe"] = false;
            string promoCodeID = "";
            //StripeInvoiceID has been saved in Session variable during GET
            if (cart.SubscriptionProcessing(promoCodeID) != 0) //transaction was OK if result == 0
            {
                string errMsg = (Session["subErrorMessage"] == null) ? "" : Session["subErrorMessage"].ToString();
                int first = errMsg.IndexOf("errMessage:");
                int last = errMsg.Length;
                int subStringLength = last - first;
                if (first > 0)
                {
                    errMsg = errMsg.Substring(first, subStringLength);
                }
                errMsg = errMsg.Replace("errMessage:", "");
                ViewBag.ErrorMsg = "Your Payment did not Successfully complete: " + errMsg;
                return View(tran);
            }
            else
            {
                adminCER.CER cer = new adminCER.CER(cart);
                cer.EventDescription = "Subscription Payment or Credit Card Update" + Environment.NewLine;
                cer.EventDescription += "Card Update: New Card#" + cart.Tran.CardNo + Environment.NewLine;
                cer.EventDescription += "Card Update: New Expiration" + cart.Tran.ExpDate + Environment.NewLine;
                cer.EventDescription += "Original Subscription ID: " + cvm.currSubscriptionId + Environment.NewLine;
                cer.EventDescription += "Stripe Customer ID: " + cvm.stripeCustomerId + Environment.NewLine;
                cer.EventDescription += "Stripe Subscriber ID: " + cvm.currStripeSubscriptionId + Environment.NewLine;
                cer.EventDescription += "Payment Amount: " + cart.Tran.TransactionAmount.ToString("C") + Environment.NewLine;
                cer.EventDescription += "Updated By: " + cer.ReceivedBy + Environment.NewLine;
                cer.ActionExplaination = "Subscription Payment or Credit Card Update";
                int result = cer.Add();
            }
            return RedirectToAction("CustomerThankYou");
        }

        public ActionResult CustomerThankYou()
        {
            System.Threading.Thread.Sleep(1100);
            List<int> list = ShopifyAgent.ShopifyAgent.SyncShopify(); //sync orders from Shopify

            Cart cart = (Cart)Session["CSCart"];

            ViewBag.Total = cart.Total.ToString("C");
            if (string.IsNullOrEmpty(cart.Tran.AuthCode))
            {
                ViewBag.AuthCode = "CCU" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString();
            }
            else
            {
                ViewBag.AuthCode = cart.Tran.AuthCode;
            }
            ViewBag.Date = DateTime.Now.ToString();
            ViewBag.Name = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName;
            ViewBag.Street = cart.ShipAddress.Street;
            ViewBag.City = cart.ShipAddress.City;
            ViewBag.State = cart.ShipAddress.State;
            ViewBag.Zip = cart.ShipAddress.Zip;
            ViewBag.Country = cart.ShipAddress.Country;
            ViewBag.Email = cart.SiteCustomer.Email;

            return View();
        }

    }
}

