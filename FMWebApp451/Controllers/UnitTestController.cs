﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models;
using Stripe;
using AdminCart;
using System.Runtime.InteropServices;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using FM2015.Helpers;
using System.Diagnostics;
using FMWebApp451.Services.StripeServices;

namespace FMWebApp451.Controllers
{
    public class ClassList
    {
        public string Name { get; set; } 
        public long NumberOfItems { get; set; }
        public long NumberOfBytes { get; set; }
        public long ElapsedTime { get; set; }

        public ClassList()
        {
            Name = string.Empty;
            NumberOfItems = 0;
            NumberOfBytes = 0;
            ElapsedTime = 0;
        }
    }
    public class UnitTestController : Controller
    {

        //GET: Test Web Api ping to Activate any Deferred Subscription
        public ActionResult TestWebApi_ActivateDeferredSubscriptions()
        {
            ViewBag.What = "Test WebApi: Activate Deferred Subscriptions";
            ViewBag.URL = "";
            ViewBag.Response = "";
            return View();
        }

        //GET: Test Web Api ping to Activate any Deferred Subscription
        public ActionResult TestWebApi_ActivateDeferredSubscriptions_x()
        {
            string testURL = Config.TestWebApi_ActivateDeferredSubscriptions();
            string response = FMWebApp.Helpers.JsonHelpers.GETfromWebAPI(testURL);
            ViewBag.What = "Test WebApi: Activate Deferred Subscriptions";
            ViewBag.URL = testURL;
            ViewBag.Response = response;
            return View("TestWebApi_ActivateDeferredSubscriptions");
        }

        //GET: Test Web Api ping to Activate any Deferred Subscription
        public ActionResult Test_SendDunningLetter()
        {
            return RedirectToAction("SendDunning", "Report", new { id = "90735", test = true });
        }

        // GET: UnitTest
        public ActionResult Test_Cart_UpdateOrder()
        {
            AdminCart.Cart cart = new AdminCart.Cart();
            cart = cart.GetCartFromOrder(-1); //rmohme order
            int tmpSourceID = cart.SiteOrder.SourceID;
            cart.SiteOrder.SourceID = -152;
            cart.UpdateOrder();
            cart = cart.GetCartFromOrder(90730); //rmohme order
            ViewBag.SourceID = cart.SiteOrder.SourceID;
            if (cart.SiteOrder.SourceID != -152 )
            {
                ViewBag.Pass = false;
            }
            else
            {
                ViewBag.Pass = true;
                cart.SiteOrder.SourceID = tmpSourceID;
                cart.UpdateOrder();
            }

            return View();
        }

        //GET: Test Web Api ping to Activate any Deferred Subscription
        public ActionResult Test_SendFinalDunningLetter()
        {
            return RedirectToAction("SendFinalDunning", "Report", new { id = "94604", test = true });
        }

        // GET: UnitTest
        public ActionResult Test_Shopify_UpDateInventory()
        {
            long variantId = 2632825985; //should be Lavender replacement wands
            ShopifyAgent.ProductVariant variant = new ShopifyAgent.ProductVariant();
            variant = ShopifyAgent.ProductVariant.GetInventory(variantId); //READ
            if (variant.Id == 0)
            {
                ViewBag.Pass = false;
                return View();
            }
            int newInventory = 100;
            int oldInventory = variant.Old_inventory_quantity;
            ShopifyAgent.ProductVariant.UpDateInventory(variantId, newInventory, oldInventory); //MODIFY
            variant = ShopifyAgent.ProductVariant.GetInventory(2632825985); //VERIFY
            ViewBag.Sku = variant.Sku;
            Product p = new Product(variant.Sku);
            ViewBag.Name = p.Name;
            ViewBag.VariantId = variantId.ToString();
            ViewBag.OrigInventory = oldInventory.ToString();
            ViewBag.NewInventoryAttempt = newInventory.ToString();
            ViewBag.NewInventory = variant.Inventory_quantity.ToString();
            if (newInventory == variant.Inventory_quantity)
            {
                ViewBag.Pass = true;
            }
            else
            {
                ViewBag.Pass = false;
            }

            return View();
        }

        public ActionResult Test_Stripe_GetSet_AccountBalance()
        {
            string customerID = "cus_6qQQR9QnbxvTON"; // customer mohme-order1

            //bool testPass = false; //assume failure
            //get the customer from Stripe
            var customerService = new StripeCustomerService();
            StripeCustomer stripeCustomer = new StripeCustomer();

            try
            {
                stripeCustomer = customerService.Get(customerID);
                //testPass = true;
            }
            catch (StripeException ex)
            {
                //testPass = false;
                ViewBag.Pass = false;
                ViewBag.Message = "Stripe Error: couldn't locate Customer: (1st pass): " + Utilities.FMHelpers.ProcessStripeException(ex, false, "", "");
                return View();
            }

            int accountBalance = -1;
            Int32.TryParse(stripeCustomer.AccountBalance.ToString(), out accountBalance);
            if (accountBalance == -1)
            {
                //testPass = false;
                ViewBag.Pass = false;
                ViewBag.Message = "Stripe Error: account balance not updated: 1st pass";
                return View();
            }

            int origAccountBalance = accountBalance;
            var myCustomer = new StripeCustomerUpdateOptions();
            decimal setupFee = 1;
            myCustomer.AccountBalance = Convert.ToInt32((setupFee * 100)); //establish a setupFee by initializing the customer account balance
            try
            {
                stripeCustomer = customerService.Update(customerID, myCustomer);
                //testPass = true;
            }
            catch (StripeException ex)
            {
                //testPass = false;
                ViewBag.Pass = false;
                ViewBag.Message = "Stripe Error: account balance update failed: " + Utilities.FMHelpers.ProcessStripeException(ex, false, "", "");
                return View();
            }

            try
            {
                stripeCustomer = customerService.Get(customerID);
                //testPass = true;
            }
            catch (StripeException ex)
            {
                //testPass = false;
                ViewBag.Pass = false;
                ViewBag.Message = "Stripe Error: couldn't locate Customer (2nd Pass): " + Utilities.FMHelpers.ProcessStripeException(ex, false, "", "");
                return View();
            }

            accountBalance = -2;
            Int32.TryParse(stripeCustomer.AccountBalance.ToString(), out accountBalance);
            if (accountBalance == -2)
            {
                //testPass = false;
                ViewBag.Pass = false;
                ViewBag.Message = "Stripe Error: account balance not updated: 2nd pass: AccountBalance = " + accountBalance.ToString();
                return View();
            }

            try
            {
                stripeCustomer = customerService.Get(customerID);
                //testPass = true;
            }
            catch (StripeException ex)
            {
                //testPass = false;
                ViewBag.Pass = false;
                ViewBag.Message = "Stripe Error: couldn't locate Customer (3rd Pass): " + Utilities.FMHelpers.ProcessStripeException(ex, false, "", "");
                return View();
            }

            myCustomer.AccountBalance = origAccountBalance; //put account balance back to original
            try
            {
                stripeCustomer = customerService.Update(customerID, myCustomer);
                //testPass = true;
            }
            catch (StripeException ex)
            {
                //testPass = false;
                ViewBag.Pass = false;
                ViewBag.Message = "Stripe Error: account balance update failed (reset to original): " + Utilities.FMHelpers.ProcessStripeException(ex, false, "", "");
                return View();
            }

            Int32.TryParse(stripeCustomer.AccountBalance.ToString(), out accountBalance);
            if (accountBalance != origAccountBalance)
            {
                //testPass = false;
                ViewBag.Pass = false;
                ViewBag.Message = "Stripe Error: account balance not updated: 2nd pass AccountBalance = " + accountBalance.ToString();
                return View();
            }

            //testPass = true;
            ViewBag.Pass = true;
            ViewBag.Message = "Success!!";
            return View();
        }

        // GET: UnitTest
        public ActionResult Test_Customer_FindCustomerByEmail()
        {

            AdminCart.Customer c = new AdminCart.Customer();
            c = AdminCart.Customer.FindCustomerByEmail("Xrmohme@alumni.uci.edu");
            if (c.CustomerID != 0)
            {
                ViewBag.Pass = false;
            }
            else
            {
                c = AdminCart.Customer.FindCustomerByEmail("rmohme@alumni.uci.edu");
                if (c.CustomerID == 0)
                {
                    ViewBag.Pass = false;
                }
                else
                {
                    ViewBag.Pass = true;
                }
            }

            return View();
        }

        // GET: UnitTest
        public ActionResult Test_PayWhirl_FindCustomerByEmail()
        {
            string test_Email = "rmohme@alumni.uci.edu";
            PayWhirl.PayWhirlSubscriber pwSubScriber = new PayWhirl.PayWhirlSubscriber();
            try
            {
                //pwSubScriber = PayWhirl.PayWhirlSubscriber.FindSubscriberByEmail(test_Email);
                pwSubScriber = PayWhirl.PayWhirlBiz.FindSubscriberByEmail(test_Email);
            }
            catch (Exception ex)
            {
                //some exception handling stuff here
                ViewBag.Pass = false;
                ViewBag.ErrMsg = "Unit Test: Error Finding PayWhirl CustomerByEmail(rmohme): " + ex.Message;
                return View();
            }

            if (pwSubScriber.Id == 0)
            {
                ViewBag.Pass = false;
                ViewBag.ErrMsg = "Unit Test: Error Finding PayWhirl CustomerByEmail(rmohme): Id = 0";
                return View();
            }

            //krisib&84@gmail.com
            test_Email = "krisib&84@gmail.com";
            //test_Email = Uri.EscapeDataString(test_Email);
            try
            {
                //pwSubScriber = PayWhirl.PayWhirlSubscriber.FindSubscriberByEmail(test_Email);
                pwSubScriber = PayWhirl.PayWhirlBiz.FindSubscriberByEmail(test_Email);
            }
            catch (Exception ex)
            {
                //some exception handling stuff here
                ViewBag.Pass = false;
                ViewBag.ErrMsg = "Unit Test: Error Finding PayWhirl CustomerByEmail(" + test_Email +" )" + ex.Message;
                return View();
            }
            if (pwSubScriber.Id == 0)
            {
                ViewBag.Pass = false;
                ViewBag.ErrMsg = "Unit Test: Error Finding PayWhirl CustomerByEmail(krisib&84): Id = 0";
                return View();
            }
            ViewBag.Pass = true;
            ViewBag.ErrMsg = "";
            return View();
        }

        // GET: UnitTest
        public ActionResult Test_ShopifyProductTransform()
        {
            int shopifyOrderName = 801009;
            long shopifyOrderID = ShopifyAgent.ShopifyOrder.GetShopifyIDFromName(shopifyOrderName.ToString(), Config.CompanyID());
            ShopifyAgent.ShopifyOrder shopOrder = new ShopifyAgent.ShopifyOrder(shopifyOrderID, Config.CompanyID());
            long shopifyProductID = 1301421443;
            decimal shopifyProductPrice = 0;
            AdminCart.Cart cart = new AdminCart.Cart();
            cart = cart.GetCartFromOrder(20002, false); //Crystalift, rmohme purchase
            List<StripeCharge> stripeChargeList = Utilities.FMHelpers.stripeChargeList(cart.SiteCustomer.Email, "Unit Test: Shopify Product Transform: Get StripeCharge");
            if (stripeChargeList.Count() == 0)
            {
                ViewBag.Pass = false;
                ViewBag.Message = "Failed @ Get StripeChargeList; no transactions found";
            }
            else
            {
                StripeCharge stripeCharge = stripeChargeList.First();
                decimal.TryParse(stripeCharge.Amount.ToString(), out shopifyProductPrice);
                shopifyProductPrice = shopifyProductPrice / 100;
                cart.SiteOrder.SubTotal = shopifyProductPrice;
                cart.SiteOrder.Total = cart.SiteOrder.SubTotal + cart.SiteOrder.TotalTax - cart.SiteOrder.Adjust + cart.SiteOrder.TotalShipping;

                bool pricePass = false; // assume failure
                string priceStr = shopifyProductPrice.ToString();
                pricePass = (priceStr == "99") ? true : false;
                if (!pricePass) //if not $99 then maybe $39.99?
                {
                    pricePass = (priceStr == "39.99") ? true : false;
                }
                if (!pricePass)
                {
                    ViewBag.Pass = false;
                    ViewBag.Message = "Failed @ Get Price Check; shopifyProductPrice= " + priceStr;
                }
                else
                {
                    //perform Crystalift transform logic here: update shopifyProductID with transformed ID
                    long temp = TransformShopSKU.TransformShopProduct(shopifyProductID, shopifyProductPrice); //update with a Transformation method
                    if (temp != 0)
                    {
                        shopifyProductID = temp;
                        ShopifyLineItem sItem = new ShopifyLineItem(shopOrder.ShopifyOrderID);
                        long origID = 0;
                        Int64.TryParse(sItem.Product_id.ToString(), out origID);
                        AdminCart.Product origP = new AdminCart.Product(origID);
                        if (origP.Sku != "CL-P90-club-LI")
                        {
                            ViewBag.Pass = false;
                            ViewBag.Message = "Failed @ Get original Product SKU;  origP.Sku = " + origP.Sku + "; Should be: CL-P90-club-LI";
                        }
                        else
                        {
                            AdminCart.Product newP = new AdminCart.Product(shopifyProductID);
                            switch (priceStr)
                            {
                                case "99":
                                    {
                                        if (newP.Sku != "CL-P90-LI")
                                        {
                                            ViewBag.Pass = false;
                                            ViewBag.Message = "Failed @ Get new Product SKU: $99;  newP.Sku = " + newP.Sku + "; Should be: CL-P90-LI";
                                        }
                                        else
                                        {
                                            ViewBag.Pass = true;
                                            ViewBag.Message = "Passed @ Get new Product SKU: $99;  origP.Sku = " + origP.Sku + "; newP.Sku = " + newP.Sku + ";";
                                        }
                                    }
                                    break;

                                case "39.99":
                                    {
                                        if (newP.Sku != "CL-P90-LI")
                                        {
                                            ViewBag.Pass = false;
                                            ViewBag.Message = "Failed @ Get new Product SKU: $99;  newP.Sku = " + newP.Sku + "; Should be: CL-P90-LI";
                                        }
                                        else
                                        {
                                            ViewBag.Pass = true;
                                            ViewBag.Message = "Passed @ Get new Product SKU: $39.99;  origP.Sku = " + origP.Sku + "; newP.Sku = " + newP.Sku + ";";
                                        }
                                    }
                                    break;

                                default:
                                           ViewBag.Pass = false;
                                           ViewBag.Message = "Failed @ Get new Product SKU: $99 or $39.99;  origP.Sku = " + origP.Sku + "; newP.Sku = " + newP.Sku + ";";
                                    break;

                            }
                        }
                    }
                }
            }
            return View();
        }

        // GET: UnitTest
        public ActionResult Test_GetShopifyOrderRisk()
        {
            // pass 1 - a high risk order
            string shopName = "801354"; //jeff battman; sus fraud from UK
            long id = ShopifyAgent.ShopifyOrder.GetShopifyIDFromName(shopName, Config.CompanyID());
            List<ShopifyAgent.ShopOrderRisk> badRiskList = new List<ShopifyAgent.ShopOrderRisk>();
            ShopifyAgent.ShopOrderRisk risk = new ShopifyAgent.ShopOrderRisk();
            badRiskList = ShopifyAgent.ShopOrderRisk.GetOrderRiskList(id, AdminCart.Config.CompanyID());
            ViewBag.BadList = badRiskList;
            if (badRiskList.Count() == 0)
            {
                ViewBag.Pass = false;
                ViewBag.Message = "Pass 1: suspected Fraud: no Risks were found";
            }
            else
            {
                risk = badRiskList.First();
                if (risk.Id == 0)
                {
                    ViewBag.Pass = false;
                    ViewBag.Message = "Pass 1: suspected Fraud: riskList.First() not found";
                }
                else
                {
                    decimal dScore = (decimal)risk.Score * 100;
                    int score = (int)dScore;
                    if(score < 50)
                    {
                        ViewBag.Pass = false;
                        ViewBag.Message = "Pass 1: suspected Fraud: Score failed: Score= " + score.ToString();

                    }
                    else
                    {
                        //Pass 2 - a low risk order: Freeman, US
                        shopName = "801399"; //jeff battman; sus fraud from UK
                        id = ShopifyAgent.ShopifyOrder.GetShopifyIDFromName(shopName, Config.CompanyID());
                        List<ShopifyAgent.ShopOrderRisk> goodRiskList = ShopifyAgent.ShopOrderRisk.GetOrderRiskList(id, AdminCart.Config.CompanyID());
                        ViewBag.GoodList = goodRiskList;
                        if (goodRiskList.Count() == 0)
                        {
                            ViewBag.Pass = true;
                            ViewBag.Message = "Pass 2: Good Order: no risks were found";
                        }
                        else
                        {
                            risk = goodRiskList.First();
                            if (risk.Id == 0)
                            {
                                ViewBag.Pass = false;
                                ViewBag.Message = "Pass 2: Good Order: riskList.First() not found";
                            }
                            else
                            {
                                dScore = (decimal)risk.Score * 100;
                                score = (int)dScore;
                                if (score < 80)
                                {
                                    ViewBag.Pass = false;
                                    ViewBag.Message = "Pass 1: suspected Fraud: Score failed: Score= " + score.ToString();

                                }
                                ViewBag.Pass = true;
                                ViewBag.Message = "Pass 2: Good Order: Score Passed: Score= " + score.ToString();
                            }
                        }
                    }
                }
            }

            return View();
        }

        public ActionResult Test_Subscriptions_Subscribers()
        {
            Subscriptions subsTest = new Subscriptions
            {
                OrderID = -1,
                StripeSubID = "test_sub_" + DateTime.Today.ToString(),
                StripeCusID = "test_cus_" + DateTime.Today.ToString(),
                StripePlanID = "test_plan_" + DateTime.Today.ToString(),
                StripeSubDate = DateTime.Today,
                StripeStatus = "test_Active",
                StripePromoCode = "test_PromoCode"
            };
            int subsTestID = subsTest.Save(0);

            if (subsTestID == 0)
            {
                ViewBag.Pass = false;
                ViewBag.Msg = "Failed #1: initial Save";
            }
            else
            {
                Subscriptions subs2Test = new Subscriptions(subsTestID);
                if (subs2Test.StripeCusID != subsTest.StripeCusID)
                {
                    ViewBag.Pass = false;
                    ViewBag.Msg = "Failed #2: Read & Compare";
                }
                else
                {
                    subs2Test.StripeCusID = "test_cus_54321";
                    if (subs2Test.Save(subsTestID) != 0)
                    {
                        ViewBag.Msg = "Failed #3: Update before compare";
                        ViewBag.Pass = false;
                    }
                    else
                    {
                        Subscriptions subs3Test = new Subscriptions(subsTestID);
                        if (subs3Test.StripeCusID != "test_cus_54321")
                        {
                            ViewBag.Msg = "Failed #4: Update compare";
                            ViewBag.Pass = false;
                        }
                        else
                        {
                            ViewBag.Msg = "Passed: Insert, Update, Compare";
                            ViewBag.Pass = true;
                        }
                    }
                }
            }
            return View();
        }

        public ActionResult Test_GETStripeSubscriberFromPW()
        {
            StripeServices stripeServices = new StripeServices();
            StripeCustomer stripeCustomer = stripeServices.FindStripeCustomerByEmail("rmohme@alumni.uci.edu");
            if (!stripeCustomer.Id.Contains("cus_"))
            {
                ViewBag.Pass = false;
                string returnStr = (string.IsNullOrEmpty(stripeCustomer.Id)) ? "emptyStr" : stripeCustomer.Id;
                ViewBag.Msg = "Failed #1: couldn't find Stripe Subscriber: returned: " + returnStr;
            }
            else
            {
                ViewBag.Pass = true;
                string returnStr = (string.IsNullOrEmpty(stripeCustomer.Id)) ? "emptyStr" : stripeCustomer.Id;
                ViewBag.Msg = "Passed #1: successfully Stripe Subscriber via PayWhirl: returned: " + returnStr;
            }
            return View();
        }

        public ActionResult Test_GetStripeInvoiceList()
        {
            FMWebApp.ViewModels.Stripe.StripeCustomerViewModel svm = new FMWebApp.ViewModels.Stripe.StripeCustomerViewModel();

            StripeServices stripeServices = new StripeServices();
            StripeCustomer stripeCustomer = stripeServices.FindStripeCustomerByEmail("rmohme@alumni.uci.edu");
            if (!stripeCustomer.Id.Contains("cus_"))
            {
                ViewBag.Pass = false;
                string returnStr = (string.IsNullOrEmpty(stripeCustomer.Id)) ? "emptyStr" : stripeCustomer.Id;
                ViewBag.Msg = "Failed #1: couldn't find Stripe Subscriber: returned: " + returnStr;
                return View(svm);
            }

            svm.stripeCustomerInvoiceList = stripeServices.GetStripeCustomerInvoices(stripeCustomer.Id, out string errMsg);
            if (svm.stripeCustomerInvoiceList.Count() == 0)
            {
                ViewBag.Pass = false;
                ViewBag.Msg = "Failed #2: couldn't find Stripe Invoice List; errMsg: " + errMsg;
                return View(svm);
            }

            return View(svm);
        }

        //POST: Test Web Api StripeTestWebHooks
        public ActionResult StripeTestWebHooks()
        {
            string testURL = "http://localhost:56407/api/ShopifyAgent/StripeWebHooks";
            //string jsonStr = Test_evt_StripeSub_StatusChange_JSON();
            //string jsonStr = Test_evt_StripeSub_Created_noPromo_JSON();
            string jsonStr = Test_evt_StripeSub_Created_Promo_JSON();
            //string jsonStr = Test_evt_StripeSub_SubChargeSucceeded_JSON();
            string response = FMWebApp.Helpers.JsonHelpers.POSTtoWebAPI(testURL, jsonStr);
            ViewBag.What = "Test WebApi: StripeWebHooks(live)";
            ViewBag.URL = testURL;
            ViewBag.Response = response;
            return View();
        }

        //GET: Send test email for 2nd Charge Failure
        public ActionResult Test_Send2DeclineEmail()
        {
            AdminCart.Order order = new AdminCart.Order(90954); //test order
            bool ok = mvc_Mail.Send2DeclineEmail(order, 99.99M, "1234567812345678", DateTime.Now, true);
            if (ok)
            {
                ViewBag.Message = "Pass: Email Sent OK";
            }
            else
            {
                ViewBag.Pass = "Fail: Email NOT sent";
            }
 
            return View();
        }

        //GET: Reset all cache entries (CER, Products, etc.)
        public ActionResult Test_ResetCache()
        {
            CacheHelper.ClearAll(); //make sure the cache server is clear
            return RedirectToAction("Index", "AdminMVC");
        }

        //GET: Send test email for 2nd Charge Failure
        public ActionResult Test_GetInstallments()
        {
            string stripePlanName = "";
            int planInstallments = 0;
            string stripeCustomerID = "";
            string stripeSubscriptionID = "";

            switch (Config.appName.ToUpper())
            {
                case "FACEMASTER":
                    stripePlanName = "FaceMaster System 4 Easy Payments";
                    planInstallments = 4;
                    stripeCustomerID = "cus_84GSVyqko3ysIT";
                    stripeSubscriptionID = "sub_84GSrkGBVZhqqX";
                    break;

                case "CRYSTALIFT":
                    stripePlanName = "Crystalift $149.99 3 Pay";
                    planInstallments = 3;
                    stripeCustomerID = "cus_7hzmvuHzM5mQ7o";
                    stripeSubscriptionID = "sub_7hzmoQbIdP4fSh";
                    break;

                case "SONULASE":
                    stripePlanName = "Sonulase 4 Payments $49.99";
                    planInstallments = 4;
                    stripeCustomerID = "cus_7i0sqOt1DSSwxb";
                    stripeSubscriptionID = "sub_7i0sW3oC6VWAyA";
                    break;

                case "RMSHOP":
                    stripePlanName = "";
                    planInstallments = 3;
                    stripeCustomerID = "";
                    stripeSubscriptionID = "";
                    break;
            }

            int installments = PayWhirl.PayWhirlPlan.GetPlanInstallments(stripeCustomerID, stripeSubscriptionID);
            if ((installments > 0) && (installments == planInstallments))
            {
                //success!
                ViewBag.Pass = "yes";
            }
            else
            {
                //failure...
                ViewBag.Pass = "no";
            }

            ViewBag.PlanName = stripePlanName;
            ViewBag.installments = installments.ToString();
            return View();
        }

        //GET: Load a shopping cart....
        public ActionResult Test_GetLoadCart()
        {
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(94483); //FM using Suzanne50 promo code --> $199
            var cItem = (from AdminCart.Item item in cart.Items where item.LineItemProduct.IsFMSystem select item).FirstOrDefault();
            ViewBag.Pass = true; //assume the test passes
            if (cItem == null)
            {
                ViewBag.Pass = false;
            }

            return View(cart);
        }

        //GET: Find all customers by email (should be unique unless @facemaster.com & @suzannesomers.com)
        public ActionResult Test_IsEmailUnique()
        {
            bool isUnique = false;
            string email = "rmohme@alumni.uci.edu";
            isUnique = Customer.IsEmailUnique(email);
            ViewBag.Test1Pass = isUnique;
            ViewBag.Test1Email = email;
            
            email = "register@suzannesomers.com";
            isUnique = Customer.IsEmailUnique(email);
            ViewBag.Test2Pass = isUnique;
            ViewBag.Test2Email = email;

            return View();
        }

        public ActionResult Test_ListSize()
        {
            CacheHelper.ClearAll();
            List<ClassList> clList = new List<ClassList>();
            ClassList classList = new ClassList();

            classList.Name = "Orders";
            var stopwatch = Stopwatch.StartNew();
            OrderProvider orderProvider = new OrderProvider();
            List<Order> oList = orderProvider.GetAll();
            stopwatch.Stop();
            classList.NumberOfItems  = oList.Count;
            long oSize = GetObjSize(oList[0]);
            classList.NumberOfBytes = oSize * oList.Count;
            classList.ElapsedTime = stopwatch.ElapsedMilliseconds;
            clList.Add(classList);

            classList = new ClassList();
            classList.Name = "OrderDetails";
            OrderDetailProvider orderDetailProvider = new OrderDetailProvider();
            stopwatch = Stopwatch.StartNew();
            List<OrderDetail> odList = orderDetailProvider.GetAll();
            stopwatch.Stop();
            classList.NumberOfItems = odList.Count;
            long odSize = GetObjSize(odList[0]);
            classList.NumberOfBytes = odSize * odList.Count;
            classList.ElapsedTime = stopwatch.ElapsedMilliseconds;
            clList.Add(classList);

            classList = new ClassList
            {
                Name = "Customers"
            };
            CustomerProvider customerProvider = new CustomerProvider();
            stopwatch = Stopwatch.StartNew();
            List<Customer> cList = customerProvider.GetAll();
            stopwatch.Stop();
            classList.NumberOfItems = cList.Count;
            long cSize = GetObjSize(cList[0]);
            classList.NumberOfBytes = cSize * cList.Count;
            classList.ElapsedTime = stopwatch.ElapsedMilliseconds;
            clList.Add(classList);

            return View(clList);
        }

        private long GetObjSize(Object obj)
        {
            //return Marshal.SizeOf(obj);

            long size = 0;
            object o = new object();
            using (Stream s = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(s, o);
                size = s.Length;
            }
            return size;
        }



        public static string Test_evt_StripeSub_Created_noPromo_JSON()
        {
            string jsonStr = @"
                {
                  ""id"": ""evt_16c85BDl96SfQ18dFhLBMw3M"",
                  ""created"": 1440134933,
                  ""livemode"": true,
                  ""type"": ""customer.subscription.created"",
                  ""data"": {
                    ""object"": {
                      ""id"": ""sub_6pngUfgiQMQU39"",
                      ""plan"": {
                        ""interval"": ""month"",
                        ""name"": ""$99 Crystalift with Club Membership"",
                        ""created"": 1440134544,
                        ""amount"": 3999,
                        ""currency"": ""usd"",
                        ""id"": ""99-crystalift-with-club-membership"",
                        ""object"": ""plan"",
                        ""livemode"": true,
                        ""interval_count"": 1,
                        ""trial_period_days"": null,
                        ""metadata"": {},
                        ""statement_descriptor"": null,
                        ""statement_description"": null
                      },
                      ""object"": ""subscription"",
                      ""start"": 1440134931,
                      ""status"": ""active"",
                      ""customer"": ""cus_6pngMLLBIUtzJD"",
                      ""cancel_at_period_end"": false,
                      ""current_period_start"": 1440134931,
                      ""current_period_end"": 1442813331,
                      ""ended_at"": null,
                      ""trial_start"": null,
                      ""trial_end"": null,
                      ""canceled_at"": null,
                      ""quantity"": 1,
                      ""application_fee_percent"": 1,
                      ""discount"": null,
                      ""tax_percent"": null,
                      ""metadata"": {}
                    }
                  },
                  ""object"": ""event"",
                  ""pending_webhooks"": 1,
                  ""request"": ""req_6pnge96JSY1Iwu"",
                  ""api_version"": ""2014-06-17"",
                  ""user_id"": ""acct_169MFfDl96SfQ18d""
                }";
            //prep json string for replacements
            jsonStr = jsonStr.Replace("\r", "");
            jsonStr = jsonStr.Replace("\n", "");
            jsonStr = jsonStr.Replace(" ", "");
            return jsonStr;
        }

        public static string Test_evt_StripeSub_Created_Promo_JSON()
        {
            string jsonStr = @"
                {
                      ""id"": ""evt_1DBT9TINMTFgK3cecBW7SuUt"",
                      ""created"": 1537215417,
                      ""livemode"": true,
                      ""type"": ""customer.subscription.created"",
                      ""data"": {
                        ""object"": {
                          ""id"": ""sub_DciauEN8PkAcZ9"",
                          ""plan"": {
                            ""interval"": ""month"",
                            ""name"": ""FaceMaster System 4 Easy Payments"",
                            ""created"": 1483737203,
                            ""amount"": 7499,
                            ""currency"": ""usd"",
                            ""id"": ""facemaster---4-payments-of-7499"",
                            ""object"": ""plan"",
                            ""livemode"": true,
                            ""interval_count"": 1,
                            ""trial_period_days"": null,
                            ""metadata"": {},
                            ""statement_descriptor"": null
                          },
                          ""object"": ""subscription"",
                          ""start"": 1537215417,
                          ""status"": ""active"",
                          ""customer"": ""cus_DciazfmxCASlBW"",
                          ""cancel_at_period_end"": false,
                          ""current_period_start"": 1537215417,
                          ""current_period_end"": 1547215417,
                          ""ended_at"": null,
                          ""trial_start"": null,
                          ""trial_end"": null,
                          ""canceled_at"": null,
                          ""quantity"": 1,
                          ""application_fee_percent"": 1,
                          ""discount"": {
                            ""coupon"": {
                              ""id"": ""NewMail15"",
                              ""created"": 1436604406,
                              ""percent_off"": 15,
                              ""amount_off"": null,
                              ""currency"": null,
                              ""object"": ""coupon"",
                              ""livemode"": true,
                              ""duration"": ""forever"",
                              ""redeem_by"": null,
                              ""max_redemptions"": null,
                              ""times_redeemed"": 2,
                              ""duration_in_months"": null,
                              ""valid"": true,
                              ""metadata"": {}
                            },
                            ""start"": 1537215417,
                            ""object"": ""discount"",
                            ""customer"": ""cus_DciazfmxCASlBW"",
                            ""subscription"": ""ub_DciauEN8PkAcZ9"",
                            ""end"": null
                          },
                          ""tax_percent"": null,
                          ""metadata"": {}
                        }
                      },
                      ""object"": ""event"",
                      ""pending_webhooks"": 1,
                      ""request"": ""req_6ndMNCgJ1tgqCT"",
                      ""api_version"": ""2015-04-07""
                    }";
            //prep json string for replacements
            jsonStr = jsonStr.Replace("\r", "");
            jsonStr = jsonStr.Replace("\n", "");
            jsonStr = jsonStr.Replace(" ", "");
            return jsonStr;
        }

        public static string Test_evt_StripeSub_StatusChange_PastDue_JSON()
        {
            string jsonStr = @"
                {
                    ""id"": ""evt_00000000000000"",
                    ""created"": 1439484061,
                    ""livemode"": true,
                    ""type"": ""customer.subscription.updated"",
                    ""data"": {
                        ""object"": {
                        ""id"": ""sub_6bLep7347QeXAV"",
                        ""plan"": {
                            ""interval"": ""month"",
                            ""name"": ""FaceMaster System 4 Easy Payments"",
                            ""created"": 1435947415,
                            ""amount"": 6249,
                            ""currency"": ""usd"",
                            ""id"": ""facemaster-system-4-easy-payments"",
                            ""object"": ""plan"",
                            ""livemode"": true,
                            ""interval_count"": 1,
                            ""trial_period_days"": null,
                            ""metadata"": {},
                            ""statement_descriptor"": null
                        },
                        ""object"": ""subscription"",
                        ""start"": 1436801698,
                        ""status"": ""past_due"",
                        ""customer"": ""cus_6bLeNU1xkM0sRI"",
                        ""cancel_at_period_end"": false,
                        ""current_period_start"": 1439480098,
                        ""current_period_end"": 1442158498,
                        ""ended_at"": null,
                        ""trial_start"": null,
                        ""trial_end"": null,
                        ""canceled_at"": null,
                        ""quantity"": 1,
                        ""application_fee_percent"": 1.0,
                        ""discount"": null,
                        ""tax_percent"": null,
                        ""metadata"": {}
                        },
                        ""previous_attributes"": {
                        ""status"": ""active""
                        }
                    },
                    ""object"": ""event"",
                    ""pending_webhooks"": 2,
                    ""request"": null,
                    ""api_version"": ""2015-04-07""
                    }";
            //prep json string for replacements
            jsonStr = jsonStr.Replace("\r", "");
            jsonStr = jsonStr.Replace("\n", "");
            jsonStr = jsonStr.Replace(" ", "");
            return jsonStr;
        }

        public static string Test_evt_StripeSub_StatusChange_Active_JSON()
        {
            string jsonStr = @"
                {
                          ""id"": ""evt_00000000000000"",
                          ""created"": 1439743298,
                          ""livemode"": true,
                          ""type"": ""customer.subscription.updated"",
                          ""data"": {
                            ""object"": {
                              ""id"": ""sub_6bLep7347QeXAV"",
                              ""plan"": {
                                ""interval"": ""month"",
                                ""name"": ""FaceMaster System 4 Easy Payments"",
                                ""created"": 1435947415,
                                ""amount"": 6249,
                                ""currency"": ""usd"",
                                ""id"": ""facemaster-system-4-easy-payments"",
                                ""object"": ""plan"",
                                ""livemode"": true,
                                ""interval_count"": 1,
                                ""trial_period_days"": null,
                                ""metadata"": {},
                                ""statement_descriptor"": null
                              },
                              ""object"": ""subscription"",
                              ""start"": 1436801698,
                              ""status"": ""active"",
                              ""customer"": ""cus_6bLeNU1xkM0sRI"",
                              ""cancel_at_period_end"": false,
                              ""current_period_start"": 1439480098,
                              ""current_period_end"": 1442158498,
                              ""ended_at"": null,
                              ""trial_start"": null,
                              ""trial_end"": null,
                              ""canceled_at"": null,
                              ""quantity"": 1,
                              ""application_fee_percent"": 1.0,
                              ""discount"": null,
                              ""tax_percent"": null,
                              ""metadata"": {}
                            },
                            ""previous_attributes"": {
                              ""status"": ""past_due""
                            }
                          },
                          ""object"": ""event"",
                          ""pending_webhooks"": 2,
                          ""request"": null,
                          ""api_version"": ""2015-04-07""
                }";
            //prep json string for replacements
            jsonStr = jsonStr.Replace("\r", "");
            jsonStr = jsonStr.Replace("\n", "");
            jsonStr = jsonStr.Replace(" ", "");
            return jsonStr;
        }

        public static string Test_evt_StripeSub_SubChargeSucceeded_JSON()
        {
            string jsonStr = @"
                 {
  ""id"": ""evt_16p9xDINMTFgK3ceWdNqIqPZ"",
  ""created"": 1443240391,
  ""livemode"": true,
  ""type"": ""charge.succeeded"",
  ""data"": {
                ""object"": {
                    ""id"": ""ch_16p9xCINMTFgK3ce0ngXJUA4"",
      ""object"": ""charge"",
      ""created"": 1443240390,
      ""livemode"": true,
      ""paid"": true,
      ""status"": ""succeeded"",
      ""amount"": 6249,
      ""currency"": ""usd"",
      ""refunded"": false,
      ""source"": {
                        ""id"": ""card_16SfPcINMTFgK3ce3ZZaSduD"",
        ""object"": ""card"",
        ""last4"": ""2477"",
        ""brand"": ""Visa"",
        ""funding"": ""credit"",
        ""exp_month"": 5,
        ""exp_year"": 2017,
        ""fingerprint"": ""cD0ycg5MALr5kRzw"",
        ""country"": ""AU"",
        ""name"": ""Miss Julie Gargano"",
        ""address_line1"": null,
        ""address_line2"": null,
        ""address_city"": null,
        ""address_state"": null,
        ""address_zip"": null,
        ""address_country"": null,
        ""cvc_check"": null,
        ""address_line1_check"": null,
        ""address_zip_check"": null,
        ""tokenization_method"": null,
        ""dynamic_last4"": null,
        ""metadata"": { },
        ""customer"": ""cus_6g1Sn54F2sRAqR""
      },
      ""captured"": true,
      ""balance_transaction"": ""txn_16p9xDINMTFgK3ceWsiOdYso"",
      ""failure_message"": null,
      ""failure_code"": null,
      ""amount_refunded"": 0,
      ""customer"": ""cus_6g1Sn54F2sRAqR"",
      ""invoice"": ""in_16p8zOINMTFgK3ceRyq4i7yF"",
      ""description"": null,
      ""dispute"": null,
      ""metadata"": { },
      ""statement_descriptor"": null,
      ""fraud_details"": { },
      ""receipt_email"": null,
      ""receipt_number"": null,
      ""shipping"": null,
      ""destination"": null,
      ""application_fee"": null,
      ""refunds"": {
                        ""object"": ""list"",
        ""total_count"": 0,
        ""has_more"": false,
        ""url"": ""/v1/charges/ch_16p9xCINMTFgK3ce0ngXJUA4/refunds"",
        ""data"": []
    }
}
  },
  ""object"": ""event"",
  ""pending_webhooks"": 2,
  ""request"": null,
  ""api_version"": ""2015-04-07""
}";
            //prep json string for replacements
            jsonStr = jsonStr.Replace("\r", "");
            jsonStr = jsonStr.Replace("\n", "");
            jsonStr = jsonStr.Replace(" ", "");
            return jsonStr;
        }

        public static string Test_evt_StripeSub_ChargeSucceeded_JSON()
        {
            string jsonStr = @"
                 {
""id"": ""evt_16gijHINMTFgK3ce45zsNdgo"",
  ""created"": 1441229115,
  ""livemode"": true,
  ""type"": ""charge.succeeded"",
  ""data"": {
    ""object"": {
      ""id"": ""ch_16gijGINMTFgK3ceuY2Kd4ol"",
      ""object"": ""charge"",
      ""created"": 1441229114,
      ""livemode"": true,
      ""paid"": true,
      ""status"": ""succeeded"",
      ""amount"": 6250,
      ""currency"": ""usd"",
      ""refunded"": false,
      ""source"": {
        ""id"": ""card_16VSzyINMTFgK3ceDPId6GQr"",
        ""object"": ""card"",
        ""last4"": ""3667"",
        ""brand"": ""Visa"",
        ""funding"": ""credit"",
        ""exp_month"": 4,
        ""exp_year"": 2017,
        ""fingerprint"": ""hOIaomPAWYksLHyY"",
        ""country"": ""US"",
        ""name"": ""Jill Valdez"",
        ""address_line1"": null,
        ""address_line2"": null,
        ""address_city"": null,
        ""address_state"": null,
        ""address_zip"": null,
        ""address_country"": null,
        ""cvc_check"": null,
        ""address_line1_check"": null,
        ""address_zip_check"": null,
        ""tokenization_method"": null,
        ""dynamic_last4"": null,
        ""metadata"": {},
        ""customer"": ""cus_6iupQD2R5hcmF2""
      },
      ""captured"": true,
      ""balance_transaction"": ""txn_16gijHINMTFgK3ceKXhh2Ayu"",
      ""failure_message"": null,
      ""failure_code"": null,
      ""amount_refunded"": 0,
      ""customer"": ""cus_6iupQD2R5hcmF2"",
      ""invoice"": ""in_16ghmXINMTFgK3cex9C7yVaK"",
      ""description"": null,
      ""dispute"": null,
      ""metadata"": {},
      ""statement_descriptor"": null,
      ""fraud_details"": {},
      ""receipt_email"": null,
      ""receipt_number"": null,
      ""shipping"": null,
      ""destination"": null,
      ""application_fee"": null,
      ""refunds"": {
        ""object"": ""list"",
        ""total_count"": 0,
        ""has_more"": false,
        ""url"": ""/v1/charges/ch_16gijGINMTFgK3ceuY2Kd4ol/refunds"",
        ""data"": []
      }
    }
  },
  ""object"": ""event"",
  ""pending_webhooks"": 1,
  ""request"": null,
  ""api_version"": ""2015-04-07""
}";
            //prep json string for replacements
            jsonStr = jsonStr.Replace("\r", "");
            jsonStr = jsonStr.Replace("\n", "");
            jsonStr = jsonStr.Replace(" ", "");
            return jsonStr;
        }

        public static string Test_evt_StripeSub_SubscriptionDeleted_JSON()
        {
            string jsonStr = @"
                  {
  ""id"": ""evt_16awdrINMTFgK3ceKTtgi5F3"",
  ""created"": 1439852627,
  ""livemode"": true,
  ""type"": ""customer.subscription.deleted"",
  ""data"": {
    ""object"": {
      ""id"": ""sub_6ndMqpuo0przTm"",
      ""plan"": {
        ""interval"": ""month"",
        ""name"": ""FaceMaster System 4 Easy Payments"",
        ""created"": 1435947415,
        ""amount"": 6249,
        ""currency"": ""usd"",
        ""id"": ""facemaster-system-4-easy-payments"",
        ""object"": ""plan"",
        ""livemode"": true,
        ""interval_count"": 1,
        ""trial_period_days"": null,
        ""metadata"": {},
        ""statement_descriptor"": null
      },
      ""object"": ""subscription"",
      ""start"": 1439635239,
      ""status"": ""canceled"",
      ""customer"": ""cus_6ndM2bs3l9jIDJ"",
      ""cancel_at_period_end"": false,
      ""current_period_start"": 1439635239,
      ""current_period_end"": 1442313639,
      ""ended_at"": 1439852627,
      ""trial_start"": null,
      ""trial_end"": null,
      ""canceled_at"": 1439852627,
      ""quantity"": 1,
      ""application_fee_percent"": 1.0,
      ""discount"": {
        ""coupon"": {
          ""id"": ""NewMail15"",
          ""created"": 1436604406,
          ""percent_off"": 15,
          ""amount_off"": null,
          ""currency"": null,
          ""object"": ""coupon"",
          ""livemode"": true,
          ""duration"": ""forever"",
          ""redeem_by"": null,
          ""max_redemptions"": null,
          ""times_redeemed"": 2,
          ""duration_in_months"": null,
          ""valid"": true,
          ""metadata"": {}
        },
        ""start"": 1439635239,
        ""object"": ""discount"",
        ""customer"": ""cus_6ndM2bs3l9jIDJ"",
        ""subscription"": ""sub_6ndMqpuo0przTm"",
        ""end"": null
      },
      ""tax_percent"": null,
      ""metadata"": {}
    }
  },
  ""object"": ""event"",
  ""pending_webhooks"": 2,
  ""request"": ""req_6oZnOTnLs9uKQa"",
  ""api_version"": ""2015-04-07""
}";
            //prep json string for replacements
            jsonStr = jsonStr.Replace("\r", "");
            jsonStr = jsonStr.Replace("\n", "");
            jsonStr = jsonStr.Replace(" ", "");
            return jsonStr;
        }
    }
}