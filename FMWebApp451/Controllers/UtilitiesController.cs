﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models;

namespace FMWebApp451.Controllers
{
    public class UtilitiesController : Controller
    {
        // GET: Utilities
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PopulateLocations()
        {
            Location.InitializeFromXL();

            List<Location> lList = Location.GetAllLocations();
            return View(lList);
        }
    }
}