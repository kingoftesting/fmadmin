﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMWebApp451.Interfaces;
using FM2015.Models;
using AdminCart;

namespace FM2015.Controllers
{
    public class ValidationController : Controller
    {
        private readonly IValidationProvider validationProvider;

        public ValidationController(IValidationProvider validationProvider)
        {
            this.validationProvider = validationProvider;
        }

        // GET: Validation
        public ActionResult Index()
        {
            List<Validation> validationList = validationProvider.GetAll();
            validationList = validationList.OrderByDescending(v => v.Date).ToList();
            return View(validationList);
        }

        // GET: Validation/Details/5
        public ActionResult Details(int id)
        {
            Validation validation = validationProvider.GetById(id);
            return View(validation);
        }

        // GET: Validation/Create
        public ActionResult Create()
        {
            Validation validation = new Validation();

            validation.By = Models.User.GetUserName();
            validation.Date = DateTime.Now;
            return View(validation);
        }

        // POST: Validation/Create
        [HttpPost]
        public ActionResult Create(Validation validation)
        {
            if (ModelState.IsValid)
            {
                int result = validationProvider.Save(validation); //update DB entry
                return RedirectToAction("Details", "Validation", new { id = result });
            }
            return View(validation);
        }

        // GET: Validation/Edit/5
        public ActionResult Edit(int id)
        {
            Validation validation = validationProvider.GetById(id);
            return View(validation);
        }

        // POST: Validation/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Validation validation)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException("Validation(post): Edit: Id = 0");
            }
            if (ModelState.IsValid)
            {
                validation.ValidationId = id;
                int result = validationProvider.Save(validation); //update DB entry
                                                                //if (result <= 0) { dbErrorLogging.LogError(); }
                return RedirectToAction("Details", "Validation", new { id });
            }

            return View(validation);
        }

        // GET: Validation/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Validation/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
