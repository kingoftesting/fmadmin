﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using adminCER;
using antiforgery;

namespace FMWebApp451.Controllers
{
    public class JSONNameValuePair
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class apiCERController : ApiController
    {
        //GET version
        //[HttpGet]
        public string GetPing()
        {
            string response = "(" + AdminCart.Config.appName + ") Current AppVersion: " + AdminCart.Config.Version;
            return response;
        }

        //GET version
        [HttpGet]
        public IHttpActionResult RelatedCER()
        {
            string cerYear = "2016";
            string cerID = "2";
            if (string.IsNullOrEmpty(cerYear))
            {
                return NotFound();
            }

            if (string.IsNullOrEmpty(cerID))
            {
                return NotFound();
            }

            return Ok("RelatedCER GET was OK");
        }

        //POST version
        [HttpPost]
        [ValidateAjaxAntiForgeryToken]
        [ActionName("RelatedCER")]
        public HttpResponseMessage PostRelatedCER([FromBody] JSONNameValuePair[] nvArray ) //[FromBody]dynamic value; adminCER.CerID cerID
        {
            List<AJAXFormParams> responseList = Service.parseAJAXResponse(nvArray);
            if (responseList.Count == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            //now we have a list of CER year-id values that we want to add-to-related-CER
            foreach (AJAXFormParams cRelated in responseList)
            {
                adminCER.CER.AddRelatedCER(cRelated.CERNumber, cRelated.Year, cRelated.Id);
            }

            // Create a 200 response (should be a 201 "created" response w/ content & location of new Post)
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                //Content = new StringContent("Related CER succeeded")
            };
            //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { action = "GetPing" }));

            return response;
        }

        //POST version
        [HttpPost]
        [ValidateAjaxAntiForgeryToken]
        [ActionName("CERMessage")]
        public HttpResponseMessage PostCERMessage([FromBody] JSONNameValuePair[] nvArray) //[FromBody]dynamic value; adminCER.CerID cerID
        {
            List<AJAXFormParams> responseList = Service.parseAJAXResponse(nvArray);
            if (responseList.Count == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            foreach (AJAXFormParams cMessage in responseList)
            {
                adminCER.CER.AddMessage(cMessage.CERNumber, cMessage.Message);
            }

            // Create a 200 response (should be a 201 "created" response w/ content & location of new Post)
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                //Content = new StringContent("Related CER succeeded")
            };
            //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { action = "GetPing" }));

            return response;
        }

        //POST version
        [HttpPost]
        [ValidateAjaxAntiForgeryToken]
        [ActionName("DefectAnalysis")]
        public HttpResponseMessage PostDefectAnalysis([FromBody] JSONNameValuePair[] nvArray) //[FromBody]dynamic value; adminCER.CerID cerID
        {
            List<AJAXFormParams> responseList = Service.parseAJAXResponse(nvArray);
            if (responseList.Count == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            adminCER.CER cer = new adminCER.CER(responseList[0].CERNumber);
            CER.Defect defect = new CER.Defect(responseList[0].CERNumber);
            if ((cer.StatusMed.ToUpper() == "ASSIGNED") || (cer.StatusMed.ToUpper() == "CLOSED"))
            {
                defect.IsMedical = true;
            }

            foreach (AJAXFormParams cMessage in responseList)
            {
                if ((cer.StatusMed.ToUpper() == "ASSIGNED") || (cer.StatusMed.ToUpper() == "CLOSED"))
                {
                    defect.IsMedical = true;
                    defect.SymptomVal = cMessage.symptomVal;
                }
                int orderId = 0;
                Int32.TryParse(cer.OrderNumber, out orderId);
                if (orderId > 0)
                {
                    AdminCart.Order order = new AdminCart.Order(orderId);
                    if (order.scans.Count > 0)
                    {
                        foreach (var item in order.scans)
                        {
                            defect.SerialNum = item.Serial;
                        }
                    }
                }
                defect.Year = Convert.ToInt32(cer.CERYear);
                defect.CerNum = cer.CERIdentifier;
                defect.Date = DateTime.Now;
                defect.DefectVal = cMessage.defectVal;
                defect.DescriptionVal = cMessage.defectCatVal;
                defect.StorageLocation = cMessage.defectLocation.ToUpper();

                if (defect.Id == 0)
                {
                    defect.Add();
                }
                else
                {
                    defect.Update();
                }
            }

            // Create a 200 response (should be a 201 "created" response w/ content & location of new Post)
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                //Content = new StringContent("Related CER succeeded")
            };
            //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { action = "GetPing" }));

            return response;
        }

        //POST version
        [HttpPost]
        [ValidateAjaxAntiForgeryToken]
        [ActionName("ReturnAnalysis")]
        public HttpResponseMessage PostReturnAnalysis([FromBody] JSONNameValuePair[] nvArray) //[FromBody]dynamic value; adminCER.CerID cerID
        {
            List<AJAXFormParams> responseList = Service.parseAJAXResponse(nvArray);
            if (responseList.Count == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            adminCER.CER cer = new adminCER.CER(responseList[0].CERNumber);
            CER.Defect defect = new CER.Defect(responseList[0].CERNumber);
            if ((cer.StatusMed.ToUpper() == "ASSIGNED") || (cer.StatusMed.ToUpper() == "CLOSED"))
            {
                defect.IsMedical = true;
            }

            foreach (AJAXFormParams cMessage in responseList)
            {
                if ((cer.StatusMed.ToUpper() == "ASSIGNED") || (cer.StatusMed.ToUpper() == "CLOSED"))
                {
                    defect.IsMedical = true;
                    defect.SymptomVal = cMessage.symptomVal;
                }
                int orderId = 0;
                Int32.TryParse(cer.OrderNumber, out orderId);
                if (orderId > 0)
                {
                    AdminCart.Order order = new AdminCart.Order(orderId);
                    if (order.scans.Count > 0)
                    {
                        foreach (var item in order.scans)
                        {
                            defect.SerialNum = item.Serial;
                        }
                    }
                }
                defect.Year = Convert.ToInt32(cer.CERYear);
                defect.CerNum = cer.CERIdentifier;
                defect.Date = DateTime.Now;
                defect.DefectVal = cMessage.defectVal;
                defect.DescriptionVal = cMessage.defectVal;
                defect.StorageLocation = cMessage.defectLocation.ToUpper();

                if (defect.Id == 0)
                {
                    defect.Add();
                }
                else
                {
                    defect.Update();
                }
            }

            // Create a 200 response (should be a 201 "created" response w/ content & location of new Post)
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                //Content = new StringContent("Related CER succeeded")
            };
            //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { action = "GetPing" }));

            return response;
        }

        //POST version
        [HttpPost]
        [ValidateAjaxAntiForgeryToken]
        [ActionName("UpdateSerialNumber")]
        public HttpResponseMessage PutUpdateSerialNumber([FromBody] JSONNameValuePair[] nvArray) //[FromBody]dynamic value; adminCER.CerID cerID
        {
            List<AJAXFormParams> responseList = Service.parseAJAXResponse(nvArray);
            if (responseList.Count == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            adminCER.CER cer = new adminCER.CER(responseList[0].CERNumber);
            foreach (AJAXFormParams cMessage in responseList)
            {
                cer.SerialNumber = cMessage.serialNumber;
            }
            cer.Update();

            // Create a 200 response (should be a 201 "created" response w/ content & location of new Post)
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                //Content = new StringContent("Related CER succeeded")
            };
            //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { action = "GetPing" }));

            return response;
        }

        //POST version
        [HttpPost]
        [ValidateAjaxAntiForgeryToken]
        [ActionName("UpdateOrderNumber")]
        public HttpResponseMessage PutUpdateOrderNumber([FromBody] JSONNameValuePair[] nvArray) //[FromBody]dynamic value; adminCER.CerID cerID
        {
            List<AJAXFormParams> responseList = Service.parseAJAXResponse(nvArray);
            if (responseList.Count == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            adminCER.CER cer = new adminCER.CER(responseList[0].CERNumber);
            foreach (AJAXFormParams cMessage in responseList)
            {
                cer.OrderNumber = cMessage.orderNumber;
            }
            cer.Update();

            // Create a 200 response (should be a 201 "created" response w/ content & location of new Post)
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                //Content = new StringContent("Related CER succeeded")
            };
            //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { action = "GetPing" }));

            return response;
        }

    }
}
