﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FM2015.Models;
using AdminCart;
using PayWhirl;
using Stripe;
using FMWebApp.ViewModels.Stripe;

namespace FMWebApp451.Controllers
{
    public class apiSubscriptionsController : ApiController
    {

        //GET version
        //[HttpGet]
        public string GetPing()
        {
            string response = "(" + AdminCart.Config.appName + ") Current AppVersion: " + Config.Version;
            return response;
        }

    
        //GET version
        [HttpGet]
        public HttpResponseMessage CheckDeferredSubscriptions(bool isTest)
        {
            List<DelayedPlanChanges> dpcList = new List<DelayedPlanChanges>();
            dpcList = DelayedPlanChanges.GetDelayedPlanChangesList();
            List<DelayedPlanChanges> activateList = new List<DelayedPlanChanges>();
            activateList = (from dpc in dpcList where( (dpc.StartDate <= DateTime.Today) && (!dpc.Activated)) select dpc).ToList();
            string msg = "Test Mode: " + isTest.ToString() + Environment.NewLine;
            msg = "Subscriptions Activated (if any):" + Environment.NewLine;
            foreach (DelayedPlanChanges activate in activateList)
            {
                Customer c = new Customer(activate.CustomerID);
                msg += "activateID = " + activate.ID + Environment.NewLine;
                msg += "LastName = " + c.LastName + Environment.NewLine;
                string result = DelayedPlanChanges.Activate(activate, isTest); //test mode
                if (result != "success")
                {
                    msg += "Failed @ activateID = " + activate.ID + Environment.NewLine;
                    mvc_Mail.SendDiagEmail("api: CheckDeferredSubscription Failed", msg);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, msg);
                }
            }
            mvc_Mail.SendDiagEmail("api: CheckDeferredSubscription Succeeded", msg);
            return Request.CreateResponse(HttpStatusCode.OK, "Deferred Subscriptions Processed OK");
        }
    }
}
