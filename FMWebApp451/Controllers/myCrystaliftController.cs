﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminCart;
using FM2015.ViewModels;

namespace FM2015.Controllers
{
    public class myCrystaliftController : Controller
    {
        // GET: myCrystalift
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CustomerInfo()
        {
            CustomerViewModel cvm = new CustomerViewModel();
            cvm = (Session["cvm"] == null) ? cvm : (CustomerViewModel)Session["cvm"];
            ViewBag.ErrorMsg = "";
            cvm.Customer.Email = "email";
            cvm.Customer.Phone = "phone number";
            cvm.Customer.FirstName = "first name";
            cvm.Customer.LastName = "last name";
            cvm.ShipAddress.Street = "street";
            cvm.ShipAddress.Street2 = "street2";
            cvm.ShipAddress.City = "city";
            cvm.ShipAddress.Zip = "postal code"; 
            
            return View(cvm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CustomerInfo(CustomerViewModel cvm, string hdnShipState)
        {
            if ((string.IsNullOrEmpty(cvm.Customer.Email) || (cvm.Customer.Email == "email")))
            {
                ViewBag.ErrorMsg = "Email address is required";
                return View (cvm);
            }
            if ((string.IsNullOrEmpty(cvm.Customer.Phone) || (cvm.Customer.Email == "phone number")))
            {
                ViewBag.ErrorMsg = "Phone number is required";
                return View(cvm);
            }
            if ((string.IsNullOrEmpty(cvm.Customer.FirstName) || (cvm.Customer.FirstName == "first name")))
            {
                ViewBag.ErrorMsg = "First Name is required";
                return View(cvm);
            }
            if ((string.IsNullOrEmpty(cvm.Customer.LastName) || (cvm.Customer.LastName == "last name")))
            {
                ViewBag.ErrorMsg = "Last Name is required";
                return View(cvm);
            }
            if ((string.IsNullOrEmpty(cvm.ShipAddress.Street) || (cvm.ShipAddress.Street == "street")))
            {
                ViewBag.ErrorMsg = "Street is required";
                return View(cvm);
            }
            if (cvm.ShipAddress.Street2 == "street2")
            {
                cvm.ShipAddress.Street2 = "";
            }
            if ((string.IsNullOrEmpty(cvm.ShipAddress.City) || (cvm.ShipAddress.City == "city")))
            {
                ViewBag.ErrorMsg = "City is required";
                return View(cvm);
            }
            //string billState = Request["txtBillState"];
            //string shipState = Request["hdnShipState"];
            string shipState = hdnShipState;
            if (shipState.ToUpper() == "STATE/PROVINCE")
            {
                shipState = "";
            }
            cvm = FM2015.Helpers.Helpers.UpdateStateCountrySelectLists(cvm, shipState, shipState);

            if ((string.IsNullOrEmpty(cvm.ShipAddress.Zip) || (cvm.ShipAddress.City == "postal code")))
            {
                ViewBag.ErrorMsg = "Postal Code is required";
                return View(cvm);
            }

            string zipErr = FM2015.Helpers.DBUtil.CheckZipCode(cvm.ShipAddress.Country, cvm.ShipAddress.Zip);
            if (!string.IsNullOrEmpty(zipErr))
            {
                ViewBag.ErrorMsg = zipErr;
                return View(cvm);

            }

            Cart cart = new Cart();
            cart.SiteCustomer = cvm.Customer;
            cart.BillAddress = cvm.BillAddress;
            cart.ShipAddress = cvm.ShipAddress;
            Item item = new Item(13, 1); //CL-P90-club-LI --> CL-P90-LI, Crystalift Microdermabrasion Complete System - club
            cart.Items.AddItem(item);
            cart.CalculateTotals();
            Session["setupFee"] = 59.01M; //establish a setup fee for this offer; used in cart.SubscriptionProcessing
            Session["cvm"] = cvm;
            Session["shopCart"] = cart;
            return RedirectToAction("CustomerShipping");
        }

        public ActionResult CustomerShipping()
        {
            CustomerViewModel cvm = (CustomerViewModel)Session["cvm"];
            Cart cart = (Cart)Session["shopCart"];
            ViewBag.ShippingMethod = "Standard Ground";
            ViewBag.TotalShipping = cart.TotalShipping.ToString("C");
            ViewBag.TotalTax = cart.TotalTax.ToString("C");
            ViewBag.Total = cart.Total.ToString("C");
            return View(cvm);
        }

        public ActionResult CustomerPayment()
        {
            CustomerViewModel cvm = (CustomerViewModel)Session["cvm"];
            Cart cart = (Cart)Session["shopCart"];
            ViewBag.TotalShipping = cart.TotalShipping.ToString("C");
            ViewBag.TotalTax = cart.TotalTax.ToString("C");
            ViewBag.Total = cart.Total.ToString("C");
            return View(cart.Tran);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CustomerPayment(Transaction tran)
        {
            Cart cart = new Cart();
            cart = (Cart)Session["shopCart"];
            cart.Tran = tran;
            Session["shopCart"] = cart;
            ViewBag.TotalShipping = cart.TotalShipping.ToString("C");
            ViewBag.TotalTax = cart.TotalTax.ToString("C");
            ViewBag.Total = cart.Total.ToString("C");

            if (string.IsNullOrEmpty(tran.CardNo))
            {
                ViewBag.ErrorMsg = "Card Number is required";
                return View(tran);
            }
            if (tran.CardNo.Length < 15)
            {
                ViewBag.ErrorMsg = "Card is less than 15 characters.  Please correct, then try again.";
                return View(tran);
            }
            if (string.IsNullOrEmpty(tran.CardHolder))
            {
                ViewBag.ErrorMsg = "Name is required";
                return View(tran);
            }
            if (string.IsNullOrEmpty(tran.ExpDate))
            {
                ViewBag.ErrorMsg = "Expiration is required";
                return View(tran);
            }
            int temp = 0;
            if (!Int32.TryParse(tran.ExpDate.Replace("/",""), out temp))
            {
                ViewBag.ErrorMsg = "Expiration date must be in the form of MM/YY (for instance, 06/15)";
                return View(tran);
            }
            if (string.IsNullOrEmpty(tran.CvsCode))
            {
                ViewBag.ErrorMsg = "CVS is required";
                return View(tran);
            }
            temp = 0;
            if (!Int32.TryParse(tran.CvsCode, out temp))
            {
                ViewBag.ErrorMsg = "CVS must be all numbers";
                return View(tran);
            }

            tran.CardNo = tran.CardNo.Replace(" ", ""); //get rid of any spaces customer may have used
            tran.ExpDate = tran.ExpDate.Replace("/", ""); //get rid of possible / in MM/YY 
            cart.Tran = tran;

            CustomerViewModel cvm = new CustomerViewModel();
            cvm = (CustomerViewModel)Session["cvm"];
            if (cvm.BillAddress.AddressId == 0)
            {
                cvm.BillAddress = cvm.ShipAddress; //if no billing address then use the shipping address as default
            }
            cart.Tran.Address = cvm.BillAddress;
            Session["shopCart"] = cart;

            string promoCodeID = "";
            if (cart.SubscriptionProcessing(promoCodeID) != 0) //transaction was OK if result == 0
            {
                string errMsg = (Session["subErrorMessage"] == null) ? "" : Session["subErrorMessage"].ToString();
                int first = errMsg.IndexOf("errMessage:");
                int last = errMsg.Length;
                int subStringLength = last - first;
                if (first > 0)
                {
                    errMsg = errMsg.Substring(first, subStringLength);
                }
                errMsg = errMsg.Replace("errMessage:", "");
                ViewBag.ErrorMsg = "Your Order did not Successfully complete: " + errMsg;
                return View(tran);
            }
            return RedirectToAction("CustomerThankYou");
        }

        public ActionResult CustomerThankYou()
        {
            System.Threading.Thread.Sleep(1100);
            List<int> list = ShopifyAgent.ShopifyAgent.SyncShopify(); //sync orders from Shopify

            CustomerViewModel cvm = (CustomerViewModel)Session["cvm"];
            Cart cart = (Cart)Session["shopCart"];

            ViewBag.ShippingMethod = "Standard Ground";
            ViewBag.TotalShipping = cart.TotalShipping.ToString("C");
            ViewBag.TotalTax = cart.TotalTax.ToString("C");
            ViewBag.Total = cart.Total.ToString("C");
            ViewBag.Date = DateTime.Now.ToString();
            ViewBag.Name = cvm.Customer.FirstName + " " + cvm.Customer.LastName;
            ViewBag.Street = cvm.ShipAddress.Street;
            ViewBag.City = cvm.ShipAddress.City;
            ViewBag.State = cvm.ShipAddress.State;
            ViewBag.Zip = cvm.ShipAddress.Zip;
            ViewBag.Country = cvm.ShipAddress.Country;
            ViewBag.Email = cvm.Customer.Email;
            return View();
        }

    }
}