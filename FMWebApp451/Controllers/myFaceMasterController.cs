﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminCart;
using Stripe;
using FM2015.ViewModels;

namespace FMWebApp451.Controllers
{
    public class myFaceMasterController : Controller
    {
        public ActionResult InvoicePayment(string orderId, string invoiceId)
        {
            int oID = 0;
            int.TryParse(orderId, out oID);
            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(oID);
            CustomerViewModel cvm = new CustomerViewModel(cart.SiteCustomer);

            var invoiceService = new StripeInvoiceService();
            var invoiceServiceOptions = new StripeInvoiceListOptions();
            StripeInvoice stripeInvoice = new StripeInvoice();
            try
            {
                stripeInvoice = invoiceService.Get(invoiceId);
            }
            catch (StripeException ex)
            {
                HttpContext.Session["subErrorMessage"] = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "InvoicePayment: Get Invoce: Stripe Error");
                return View();
            }

            ViewBag.TotalShipping = cart.TotalShipping.ToString("C");
            ViewBag.TotalTax = cart.TotalTax.ToString("C");
            ViewBag.Total = cart.Total.ToString("C");
            return View(cart.Tran);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InvoicePayment(Transaction tran)
        {
            Cart cart = new Cart();
            cart = (Cart)Session["shopCart"];
            cart.Tran = tran;
            Session["shopCart"] = cart;
            ViewBag.TotalShipping = cart.TotalShipping.ToString("C");
            ViewBag.TotalTax = cart.TotalTax.ToString("C");
            ViewBag.Total = cart.Total.ToString("C");

            if (string.IsNullOrEmpty(tran.CardNo))
            {
                ViewBag.ErrorMsg = "Card Number is required";
                return View(tran);
            }
            if (tran.CardNo.Length < 15)
            {
                ViewBag.ErrorMsg = "Card is less than 15 characters.  Please correct, then try again.";
                return View(tran);
            }
            if (string.IsNullOrEmpty(tran.CardHolder))
            {
                ViewBag.ErrorMsg = "Name is required";
                return View(tran);
            }
            if (string.IsNullOrEmpty(tran.ExpDate))
            {
                ViewBag.ErrorMsg = "Expiration is required";
                return View(tran);
            }
            int temp = 0;
            if (!Int32.TryParse(tran.ExpDate.Replace("/", ""), out temp))
            {
                ViewBag.ErrorMsg = "Expiration date must be in the form of MM/YY (for instance, 06/15)";
                return View(tran);
            }
            if (string.IsNullOrEmpty(tran.CvsCode))
            {
                ViewBag.ErrorMsg = "CVS is required";
                return View(tran);
            }
            temp = 0;
            if (!Int32.TryParse(tran.CvsCode, out temp))
            {
                ViewBag.ErrorMsg = "CVS must be all numbers";
                return View(tran);
            }

            tran.CardNo = tran.CardNo.Replace(" ", ""); //get rid of any spaces customer may have used
            tran.ExpDate = tran.ExpDate.Replace("/", ""); //get rid of possible / in MM/YY 
            cart.Tran = tran;

            CustomerViewModel cvm = new CustomerViewModel();
            cvm = (CustomerViewModel)Session["cvm"];
            if (cvm.BillAddress.AddressId == 0)
            {
                cvm.BillAddress = cvm.ShipAddress; //if no billing address then use the shipping address as default
            }
            cart.Tran.Address = cvm.BillAddress;
            Session["shopCart"] = cart;

            string promoCodeID = "";
            if (cart.SubscriptionProcessing(promoCodeID) != 0) //transaction was OK if result == 0
            {
                string errMsg = (Session["subErrorMessage"] == null) ? "" : Session["subErrorMessage"].ToString();
                int first = errMsg.IndexOf("errMessage:");
                int last = errMsg.Length;
                int subStringLength = last - first;
                if (first > 0)
                {
                    errMsg = errMsg.Substring(first, subStringLength);
                }
                errMsg = errMsg.Replace("errMessage:", "");
                ViewBag.ErrorMsg = "Your Order did not Successfully complete: " + errMsg;
                return View(tran);
            }
            return RedirectToAction("CustomerThankYou");
        }

        public ActionResult InvoiceThankYou()
        {
            System.Threading.Thread.Sleep(1100);
            List<int> list = ShopifyAgent.ShopifyAgent.SyncShopify(); //sync orders from Shopify

            CustomerViewModel cvm = (CustomerViewModel)Session["cvm"];
            Cart cart = (Cart)Session["shopCart"];

            ViewBag.ShippingMethod = "Standard Ground";
            ViewBag.TotalShipping = cart.TotalShipping.ToString("C");
            ViewBag.TotalTax = cart.TotalTax.ToString("C");
            ViewBag.Total = cart.Total.ToString("C");
            ViewBag.Date = DateTime.Now.ToString();
            ViewBag.Name = cvm.Customer.FirstName + " " + cvm.Customer.LastName;
            ViewBag.Street = cvm.ShipAddress.Street;
            ViewBag.City = cvm.ShipAddress.City;
            ViewBag.State = cvm.ShipAddress.State;
            ViewBag.Zip = cvm.ShipAddress.Zip;
            ViewBag.Country = cvm.ShipAddress.Country;
            ViewBag.Email = cvm.Customer.Email;
            return View();
        }
    }
}