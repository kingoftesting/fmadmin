﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Controls_Bing_FB_Tracking : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlHead h = new HtmlHead();
        Literal l = new Literal();
        l.Text = @"
            <!-- Facebook Conversion Code for FaceMaster Checkouts -->
            <script>
                (function() {
                    var _fbq = window._fbq || (window._fbq = []);
                    if (!_fbq.loaded) {
                        var fbds = document.createElement('script');
                        fbds.async = true;
                        fbds.src = '//connect.facebook.net/en_US/fbds.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(fbds, s);
                         _fbq.loaded = true;
                    }
                })();
                window._fbq = window._fbq || [];
                window._fbq.push(['track', '6016720937591', {'value':'0.00','currency':'USD'}]);
            </script>
            <noscript>
	            <img height='1' width='1' alt='' style='display:none' src='ttps://www.facebook.com/tr?ev=6016720937591&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1' />
            </noscript>

            <!-- for Bing -->
            <script type='text/javascript'> 
	            if (!window.mstag) mstag = {loadTag : function(){},time : (new Date()).getTime()};
            </script> 
            <script id='mstag_tops' type='text/javascript' src='//flex.msn.com/mstag/site/289c3e4e-c80f-46dc-95d2-371f0cb4aca2/mstag.js;></script> 
            <script type='text/javascript'> 
	            mstag.loadTag('analytics', {dedup:'1',domainId:'3039801',type:'1',revenue:'',actionid:'246469'})
            </script> 
            <noscript> 
	            <iframe src='//flex.msn.com/mstag/tag/289c3e4e-c80f-46dc-95d2-371f0cb4aca2/analytics.html?dedup=1&domainId=3039801&type=1&revenue=&actionid=246469' frameborder='0' scrolling='no' width='1' height='1' style='visibility:hidden;display:none'> </iframe> 
            </noscript>
        ";
        //h.Controls.Add(l);
        this.Page.Header.Controls.Add(l);
    }
}