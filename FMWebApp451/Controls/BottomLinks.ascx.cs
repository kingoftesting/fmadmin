namespace FaceMaster
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
    using System.Data.SqlClient;
    using FM2015.Helpers;

	/// <summary>
	///		Summary description for BottomLinks.
	/// </summary>
	public class BottomLinks : System.Web.UI.UserControl
	{
        protected System.Web.UI.WebControls.Literal Literal1;

		private void Page_Load(object sender, System.EventArgs e)
		{
			Literal1.Text = "";
			//if(!IsPostBack)
			//{
				SqlDataReader dr = DBUtil.FillDataReader("SELECT * FROM Pages where BottomNavsort is not null order by bottomNavsort asc");
				while(dr.Read())
				{				
					string PageLink =  dr["PageLink"].ToString() == "" ? "Page.aspx?PageID=" + dr["PageID"].ToString() : dr["PageLink"].ToString();
                    Literal1.Text += "here" + "<a class=afoot href=" + PageLink + ">" + dr["PageTitle"] + "</a>&nbsp;&nbsp;&nbsp;";
				}
				dr.Close();
			//}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion
	}
}
