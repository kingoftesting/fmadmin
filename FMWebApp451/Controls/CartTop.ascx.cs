namespace FaceMaster
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
    using Certnet;
	/// <summary>
	///		Summary description for CartTop.
	/// </summary>
	public class CartTop : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Literal CartTopLit;
		Cart cart = new Cart();

        //void Init(Object sender, EventArgs e)
        //{
         //   cart = new Cart();
         //   cart.Load(Session["cart"]);
       // }
        
		private void Page_Load(object sender, System.EventArgs e)
		{
			cart.Load(Session["Cart"]);

            

			CartTopLit.Text  = @"<span id=logoline>" + Spaces(1) + "<img src=images/FaceMasterLogo9.gif>" + Spaces(33) + "</span>";
			CartTopLit.Text += @"<span id=carttop><a href=ViewCart.aspx><img src=images\cart3.gif border=0>View Cart</a>" + Spaces(3);
            if (cart.SiteCustomer.CustomerID == 0)
                
                {
				CartTopLit.Text += @"<a href=Login.aspx>Login</a>" + Spaces(3);
			} 
			else 
			{
				CartTopLit.Text += @"<a href=Logout.aspx>Logout</a>" + Spaces(3);
			
			}
			if (Request["process"] == "Checkout")
				CartTopLit.Text += @"<a href=MyAccount.aspx?Process=Checkout>Register</a></span>";
			else
				CartTopLit.Text += @"<a href=MyAccount.aspx>Register</a></span>";
		}

		protected string Spaces(int NumberOfSpaces)
		{
			string spaces = "";

			for(int i=0;i<NumberOfSpaces;i++)
			{
				spaces += @"&nbsp";
			}

			return spaces;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
