﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="EmailSubscribe" Codebehind="EmailSubscribe.ascx.cs" %>

<div class="subscribeFloatBox">

    <div id="mc_embed_signup">

        <asp:Label ID="Label1" runat="server">
            <h2><strong>Subscribe To Our Email List</strong></h2>
            <h3>Sign Up to get the lastest offers and promotions and receive a coupon for 
                <span style="color:#eb0184;"><strong>15%-off</strong></span> your next FaceMaster purchase!</h3>
        </asp:Label>
        <div style="clear: both"></div>


        <p class="indicates-required"><span class="asterisk">*</span> indicates required</p>


        <div class="mc-field-group">
            <label for="mce-EMAIL">
                Email Address  <span class="asterisk">*</span>
            </label>
            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
        </div>
        <p>&nbsp;</p>
        <div class="mc-field-group">
            <label for="mce-FNAME">First Name </label>
            <input type="text" value="" name="FNAME" class="required email" id="mce-FNAME">
        </div>

        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
        <div style="position: absolute; left: -5000px;">
            <input type="text" name="b_f8a0dbc89da8e8217b0f31802_bd2f335dfd" tabindex="-1" value="">
        </div>

        <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display: none"></div>
            <div class="response" id="mce-success-response" style="display: none"></div>
        </div>
        <p>&nbsp;</p>
        <asp:Button ID="Button1" PostBackUrl="http://facemaster.us8.list-manage.com/subscribe/post?u=f8a0dbc89da8e8217b0f31802&amp;id=bd2f335dfd" Text="Subscribe" runat="server" ValidationGroup="emailSubscribe"></asp:Button>
    </div>

</div>

<!--
<input type="Text" style="width: 55%" value="" name="EMAIL" class="email" id="Email1" placeholder="email address" required="true" />
-->

