<%@ Control Language="C#" AutoEventWireup="true" Inherits="FooterCtl" Codebehind="FooterCtl.ascx.cs" %>

<table>
    <tr>
        <td style="background-color: #636466">
            <asp:ImageMap ID="ImageMap3" ImageUrl="../images/SliceStuff/ToolBarBottom.jpg" HotSpotMode="Navigate"  runat="server" AlternateText="">
            <asp:RectangleHotSpot Top="0" Bottom="25" Left="290" Right="400" AlternateText="Privacy Policy" NavigateURL="../PrivacyStatement.aspx" />
            <asp:RectangleHotSpot Top="0" Bottom="25" Left="410" Right="535" AlternateText="Terms of Service" NavigateURL="../Help.aspx#TermsAndConditions.aspx" />
            <asp:RectangleHotSpot Top="0" Bottom="25" Left="550" Right="640" AlternateText="Contact Us" NavigateURL="../ContactUs.aspx" />
            <asp:RectangleHotSpot Top="0" Bottom="25" Left="650" Right="695" AlternateText="Help" NavigateURL="../Help.aspx" />
            <asp:RectangleHotSpot Top="0" Bottom="25" Left="700" Right="745" AlternateText="FAQs" NavigateURL="../Faq.aspx" />
            </asp:ImageMap>
             <!--
            <asp:HyperLink ID="HyperLink2" runat="server" Text="PRIVACY POLICY" NavigateUrl="../PrivacyStatement.aspx"  /> | 
            <asp:HyperLink ID="HyperLink1" runat="server" Text="TERMS OF SERVICE" NavigateUrl="../Help.aspx#TermsAndConditions"  /> | 
            <asp:HyperLink ID="HyperLink56" runat="server" Text="CONTACT US" NavigateUrl="../ContactUs.aspx"  />  | 
            <asp:HyperLink ID="HyperLink54" runat="server" Text="HELP" NavigateUrl="../Help.aspx" />  |  
            <asp:HyperLink ID="HyperLink52" runat="server" Text="FAQS" NavigateUrl="../Faq.aspx" />            
            -->
       </td>
    </tr>
    <tr>
        <td class="notify">
            <b>NOTICE: Counterfeit FaceMasters are being sold on eBay and Amazon. For more information click here:</b> &nbsp;
            <asp:HyperLink ID="lnkNotify" Text="Unauthorized and Counterfeit Product" NavigateUrl="~/Notify1.aspx" runat="server">Unauthorized and Counterfeit Product</asp:HyperLink>
            <br />
        </td>
    </tr>
    <tr>
        <td align="center" style="background-color: white">
            <font color="black">�<asp:Label ID="lblYearNow" CSSClass="copy" runat="server" />  FaceMaster of Beverly Hills, Inc. All Rights Reserved</font>
            <!--
            <asp:Image ID="Image1" ImageUrl="../images/SliceStuff/copyright.jpg" runat="server" />
            -->
            <br />
            <!-- GeoTrust QuickSSL [tm] Smart  Icon tag. Do not edit. -->
            <!--
            <script language="JavaScript"  type="text/javascript"  
            src="//smarticon.geotrust.com/si.js"></script>
            -->
            <!-- end  GeoTrust Smart Icon tag -->         
        </td>
    </tr>            	
</table>
