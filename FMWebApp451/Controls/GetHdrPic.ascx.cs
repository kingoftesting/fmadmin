using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class GetHdrPic : System.Web.UI.UserControl
{
    //Variables that are global to this web page
    private string LHeader = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //declare and initialize variables; set default to 1
        int id = 3;

        //Random random = new Random();
        //id = random.Next() % 5;

        //main page logic
        switch (id)
        {

            case 1:
                LHeader = @"Learn about my #1 beauty secret";
                LHeader += "<br /> Suzanne Somers";
                break;

            case 2:
                LHeader = @"Hi Peter! Just letting you know";
                LHeader += "<br /> we can change this space";
                break;

            case 3:
                LHeader = @"Defy Gravity!";
                LHeader += "<br /> Buy your FaceMaster today!";
                break;

            case 4:
                LHeader = @"Free Shipping Forever!";
                LHeader += "<br /> Buy your FaceMaster today!";
                break;

            default:
                LHeader = @"Learn about my #1 beauty secret";
                LHeader += "<br /> Suzanne Somers";
                break;
        }

        Literal1.Text = LHeader;
    }

}
