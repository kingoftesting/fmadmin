using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Certnet;

public partial class Controls_Header : System.Web.UI.UserControl
{

    Cart cart = new Cart();

    protected void Page_Load(object sender, EventArgs e)
    {
        bool IsDev = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsDevelopment"]);
        lnkAdmin.Visible = IsDev;

        Page.Form.DefaultButton = SearchButton1.UniqueID;

        if (!IsPostBack)
        {
            cart.Load(Session["cart"]);

            if (cart.SiteCustomer.CustomerID == 0)
            {
                lnkLogin.Text = "Sign In";
                lnkLogin.NavigateUrl = "../Login.aspx";
            }
            else
            {
                lnkLogin.Text = "Sign Out";
                lnkLogin.NavigateUrl = "../Logout.aspx";
            }
        }

    }

    protected void SearchButton1_Click(object sender, ImageClickEventArgs e)
    {
        if (SearchBox1.Text != null)
        {
            if (SearchBox1.Text.ToString() != "Search...")
            {
                Response.Redirect("SearchResult.aspx?SearchBox=" + SearchBox1.Text.ToString());
            }
        }

    }
}
