<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_LeftCol_MstrNoContent" Codebehind="LeftCol_MstrNoContent.ascx.cs" %>

<table id="Table_01" style="width:300px; border:0;">

    <tr>
		<td>
            <asp:ImageMap ID="ImageMap2" ImageUrl="../images/SliceStuff/LeftNav_GTL_2.jpg" HotSpotMode=Navigate  runat="server" AlternateText="">
            <asp:RectangleHotSpot Top="0" Bottom="169" Left="0" Right="319" AlternateText="Discover Suzanne Somer's #1 Skin Care Beauty Secret: FaceMaster Platinum! Reduce Wrinkles  - Learn More" NavigateURL="../GetTheLook.aspx" />
            </asp:ImageMap>
	    </td>
	</tr>
	<tr>
		<td>
            <asp:ImageMap ID="ImageMap1" ImageUrl="../images/SliceStuff/LeftNav_FW.jpg" width="300" height="140" HotSpotMode=Navigate  runat="server" AlternateText="">
            <asp:RectangleHotSpot Top="0" Bottom="130" Left="0" Right="320" AlternateText="Finger Wands! New for Suzanne Somer's #1 Skin Care Beauty Secret: FaceMaster Platinum! - Learn More" NavigateURL="../FingerWands.aspx" />
            </asp:ImageMap>
       </td>
	</tr>
	<tr>
		<td>
            <asp:ImageMap ID="ImageMap4" ImageUrl="../images/SliceStuff/LeftNav_GS.jpg" width="300" height="220" HotSpotMode=Navigate  runat="server" AlternateText="">
            <asp:RectangleHotSpot Top="0" Bottom="215" Left="0" Right="320" AlternateText="Getting Started With Suzanne Somer's #1 Skin Care Beauty Secret: FaceMaster Platinum! Begin Reducing Fine Line & Wrinkles Now! - Learn More" NavigateURL="../GettingStarted.aspx" />
            </asp:ImageMap>
	    </td>
	</tr>
	<tr>
		<td>
            <asp:ImageMap ID="ImageMap5" ImageUrl="../images/SliceStuff/LeftNav_VL.jpg" width="300" height="200" HotSpotMode=Navigate  runat="server" AlternateText="">
            <asp:RectangleHotSpot Top="0" Bottom="195" Left="0" Right="320" AlternateText="Video Libray - Suzanne Somer's #1 Skin Care Beauty Secret: FaceMaster Platinum! Watch How The FaceMaster Is Used To Reduce Fine Lines & Wrinkles For A More Ageless & Youthful Look - View Here" NavigateURL="../VideoTips.aspx" />
            </asp:ImageMap>
	    </td>
	</tr>
</table>
