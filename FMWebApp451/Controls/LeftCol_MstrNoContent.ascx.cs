using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Controls_LeftCol_MstrNoContent : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //Create META Keywords
        HtmlMeta metaKeywords = new HtmlMeta();
        metaKeywords.Name = "KEYWORDS";
        metaKeywords.Content = "Suzanne somers, facemaster, facemaster, facemaster platinum";

        //Add META controls to HtmlHead
        HtmlHead head = (HtmlHead)Page.Header;
        head.Controls.Add(metaKeywords);
    }
}
