<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_MstrBenefitsList" Codebehind="MstrBenefitsList.ascx.cs" %>

<p class="standardtext">
The FaceMaster works <strong>wonders</strong> on tired, aging skin to reveal a fresh, rejuvenated complexion that 
will keep your friends guessing&#8230; did she or didn�t she? Great for men, too.
<br /><br />
<ul>
    <li><strong>Plump up</strong> the volume of those facial muscles - <strong>reduce</strong> the look of lines & wrinkles!</li>
    <li><strong>Lift</strong> the upper lid, hold back that sagging "curtain" effect by toning the muscles.</li>
    <li><strong>Pull up</strong> the lower lid, help reduce that tired "baggage" under the eyes!</li>
    <li><strong>Open</strong>  your eyes, look more alert!</li>
</ul>
<br /><br />
</p>

