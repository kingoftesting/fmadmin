<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_MstrBenefitsLower" Codebehind="MstrBenefitsLower.ascx.cs" %>

<p class="standardtext">
<strong>The FaceMaster uses micro-currents</strong> of electrical energy to stimulate and strengthen underlying 
facial muscles. Every time you use it, you�ll notice a visible improvement in overall skin tone 
and elasticity. And it�s easy! Computerized graphics show you just what areas need toning and 
for how long.
<br /><br />
</p>
<ul>
    <li><strong>Reduce</strong>  the look of the "puppet lines" or "parenthesis/brackets" beside the mouth</li>
    <li><strong>Help Increase</strong>  the color and circulation to the face; get back those "apple cheeks" of your younger days.</li>
    <li><strong>Erase!</strong>  Use the Feathering step to help "Erase" those wrinkles away.  Reduce the look of furrows in the forehead, the "eleven" lines between the brows, and the fine "crow's feet" lines beside the eyes.</li>
    <li><strong>Help Thicken your Lips!</strong> Reduce the zig-zag line at the edges of your lipstick  
        Nobody likes lipstick that looks like an "electrocardiogram".  The FaceMaster helps to plump-up thin lips, increase the volume under the lips, and provides a smooth platform to show off the lipstick!</li>
    <li><strong>Smooth</strong> your skin</li>
</ul>

