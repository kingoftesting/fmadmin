<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_MstrMenu3" Codebehind="MstrMenu3.ascx.cs" %>

<table>
<tr>
    <td class="contentmenulinklower" >
        &nbsp &nbsp &nbsp &nbsp
        <asp:HyperLink ID="lnkCollagen" runat="server" Text="Collagen" NavigateUrl="../Collagen.aspx" />
        &nbsp &nbsp
        <asp:HyperLink ID="lnkSkinCare" runat="server" Text="Skin Care" NavigateUrl="../SkinCare.aspx" />
        &nbsp &nbsp
        <asp:HyperLink ID="lnkResults" runat="server" Text="Results" NavigateUrl="../Results.aspx" />
        &nbsp &nbsp
        <asp:HyperLink ID="lnkAntiAging" runat="server" Text="AntiAging" NavigateUrl="../AntiAging.aspx" />
        &nbsp &nbsp
        <asp:HyperLink ID="lnkShipping" runat="server" Text="Shipping Policy" NavigateUrl="../Help.aspx#ShippingPolicy.aspx" />
        &nbsp &nbsp
        <asp:HyperLink ID="lnkFaqs" runat="server" Text="FAQs" NavigateUrl="../FAQ.aspx" />
        &nbsp &nbsp
        <asp:HyperLink ID="lnkOpInst" runat="server" Text="Operating Instructions" NavigateUrl="../OperatingInstructions.aspx" Target="_blank" />
    </td>
</tr>
</table>