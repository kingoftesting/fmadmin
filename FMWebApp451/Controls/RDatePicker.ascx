<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_RDatePicker" Codebehind="RDatePicker.ascx.cs" %>

    <asp:TextBox ID="TextBox1" width="75px" runat="server"></asp:TextBox>
    <asp:LinkButton ID="lnkButton2" OnClick="lnkButton2_MakeVisible" runat="server">View Cal</asp:LinkButton>
    <br />
    <asp:CustomValidator ID="valTextBox1" runat="server" ControlToValidate="TextBox1" ValidationGroup = "ListByDate"
    Display="Dynamic" ErrorMessage="<br />The format of the Date is not valid. Please use mm/dd/yyyy" OnServerValidate="valTextBox1_ServerValidate"></asp:CustomValidator>
    <br />
    <asp:Panel ID="Panel1" Visible="false" runat="server">
    <table>
        <tr>
            <td style="text-align:left; background-color:#cccccc">
                <asp:DropDownList id="drpCalMonth" Runat="Server" AutoPostBack="True" OnSelectedIndexChanged="Set_Calendar" cssClass="calTitle"></asp:DropDownList>
            </td>
            <td style="text-align:right; background-color:#cccccc">
                <asp:DropDownList id="drpCalYear" Runat="Server" AutoPostBack="True" OnSelectedIndexChanged="Set_Calendar" cssClass="calTitle"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Calendar 
                    id="Calendar1" 
                    Runat="Server" 
                    cssClass="calBody" 
                    OtherMonthDayStyle-BackColor="White" 
                    DayStyle-BackColor="LightYellow" 
                    DayHeaderStyle-BackColor="#eeeeee" 
                    SelectedDayStyle-BackColor="blue"
                    NextPrevFormat="ShortMonth"                    
                    Width="100%"
                    OnSelectionChanged="Calendar1_OnSelectionChanged"
                    OnVisibleMonthChanged="Calendar1_OnVisibleMonthChanged">
                </asp:Calendar>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:LinkButton ID="lnkButton1" OnClick="lnkButton1_SelectTodaysDate" runat="server">Today's Date</asp:LinkButton>
            </td>
        </tr>
    </table>
    </asp:Panel>
