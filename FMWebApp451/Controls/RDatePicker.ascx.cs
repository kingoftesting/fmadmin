using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Controls_RDatePicker : System.Web.UI.UserControl
{
    private string selecteddate;
    public string SelectedDate
    {
        get { return selecteddate = TextBox1.Text; }
        set { 
                selecteddate = value;
                Calendar1.SelectedDate = DateTime.Parse(selecteddate);
                Calendar1.VisibleDate = DateTime.Parse(selecteddate);
                TextBox1.Text = selecteddate;
            }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //Calendar1.ShowTitle = false;

        if (!IsPostBack)
        {
            //DateTime time1 = DateTime.Now;

            //TextBox1.Text = time1.ToShortDateString();
            Populate_MonthList();
            Populate_YearList();
        }
    }

    protected void Calendar1_OnSelectionChanged(object sender, EventArgs e)
    {
        DateTime newDate = Calendar1.SelectedDate;
        TextBox1.Text = newDate.ToShortDateString();


        String strmonth = newDate.ToString("MMMM");
        ListItem item = drpCalMonth.Items.FindByText(strmonth);
        drpCalMonth.SelectedIndex = drpCalMonth.Items.IndexOf(item);

        drpCalYear.SelectedIndex = drpCalYear.Items.IndexOf(drpCalYear.Items.FindByText(newDate.Year.ToString()));
    }

    protected void Calendar1_OnVisibleMonthChanged(Object sender, MonthChangedEventArgs e)
    {
        DateTime newDate = e.NewDate;
        DateTime previousDate = e.PreviousDate;
        String strmonth = newDate.ToString("MMMM");
        ListItem item = drpCalMonth.Items.FindByText(strmonth);
        drpCalMonth.SelectedIndex = drpCalMonth.Items.IndexOf(item);

        drpCalYear.SelectedIndex = drpCalYear.Items.IndexOf(drpCalYear.Items.FindByText(newDate.Year.ToString()));
        Calendar1.SelectedDate = newDate;
        Calendar1.VisibleDate = newDate;
        TextBox1.Text = newDate.ToShortDateString();

    }

    protected void Set_Calendar(object sender, EventArgs e)
    {
        //Whenever month or year selection changes display the calendar for that month/year
        DateTime currentDate = Calendar1.TodaysDate;
        String day = " ";
        day += currentDate.Day.ToString() + ", ";
        DateTime newDate = Convert.ToDateTime(drpCalMonth.SelectedItem.Value + day + drpCalYear.SelectedItem.Value);
        //Calendar1.TodaysDate = newDate;
        Calendar1.SelectedDate = newDate;
        Calendar1.VisibleDate = newDate;
        TextBox1.Text = newDate.ToShortDateString();
    }

    protected void Populate_MonthList()
    {

        drpCalMonth.Items.Add("January");
        drpCalMonth.Items.Add("February");
        drpCalMonth.Items.Add("March");
        drpCalMonth.Items.Add("April");
        drpCalMonth.Items.Add("May");
        drpCalMonth.Items.Add("June");
        drpCalMonth.Items.Add("July");
        drpCalMonth.Items.Add("August");
        drpCalMonth.Items.Add("September");
        drpCalMonth.Items.Add("October");
        drpCalMonth.Items.Add("November");
        drpCalMonth.Items.Add("December");

        drpCalMonth.Items.FindByValue(DateTime.Now.ToString("MMMM")).Selected = true;
    }

    protected void Populate_YearList()
    {

        //Year list can be extended
        int intYear = 2009;
        for (intYear = DateTime.Now.Year - 20; intYear < DateTime.Now.Year + 20; intYear++)
        {
            drpCalYear.Items.Add(intYear.ToString());
        }

        string year = DateTime.Now.Year.ToString();
        ListItem selectyear = drpCalYear.Items.FindByValue(year);
        drpCalYear.Items.FindByValue(year).Selected = true;
    }

    protected void lnkButton1_SelectTodaysDate(object sender, EventArgs e)
    {
        DateTime newDate = DateTime.Today;
        TextBox1.Text = newDate.ToShortDateString();


        String strmonth = newDate.ToString("MMMM");
        ListItem item = drpCalMonth.Items.FindByText(strmonth);
        drpCalMonth.SelectedIndex = drpCalMonth.Items.IndexOf(item);

        drpCalYear.SelectedIndex = drpCalYear.Items.IndexOf(drpCalYear.Items.FindByText(newDate.Year.ToString()));

        Calendar1.TodaysDate = newDate;
        Calendar1.SelectedDate = Calendar1.TodaysDate;
        Calendar1.VisibleDate = Calendar1.TodaysDate;
    }

    protected void lnkButton2_MakeVisible(object sender, EventArgs e)
    {
        if (Panel1.Visible)
        {
            Panel1.Visible = false;
            lnkButton2.Text = "View Cal";
        }
        else
        {
            Panel1.Visible = true;
            lnkButton2.Text = "Hide Cal";
        }

    }


    protected void valTextBox1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = ValidateUtil.IsDate(TextBox1.Text);
    }


}
