<%@ Control Language="C#" AutoEventWireup="true" Inherits="MB.TheBeerHouse.UI.Controls.RatingDisplay" Codebehind="RatingDisplay.ascx.cs" %>
<asp:Image runat="server" ID="imgRating" AlternateText="Average rating" />
<asp:Label runat="server" ID="lblNotRated" Text="(Not rated)" Font-Size="smaller" />