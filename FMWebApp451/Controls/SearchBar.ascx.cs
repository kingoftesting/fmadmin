namespace FaceMasterWebForm
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for SearchBar.
	/// </summary>
	public class ctlSearchBar : System.Web.UI.UserControl
	{
		//protected System.Web.UI.WebControls.TextBox search;
		//protected System.Web.UI.HtmlControls.HtmlImage IMG1;
		//protected System.Web.UI.WebControls.ImageButton ImageButton1;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void ImageSearch_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if(Request["SearchBox"] != null)
			{
				if(Request["SearchBox"].ToString() != "Search...")
				{
					Response.Redirect("SearchResult.aspx?SearchBox=" + Request["SearchBox"].ToString());
				}
			}
		}
	}
}
