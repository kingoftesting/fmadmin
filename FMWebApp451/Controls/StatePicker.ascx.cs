using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class StatePicker : System.Web.UI.UserControl
{
    private void Page_Load(object sender, System.EventArgs e)
    {
    }

    public string State
    {
        get { return dropDownList.SelectedItem.Value; }
        set { dropDownList.Items.FindByValue(value).Selected = true; }
    }

    public bool AutoPostBack
    {
        set { dropDownList.AutoPostBack = value; }
    }

    protected void dropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.OnSelect(e);
    }

    public event EventHandler Select;

    protected virtual void OnSelect(EventArgs e)
    {
        if (Select != null)
            Select(this, e);
    }
}
