namespace FaceMaster
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
    using System.Data.SqlClient;
    using FM2015.Helpers;
    

	/// <summary>
	///		Summary description for TopLinks.
	/// </summary>
	public class TopLinks : System.Web.UI.UserControl
	{
        protected System.Web.UI.WebControls.Literal Literal1;

		private void Page_Load(object sender, System.EventArgs e)
		{
			//if(!IsPostBack)
			//{
				Literal1.Text = "";

                SqlDataReader dr = DBUtil.FillDataReader("SELECT * FROM Pages where SiteID = " + AdminCart.Config.SiteID.ToString() + " and topNavsort is not null order by topnavsort asc");
				while(dr.Read())
				{
					string PageLink = dr["PageLink"].ToString() == "" ? "Page.aspx?PageID=" + dr["PageID"].ToString() : dr["PageLink"].ToString();
					Literal1.Text += "&nbsp;&nbsp;<a class=atoplink href=" + PageLink + ">" + dr["PageTitle"] + "</a>&nbsp;&nbsp;&nbsp;";
				}
				dr.Close();
			//}/
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.Page_Load);

        }
		#endregion
	}
}
