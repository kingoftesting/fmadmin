﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ccForm.ascx.cs" Inherits="FMWebApp451.Controls.ccForm" %>

<asp:Panel ID="pnlFMAdminSubs" runat="server" Visible="true">

    <div style="width: 800px; margin-left: 50px">

        <p><strong>Credit Card Info</strong></p>

        <div class="simpleClear"></div>
        &nbsp;
        <div class="formHTML">
            <asp:Label runat="server" Text="Card Name:"></asp:Label>
            <asp:TextBox ID="txtCardName" runat="server" Width="200px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfield_txtCardName" ControlToValidate="txtCardName" ErrorMessage="* card holder's name is required"
                Display="Dynamic" runat="server"></asp:RequiredFieldValidator>
        </div>

        <div class="simpleClear"></div>
        &nbsp;
        <div class="formHTML">
            <asp:Label runat="server" Text="Card #:"></asp:Label>
            <asp:TextBox ID="txtCardNumber" runat="server" Width="200px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfield_txtCardNumber" ControlToValidate="txtCardNumber" ErrorMessage="* card number is required"
                Display="Dynamic" runat="server"></asp:RequiredFieldValidator>
        </div>

        <div class="simpleClear"></div>
        &nbsp;
        <div class="formHTML">
            <asp:Label runat="server" Text="Expires (MM/YYYY):"></asp:Label>
            <asp:DropDownList ID="ddlExpMonth" Width="100px" runat="server" AutoPostBack="true">
                <asp:ListItem Text="" Selected="True" />
                <asp:ListItem Text="01-JAN" Value="1" Selected="False" />
                <asp:ListItem Text="02-FEB" Value="2" Selected="False" />
                <asp:ListItem Text="03-MAR" Value="3" Selected="False" />
                <asp:ListItem Text="04-APR" Value="4" Selected="False" />
                <asp:ListItem Text="05-MAY" Value="5" Selected="False" />
                <asp:ListItem Text="06-JUN" Value="6" Selected="False" />
                <asp:ListItem Text="07-JUL" Value="7" Selected="False" />
                <asp:ListItem Text="08-AUG" Value="8" Selected="False" />
                <asp:ListItem Text="09-SEP" Value="9" Selected="False" />
                <asp:ListItem Text="10-OCT" Value="10" Selected="False" />
                <asp:ListItem Text="11-NOV" Value="11" Selected="False" />
                <asp:ListItem Text="12-DEC" Value="12" Selected="False" />
            </asp:DropDownList>
            <asp:DropDownList ID="ddlExpYear" Width="100px" runat="server" AutoPostBack="true"></asp:DropDownList>
        </div>

        <div class="simpleClear"></div>
        &nbsp;
        <div class="formHTML">
            <asp:Label runat="server" Text="CSV:"></asp:Label>
            <asp:TextBox ID="txtCSV" runat="server" Width="40px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfield_txtCSV" ControlToValidate="txtCSV" ErrorMessage="* card CSV is required"
                Display="Dynamic" runat="server"></asp:RequiredFieldValidator>
        </div>
        <p>&nbsp;</p>

        <hr />
        <h4>
            <asp:Label ID="CartLabel" runat="server" Text="Enter your information, then click 'Subscribe' only once."></asp:Label></h4>

    </div>
</asp:Panel>
