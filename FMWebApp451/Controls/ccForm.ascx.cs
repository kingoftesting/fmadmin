﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FMWebApp451.Controls
{
    public partial class ccForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //set expiration date range
            ddlExpMonth.SelectedValue = DateTime.Now.Month.ToString();

            SortedList yearList = new SortedList();
            int year = DateTime.Now.Year;
            for (int i = 0; i < 15; i++)
            {
                yearList.Add(year.ToString(), year.ToString());
                year++;
            }
            ddlExpYear.DataSource = yearList;
            ddlExpYear.DataTextField = "Key";
            ddlExpYear.DataValueField = "Value";
            ddlExpYear.DataBind();
            ddlExpYear.SelectedValue = DateTime.Now.Year.ToString();
        }
    }
}