<%@ Page Language="C#" Trace="false" masterPageFile="~/MasterPage3_Cart.master" Theme="Platinum" maintainScrollPositionOnPostBack="true" AutoEventWireup="true" Inherits="Default" Title="Suzanne Somers FaceMaster" Codebehind="Default.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="EmailSubscribe" Src="controls/EmailSubscribe.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:HiddenField ID="hdnEmailSignup" runat="server" />
    <script type="text/javascript">

        $(document).ready(function () {
            var emailSignup = document.getElementById('<%= hdnEmailSignup.ClientID%>').value;
            if ( emailSignup == "1") {
                $('#cboxOverlay, #colorbox').appendTo('form');
                $.colorbox({
                    width: "40%",
                    inline: true,
                    href: "#EmailSignUpPanel",
                    opacity: 0.4,
                    trapFocus: true,
                    onComplete: function () { },
                    onClosed: function () {
                        $('div.lightbox-content').hide();
                    }
                });
            }
        });

        function closeOverlay() {
            $.colorbox.close();
           return false;
        }
    </script>

<table id="Table_01" border="0" cellpadding="0" cellspacing="0">
<!--
	<tr>
		<td style="padding-left:6px">
            <asp:ImageMap ID="imgmapSpecial" ImageUrl="images/SliceStuff/FreeShip-Holiday-24NOV11.jpg" Width="680px" HotSpotMode="Navigate"  runat="server" AlternateText="">
            <asp:RectangleHotSpot Top="0" Bottom="150" Left="0" Right="700" AlternateText="Buy Now! FaceMaster Platinum, Suzanne Somers #1 skin care beauty secret for an ageless, more youthful look" NavigateURL="ViewCart.aspx?action=Add&ProductID=14" />
            </asp:ImageMap>
		</td>
	</tr>
-->
	<tr>
		<td>
            <asp:ImageMap ID="ImageMap1" ImageUrl="images/SliceStuff/FMgrid(8)_body_03_3.jpg" HotSpotMode="Navigate"  runat="server" AlternateText="">
            <asp:RectangleHotSpot Top="0" Bottom="315" Left="0" Right="700" AlternateText="Buy Now! FaceMaster Platinum, Suzanne Somers #1 skin care beauty secret for an ageless, more youthful look" NavigateURL="ViewCart.aspx?action=Add&ProductID=14" />
            </asp:ImageMap>
		</td>
	</tr>
	<tr>
		<td align="left">
            <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="650" height="72" id="MiddleSloganBar" align="middle">
            <param name="allowScriptAccess" value="sameDomain" />
            <param name="movie" value="Flash/MiddleSloganBar.swf" /><param name="quality" value="high" /><param name="bgcolor" value="#ffffff" /><embed src="Flash/MiddleSloganBar.swf" quality="high" bgcolor="#ffffff" width="650" height="72" name="MiddleSloganBar" align="middle" allowscriptaccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
            </object>
        </td>
	</tr>
	<tr>
		<td>
            <asp:ImageMap ID="ImageMap2" ImageUrl="images/SliceStuff/FMgrid(8)_body_06_3.jpg" Width="700px" HotSpotMode="Navigate"  runat="server" AlternateText="FaceMaster Platinum, Suzanne Somers #1 skin care beauty secret for an ageless, more youthful look">
            <asp:RectangleHotSpot Top="0" Bottom="360" Left="0" Right="295" AlternateText="Buy Now! FaceMaster Platinum, Suzanne Somers #1 skin care beauty secret for an ageless, more youthful look" NavigateURL="ViewCart.aspx?action=Add&ProductID=14" />
            <asp:RectangleHotSpot Top="0" Bottom="250" Left="420" Right="700" AlternateText="Video - Suzanne Somer's FaceMaster Platinum Treatment Routine; Suzanne Somers #1 skin care face beauty tip for an ageless, more youthful look" NavigateURL="videoTips.aspx?id=1" />
            <asp:RectangleHotSpot Top="275" Bottom="290" Left="420" Right="470" AlternateText="Video - Introduction to FaceMaster Platinum, Suzanne Somers #1 skin care beauty secret for an ageless, more youthful look" NavigateURL="videoTips.aspx?id=1" />
            <asp:RectangleHotSpot Top="292" Bottom="304" Left="420" Right="460" AlternateText="Video - The Eyes Treatment routine for FaceMaster Platinum, Suzanne Somers #1 skin care face beauty tip for an ageless, more youthful look" NavigateURL="videoTips.aspx?id=2" />
            <asp:RectangleHotSpot Top="305" Bottom="324" Left="461" Right="515" AlternateText="Video - The Cheeks Treatement routine for FaceMaster Platinum, Suzanne Somers #1 skin care beauty secret for an ageless, more youthful look" NavigateURL="videoTips.aspx?id=3" />
            <asp:RectangleHotSpot Top="305" Bottom="324" Left="520" Right="550" AlternateText="Video - The Jaw Treatment routine for FaceMaster Platinum, Suzanne Somers #1 skin care face beauty tip for an ageless, more youthful look" NavigateURL="videoTips.aspx?id=4" />
            <asp:RectangleHotSpot Top="305" Bottom="324" Left="560" Right="605" AlternateText="Video - The Mouth Treatment routine for the FaceMaster Platinum, Suzanne Somers #1 skin care beauty secret for an ageless, more youthful look" NavigateURL="videoTips.aspx?id=5" />
            <asp:RectangleHotSpot Top="305" Bottom="324" Left="615" Right="685" AlternateText="Video - The Forehead Treatment routine for the FaceMaster Platinum, Suzanne Somers #1 skin care face beauty tip for an ageless, more youthful look" NavigateURL="videoTips.aspx?id=6" />
            <asp:RectangleHotSpot Top="325" Bottom="336" Left="420" Right="510" AlternateText="Video - The Feathering Treatment routine for the FaceMaster Platinum, Suzanne Somers #1 skin care beauty secret for an ageless, more youthful look" NavigateURL="videoTips.aspx?id=7" />
            <asp:RectangleHotSpot Top="341" Bottom="355" Left="420" Right="605" AlternateText="Video - Completing Your Routine for the FaceMaster Platinum, Suzanne Somers #1 skin care face beauty tip for an ageless, more youthful look" NavigateURL="videoTips.aspx?id=8" />
            </asp:ImageMap>
        </td>
	</tr>
</table>

    <asp:Panel ID="pnlEmailSignUp" runat="server" Style="visibility: hidden; height: 1px">
        <div id="EmailSignUpPanel" class="overlaypanel">
            <uc1:EmailSubscribe ID="emSub1" runat="server"></uc1:EmailSubscribe>
        </div>
    </asp:Panel>


</asp:Content>
