using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using Certnet;

public partial class Default : System.Web.UI.Page
{

    Cart cart = new Cart();
       
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/default.aspx");
        bool emailSignUp = (bool)Session["emailSignup"];
        if (emailSignUp) { hdnEmailSignup.Value = "1"; }
        else { hdnEmailSignup.Value = "0"; }
        Session["emailSignup"] = false; //display the email signup box just once per session Initialized in Global.aspx)

        //Create META Description
        HtmlMeta metaDESC = new HtmlMeta();
        metaDESC.Name = "DESCRIPTION";
        metaDESC.Content = "Suzanne Somers - Facemaster Facial Toning";

        //Create META Page-Topic
        HtmlMeta metaPageTopic = new HtmlMeta();
        metaPageTopic.Name = "PAGE-TOPIC";
        metaPageTopic.Content = "Suzanne Somers FaceMaster";

        //Create META Keywords
        HtmlMeta metaKeywords = new HtmlMeta();
        metaKeywords.Name = "KEYWORDS";
        metaKeywords.Content = "Suzanne somers, facemaster, facemaster, beauty secrets, finger wands, fingerwands, FaceMaster Treatment";

        //Add META controls to HtmlHead
        HtmlHead head = (HtmlHead)Page.Header;
        head.Controls.Add(metaDESC);
        head.Controls.Add(metaPageTopic);
        head.Controls.Add(metaKeywords);

        cart.Load(Session["cart"]);

        //if (cart.SiteCustomer.CustomerID == 0) { Response.Redirect("login.aspx"); }

        if (Request.UrlReferrer != null)
        {
            cart.SiteCustomer.ReferrerDomain = Request.UrlReferrer.OriginalString;
            cart.SiteCustomer.InBoundQuery = Request.UrlReferrer.OriginalString;
        }
        Session["cart"] = cart;

        if (Request["cid"] != null)
        {
            //Response.Redirect("~/Face-Makeover/");
        }

    }


}
