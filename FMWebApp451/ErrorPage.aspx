<%@ Page Language="C#" masterPageFile="~/Admin/MasterPage3_Blank.master" Theme="Platinum" AutoEventWireup="true" Inherits="ErrorPage" Codebehind="ErrorPage.aspx.cs" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">

<!--
    http://benfoster.io/blog/aspnet-mvc-custom-error-pages
    http://stackoverflow.com/questions/13905164/how-to-make-custom-error-pages-work-in-asp-net-mvc-4
    http://tech.trailmax.info/2013/08/error-handling-in-mvc-and-nice-error-pages/
-->

    <div class="sectiontitle">Oops! We're sorry, but we couldn't successfully complete your request.</div>
    <div style="margin: 10px; text-align:left">        
        <h4>Our IT department will receive an email about this error... heads may or may not roll depending on the 
            exact nature of the problem!</h4>
        <hr />

        <p>Don't worry about this stuff... most likely one of our IT specialists will enjoy reading it!</p>

        <p>
            <asp:Label runat="server" ID="lbl400" Visible="false" Text="400-Bad Request: The request had bad syntax or was impossible to be satisified." />
            <asp:Label runat="server" ID="lbl403" Visible="false" Text="403-Forbidden: The request does not specify the file name. Or the directory or the file does not have the permission that allows the pages to be viewed from the web." />
            <asp:Label runat="server" ID="lbl404" Visible="false" Text="404-Not Found: The requested page or resource could not be found." />
            <asp:Label runat="server" ID="lbl408" Visible="false" Text="408-Request Time-Out: The request timed out. This may be caused by a too high traffic. Please try again later." />
            <asp:Label runat="server" ID="lbl500" Visible="false" Text="500-Server Error: " />
            <asp:Label runat="server" ID="lbl505" Visible="false" Text="505-Our web server encountered an unexpected condition which prevented it from fulfilling the request. Please try again later." />
            <asp:Label runat="server" ID="lblError" Visible="false" Text="There was some problems processing your request. An e-mail with details about this error has already been sent to the administrator." />
        </p>
        <p>
            <asp:Literal runat="server" ID="litAdminError" Visible="false" Text="" />
        </p>
    </div>


</asp:Content>

