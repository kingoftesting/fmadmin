using System;
using System.Web;
using System.Web.UI;

public partial class ErrorPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        /*
        lbl400.Visible = (this.Request.QueryString["code"] != null && this.Request.QueryString["code"] == "400");
        lbl403.Visible = (this.Request.QueryString["code"] != null && this.Request.QueryString["code"] == "403");
        lbl404.Visible = (this.Request.QueryString["code"] != null && this.Request.QueryString["code"] == "404");
        lbl408.Visible = (this.Request.QueryString["code"] != null && this.Request.QueryString["code"] == "408");
        lbl500.Visible = (this.Request.QueryString["code"] != null && this.Request.QueryString["code"] == "500");
        lbl505.Visible = (this.Request.QueryString["code"] != null && this.Request.QueryString["code"] == "505");
        */

        lblError.Visible = (string.IsNullOrEmpty(this.Request.QueryString["code"]));

        if (this.Request.QueryString["code"] != null)
        {
            string rqstQuery = this.Request.QueryString["code"];
            switch (rqstQuery)
            {
                case "400":
                    lbl400.Visible = true;
                    Response.StatusCode = 400;
                    break;

                case "403":
                    lbl403.Visible = true;
                    Response.StatusCode = 403;
                    break;

                case "404":
                    lbl404.Visible = true;
                    Response.StatusCode = 404;
                    break;

                case "408":
                    lbl408.Visible = true;
                    Response.StatusCode = 408;
                    break;

                case "500":
                    lbl400.Visible = true;
                    Response.StatusCode = 500;
                    break;

                case "505":
                    lbl400.Visible = true;
                    Response.StatusCode = 505;
                    break;

                default:
                    break;
            }
        }

        HttpContext ctxObject = HttpContext.Current;
        Exception exception = ctxObject.Server.GetLastError();
        string errorInfo = "";

        if (AdminCart.Config.RedirectMode == "Test") //let us know if in Test or Live mode
        { errorInfo = "Test Mode: Redirected to ErrorPage - "; }
        else { errorInfo = "Live Mode: Redirected to ErrorPage - "; }

        string logDateTime = DateTime.Now.ToString("g");
        string strReqURL = (ctxObject.Request.Url != null) ? ctxObject.Request.Url.ToString() : String.Empty;
        string strReqQS = (ctxObject.Request.QueryString != null) ? ctxObject.Request.QueryString.ToString() : String.Empty;
        string strServerName = String.Empty;
        if (ctxObject.Request.ServerVariables["HTTP_REFERER"] != null)
        {
            strServerName = ctxObject.Request.ServerVariables["HTTP_REFERER"].ToString();
        }
        string strUserAgent = (ctxObject.Request.UserAgent != null) ? ctxObject.Request.UserAgent : String.Empty;
        string strUserIP = (ctxObject.Request.UserHostAddress != null) ? ctxObject.Request.UserHostAddress : String.Empty;
        string strUserAuthen = (FM2015.Models.User.IsAuthenticated()) ? "YES" : String.Empty;
        string strUserName = (FM2015.Models.User.GetUserID() > 0) ? FM2015.Models.User.GetUserName() : String.Empty;

        string strMessage = string.Empty, strSource = string.Empty, strTargetSite = string.Empty, strStackTrace = string.Empty;

        errorInfo += "Log date/time=" + logDateTime + "<br />";
        errorInfo += "Offending URL=" + strReqURL + "<br />";
        errorInfo += "Offending Quesry String=" + strReqQS + "<br />";
        errorInfo += "Source=" + exception.Source + "<br />";
        errorInfo += "Message=" + exception.Message + "<br />" + "------------------" + "<br />";
        errorInfo += "Error generated at=" + strServerName + "<br />";
        errorInfo += "User Agent=" + strUserAgent + "<br />";
        errorInfo += "IP addr: " + strUserIP + "<br />" + "------------------" + "<br />";
        errorInfo += "User Authenticated=" + strUserAuthen + "<br />";
        errorInfo += "User Name=" + strUserName + "<br />";
        errorInfo += "StkTrace=" + exception.StackTrace + "<br />";
        strMessage = "Caller Message: " + errorInfo + "<br />";
        while (exception != null)
        {
            strMessage += "System Message: " + exception.Message;
            strSource = exception.Source;
            strTargetSite = exception.TargetSite.ToString();
            strStackTrace = exception.StackTrace;
            exception = exception.InnerException;
        }

        litAdminError.Text = strMessage;
        litAdminError.Visible = true;
        //if (FM2015.Models.User.HasRole("ADMIN")) { litAdminError.Visible = true; }
    }

}
