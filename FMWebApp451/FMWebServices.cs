﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Web.Services;
using AdminCart;
using FM2015.Helpers;

/// <summary>
/// Summary description for FMWebServices
/// </summary>
//[WebService(Namespace = "http://localhost:56406/FMWebServices.asmx")]
[WebService(Namespace = "http://www.facemaster.com/FMWebServices.asmx")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class FMWebServices : System.Web.Services.WebService {

    public FMWebServices () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public DataTable GetSymptomCollection()
    {
        return Utilities.FMHelpers.GetSymptomCollection();
    }


    [WebMethod]
    public String GetVersion()
    {
        return Config.WebServiceVersion;
    }

    [WebMethod]
    public DataSet GetLabelsOverFM(int startOrderID)
    {

        string sql = @"

SELECT OrderNumber, BoxID = 1, SourceSystemID = 2, OrderDate, FirstName, LastName, Company = '', 
		CustomerAddressStreet1, CustomerAddressStreet2, CustomerAddressStreet3, CustomerAddressCity, 
		CustomerAddressState, CustomerAddressZip, CustomerAddressCountry, 
		ShipMode = 'Ground', EmailFlag = 1, EmailAddress, CustomerPhone, SkuCount = 1, LineItemCount = 1 
FROM 
	( 
		SELECT OrderNumber = o.orderid, OrderDate = o.orderdate, FirstName = c.FirstName, LastName= c.LastName, 
			CustomerAddressStreet1 = a.street1, CustomerAddressStreet2 = a.street2, CustomerAddressStreet3 = '', 
			CustomerAddressCity = a.city, CustomerAddressState = a.[state], CustomerAddressZip = a.zip, 
			CustomerAddressCountry = a.CountryCode, 
			EmailAddress = c.Email, CustomerPhone = c.phone 
		FROM orders o, customers c, addresses a, transactions t  
		WHERE o.orderid = t.orderid AND (t.trxtype = 'S' AND t.ResultCode = 0) AND 
			o.customerid = c.customerid AND o.ShippingAddressID = a.AddressID 

		UNION ALL 

		SELECT OrderNumber = o.orderid, OrderDate = o.orderdate, FirstName = c.FirstName, LastName= c.LastName, 
			CustomerAddressStreet1 = a.street1, CustomerAddressStreet2 = a.street2, CustomerAddressStreet3 = '', 
			CustomerAddressCity = a.city, CustomerAddressState = a.[state], CustomerAddressZip = a.zip, 
			CustomerAddressCountry = a.CountryCode, 
			EmailAddress = c.Email, CustomerPhone = c.phone 
		FROM orders o, customers c, addresses a   
		WHERE o.sourceID = -2 AND 
			o.customerid = c.customerid AND o.ShippingAddressID = a.AddressID 
	) AS tmp 
WHERE OrderNumber BETWEEN @startID AND 400000  
ORDER BY OrderNumber DESC  
";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startOrderID);
        DataSet ds = DBUtil.FillDataSet(sql, "table1", mySqlParameters);

        return ds;
    }

    [WebMethod]
    public DataSet GetCustomsFormFM(int startOrderID)
    {

        string sql = @"

SELECT OrderNumber, SourceSystemID = 2, Description, Weight, Value, Quantity, CountryOfOrigin  
FROM 
	(	select OrderNumber = o.orderid, Description = p.Name, Weight = p.Weight, Value = od.Price, Quantity = od.Quantity, CountryOfOrigin = p.CountryOfOrigin 
		from orders o, orderdetails od, products p, transactions t 
		where o.orderid = t.orderid AND (t.trxtype = 'S' AND t.ResultCode = 0) AND 
			o.orderid = od.orderid AND od.ProductID = p.ProductID AND p.IsShippable = 1 

		UNION ALL

		select OrderNumber = o.orderid, Description = p.Name, Weight = p.Weight, Value = od.Price, Quantity = od.Quantity, CountryOfOrigin = p.CountryOfOrigin 
		from orders o, orderdetails od, products p 
		where o.sourceid = -2 AND 
			o.orderid = od.orderid AND od.ProductID = p.ProductID AND p.IsShippable = 1  
	) AS tmp
WHERE OrderNumber BETWEEN @startID AND 400000
ORDER BY OrderNumber DESC, CountryOfOrigin ASC 
";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startOrderID);
        DataSet ds = DBUtil.FillDataSet(sql, "table1", mySqlParameters);

        return ds;
    }

    [WebMethod]
    public DataSet GetLabelsOverFMInfoM(int startOrderID)
    {

        string sql = @"

SELECT OrderNumber, BoxID = 1, SourceSystemID = 4, OrderDate, FirstName, LastName, Company = '', 
		CustomerAddressStreet1, CustomerAddressStreet2, CustomerAddressStreet3, CustomerAddressCity, 
		CustomerAddressState, CustomerAddressZip, CustomerAddressCountry, 
		ShipMode = 'Ground', EmailFlag = 1, EmailAddress, CustomerPhone, SkuCount = 1, LineItemCount = 1 
FROM 
	( 
		SELECT OrderNumber = o.vendororderid, OrderDate = o.orderdate, FirstName = c.CustomerFirstName, LastName= c.CustomerLastName, 
			CustomerAddressStreet1 = c.CustomerAddress1, CustomerAddressStreet2 = c.CustomerAddress2, CustomerAddressStreet3 = '', 
			CustomerAddressCity = c.CustomerCity, CustomerAddressState = c.CustomerState, CustomerAddressZip = c.CustomerZip, 
			CustomerAddressCountry = c.CustomerCountry, 
			EmailAddress = c.CustomerEmail, CustomerPhone = '' 
		FROM orders o, customers c    
		WHERE o.customerid = c.customerid  
	) AS tmp 
WHERE OrderNumber BETWEEN @startID AND 700000  
ORDER BY OrderNumber DESC  
";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startOrderID);
        DataSet ds = DBUtil.FillDataSet(sql, "table1", mySqlParameters, Config.ConnStrHDI());

        return ds;
    }

    [WebMethod]
    public DataSet GetCustomsFormFMInfoM(int startOrderID)
    {

        string sql = @"

SELECT OrderNumber, SourceSystemID = 4, Description, Weight, Value, Quantity, CountryOfOrigin  
FROM 
    (
		select OrderNumber = o.vendororderid, Description = p.ProductName, Weight = p.Weight, Value = 1, Quantity = od.Quantity, CountryOfOrigin = p.CountryOfOrigin 
		from orders o, orderdetails od, products p 
		where o.orderid = od.orderid AND od.ProductID = p.ProductID   
	) AS tmp
WHERE OrderNumber BETWEEN @startID AND 700000
ORDER BY OrderNumber DESC, CountryOfOrigin ASC 
";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startOrderID);
        DataSet ds = DBUtil.FillDataSet(sql, "table1", mySqlParameters, Config.ConnStrHDI());

        return ds;
    }


}
