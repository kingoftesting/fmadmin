﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using System.Web.Http;
using FM2015;
using FM2015.Helpers;
using System.Web.Optimization;
using Unity;
using Stripe;
using FMWebApp401;

namespace FMWebApp451
{
    public class Global : HttpApplication
    {
        //http://stackoverflow.com/questions/9594229/accessing-session-using-asp-net-web-api
        protected void Application_PostAuthorizeRequest()
        {
            if (IsWebApiRequest())
            {
                HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
            }
        }

        private bool IsWebApiRequest()
        {
            return HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith(WebApiConfig.UrlPrefixRelative);
        }

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();

            var container = new UnityContainer();
            UnityConfig.RegisterTypes(container); //NuGet unity.mvc5

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //IUnityContainer myContainer = new UnityContainer();
            //ControllerBuilder.Current.SetControllerFactory(typeof(UnityControllerFactory));
            //myContainer.RegisterType<IOrdersWithDetails, OrdersWithDetails>(new ContainerControlledLifetimeManager());
            //Setup DI
            //Bootstrapper.Initialise();

            CacheHelper.ClearAll(); //make sure the cache server is clear

            //allow caches to be loaded on 1st reference
            //List<AdminCart.Product> productList = AdminCart.Product.GetProducts(); //load product cache
            //ShopOrderNoteProvider shopOrderNoteProvider = new ShopOrderNoteProvider();
            //List<ShopOrderNote> shopNoteList = shopOrderNoteProvider.GetAll(); //load shopify customer order note cache

            string stripeKey = AdminCart.Config.ApiSecretKeyStripe();
            StripeConfiguration.SetApiKey(stripeKey);

        }

        void Application_PreSendRequestHeaders(Object sender, EventArgs e)
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string requestedPage = oInfo.Name;
            requestedPage = requestedPage.ToUpper();
            if (requestedPage == "PROCESSING.ASPX") { Response.AppendHeader("Refresh", "2; URL=Processing.aspx?Proc=charge"); }
            else { Response.AppendHeader("Refresh", "900; URL='/'"); }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

            //You don't want to redirect on posts, or images/css/js
            bool isGet = HttpContext.Current.Request.RequestType.ToLowerInvariant().Contains("get");
            if (isGet && HttpContext.Current.Request.Url.AbsolutePath.Contains(".") == false)
            {
                //string lowercaseURL = (Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.Url.AbsolutePath);
                //if (Regex.IsMatch(lowercaseURL, @"[A-Z]"))
                //{
                //    //You don't want to change casing on query strings
                //    lowercaseURL = lowercaseURL.ToLower() + HttpContext.Current.Request.Url.Query;

                //    Response.Clear();
                //    Response.Status = "301 Moved Permanently";
                //    Response.AddHeader("Location", lowercaseURL);
                //    Response.End();
                //}
            }
            else
            {
                if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                {
                    HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
                    HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
                    HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");
                    HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
                    HttpContext.Current.Response.End();
                }
            }
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
            Session.Clear();
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

            // At this point we have information about the error
            HttpContext ctx = HttpContext.Current;

            Exception exception = ctx.Server.GetLastError();

            dbErrorLogging.LogError("Global.aspx", exception);

            // Send a mail message alerting us to a user problem with the site
            string errorInfo = "";

            if (AdminCart.Config.RedirectMode == "Test") //let us know if in Test or Live mode
            { errorInfo = "Test Mode: Redirected to ErrorPage - "; }
            else { errorInfo = "Live Mode: Redirected to ErrorPage - "; }

            errorInfo += "Offending URL=" + ctx.Request.Url.ToString() + Environment.NewLine;
            errorInfo += "Source=" + exception.Source + Environment.NewLine;
            errorInfo += "Message=" + exception.Message + Environment.NewLine + "------------------" + Environment.NewLine;
            errorInfo += "Error generated at: " + Request.ServerVariables["SERVER_NAME"].ToString() + Environment.NewLine;
            errorInfo += "IP addr: " + Request.ServerVariables["REMOTE_ADDR"].ToString() + Environment.NewLine + "------------------" + Environment.NewLine;
            errorInfo += "StkTrace=" + exception.StackTrace + Environment.NewLine;


            //Mail.SendMail("site@facemaster.com", "rmohme@sbcglobal.net", "User Redirected to ErrorPage", errorInfo);

            //Response.Redirect("ErrorPage.aspx");

        }

        void Session_Start(object sender, EventArgs e)
        {
            InitConfig.RegisterInit();
            FMWebApp451.Helpers.HttpContextManager.SetCurrentContext(new HttpContextWrapper(HttpContext.Current));

            // Code that runs when a new session is started
            if (Request.ServerVariables["QUERY_STRING"] != null) { Session["QueryString"] = Request.ServerVariables["QUERY_STRING"].ToString(); }
            if (Request["sset"] != null) { Session["sset"] = Request["sset"].ToString(); }
            if (Request["utm_campaign"] != null) { Session["utm_campaign"] = Request["utm_campaign"].ToString(); }
            if (Request["utm_medium"] != null) { Session["utm_medium"] = Request["utm_medium"].ToString(); }
            if (Request["utm_source"] != null) { Session["utm_source"] = Request["utm_source"].ToString(); }
            if (Request["utm_content"] != null) { Session["utm_content"] = Request["utm_content"].ToString(); }
            Session["emailSignup"] = true;

            // this is here for sexyforever test purposes; comment out for live use
            /*
            Session["QueryString"] = "utm_campaign=sexyforever&tm_medium=affiliate&utm_source=rodger&utm_content=test";
            Session["utm_campaign"] = "sexyforever";
            Session["utm_medium"] = "affiliate";
            Session["utm_source"] = "rodger";
            Session["utm_content"] = "test";
            */

            // this is here for SiteSettings test purposes; comment out for live use
            /*
            Session["QueryString"] = "sset=4";
            Session["sset"] = "4";
            */

            Session["cart"] = null;
            Session["CScart"] = null;
            Session["CustomerID"] = 0;
            Session["cache_ProductItems"] = null;
            Session["promo"] = null;
            Session["UnshippedReportList"] = null;
            Session["UnshippedSerReport"] = null;
            Session["CERList"] = null;
            Session["ddlSelectedPromoCode"] = string.Empty;

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.
        }
    }
}