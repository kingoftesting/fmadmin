﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminCart;

namespace FM2015.Helpers
{
    public static class CacheHelper
    {
        //http://johnnycoder.com/blog/2008/12/10/c-cache-helper-class/

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="o">Item to be cached</param>
        /// <param name="key">Name of item</param>
        /// <param name="duration">expiration after insertion, in minutes</param>

        public static void Add<T>(T o, string key, int duration)
        {
            if (Config.globalCacheEnable)
            {
                HttpContext.Current.Cache.Insert(
                key,
                o,
                null,
                System.Web.Caching.Cache.NoAbsoluteExpiration,
                new TimeSpan(0, duration, 0), //expires duration minutes after insert
                System.Web.Caching.CacheItemPriority.Default, //let the system determine when to flush to make room
                null //no callback routine
                );
            }
        }

        public static void Add<T>(T o, string key)
        {
            int duration = Config.globalCacheDuration;
            if (Config.globalCacheEnable)
            {
                HttpContext.Current.Cache.Insert(
                    key,
                     o,
                    null,
                    System.Web.Caching.Cache.NoAbsoluteExpiration,
                    new TimeSpan(0, duration, 0), //expires duration minutes after insert
                   System.Web.Caching.CacheItemPriority.Default, //let the system determine when to flush to make room
                    null //no callback routine
                );
            }
        }

        /// <summary>
        /// Remove item from cache 
        /// </summary>
        /// <param name="key">Name of cached item</param>
        public static void Clear(string key)
        {
            HttpContext.Current.Cache.Remove(key);
        }

        public static void ClearAll()
        {
            foreach (System.Collections.DictionaryEntry entry in HttpContext.Current.Cache)
            {
                HttpContext.Current.Cache.Remove((string)entry.Key);
            }
        }

        /// <summary>
        /// Check for item in cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            return HttpContext.Current.Cache[key] != null;
        }

        /// <summary>
        /// Retrieve cached item
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Name of cached item</param>
        /// <returns>Cached item as type</returns>
        public static bool Get<T>(string key, out T value)
        {
            try
            {
                if (!Exists(key))
                {
                    value = default(T);
                    return false;
                }

                value = (T)HttpContext.Current.Cache[key];
            }
            catch
            {
                value = default(T);
                return false;
            }

            return true;
        }
    }
}