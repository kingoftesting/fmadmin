using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace FM2015.Helpers
{
    /// <summary>
    /// DBUtil Database Interface Class
    /// 
    /// .Net 2.0 or greater
    /// 
    /// Provides procedural access to SQL Server
    /// </summary>

    public class DBUtil
    {
        //static private string FMConnString = ConfigurationManager.ConnectionStrings["FMSqlServer"].ConnectionString;
        static private string FMConnString = AdminCart.Config.ConnStr();

        public DBUtil()
        {
        }

        //static private bool RedirectOnError = true;

        /// <summary>
        /// Default exception handler for all methods.  Sends and email to an admin.
        /// </summary>
        /// <param name="ex">The exception object</param>
        /// <param name="query">The calling sql query</param>
        /// <param name="notes">Any context oriented information</param>
        static public void ExceptionHandler(Exception ex, string query, string notes)
        {
            //This routine can vary greatly depending upon which environment it is called from.
            //By centralizing this routine, you can write your exception handling code in
            //one spot.

            //1. Log to a file or database
            string msg = "Error in DBUtil" + Environment.NewLine;
            msg += "sql = " + query + Environment.NewLine;
            msg += notes;
            dbErrorLogging.LogError(msg, ex);

            //2. Send a mail message
            //Mail.SendMail("site@facemaster.com", "dhenson@certifiednetworks.com", "site dbutil error - AppCode-DBUtil FillDataSet()", ex.Message + "---------" + query);

            //3. Set ASP.Net Session info for error page
            //System.Web.HttpContext.Current.Session["SqlError"] = ex.Message;
            //System.Web.HttpContext.Current.Session["SqlErrorString"] = sql;
            //System.Web.HttpContext.Current.Session["StackTrace"] = ex.StackTrace.ToString();
            //System.Web.HttpContext.Current.Response.Redirect("ErrorPage.aspx");

            //Application specific routines here
            //    if (!sql.Contains("AddressType = 2"))
            //   {
            //       Mail.SendMail("site@facemaster.com", "dhenson@certifiednetworks.com", "site dbutil error -- AppCode- GetScalar()", ex.Message + "---------" + sql);
            //   }
        }


        /// <summary>
        /// Returns a DataSet with one DataTable.  Uses connection string from Config class.
        /// </summary>
        /// <param name="sql">The SQL query used to fill the dataset</param>
        /// <param name="tablename">The resulting DataTable</param>
        /// <returns></returns>
        static public DataSet FillDataSet(string sql, string tablename)
        {

            SqlConnection conn = new SqlConnection(FMConnString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                ad.Fill(ds, tablename);
            }
            catch (SqlException ex)
            {
                ExceptionHandler(ex, sql, "");
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }


        /// <summary>
        /// Returns a DataSet with one DataTable.  Uses connection string parameter for flexibility.
        /// </summary>
        /// <param name="sql">Query used to populate the DataTable</param>
        /// <param name="tablename">Name of the DataTable in the resulting DataSet</param>
        /// <param name="ConnectionString">SQL login information.</param>
        /// <returns></returns>
        static public DataSet FillDataSet(string sql, string tablename, string ConnectionString)
        {

            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                ad.Fill(ds, tablename);
            }
            catch (SqlException ex)
            {
                ExceptionHandler(ex, sql, "");
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }



        /// <summary>
        /// Returns a DataSet with one DataTable.  Uses connection string from Config class.
        /// </summary>
        /// <param name="sql">The SQL query used to fill the dataset</param>
        /// <param name="tablename">The resulting DataTable</param>
        /// <sql params passed in by caller
        /// <returns></returns>
        static public DataSet FillDataSet(string sql, string tablename, SqlParameter[] parms)
        {

            SqlConnection conn = new SqlConnection(FMConnString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 120;

            foreach (SqlParameter p in parms)
            {
                cmd.Parameters.Add(p);
            }

            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                ad.Fill(ds, tablename);
            }
            catch (SqlException ex)
            {
                ExceptionHandler(ex, sql, "");
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }

        /// <summary>
        /// Returns a DataSet with one DataTable.  Uses connection string from Config class.
        /// </summary>
        /// <param name="sql">The SQL query used to fill the dataset</param>
        /// <param name="tablename">The resulting DataTable</param>
        /// <sql params passed in by caller
        /// <returns></returns>
        static public DataSet FillDataSet(string sql, string tablename, SqlParameter[] parms, string ConnectionString)
        {

            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 120;

            foreach (SqlParameter p in parms)
            {
                cmd.Parameters.Add(p);
            }

            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                ad.Fill(ds, tablename);
            }
            catch (SqlException ex)
            {
                ExceptionHandler(ex, sql, "");
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }

        /// <summary>
        /// Returns an SqlDataReader object when pass an SQL query.
        /// Uses connectionstring from Config class
        /// </summary>
        /// <param name="sql">SQL query</param>
        /// <returns>SqlDataReader(Be sure to check for null in calling code)</returns>
        static public SqlDataReader FillDataReader(string sql)
        {
            SqlConnection conn = new SqlConnection(FMConnString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 120;

            SqlDataReader dr = null;

            try
            {
                conn.Open();

                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (SqlException ex)
            {
                if (dr != null) { dr.Close(); }
                conn.Close();

                ExceptionHandler(ex, sql, "");

                throw ex;
            }
            finally
            {
            }

            //Be sure to test for null in calling code
            return dr;
        }


        /// <summary>
        /// Returns an SqlDataReader object when pass an SQL query.
        /// Uses connectionstring parameter for flexibility.
        /// 
        /// </summary>
        /// <param name="sql">SQL Query</param>
        /// <param name="ConnectionString">SQL Login information</param>
        /// <returns>SqlDataReader (Be sure to check for null in calling code)</returns>
        static public SqlDataReader FillDataReader(string sql, string ConnectionString)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 120;

            SqlDataReader dr = null;

            try
            {
                conn.Open();

                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (SqlException ex)
            {
                if (dr != null) { dr.Close(); }
                conn.Close();

                ExceptionHandler(ex, sql, "");

                throw ex;
            }
            finally
            {
            }

            //Be sure to test for null in calling code
            return dr;
        }


        // Returns an SqlDataReader object when passed an SQL query.
        /// <summary>
        /// Returns an SqlDataReader object when passed an SQL query.
        /// Uses connectionstring parameter for flexibility.
        /// 
        /// </summary>
        /// <param name="sql">SQL Query</param>
        /// <param name="ConnectionString">SQL Login information</param>
        /// <returns>SqlDataReader (Be sure to check for null in calling code)</returns>
        static public SqlDataReader FillDataReader(string sql, SqlParameter[] parms)
        {
            SqlConnection conn = new SqlConnection(FMConnString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 120;

            foreach (SqlParameter p in parms)
            {
                cmd.Parameters.Add(p);
            }

            SqlDataReader dr = null;

            try
            {
                conn.Open();

                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                if (dr != null) { dr.Close(); }
                conn.Close();

                string notes = "FillDataReader(string sql, SqlParameter[] parms)";
                foreach (SqlParameter p in parms)
                {
                    notes += p.ParameterName + "=" + p.Value;
                }

                ExceptionHandler(ex, sql, notes);

                throw ex;
            }
            finally
            {
            }

            //Be sure to test for null in calling code
            return dr;
        }

        // Returns an SqlDataReader object when passed an SQL query.
        /// <summary>
        /// Returns an SqlDataReader object when passed an SQL query.
        /// Uses connectionstring parameter for flexibility.
        /// 
        /// </summary>
        /// <param name="sql">SQL Query</param>
        /// <param name="ConnectionString">SQL Login information</param>
        /// <returns>SqlDataReader (Be sure to check for null in calling code)</returns>
        static public SqlDataReader FillDataReader(string sql, SqlParameter[] parms, string ConnectionString)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 120;

            foreach (SqlParameter p in parms)
            {
                cmd.Parameters.Add(p);
            }

            SqlDataReader dr = null;

            try
            {
                conn.Open();

                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                if (dr != null) { dr.Close(); }
                conn.Close();

                string notes = "FillDataReader(string sql, SqlParameter[] parms, string conn)";
                foreach (SqlParameter p in parms)
                {
                    notes += p.ParameterName + "=" + p.Value;
                }

                ExceptionHandler(ex, sql, notes);

                throw ex;
            }
            finally
            {
            }

            //Be sure to test for null in calling code
            return dr;
        }

        // Returns an SqlDataReader object when passed an SQL query.
        /// <summary>
        /// Returns an SqlDataReader object when passed an SQL query.
        /// Uses connectionstring parameter for flexibility.
        /// 
        /// </summary>
        /// <param name="sql">SQL Query</param>
        /// <param name="ConnectionString">SQL Login information</param>
        /// <returns>SqlDataReader (Be sure to check for null in calling code)</returns>
        static public SqlDataReader FillDataReader(string sql, string ConnectionString, SqlParameter[] NamedParameters)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 120;

            foreach (SqlParameter p in NamedParameters)
            {
                cmd.Parameters.Add(p);
            }

            SqlDataReader dr = null;

            try
            {
                conn.Open();

                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (SqlException ex)
            {
                if (dr != null) { dr.Close(); }
                conn.Close();
                string notes = "FillDataReader(string sql, string conn, SqlParameter[] parms, )";
                foreach (SqlParameter p in NamedParameters)
                {
                    notes += p.ParameterName + "=" + p.Value;
                }

                ExceptionHandler(ex, sql, notes);

                throw ex;
            }
            finally
            {
            }

            //Be sure to test for null in calling code
            return dr;
        }



        /// <summary>
        /// Returns a Single string value from the database
        /// </summary>
        /// <param name="sql">SQL query used to pull the single value.</param>
        /// <returns></returns>
        static public string GetScalar(string sql)
        {
            string result = "";

            SqlConnection conn = new SqlConnection(FMConnString);
            SqlCommand cmd = new SqlCommand(sql, conn);

            try
            {
                conn.Open();

                result = cmd.ExecuteScalar().ToString();
            }
            catch (SqlException ex)
            {
                ExceptionHandler(ex, sql, "");
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return result;
        }


        /// <summary>
        /// Returns a single string value from the database.  
        /// ConnectionString is a parameter for flexibility.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="ConnectionString"></param>
        /// <returns></returns>
        static public string GetScalar(string sql, string ConnectionString)
        {
            string result = "";

            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, conn);

            try
            {
                conn.Open();

                result = cmd.ExecuteScalar().ToString();
            }
            catch (SqlException ex)
            {
                ExceptionHandler(ex, sql, "");
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return result;
        }

        static public int GetScalar(string sql, bool inttrue)
        {
            string call = "";
            int result = 0;

            SqlConnection cn = new SqlConnection(FMConnString);
            SqlCommand cmd = new SqlCommand(sql, cn);
            cn.Open();

            try
            {
                call = cmd.ExecuteScalar().ToString();
                if (call == "")
                {
                    result = 0;
                }
                else
                {
                    result = Convert.ToInt32(call);
                }
            }
            catch (SqlException ex)
            {
                //dr.Close();
                cn.Close();

                ExceptionHandler(ex, sql, "ExecScalar, true");
            }
            finally
            {
                cn.Close();
            }

            return result;
        }

        static public int GetScalar(string sql, string ConnectionString, bool inttrue)
        {
            string call = "";
            int result = 0;

            SqlConnection cn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, cn);
            cn.Open();

            try
            {
                call = cmd.ExecuteScalar().ToString();
                if (call == "")
                {
                    result = 0;
                }
                else
                {
                    result = Convert.ToInt32(call);
                }
            }
            catch (SqlException ex)
            {
                //dr.Close();
                cn.Close();

                ExceptionHandler(ex, sql, "ExecScalar, true");
            }
            finally
            {
                cn.Close();
            }

            return result;
        }

        static public string GetScalar(string sql, SqlParameter[] parms)
        {
            string result = "";

            SqlConnection conn = new SqlConnection(FMConnString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 120;

            foreach (SqlParameter p in parms)
            {
                cmd.Parameters.Add(p);
            }

            try
            {
                conn.Open();

                result = cmd.ExecuteScalar().ToString();
            }
            catch (SqlException ex)
            {
                ExceptionHandler(ex, sql, "");
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Executes an SQL Statement with no return value.
        /// Uses connectionstring from Config class.
        /// </summary>
        /// <param name="sql"></param>
        static public void Exec(string sql)
        {
            SqlConnection conn = new SqlConnection(FMConnString);

            SqlCommand cmd = new SqlCommand(sql, conn);
            try
            {
                conn.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                ExceptionHandler(ex, sql, "");
            }

            finally
            {
                conn.Close();
            }

        }


        /// <summary>
        /// Executes an SQL Statement with no return data.
        /// </summary>
        /// <param name="sql">SQL query to execute</param>
        /// <param name="ConnectionString">SQL Login information</param>
        static public void Exec(string sql, string ConnectionString)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            SqlCommand cmd = new SqlCommand(sql, conn);
            try
            {
                conn.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                ExceptionHandler(ex, sql, "");
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// Executes an SQL Statement with no return data.
        /// </summary>
        /// <param name="sql">SQL query to execute</param>
        /// <param name="ConnectionString">SQL Login information</param>
        static public void Exec(string sql, string ConnectionString, SqlParameter[] parms)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, conn);

            foreach (SqlParameter p in parms)
            {
                cmd.Parameters.Add(p);
            }
            SqlDataReader dr = null;

            try
            {
                conn.Open();
                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (SqlException ex)
            {
                ExceptionHandler(ex, sql, "");
            }
            finally
            {
                conn.Close();
            }

        }

        static public void Exec(string sql, SqlParameter[] parms)
        {
            SqlConnection cn = new SqlConnection(FMConnString);

            cn.Open();
            SqlCommand cmd1 = new SqlCommand(sql, cn);

            foreach (SqlParameter p in parms)
            {
                cmd1.Parameters.Add(p);
            }

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                string notes = "Exec(string sql, SqlParameter[] parms, )";
                foreach (SqlParameter p in parms)
                {
                    notes += p.ParameterName + "=" + p.Value;
                }

                ExceptionHandler(ex, sql, notes);

            }
            cn.Close();

        }


        static public SqlParameter[] BuildParametersFrom(string sql, params object[] list)
        {
            //parse sql string, then create one param per @ in query.
            //There has to be a better way to do this...so far so good though
            int itemcount = list.GetLength(0);
            SqlParameter[] mySqlParameters = new SqlParameter[itemcount];

            //split sql string into words, then test each word for '@'.  Add param when found.
            char[] arrSplitChars = { ' ', ',', '(', ')' };
            string[] arrWords = sql.Split(arrSplitChars, StringSplitOptions.RemoveEmptyEntries);

            int currentParam = 0;
            foreach (string s in arrWords)
            {
                if (s.Substring(0, 1) == "@")
                {

                    //This was Dave's original code; can't make every param a string!
                    //SqlParameter p = new SqlParameter(s, list[currentParam].ToString());

                    SqlParameter p = new SqlParameter(s, list[currentParam]);
                    mySqlParameters[currentParam] = p;

                    currentParam++;
                }
            }

            if (itemcount != currentParam)
            {
                Exception ex = new Exception("The sql string and the number of objects passed in must match.");
                dbErrorLogging.LogError("DBUtil:BuildParametersFrom", ex);
                throw ex;
            }

            return mySqlParameters;
        }

        //Helpful data routines

        static public string CheckZipCode(string country, string zip)
        {
            string result = "";
            String x1, x2, x3, x4, x5, x6, x7, x8, x9, x10;
            if (country == "US" || country == "CA")
            {
                if (country == "US" && zip.Length == 5)
                {
                    x1 = zip.Substring(0, 1); x2 = zip.Substring(1, 1); x3 = zip.Substring(2, 1); x4 = zip.Substring(3, 1); x5 = zip.Substring(4, 1);

                    if (IsNumeric(x1) && IsNumeric(x2) && IsNumeric(x3) && IsNumeric(x4) && IsNumeric(x5))
                        return result;
                    else
                        return "Zip in USA must be numeric (XXXXX or XXXXX-XXXX)";
                }
                else if (country == "US" && zip.Length == 10)
                {
                    x1 = zip.Substring(0, 1); x2 = zip.Substring(1, 1); x3 = zip.Substring(2, 1); x4 = zip.Substring(3, 1); x5 = zip.Substring(4, 1); x6 = zip.Substring(5, 1); x7 = zip.Substring(6, 1); x8 = zip.Substring(7, 1); x9 = zip.Substring(8, 1); x10 = zip.Substring(9, 1);

                    if (IsNumeric(x1) && IsNumeric(x2) && IsNumeric(x3) && IsNumeric(x4) && IsNumeric(x5) && x6 == "-" && IsNumeric(x7) && IsNumeric(x8) && IsNumeric(x9) && IsNumeric(x10))
                        return result;
                    else
                        return "Zipc in USA must be numeric (XXXXX or XXXXX-XXXX)";
                }
                else if (country == "CA" && zip.Length == 7)
                {
                    x1 = zip.Substring(0, 1); x2 = zip.Substring(1, 1); x3 = zip.Substring(2, 1); x4 = zip.Substring(3, 1); x5 = zip.Substring(4, 1); x6 = zip.Substring(5, 1); x7 = zip.Substring(6, 1);

                    if (!IsNumeric(x1) && IsNumeric(x2) && !IsNumeric(x3) && x4 == " " && IsNumeric(x5) && !IsNumeric(x6) && IsNumeric(x7))
                        return result;
                    else
                        return "Zip in Canada must be of type AXA XAX (where A is a letter and X is a number)";
                }
                else if (country == "CA" && zip.Length == 6)
                {
                    x1 = zip.Substring(0, 1); x2 = zip.Substring(1, 1); x3 = zip.Substring(2, 1); x4 = zip.Substring(3, 1); x5 = zip.Substring(4, 1); x6 = zip.Substring(5, 1);

                    if (!IsNumeric(x1) && IsNumeric(x2) && !IsNumeric(x3) && IsNumeric(x4) && !IsNumeric(x5) && IsNumeric(x6))
                        return result;
                    else
                        return "Zip in Canada must be of type AXA XAX (where A is a letter and X is a number)";
                }
                else if (country == "US")
                {
                    return "Zipcode in USA must be numeric (XXXXX or XXXXX-XXXX)";
                }
                else if (country == "CA")
                {
                    return "Zip in Canada must be of type AXA XAX (where A is a letter and X is a number)";
                }
                else
                    return result;
            }
            else
                return result;
        }


        public static bool IsNumeric(string s)
        {
            try
            {
                Int32.Parse(s);
            }
            catch
            {
                return false;
            }
            return true;
        }


        static public string AddBlanks(int stringLen)
        {
            string blankString = "";

            for (int i = 0; i < stringLen - 1; i++)
            {
                blankString += " ";
            }
            return blankString;
        }

        public static string GetCountry(string countryCode)
        {
            String sql, country = "";

            SqlConnection cn = new SqlConnection(AdminCart.Config.ConnStr());
            cn.Open();

            sql = "SELECT Name FROM Locations WHERE Code = '" + countryCode + "' AND Parent = 0";
            SqlCommand sqlCommand = new SqlCommand(sql, cn);
            SqlDataReader sqlDataReader;

            try
            {
                sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.Read())
                    country = sqlDataReader.GetString(0).ToString();
                else
                    country = "";

                sqlDataReader.Close();
                cn.Close();

            }
            catch 
            {; } //Mail.SendMail("site@facemaster.com", "dhenson@certifiednetworks.com", "site dbutil error - GetCountry()", ex.Message + "---------" + sql);

            return country;
        }

        static public string GetLocationParentID(string code)
        {
            //get the parent ID to get the type/date of state province dropdown
            string sql = "SELECT ID FROM Locations WHERE Code = '" + code + "'";
            string result = "";

            SqlConnection cn = new SqlConnection(AdminCart.Config.ConnStr());
            cn.Open();

            SqlCommand cmd = new SqlCommand(sql, cn);
            try
            {
                result = cmd.ExecuteScalar().ToString();
            }
            catch 
            {; } //Mail.SendMail("site@facemaster.com", "dhenson@certifiednetworks.com", "site dbutil error -- AppCode- GetLocationParentID()", ex.Message + "---------" + sql);
            finally
            {
                cmd.Connection.Close();
            }

            return result;
        }

        static public DataSet GetLocations(int parentID)
        {
            string sql = @"
        SELECT Code, Name 
        FROM Locations 
        WHERE Parent = " + parentID.ToString() + @"
        ORDER BY Name ASC
        ";
            DataSet ds = FillDataSet(sql, "Locations");
            return ds;
        }

    }
}

