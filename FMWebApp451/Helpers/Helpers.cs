﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FM2015.Models;
using FM2015.ViewModels;
using Stripe;

namespace FM2015.Helpers
{
    public class Helpers
    {
        public static string GetSelectListValue(SelectList selectList)
        {
            SelectListItem[] items = selectList.ToArray();
            SelectListItem selectedItem = items.FirstOrDefault(i => i.Selected == true)
                ?? items[0];
            return selectedItem.Value;
        }

        public static CustomerViewModel UpdateStateCountrySelectLists(CustomerViewModel cvm, string billState, string shipState)
        {
            cvm.BillAddress.Country = cvm.SelectBillCountryCode;
            cvm.BillCountrySelectList = Location.GetCountrySelectList(cvm.SelectBillCountryCode);
            cvm.BillStateSelectList = Location.GetStateSelectList(Location.GetCountryCodeIDFromSelectList(cvm.BillCountrySelectList));

            if (cvm.BillStateSelectList.Count() != 0)
            {
                cvm.BillAddress.State = cvm.SelectBillStateCode;
            }
            else
            {
                if (billState != null)
                {
                    cvm.BillAddress.State = billState.ToString();
                }
            }

            cvm.ShipAddress.Country = cvm.SelectShipCountryCode;
            cvm.ShipCountrySelectList = Location.GetCountrySelectList(cvm.SelectShipCountryCode);
            cvm.ShipStateSelectList = Location.GetStateSelectList(Location.GetCountryCodeIDFromSelectList(cvm.ShipCountrySelectList));

            if (cvm.ShipStateSelectList.Count() != 0)
            {
                cvm.ShipAddress.State = cvm.SelectShipStateCode;
            }
            else
            {
                if (shipState != null)
                {
                    cvm.ShipAddress.State = shipState.ToString();
                }
            }

            return cvm;
        }

        public static SelectList SetMonthSelectList()
        {

            List<SelectListItem> items = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                DateTime dt = new DateTime(DateTime.Now.Year, i, 1);
                items.Add(new SelectListItem { Text = dt.ToString("MMM"), Value = i.ToString() });
            }

            SelectList selList = new SelectList(items, "Value", "Text", DateTime.Now.Month.ToString());

            return selList;
        }

        public static SelectList SetYearSelectList()
        {

            List<SelectListItem> items = new List<SelectListItem>();
            int year = DateTime.Now.Year;
            year = year - 10; //start ten years back
            for (int i = 1; i <= 20; i++)
            {
                items.Add(new SelectListItem { Text = year.ToString(), Value = year.ToString() });
                year = year + 1;
            }

            SelectList selList = new SelectList(items, "Value", "Text", DateTime.Now.Year.ToString());

            return selList;
        }

        public static void ProcessStripeException(StripeException ex, string message)
        {
            int response = (int)ex.HttpStatusCode;
            StripeError err = ex.StripeError;
            string errType = err.ErrorType;
            string errCode = (string.IsNullOrEmpty(err.Code)) ? "" : err.Code;
            string errError = (string.IsNullOrEmpty(err.Error)) ? "" : err.Error;
            string errMessage = (string.IsNullOrEmpty(err.Message)) ? "" : err.Message;
            string msg = message + Environment.NewLine;
            msg += " Stripe Error: errorType= " + errType + Environment.NewLine;
            msg += "errorCode= " + errCode + Environment.NewLine;
            msg += "errorError=" + errError + Environment.NewLine;
            msg += "errorMEssage= " + errMessage + Environment.NewLine;
            mvc_Mail.SendDiagEmail("api:Stripe WebHooks: ", msg);
        }

        public static void ProcessUpdateEmail(StripeEvent stripeEvent, string value, string message)
        {
            string id = (string.IsNullOrEmpty(stripeEvent.Id)) ? "stripeEvent ID = null" : stripeEvent.Id;
            //string userID = (string.IsNullOrEmpty(stripeEvent.Account)) ? "stripeEvent UserID = null" : stripeEvent.UserId;
            string userID = "null";
            string type = (string.IsNullOrEmpty(stripeEvent.Type)) ? "stripeEvent Type = null" : stripeEvent.Type;
            DateTime created = (stripeEvent.Created == null) ? DateTime.MinValue : (DateTime)stripeEvent.Created;
            string data = (stripeEvent.Data == null) ? "Stripe Data is Null" : stripeEvent.Data.ToString();
            string rqstIP = (HttpContext.Current.Request.UserHostAddress != null) ? HttpContext.Current.Request.UserHostAddress : String.Empty;

            string msg = "Stripe WebHook Event" + Environment.NewLine;
            msg += "Event ID: " + id + Environment.NewLine;
            msg += "Event Type: " + type + Environment.NewLine;
            msg += "User ID: " + userID + Environment.NewLine;
            msg += "Sent FROM IP: " + rqstIP + Environment.NewLine;
            msg += "Time Created: " + created.ToString() + Environment.NewLine;
            msg += "Event Data: " + data + Environment.NewLine;
            msg += "raw json data: " + value.ToString() + Environment.NewLine;
            mvc_Mail.SendDiagEmail("api:Stripe WebHooks: " + type + " :" + message, msg);
        }
    }
}