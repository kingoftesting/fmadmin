﻿using System;
using System.Net;

namespace ShopifyAgent
{
    [Serializable]
    public class ShopifyException : ApplicationException
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public ShopifyError ShopifyError { get; set; }

        public ShopifyException() 
        { 
        }

        public ShopifyException(HttpStatusCode httpStatusCode, ShopifyError shopifyError, string message)
            : base(message)
        {
            HttpStatusCode = httpStatusCode;
            ShopifyError = shopifyError;
        }
    }
}