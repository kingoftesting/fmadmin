﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using ShopifyAgent;

namespace FMWebApp.Helpers
{
    public class ShopifyWebHookHelpers
    {
        public static ShopifyOrder GetOrderCreationFromWebHook(string webHookValue)
        {
            ShopifyAgent.ShopifyOrder order = new ShopifyOrder();
            string msg = string.Empty;
            try
            {
                order = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyOrder>(webHookValue);
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                msg = "error in ShopifyWebHooks: GetOrderCreationFromWebHook" + Environment.NewLine;
                msg += "value: " + webHookValue + Environment.NewLine;
                msg += "exception message: " + ex.Message + Environment.NewLine;
                mvc_Mail.SendDiagEmail("GetOrderCreationFromWebHook: Error", msg);
                return order;
            }

            string rqstIP = string.Empty;
            msg = string.Empty;
            rqstIP = (HttpContext.Current.Request.UserHostAddress != null) ? HttpContext.Current.Request.UserHostAddress : String.Empty;
            msg = "Shopify WebHook Event: GetOrderCreationFromWebHook" + Environment.NewLine;
            msg += "Event Type: " + "Shopify Order Creation" + Environment.NewLine;
            msg += "Sent FROM IP: " + rqstIP + Environment.NewLine;
            msg += "Time Created: " + DateTime.Now.ToString() + Environment.NewLine;
            msg += "Order: Name: " + order.Name + Environment.NewLine;
            msg += "Order: Email: " + order.Email + Environment.NewLine;
            msg += "Order: Number: " + order.Number + Environment.NewLine;
            msg += "Order: Total: " + order.Total_price + Environment.NewLine;
            msg += "Order: ID: " + order.Id + Environment.NewLine;
            msg += "raw json data: " + webHookValue + Environment.NewLine;
            //mvc_Mail.SendDiagEmail("GetOrderCreationFromWebHook", msg);

            return order;
        }

        public static ShopifyOrder GetOrderUpdateFromWebHook(string webHookValue)
        {
            ShopifyAgent.ShopifyOrder order = new ShopifyOrder();
            string msg = string.Empty;
            try
            {
                order = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyOrder>(webHookValue);
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                msg = "error in ShopifyWebHooks: GetOrderUpdateFromWebHook" + Environment.NewLine;
                msg += "value: " + webHookValue + Environment.NewLine;
                msg += "exception message: " + ex.Message + Environment.NewLine;
                mvc_Mail.SendDiagEmail("GetOrderUpdateFromWebHook: Error", msg);
                return order;
            }

            string rqstIP = string.Empty;
            msg = string.Empty;
            rqstIP = (HttpContext.Current.Request.UserHostAddress != null) ? HttpContext.Current.Request.UserHostAddress : String.Empty;
            msg = "Shopify WebHook Event: GetOrderUpdateFromWebHook" + Environment.NewLine;
            msg += "Event Type: " + "Shopify Order Update" + Environment.NewLine;
            msg += "Sent FROM IP: " + rqstIP + Environment.NewLine;
            msg += "Time Created: " + DateTime.Now.ToString() + Environment.NewLine;
            msg += "Order: Name: " + order.Name + Environment.NewLine;
            msg += "Order: Email: " + order.Email + Environment.NewLine;
            msg += "Order: Number: " + order.Number + Environment.NewLine;
            msg += "Order: Total: " + order.Total_price + Environment.NewLine;
            msg += "Order: ID: " + order.Id + Environment.NewLine;
            msg += "raw json data: " + webHookValue + Environment.NewLine;
            //mvc_Mail.SendDiagEmail("GetOrderUpdateFromWebHook", msg);

            return order;
        }

        public static ShopifyProduct GetProductCreationFromWebHook(string webHookValue)
        {
            ShopifyAgent.ShopifyProduct product = new ShopifyProduct();
            string msg = string.Empty;
            try
            {
                product = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyProduct>(webHookValue);
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                msg = "error in ShopifyWebHooks: GetProductCreationFromWebHook" + Environment.NewLine;
                msg += "value: " + webHookValue + Environment.NewLine;
                msg += "exception message: " + ex.Message + Environment.NewLine;
                mvc_Mail.SendDiagEmail("GetProductCreationFromWebHook: Error", msg);
                return product;
            }

            string rqstIP = string.Empty;
            msg = string.Empty;
            rqstIP = (HttpContext.Current.Request.UserHostAddress != null) ? HttpContext.Current.Request.UserHostAddress : String.Empty;
            msg = "Shopify WebHook Event: GetProductCreationFromWebHook" + Environment.NewLine;
            msg += "Event Type: " + "Shopify Product Creation" + Environment.NewLine;
            msg += "Sent FROM IP: " + rqstIP + Environment.NewLine;
            msg += "Time Created: " + DateTime.Now.ToString() + Environment.NewLine;
            msg += "Product: title: " + product.Title + Environment.NewLine;
            msg += "Product: productId: " + product.variants[0].Product_id + Environment.NewLine;
            msg += "Product: sku: " + product.variants[0].Sku + Environment.NewLine;
            msg += "Product: price: " + product.variants[0].Price + Environment.NewLine;
            msg += "raw json data: " + webHookValue + Environment.NewLine;
            mvc_Mail.SendDiagEmail("GetProductCreationFromWebHook", msg);

            return product;
        }

        public static ShopifyProduct GetProductUpdateFromWebHook(string webHookValue)
        {
            ShopifyAgent.ShopifyProduct product = new ShopifyProduct();
            string msg = string.Empty;
            try
            {
                product = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyProduct>(webHookValue);
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                msg = "error in ShopifyWebHooks: GetProductUpdateFromWebHook" + Environment.NewLine;
                msg += "value: " + webHookValue + Environment.NewLine;
                msg += "exception message: " + ex.Message + Environment.NewLine;
                mvc_Mail.SendDiagEmail("GetProductUpdateFromWebHook: Error", msg);
                return product;
            }

            string rqstIP = string.Empty;
            msg = string.Empty;
            rqstIP = (HttpContext.Current.Request.UserHostAddress != null) ? HttpContext.Current.Request.UserHostAddress : String.Empty;
            msg = "Shopify WebHook Event: GetProductUpdateFromWebHook" + Environment.NewLine;
            msg += "Event Type: " + "Shopify Product Update" + Environment.NewLine;
            msg += "Sent FROM IP: " + rqstIP + Environment.NewLine;
            msg += "Time Created: " + DateTime.Now.ToString() + Environment.NewLine;
            msg += "Product: title: " + product.Title + Environment.NewLine;
            msg += "Product: productId: " + product.variants[0].Product_id + Environment.NewLine;
            msg += "Product: sku: " + product.variants[0].Sku + Environment.NewLine;
            msg += "Product: price: " + product.variants[0].Price + Environment.NewLine;
            msg += "raw json data: " + webHookValue + Environment.NewLine;
            mvc_Mail.SendDiagEmail("GetProductUpdateFromWebHook", msg);

            return product;
        }

        public static ShopifyRefund GetRefundCreationFromWebHook(string webHookValue)
        {
            ShopifyAgent.ShopifyRefund refund = new ShopifyRefund();
            string msg = string.Empty;
            try
            {
                refund = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyRefund>(webHookValue);
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                msg = "error in ShopifyWebHooks: GetRefundCreationFromWebHook" + Environment.NewLine;
                msg += "value: " + webHookValue + Environment.NewLine;
                msg += "exception message: " + ex.Message + Environment.NewLine;
                mvc_Mail.SendDiagEmail("GetRefundCreationFromWebHook: Error", msg);
                return refund;
            }

            string rqstIP = string.Empty;
            msg = string.Empty;
            rqstIP = (HttpContext.Current.Request.UserHostAddress != null) ? HttpContext.Current.Request.UserHostAddress : String.Empty;
            msg = "Shopify WebHook Event: GetRefundCreationFromWebHook" + Environment.NewLine;
            msg += "Event Type: " + "Shopify Refund Creation" + Environment.NewLine;
            msg += "Sent FROM IP: " + rqstIP + Environment.NewLine;
            msg += "Time Created: " + DateTime.Now.ToString() + Environment.NewLine;
            msg += "Refund: OrderId: " + refund.Order_id + Environment.NewLine;
            msg += "Refund: sku: " + refund.Transactions + Environment.NewLine;
            msg += "raw json data: " + webHookValue + Environment.NewLine;
            mvc_Mail.SendDiagEmail("GetRefundCreationFromWebHook", msg);

            return refund;
        }
    }
}