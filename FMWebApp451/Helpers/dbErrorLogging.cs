///////////////////////////////////////////////////////////////////////////////
//  Filename: dbErrorLogging.cs
//  Author:  Kishore Kumar.N
//  Date:  08/03/2008
//  Description:  This file log the errors into the database 
//  This code may be used freely as long as you do not sell the code, binaries
//  or any other form
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Configuration;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Text;

namespace FM2015.Helpers
{
    /// <summary>
    /// This file Handles the Exception and logging them into the database.
    /// </summary>
    public class dbErrorLogging
    {
        public dbErrorLogging()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static void LogError(string msg, Exception oEx)
        {
            bool errLogCheck = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableLog"]);
            bool errLogEmailCheck = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableLogEmail"]);
            if (errLogCheck)
            {
                HandleException(msg, oEx);
            }
            if (errLogEmailCheck)
            {
                SendExceptionMail(msg, oEx);
            }
        }

        public static void LogError(string msg, Exception oEx, bool sendMail)
        {
            bool errLogCheck = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableLog"]);
            bool errLogEmailCheck = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableLogEmail"]);
            if (errLogCheck)
            {
                HandleException(msg, oEx);
            }
            if (errLogEmailCheck && sendMail)
            {
                SendExceptionMail(msg, oEx);
            }
        }

        public static void LogError(string msg)
        {
            Exception oEx = new Exception();
            bool sendMail = true;
            bool errLogCheck = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableLog"]);
            bool errLogEmailCheck = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableLogEmail"]);
            if (errLogCheck)
            {
                HandleException(msg, oEx);
            }
            if (errLogEmailCheck && sendMail)
            {
                SendExceptionMail(msg, oEx);
            }
        }


        public static void HandleException(string msg, Exception ex)
        {
            HttpContext ctxObject = HttpContext.Current;

            String ConnStringName = AdminCart.Config.ConnStr();
            //string strLogConnString = WebConfigurationManager.ConnectionStrings[ConnStringName].ConnectionString;
            string strLogConnString = ConnStringName;

            string logDateTime = DateTime.Now.ToString("g");
            string strReqURL = (ctxObject.Request.Url != null) ? ctxObject.Request.Url.ToString() : String.Empty;
            string strReqQS = (ctxObject.Request.QueryString != null) ? ctxObject.Request.QueryString.ToString() : String.Empty;
            string strServerName = String.Empty;
            if (ctxObject.Request.ServerVariables["HTTP_REFERER"] != null)
            {
                strServerName = ctxObject.Request.ServerVariables["HTTP_REFERER"].ToString();
            }
            string strUserAgent = (ctxObject.Request.UserAgent != null) ? ctxObject.Request.UserAgent : String.Empty;
            string strUserIP = (ctxObject.Request.UserHostAddress != null) ? ctxObject.Request.UserHostAddress : String.Empty;

            string strUserAuthen = (FM2015.Models.User.IsAuthenticated()) ? "YES" : String.Empty;

            string strUserName = (FM2015.Models.User.GetUserID() > 0) ? FM2015.Models.User.GetUserName() : String.Empty;
            string strMessage = string.Empty, strSource = string.Empty, strTargetSite = string.Empty, strStackTrace = string.Empty;

            strMessage = "Caller Message: " + msg + Environment.NewLine;
            while (ex != null)
            {
                strMessage += "System Message: " + ex.Message;
                strSource = (ex.Source == null) ? "n/a" : ex.Source.ToString();
                strTargetSite = (ex.TargetSite == null) ? "n/a" : ex.TargetSite.ToString();
                strStackTrace = (ex.StackTrace == null) ? "n/a" : ex.StackTrace;
                ex = ex.InnerException;
            }

            if (strLogConnString.Length > 0)
            {
                SqlCommand strSqlCmd = new SqlCommand();
                strSqlCmd.CommandType = CommandType.StoredProcedure;
                strSqlCmd.CommandText = "fm_LogExceptionToDB";
                SqlConnection sqlConn = new SqlConnection(ConnStringName);
                strSqlCmd.Connection = sqlConn;
                sqlConn.Open();
                try
                {
                    strSqlCmd.Parameters.Add(new SqlParameter("@Source", strSource));
                    strSqlCmd.Parameters.Add(new SqlParameter("@LogDateTime", logDateTime));
                    strSqlCmd.Parameters.Add(new SqlParameter("@Message", strMessage));
                    strSqlCmd.Parameters.Add(new SqlParameter("@QueryString", strReqQS));
                    strSqlCmd.Parameters.Add(new SqlParameter("@TargetSite", strTargetSite));
                    strSqlCmd.Parameters.Add(new SqlParameter("@StackTrace", strStackTrace));
                    strSqlCmd.Parameters.Add(new SqlParameter("@ServerName", strServerName));
                    strSqlCmd.Parameters.Add(new SqlParameter("@RequestURL", strReqURL));
                    strSqlCmd.Parameters.Add(new SqlParameter("@UserAgent", strUserAgent));
                    strSqlCmd.Parameters.Add(new SqlParameter("@UserIP", strUserIP));
                    strSqlCmd.Parameters.Add(new SqlParameter("@UserAuthentication", strUserAuthen));
                    strSqlCmd.Parameters.Add(new SqlParameter("@UserName", strUserName));
                    SqlParameter outParm = new SqlParameter("@EventId", SqlDbType.Int);
                    outParm.Direction = ParameterDirection.Output;
                    strSqlCmd.Parameters.Add(outParm);
                    strSqlCmd.ExecuteNonQuery();
                    strSqlCmd.Dispose();
                    sqlConn.Close();
                }
                catch 
                { ; } //EventLog.WriteEntry(exc.Source, "Database Error From Exception Log!", EventLogEntryType.Error, 65535);
                finally
                {
                    strSqlCmd.Dispose();
                    sqlConn.Close();
                }
            }
        }

        protected static string FormatExceptionDescription(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            HttpContext context = HttpContext.Current;

            sb.Append("Time of Error: " + DateTime.Now.ToString("g") + Environment.NewLine);
            sb.Append("URL: " + context.Request.Url + Environment.NewLine);
            sb.Append("QueryString: " + context.Request.QueryString.ToString() + Environment.NewLine);
            sb.Append("Server Name: " + context.Request.ServerVariables["SERVER_NAME"] + Environment.NewLine);
            sb.Append("User Agent:  " + context.Request.UserAgent + Environment.NewLine);
            sb.Append("User IP: " + context.Request.UserHostAddress + Environment.NewLine);
            sb.Append("User Host Name: " + context.Request.UserHostName + Environment.NewLine);

            string userAuth = (FM2015.Models.User.IsAuthenticated()) ? "YES" : String.Empty;
            sb.Append("User is Authenticated: " + userAuth + Environment.NewLine);

            string userName = (FM2015.Models.User.GetUserID() > 0) ? FM2015.Models.User.GetUserName() : String.Empty;
            sb.Append("User Name: " + userName + Environment.NewLine);

            while (e != null)
            {
                sb.Append("Message: " + e.Message + Environment.NewLine);
                sb.Append("Source: " + e.Source + Environment.NewLine);
                sb.Append("TargetSite: " + e.TargetSite + Environment.NewLine);
                sb.Append("StackTrace: " + e.StackTrace + Environment.NewLine);
                sb.Append(Environment.NewLine + Environment.NewLine);

                e = e.InnerException;
            }

            sb.Append("--------------------------------------------------------" + Environment.NewLine);
            sb.Append("Regards," + Environment.NewLine);
            sb.Append("Admin");
            return sb.ToString();
        }

        public static void SendExceptionMail(Exception e)
        {
            string mailFrom = AdminCart.Config.MailFrom();
            string mailTo = AdminCart.Config.MailTo;
            string subject = "FM dbErrorLogging";
            string contactBody = FormatExceptionDescription(e);
            //mvc_Mail.SendMail(mailFrom, mailTo, subject, contactBody);
        }

        public static void SendExceptionMail(string msg, Exception e)
        {
            string mailFrom = AdminCart.Config.MailFrom();
            string mailTo = AdminCart.Config.MailTo;
            string subject = "FM dbErrorLogging";
            string contactBody = msg + Environment.NewLine + FormatExceptionDescription(e);
            //bool response = mvc_Mail.SendMail(mailTo, mailFrom, string.Empty, string.Empty, subject, contactBody, false);
        }

    }
}

    