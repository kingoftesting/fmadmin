<%@ Import Namespace="System.Drawing" %>                                        
<%@ Import Namespace="System.Drawing.Imaging" %> 
<%@ Import Namespace="System.Data" %> 
<%@ Import Namespace="System.Data.SqlClient" %> 
                               
<script language="C#" runat="server">                                           

//this file is used for email blasts
//created 7/2004 dhenson@certifiednetworks.com
                                                                                
  void Page_Load(Object sender, EventArgs e)                                    
  {                                        
      string productId = "";
      
      if (Request["ProductId"] == null)
      	productId = "11";
      else
    	productId = Request["ProductId"].ToString();
    	
    int width = 125, height = 165;
	string description = "", imagePath = "";
	
	SqlDataReader dr = DBUtil.FillDataReader("SELECT TOP 1 * FROM Products WHERE ProductId = " + productId);
	while(dr.Read())
	{
	  description = dr["Name"].ToString() + " - " + String.Format("{0:c} ", dr["Price"]);
	  imagePath = "images/products/" + dr["ThumbnailImage"].ToString();
	}
	dr.Close();

      string webpath = Server.MapPath(imagePath);
      	
    Bitmap objBitmap = new Bitmap(webpath); //new Bitmap(width, height);
    Graphics objGraphics = Graphics.FromImage(objBitmap);

    // Create a background for the border                                 
    //objGraphics.FillRectangle(new SolidBrush(Color.Transparent), 0, 0, width, height);

    // Create a LightBlue background                                            
    //objGraphics.FillRectangle(new SolidBrush(Color.White), 0, 0, width, height);

    // Place text on image
    Font fontBanner = new Font("Trebuchet MS", 8); //, FontStyle.Bold);  //("WASP UPC LC",18, FontStyle.Bold); 
    Font fontWhiteBack = new Font("Trebuchet MS", 8); //, FontStyle.Bold);
	
	StringFormat stringFormat = new StringFormat();
    stringFormat.Alignment = StringAlignment.Center;
    stringFormat.LineAlignment = StringAlignment.Center;

    String buttontext = description;
    objGraphics.DrawString(buttontext, fontWhiteBack, new SolidBrush(Color.White), new Rectangle(2, 2, width, height-10), stringFormat);
    objGraphics.DrawString(buttontext, fontWhiteBack, new SolidBrush(Color.White), new Rectangle(0, 0, width, height-10), stringFormat);
    objGraphics.DrawString(buttontext, fontWhiteBack, new SolidBrush(Color.White), new Rectangle(1, 2, width, height-10), stringFormat);
    objGraphics.DrawString(buttontext, fontWhiteBack, new SolidBrush(Color.White), new Rectangle(1, 0, width, height-10), stringFormat);
    objGraphics.DrawString(buttontext, fontBanner, new SolidBrush(Color.Black), new Rectangle(1, 1, width, height-10), stringFormat);

    
    // Save the image to the OutputStream                                       
    objBitmap.Save(Response.OutputStream, ImageFormat.Jpeg);

    // clean up...                                                              
    objGraphics.Dispose();
    objBitmap.Dispose();                                                        
  }                                                                             
                                                                                
</script>                                                                       
                  