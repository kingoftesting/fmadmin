﻿using AdminCart;

namespace FMWebApp451.Interfaces
{
    public interface IAddress
    {
        Address GetByCustomerId(int customerId, int type); // should use Enum Address.AddressType type
    }
}
