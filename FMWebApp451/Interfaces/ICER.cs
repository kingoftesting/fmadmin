﻿
using AdminCart;

namespace FMWebApp451.Interfaces
{
    public interface ICER
    {
        adminCER.CER GetByCustomer(Customer c);
        int Add(adminCER.CER cer);
    }
}
