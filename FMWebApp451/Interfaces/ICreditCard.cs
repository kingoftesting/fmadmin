﻿using System;
using System.Collections.Generic;
using FMWebApp451.ViewModels.Stripe;

namespace FMWebApp451.Interfaces
{
    public interface ICreditCard
    {
        string AddNewCard(CreditCardViewModel ccvm);
    }
}
