﻿using System.Collections.Generic;
using AdminCart;

namespace FMWebApp451.Interfaces
{
    public interface ICustomer
    {
        Customer GetById(int id);
        List<Customer> GetAll();
        bool Save(Customer customer);
    }
}
