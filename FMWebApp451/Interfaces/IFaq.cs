﻿using System.Collections.Generic;
using FM2015.Models;

namespace FMWebApp451.Interfaces
{
    public interface IFaq
    {
        Faq GetById(int FaqId);
        List<Faq> GetAll();
        List<Faq> GetAll(string lookUpParam);
        int Save(Faq faq);
    }
}
