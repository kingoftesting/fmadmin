﻿using System;
using System.Collections.Generic;
using FMWebApp451.ViewModels;

namespace FMWebApp451.Interfaces
{
    public interface IMultiPayTransactionsByDate
    {
        List<MultiPayTransactionsByDate> GetAll(DateTime startDate, DateTime endDate);
    }
}
