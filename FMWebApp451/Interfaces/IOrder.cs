﻿using System;
using System.Collections.Generic;

namespace FMWebApp451.Interfaces
{
    public interface IOrder
    {
        List<AdminCart.Order> GetAll();
        List<AdminCart.Order> GetByDate(DateTime startDate, DateTime endDate);
        bool Save(AdminCart.Order order);
    }
}
