﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminCart;

namespace FMWebApp451.Interfaces
{
    public interface IOrderDetail
    {
        List<OrderDetail> GetById(int Id);
        List<OrderDetail> GetByDate(DateTime startDate, DateTime endDate);
        List<OrderDetail> GetAll();
        bool Save(OrderDetail orderDetail);

    }
}
