﻿using System;
using System.Collections.Generic;
using FM2015.Models;

namespace FMWebApp451.Interfaces
{
    public interface IOrdersWithDetailProvider
    {
        List<OrdersWithDetail> GetMany(DateTime startDate); //limited to the 1st 10K records
    }
}
