﻿using PayWhirl;

namespace FMWebApp451.Interfaces
{
    public interface IPayWhirlService
    {
        PayWhirlSubscriber FindSubscriberByEmail(string email);
    }
}
