﻿using AdminCart;
using System.Collections.Generic;

namespace FMWebApp451.Interfaces
{
    public interface IProductCostProvider
    {
        ProductCost GetById(int id);
        ProductCost GetByProduct(Product product);
        int Save(ProductCost productCost);
        List<ProductCost> GetAll();
    }
}
