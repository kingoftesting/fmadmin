﻿using System;
using System.Collections.Generic;
using FM2015.Models;
using AdminCart;

namespace FMWebApp451.Interfaces
{
    public interface IProductProvider
    {
        Product GetById(int id);
        Product GetBySku(string sku);
        Product GetByShopifyProductId(long id);
        int Save(Product product);
        List<Product> GetAll();
        List<Product> GetAll(bool customerOnly);
    }
}
