﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FM2015.Models;

namespace FMWebApp451.Interfaces
{
    public interface IRegistration
    {
        List<Registration> GetAll();
        Registration GetById(int id);
        int Save(Registration registration);
    }
}
