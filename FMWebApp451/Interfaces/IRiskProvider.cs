﻿using System;
using System.Collections.Generic;
using FM2015.Models;
using AdminCart;
using ShopifyAgent;

namespace FMWebApp451.Interfaces
{
    public interface IRiskProvider
    {
        List<ShopOrderRisk> GetAll();
        ShopOrderRisk GetByShopifyName(int shopifyName);
        int Save(ShopOrderRisk risk);
    }
}
