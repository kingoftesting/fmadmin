﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FM2015.Models;

namespace FMWebApp451.Interfaces
{
    public interface ISalesDetails
    {
        List<SalesDetails> GetAll(DateTime startDate, DateTime endDate);
    }
}
