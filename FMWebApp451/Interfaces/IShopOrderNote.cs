﻿using System.Collections.Generic;
using FM2015.Models;

namespace FMWebApp451.Interfaces
{
    public interface IShopOrderNote
    {
        List<ShopOrderNote> GetAll();
        ShopOrderNote GetByShopName(int shopName);
        void ClearCache();
    }
}
