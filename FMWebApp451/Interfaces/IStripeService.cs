﻿using System;
using System.Collections.Generic;
using Stripe;

namespace FMWebApp451.Interfaces
{
    public interface IStripeService
    {
        //******* CUSTOMER STUFF
        List<StripeCustomer> GetStripeCustomerList(string param, out string errMsg);
        StripeCustomer GetStripeCustomer(string id, out string errMsg);
        StripeCustomer CreateStripeCustomer(StripeCustomerCreateOptions stripeCustomerCreateOptions, out string errMsg);
        StripeCustomer UpdateStripeCustomer(string stripeCustomerId, StripeCustomerUpdateOptions stripeCustomerUpdateOptions, out string errMsg);

        //******* PLAN STUFF
        List<StripePlan> GetStripePlanList(out string errMsg);
        StripePlan GetStripePlanById(string planId, out string errMsg);
        StripePlan GetStripePlanByName(string planName, out string errMsg);
        StripePlan CreateStripePlan(StripePlanCreateOptions planOptions, out string errMsg);
        StripePlan UpdateStripePlan(string planId, StripePlanUpdateOptions planOptions, out string errMsg);
        bool DeleteStripePlan(string planId, out string errMsg);

        //******* SUBSCRIPTION STUFF
        List<StripeSubscription> GetCustomerSubscriptionList(string stipeCustomerID, out string errMsg);
        StripeSubscription CreateStripeSubscription(string stripeCustomerID, string stripePlanID, string StripeCouponID, string subscriptionErrMsg, out string errMsg);
        bool DeleteStripeSubscription(string stripeCustomerID, string stripeSubscriptionId, string subscriptionErrMsg, out string errMsg);

        //******* Token STUFF
        StripeToken CreateStripeToken(StripeTokenCreateOptions tokenOptions, out string errMsg);
        StripeToken GetStripeToken(string tokenId, out string errMsg);

        //******* CREDIT CARD STUFF
        List<StripeCard> GetStripeCustomerCards(string id, out string errMsg);
        StripeCard GetStripeCustomerCard(string stripeCustomerId, string stripeCardId, out string errMsg);
        StripeCard CreateStripeCard(string stripeCustomerId, StripeCardCreateOptions cardCreateOptions, out string errMsg);
        StripeCard UpdateStripeCard(string stripeCustomerId, string stripeCardId, StripeCardUpdateOptions cardCreateOptions, out string errMsg);
        bool DeleteStripeCard(string stripeCustomerId, string stripeCardId, out string errMsg);

        //******* CHARGE STUFF
        List<StripeCharge> GetStripeCustomerCharges(string id, out string errMsg);
        StripeCharge GetStripeCustomerCharge(string stripeChargeId, out string errMsg);
        StripeCharge CreateStripeCharge(StripeChargeCreateOptions stripeChargeCreateOptions, out string errMsg);
        StripeCharge UpdateStripeCharge(string stripeChargeId, StripeChargeUpdateOptions stripeChargeUpdateOptions, out string errMsg);


        //******* INVOICE STUFF
        List<StripeInvoice> GetStripeCustomerInvoices(string id, out string errMsg);


        // TO DO
        StripeCard CardService_Create(string customerId, StripeCardCreateOptions myCard);
        StripeCustomer CustomerService_Update(string customerId, StripeCustomerUpdateOptions myCustomer);

        StripeCustomer FindStripeCustomerByEmail(string email);
    }
}
