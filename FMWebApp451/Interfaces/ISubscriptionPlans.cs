﻿using System.Collections.Generic;
using FM2015.Models;

namespace FMWebApp451.Interfaces
{
    public interface ISubscriptionPlans
    {
        SubscriptionPlans GetByPlanId(string planId);
        List<SubscriptionPlans> GetAll();
    }
}
