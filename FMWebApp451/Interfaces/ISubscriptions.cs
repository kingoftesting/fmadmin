﻿using System.Collections.Generic;
using FM2015.Models;

namespace FMWebApp451.Interfaces
{
    public interface ISubscriptions
    {
        List<Subscriptions> GetByStripeCustomerId(string stripeCustomerId);
        List<Subscriptions> GetAll();
        bool Update(Subscriptions subscription);
        bool Insert(Subscriptions subscription);
    }
}
