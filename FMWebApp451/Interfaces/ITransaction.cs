﻿using System;
using System.Collections.Generic;
using AdminCart;

namespace FMWebApp451.Interfaces
{
    public interface ITransaction
    {
        List<Transaction> GetAll(DateTime startDate, DateTime endDate);
    }
}
