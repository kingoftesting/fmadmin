﻿using System.Collections.Generic;
using FM2015.Models;

namespace FMWebApp451.Interfaces
{
    public interface IValidationProvider
    {
        Validation GetById(int id);
        int Save(Validation validation);
        List<Validation> GetAll();
    }
}