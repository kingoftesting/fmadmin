using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPage3_Cart : System.Web.UI.MasterPage
{
 
    protected void Page_PreRender(object sender, EventArgs e)
    {

        string ssl = Request.ServerVariables["HTTPS"].ToString();

        if (ssl == "on")
        {
            GoogleAnalyticsScript.Text = @"
<script type='text/javascript' src='https://ssl.google-analytics.com/ga.js'></script>
<script type='text/javascript'>
var pageTracker = _gat._getTracker('UA-1560367-1');
pageTracker._initData();
pageTracker._trackPageview();
</script>";

        }
        else
        {
            GoogleAnalyticsScript.Text = @"
<script type='text/javascript' src='http://www.google-analytics.com/ga.js'></script>
<script type='text/javascript'>
var pageTracker = _gat._getTracker('UA-1560367-1');
pageTracker._initData();
pageTracker._trackPageview();
</script>";

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public static void SetRefreshValues(System.Web.UI.HtmlControls.HtmlHead head, string name, string content)
    {
        HtmlMeta metaValue = null;

        metaValue = new HtmlMeta();
        metaValue.Attributes.Add("http-equiv", name);
        metaValue.Attributes.Add("content", content);
        head.Controls.Add(metaValue);

        //return true;
    }
        

 
}
