﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using FM2015.Helpers;

namespace FM2015.Models
{
    public class BulkReturnsIBNU
    {
        /*
        CREATE TABLE [dbo].[BulkReturnsIBNU](
	        [ID] [int] IDENTITY(1,1) NOT NULL,
	        [AddDate] [datetime] NULL,
	        [AddBy] [nvarchar](100) NULL,
	        [AddByID] [int] NULL,
	        [Serial] [bigint] NULL,
	        [Test] [bit] NULL
        ) ON [PRIMARY]
        */

        private int id = 0;
        public int Id { get { return id; } set { id = value; } }

        private DateTime addDate = Convert.ToDateTime("1/1/1900");
        public DateTime AddDate { get { return addDate; } set { addDate = value; } }

        private string addBy = "";
        public string AddBy { get { return addBy; } set { addBy = value; } }

        private int addByID = 0;
        public int AddByID { get { return addByID; } set { addByID = value; } }

        private long serial = 0;
        [Required]
        [Display(Name = "Serial Number: ")]
        public long Serial { get { return serial; } set { serial = value; } }

        private bool test = true;
        public bool Test { get { return test; } set { test = value; } }

        public BulkReturnsIBNU()
        { }

        public BulkReturnsIBNU(int id, bool test)
        {
            string sql = @"
            SELECT [ID] 
                  ,[AddDate] 
                  ,[AddBy] 
                  ,[AddByID] 
                  ,[Serial] 
                  ,[Test] 
            FROM [SiteV2].[dbo].[BulkReturnsIBNU] 
            WHERE ID = @ID AND Test = @Test 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, id, test);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                id = Convert.ToInt32(dr["ID"]);
                addDate = Convert.ToDateTime(dr["AddDate"].ToString());
                addBy = dr["AddBy"].ToString();
                addByID = Convert.ToInt32(dr["AddByID"]);
                serial = Convert.ToInt64(dr["Serial"].ToString());
                test = Convert.ToBoolean(dr["Test"]);
            }
            if (dr != null) { dr.Close(); }
        }

        public int Save()
        {
            int result = 0;
            addBy = User.GetUserName();
            addDate = DateTime.Now;
            addByID = User.GetUserID();
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (id == 0)
            {
                string sql = @" 
                INSERT INTO BulkReturnsIBNU 
                    (AddDate, AddBy, AddByID, Serial, Test) 
                VALUES
                    (@AddDate, @AddBY, @AddByID, @Serial, @Test); 
                SELECT ID=@@identity;  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, addDate, addBy, addByID, serial, test);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ID"]);
                }
                else
                {
                    result = 99;
                }
            }
            else
            {
                string sql = @" 
                UPDATE Subscribers 
                SET  
                    ID = @ID,  
                    AddDate = @AddDate,  
                    AddBy = @AddBy,  
                    AddByID = @AddByID,  
                    Serial = @Serial,  
                    Test = @Test  
                WHERE ID = @ID  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, id, addDate, addBy, addByID, serial, test);
                DBUtil.Exec(sql, mySqlParameters);
            }
            return result;
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all records in BulkReturnsIBNU
        /// </summary>
        public static List<BulkReturnsIBNU> GetBulkReturnsIBNUList()
        {
            List<BulkReturnsIBNU> thelist = new List<BulkReturnsIBNU>();

            string sql = @" 
                SELECT * FROM BulkReturnsIBNU 
                ORDER BY addDate desc 
            ";

            SqlDataReader dr = DBUtil.FillDataReader(sql);

            while (dr.Read())
            {
                BulkReturnsIBNU obj = new BulkReturnsIBNU();

                obj.Id = Convert.ToInt32(dr["ID"]);
                obj.AddDate = Convert.ToDateTime(dr["AddDate"].ToString());
                obj.AddBy = dr["AddBy"].ToString();
                obj.AddByID = Convert.ToInt32(dr["AddByID"]);
                obj.Serial = Convert.ToInt64(dr["Serial"].ToString());
                obj.Test = Convert.ToBoolean(dr["Test"]);

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }
    }
}