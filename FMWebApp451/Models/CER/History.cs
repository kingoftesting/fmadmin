﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace FM2015.Models.mvcCER
{
    public class CERHistory
    {
        static private string CERConnString = AdminCart.Config.ConnStrCER();

        #region Private/Public Vars
        private int historyId;
        private string cERYear;
        private int cERIdentifier;
        private string cERSource;
        private DateTime dateReportReceived;
        private string cERType;
        private string receivedBy;
        private int complaintNumber;
        private string complaintDeterminationBy;
        private string customerID;
        private string pointOfPurchase;
        private string firstName;
        private string lastName;
        private string street;
        private string city;
        private string state;
        private string zip;
        private string country;
        private string email;
        private string phone;
        private string productNumber;
        private string serialNumber;
        private string orderNumber;
        private string eventTiming;
        private string eventDescription;
        private string actionExplaination;
        private string statusCs;
        private string statusQa;
        private string statusMed;
        private DateTime insertDate;
        private string insertBy;
        private string investigationSummary;
        private bool managerApproved;
        private string medicalCondition;
        private bool mdr;
        private bool analyze;

        public string CERNumber { get { return CERYear + "000000".Substring(CERIdentifier.ToString().Length) + CERIdentifier.ToString(); } }

        public int HistoryId { get { return historyId; } set { historyId = value; } }
        public string CERYear { get { return cERYear; } set { cERYear = value; } }
        public int CERIdentifier { get { return cERIdentifier; } set { cERIdentifier = value; } }
        public string CERSource { get { return cERSource; } set { cERSource = value; } }
        public DateTime DateReportReceived { get { return dateReportReceived; } set { dateReportReceived = value; } }
        public string CERType { get { return cERType; } set { cERType = value; } }
        public string ReceivedBy { get { return receivedBy; } set { receivedBy = value; } }
        public int ComplaintNumber { get { return complaintNumber; } set { complaintNumber = value; } }
        public string ComplaintDeterminationBy { get { return complaintDeterminationBy; } set { complaintDeterminationBy = value; } }
        public string CustomerID { get { return customerID; } set { customerID = value; } }
        public string PointOfPurchase { get { return pointOfPurchase; } set { pointOfPurchase = value; } }
        public string FirstName { get { return firstName; } set { firstName = value; } }
        public string LastName { get { return lastName; } set { lastName = value; } }
        public string Street { get { return street; } set { street = value; } }
        public string City { get { return city; } set { city = value; } }
        public string State { get { return state; } set { state = value; } }
        public string Zip { get { return zip; } set { zip = value; } }
        public string Email { get { return email; } set { email = value; } }
        public string Country { get { return country; } set { country = value; } }
        public string Phone { get { return phone; } set { phone = value; } }
        public string ProductNumber { get { return productNumber; } set { productNumber = value; } }
        public string SerialNumber { get { return serialNumber; } set { serialNumber = value; } }
        public string OrderNumber { get { return orderNumber; } set { orderNumber = value; } }
        public string EventTiming { get { return eventTiming; } set { eventTiming = value; } }
        public string EventDescription { get { return eventDescription; } set { eventDescription = value; } }
        public string ActionExplaination { get { return actionExplaination; } set { actionExplaination = value; } }
        public string StatusCs { get { return statusCs; } set { statusCs = value; } }
        public string StatusQa { get { return statusQa; } set { statusQa = value; } }
        public string StatusMed { get { return statusMed; } set { statusMed = value; } }
        public DateTime InsertDate { get { return insertDate; } set { insertDate = value; } }
        public string InsertBy { get { return insertBy; } set { insertBy = value; } }
        public string InvestigationSummary { get { return investigationSummary; } set { investigationSummary = value; } }
        public bool ManagerApproved { get { return managerApproved; } set { managerApproved = value; } }
        public string MedicalCondition { get { return medicalCondition; } set { medicalCondition = value; } }
        public bool Mdr { get { return mdr; } set { mdr = value; } }
        public bool Analyze { get { return analyze; } set { analyze = value; } }
        #endregion Private/Public Vars


        #region Constructors
        public CERHistory()
        {
            historyId = 0;
            cERYear = "2007";
            cERIdentifier = 1;
            cERSource = ""; //FM Email, Zanco Order, CER System

            dateReportReceived = DateTime.Now;
            cERType = "";
            receivedBy = "";
            complaintNumber = 0;
            complaintDeterminationBy = "";
            customerID = "0";
            orderNumber = "";
            pointOfPurchase = "";
            firstName = "";
            lastName = "";
            street = "";
            city = "";
            state = "";
            zip = "";
            country = "";
            email = "";
            phone = "";
            productNumber = "";
            serialNumber = "";
            eventTiming = "";
            eventDescription = "";
            actionExplaination = "";
            statusCs = "";
            statusQa = "";
            statusMed = "";
            medicalCondition = "";
            insertDate = DateTime.Now;
            insertBy = "";
            investigationSummary = "";
            managerApproved = false;
            mdr = false;
            analyze = false;
        }
        #endregion

        #region Static Methods
        public static List<CERHistory> GetHistoryList(string CERNumber)
        {
            //parse CER into two parts: Year and CerID
            string _CERYear = CERNumber.Substring(0, 4);
            string _CERIdentifier = CERNumber.Substring(4);

            List<CERHistory> cerHistoryList = new List<CERHistory>();

            string sql = @"
                    SELECT *
                    FROM CER2History 
                    WHERE CERYear = @CERYear AND CERIdentifier = @CERIdentifier                                                           
                    ORDER BY InsertDate DESC  
                        ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, _CERYear, _CERIdentifier);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);

            while (dr.Read())
            {
                CERHistory obj = new CERHistory();

                obj.cERYear = dr["CERYear"].ToString();
                obj.cERIdentifier = Convert.ToInt32(dr["CERIdentifier"]);
                obj.pointOfPurchase = dr["PointOfPurchase"].ToString();
                obj.cERSource = dr["CERSource"].ToString();
                obj.DateReportReceived = DateTime.Parse(dr["DateReportReceived"].ToString());
                obj.cERType = dr["CerType"].ToString().Trim();
                obj.receivedBy = dr["ReceivedBy"].ToString();
                obj.complaintNumber = Convert.ToInt32(dr["ComplaintNumber"]);
                obj.complaintDeterminationBy = dr["ComplaintDeterminationBy"].ToString();
                obj.customerID = dr["CustomerID"].ToString();

                obj.firstName = dr["FirstName"].ToString();
                obj.lastName = dr["Lastname"].ToString();
                obj.street = dr["Street"].ToString();
                obj.city = dr["City"].ToString();
                obj.state = dr["State"].ToString();
                obj.zip = dr["Zip"].ToString();
                obj.country = dr["country"].ToString();
                obj.email = dr["Email"].ToString();
                obj.phone = dr["Phone"].ToString();
                obj.productNumber = dr["ProductNumber"].ToString();
                obj.serialNumber = dr["SerialNumber"].ToString();
                obj.orderNumber = dr["OrderNumber"].ToString();
                obj.eventTiming = dr["EventTiming"].ToString();
                obj.eventDescription = dr["EventDescription"].ToString();
                obj.actionExplaination = dr["ActionExplaination"].ToString();
                obj.statusCs = dr["StatusCs"].ToString();
                obj.statusQa = dr["StatusQa"].ToString();
                obj.statusMed = dr["StatusMed"].ToString();
                obj.medicalCondition = dr["MedicalCondition"].ToString();
                obj.InsertDate = DateTime.Parse(dr["InsertDate"].ToString());
                obj.InsertBy = dr["InsertBy"].ToString();
                obj.investigationSummary = dr["InvestigationSummary"].ToString();
                //obj.managerApproved = bool.Parse(dr["ManagerApproved"].ToString());
                obj.mdr = bool.Parse(dr["Mdr"].ToString());
                obj.analyze = bool.Parse(dr["Analyze"].ToString());

                cerHistoryList.Add(obj);
            }
            if (dr != null) { dr.Close(); }

            return cerHistoryList;
        }
        #endregion
    }
}