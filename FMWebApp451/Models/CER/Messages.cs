﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace FM2015.Models.mvcCER
{
    public class CERMessages
    {
        static private string CERConnString = AdminCart.Config.ConnStrCER();

        #region Private/Public Vars
        private int messageID;
        public int MessageID { get { return messageID; } set { messageID = value; } }
        private string cERYear;
        public string CERYear { get { return cERYear; } set { cERYear = value; } }
        private int cERIdentifier;
        public int CERIdentifier { get { return cERIdentifier; } set { cERIdentifier = value; } }
        private DateTime dated;
        public DateTime Dated { get { return dated; } set { dated = value; } }
        private string type;
        public string Type { get { return type; } set { type = value; } }
        private string message;
        public string Message { get { return message; } set { message = value; } }
        private DateTime addDate;
        public DateTime AddDate { get { return addDate; } set { addDate = value; } }
        private string addBy;
        public string AddBy { get { return addBy; } set { addBy = value; } }
        #endregion

        public CERMessages()
        {
            messageID = 0;
            cERYear = "n/a";
            cERIdentifier = 0;
            dated = DateTime.MinValue;
            type = "n/a";
            message = "n/a";
            addDate = DateTime.MinValue;
            addBy = "n/a";
        }

        public CERMessages(int messageID)
        {

            string sql = @" 
                SELECT * 
                FROM  MessagesCER 
                WHERE MessageID = @MessageID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, messageID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);
            if (dr.Read())
            {
                messageID = Convert.ToInt32(dr["MessageID"]);
                cERYear = dr["CERYear"].ToString();
                cERIdentifier = Convert.ToInt32(dr["CERIdentifier"].ToString());
                dated = Convert.ToDateTime(dr["Dated"].ToString());
                type = dr["Type"].ToString();
                message = dr["Message"].ToString();
                addDate = Convert.ToDateTime(dr["AddDate"].ToString());
                addBy = dr["AddBy"].ToString();
            }
            if (dr != null) { dr.Close(); }
        }

        public CERMessages(string CERNumber)
        {
            string _CERYear = CERNumber.Substring(0, 4);
            string _CERIdentifier = CERNumber.Substring(4);

            string sql = @" 
                SELECT * 
                FROM  MessagesCER 
                WHERE CERYear = @CERYear AND CERIdentifier = @CERIdentifier 
            ";


            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, _CERYear, _CERIdentifier);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);
            if (dr.Read())
            {
                messageID = Convert.ToInt32(dr["MessageID"]);
                cERYear = dr["CERYear"].ToString();
                cERIdentifier = Convert.ToInt32(dr["CERIdentifier"].ToString());
                dated = Convert.ToDateTime(dr["Dated"].ToString());
                type = dr["Type"].ToString();
                message = dr["Message"].ToString();
                addDate = Convert.ToDateTime(dr["AddDate"].ToString());
                addBy = dr["AddBy"].ToString();
            }
            if (dr != null) { dr.Close(); }
        }

        public int Save(int MessageID)
        {
            int result = 0;
            string userName = User.GetUserName();
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (MessageID == 0)
            {
                string sql = @" 
                INSERT INTO MessagesCER 
                (CERYear, CERIdentifier, Dated, Type, Message, AddDate, AddBy) 
                VALUES(@CERYear, @CERIdentifier, @Dated, @Type, @Message, @AddDate, @AddBy); 
                SELECT ID=@@identity;  
                ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, CERYear, CERIdentifier, Dated, Type, Message, DateTime.Now, userName);
                dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ID"]);
                }
                else
                {
                    result = 99;
                }
            }
            else
            {
                string sql = @" 
                UPDATE MessagesCER 
                SET  
                    CERYear = @CERYear,  
                    CERIdentifier = @CERIdentifier,  
                    Dated = @Dated,  
                    Type = @Type,  
                    Message = @Message, 
                    AddDate = @AddDate,   
                    AddBy = @AddBy  
                    WHERE MessageID = @MessageID  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, CERYear, CERIdentifier, Dated, Type, Message, DateTime.Now, userName, MessageID);
                DBUtil.Exec(sql, CERConnString, mySqlParameters);
            }
            return result;
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all CERMessages
        /// </summary>
        public static List<CERMessages> GetCERMessagesList()
        {
            List<CERMessages> thelist = new List<CERMessages>();

            string sql = @" 
                SELECT Top 1000 FROM MessagesCER 
                ORDER BY Dated DESC 
            ";

            SqlDataReader dr = DBUtil.FillDataReader(sql, CERConnString);

            while (dr.Read())
            {
                CERMessages obj = new CERMessages();

                obj.MessageID = Convert.ToInt32(dr["MessageID"]);
                obj.CERYear = dr["CERYear"].ToString();
                obj.CERIdentifier = Convert.ToInt32(dr["CERIdentifier"].ToString());
                obj.Dated = Convert.ToDateTime(dr["Dated"].ToString());
                obj.Type = dr["Type"].ToString();
                obj.Message = dr["Message"].ToString();
                obj.AddDate = Convert.ToDateTime(dr["AddDate"].ToString());
                obj.AddBy = dr["AddBy"].ToString();

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a collection with all CERMessages
        /// </summary>
        public static List<CERMessages> GetCERMessagesList(string CERNumber)
        {
            List<CERMessages> thelist = new List<CERMessages>();

            string _CERYear = CERNumber.Substring(0, 4);
            string _CERIdentifier = CERNumber.Substring(4);

            string sql = @" 
                SELECT * 
                FROM MessagesCER 
                WHERE CERYear = @CERYear AND CERIdentifier = @CERIdentifier 
                ORDER BY Dated DESC 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, _CERYear, _CERIdentifier);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);

            while (dr.Read())
            {
                CERMessages obj = new CERMessages();

                obj.MessageID = Convert.ToInt32(dr["MessageID"]);
                obj.CERYear = dr["CERYear"].ToString();
                obj.CERIdentifier = Convert.ToInt32(dr["CERIdentifier"].ToString());
                obj.Dated = Convert.ToDateTime(dr["Dated"].ToString());
                obj.Type = dr["Type"].ToString();
                obj.Message = dr["Message"].ToString();
                obj.AddDate = Convert.ToDateTime(dr["AddDate"].ToString());
                obj.AddBy = dr["AddBy"].ToString();

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a collection with all CERMessages
        /// </summary>
        public static List<CERMessages> SearchCERMEssagesList(string searchParam)
        {
            List<CERMessages> thelist = new List<CERMessages>();

            string sql = @" 
                SELECT * 
                FROM MessagesCER 
                WHERE Message LIKE @Message 
                ORDER BY Dated DESC 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, "%"+ searchParam + "%");
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, CERConnString);

            while (dr.Read())
            {
                CERMessages obj = new CERMessages();

                obj.MessageID = Convert.ToInt32(dr["MessageID"]);
                obj.CERYear = dr["CERYear"].ToString();
                obj.CERIdentifier = Convert.ToInt32(dr["CERIdentifier"].ToString());
                obj.Dated = Convert.ToDateTime(dr["Dated"].ToString());
                obj.Type = dr["Type"].ToString();
                obj.Message = dr["Message"].ToString();
                obj.AddDate = Convert.ToDateTime(dr["AddDate"].ToString());
                obj.AddBy = dr["AddBy"].ToString();

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a className object with the specified MessageID
        /// </summary>

        public static CERMessages GetCERMessagesByID(int messageID)
        {
            CERMessages obj = new CERMessages(messageID);
            return obj;
        }
        /// <summary>
        /// Returns a className object with the specified CERNumber
        /// </summary>

        public static CERMessages GetCERMessagesByCERNum(string CERNumber)
        {
            CERMessages obj = new CERMessages(CERNumber);
            return obj;
        }

        /// <summary>
        /// Updates an existing CERMessages
        /// </summary>
        public static bool UpdateCERMessages(int messageID,
        string ceryear, int ceridentifier, DateTime dated, string type, string message, DateTime addDate, string addby)
        {
            CERMessages obj = new CERMessages();

            obj.MessageID = messageID;
            obj.CERYear = ceryear;
            obj.CERIdentifier = ceridentifier;
            obj.Dated = dated;
            obj.Type = type;
            obj.Message = message;
            obj.addDate = addDate;
            obj.AddBy = addby;
            int ret = obj.Save(obj.MessageID);
            return (ret > 0) ? true : false;
        }

        /// <summary>
        /// Creates a new CERMessages
        /// </summary>
        public static int InsertCERMessages(
        string ceryear, int ceridentifier, DateTime dated, string type, string message, DateTime addDate, string addby)
        {
            CERMessages obj = new CERMessages();

            obj.MessageID = 0; //make really sure this is an INSERT
            obj.CERYear = ceryear;
            obj.CERIdentifier = ceridentifier;
            obj.Dated = dated;
            obj.Type = type;
            obj.Message = message;
            obj.AddBy = addby;
            int ret = obj.Save(0);
            return ret;
        }
    }
}