﻿using System;
using System.Collections.Generic;
using FMWebApp451.ViewModels.Stripe;
using FMWebApp451.Interfaces;
using Stripe;
using AdminCart;

namespace FM2015.Models
{
    public class CreditCard : ICreditCard
    {
        private readonly ICustomer customerProvider;
        private readonly IAddress addressProvider;
        private readonly IPayWhirlService payWhirlService;
        private readonly IStripeService stripeService;
        private readonly ICER cerService;

        public CreditCard(ICustomer customerProvider, IAddress addressProvider, IPayWhirlService payWhirlService,
            IStripeService stripeService, ICER cerService)
        {
            this.customerProvider = customerProvider;
            this.addressProvider = addressProvider;
            this.payWhirlService = payWhirlService;
            this.stripeService = stripeService;
            this.cerService = cerService;
        }

        public string AddNewCard(CreditCardViewModel ccvm)
        {
            string errMsg = string.Empty;
            if (ccvm.CreditCardViewModelKey <= 0)
            {
                return @"Customer information is unavailable. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";
            }
            ccvm.customer = customerProvider.GetById(ccvm.CreditCardViewModelKey);
            if (ccvm.customer.CustomerID == 0)
            {
                return @"Customer information is unavailable. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";
            }

            // set these properties if passing full card details (do not
            Address a = addressProvider.GetByCustomerId(ccvm.customer.CustomerID, 1);
            StripeCustomer stripeCustomer = stripeService.FindStripeCustomerByEmail(ccvm.customer.Email);
            if (string.IsNullOrEmpty(stripeCustomer.Id))
            {
                return @"Could not find customer email in subscription system. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";
            }

            ccvm.StripeCustomerID = stripeCustomer.Id;

            //get a credit card token for this subscription purchase
            var myCard = new StripeCardCreateOptions(); //create a token from the customers card
            myCard.SourceCard = new SourceCard(); //**********
            myCard.SourceCard.Name = ccvm.Name;
            myCard.SourceCard.Number = ccvm.CardNumber;
            myCard.SourceCard.ExpirationYear = Convert.ToInt32(ccvm.ExpirationYear);
            myCard.SourceCard.ExpirationMonth = Convert.ToInt32(ccvm.ExpirationMonth);
            myCard.SourceCard.Cvc = ccvm.Cvc;
            myCard.SourceCard.AddressLine1 = a.Street;
            myCard.SourceCard.AddressCity = a.City;
            myCard.SourceCard.AddressState = a.State;
            myCard.SourceCard.AddressCountry = a.Country;
            myCard.SourceCard.AddressZip = a.Zip;

            //var cardService = new StripeCardService();
            StripeCard stripeCard = stripeService.CardService_Create(ccvm.StripeCustomerID, myCard);
            if (string.IsNullOrEmpty(stripeCard.Id))
            {
                {
                    return @"Credit Card was declined. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";
                }
            }

            //begin creating CER
            adminCER.CER cer = cerService.GetByCustomer(ccvm.customer);
            cer.EventDescription = "Added new credit card: ";
            cer.EventDescription += "**** **** **** " + ccvm.CardNumber.Substring(12) + " to Stripe Account";

            stripeCustomer = stripeService.GetStripeCustomer(ccvm.StripeCustomerID, out errMsg);
            if (string.IsNullOrEmpty(stripeCustomer.Id))
            {
                {
                    return @"Subscription Customer information not accessible. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";
                }
            }

            var myCustomer = new StripeCustomerUpdateOptions();
            myCustomer.DefaultSource = stripeCard.Id;  //****************
            stripeCustomer = stripeService.CustomerService_Update(ccvm.StripeCustomerID, myCustomer);
            if (string.IsNullOrEmpty(stripeCustomer.Id))
            {
                {
                    return @"New card addition not successful. Please try again later. 
                    If this problem persists please give us a call at 1-800-770-2521 (M-F, 9am-5pm Pacific Time)";
                }
            }

            if (string.IsNullOrEmpty(errMsg))
            {
                errMsg = "Credit Card Update Successfully!";
                cer.EventDescription += errMsg;
                int result = cerService.Add(cer);
            }

            return errMsg;
        }

        public StripeCustomer FindStripeCustomerByEmail(string email)
        {
            StripeCustomer s = new StripeCustomer();

            PayWhirl.PayWhirlSubscriber pwSubScriber = new PayWhirl.PayWhirlSubscriber();
            try
            {
                //pwSubScriber = PayWhirl.PayWhirlSubscriber.FindSubscriberByEmail(email);
                pwSubScriber = PayWhirl.PayWhirlBiz.FindSubscriberByEmail(email);
            }
            catch (Exception ex)
            {
                //some exception handling stuff here
                return s;
            }

            if (pwSubScriber.Id == 0) //wasn't located in the PayWhirl world
            {
                return s;
            }

            //get the customer from Stripe
            var customerService = new StripeCustomerService();
            try
            {
                s = customerService.Get(pwSubScriber.Stripe_id);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "AddCard: Get StripeCustomer: Stripe Error");
                return s;
            }

            return s;
        }
    }
}