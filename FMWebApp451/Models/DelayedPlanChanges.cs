﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FM2015.Helpers;
using Stripe;
using FMWebApp451.Services.StripeServices;

namespace FM2015.Models
{
    public class DelayedPlanChanges
    {
        #region Private/Public Vars
        private int id;
        public int ID { get { return id; } set { id = value; } }
        private int customerID;
        public int CustomerID { get { return customerID; } set { customerID = value; } }
        private string lastName;
        public string LastName { get { return lastName; } set { lastName = value; } }
        private string stripeCustomerID;
        public string StripeCustomerID { get { return stripeCustomerID; } set { stripeCustomerID = value; } }
        private string newStripePlanID;
        public string NewStripePlanID { get { return newStripePlanID; } set { newStripePlanID = value; } }
        private string oldStripePlanID;
        public string OldStripePlanID { get { return oldStripePlanID; } set { oldStripePlanID = value; } }
        private DateTime startDate;
        public DateTime StartDate { get { return startDate; } set { startDate = value; } }
        private string promoCode;
        public string PromoCode { get { return promoCode; } set { promoCode = value; } }
        private bool activated;
        public bool Activated { get { return activated; } set { activated = value; } }
        private bool deleted;
        public bool Deleted { get { return deleted; } set { deleted = value; } }
        private string addedBy;
        public string AddedBy { get { return addedBy; } set { addedBy = value; } }
        private DateTime addedDate;
        public DateTime AddedDate { get { return addedDate; } set { addedDate = value; } }

        #endregion

        public DelayedPlanChanges()
        {
            this.ID = 0;
            this.CustomerID = 0;
            this.LastName = "n/a";
            this.StripeCustomerID = "n/a";
            this.NewStripePlanID = "n/a";
            this.OldStripePlanID = "n/a";
            this.StartDate = Convert.ToDateTime("1/1/1900");
            this.PromoCode = "n/a";
            this.Activated = false;
            this.Deleted = false;
            this.AddedBy = "";
            this.AddedDate = Convert.ToDateTime("1/1/1900");
        }

        public DelayedPlanChanges(int ID)
        {
            string sql = @" 
            SELECT * FROM  DelayedPlanChanges 
            WHERE ID = @ID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, ID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                this.ID = Convert.ToInt32(dr["ID"]);
                this.CustomerID = Convert.ToInt32(dr["CustomerID"].ToString());
                this.LastName = dr["LastName"].ToString();
                this.StripeCustomerID = dr["StripeCustomerID"].ToString();
                this.NewStripePlanID = dr["NewStripePlanID"].ToString();
                this.OldStripePlanID = dr["OldStripePlan"].ToString();
                this.StartDate = Convert.ToDateTime(dr["StartDate"].ToString());
                this.PromoCode = dr["PromoCode"].ToString();
                this.Activated = Convert.ToBoolean(dr["Activated"]);
                this.Deleted = Convert.ToBoolean(dr["Deleted"]);
                this.AddedBy = dr["AddedBy"].ToString();
                this.AddedDate = Convert.ToDateTime(dr["AddedDate"].ToString());
            }
            if (dr != null) { dr.Close(); }
        }

        public int Save(int ID)
        {
            int result = -1;
            if (string.IsNullOrEmpty(PromoCode))
            {
                PromoCode = "n/a"; //don't insert an empty string
            }

            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (ID == 0)
            {
                string sql = @" 
                INSERT INTO DelayedPlanChanges 
                    (CustomerID, LastName, StripeCustomerID, NewStripePlanID, OldStripePlan, StartDate, 
                     PromoCode, Activated, Deleted, AddedBy, AddedDate) 
                VALUES
                    (@CustomerID, @StripeCustomerID, @LastName, @NewStripePlanID, @OldStripePlan, @StartDate, 
                     @PromoCode, @Activated, @Deleted, @AddedBy, @AddedDate); 
                SELECT ID=@@identity;  
                ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, CustomerID, LastName, StripeCustomerID, 
                    NewStripePlanID, OldStripePlanID, StartDate, PromoCode, Activated, Deleted, AddedBy, AddedDate);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ID"]);
                }

            }
            else
            {
                string sql = @" 
                UPDATE DelayedPlanChanges 
                SET  
                    CustomerID = @CustomerID,  
                    LastName = @LastName,  
                    StripeCustomerID = @StripeCustomerID,  
                    NewStripePlanID = @NewStripePlanID,  
                    OldStripePlan = @OldStripePlan,  
                    StartDate = @StartDate, 
                    PromoCode = @PromoCode,   
                    Activated = @Activated, 
                    Deleted = @Deleted, 
                    AddedBy = @AddedBy, 
                    AddedDate = @AddedDate  
                WHERE ID = @ID  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, CustomerID, LastName, StripeCustomerID, 
                    NewStripePlanID, OldStripePlanID, StartDate, PromoCode, Activated, Deleted, AddedBy, AddedDate, ID);
                DBUtil.Exec(sql, mySqlParameters);
                result = 0;
            }
            return result;
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all DelayedPlanChanges
        /// </summary>
        public static List<DelayedPlanChanges> GetDelayedPlanChangesList()
        {
            List<DelayedPlanChanges> thelist = new List<DelayedPlanChanges>();

            string sql = @" 
            SELECT * FROM DelayedPlanChanges 
            ORDER BY StartDate ASC 
            ";

            SqlDataReader dr = DBUtil.FillDataReader(sql);

            while (dr.Read())
            {
                DelayedPlanChanges obj = new DelayedPlanChanges();

                obj.ID = Convert.ToInt32(dr["ID"]);
                obj.CustomerID = Convert.ToInt32(dr["CustomerID"].ToString());
                obj.LastName = dr["LastName"].ToString();
                obj.StripeCustomerID = dr["StripeCustomerID"].ToString();
                obj.NewStripePlanID = dr["NewStripePlanID"].ToString();
                obj.OldStripePlanID = dr["OldStripePlan"].ToString();
                obj.StartDate = Convert.ToDateTime(dr["StartDate"].ToString());
                obj.PromoCode = dr["PromoCode"].ToString();
                obj.Activated = Convert.ToBoolean(dr["Activated"]);
                obj.Deleted = Convert.ToBoolean(dr["Deleted"]);
                obj.AddedBy = dr["AddedBy"].ToString();
                obj.AddedDate = Convert.ToDateTime(dr["AddedDate"].ToString());

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a collection with all DelayedPlanChanges
        /// </summary>
        public static List<DelayedPlanChanges> GetDelayedPlanChangesList(int CustomerID)
        {
            List<DelayedPlanChanges> thelist = new List<DelayedPlanChanges>();

            string sql = @" 
            SELECT * FROM DelayedPlanChanges 
            WHERE CustomerID = @CustomerID AND Deleted = 0 
            ORDER BY StartDate ASC 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, CustomerID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                DelayedPlanChanges obj = new DelayedPlanChanges();

                obj.ID = Convert.ToInt32(dr["ID"]);
                obj.CustomerID = Convert.ToInt32(dr["CustomerID"].ToString());
                obj.LastName = dr["LastName"].ToString();
                obj.StripeCustomerID = dr["StripeCustomerID"].ToString();
                obj.NewStripePlanID = dr["NewStripePlanID"].ToString();
                obj.OldStripePlanID = dr["OldStripePlan"].ToString();
                obj.StartDate = Convert.ToDateTime(dr["StartDate"].ToString());
                obj.PromoCode = dr["PromoCode"].ToString();
                obj.Activated = Convert.ToBoolean(dr["Activated"]);
                obj.Deleted = Convert.ToBoolean(dr["Deleted"]);
                obj.AddedBy = dr["AddedBy"].ToString();
                obj.AddedDate = Convert.ToDateTime(dr["AddedDate"].ToString());

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a className object with the specified ID
        /// </summary>

        public static DelayedPlanChanges GetDelayedPlanChangesByID(int ID)
        {
            DelayedPlanChanges obj = new DelayedPlanChanges(ID);
            return obj;
        }

        /// <summary>
        /// Updates an existing DelayedPlanChanges
        /// </summary>
        public static bool UpdateDelayedPlanChanges(int id,
        int customerid, string lastname, string stripecustomerid, string newstripeplanid, string oldstripeplanid, 
        DateTime startdate, string promoCode, AdminCart.Cart cart, Boolean activated, 
        Boolean deleted, string addedBy, DateTime addedDate)
        {
            DelayedPlanChanges obj = new DelayedPlanChanges();

            obj.ID = id;
            obj.CustomerID = customerid;
            obj.StripeCustomerID = stripecustomerid;
            obj.NewStripePlanID = newstripeplanid;
            obj.OldStripePlanID = oldstripeplanid;
            obj.StartDate = startdate;
            obj.PromoCode = promoCode;
            obj.Activated = activated;
            obj.Deleted = deleted;
            obj.AddedBy = addedBy;
            obj.AddedDate = addedDate;

            int ret = obj.Save(obj.ID);

            adminCER.CER cer = new adminCER.CER(cart);
            if (ret > 0)
            {
                cer.EventDescription = "Successful Delayed Subscription Plan Update " + Environment.NewLine;
                cer.EventDescription += "Customer ID: " + customerid.ToString() + Environment.NewLine;
                cer.EventDescription += "LastName: " + lastname + Environment.NewLine;
                cer.EventDescription += "Stripe Customer ID: " + stripecustomerid + Environment.NewLine;
                cer.EventDescription += "New Stripe Plan ID: " + newstripeplanid + Environment.NewLine;
                cer.EventDescription += "Old Stripe Plan ID: " + oldstripeplanid + Environment.NewLine;
                cer.EventDescription += "Plan Update By: " + cer.ReceivedBy + Environment.NewLine;
                cer.ActionExplaination = "Update Delayed Subscription Plan";
                int result = cer.Add();
            }
            return (ret > 0) ? true : false;
        }

        /// <summary>
        /// Creates a new DelayedPlanChanges
        /// </summary>
        public static int InsertDelayedPlanChanges(
        int customerid, string lastname, string stripecustomerid, string newstripeplanid, string oldstripeplanid, 
        DateTime startdate, string promoCode, AdminCart.Cart cart, Boolean activated)
        {
            DelayedPlanChanges obj = new DelayedPlanChanges();

            obj.CustomerID = customerid;
            obj.LastName = lastname;
            obj.StripeCustomerID = stripecustomerid;
            obj.NewStripePlanID = newstripeplanid;
            obj.OldStripePlanID = oldstripeplanid;
            obj.StartDate = startdate;
            obj.PromoCode = promoCode;
            obj.Activated = activated;
            obj.Deleted = false;
            obj.AddedBy = User.GetUserName();
            obj.AddedDate = DateTime.Now;
            int ret = obj.Save(0);

            adminCER.CER cer = new adminCER.CER(cart);
            if (ret > 0)
            {
                cer.EventDescription = "Successful Delayed Subscription Plan Insert " + Environment.NewLine;
                cer.EventDescription += "Customer ID: " + customerid.ToString() + Environment.NewLine;
                cer.EventDescription += "LastName: " + lastname + Environment.NewLine;
                cer.EventDescription += "Stripe Customer ID: " + stripecustomerid + Environment.NewLine;
                cer.EventDescription += "New Stripe Plan ID: " + newstripeplanid + Environment.NewLine;
                cer.EventDescription += "Old Stripe Plan ID: " + oldstripeplanid + Environment.NewLine;
                cer.EventDescription += "Plan Change By: " + cer.ReceivedBy + Environment.NewLine;
                cer.ActionExplaination = "Insert New Delayed Subscription Plan";
                int result = cer.Add();
            }

            return ret;
        }

        /// <summary>
        /// Activate (creates a new subscription) for a DelayedPlanChange
        /// </summary>
        public static string Activate(DelayedPlanChanges dpc, bool test)
        {
            if (dpc.ID == 0)
            {
                return "failed: DelayedPlanChanges: Activate Deferred Subscription Failed: ID = 0";
            }

            string errMsg = string.Empty;
            string subscriptionErrMsg = "DelayedPlanChanges: Activate Deferred Subscription Failed: Stripe Error";
            dpc.PromoCode = (dpc.PromoCode.ToUpper() == "N/A") ? string.Empty : dpc.PromoCode;
            StripeSubscription stripeSubscription = new StripeSubscription();
            if (!test)
            {
                StripeServices stripeServices = new StripeServices();
                stripeSubscription = stripeServices.CreateStripeSubscription(dpc.StripeCustomerID, dpc.NewStripePlanID, dpc.PromoCode, subscriptionErrMsg, out errMsg);
            }

            if (string.IsNullOrEmpty(errMsg))
            {
                dpc.Activated = true;
                dpc.AddedBy = User.GetUserName();
                dpc.AddedDate = DateTime.Now;
                if (!test)
                {
                    if (dpc.Save(dpc.ID) < 0)
                    {
                        return "failed: DelayedPlanChanges: Activate Deferred Subscription Failed: updating .Activated";
                    }
                }

                AdminCart.Customer c = new AdminCart.Customer(dpc.CustomerID);
                adminCER.CER cer = new adminCER.CER(c)
                {
                    EventDescription = (test) ? "TEST: " : ""
                };
                cer.EventDescription += "Successful Activation of Deferred Subscription " + Environment.NewLine;
                cer.EventDescription += "Customer ID: " + c.CustomerID.ToString() + Environment.NewLine;
                cer.EventDescription += "LastName: " + c.LastName + Environment.NewLine;
                cer.EventDescription += "Stripe Customer ID: " + dpc.StripeCustomerID + Environment.NewLine;
                cer.EventDescription += "New Stripe Plan ID: " + dpc.NewStripePlanID + Environment.NewLine;
                cer.EventDescription += "Old Stripe Plan ID: " + dpc.OldStripePlanID + Environment.NewLine;
                cer.EventDescription += "Subscription Activated By: " + cer.ReceivedBy + Environment.NewLine;
                cer.ActionExplaination = "Activate Deferred Subscription Plan";
                int result = cer.Add();

                string msg = "Deferred Subscription Processing: Activate" + Environment.NewLine;
                msg += "Customer name: " + c.FirstName + " " + c.LastName + Environment.NewLine;
                msg += "Customer email: " + c.Email + Environment.NewLine;
                mvc_Mail.SendMail(AdminCart.Config.MailTo, AdminCart.Config.MailFrom(), "", "", "Activating Deferred Subscription", msg, true);

                return "success";
            }
            else
            {
                return "failed: DelayedPlanChanges: CreatSubscription @ StripeBiz";
            }
        }

        /// <summary>
        /// Delete () for a DelayedPlanChange
        /// </summary>
        public static string Delete(DelayedPlanChanges dpc, bool test)
        {
            if (dpc.ID == 0)
            {
                return "failed: DelayedPlanChanges: Delete Deferred Subscription Failed: ID = 0";
            }

            dpc.Deleted = true;
            dpc.AddedBy = User.GetUserName(); ;
            dpc.AddedDate = DateTime.Now;

            if (!test)
            {
                if (dpc.Save(dpc.ID) < 0)
                {
                    return "failed: DelayedPlanChanges: Delete Deferred Subscription Failed: updating .Deleted";
                }
            }

            AdminCart.Customer c = new AdminCart.Customer(dpc.CustomerID);
            adminCER.CER cer = new adminCER.CER(c)
            {
                EventDescription = (test) ? "TEST: " : ""
            };
            cer.EventDescription += "Successful Deletion of Deferred Subscription " + Environment.NewLine;
            cer.EventDescription += "Customer ID: " + c.CustomerID.ToString() + Environment.NewLine;
            cer.EventDescription += "LastName: " + c.LastName + Environment.NewLine;
            cer.EventDescription += "Stripe Customer ID: " + dpc.StripeCustomerID + Environment.NewLine;
            cer.EventDescription += "New Stripe Plan ID: " + dpc.NewStripePlanID + Environment.NewLine;
            cer.EventDescription += "Old Stripe Plan ID: " + dpc.OldStripePlanID + Environment.NewLine;
            cer.EventDescription += "Subscription Deleted By: " + cer.ReceivedBy + Environment.NewLine;
            cer.ActionExplaination = "Activate Deferred Subscription Plan";
            int result = cer.Add();

            string msg = "Deferred Subscription Processing: Delete" + Environment.NewLine;
            msg += "Customer name: " + c.FirstName + " " + c.LastName + Environment.NewLine;
            msg += "Customer email: " + c.Email + Environment.NewLine;
            mvc_Mail.SendMail(AdminCart.Config.MailTo, AdminCart.Config.MailFrom(), "", "", "Deleting Deferred Subscription", msg, true);

            return "success";
        }
    }
}