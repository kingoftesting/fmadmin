﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using FM2015.Helpers;
using FMWebApp451.Interfaces;
using System.Linq;

namespace FM2015.Models
{
    public class Faq : IFaq
    {
        #region Private/Public Vars
        private int faqId;
        public int FaqId { get { return faqId; } set { faqId = value; } }
        private int siteId;
        public int SiteId { get { return siteId; } set { siteId = value; } }
        private string category;
        public string Category { get { return category; } set { category = value; } }
        private int sortOrder;
        public int SortOrder { get { return sortOrder; } set { sortOrder = value; } }
        private string question;
        public string Question { get { return question; } set { question = value; } }
        private string answer;
        public string Answer { get { return answer; } set { answer = value; } }
        private bool isEnabled;
        public bool IsEnabled { get { return isEnabled; } set { isEnabled = value; } }
        private DateTime dateCreated;
        public DateTime DateCreated { get { return dateCreated; } set { dateCreated = value; } }
        private DateTime dateModified;
        public DateTime DateModified { get { return dateModified; } set { dateModified = value; } }
        #endregion

        public Faq()
        {
            FaqId = 0;
            SiteId = 0;
            Category = "n/a";
            SortOrder = 0;
            Question = "n/a";
            Answer = "n/a";
            IsEnabled = false;
            DateCreated = Convert.ToDateTime("1/1/1900");
            DateModified = Convert.ToDateTime("1/1/1900");
        }

        public Faq(int IDfield)
        {
            string sql = @" 
SELECT * FROM  faqs 
WHERE ID = @ID 
";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, IDfield);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                FaqId = Convert.ToInt32(dr["FaqId"].ToString());
                SiteId = Convert.ToInt32(dr["SiteId"].ToString());
                Category = dr["Category"].ToString();
                SortOrder = Convert.ToInt32(dr["SortOrder"].ToString());
                Question = dr["Question"].ToString();
                Answer = dr["Answer"].ToString();
                IsEnabled = Convert.ToBoolean(dr["Enabled"].ToString());
                DateCreated = Convert.ToDateTime(dr["DateCreated"].ToString());
                DateModified = Convert.ToDateTime(dr["DateModified"].ToString());
            }
            if (dr != null) { dr.Close(); }
        }

        public int Save(int ID)
        {
            int result = 0;
            SiteId = 1;
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (ID == 0)
            {
                string sql = @" 
INSERT INTO faqs 
(SiteId, Category, SortOrder, Question, Answer, Enabled, DateCreated, DateModified) 
VALUES(@FaqId, @SiteId, @Category, @SortOrder, @Question, @Answer, @Enabled, @DateCreated, @DateModified); 
SELECT FaqId=@@identity;  
";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, FaqId, SiteId, Category, SortOrder, Question, Answer, IsEnabled, DateCreated, DateModified);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["FaqId"]);
                }
                else
                {
                    result = 99;
                }
            }
            else
            {
                string sql = @" 
UPDATE faqs 
SET  
SiteId = @SiteId,  
Category = @Category,  
SortOrder = @SortOrder,  
Question = @Question,  
Answer = @Answer,  
Enabled = @Enabled,  
DateCreated = @DateCreated,  
DateModified = @DateModified  
WHERE ID = @ID  
";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, SiteId, Category, SortOrder, Question, Answer, IsEnabled, DateCreated, DateModified, ID);
                DBUtil.Exec(sql, mySqlParameters);
            }
            return result;
        }

        public Faq GetById(int id)
        {
            Faq faq = new Faq(id);
            return faq;
        }

        public List<Faq> GetAll()
        {
            List<Faq> faqList = Faq.GetfaqList();
            return faqList;
        }

        public List<Faq> GetAll(string paramLookUp)
        {
            List<Faq> faqList = Faq.GetfaqList();
            if (!string.IsNullOrEmpty(paramLookUp))
            {
                faqList = (from f in faqList where ((f.Question.ToLower().Contains(paramLookUp.ToLower())) || (f.Answer.ToLower().Contains(paramLookUp.ToLower()))) select f).ToList();
            }
            return faqList;
        }

        public int Save(Faq faq)
        {
            return faq.Save(faq.faqId);
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all faq
        /// </summary>
        public static List<Faq> GetfaqList()
        {
            List<Faq> thelist = new List<Faq>();

            string sql = @" 
SELECT * FROM faqs 
Order By Category asc, SortOrder asc 
";

            SqlDataReader dr = DBUtil.FillDataReader(sql);

            while (dr.Read())
            {
                Faq obj = new Faq();

                obj.FaqId = Convert.ToInt32(dr["FaqId"].ToString());
                obj.SiteId = (dr["SiteId"] == DBNull.Value) ? 1 : Convert.ToInt32(dr["SiteId"].ToString());
                obj.Category = dr["Category"].ToString();
                obj.SortOrder = Convert.ToInt32(dr["SortOrder"].ToString());
                obj.Question = dr["Question"].ToString();
                obj.Answer = dr["Answer"].ToString();
                obj.IsEnabled = Convert.ToBoolean(dr["Enabled"].ToString());
                obj.DateCreated = Convert.ToDateTime(dr["DateCreated"].ToString());
                obj.DateModified = Convert.ToDateTime(dr["DateModified"].ToString());

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a className object with the specified ID
        /// </summary>

        public static Faq GetfaqByID(int id)
        {
            Faq obj = new Faq(id);
            return obj;
        }

        /// <summary>
        /// Updates an existing faq
        /// </summary>
        public static bool Updatefaq(int id,
        int faqid, int siteid, string category, int sortorder, string question, string answer, bool isenabled, DateTime datecreated, DateTime datemodified)
        {
            Faq obj = new Faq();

            obj.FaqId = faqid;
            obj.SiteId = siteid;
            obj.Category = category;
            obj.SortOrder = sortorder;
            obj.Question = question;
            obj.Answer = answer;
            obj.IsEnabled = isenabled;
            obj.DateCreated = datecreated;
            obj.DateModified = datemodified;
            int ret = obj.Save(obj.FaqId);
            return (ret > 0) ? true : false;
        }

        /// <summary>
        /// Creates a new faq
        /// </summary>
        public static int Insertfaq(
        int siteid, string category, int sortorder, string question, string answer, bool isenabled, DateTime datecreated, DateTime datemodified)
        {
            Faq obj = new Faq();

            obj.SiteId = siteid;
            obj.Category = category;
            obj.SortOrder = sortorder;
            obj.Question = question;
            obj.Answer = answer;
            obj.IsEnabled = isenabled;
            obj.DateCreated = datecreated;
            obj.DateModified = datemodified;
            int ret = obj.Save(0);
            return ret;
        }

    }
}