﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FM2015.Models
{
    public class FM4AppDownload
    {
        #region Private/Public Vars
        public enum Devicetype { Phone, Tablet, Laptop, Desktop };

        private string browser;
        public string Browser { get { return browser; } set { browser = value; } }
        private string operatingSystem;
        public string OperatingSystem { get { return operatingSystem; } set { operatingSystem = value; } }
        private string isMobile;
        public string IsMobile { get { return isMobile; } set { isMobile = value; } }
        private string downloadMessage;
        public string DownloadMessage { get { return downloadMessage; } set { downloadMessage = value; } }
        private HtmlString downloadLink;
        public HtmlString DownloadLink { get { return downloadLink; } set { downloadLink = value; } }
        #endregion

        public FM4AppDownload()
        {
            string userAgent = HttpContext.Current.Request.UserAgent;
            HttpBrowserCapabilities userBrowser = HttpContext.Current.Request.Browser;
            Browser = userBrowser.Browser;
            OperatingSystem = GetUserPlatform(userAgent);
            IsMobile = (userBrowser.IsMobileDevice) ? "YES" : "NO";

            if (!userBrowser.IsMobileDevice)
            {
                DownloadMessage = "The FM4 app is not available because this device is not mobile. This app " +
                    "is only available for iOS, Android or Windows devices equipped with Bluetooth Low Energy technology";
                //DownloadLink = string.Empty;
                //downloadLink = new HtmlString("<span><a href='../FM4app/Android/FM4 App Download - Android.pdf'>" +
                //    "<img src='../images/FM4/icon.png'></img></a></span>");
            }
            else
            {
                if (OperatingSystem.Contains("iOS"))
                {
                    DownloadMessage = "Download FM4 app for iOS";
                    downloadLink = new HtmlString("<span><a href='../FM4app/iOS/FM4 App Download - iOS.pdf'>" +
                        "<img src='../images/FM4/icon.png'></a></span>" +
                        "<span>Tap Icon Image to Start App Install...</span>");
                }
                else if (OperatingSystem.Contains("Android"))
                {
                    DownloadMessage = "Download FM4 app for Android";
                    downloadLink = new HtmlString("<span><a href='https://drive.google.com/open?id=1DJmU54dxrVgWYvOF-zSGU9jX1HkJspyy'>" +
                        "<img src='../images/FM4/icon.png'></img></a></span>" +
                        "<span>Tap Icon Image to Start App Install...</span>");
                }
                else
                {
                    DownloadMessage = "Unrecognized mobile device OS: " + OperatingSystem + "; FM4 app download has failed";
                    DownloadLink = new HtmlString(string.Empty);
                }
            }
        }

        private string GetUserPlatform(string ua)
        {
            if (ua.Contains("Android"))
                return string.Format("Android {0}", GetMobileVersion(ua, "Android"));

            if (ua.Contains("iPad"))
                return string.Format("iPad iOS {0}", GetMobileVersion(ua, "OS"));

            if (ua.Contains("iPhone"))
                return string.Format("iPhone iOS {0}", GetMobileVersion(ua, "OS"));

            if (ua.Contains("Linux") && ua.Contains("KFAPWI"))
                return "Kindle Fire";

            if (ua.Contains("RIM Tablet") || (ua.Contains("BB") && ua.Contains("Mobile")))
                return "Black Berry";

            if (ua.Contains("Windows Phone"))
                return string.Format("Windows Phone {0}", GetMobileVersion(ua, "Windows Phone"));

            if (ua.Contains("Mac OS"))
                return "Mac OS";

            if (ua.Contains("Windows NT 5.1") || ua.Contains("Windows NT 5.2"))
                return "Windows XP";

            if (ua.Contains("Windows NT 6.0"))
                return "Windows Vista";

            if (ua.Contains("Windows NT 6.1"))
                return "Windows 7";

            if (ua.Contains("Windows NT 6.2"))
                return "Windows 8";

            if (ua.Contains("Windows NT 6.3"))
                return "Windows 8.1";

            if (ua.Contains("Windows NT 10"))
                return "Windows 10";

            //fallback to basic platform:
            return (ua.Contains("Mobile") ? " Mobile " : "");
        }

        public String GetMobileVersion(string userAgent, string device)
        {
            var temp = userAgent.Substring(userAgent.IndexOf(device) + device.Length).TrimStart();
            var version = string.Empty;

            foreach (var character in temp)
            {
                var validCharacter = false;
                int test = 0;

                if (Int32.TryParse(character.ToString(), out test))
                {
                    version += character;
                    validCharacter = true;
                }

                if (character == '.' || character == '_')
                {
                    version += '.';
                    validCharacter = true;
                }

                if (validCharacter == false)
                    break;
            }

            return version;
        }
    }
}