﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace FM2015.Models
{
    public class FaceMasterScans
    {
        #region Private/Public Vars
        private int faceMasterScanID;
        public int FaceMasterScanID { get { return faceMasterScanID; } set { faceMasterScanID = value; } }
        private int orderID;
        public int OrderID { get { return orderID; } set { orderID = value; } }
        private long serial;
        public long Serial { get { return serial; } set { serial = value; } }
        private DateTime scanDate;
        public DateTime ScanDate { get { return scanDate; } set { scanDate = value; } }
        private int sSOrderID;
        public int SSOrderID { get { return sSOrderID; } set { sSOrderID = value; } }
        #endregion

        public FaceMasterScans()
        {
            faceMasterScanID = 0;
            orderID = 0;
            serial = 0;
            scanDate = Convert.ToDateTime("1/1/1900");
            sSOrderID = 0;
        }

        public FaceMasterScans(int orderID)
        {
            string sql = @" 
                SELECT * FROM  FaceMasterScans 
                WHERE OrderID = @OrderD 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                faceMasterScanID = Convert.ToInt32(dr["FaceMasterScanID"]);
                orderID = Convert.ToInt32(dr["OrderID"].ToString());
                serial = Convert.ToInt64(dr["Serial"]);
                scanDate = Convert.ToDateTime(dr["ScanDate"].ToString());
                sSOrderID = Convert.ToInt32(dr["SSOrderID"].ToString());
            }
            if (dr != null) { dr.Close(); }
        }

        public int Save(int faceMasterScansID)
        {
            int result = 0;
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (faceMasterScansID == 0)
            {
                string sql = @" 
                    INSERT INTO FaceMasterScans 
                    (OrderID, Serial, ScanDate, SSOrderID) 
                    VALUES(@OrderID, @Serial, @ScanDate, @SSOrderID); 
                    SELECT ID=@@identity;  
                ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID, serial, scanDate, sSOrderID);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ID"]);
                }
                else
                {
                    result = 99;
                }
            }
            else
            {
                string sql = @" 
                    UPDATE FaceMasterScans 
                    SET  
                        OrderID = @OrderID, 
                        Serial = @Serial,  
                        ScanDate = @ScanDate,  
                        SSOrderID = @SSOrderID  
                    WHERE FaceMasterScanID = @FaceMasterScanID
                ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID, serial, scanDate, sSOrderID, faceMasterScanID);
                DBUtil.Exec(sql, mySqlParameters);
            }
            return result;
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all FaceMasterScans
        /// </summary>
        public static List<FaceMasterScans> GetFaceMasterScansList(int orderID)
        {
            List<FaceMasterScans> thelist = new List<FaceMasterScans>();

            string sql = @" 
                SELECT * FROM FaceMasterScans 
                WHERE OrderID = @OrderID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                FaceMasterScans obj = new FaceMasterScans();

                obj.FaceMasterScanID = Convert.ToInt32(dr["FaceMasterScanID"]);
                obj.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                obj.Serial = Convert.ToInt64(dr["Serial"].ToString());
                obj.ScanDate = Convert.ToDateTime(dr["ScanDate"].ToString());
                obj.SSOrderID = Convert.ToInt32(dr["SSOrderID"].ToString());

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a className object with the specified ID
        /// </summary>

        public static FaceMasterScans GetFaceMasterScansByID(int id)
        {
            FaceMasterScans obj = new FaceMasterScans(id);
            return obj;
        }

        /// <summary>
        /// Updates an existing FaceMasterScans
        /// </summary>
        public static bool UpdateFaceMasterScans(int id, int orderid, long serial, DateTime scandate, int ssorderid)
        {
            FaceMasterScans obj = new FaceMasterScans();

            obj.FaceMasterScanID = id;
            obj.OrderID = orderid;
            obj.Serial = serial;
            obj.ScanDate = scandate;
            obj.SSOrderID = ssorderid;
            int ret = obj.Save(obj.FaceMasterScanID);
            return (ret > 0) ? true : false;
        }

        /// <summary>
        /// Creates a new FaceMasterScans
        /// </summary>
        public static int InsertFaceMasterScans(int orderid, long serial, DateTime scandate, int ssorderid)
        {
            FaceMasterScans obj = new FaceMasterScans();

            obj.OrderID = orderid;
            obj.Serial = serial;
            obj.ScanDate = scandate;
            obj.SSOrderID = ssorderid;
            int ret = obj.Save(obj.FaceMasterScanID);
            return ret;
        }
    }
}