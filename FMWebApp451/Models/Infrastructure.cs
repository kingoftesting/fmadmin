﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace antiforgery
{
    //http://geekswithblogs.net/Frez/archive/2015/01/11/anti-forgery-tokens-with-angularjs-and-asp.net-web-api.aspx

    public sealed class ValidateAjaxAntiForgeryToken : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext == null)
            {
                throw new ArgumentNullException("actionContext");
            }
            var headers = actionContext.Request.Headers;
            var cookie = headers
                .GetCookies()
                .Select(c => c[AntiForgeryConfig.CookieName])
                .FirstOrDefault();
            var tokenFromHeader = "";
            if (headers.Contains("X-XSRF-Token"))
                tokenFromHeader = headers.GetValues("X-XSRF-Token").FirstOrDefault();
            AntiForgery.Validate(cookie != null ? cookie.Value : null, tokenFromHeader);

            base.OnActionExecuting(actionContext);
        }

    }
}