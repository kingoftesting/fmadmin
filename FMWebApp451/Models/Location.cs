
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Data.SqlClient;
using FM2015.Models;
using FM2015.Helpers;
using System.Data.OleDb;
using System.Web;
using System.Text;
using System.Data;
 
namespace FM2015.Models
{
   
    public partial class Location
    {

        public int ID { get; set; }
        public Nullable<int> TypeID { get; set; }
        public string Name { get; set; }
        public Nullable<int> Parent { get; set; }
        public string Code { get; set; }

        public Location()
        {
            ID = 0;
            TypeID = 0;
            Name = "";
            Parent = 0;
            Code = "";
        }

        public void Save()
        {
            string sql = "";
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;

            if (ID == 0)
            {
                sql = @"
                INSERT INTO Locations   
                   (TypeID, Name, Parent, Code)  
                VALUES 
                    (@TypeID, @Name, @Parent, @Code)  
                SELECT ID = scope_identity()  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, TypeID, Name, Parent, Code);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read()) { ID = Convert.ToInt32(dr["ID"]); }
            }
            if (dr != null) { dr.Close(); }
        }

        public static string CountryNameFromCode(string countryCode)
        {
            List<Location> lList = GetAllLocations();
            string name = lList.Find(x => ((x.Parent == 0) && (x.Code == countryCode))).Name;

            name = (string.IsNullOrEmpty(name)) ? "" : name;
            return name;
        }

        public static string StateNameFromCode(string stateCode, string countryCode)
        {
            List<Location> lList = GetAllLocations();
            int countryID = lList.Find(x => ((x.Parent == 0) && (x.Code == countryCode))).ID;

            string name = "";
            try
            {
                 name = lList.Find(x => (x.Code == stateCode) && (x.Parent == countryID)).Name;            
            }
            catch
            {
                name = "";
            }
            name = (string.IsNullOrEmpty(name)) ? "" : name;
            return name;
        }

        public static List<Location> Countries()
        {
            List<Location> lList = GetAllLocations();
            lList = lList.FindAll(x => x.Parent == 0);
            return lList;
        }

        public static List<Location> StatesProvinces(int parent)
        {
            List<Location> lList = GetAllLocations();
            lList = lList.FindAll(x => x.Parent == parent);
            return lList;
        }

        public static SelectList GetCountrySelectList(string countryCode)
        {
            List<Location> lList = Countries();
            List<SelectListItem> selList = new List<SelectListItem>();

            foreach(Location location in lList)
            {
                SelectListItem selectListItem = new SelectListItem();
                selectListItem.Value = location.Code;
                selectListItem.Text = location.Name;
                selList.Add(selectListItem);
            }
            SelectList CountrySelectList = new SelectList(selList, "Value", "Text", countryCode);
            return CountrySelectList;
        }

        public static SelectList GetStateSelectList(int id)
        {
            List<Location> lList = StatesProvinces(id);
            List<SelectListItem> selList = new List<SelectListItem>();

            foreach (Location location in lList)
            {
                SelectListItem selectListItem = new SelectListItem();
                selectListItem.Value = location.Code;
                selectListItem.Text = location.Name;
                selList.Add(selectListItem);
            }
            SelectList stateSelectList = new SelectList(selList, "Value", "Text");
            return stateSelectList;
        }

        public static int GetCountryCodeIDFromSelectList(SelectList countrySelectList)
        {
            string countryCode = Helpers.Helpers.GetSelectListValue(countrySelectList);
            List<Location> countryList = Location.Countries();
            Location l = (from country in countryList
                          where country.Code == Helpers.Helpers.GetSelectListValue(countrySelectList)
                           select country).FirstOrDefault();
            return l.ID;
        }

        public static List<Location> GetAllLocations()
        {
            List<Location> lList = new List<Location>();

            SqlDataReader dr = DBUtil.FillDataReader("select * from locations");
            while (dr.HasRows && dr.Read())
            {
                Location loc = new Location();
                loc.ID = Convert.ToInt32(dr["ID"].ToString());
                loc.TypeID = Convert.ToInt32(dr["TypeID"].ToString());
                loc.Name = dr["Name"].ToString();
                loc.Parent = Convert.ToInt32(dr["Parent"].ToString());
                loc.Code = dr["Code"].ToString();
                lList.Add(loc);
            }
            if (dr != null) { dr.Close(); }

            return lList;
        }

        public static void TruncateLocations()
        {
            string sql = @"
            Truncate Table Locations
            ";

            SqlDataReader dr = DBUtil.FillDataReader(sql);
            if (dr != null) { dr.Close(); }
        }

        public static void InitializeFromXL()
        {
            Location.TruncateLocations();

            string sql = @"
                Select ID, TypeID, Name, Parent, Code 
                FROM Locations
            ";

            string conn = "SERVER=fmadmin.facemaster.com;UID=web;PWD=hello101;DATABASE=SiteV2;Application Name=FaceMasterWeb";
            SqlDataReader dr = DBUtil.FillDataReader(sql, conn);
            while (dr.HasRows && dr.Read())
            {
                Location l = new Location();

                Int32 tempInt = 0; //default TypeID
                if (!Int32.TryParse(dr["TypeID"].ToString(), out tempInt)) { l.TypeID = 0; }
                else { l.TypeID = tempInt; }

                if (!String.IsNullOrEmpty(dr["Name"].ToString())) { l.Name = dr["Name"].ToString(); }
                else { l.Name = ""; }

                tempInt = 0; //default Parent
                if (!Int32.TryParse(dr["Parent"].ToString(), out tempInt)) { l.Parent = 0; }
                else { l.Parent = tempInt; }

                if (!String.IsNullOrEmpty(dr["Code"].ToString())) { l.Code = dr["Code"].ToString(); }
                else { l.Code = ""; }

                l.ID = 0; //make sure this is going to be an insert
                l.Save();
            }
            if (dr != null) { dr.Close(); }
        }
    }
}

