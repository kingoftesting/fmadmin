using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Net.Mail;
using FM2015.Helpers;

/// <summary>
/// Summary description for Mail
/// </summary>
public class mvc_Mail
{
    private static string SMTPHost = ConfigurationManager.AppSettings["SMTPServer"].ToString();
    private static string SMTPLogin = ConfigurationManager.AppSettings["SMTPLogin"].ToString();
    private static string SMTPPassword = ConfigurationManager.AppSettings["SMTPPassword"].ToString();
    private static string SMTPIP = ConfigurationManager.AppSettings["SMTPIP"].ToString();
    private static string IAMUser = ConfigurationManager.AppSettings["IAMUser"].ToString();
    private static bool EnableSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
    private static bool EnableTLS = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableTLS"]);
    private static int SMTPPort = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());

    public mvc_Mail()
    {
        
    }

    public static void SendMail(string from, string to, string subject, string body)
    {
        subject = "(" + AdminCart.Config.appName + ") " + subject;
        MailMessage MyMailMessage = new MailMessage(from, to, subject, body);

        if (!to.Contains("mohme"))
        {
            MailAddress rm = new MailAddress("rmohme@alumni.uci.edu");
            MyMailMessage.Bcc.Add(rm);
        }

        MyMailMessage.IsBodyHtml = false;

        NetworkCredential mailAuthentication = new NetworkCredential(SMTPLogin, SMTPPassword);

        SmtpClient mailClient = new SmtpClient(SMTPHost, SMTPPort);
        mailClient.EnableSsl = EnableSSL;
        mailClient.UseDefaultCredentials = false;
        mailClient.Credentials = mailAuthentication;

        if (EnableTLS) { ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls; }

        try
        {
            mailClient.Send(MyMailMessage);
        }
        catch (Exception ex)
        {
            //just catch any outbound mail issues and drop them
            string msg = "Mail Exception: " + Environment.NewLine;
            msg += "To: " + to + Environment.NewLine;
            msg += "From: " + from + Environment.NewLine;
            msg += "Subject: " + subject + Environment.NewLine;
            dbErrorLogging.LogError(msg, ex, false);
        }
      
    }

    public static bool SendMail(string to, string from, string name, string rawcclist, string subject, string body)
    {
        subject = "(" + AdminCart.Config.appName + ") " + subject;
        bool sentOK = false; //assume there was a problem sending the email
        MailMessage MyMailMessage = new MailMessage(from, to, subject, body);
        MyMailMessage.From = new MailAddress(from, name);

        if (!to.Contains("mohme"))
        {
            MailAddress rm = new MailAddress("rmohme@alumni.uci.edu");
            MyMailMessage.Bcc.Add(rm);
        }

        if (!string.IsNullOrEmpty(rawcclist))
        {
            String[] cclist = rawcclist.Split(',');
            foreach (string s in cclist)
            {
                MyMailMessage.CC.Add(s);
            }
        }

        MyMailMessage.IsBodyHtml = false;
        MyMailMessage.Priority = MailPriority.Normal;

        NetworkCredential mailAuthentication = new NetworkCredential(SMTPLogin, SMTPPassword);

        SmtpClient mailClient = new SmtpClient(SMTPHost, SMTPPort);
        mailClient.EnableSsl = EnableSSL;
        mailClient.UseDefaultCredentials = false;
        mailClient.Credentials = mailAuthentication;

        if (EnableTLS) { ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls; }
        try
        {
            sentOK = true; //assume this will work
            mailClient.Send(MyMailMessage);
        }
        catch (Exception ex)
        {
            //just catch any outbound mail issues and drop them
            string msg = "Mail Exception: " + Environment.NewLine;
            msg += "To: " + to + Environment.NewLine;
            msg += "From: " + from + Environment.NewLine;
            msg += "Subject: " + subject + Environment.NewLine;
            dbErrorLogging.LogError(msg, ex, false);
            sentOK = false; //flag the error back to the caller
        }

        return sentOK;
    }

    public static bool SendMail(string to, string from, string name, string rawcclist, string subject, string body, bool ishtml)
    {
        subject = "(" + AdminCart.Config.appName + ") " + subject;
        bool sentOK = false; //assume there was a problem sending the email
        MailMessage MyMailMessage = new MailMessage(from, to, subject, body);
        MyMailMessage.From = new MailAddress(from, name);

        if (!to.Contains("mohme"))
        {
            MailAddress rm = new MailAddress("rmohme@alumni.uci.edu");
            MyMailMessage.Bcc.Add(rm);
        }

        if (!string.IsNullOrEmpty(rawcclist))
        {
            String[] cclist = rawcclist.Split(',');
            foreach (string s in cclist)
            {
                MyMailMessage.CC.Add(s);
            }
        }

        MyMailMessage.IsBodyHtml = ishtml;
        MyMailMessage.Priority = MailPriority.Normal;

        NetworkCredential mailAuthentication = new NetworkCredential(SMTPLogin, SMTPPassword);
        SmtpClient mailClient = new SmtpClient(SMTPHost, SMTPPort)
        {
            EnableSsl = EnableSSL,
            UseDefaultCredentials = false,
            Credentials = mailAuthentication
        };

        if (EnableTLS) { ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls; }
        try
        {
            sentOK = true; //assume this will work
            mailClient.Send(MyMailMessage);
        }
        catch (Exception ex)
        {
            //just catch any outbound mail issues and drop them
            string msg = "Mail Exception: " + Environment.NewLine;
            msg += "To: " + to + Environment.NewLine;
            msg += "From: " + from + Environment.NewLine;
            msg += "Subject: " + subject + Environment.NewLine;
            dbErrorLogging.LogError(msg, ex, false);
            sentOK = false; //flag the error back to the caller
        }

        return sentOK;
    }

    public static void SendDiagEmail(string subject, string body)
    {
        string mailFrom = AdminCart.Config.MailFrom();
        //string mailTo = AdminCart.Config.MailTo;
        string mailTo = "rmohme@gmail.com";
        mvc_Mail.SendMail(mailFrom, mailTo, subject, body);
    }

    public static bool Send2DeclineEmail(AdminCart.Order order, decimal amountDue, string cardNum, DateTime nextChargeDate, bool test)
    {
        AdminCart.Customer c = new AdminCart.Customer(order.CustomerID);
        string emailBody = CustomerEmailSecondDecline;
        emailBody = emailBody.Replace("%NAME%", c.FirstName + " " + c.LastName);
        emailBody = emailBody.Replace("%COMPANY%", AdminCart.Config.appName);
        emailBody = emailBody.Replace("%ORDERNUMBER%", order.SSOrderID.ToString());
        decimal amount = amountDue;
        emailBody = emailBody.Replace("%AMOUNTDUE%", amount.ToString("C"));
        emailBody = emailBody.Replace("%CARDNUMBER%", cardNum);
        emailBody = emailBody.Replace("%NEXTCHARGEDATE%", nextChargeDate.ToString());
        emailBody = emailBody.Replace("%COMPANYPHONE%", AdminCart.Config.AppPhoneNumber());
        string mailFrom = AdminCart.Config.MailFrom();
        string mailTo = "rmohme@sbcglobal.net"; //assume test
        string subject = " Did you forget a payment?";
        bool ok = false; //assume failure
        HttpContext.Current.Session["EmailDunningCopy"] = emailBody;
        if (test)
        {
            ok = mvc_Mail.SendMail(mailTo, mailFrom, AdminCart.Config.appName, "", subject, emailBody, true);
        }
        else
        {
            mailTo = c.Email;
            ok = mvc_Mail.SendMail(mailTo, mailFrom, AdminCart.Config.appName, "", subject, emailBody, true);
        }

        return ok;
    }

    public static bool Send2DeclineEmail2(AdminCart.Order order, decimal amountDue, string cardNum, DateTime nextChargeDate, bool test)
    {
        AdminCart.Customer c = new AdminCart.Customer(order.CustomerID);
        string emailBody = CustomerEmailSecondDeclineFMUpdate;
        emailBody = emailBody.Replace("%NAME%", c.FirstName + " " + c.LastName);
        emailBody = emailBody.Replace("%COMPANY%", AdminCart.Config.appName);
        emailBody = emailBody.Replace("%ORDERNUMBER%", order.SSOrderID.ToString());
        decimal amount = amountDue;
        emailBody = emailBody.Replace("%AMOUNTDUE%", amount.ToString("C"));
        emailBody = emailBody.Replace("%CARDNUMBER%", cardNum);
        emailBody = emailBody.Replace("%NEXTCHARGEDATE%", nextChargeDate.ToString());
        emailBody = emailBody.Replace("%COMPANYPHONE%", AdminCart.Config.AppPhoneNumber());
        string url = "https://fmadmin.facemaster.com/ccupdate/index?customerid=" + order.CustomerID.ToString();
        emailBody = emailBody.Replace("%UPDATEURL%", url);
        string mailFrom = AdminCart.Config.MailFrom();
        string mailTo = "rmohme@sbcglobal.net"; //assume test
        string subject = " Did you forget a payment?";
        bool ok = false; //assume failure
        HttpContext.Current.Session["EmailDunningCopy"] = emailBody;
        if (test)
        {
            ok = mvc_Mail.SendMail(mailTo, mailFrom, AdminCart.Config.appName, "", subject, emailBody, true);
        }
        else
        {
            mailTo = c.Email;
            ok = mvc_Mail.SendMail(mailTo, mailFrom, AdminCart.Config.appName, "", subject, emailBody, true);
        }

        return ok;
    }

    public static string CustomerEmailSecondDecline = @"
<p>Dear %NAME%,</p>

<p>re: %COMPANY% order# %ORDERNUMBER%; payment attempts have been declined</p>

<p>Amount Due: %AMOUNTDUE%</p>

<p>The credit card number being declined is %CARDNUMBER%</p>

<p>
Our payment service is automated and will make several more attempts to successfully charge your credit card over the next few days. 
If you need to update your credit card information with our payment service, you have a couple of options:
</p>
<ul>
<li>If you created an account for your purchase please go to <a href='www.paywhirl.com' target='_blank'>PayWhirl Payment Processing</a> 
and update your card info online using the email address and password you used to place your order</li>
<li>Give us a call at %COMPANYPHONE% (M-F 9:00am - 5:00pm Pacific / noon - 8:00pm Eastern; Holidays excluded) and 
one of our customer service team members will be happy to help you</li>
</ul>
<p>
We look forward to your account returning to one of ""good standing"". We appreciate your business and hope to have you as a permanent %COMPANY% customer!  
</p>
<p>
Thank You,<br />
%COMPANY% Customer Service
</p>
";

    public static string CustomerEmailSecondDeclineFMUpdate = @"
<p>Dear %NAME%,</p>

<p>re: %COMPANY% order# %ORDERNUMBER%; payment attempts have been declined</p>

<p>Amount Due: %AMOUNTDUE%</p>

<p>The credit card number being declined is %CARDNUMBER%</p>

<p>
Our payment service is automated and will make several more attempts to successfully charge your credit card over the next few days. 
If you need to update your credit card information with our payment service, you have a couple of options:
</p>
<ul>
<li>You can click here <a href='%UPDATEURL%' target='_blank'>FaceMaster Payment Method Update</a> 
and update your card info online</li>
<li>Give us a call at %COMPANYPHONE% (M-F 9:00am - 5:00pm Pacific / noon - 8:00pm Eastern; Holidays excluded) and 
one of our customer service team members will be happy to help you</li>
</ul>
<p>
We look forward to your account returning to one of ""good standing"". We appreciate your business and hope to have you as a permanent %COMPANY% customer!  
</p>
<p>
Thank You,<br />
%COMPANY% Customer Service
</p>
";

}
