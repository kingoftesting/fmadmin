﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using FM2015.Helpers;

namespace FM2015.Models
{
    public class OrderTransfers
    {
        #region Private/Public Vars
        private int orderTransferID;
        public int OrderTransferID { get { return orderTransferID; } set { orderTransferID = value; } }
        private int orderID;
        public int OrderID { get { return orderID; } set { orderID = value; } }
        private int shippingLocation;
        public int ShippingLocation { get { return shippingLocation; } set { shippingLocation = value; } }
        private int shippingSystemID;
        public int ShippingSystemID { get { return shippingSystemID; } set { shippingSystemID = value; } }
        private int batchID;
        public int BatchID { get { return batchID; } set { batchID = value; } }
        private DateTime createDate;
        public DateTime CreateDate { get { return createDate; } set { createDate = value; } }
        private DateTime transferDate;
        public DateTime TransferDate { get { return transferDate; } set { transferDate = value; } }
        private DateTime labelDate;
        public DateTime LabelDate { get { return labelDate; } set { labelDate = value; } }
        private int siteID;
        public int SiteID { get { return siteID; } set { siteID = value; } }
        private int sSOrderID;
        public int SSOrderID { get { return sSOrderID; } set { sSOrderID = value; } }
        #endregion

        public OrderTransfers()
        {
            OrderTransferID = 0;
            OrderID = 0;
            ShippingLocation = 0;
            ShippingSystemID = 0;
            BatchID = 0;
            CreateDate = Convert.ToDateTime("1/1/1900");
            TransferDate = Convert.ToDateTime("1/1/1900");
            LabelDate = Convert.ToDateTime("1/1/1900");
            SiteID = 0;
            SSOrderID = 0;
        }

        public OrderTransfers(int orderID)
        {
            string sql = @" 
                SELECT * FROM  OrderTransfers 
                WHERE OrderID = @OrderID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                try
                {
                    OrderTransferID = Convert.ToInt32(dr["OrderTransferID"]);
                    OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                    ShippingLocation = Convert.ToInt32(dr["ShippingLocationID"].ToString());
                    ShippingSystemID = Convert.ToInt32(dr["ShippingSystemID"].ToString());
                    BatchID = Convert.ToInt32(dr["BatchID"].ToString());
                    if (dr["CreateDate"] != DBNull.Value) { CreateDate = Convert.ToDateTime(dr["CreateDate"]); }
                    if (dr["TransferDate"] != DBNull.Value) { TransferDate = Convert.ToDateTime(dr["TransferDate"]); }
                    if (dr["LabelDate"] != DBNull.Value) { LabelDate = Convert.ToDateTime(dr["LabelDate"]); }
                    SiteID = Convert.ToInt32(dr["SiteID"].ToString());
                    if (dr["SSOrderID"] != DBNull.Value) { SSOrderID = Convert.ToInt32(dr["SSOrderID"].ToString()); }

                }
                catch 
                {; }
            }
            if (dr != null) { dr.Close(); }
        }

        public int Save(int OrderID)
        {
            int result = 0;
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (OrderID == 0)
            {
                string sql = @" 
                    INSERT INTO OrderTransfers 
                        (OrderID, ShippingLocation, ShippingSystemID, BatchID, CreateDate, TransferDate, LabelDate, SiteID, SSOrderID) 
                        VALUES(@OrderID, @ShippingLocation, @ShippingSystemID, @BatchID, @CreateDate, @TransferDate, @LabelDate, @SiteID, @SSOrderID); 
                    SELECT ID=@@identity;  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID, ShippingLocation, ShippingSystemID, BatchID, CreateDate, TransferDate, LabelDate, SiteID, SSOrderID);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ID"]);
                }
                else
                {
                    result = 99;
                }
            }
            else
            {
                string sql = @" 
                    UPDATE OrderTransfers 
                    SET  
                        OrderID = @OrderID,  
                        ShippingLocation = @ShippingLocation,  
                        ShippingSystemID = @ShippingSystemID,  
                        BatchID = @BatchID,  
                        CreateDate = @CreateDate,  
                        TransferDate = @TransferDate,  
                        LabelDate = @LabelDate,  
                        SiteID = @SiteID,  
                        SSOrderID = @SSOrderID  
                    WHERE OrderID = @OrderID  
";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID, ShippingLocation, ShippingSystemID, BatchID, CreateDate, TransferDate, LabelDate, SiteID, SSOrderID, OrderTransferID);
                DBUtil.Exec(sql, mySqlParameters);
            }
            return result;
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all OrderTransfers
        /// </summary>
        public static List<OrderTransfers> GetOrderTransfersList()
        {
            List<OrderTransfers> thelist = new List<OrderTransfers>();

            string sql = @" 
                SELECT Top 1000 * FROM OrderTransfers 
            ";

            SqlDataReader dr = DBUtil.FillDataReader(sql);

            while (dr.Read())
            {
                OrderTransfers obj = new OrderTransfers();

                obj.OrderTransferID = Convert.ToInt32(dr["OrderTransferID"]);
                obj.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                obj.ShippingLocation = Convert.ToInt32(dr["ShippingLocation"].ToString());
                obj.ShippingSystemID = Convert.ToInt32(dr["ShippingSystemID"].ToString());
                obj.BatchID = Convert.ToInt32(dr["BatchID"].ToString());
                obj.CreateDate = Convert.ToDateTime(dr["CreateDate"].ToString());
                obj.TransferDate = Convert.ToDateTime(dr["TransferDate"].ToString());
                obj.LabelDate = Convert.ToDateTime(dr["LabelDate"].ToString());
                obj.SiteID = Convert.ToInt32(dr["SiteID"].ToString());
                obj.SSOrderID = Convert.ToInt32(dr["SSOrderID"].ToString());

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a collection with all OrderTransfers
        /// </summary>
        public static List<OrderTransfers> GetOrderTransfersListByID(int orderID)
        {
            List<OrderTransfers> thelist = new List<OrderTransfers>();

            string sql = @" 
                SELECT Top 1000 * FROM OrderTransfers 
                WHERE OrderID = @OrderID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                OrderTransfers obj = new OrderTransfers();

                obj.OrderTransferID = Convert.ToInt32(dr["OrderTransferID"]);
                obj.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                obj.ShippingLocation = Convert.ToInt32(dr["ShippingLocation"].ToString());
                obj.ShippingSystemID = Convert.ToInt32(dr["ShippingSystemID"].ToString());
                obj.BatchID = Convert.ToInt32(dr["BatchID"].ToString());
                obj.CreateDate = Convert.ToDateTime(dr["CreateDate"].ToString());
                obj.TransferDate = Convert.ToDateTime(dr["TransferDate"].ToString());
                obj.LabelDate = Convert.ToDateTime(dr["LabelDate"].ToString());
                obj.SiteID = Convert.ToInt32(dr["SiteID"].ToString());
                obj.SSOrderID = Convert.ToInt32(dr["SSOrderID"].ToString());

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a className object with the specified ID
        /// </summary>

        public static OrderTransfers GetOrderTransfersByID(int orderID)
        {
            OrderTransfers obj = new OrderTransfers(orderID);
            return obj;
        }

        /// <summary>
        /// Updates an existing OrderTransfers
        /// </summary>
        public static bool UpdateOrderTransfers(int orderTransferID,
        int orderid, int shippinglocation, int shippingsystemid, int batchid, DateTime createdate, DateTime transferdate, DateTime labeldate, int siteid, int ssorderid)
        {
            OrderTransfers obj = new OrderTransfers();

            obj.OrderTransferID = orderTransferID;
            obj.OrderID = orderid;
            obj.ShippingLocation = shippinglocation;
            obj.ShippingSystemID = shippingsystemid;
            obj.BatchID = batchid;
            obj.CreateDate = createdate;
            obj.TransferDate = transferdate;
            obj.LabelDate = labeldate;
            obj.SiteID = siteid;
            obj.SSOrderID = ssorderid;
            int ret = obj.Save(obj.OrderID);
            return (ret > 0) ? true : false;
        }

        /// <summary>
        /// Creates a new OrderTransfers
        /// </summary>
        public static int InsertOrderTransfers(
        int orderid, int shippinglocation, int shippingsystemid, int batchid, DateTime createdate, DateTime transferdate, DateTime labeldate, int siteid, int ssorderid)
        {
            OrderTransfers obj = new OrderTransfers();

            obj.OrderID = orderid;
            obj.ShippingLocation = shippinglocation;
            obj.ShippingSystemID = shippingsystemid;
            obj.BatchID = batchid;
            obj.CreateDate = createdate;
            obj.TransferDate = transferdate;
            obj.LabelDate = labeldate;
            obj.SiteID = siteid;
            obj.SSOrderID = ssorderid;
            int ret = obj.Save(obj.OrderID);
            return ret;
        }
    }
}