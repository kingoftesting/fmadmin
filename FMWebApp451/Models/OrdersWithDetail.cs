﻿using System;

namespace FM2015.Models
{
    public class OrdersWithDetail
    {
        //o.orderdate, o.customerid, od.productId, p.IsFMSystem
        public int OrdersWithDetailsId;
        public DateTime OrderDate;
        public int CustomerId;
        //public int ProductId;
        public bool IsFMSystem;
        public string FirstName;
        public string LastName;
        public string Email;

        public OrdersWithDetail()
        {
            OrdersWithDetailsId = 0;
            OrderDate = DateTime.MinValue;
            CustomerId = 0;
            //ProductId = 0;
            IsFMSystem = false;
            FirstName = "";
            LastName = "";
            Email = "";
        }
    }
}