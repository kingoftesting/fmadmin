﻿using System;
using System.Text;
using System.Data;

using System.Data.SqlClient;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.IO;
using FM2015.Helpers;
using System.Collections.Specialized;

namespace PayWhirl
{
    public class JsonPayWhirlRequest
    {

        /// <summary>
        /// GETs from Shopify API url jsonURL;
        /// </summary>
        /// <param name="jsonURL"></param>
        /// <returns></returns>
        public static string GET(string jsonURL)
        {
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

            string apikey = AdminCart.Config.ApiKeyPayWhirl();
            string pass = AdminCart.Config.SecretPayWhirl();

            string uri = AdminCart.Config.RootPayWhirlApi() + jsonURL;

            WebClient webClient = new WebClient();
            webClient.Headers[HttpRequestHeader.Host] = "www.paywhirl.com";
            webClient.Headers[HttpRequestHeader.CacheControl] = "no-cache";
            webClient.Headers["api_key"] = apikey;
            webClient.Headers["api_secret"] = pass;

            string response = "";
            try
            {
                //ignores SSL errors; really want to use SecurityProtocolType.Tls12, but need a .net4.5 box
                if (ConfigurationManager.AppSettings["IgnoreSSLErrors"].ToString().ToUpperInvariant() == "YES")
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                } 
                response = webClient.DownloadString(uri);
            }
            catch (WebException ex)
            {
                if (ex.Response is HttpWebResponse)
                {
                    response = "HttpStatus= #STATUS#; Failed PayWhirl json request GET(webclient.UpLoadString); ";
                    response += "jsonURL= " + jsonURL + "; ";
                    response += "responseText= #TEXT#";
                    HttpWebResponse webResponse = (HttpWebResponse)ex.Response;
                    string responseText;
                    var responseStream = ex.Response.GetResponseStream();

                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseText = reader.ReadToEnd();
                            response = response.Replace("#TEXT#", responseText);
                        }
                    }

                    response = UpdateResponseStatus(response, webResponse);
                    FM2015.Helpers.dbErrorLogging.LogError(response, ex);
                }
                else
                {
                    response = "Failed PayWhirl json request GET(webclient.DownLoadString); Response not HTTPWebResponse" + Environment.NewLine;
                    response += "URL= " + jsonURL + Environment.NewLine;
                    FM2015.Helpers.dbErrorLogging.LogError(response, ex);
                }
            }
            if (webClient != null) { webClient.Dispose(); }


            return response;
        }

        /// <summary>
        /// POSTS jsonStr to Shopify API url jsonURL; Returns string with POST result
        /// </summary>
        /// <param name="jsonURL"></param>
        /// <param name="jsonStr"></param>
        /// <returns>string</returns>
        public static string POST(string jsonURL, string jsonStr)
        {
            string apikey = AdminCart.Config.ApiKeyPayWhirl();
            string pass = AdminCart.Config.SecretPayWhirl();

            string uri = AdminCart.Config.RootPayWhirl() + jsonURL;

            //string uriStr = "https://" + ConfigurationManager.AppSettings["adminroot"] + "admin/customers.json";
            //Uri myUri = new Uri(uri);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

            WebClient webClient = new WebClient();
            webClient.Headers[HttpRequestHeader.Host] = "www.paywhirl.com";
            webClient.Headers[HttpRequestHeader.CacheControl] = "no-cache";
            webClient.Headers["api_key"] = apikey;
            webClient.Headers["api_secret"] = pass;
            webClient.Headers.Add("Content-Type", "application/json");

            string response = "";
            try
            {
                //ignores SSL errors; really want to use SecurityProtocolType.Tls12, but need a .net4.5 box
                if (ConfigurationManager.AppSettings["IgnoreSSLErrors"].ToString().ToUpperInvariant() == "YES")
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                }
                response = webClient.UploadString(jsonURL, jsonStr);
                if (webClient != null) { webClient.Dispose(); }
                return response;
            }

            catch (WebException ex)
            {
                if (ex.Response is HttpWebResponse)
                {
                    response = "HttpStatus= #STATUS#; Failed PayWhirl json request POST(webclient.UpLoadString); responseText= #TEXT#";
                    HttpWebResponse webResponse = (HttpWebResponse)ex.Response;
                    string responseText;
                    var responseStream = ex.Response.GetResponseStream();

                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseText = reader.ReadToEnd();
                            response = response.Replace("#TEXT#", responseText);
                        }
                    }

                    response = UpdateResponseStatus(response, webResponse);
                    FM2015.Helpers.dbErrorLogging.LogError(response, ex);
                }
                else
                {
                    response = "failed PayWhirl json request POST(webclient.UpLoadString); Response not HTTPResponse" + Environment.NewLine;
                    response += "URL= " + jsonURL + Environment.NewLine;
                    response += "json= " + jsonStr + Environment.NewLine;
                    FM2015.Helpers.dbErrorLogging.LogError(response, ex);
                }

                if (webClient != null) { webClient.Dispose(); }
                return response;
            }
        }

        /// <summary>
        /// POSTS jsonStr to Shopify API url jsonURL; Returns string with POST result
        /// </summary>
        /// <param name="jsonURL"></param>
        /// <param name="jsonStr"></param>
        /// <returns>string</returns>
        public static string POST(string jsonURL, string jsonStr, NameValueCollection collection)
        {
            string apikey = AdminCart.Config.ApiKeyPayWhirl();
            string pass = AdminCart.Config.SecretPayWhirl();

            jsonURL = AdminCart.Config.RootPayWhirl() + jsonURL;

            //string uriStr = "https://" + ConfigurationManager.AppSettings["adminroot"] + "admin/customers.json";
            //Uri myUri = new Uri(uri);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

            WebClient webClient = new WebClient();
            webClient.Headers[HttpRequestHeader.Host] = "www.paywhirl.com";
            webClient.Headers[HttpRequestHeader.CacheControl] = "no-cache";
            webClient.Headers["api_key"] = apikey;
            webClient.Headers["api_secret"] = pass;
            //webClient.Headers.Add("Content-Type", "application/json");

            string response = "";
            try
            {
                //ignores SSL errors; really want to use SecurityProtocolType.Tls12, but need a .net4.5 box
                if (ConfigurationManager.AppSettings["IgnoreSSLErrors"].ToString().ToUpperInvariant() == "YES")
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                }
                //response = webClient.UploadString(jsonURL, jsonStr);
                byte[] responseArray = webClient.UploadValues(jsonURL, collection);
                response = Encoding.ASCII.GetString(responseArray);
                if (webClient != null) { webClient.Dispose(); }
                return response;
            }

            catch (WebException ex)
            {
                if (ex.Response is HttpWebResponse)
                {
                    response = "HttpStatus= #STATUS#; Failed PayWhirl json request POST(webclient.UploadValues); responseText= #TEXT#";
                    HttpWebResponse webResponse = (HttpWebResponse)ex.Response;
                    string responseText;
                    var responseStream = ex.Response.GetResponseStream();

                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseText = reader.ReadToEnd();
                            response = response.Replace("#TEXT#", responseText);
                        }
                    }

                    response = UpdateResponseStatus(response, webResponse);
                    FM2015.Helpers.dbErrorLogging.LogError(response, ex);
                }
                else
                {
                    response = "failed PayWhirl json request POST(webclient.UploadValues); Response not HTTPResponse; ";
                    response += "URL= " + jsonURL + Environment.NewLine;
                    //response += "json= " + jsonStr + Environment.NewLine;
                    response += "collection: ";
                    foreach (var key in collection.Keys)
                    {
                        response += key.ToString() + ":" + collection[key.ToString()].ToString() + Environment.NewLine;
                    }
                    FM2015.Helpers.dbErrorLogging.LogError(response, ex);
                }

                if (webClient != null) { webClient.Dispose(); }
                return response;
            }
        }

        /// <summary>
        /// PUTS jsonStr to Shopify API url jsonURL; Returns string with PUT result
        /// </summary>
        /// <param name="jsonURL"></param>
        /// <param name="jsonStr"></param>
        /// <returns>string</returns>
        public static string PUT(string jsonURL, string jsonStr)
        {
            string apikey = AdminCart.Config.ApiKeyPayWhirl();
            string pass = AdminCart.Config.SecretPayWhirl();

            string uri = AdminCart.Config.RootPayWhirl() + jsonURL;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

            WebClient webClient = new WebClient();
            webClient.Headers[HttpRequestHeader.Host] = "www.paywhirl.com";
            webClient.Headers[HttpRequestHeader.CacheControl] = "no-cache";
            webClient.Headers["api_key"] = apikey;
            webClient.Headers["api_secret"] = pass;
            webClient.Headers.Add("Content-Type", "application/json");

            string response = "";
            try
            {
                //ignores SSL errors; really want to use SecurityProtocolType.Tls12, but need a .net4.5 box
                if (ConfigurationManager.AppSettings["IgnoreSSLErrors"].ToString().ToUpperInvariant() == "YES")
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                }
                response = webClient.UploadString(jsonURL, "PUT", jsonStr);
                if (webClient != null) { webClient.Dispose(); }
                return response;
            }

            catch (WebException ex)
            {
                if (ex.Response is HttpWebResponse)
                {
                    response = "HttpStatus= #STATUS#; Failed PayWhirl json request PUT(webclient.UpLoadString); responseText= #TEXT#";
                    HttpWebResponse webResponse = (HttpWebResponse)ex.Response;
                    string responseText;
                    var responseStream = ex.Response.GetResponseStream();

                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseText = reader.ReadToEnd();
                            response = response.Replace("#TEXT#", responseText);
                        }
                    }

                    response = UpdateResponseStatus(response, webResponse);
                    FM2015.Helpers.dbErrorLogging.LogError(response, ex);
                }
                else
                {
                    response = "failed PayWhirl json request PUT(webclient.UpLoadString); Response not HTTPResponse" + Environment.NewLine;
                    response += "URL= " + jsonURL + Environment.NewLine;
                    response += "json= " + jsonStr + Environment.NewLine;
                    FM2015.Helpers.dbErrorLogging.LogError(response, ex);
                }

                if (webClient != null) { webClient.Dispose(); }
                return response;
            }
        }

        private static string UpdateResponseStatus(string response, HttpWebResponse webResponse)
        {
            switch (webResponse.StatusCode)
            {
                case HttpStatusCode.BadRequest:
                    response = response.Replace("#STATUS#", "400/BadRequest: missing/bad parameter?");
                    break;

                case HttpStatusCode.Unauthorized:
                    response = response.Replace("#STATUS#", "401/Unauthorized: no valid API key");
                    break;

                case HttpStatusCode.Conflict:
                    response = response.Replace("#STATUS#", "402/Parameters OK, but request failed; payment required or reserved for furture use");
                    break;

                case HttpStatusCode.Forbidden:
                    response = response.Replace("#STATUS#", "403/Forbidden by server");
                    break;

                case HttpStatusCode.NotFound:
                    response = response.Replace("#STATUS#", "404/NotFound; requested item doesn't exist");
                    break;

                case HttpStatusCode.MethodNotAllowed:
                    response = response.Replace("#STATUS#", "405/MethodNotAllowed");
                    break;

                case HttpStatusCode.NotAcceptable:
                    response = response.Replace("#STATUS#", "406/NotAcceptable");
                    break;

                case HttpStatusCode.Ambiguous:
                    response = response.Replace("#STATUS#", "300/Ambiguous");
                    break;

                default:
                    response = response.Replace("#STATUS#", "???/unknown");
                    break;
            }
            return response;
        }
    }
}