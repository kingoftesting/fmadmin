﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminCart;
using FM2015.Models;

namespace PayWhirl
{
    public class PayWhirlBiz
    {
        public static PayWhirlSubscriber GetSubscriber(int id)
        {
            PayWhirlSubscriber pwSub = new PayWhirlSubscriber();

            switch (Config.PayWhirlApiVersion)
            {
                case "V1":
                    pwSub = PayWhirlSubscriber.GetSubscriber(id);
                    break;

                case "V2":
                    PayWhirlV2.pwCustomerV2 pw2Customer = new PayWhirlV2.pwCustomerV2();
                    pw2Customer = PayWhirlV2.pwCustomerV2.GetCustomer(id);
                    //map pw2Customer to pwSub
                    pwSub.Id = pw2Customer.Id;
                    pwSub.User_id = pw2Customer.user_Id;
                    pwSub.Email = pw2Customer.Email;
                    pwSub.First_name = pw2Customer.First_name;
                    pwSub.Last_name = pw2Customer.Last_name;
                    pwSub.Address_1 = pw2Customer.Address;
                    pwSub.Address_2 = "";
                    pwSub.City = pw2Customer.City;
                    pwSub.State = pw2Customer.State;
                    pwSub.Zip = pw2Customer.Zip;
                    pwSub.Country = pw2Customer.Country;
                    pwSub.Phone = pw2Customer.Phone;
                    pwSub.Stripe_id = pw2Customer.Gateway_reference;
                    pwSub.Date_created = pw2Customer.Created_at;
                    pwSub.Admin = "";
                    break;

                default:
                    throw new NotImplementedException();
            }

            return pwSub;
        }

        public static PayWhirlSubscriber FindSubscriberByEmail(string email)
        {
            PayWhirlSubscriber pwSub = new PayWhirlSubscriber();

            switch (Config.PayWhirlApiVersion)
            {
                case "V1":
                    pwSub = PayWhirlSubscriber.FindSubscriberByEmail(email);
                    break;

                case "V2":
                    PayWhirlV2.pwCustomerV2 pw2Customer = new PayWhirlV2.pwCustomerV2();
                    pw2Customer = PayWhirlV2.pwCustomerV2.GetCustomer(email);
                    //map pw2Customer to pwSub
                    pwSub.Id = pw2Customer.Id;
                    pwSub.User_id = pw2Customer.user_Id;
                    pwSub.Email = pw2Customer.Email;
                    pwSub.First_name = pw2Customer.First_name;
                    pwSub.Last_name = pw2Customer.Last_name;
                    pwSub.Address_1 = pw2Customer.Address;
                    pwSub.Address_2 = "";
                    pwSub.City = pw2Customer.City;
                    pwSub.State = pw2Customer.State;
                    pwSub.Zip = pw2Customer.Zip;
                    pwSub.Country = pw2Customer.Country;
                    pwSub.Phone = pw2Customer.Phone;
                    pwSub.Stripe_id = pw2Customer.Gateway_reference;
                    pwSub.Date_created = pw2Customer.Created_at;
                    pwSub.Admin = "";
                    break;

                default:
                    throw new NotImplementedException();
            }

            return pwSub;
        }

        public static List<PayWhirlSubscriber> GetSubscriberList(string param)
        {
            string email = ParseCustomerListParam(param);
            List<PayWhirlSubscriber> pwSubList = new List<PayWhirlSubscriber>();

            switch (Config.PayWhirlApiVersion)
            {
                case "V1":
                    
                    if (!string.IsNullOrEmpty(email))
                    {
                        PayWhirlSubscriber pwSub = PayWhirlSubscriber.FindSubscriberByEmail(email);
                        pwSubList.Add(pwSub); 
                    }
                    else
                    {
                        pwSubList = PayWhirlSubscriber.GetSubscriberList();
                    }
                    break;

                case "V2":
                    List<PayWhirlV2.pwCustomerV2> pw2CustomerList = PayWhirlV2.pwCustomerV2.GetCustomerListV2();
                    foreach(PayWhirlV2.pwCustomerV2 pw2Customer in pw2CustomerList)
                    {
                        PayWhirlSubscriber pwSub = new PayWhirlSubscriber();
                        //map pw2Customer to pwSub
                        pwSub.Id = pw2Customer.Id;
                        pwSub.User_id = pw2Customer.user_Id;
                        pwSub.Email = pw2Customer.Email;
                        pwSub.First_name = pw2Customer.First_name;
                        pwSub.Last_name = pw2Customer.Last_name;
                        pwSub.Address_1 = pw2Customer.Address;
                        pwSub.Address_2 = "";
                        pwSub.City = pw2Customer.City;
                        pwSub.State = pw2Customer.State;
                        pwSub.Zip = pw2Customer.Zip;
                        pwSub.Country = pw2Customer.Country;
                        pwSub.Phone = pw2Customer.Phone;
                        pwSub.Stripe_id = pw2Customer.Gateway_reference;
                        pwSub.Date_created = pw2Customer.Created_at;
                        pwSub.Admin = "";
                        pwSubList.Add(pwSub);
                    }
                    break;

                default:
                    throw new NotImplementedException();
            }

            return pwSubList;
        }

        private static string ParseCustomerListParam(string param)
        {
            string email = string.Empty;
            if (param != null)
            {
                if (param.Contains("@"))
                {
                    email = param;
                }
                else if (int.TryParse(param, out int customerID))
                {
                    Customer c = new Customer(customerID);
                    email = c.Email;
                }
                else if ((param.ToUpper().Contains("CUS_")) || (param.ToUpper().Contains("SUB_")))
                {
                    List<Subscriptions> subscriptions = Subscriptions.GetSubscribersList();
                    Subscriptions subscriber = (from s in subscriptions where (s.StripeCusID.ToUpper() == param.ToUpper() || s.StripeSubID.ToUpper() == param.ToUpper()) select s).FirstOrDefault();
                    if (subscriber != null)
                    {
                        Order o = new Order(subscriber.OrderID);
                        Customer c = new Customer(o.CustomerID);
                        email = c.Email;
                    }
                }
            }
            return email;
        }

        public static PayWhirlUser GetCurrentUser()
        {
            PayWhirlUser pwUser = new PayWhirlUser();

            switch (Config.PayWhirlApiVersion)
            {
                case "V1":
                    pwUser = PayWhirlUser.GetCurrentUser();
                    break;

                case "V2":
                    PayWhirlV2.pwAccountV2 pwAccount = new PayWhirlV2.pwAccountV2();
                    pwAccount = PayWhirlV2.pwAccountV2.GetAccount();
                    //map pw2Customer to pwSub
                    pwUser.Id = pwAccount.Id;
                    pwUser.First_name = pwAccount.First_name;
                    pwUser.Last_name = pwAccount.Last_name;
                    pwUser.Email = pwAccount.Email;
                    pwUser.Api_key = "";
                    pwUser.Phone = pwAccount.Phone;
                    pwUser.Date_created = pwAccount.Created_at;                    
                    break;

                default:
                    throw new NotImplementedException();
            }

            return pwUser;
        }
    }
}