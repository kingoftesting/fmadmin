﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;
using System.Configuration;
using System.Linq;
using Stripe;

namespace PayWhirl
{
    public class PayWhirlPlans
    {
        [JsonProperty("plans")]
        public PayWhirlPlan[] plans;

        public PayWhirlPlans()
        {
            plans = new PayWhirlPlan[] { };
        }

    }

    public class PayWhirlPlan
    {
        #region Fields & Properties
        private int id;
        [JsonProperty("id")]
        public int Id { get { return id; } set { id = value; } }

        private string name;
        [JsonProperty("name")]
        public string Name { get { return name; } set { name = value; } }

        private string shipping;
        [JsonProperty("shipping")]
        public string Shipping { get { return shipping; } set { shipping = value; } }

        private string enabled;
        [JsonProperty("enabled")]
        public string Enabled { get { return enabled; } set { enabled = value; } }

        private string image;
        [JsonProperty("image")]
        public string Image { get { return image; } set { image = value; } }

        private decimal setup_fee;
        [JsonProperty("setup_fee")]
        public decimal Setup_fee { get { return setup_fee; } set { setup_fee = value; } }

        private long? shopify_product_id;
        [JsonProperty("shopify_product_id")]
        public long? Shopify_product_id { get { return shopify_product_id; } set { shopify_product_id = value; } }

        private long? shopify_variant_id;
        [JsonProperty("shopify_variant_id")]
        public long? Shopify_variant_id { get { return shopify_variant_id; } set { shopify_variant_id = value; } }

        private string shopify_fulfillment;
        [JsonProperty("shopify_fulfillment")]
        public string Shopify_fulfillment { get { return shopify_fulfillment; } set { shopify_fulfillment = value; } }

        private string sku;
        [JsonProperty("sku")]
        public string Sku { get { return sku; } set { sku = value; } }

        private string ordering;
        [JsonProperty("ordering")]
        public string Ordering { get { return ordering; } set { ordering = value; } }

        private string installments;
        [JsonProperty("installments")]
        public string Installments { get { return installments; } set { installments = value; } }

        private string billing_cycle_anchor;
        [JsonProperty("billing_cycle_anchor")]
        public string Billing_cycle_anchor { get { return billing_cycle_anchor; } set { billing_cycle_anchor = value; } }

        #endregion
                #region Constructors
        public PayWhirlPlan()
        {
            id = 0;
            name = "";
            shipping = "yes";
            enabled = "no";
            image = "";
            setup_fee = 0;
            shopify_product_id = 0;
            shopify_variant_id = 0;
            shopify_fulfillment = "manual";
            sku = "";
            ordering = "0";
            installments = "0";
            billing_cycle_anchor = "0";
        }
        #endregion

        #region Methods

        public static List<PayWhirlPlan> GetPlanList()
        {
            List<PayWhirlPlan> planList = new List<PayWhirlPlan>();
            PayWhirlPlans plans = new PayWhirlPlans();
            PayWhirlPlan plan = new PayWhirlPlan();

            string jsonURL = "/api/plans";
            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            try
            {
                string jsonResult = @"{""plans"":" + apiResult + "}";
                plans = JsonConvert.DeserializeObject<PayWhirlPlans>(jsonResult);
                for (int i = 0; i < plans.plans.Length; i++)
                { planList.Add(plans.plans[i]); }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                planList = new List<PayWhirlPlan>(); //return something that won't crash
                string msg = "error in PayWhirl: GetPlanList </br>";
                msg += "jsonURL: " + jsonURL + "</br>";
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            return planList;
        }

        public static PayWhirlPlan GetPlan(int id)
        {
            PayWhirlPlan plan = new PayWhirlPlan();

            string jsonURL = "/api/plan?id=" + id.ToString();
            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            try
            {
                plan = JsonConvert.DeserializeObject<PayWhirlPlan>(apiResult);
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                plan = new PayWhirlPlan(); //return something that won't crash
                string msg = "error in PayWhirl: GetPlan </br>";
                msg += "id: " + id.ToString() + "</br>";
                msg += "jsonURL: " + jsonURL + "</br>";
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            return plan;
        }

        public static int GetPlanInstallments (string StripePlanName)
        {
            int installments = 0;
            List<PayWhirlPlan> planList = PayWhirlPlan.GetPlanList();
            PayWhirlPlan plan = (from p in planList where (p.name == StripePlanName) select p).First();
            if (plan != null)
            { installments = Convert.ToInt32(plan.Installments); }

            return installments;
        }

        public static int GetPlanInstallments(string stripeCustumerID, string subScriptionID)
        {
            Stripe.StripeSubscription stripeSubScription = new Stripe.StripeSubscription();
            var subscriptionService = new Stripe.StripeSubscriptionService(); //get list of all subscriptions for customer
            try
            {
                stripeSubScription = subscriptionService.Get(subScriptionID); //***********
            }
            catch 
            {; } //ignore exception if not a subscription

            //int installments = stripeSubScription.StripePlan.IntervalCount;
            //List<PayWhirlPlan> planList = PayWhirlPlan.GetPlanList();
            //PayWhirlPlan plan = (from p in planList where (p.name == stripeSubScription.StripePlan.Product.Name) select p).First();
            //if (plan != null)
            //{ installments = Convert.ToInt32(plan.Installments); }

            return stripeSubScription.StripePlan.IntervalCount;
        }
        #endregion


    }
}