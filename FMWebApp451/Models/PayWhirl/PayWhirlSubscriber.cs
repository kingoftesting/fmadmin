﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;
using System.Configuration;
using System.Collections.Specialized;

namespace PayWhirl
{
    public class PayWhirlSubscribers
    {
        [JsonProperty("subscribers")]
        public PayWhirlSubscriber[] subscribers;

        public PayWhirlSubscribers()
        {
            subscribers = new PayWhirlSubscriber[] { };
        }

    }
    public class PayWhirlSubscriber
    {
        #region Fields & Properties
        private int id;
        [JsonProperty("id")]
        public int Id { get { return id; } set { id = value; } }

        private int user_id;
        [JsonProperty("user_id")]
        public int User_id { get { return user_id; } set { user_id = value; } }

        private string email;
        [JsonProperty("email")]
        public string Email { get { return email; } set { email = value; } }

        private string password;
        [JsonProperty("password")]
        public string Password { get { return password; } set { password = value; } }

        private string first_name;
        [JsonProperty("first_name")]
        public string First_name { get { return first_name; } set { first_name = value; } }

        private string last_name;
        [JsonProperty("last_name")]
        public string Last_name { get { return last_name; } set { last_name = value; } }

        private string phone;
        [JsonProperty("phone")]
        public string Phone { get { return phone; } set { phone = value; } }

        private string address_1;
        [JsonProperty("address_1")]
        public string Address_1 { get { return address_1; } set { address_1 = value; } }

        private string address_2;
        [JsonProperty("address_2")]
        public string Address_2 { get { return address_2; } set { address_2 = value; } }

        private string city;
        [JsonProperty("city")]
        public string City { get { return city; } set { city = value; } }

        private string state;
        [JsonProperty("state")]
        public string State { get { return state; } set { state = value; } }

        private string country;
        [JsonProperty("country")]
        public string Country { get { return country; } set { country = value; } }

        private string zip;
        [JsonProperty("zip")]
        public string Zip { get { return zip; } set { zip = value; } }

        private string stripe_id;
        [JsonProperty("stripe_id")]
        public string Stripe_id { get { return stripe_id; } set { stripe_id = value; } }

        private string admin;
        [JsonProperty("admin")]
        public string Admin { get { return admin; } set { admin = value; } }

        private DateTime date_created;
        [JsonProperty("date_created")]
        public DateTime Date_created { get { return date_created; } set { date_created = value; } }


        #endregion
        #region Constructors
        public PayWhirlSubscriber()
        {
            id = 0;
            email = "";
            password = "";
            first_name = "";
            last_name = "";
            phone = "";
            address_1 = "";
            address_2 = "";
            state = "";
            country = "";
            zip = "";
            stripe_id = "";
            admin = "";
            date_created = DateTime.MinValue;
        }
        #endregion

        #region Methods

        public static List<PayWhirlSubscriber> GetSubscriberList()
        {
            List<PayWhirlSubscriber> subList = new List<PayWhirlSubscriber>();
            PayWhirlSubscribers subs = new PayWhirlSubscribers();
            PayWhirlSubscriber sub = new PayWhirlSubscriber();

            string jsonURL = "/api/subscribers?limit=100";
            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            try
            {
                string jsonResult = @"{""subscribers"":" + apiResult + "}";
                subs = JsonConvert.DeserializeObject<PayWhirlSubscribers>(jsonResult);
                for (int i = 0; i < subs.subscribers.Length; i++)
                { subList.Add(subs.subscribers[i]); }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                subList = new List<PayWhirlSubscriber>(); //return something that won't crash
                string msg = "error in PayWhirl: GetSubscriberList" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "response: " + apiResult + Environment.NewLine;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            return subList;
        }

        public static PayWhirlSubscriber GetSubscriber(int id)
        {
            PayWhirlSubscriber sub = new PayWhirlSubscriber();

            string jsonURL = "/api/subscriber?id=" + id.ToString();
            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            try
            {
                sub = JsonConvert.DeserializeObject<PayWhirlSubscriber>(apiResult);
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                sub = new PayWhirlSubscriber(); //return something that won't crash
                string msg = "error in PayWhirl: GetSubscriber" + Environment.NewLine;
                msg += "id: " + id.ToString() + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            return sub;
        }

        public static PayWhirlSubscriber FindSubscriberByEmail(string email)
        {
            string escapedEmail = Uri.EscapeDataString(email);
            PayWhirlSubscriber sub = new PayWhirlSubscriber();

            string jsonURL = "/api/subscriber?email=" + escapedEmail;
            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            apiResult = apiResult.Replace("\n", "");
            if (apiResult != "[]")
            {
                try
                {
                    sub = JsonConvert.DeserializeObject<PayWhirlSubscriber>(apiResult);
                }
                catch (Exception ex)
                {
                    //response wasn't a good Product response
                    sub = new PayWhirlSubscriber(); //return something that won't crash
                    string msg = "error in PayWhirl: FindSubscriberByEmail" + Environment.NewLine;
                    msg += "Email: " + email + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "response: " + apiResult;
                    FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                }
            }

            return sub;
        }

        public static PayWhirlSubscriber CreateSubscriber(PayWhirlSubscriber subscriber)
        {
            PayWhirlSubscriber newSub = new PayWhirlSubscriber();
            string jsonURL = "/api/subscriber/create";
            
            string jsonStr = JsonConvert.SerializeObject(subscriber);
            //jsonStr = @"{""subscriber"":{""phone"":""8056106826""}}";
            //jsonStr = @"{""subscriber"":" + jsonStr + "}";
            //jsonStr = "{}";
            NameValueCollection collection = new NameValueCollection();
            collection.Add("email", subscriber.Email);
            collection.Add("first_name", subscriber.First_name);
            collection.Add("last_name", subscriber.Last_name);
            collection.Add("phone", subscriber.Phone);
            collection.Add("address_1", subscriber.Address_1);
            collection.Add("address_2", subscriber.Address_2);
            collection.Add("city", subscriber.City);
            collection.Add("state", subscriber.State);
            collection.Add("country", subscriber.Country);
            collection.Add("zip", subscriber.Zip);

            //string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr);
            string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr, collection);
            try
            {
                newSub = JsonConvert.DeserializeObject<PayWhirlSubscriber>(apiResult);
                if (newSub.Id == 0)
                {
                    //response wasn't a good Product response
                    newSub = new PayWhirlSubscriber(); //return something that won't crash
                    string msg = "error in PayWhirl: CreateSubscriber" + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "jsonStr: " + jsonStr + Environment.NewLine;
                    msg += "response: " + apiResult;
                    string mailFrom = AdminCart.Config.MailFrom();
                    string mailTo = AdminCart.Config.MailTo;
                    string subject = "PayWhirl JSON Error";
                    string contactBody = msg;
                    mvc_Mail.SendMail(mailTo, mailFrom, "", "", subject, contactBody, true);
                }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                newSub = new PayWhirlSubscriber(); //return something that won't crash
                string msg = "error in PayWhirl: UpdateSubscriber" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "jsonStr: " + jsonStr + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return newSub;
        }

        public static PayWhirlSubscriber UpdateSubscriber(PayWhirlSubscriber subscriber)
        {
            PayWhirlSubscriber sub = new PayWhirlSubscriber();
            string jsonURL = "/api/subscriber/update";
            string jsonStr = JsonConvert.SerializeObject(subscriber);

            NameValueCollection collection = new NameValueCollection();
            collection.Add("id", subscriber.Id.ToString());
            //collection.Add("password", subscriber.Password);
            collection.Add("email", subscriber.Email);
            collection.Add("first_name", subscriber.First_name);
            collection.Add("last_name", subscriber.Last_name);
            collection.Add("phone", subscriber.Phone);
            collection.Add("address_1", subscriber.Address_1);
            collection.Add("address_2", subscriber.Address_2);
            collection.Add("city", subscriber.City);
            collection.Add("state", subscriber.State);
            collection.Add("country", subscriber.Country);
            collection.Add("zip", subscriber.Zip);
            
            string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr, collection);
            try
            {
                sub = JsonConvert.DeserializeObject<PayWhirlSubscriber>(apiResult);
                if (sub.Id == 0)
                {
                    //response wasn't a good Product response
                    sub = new PayWhirlSubscriber(); //return something that won't crash
                    string msg = "error in PayWhirl: UpdateSubscriber" + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "jsonStr: " + jsonStr + Environment.NewLine;
                    msg += "response: " + apiResult;
                    string mailFrom = AdminCart.Config.MailFrom();
                    string mailTo = AdminCart.Config.MailTo;
                    string subject = "PayWhirl JSON Error";
                    string contactBody = msg ;
                    mvc_Mail.SendMail(mailTo, mailFrom, "", "", subject, contactBody, true);
                }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                sub = new PayWhirlSubscriber(); //return something that won't crash
                string msg = "error in PayWhirl: UpdateSubscriber" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "jsonStr: " + jsonStr + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            return sub;
        }
        #endregion
    }
}