﻿using System;
using Newtonsoft.Json;


namespace PayWhirl
{  
    public class PayWhirlUser
    {
        #region Fields & Properties
        private int id;
        [JsonProperty("id")]
        public int Id { get { return id; } set { id = value; } }

        private string first_name;
        [JsonProperty("first_name")]
        public string First_name { get { return first_name; } set { first_name = value; } }

        private string last_name;
        [JsonProperty("last_name")]
        public string Last_name { get { return last_name; } set { last_name = value; } }

        private string email;
        [JsonProperty("email")]
        public string Email { get { return email; } set { email = value; } }
        
        private string phone;
        [JsonProperty("phone")]
        public string Phone { get { return phone; } set { phone = value; } }

        private string industry;
        [JsonProperty("industry")]
        public string Industry { get { return industry; } set { industry = value; } }

        private string api_key;
        [JsonProperty("api_key")]
        public string Api_key { get { return api_key; } set { api_key = value; } }

        private DateTime date_created;
        [JsonProperty("date_created")]
        public DateTime Date_created { get { return date_created; } set { date_created = value; } }

        #endregion

        #region Constructors
        public PayWhirlUser()
        {
            id = 0;
            first_name = "";
            last_name = "";
            email = "";
            phone = "";
            industry = "";
            api_key = "";
            date_created = DateTime.MinValue;
        }

        #endregion

        #region Methods

        public static PayWhirlUser GetCurrentUser()
        {
            string jsonURL = "/api/me";

            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            PayWhirlUser user = new PayWhirlUser();
            try
            {
                user = JsonConvert.DeserializeObject<PayWhirlUser>(apiResult);
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                user = new PayWhirlUser(); //return something that won't crash
                string msg = "error in PayWhirl: GetCurrentUser </br>";
                msg += "jsonURL: " + jsonURL + "</br>";
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return user;
        }

        #endregion

    }
}