﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace PayWhirlV2
{

    public class pwAccountV2
    {
        /*
        //Example Response
        {
          "id": "1",
          "first_name": "Brandon",
          "last_name": "Swift",
          "company_name": "Brandon's Site ",
          "country": "US",
          "email": "brandon@paywhirl.com",
          "created_at": "2015-10-23 02:57:27",
          "updated_at": "2015-11-02 03:02:27",
          "phone": "8054444444",
          "address": "123 Easy St",
          "city": "Atlanta",
          "state": "Georgia",
          "zip": "30084",
          "currency": "USD",
          "slug": "brandonswift",
          "email_settings": {
            "test_mode": "on",
            "welcome_email": "13",
            "new_subscription": "default",
            "new_card": "default",
            "payment_receipt": "default",
            "payment_problem": "default",
            "payment_request": "default",
            "email_via": "paywhirl",
            "host": "smtp.gmail.com",
            "username": "",
            "password": "",
            "from_address": "",
            "from_name": ""
          }
        }
        */
        #region Fields & Properties
        private int id;
        [JsonProperty("id")]
        public int Id { get { return id; } set { id = value; } }

        private string first_name;
        [JsonProperty("first_name")]
        public string First_name { get { return first_name; } set { first_name = value; } }

        private string last_name;
        [JsonProperty("last_name")]
        public string Last_name { get { return last_name; } set { last_name = value; } }

        private string company_name;
        [JsonProperty("company_name")]
        public string Company_name { get { return company_name; } set { company_name = value; } }

        private string email;
        [JsonProperty("email")]
        public string Email { get { return email; } set { email = value; } }

        private string phone;
        [JsonProperty("phone")]
        public string Phone { get { return phone; } set { phone = value; } }

        private string address;
        [JsonProperty("address")]
        public string Address { get { return address; } set { address = value; } }

        private string city;
        [JsonProperty("city")]
        public string City { get { return city; } set { city = value; } }

        private string state;
        [JsonProperty("state")]
        public string State { get { return state; } set { state = value; } }

        private string zip;
        [JsonProperty("zip")]
        public string Zip { get { return zip; } set { zip = value; } }

        private string country;
        [JsonProperty("country")]
        public string Country { get { return country; } set { country = value; } }

        private string currency;
        [JsonProperty("currency")]
        public string Currency { get { return currency; } set { currency = value; } }

        private string slug;
        [JsonProperty("slug")]
        public string Slug { get { return slug; } set { slug = value; } }

        private DateTime created_at;
        [JsonProperty("created_at")]
        public DateTime Created_at { get { return created_at; } set { created_at = value; } }

        private DateTime updated_at;
        [JsonProperty("updated_at")]
        public DateTime Updated_at { get { return updated_at; } set { updated_at = value; } }

        public pwEmailSettingsV2 EmailSettings;

        #endregion

        #region Constructors
        public pwAccountV2()
        {
            id = 0;
            first_name = "";
            last_name = "";
            company_name = "";
            email = "";
            phone = "";
            address = "";
            city = "";
            state = "";
            zip = "";
            country = "";
            currency = "";
            slug = "";
            created_at = DateTime.MinValue;
            updated_at = DateTime.MinValue;

            EmailSettings = new pwEmailSettingsV2();
        }

        #endregion

        #region Methods

        ///
        /// Get Account info
        /// @return Account Object
        ///
        public static pwAccountV2 GetAccount()
        {
            string jsonURL = "/account";

            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            pwAccountV2 pwAccount = new pwAccountV2();
            try
            {
                pwAccount = JsonConvert.DeserializeObject<pwAccountV2>(apiResult);
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                pwAccount = new pwAccountV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: GetAccount" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return pwAccount;
        }

        #endregion
    }
}