﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace PayWhirlV2
{
    public class pwCustomersV2
    {
        [JsonProperty("customers")]
        public pwCustomerV2[] customersV2;

        public pwCustomersV2()
        {
            customersV2 = new pwCustomerV2[] { };
        }
    }

    public class pwCustomerV2
    {
        /*
        {
          "id": "1",
          "user_id": "1",
          "first_name": "Fred",
          "last_name": "Flintstone",
          "email": "fred@paywhirl.com",
          "phone": "8052843895",
          "address": "4447 Cain Cir",
          "city": "Tucker",
          "state": "Georgia",
          "zip": "30084",
          "country": "US",
          "created_at": "2015-10-23 18:48:26",
          "updated_at": "2015-11-02 02:36:45",
          "gateway_reference": "cus_7GazY7Xk3CFCrv",
          "default_card": "55",
          "gateway_type": "Stripe",
          "gateway_id": "1",
          "currency": "USD",
          "deleted_at": null
          "utm_source": "google",
          "utm_medium": "cpc",
          "utm_term": "Billing System",
          "utm_content": "Best billing system",
          "utm_campaign": "California_campaign",
          "utm_group": "State Ads"
        }
        */
        #region Fields & Properties
        private int id;
        [JsonProperty("id")]
        public int Id { get { return id; } set { id = value; } }

        private int user_id;
        [JsonProperty("user_id")]
        public int user_Id { get { return user_id; } set { user_id = value; } }

        private string first_name;
        [JsonProperty("first_name")]
        public string First_name { get { return first_name; } set { first_name = value; } }

        private string last_name;
        [JsonProperty("last_name")]
        public string Last_name { get { return last_name; } set { last_name = value; } }

        private string email;
        [JsonProperty("email")]
        public string Email { get { return email; } set { email = value; } }

        private string password;
        [JsonProperty("password")]
        public string Password { get { return password; } set { password = value; } }

        private string phone;
        [JsonProperty("phone")]
        public string Phone { get { return phone; } set { phone = value; } }

        private string address;
        [JsonProperty("address")]
        public string Address { get { return address; } set { address = value; } }

        private string city;
        [JsonProperty("city")]
        public string City { get { return city; } set { city = value; } }

        private string state;
        [JsonProperty("state")]
        public string State { get { return state; } set { state = value; } }

        private string zip;
        [JsonProperty("zip")]
        public string Zip { get { return zip; } set { zip = value; } }

        private string country;
        [JsonProperty("country")]
        public string Country { get { return country; } set { country = value; } }

        private DateTime created_at;
        [JsonProperty("created_at")]
        public DateTime Created_at { get { return created_at; } set { created_at = value; } }

        private DateTime updated_at;
        [JsonProperty("updated_at")]
        public DateTime Updated_at { get { return updated_at; } set { updated_at = value; } }

        private string gateway_reference;
        [JsonProperty("gateway_reference")]
        public string Gateway_reference { get { return gateway_reference; } set { gateway_reference = value; } }

        private string default_card;
        [JsonProperty("default_card")]
        public string Default_card { get { return default_card; } set { default_card = value; } }

        private string gateway_type;
        [JsonProperty("gateway_type")]
        public string Gateway_type { get { return gateway_type; } set { gateway_type = value; } }

        private string gateway_id;
        [JsonProperty("gateway_id")]
        public string Gateway_id { get { return gateway_id; } set { gateway_id = value; } }

        private string currency;
        [JsonProperty("currency")]
        public string Currency { get { return currency; } set { currency = value; } }

        private DateTime? deleted_at;
        [JsonProperty("deleted_at")]
        public DateTime? Deleted_at { get { return deleted_at; } set { deleted_at = value; } }

        private string utm_source;
        [JsonProperty("utm_source")]
        public string Utm_source { get { return utm_source; } set { utm_source = value; } }

        private string utm_medium;
        [JsonProperty("utm_medium")]
        public string Utm_medium { get { return utm_medium; } set { utm_medium = value; } }

        private string utm_term;
        [JsonProperty("utm_term")]
        public string Utm_term { get { return utm_term; } set { utm_term = value; } }

        private string utm_content;
        [JsonProperty("utm_content")]
        public string Utm_content { get { return utm_content; } set { utm_content = value; } }

        private string utm_campaign;
        [JsonProperty("utm_campaign")]
        public string Utm_campaign { get { return utm_campaign; } set { utm_campaign = value; } }

        private string utm_group;
        [JsonProperty("utm_group")]
        public string Utm_group { get { return utm_group; } set { utm_group = value; } }

        #endregion

        #region Constructors
        public pwCustomerV2()
        {
            id = 0;
            user_Id = 0;
            first_name = "";
            last_name = "";
            email = "";
            phone = "";
            password = "";
            address = "";
            city = "";
            state = "";
            zip = "";
            country = "";
            created_at = DateTime.MinValue;
            updated_at = DateTime.MinValue;
            gateway_reference = "";
            default_card = "";
            gateway_type = "";
            gateway_id = "";
            currency = "USD";
            deleted_at = null;
            utm_source = "";
            utm_medium = "";
            utm_content = "";
            utm_campaign = "";
            utm_group = "";
        }

        #endregion

        #region Methods

        ///
        /// Get a customer
        /// @param  int id  //Customer ID
        /// @return Customer Object
        ///
        public static pwCustomerV2 GetCustomer(int id)
        {
            string jsonURL = "/customer/" + id.ToString();

            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            pwCustomerV2 pwCustomer = new pwCustomerV2();
            try
            {
                pwCustomer = JsonConvert.DeserializeObject<pwCustomerV2>(apiResult);
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                pwCustomer = new pwCustomerV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: GetCustomer" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return pwCustomer;
        }

        ///
        /// Get a customer
        /// @param  int id  //Customer ID
        /// @return Customer Object
        ///
        public static pwCustomerV2 GetCustomer(string email)
        {
            string escapedEmail = Uri.EscapeDataString(email);
            string jsonURL = "/customer/" + email;

            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            pwCustomerV2 pwCustomer = new pwCustomerV2();
            try
            {
                pwCustomer = JsonConvert.DeserializeObject<pwCustomerV2>(apiResult);
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                pwCustomer = new pwCustomerV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: GetCustomer-by-email" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return pwCustomer;
        }

        ///
        /// Create a customer
        /// @param  Customer Data (id = 0)
        /// @return Customer Object
        ///
        public static pwCustomerV2 CreateCustomer(pwCustomerV2 pwCustomer)
        {
            pwCustomerV2 newCustomer = new pwCustomerV2();
            string jsonURL = "/create/customer";

            string jsonStr = JsonConvert.SerializeObject(pwCustomer);
            NameValueCollection collection = new NameValueCollection();
            collection.Add("first_name", pwCustomer.First_name);
            collection.Add("last_name", pwCustomer.Last_name);
            collection.Add("email", pwCustomer.Email);
            collection.Add("phone", pwCustomer.Phone);
            collection.Add("address", pwCustomer.Address);
            collection.Add("city", pwCustomer.City);
            collection.Add("state", pwCustomer.State);
            collection.Add("country", pwCustomer.Country); //ie, countryCode: US, GB, CA, AU, etc 
            collection.Add("zip", pwCustomer.Zip);

            //string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr);
            string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr, collection);
            try
            {
                newCustomer = JsonConvert.DeserializeObject<pwCustomerV2>(apiResult);
                if (newCustomer.Id == 0)
                {
                    //response wasn't a good Product response
                    newCustomer = new pwCustomerV2(); //return something that won't crash
                    string msg = "error in PayWhirlV2: CreateCustomer" + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "jsonStr: " + jsonStr + Environment.NewLine;
                    msg += "response: " + apiResult;
                    string mailFrom = AdminCart.Config.MailFrom();
                    string mailTo = AdminCart.Config.MailTo;
                    string subject = "PayWhirlV2 JSON Error";
                    string contactBody = msg;
                    mvc_Mail.SendMail(mailTo, mailFrom, "", "", subject, contactBody, true);
                }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                newCustomer = new pwCustomerV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: CreateCustomer" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "jsonStr: " + jsonStr + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return newCustomer;
        }

        ///
        /// Update a customer
        /// @param  Customer Date (id != 0)
        /// @return Customer Object
        ///
        public static pwCustomerV2 UpdateCustomer(pwCustomerV2 pwCustomer)
        {
            pwCustomerV2 newCustomer = new pwCustomerV2();
            string jsonURL = "/update/customer";

            string jsonStr = JsonConvert.SerializeObject(pwCustomer);
            NameValueCollection collection = new NameValueCollection();
            collection.Add("id", pwCustomer.Id.ToString());
            collection.Add("first_name", pwCustomer.First_name);
            collection.Add("last_name", pwCustomer.Last_name);
            collection.Add("email", pwCustomer.Email);
            collection.Add("phone", pwCustomer.Phone);
            collection.Add("address", pwCustomer.Address);
            collection.Add("city", pwCustomer.City);
            collection.Add("state", pwCustomer.State);
            collection.Add("country", pwCustomer.Country); //ie, countryCode: US, GB, CA, AU, etc 
            collection.Add("zip", pwCustomer.Zip);

            //string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr);
            string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr, collection);
            try
            {
                newCustomer = JsonConvert.DeserializeObject<pwCustomerV2>(apiResult);
                if (newCustomer.Id == 0)
                {
                    //response wasn't a good Product response
                    newCustomer = new pwCustomerV2(); //return something that won't crash
                    string msg = "error in PayWhirlV2: UpdateCustomer" + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "jsonStr: " + jsonStr + Environment.NewLine;
                    msg += "response: " + apiResult;
                    string mailFrom = AdminCart.Config.MailFrom();
                    string mailTo = AdminCart.Config.MailTo;
                    string subject = "PayWhirlV2 JSON Error";
                    string contactBody = msg;
                    mvc_Mail.SendMail(mailTo, mailFrom, "", "", subject, contactBody, true);
                }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                newCustomer = new pwCustomerV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: UpdateCustomer" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "jsonStr: " + jsonStr + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return newCustomer;
        }

        public static List<pwCustomerV2> GetCustomerListV2()
        {
            List<pwCustomerV2> customerList = new List<pwCustomerV2>();
            pwCustomersV2 customers = new pwCustomersV2();
            pwCustomerV2 customer = new pwCustomerV2();

            string jsonURL = "/customers";
            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            try
            {
                string jsonResult = @"{""customers"":" + apiResult + "}";
                customers = JsonConvert.DeserializeObject<pwCustomersV2>(jsonResult);
                for (int i = 0; i < customers.customersV2.Length; i++)
                { customerList.Add(customers.customersV2[i]); }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                customerList = new List<pwCustomerV2>(); //return something that won't crash
                string msg = "error in PayWhirlV2: GetCustomerList" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            return customerList;
        }

        #endregion
    }
}