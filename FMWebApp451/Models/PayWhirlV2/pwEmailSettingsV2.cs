﻿using System;
using Newtonsoft.Json;

namespace PayWhirlV2
{

    public class pwEmailSettingsV2
    {
        /*
        //Example Response
        {
            "test_mode": "on",
            "welcome_email": "13",
            "new_subscription": "default",
            "new_card": "default",
            "payment_receipt": "default",
            "payment_problem": "default",
            "payment_request": "default",
            "email_via": "paywhirl",
            "host": "smtp.gmail.com",
            "username": "",
            "password": "",
            "from_address": "",
            "from_name": ""
          }
        */
        #region Fields & Properties
        private string test_mode;
        [JsonProperty("test_mode")]
        public string Test_mode { get { return test_mode; } set { test_mode = value; } }

        private string welcome_email;
        [JsonProperty("welcome_email")]
        public string Welcome_email { get { return welcome_email; } set { welcome_email = value; } }

        private string new_subscription;
        [JsonProperty("new_subscription")]
        public string New_subscription { get { return new_subscription; } set { new_subscription = value; } }

        private string new_card;
        [JsonProperty("new_card")]
        public string New_card { get { return new_card; } set { new_card = value; } }

        private string payment_receipt;
        [JsonProperty("payment_receipt")]
        public string Payment_receipt { get { return payment_receipt; } set { payment_receipt = value; } }

        private string payment_problem;
        [JsonProperty("payment_problem")]
        public string Payment_problem { get { return payment_problem; } set { payment_problem = value; } }

        private string payment_request;
        [JsonProperty("payment_request")]
        public string Payment_request { get { return payment_request; } set { payment_request = value; } }

        private string email_via;
        [JsonProperty("email_via")]
        public string Email_via { get { return email_via; } set { email_via = value; } }

        private string host;
        [JsonProperty("host")]
        public string Host { get { return host; } set { host = value; } }

        private string username;
        [JsonProperty("username")]
        public string Username { get { return username; } set { username = value; } }

        private string password;
        [JsonProperty("password")]
        public string Password { get { return password; } set { password = value; } }

        private string from_address;
        [JsonProperty("from_address")]
        public string From_address { get { return from_address; } set { from_address = value; } }

        private string from_name;
        [JsonProperty("from_name")]
        public string From_name { get { return from_name; } set { from_name = value; } }

        #endregion

        #region Constructors
        public pwEmailSettingsV2()
        {
            test_mode = "";
            welcome_email = "";
            new_subscription = "";
            new_card = ""; ;
            payment_receipt = "";
            payment_problem = "";
            payment_request = "";
            email_via = "";
            host = "";
            username = "";
            password = "";
            from_address = "";
            from_name = "";
        }

        #endregion

        #region Methods

        #endregion
    }
}