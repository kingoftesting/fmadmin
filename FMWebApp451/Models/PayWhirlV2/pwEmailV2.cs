﻿using System;
using Newtonsoft.Json;

namespace PayWhirlV2
{

    public class pwEmailV2
    {
        /*
        //Example Response
        {
          "id": "1",
          "user_id": "1",
          "type": "",
          "name": "Welcome Email",
          "from_name": "",
          "subject": "Welcome Aboard!",
          "template": "Hello {{ customer.first_name }}!",
          "error_message": "",
          "created_at": "2015-11-01 01:56:25",
          "updated_at": "2015-11-01 02:02:28"
        }
        */
        #region Fields & Properties
        private int id;
        [JsonProperty("id")]
        public int Id { get { return id; } set { id = value; } }

        private int user_id;
        [JsonProperty("user_id")]
        public int user_Id { get { return user_id; } set { user_id = value; } }

        private string type;
        [JsonProperty("type")]
        public string Type { get { return type; } set { type = value; } }

        private string name;
        [JsonProperty("name")]
        public string Name { get { return name; } set { name = value; } }

        private string from_name;
        [JsonProperty("from_name")]
        public string From_name { get { return from_name; } set { from_name = value; } }

        private string subject;
        [JsonProperty("subject")]
        public string Subject { get { return subject; } set { subject = value; } }

        private string template;
        [JsonProperty("template")]
        public string Template { get { return template; } set { template = value; } }

        private string error_message;
        [JsonProperty("error_message")]
        public string Error_message { get { return error_message; } set { error_message = value; } }

        private DateTime created_at;
        [JsonProperty("created_at")]
        public DateTime Created_at { get { return created_at; } set { created_at = value; } }

        private DateTime updated_at;
        [JsonProperty("updated_at")]
        public DateTime Updated_at { get { return updated_at; } set { updated_at = value; } }

        #endregion

        #region Constructors
        public pwEmailV2()
        {
            id = 0;
            user_Id = 0;
            type = "";
            name = "";
            from_name = "";
            subject = "";
            template = "";
            error_message = "";
            created_at = DateTime.MinValue;
            updated_at = DateTime.MinValue;
        }

        #endregion

        #region Methods

        ///
        /// Get Email template
        /// @param  int id  //Email Template ID
        /// @return Email Tempate Object
        ///
        public static pwEmailV2 GetEmailTemplate(int id)
        {
            string jsonURL = "/email/" + id.ToString();

            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            pwEmailV2 pwEmail = new pwEmailV2();
            try
            {
                pwEmail = JsonConvert.DeserializeObject<pwEmailV2>(apiResult);
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                pwEmail = new pwEmailV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: GetEmailTeplate" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return pwEmail;
        }

        #endregion
    }
}