﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace PayWhirlV2
{
    public class pwPlansV2
    {
        [JsonProperty("plans")]
        public pwPlanV2[] plansV2;

        public pwPlansV2()
        {
            plansV2 = new pwPlanV2[] { };
        }
    }

    public class pwPlanV2
    {
        /*
        //Example Response
        {
          "id": "1",
          "user_id": "1",
          "name": "My Test Plan (with anchor)",
          "setup_fee": "12.00",
          "installments": "0",
          "require_shipping": "1",
          "active": "0",
          "image": "",
          "starting_day": "0",
          "description": "",
          "metadata": "",
          "created_at": "2015-10-24 02:56:45",
          "updated_at": "2015-11-01 21:58:09",
          "billing_amount": "20.00",
          "billing_interval": "3",
          "billing_frequency": "day",
          "trial_days": "0",
          "sku": "455KYUH5",
          "currency": "USD",
          "require_tax": "1",
          "billing_cycle_anchor": "4",
          "deleted_at": null
        }
        */
        #region Fields & Properties
        private int id;
        [JsonProperty("id")]
        public int Id { get { return id; } set { id = value; } }

        private int user_id;
        [JsonProperty("user_id")]
        public int user_Id { get { return user_id; } set { user_id = value; } }

        private string name;
        [JsonProperty("name")]
        public string Name { get { return name; } set { name = value; } }

        private decimal setup_fee;
        [JsonProperty("setup_fee")]
        public decimal Setup_fee { get { return setup_fee; } set { setup_fee = value; } }

        private int installments;
        [JsonProperty("installments")]
        public int Installments { get { return installments; } set { installments = value; } }

        private bool require_shipping;
        [JsonProperty("require_shipping")]
        public bool Require_shipping { get { return require_shipping; } set { require_shipping = value; } }

        private bool active;
        [JsonProperty("active")]
        public bool Active { get { return active; } set { active = value; } }

        private string image;
        [JsonProperty("image")]
        public string Image { get { return image; } set { image = value; } }

        private int starting_day;
        [JsonProperty("starting_day")]
        public int Starting_day { get { return starting_day; } set { starting_day = value; } }

        private string description;
        [JsonProperty("description")]
        public string Description { get { return description; } set { description = value; } }

        private string metadata;
        [JsonProperty("metadata")]
        public string Metadata { get { return metadata; } set { metadata = value; } }

        private DateTime created_at;
        [JsonProperty("created_at")]
        public DateTime Created_at { get { return created_at; } set { created_at = value; } }

        private DateTime updated_at;
        [JsonProperty("updated_at")]
        public DateTime Updated_at { get { return updated_at; } set { updated_at = value; } }

        private decimal billing_amount;
        [JsonProperty("billing_amount")]
        public decimal Billing_amount { get { return billing_amount; } set { billing_amount = value; } }

        private int billing_interval;
        [JsonProperty("billing_interval")]
        public int Billing_interval { get { return billing_interval; } set { billing_interval = value; } }

        private string billing_frequency;
        [JsonProperty("billing_frequency")]
        public string Billing_frequency { get { return billing_frequency; } set { billing_frequency = value; } }

        private int trial_days;
        [JsonProperty("trial_days")]
        public int Trial_days { get { return trial_days; } set { trial_days = value; } }

        private string sku;
        [JsonProperty("sku")]
        public string Sku { get { return sku; } set { sku = value; } }

        private string currency;
        [JsonProperty("currency")]
        public string Currency { get { return currency; } set { currency = value; } }

        private bool require_tax;
        [JsonProperty("require_tax")]
        public bool Require_tax { get { return require_tax; } set { require_tax = value; } }

        private int billing_cycle_anchor;
        [JsonProperty("billing_cycle_anchor")]
        public int Billing_cycle_anchor { get { return billing_cycle_anchor; } set { billing_cycle_anchor = value; } }

        private bool enabled;
        [JsonProperty("enabled")]
        public bool Enabled { get { return enabled; } set { enabled = value; } }

        private string tags;
        [JsonProperty("tags")]
        public string Tags { get { return tags; } set { tags = value; } }

        private DateTime? deleted_at;
        [JsonProperty("deleted_at")]
        public DateTime? Deleted_at { get { return deleted_at; } set { deleted_at = value; } }

        #endregion

        #region Constructors
        public pwPlanV2()
        {
            id = 0;
            user_Id = 0;
            name = "";
            setup_fee = 0;
            installments = 0;
            require_shipping = false;
            active = false;
            image = "";
            starting_day = 0;
            description = "";
            metadata = "";
            created_at = DateTime.MinValue;
            updated_at = DateTime.MinValue;
            billing_amount = 0;
            billing_interval = 0;
            billing_frequency = "";
            trial_days = 0;
            sku = "";
            currency = "USD";
            require_tax = false;
            billing_cycle_anchor = 0;
            enabled = false;
            tags = "";
            deleted_at = null;

        }

        #endregion

        #region Methods

        ///
        /// Get a plan
        /// @param  int id  //Plan ID
        /// @return Plan Object
        ///
        public static pwPlanV2 GetPlan(int id)
        {
            string jsonURL = "/plan/" + id.ToString();

            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            pwPlanV2 pwPlan = new pwPlanV2();
            try
            {
                pwPlan = JsonConvert.DeserializeObject<pwPlanV2>(apiResult);
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                pwPlan = new pwPlanV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: GetPlan" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return pwPlan;
        }

        ///
        /// Create a Plan
        /// @param  Plan Data (id = 0; not included in data collection)
        /// @return Plan Object
        ///
        public static pwPlanV2 CreatePlan(pwPlanV2 pwPlan)
        {
            pwPlanV2 newPlan = new pwPlanV2();
            string jsonURL = "/create/plan";

            string jsonStr = JsonConvert.SerializeObject(pwPlan);
            NameValueCollection collection = new NameValueCollection();
            collection.Add("name", pwPlan.Name);
            collection.Add("setup_fee", pwPlan.Setup_fee.ToString());
            collection.Add("installments", pwPlan.Installments.ToString());
            collection.Add("description", pwPlan.Description);
            collection.Add("billing_amount", pwPlan.Billing_amount.ToString());
            collection.Add("billing_interval", pwPlan.Billing_interval.ToString());
            collection.Add("billing_frequency", pwPlan.Billing_frequency);
            collection.Add("trial_days", pwPlan.Trial_days.ToString()); //ie, countryCode: US, GB, CA, AU, etc 
            collection.Add("sku", pwPlan.Sku);
            collection.Add("currency", pwPlan.Currency);
            collection.Add("require_tax", pwPlan.Require_tax.ToString());
            collection.Add("require_shipping", pwPlan.Require_shipping.ToString());
            collection.Add("billing_cycle_anchor", pwPlan.Billing_cycle_anchor.ToString());
            collection.Add("enabled", pwPlan.Enabled.ToString());
            collection.Add("tags", pwPlan.Tags);

            //string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr);
            string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr, collection);
            try
            {
                newPlan = JsonConvert.DeserializeObject<pwPlanV2>(apiResult);
                if (newPlan.Id == 0)
                {
                    //response wasn't a good Product response
                    newPlan = new pwPlanV2(); //return something that won't crash
                    string msg = "error in PayWhirlV2: CreatePlan" + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "jsonStr: " + jsonStr + Environment.NewLine;
                    msg += "response: " + apiResult;
                    string mailFrom = AdminCart.Config.MailFrom();
                    string mailTo = AdminCart.Config.MailTo;
                    string subject = "PayWhirlV2 JSON Error";
                    string contactBody = msg;
                    mvc_Mail.SendMail(mailTo, mailFrom, "", "", subject, contactBody, true);
                }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                newPlan = new pwPlanV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: CreatePlan" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "jsonStr: " + jsonStr + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return newPlan;
        }

        ///
        /// Update a plan
        /// @param  Plan Data (id != 0)
        /// @return Plan Object
        ///
        public static pwPlanV2 UpdatePlan(pwPlanV2 pwPlan)
        {
            pwPlanV2 newPlan = new pwPlanV2();
            string jsonURL = "/update/plan";

            string jsonStr = JsonConvert.SerializeObject(pwPlan);
            NameValueCollection collection = new NameValueCollection();
            collection.Add("id", pwPlan.Id.ToString());
            collection.Add("name", pwPlan.Name);
            collection.Add("setup_fee", pwPlan.Setup_fee.ToString());
            collection.Add("installments", pwPlan.Installments.ToString());
            collection.Add("description", pwPlan.Description);
            collection.Add("billing_amount", pwPlan.Billing_amount.ToString());
            collection.Add("billing_interval", pwPlan.Billing_interval.ToString());
            collection.Add("billing_frequency", pwPlan.Billing_frequency);
            collection.Add("trial_days", pwPlan.Trial_days.ToString()); //ie, countryCode: US, GB, CA, AU, etc 
            collection.Add("sku", pwPlan.Sku);
            collection.Add("currency", pwPlan.Currency);
            collection.Add("require_tax", pwPlan.Require_tax.ToString());
            collection.Add("require_shipping", pwPlan.Require_shipping.ToString());
            collection.Add("billing_cycle_anchor", pwPlan.Billing_cycle_anchor.ToString());
            collection.Add("enabled", pwPlan.Enabled.ToString());
            collection.Add("tags", pwPlan.Tags);

            //string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr);
            string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr, collection);
            try
            {
                newPlan = JsonConvert.DeserializeObject<pwPlanV2>(apiResult);
                if (newPlan.Id == 0)
                {
                    //response wasn't a good Product response
                    newPlan = new pwPlanV2(); //return something that won't crash
                    string msg = "error in PayWhirlV2: CreatePlan" + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "jsonStr: " + jsonStr + Environment.NewLine;
                    msg += "response: " + apiResult;
                    string mailFrom = AdminCart.Config.MailFrom();
                    string mailTo = AdminCart.Config.MailTo;
                    string subject = "PayWhirlV2 JSON Error";
                    string contactBody = msg;
                    mvc_Mail.SendMail(mailTo, mailFrom, "", "", subject, contactBody, true);
                }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                newPlan = new pwPlanV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: CreatePlan" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "jsonStr: " + jsonStr + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return newPlan;
        }

        public static List<pwPlanV2> GetPlanListV2()
        {
            List<pwPlanV2> planList = new List<pwPlanV2>();
            pwPlansV2 plans = new pwPlansV2();
            pwPlanV2 plan = new pwPlanV2();

            string jsonURL = "/plans";
            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            try
            {
                string jsonResult = @"{""plans"":" + apiResult + "}";
                plans = JsonConvert.DeserializeObject<pwPlansV2>(jsonResult);
                for (int i = 0; i < plans.plansV2.Length; i++)
                { planList.Add(plans.plansV2[i]); }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                planList = new List<pwPlanV2>(); //return something that won't crash
                string msg = "error in PayWhirlV2: GetPlanList" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            return planList;
        }

        #endregion
    }
}