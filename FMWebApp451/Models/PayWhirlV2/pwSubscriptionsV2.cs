﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace PayWhirlV2
{
    public class pwSubscriptionsV2
    {
        [JsonProperty("subscriptions")]
        public pwSubscriptionV2[] subscriptionsV2;

        public pwSubscriptionsV2()
        {
            subscriptionsV2 = new pwSubscriptionV2[] { };
        }
    }

    public class pwSubscriptionV2
    {
        /*
        /Example Response
            {
              "id": "34",
              "user_id": "1",
              "customer_id": "2",
              "plan_id": "1",
              "quantity": "1",
              "cancel_at_period_end": "0",
              "current_period_start": "1446344697",
              "current_period_end": "1446344697",
              "trial_start": "0",
              "trial_end": "0",
              "installment_plan": "0",
              "installments_left": "0",
              "created_at": "2015-11-01 02:24:57",
              "updated_at": "2015-11-01 02:24:57",
              "deleted_at": null
            }
        */
        #region Fields & Properties
        private int id;
        [JsonProperty("id")]
        public int Id { get { return id; } set { id = value; } }

        private int user_id;
        [JsonProperty("user_id")]
        public int user_Id { get { return user_id; } set { user_id = value; } }

        private int customer_id;
        [JsonProperty("customer_id")]
        public int Customer_id { get { return customer_id; } set { customer_id = value; } }

        private int plan_id;
        [JsonProperty("plan_id")]
        public int Plan_id { get { return plan_id; } set { plan_id = value; } }

        private int promo_id;
        [JsonProperty("promo_id")]
        public int Promo_id { get { return promo_id; } set { promo_id = value; } }

        private int quantity;
        [JsonProperty("quantity")]
        public int Quantity { get { return quantity; } set { quantity = value; } }

        private bool cancel_at_period_end;
        [JsonProperty("cancel_at_period_end")]
        public bool Cancel_at_period_end { get { return cancel_at_period_end; } set { cancel_at_period_end = value; } }

        private DateTime current_period_start;
        [JsonProperty("current_period_start")]
        public DateTime Current_period_start { get { return current_period_start; } set { current_period_start = value; } }

        private DateTime current_period_end;
        [JsonProperty("current_period_end")]
        public DateTime Current_period_end { get { return current_period_end; } set { current_period_end = value; } }

        private DateTime trial_start;
        [JsonProperty("trial_start")]
        public DateTime Trial_start { get { return trial_start; } set { trial_start = value; } }

        private DateTime trial_end;
        [JsonProperty("trial_end")]
        public DateTime Trial_end { get { return trial_end; } set { trial_end = value; } }

        private int installment_plan;
        [JsonProperty("installment_plan")]
        public int Installment_plan { get { return installment_plan; } set { installment_plan = value; } }

        private int installments_left;
        [JsonProperty("installments_left")]
        public int Installments_left { get { return installments_left; } set { installments_left = value; } }

        private DateTime created_at;
        [JsonProperty("created_at")]
        public DateTime Created_at { get { return created_at; } set { created_at = value; } }

        private DateTime updated_at;
        [JsonProperty("updated_at")]
        public DateTime Updated_at { get { return updated_at; } set { updated_at = value; } }

        private DateTime? deleted_at;
        [JsonProperty("deleted_at")]
        public DateTime? Deleted_at { get { return deleted_at; } set { deleted_at = value; } }

        #endregion

        #region Constructors
        public pwSubscriptionV2()
        {
            id = 0;
            user_Id = 0;
            customer_id = 0;
            plan_id = 0;
            promo_id = 0;
            quantity = 0;
            cancel_at_period_end = false;
            current_period_start = DateTime.MinValue;
            current_period_end = DateTime.MinValue;
            trial_start = DateTime.MinValue;
            trial_end = DateTime.MinValue;
            installment_plan = 0;
            installments_left = 0;
            created_at = DateTime.MinValue;
            updated_at = DateTime.MinValue;
            deleted_at = null;
        }

        #endregion

        #region Methods

        ///
        /// Get a Subscription
        /// @param  int id  //Subscription ID
        /// @return Subscription Object
        ///
        public static pwSubscriptionV2 GetSubscription(int id)
        {
            string jsonURL = "/subscription/" + id.ToString();

            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            pwSubscriptionV2 pwSubscription = new pwSubscriptionV2();
            try
            {
                pwSubscription = JsonConvert.DeserializeObject<pwSubscriptionV2>(apiResult);
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                pwSubscription = new pwSubscriptionV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: GetSubscription" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return pwSubscription;
        }

        ///
        /// Create a Subscription
        /// @param  Subscription Data (id = 0; not included in data collection)
        /// @return Subscription Object
        ///
        public static pwSubscriptionV2 CreateSubscription(pwSubscriptionV2 pwSubscription)
        {
            pwSubscriptionV2 newSubscription = new pwSubscriptionV2();
            string jsonURL = "/subscribe/customer";

            string jsonStr = JsonConvert.SerializeObject(pwSubscription);
            NameValueCollection collection = new NameValueCollection();
            collection.Add("customer_id", pwSubscription.Customer_id.ToString());
            collection.Add("plan_id", pwSubscription.Plan_id.ToString());
            collection.Add("quantity", pwSubscription.Quantity.ToString());
            collection.Add("promo_id", pwSubscription.Promo_id.ToString());
            collection.Add("trial_end", pwSubscription.trial_end.ToString());

            //string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr);
            string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr, collection);
            try
            {
                newSubscription = JsonConvert.DeserializeObject<pwSubscriptionV2>(apiResult);
                if (newSubscription.Id == 0)
                {
                    //response wasn't a good Product response
                    newSubscription = new pwSubscriptionV2(); //return something that won't crash
                    string msg = "error in PayWhirlV2: CreateSubscription" + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "jsonStr: " + jsonStr + Environment.NewLine;
                    msg += "response: " + apiResult;
                    string mailFrom = AdminCart.Config.MailFrom();
                    string mailTo = AdminCart.Config.MailTo;
                    string subject = "PayWhirlV2 JSON Error";
                    string contactBody = msg;
                    mvc_Mail.SendMail(mailTo, mailFrom, "", "", subject, contactBody, true);
                }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                newSubscription = new pwSubscriptionV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: CreateSubscription" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "jsonStr: " + jsonStr + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return newSubscription;
        }

        ///
        /// Update a Subscription
        /// @param  Subscription Data (id != 0)
        /// @return Subscription Object
        ///
        public static pwSubscriptionV2 UpdateSubscription(pwSubscriptionV2 pwSubscription)
        {
            pwSubscriptionV2 newSubscription = new pwSubscriptionV2();
            string jsonURL = "/update/subscription";

            string jsonStr = JsonConvert.SerializeObject(pwSubscription);
            NameValueCollection collection = new NameValueCollection();
            collection.Add("subscription_id", pwSubscription.Id.ToString());
            collection.Add("plan_id", pwSubscription.Plan_id.ToString());

            //string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr);
            string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr, collection);
            try
            {
                newSubscription = JsonConvert.DeserializeObject<pwSubscriptionV2>(apiResult);
                if (newSubscription.Id == 0)
                {
                    //response wasn't a good Product response
                    newSubscription = new pwSubscriptionV2(); //return something that won't crash
                    string msg = "error in PayWhirlV2: UpdateSubscription" + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "jsonStr: " + jsonStr + Environment.NewLine;
                    msg += "response: " + apiResult;
                    string mailFrom = AdminCart.Config.MailFrom();
                    string mailTo = AdminCart.Config.MailTo;
                    string subject = "PayWhirlV2 JSON Error";
                    string contactBody = msg;
                    mvc_Mail.SendMail(mailTo, mailFrom, "", "", subject, contactBody, true);
                }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                newSubscription = new pwSubscriptionV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: UpdateSubscription" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "jsonStr: " + jsonStr + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return newSubscription;
        }

        ///
        /// Update a Subscription
        /// @param  Subscription Data (id != 0)
        /// @return Subscription Object
        ///
        public static pwSubscriptionV2 DeleteSubscription(pwSubscriptionV2 pwSubscription)
        {
            pwSubscriptionV2 newSubscription = new pwSubscriptionV2();
            string jsonURL = "/unsubscribe/subscription";

            string jsonStr = JsonConvert.SerializeObject(pwSubscription);
            NameValueCollection collection = new NameValueCollection();
            collection.Add("subscription_id", pwSubscription.Id.ToString());

            //string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr);
            string apiResult = JsonPayWhirlRequest.POST(jsonURL, jsonStr, collection);
            try
            {
                newSubscription = JsonConvert.DeserializeObject<pwSubscriptionV2>(apiResult);
                if (newSubscription.Id == 0)
                {
                    //response wasn't a good Product response
                    newSubscription = new pwSubscriptionV2(); //return something that won't crash
                    string msg = "error in PayWhirlV2: DeleteSubscription" + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "jsonStr: " + jsonStr + Environment.NewLine;
                    msg += "response: " + apiResult;
                    string mailFrom = AdminCart.Config.MailFrom();
                    string mailTo = AdminCart.Config.MailTo;
                    string subject = "PayWhirlV2 JSON Error";
                    string contactBody = msg;
                    mvc_Mail.SendMail(mailTo, mailFrom, "", "", subject, contactBody, true);
                }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                newSubscription = new pwSubscriptionV2(); //return something that won't crash
                string msg = "error in PayWhirlV2: DeleteSubscription" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "jsonStr: " + jsonStr + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return newSubscription;
        }

        public static List<pwSubscriptionV2> GetSubscriptionListV2(int id)
        {
            List<pwSubscriptionV2> subscriptionList = new List<pwSubscriptionV2>();
            pwSubscriptionsV2 subscriptions = new pwSubscriptionsV2();
            pwSubscriptionV2 subscription = new pwSubscriptionV2();

            string jsonURL = "/subscriptions/" + id.ToString();
            string apiResult = JsonPayWhirlRequest.GET(jsonURL);
            try
            {
                string jsonResult = @"{""subscriptions"":" + apiResult + "}";
                subscriptions = JsonConvert.DeserializeObject<pwSubscriptionsV2>(jsonResult);
                for (int i = 0; i < subscriptions.subscriptionsV2.Length; i++)
                { subscriptionList.Add(subscriptions.subscriptionsV2[i]); }
            }
            catch (Exception ex)
            {
                //response wasn't a good Product response
                subscriptionList = new List<pwSubscriptionV2>(); //return something that won't crash
                string msg = "error in PayWhirlV2: GetSubscriptionList" + Environment.NewLine;
                msg += "jsonURL: " + jsonURL + Environment.NewLine;
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            return subscriptionList;
        }

        #endregion
    }
}