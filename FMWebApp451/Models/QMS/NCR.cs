﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using FM2015.Helpers;

namespace FM2015.Models.QMS
{
    public class NCR
    {
        static private string CERConnString = AdminCart.Config.ConnStrCER();

        private int ncrTrackingID;
        public int NCRTrackingID { get { return ncrTrackingID; } set { ncrTrackingID = value; } }

        private int ncrNumber;
        public int NCRNumber { get { return ncrNumber; } set { ncrNumber = value; } }

        private DateTime openDate;
        public DateTime OpenDate { get { return openDate; } set { openDate = value; } }

        private DateTime? closeDate;
        public DateTime? CloseDate { get { return closeDate; } set { closeDate = value; } }

        private string description;
        public string Description { get { return description; } set { description = value; } }

        public NCR()
        {
            NCRTrackingID = 0;
            NCRNumber = 0;
            OpenDate = Convert.ToDateTime("1/1/1900");
            CloseDate = Convert.ToDateTime("1/1/1900");
            Description = "";
        }

        public static List<NCR> GetNCRList()
        {
            List<NCR> ncrList = new List<NCR>();
            string sql = @"                       
                        SELECT * 
                        FROM NCRTracking  
                        ORDER BY NCRNumber Desc                         
                    ";

            SqlDataReader dr = DBUtil.FillDataReader(sql, CERConnString);
            while (dr.Read())
            {
                NCR ncr = new NCR();
                ncr.ncrTrackingID = Convert.ToInt32(dr["NCRTrackingID"]);
                ncr.NCRNumber = Convert.ToInt32(dr["NCRNumber"]);
                ncr.OpenDate = Convert.ToDateTime(dr["OpenDate"]);

                if (dr["CloseDate"] == DBNull.Value)
                {
                    ncr.CloseDate = null;
                }
                else
                {
                    ncr.CloseDate = Convert.ToDateTime(dr["CloseDate"]);
                }
                ncr.Description = dr["Description"].ToString();
                ncrList.Add(ncr);
            }
            if (dr != null) { dr.Close(); }

            return ncrList;
        }
    }
}