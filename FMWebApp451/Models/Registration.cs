﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FM2015.Models
{
    public class Registration
    {
        #region Private/Public Vars
        private int registrationId;
        public int RegistrationId { get { return registrationId; } set { registrationId = value; } }
        private string firstName;
        public string FirstName { get { return firstName; } set { firstName = value; } }
        private string lastName;
        public string LastName { get { return lastName; } set { lastName = value; } }
        private string email;
        public string Email { get { return email; } set { email = value; } }
        private string phone;
        public string Phone { get { return phone; } set { phone = value; } }
        private DateTime created;
        public DateTime Created { get { return created; } set { created = value; } }
        private long serial;
        public long Serial { get { return serial; } set { serial = value; } }
        private bool confirmed;
        public bool Confirmed { get { return confirmed; } set { confirmed = value; } }
        #endregion

        public Registration()
        {
            registrationId = 0;
            firstName = string.Empty;
            lastName = string.Empty;
            email = string.Empty;
            phone = string.Empty;
            created = Convert.ToDateTime("1/1/1900");
            serial = 0;
            confirmed = false;
        }
    }
}