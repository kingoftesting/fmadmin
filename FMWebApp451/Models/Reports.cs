﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using AdminCart;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using FMWebApp451.Interfaces;
using FMWebApp451.ViewModels;

namespace FM2015.Models
{
    public class Sales
    {

        #region Properties
        public int DailySalesID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        [Display(Name = "Orders")]
        public int TotalOrders { get; set; }

        [Display(Name = "Units")]
        public int TotalUnits { get; set; }

        [Display(Name = "Devices")]
        public int TotalDevices { get; set; }

        [Display(Name = "Refills")]
        public int TotalRefills { get; set; }

        [Display(Name = "Multi-Pay Devices")]
        public int TotalMPDevices { get; set; }

        [Display(Name = "Gross Revenue")]
        [DataType(DataType.Currency)]
        public decimal TotalSinglePayGrossRevenue { get; set; }

        [Display(Name = "(almost) Gross Revenue")]
        [DataType(DataType.Currency)]
        public decimal TotalSinglePayGrossRevenueCash { get; set; }

        [Display(Name = "Multi-Pay: 1-Pay Revenue")]
        [DataType(DataType.Currency)]
        public decimal Mp1PayRevenue { get; set; }

        [Display(Name = "Adj Gross Revenue")]
        [DataType(DataType.Currency)]
        public decimal TotalGrossRevenue { get; set; }

        [DataType(DataType.Currency)]
        public decimal tstTotalGrossRevenue { get; set; }

        [Display(Name = "Adj Gross Cash")]
        [DataType(DataType.Currency)]
        public decimal TotalGrossCash { get; set; }

        [Display(Name = "Multi-Pay: Revenue")]
        [DataType(DataType.Currency)]
        public decimal MpRevenue { get; set; }

        [Display(Name = "+other CS")]
        [DataType(DataType.Currency)]
        public decimal TotalOtherCS { get; set; }

        [Display(Name = "-CS LI Discounts")]
        [DataType(DataType.Currency)]
        public decimal TotalLineItemDiscounts { get; set; }

        [DataType(DataType.Currency)]
        public decimal tstTotalLineItemDiscounts { get; set; }

        [Display(Name = "-Shopify 1Pay Discounts")]
        [DataType(DataType.Currency)]
        public decimal TotalDiscounts { get; set; }
        
        [Display(Name = "MultiPay Discounts")]
        [DataType(DataType.Currency)]
        public decimal MpDiscounts { get; set; }

        [Display(Name = "MultiPay (cash) Discounts")]
        [DataType(DataType.Currency)]
        public decimal MpCashDiscounts { get; set; }

        [Display(Name = "MultiPay Net Revenue")]
        [DataType(DataType.Currency)]
        public decimal MpNetRevenue { get; set; }

        [Display(Name = "MultiPay Collected")]
        [DataType(DataType.Currency)]
        public decimal MpCollected { get; set; }

        [Display(Name = "MultiPay ToBeCollected")]
        [DataType(DataType.Currency)]
        public decimal MpToBeCollected { get; set; }

        [Display(Name = "Net Revenue")]
        [DataType(DataType.Currency)]
        public decimal TotalNetRevenue { get; set; }

        [Display(Name = "Net Cash")]
        [DataType(DataType.Currency)]
        public decimal TotalNetCash { get; set; }

        [Display(Name = "Total Cost")]
        [DataType(DataType.Currency)]
        public decimal TotalCost { get; set; }

        [Display(Name = "Margin")]
        [DataType(DataType.Currency)]
        public decimal TotalMargin { get; set; }

        [Display(Name = "Sales Tax")]
        [DataType(DataType.Currency)]
        public decimal TotalSalesTax { get; set; }

        [Display(Name = "Shipping")]
        [DataType(DataType.Currency)]
        public decimal TotalShipping { get; set; }

        [Display(Name = "Refunds")]
        [DataType(DataType.Currency)]
        public decimal TotalRefunds { get; set; }

        [Display(Name = "Voids")]
        [DataType(DataType.Currency)]
        public decimal TotalVoids { get; set; }

        [Display(Name = "Collected(curr sales)")]
        [DataType(DataType.Currency)]
        public decimal TotalCollectedWebSite { get; set; }

        [Display(Name = "Collected(+mp pmnts)")]
        [DataType(DataType.Currency)]
        public decimal TotalCollectedCC { get; set; }

        [Display(Name = "Gross Reconcile")]
        [DataType(DataType.Currency)]
        public decimal GrossReconcile { get; set; }

        [Display(Name = "Gross Revenue")]
        [DataType(DataType.Currency)]
        public decimal GrossRevRevenueReconcile { get; set; }

        [Display(Name = "adjGross Revenue")]
        [DataType(DataType.Currency)]
        public decimal adjGrossRevRevenueReconcile { get; set; }

        [Display(Name = "LI Discount")]
        [DataType(DataType.Currency)]
        public decimal LineItemDiscountReconcile { get; set; }

        [Display(Name = "Cancelled")]
        [DataType(DataType.Currency)]
        public decimal MpCancelled { get; set; }

        [Display(Name = "Past Due")]
        [DataType(DataType.Currency)]
        public decimal MpUnCollectable { get; set; }

        [Display(Name = "Written OFf")]
        [DataType(DataType.Currency)]
        public decimal MpWrittenOff { get; set; }

        [Display(Name = "Collected")]
        [DataType(DataType.Currency)]
        public decimal CollectedReconcile { get; set; }
        
        [Display(Name = "Gift Cards")]
        [DataType(DataType.Currency)]
        public decimal TotalGiftCardRevenue { get; set; }

        public List<Order> OrderList;
        public List<OrderDetail> OrderDetailList;
        public List<Transaction> TransactionList;
        public List<MultiPayRevenueByOrder> mpRevenueByOrder;
        public List<SalesDetails> SalesDetailsList;
        public List<Transaction> OtherCSTransactions;
        public List<Order> GrossMismatchList;
        public List<Order> TransAmountMismatchList;
        public decimal[] TransAmountsFromPriorMonths;
        public List<MultiPayTransactionsByDate> mpTransactionList;
        public List<OrderDetail> liDiscountList;

        #endregion

        #region Constructors
        public Sales()
        {
            DailySalesID = 0;
            OrderList = new List<Order>();
            OrderDetailList = new List<OrderDetail>();
            TransactionList = new List<Transaction>();
            mpRevenueByOrder = new List<MultiPayRevenueByOrder>();
            SalesDetailsList = new List<SalesDetails>();
            OtherCSTransactions = new List<Transaction>();
            GrossMismatchList = new List<Order>();
            TransAmountMismatchList = new List<Order>();
            TransAmountsFromPriorMonths = new decimal[] { 0M, 0M, 0M, 0M, 0M, 0M };
            mpTransactionList = new List<MultiPayTransactionsByDate>();
            liDiscountList = new List<OrderDetail>();

            StartDate = Convert.ToDateTime("1/1/1900");
            EndDate = Convert.ToDateTime("1/1/1900");
            TotalOrders = 0;
            TotalUnits = 0;
            TotalDevices = 0;
            TotalRefills = 0;
            TotalMPDevices = 0;
            TotalSinglePayGrossRevenue = 0;
            TotalSinglePayGrossRevenueCash = 0;
            Mp1PayRevenue = 0;
            TotalGrossRevenue = 0;
            tstTotalGrossRevenue = 0;
            TotalGrossCash = 0;
            MpRevenue = 0;
            MpDiscounts = 0;
            MpCashDiscounts = 0;
            MpNetRevenue = 0;
            MpCollected = 0;
            MpToBeCollected = 0;
            MpCancelled = 0;
            MpUnCollectable = 0;
            MpWrittenOff = 0;
            TotalOtherCS = 0;
            TotalLineItemDiscounts = 0;
            tstTotalLineItemDiscounts = 0;
            TotalDiscounts = 0;
            TotalNetRevenue = 0;
            TotalCost = 0;
            TotalMargin = 0;
            TotalSalesTax = 0;
            TotalShipping = 0;
            TotalRefunds = 0;
            TotalVoids = 0;
            TotalCollectedWebSite = 0;
            TotalCollectedCC = 0;
            GrossReconcile = 0;
            GrossRevRevenueReconcile = 0;
            adjGrossRevRevenueReconcile = 0;
            LineItemDiscountReconcile = 0;
            CollectedReconcile = 0;
            TotalGiftCardRevenue = 0;

        }
        #endregion

    }

    public class SalesDetails : ISalesDetails
    {
        #region Properties
        public int ProductID { get; set; }
        public string SKU { get; set; }
        public string Description { get; set; }

        [DataType(DataType.Currency)]
        public decimal UnitCost { get; set; }

        public int TotalUnits { get; set; }

        [Display(Name = "Gross Revenue")]
        [DataType(DataType.Currency)]
        public decimal TotalGrossRevenue { get; set; }

        [Display(Name = "Discounts")]
        [DataType(DataType.Currency)]
        public decimal TotalDiscount { get; set; }

        [DataType(DataType.Currency)]
        public decimal TotalCost { get; set; }

        [Display(Name = "phUnits")]
        public int PhoneUnits { get; set; }

        [Display(Name = "phRevenue")]
        [DataType(DataType.Currency)]
        public decimal PhoneGrossRevenue { get; set; }

        [Display(Name = "phDiscounts")]
        [DataType(DataType.Currency)]
        public decimal PhoneDiscount { get; set; }
        #endregion

        #region Constructors
        public SalesDetails()
        {
            ProductID = 0;
            SKU = "";
            Description = "";
            UnitCost = 0;
            TotalUnits = 0;
            TotalGrossRevenue = 0;
            TotalDiscount = 0;
            TotalCost = 0;
            PhoneUnits = 0;
            PhoneGrossRevenue = 0;
            PhoneDiscount = 0;
        }

        public List<SalesDetails> GetAll(DateTime startDate, DateTime endDate)
        {
            List<SalesDetails> theList = FMmetrics.mvc_Metrics.GetOrderDetailReport(startDate, endDate);
            return theList;
        }
        #endregion
    }

    public class SalesMan
    {
        #region Properties

        [Display(Name = "Sales")]
        [DataType(DataType.Currency)]
        public decimal Sales { get; set; }

        public User user { get; set; }
        #endregion

        #region Constructors
        public SalesMan()
        {
            Sales = 0;
            user = new User();
        }
        #endregion
    }

    public class DayOfWeekSales
    {
        #region Properties
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [Display(Name = "Gross Revenue")]
        [DataType(DataType.Currency)]
        public decimal TotalRevenue { get; set; }

        [Display(Name = "Net Revenue")]
        [DataType(DataType.Currency)]
        public decimal WebRevenue { get; set; }

        [Display(Name = "Phone Revenue")]
        [DataType(DataType.Currency)]
        public decimal PhoneRevenue { get; set; }

        [Display(Name = "Gross Cash")]
        [DataType(DataType.Currency)]
        public decimal TotalRevenueCash { get; set; }

        [Display(Name = "Net Cash")]
        [DataType(DataType.Currency)]
        public decimal WebRevenueCash { get; set; }

        [Display(Name = "Phone Cash")]
        [DataType(DataType.Currency)]
        public decimal PhoneRevenueCash { get; set; }

        public List<SalesMan> SalesTeamList { get; set; }
        #endregion

        #region Constructors
        public DayOfWeekSales()
        {
            Date = Convert.ToDateTime("1/1/1900");
            TotalRevenue = 0;
            WebRevenue = 0;
            PhoneRevenue = 0;
            TotalRevenueCash = 0;
            WebRevenueCash = 0;
            PhoneRevenueCash = 0;
            SalesTeamList = new List<SalesMan>();
        }
        #endregion

        public static List<DayOfWeekSales> GetDailySales(int month, int year)
        {
            List<DayOfWeekSales> salesList = new List<DayOfWeekSales>();

            //get all sales in date range
            DateTime startDate = new DateTime(year, month, 1);
            DateTime endDate = startDate.AddMonths(1).AddTicks(-1);
            List<Order> orderList = Order.FindAllOrders(startDate, endDate, false);

            List<AdminCart.OrderDetail> orderDetailList = new List<AdminCart.OrderDetail>();
            orderDetailList = AdminCart.Order.GetOrderDetailsList(startDate, endDate);

            //get the daily sales report by salesperson
            List<Product> pList = Product.GetProducts();

            for (int day = 1; day <= DateTime.DaysInMonth(startDate.Year, startDate.Month); day++)
            {
                DayOfWeekSales dailySales = new DayOfWeekSales();

                DateTime date = new DateTime(year, month, day);
                dailySales.Date = date;

                //get all the order for each day of the month
                var dayList = from o in orderList
                              where (o.OrderDate.Day == day)
                              select o;

                dailySales.TotalRevenue = (from o in dayList select o.SubTotal).Sum() -
                    (from o in dayList select o.TotalCoupon).Sum() -
                    (from o in dayList select o.Adjust).Sum();

                List<OrderDetail> odList = new List<OrderDetail>();
                odList = (from od in orderDetailList where (od.OrderID == (from o in dayList select o.OrderID).FirstOrDefault()) select od).ToList();

                //account for gift card sales - subtract from revenue
                decimal gcRevenue = 0;
                foreach (OrderDetail od in odList)
                {
                    Product p = (from p2 in pList where p2.ProductID == od.ProductID select p2).FirstOrDefault();
                    if (p.IsGiftCard)
                    {
                        gcRevenue = od.Quantity * od.UnitPrice;
                    }
                }
                  
                dailySales.TotalRevenue = dailySales.TotalRevenue - gcRevenue;

                dailySales.PhoneRevenue = (from o in dayList where (o.ByPhone == true) select o.SubTotal).Sum() -
                    (from o in dayList where (o.ByPhone == true) select o.TotalCoupon).Sum() -
                    (from o in dayList where (o.ByPhone == true) select o.Adjust).Sum();

                dailySales.PhoneRevenue = dailySales.PhoneRevenue - gcRevenue;

                dailySales.WebRevenue = dailySales.TotalRevenue - dailySales.PhoneRevenue;

                var byPhoneList = (from o in dayList
                                  where (o.ByPhone == true)
                                  group o by o.SourceID into newList
                                  orderby newList.Key
                                  select newList).ToList();

                foreach (var group in byPhoneList)
                {
                    SalesMan salesman = new SalesMan();
                    foreach(var item in group)
                    {
                        if (!dailySales.SalesTeamList.Exists(x => x.user.UserID == item.SourceID))
                        {
                            salesman.user.UserID = item.SourceID;
                            dailySales.SalesTeamList.Add(salesman);
                        }

                        int index = dailySales.SalesTeamList.FindIndex(x => x.user.UserID == item.SourceID);
                        decimal revenue = item.SubTotal - item.TotalCoupon - item.Adjust;
                        salesman.Sales = salesman.Sales + revenue;
                        dailySales.SalesTeamList[index].Sales = salesman.Sales;
                    }
                }
                salesList.Add(dailySales);
            }

            return salesList;
        }
    }

    public class Reports
    {
        public Reports()
        { }

        public static MemoryStream genPackingListPDF(List<FM2015.FMmetrics.UnProcessedOrders> orderList)
        {
            //http://www.afterlogic.com/mailbee-net/docs-itextsharp/Index.html

            //http://daveaglick.com/posts/using-aspnet-mvc-and-razor-to-generate-pdf-files

            var directory = HttpContext.Current.Server.MapPath(Config.genPDFPackingList_StoragePath);
            if (!System.IO.Directory.Exists(directory))
            { Directory.CreateDirectory(directory); }

            string fileName = String.Format("{1}_{0:yyyy-MM-dd-hh-mm-ss}.pdf", DateTime.Now, Config.PackingListPrefix()+"pList");
            string filePath = Path.Combine(directory, fileName);
            string virtualFilePath = Config.genPDFPackingList_StoragePath + fileName;

            // creation of the document with a certain size and certain margins
            Document document = new Document(PageSize.LETTER);
            MemoryStream stream = new MemoryStream();

            try
            {
                // creation of the different writers
                //HtmlWriter.GetInstance(document, System.out);
                //PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));
                PdfWriter writer = PdfWriter.GetInstance(document, stream);
                writer.CloseStream = false;

                // add some meta information to the document
                document.AddAuthor("Rodger Mohme");
                document.AddSubject("Fulfillment Packing List");

                // open the document for writing
                document.Open();
                PdfContentByte cb = writer.DirectContent;

                //table stuff
                PdfPTable table;
                PdfPCell cell;
                iTextSharp.text.Paragraph paragraph;
                Phrase phrase;


                //Establish fonts
                var normalFont = FontFactory.GetFont(FontFactory.HELVETICA, 12);
                var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);

                //Summary Page
                string returnAddress = AdminCart.Config.BizName() + Environment.NewLine;
                returnAddress += AdminCart.Config.ReturnStreet1 + Environment.NewLine;
                returnAddress += AdminCart.Config.ReturnStreet2 + Environment.NewLine;
                returnAddress += AdminCart.Config.ReturnCity + Environment.NewLine;
                returnAddress += AdminCart.Config.AppPhoneNumber() + Environment.NewLine;
                returnAddress += AdminCart.Config.CustomerServiceEmail() + Environment.NewLine;

                var physicalLogoPath = HttpContext.Current.Server.MapPath(AdminCart.Config.AdminImage());
                Image HeaderPageLogo = Image.GetInstance(physicalLogoPath);
               
                switch (AdminCart.Config.appName.ToUpper())
                {
                    case "FACEMASTER":
                        HeaderPageLogo.ScalePercent(50f);
                        break;

                    case "CRYSTALIFT":
                        HeaderPageLogo.ScalePercent(50f);
                        break;

                    case "SONULASE":
                        HeaderPageLogo.ScalePercent(20f);
                        break;

                    case "RMSHOP":
                        HeaderPageLogo.ScalePercent(50f);
                        break;

                    default:
                        break;
                }
                HeaderPageLogo.SetAbsolutePosition(document.PageSize.Width - 36f - 72f,
                        document.PageSize.Height - 36f - 0f);

                //Table for Return Address & Logo
                table = new PdfPTable(2);
                table.SetWidthPercentage(new float[2] { 300f, 300f }, PageSize.LETTER);
                table.HorizontalAlignment = Element.ALIGN_CENTER;

                cell = new PdfPCell(new Paragraph(returnAddress));
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                cell.Border = 0;
                table.AddCell(cell);

                cell = new PdfPCell(HeaderPageLogo);
                cell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                cell.Border = 0;
                table.AddCell(cell);

                document.Add(table);

                // add some space before beginning Summary table
                document.Add(new Paragraph(Environment.NewLine));

                document.Add(new Paragraph("Packing List Summary: " + DateTime.Now.ToString()));
                document.Add(new Paragraph("Order Count: " + orderList.Count().ToString()));
                document.Add(new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))));


                //Table for Orders Summary
                table = new PdfPTable(5);
                //table.SetWidthPercentage(new float[3] { 20.0f, 60.0f, 20.0f }, PageSize.LETTER); //100% total
                table.HorizontalAlignment = Element.ALIGN_CENTER;
                table.TotalWidth = 500f;
                table.LockedWidth = true;
                float[] widths = new float[] { 25f, 75f, 95f, 123f, 182f }; //# Risk? Order# Name Ship To
                table.SetWidths(widths);
                table.SpacingBefore = 20f;
                table.SpacingAfter = 30f;

                //headers
                cell = new PdfPCell(new Paragraph(new Chunk("#", boldFont)));
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                cell.Border = 0;
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(new Chunk("Risk?", boldFont)));
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                cell.Border = 0;
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(new Chunk("Order#", boldFont)));
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                cell.Border = 0;
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(new Chunk("Name", boldFont)));
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                cell.Border = 0;
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(new Chunk("Ship To", boldFont)));
                cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                cell.Border = 0;
                table.AddCell(cell);

                //Order Row Data
                int rowNum = 1;
                foreach(FM2015.FMmetrics.UnProcessedOrders o in orderList)
                {
                    Cart cart = new Cart();
                    cart = cart.GetCartFromOrder(o.OrderId);

                    cell = new PdfPCell(new Paragraph(rowNum.ToString()));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    string riskStr = "";

                    /***** THIS IS A HORRIBLE KLUDGE; SHOULD BE REPLACED WITH A PROPERTY IN ORDER *****/
                    if (cart.ShipAddress.City.Trim().ToUpper() == "**REDDING") //REDDING
                    {
                        riskStr = "Cancel";
                    }
                    else
                    {
                        RiskProvider riskProvider = new RiskProvider();
                        ShopifyAgent.ShopOrderRisk shopRisk = riskProvider.GetByShopifyName(cart.SiteOrder.SSOrderID);
                        if ((shopRisk.Recommendation.ToUpper() == "CANCEL") || (shopRisk.Recommendation.ToUpper() == "INVESTIGATE"))
                        {
                            riskStr = shopRisk.Recommendation;
                        }
                    }

                    cell = new PdfPCell(new Paragraph(riskStr));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(cart.SiteOrder.OrderID.ToString() + "/" + cart.SiteOrder.SSOrderID.ToString()));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    string name = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName;
                    cell = new PdfPCell(new Paragraph(name));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    string shipTo = cart.ShipAddress.City + ", " + cart.ShipAddress.State + " " + Helpers.DBUtil.GetCountry(cart.ShipAddress.Country);
                    cell = new PdfPCell(new Paragraph(shipTo));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    //document.Add(new Paragraph(Environment.NewLine));
                    rowNum++;
                }
                document.Add(table);

                //begin individual packing list pages
                foreach (FM2015.FMmetrics.UnProcessedOrders o in orderList)
                {
                    document.NewPage();

                    //Table for Return Address & Logo (Header)
                    table = new PdfPTable(2);
                    table.SetWidthPercentage(new float[2] { 300f, 300f }, PageSize.LETTER);
                    table.HorizontalAlignment = Element.ALIGN_CENTER;

                    cell = new PdfPCell(new Paragraph(returnAddress));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    cell = new PdfPCell(HeaderPageLogo);
                    cell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    Cart cart = new Cart();
                    cart = cart.GetCartFromOrder(o.OrderId);

                    string orderStuff = "Order#: " + cart.SiteOrder.OrderID.ToString() + "/" + cart.SiteOrder.SSOrderID.ToString() + Environment.NewLine;
                    orderStuff += "Order Date: " + cart.SiteOrder.OrderDate.ToString() + Environment.NewLine;
                    orderStuff += "Ship Date: " + DateTime.Now.ToString() + Environment.NewLine;
                    orderStuff += "Ship Method: Ground" + Environment.NewLine;
                    Address a = Address.LoadAddress(cart.SiteOrder.ShippingAddressID);
                    if ((a.Country.ToUpper() != "US") && (cart.SiteOrder.TotalTax != 0))
                    {
                        orderStuff += "Foreign Tax Paid: " + cart.SiteOrder.TotalTax.ToString("C");
                    }
                    cell = new PdfPCell(new Paragraph(orderStuff));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    string customerStuff = cart.SiteCustomer.FirstName + " " + cart.SiteCustomer.LastName + Environment.NewLine;
                    customerStuff += cart.ShipAddress.Street + Environment.NewLine;
                    string street2 = cart.ShipAddress.Street2.Replace(" ", "");
                    if (!string.IsNullOrEmpty(street2))
                    {
                        customerStuff += cart.ShipAddress.Street2 + Environment.NewLine;
                    }
                    customerStuff += cart.ShipAddress.City + ", " + cart.ShipAddress.State + " " + cart.ShipAddress.Zip + Environment.NewLine;
                    customerStuff += Helpers.DBUtil.GetCountry(cart.ShipAddress.Country) + Environment.NewLine;

                    phrase = new Phrase();
                    phrase.Add(new Chunk("Ship To:", boldFont));
                    phrase.Add(Environment.NewLine);
                    phrase.Add(new Chunk(customerStuff));
                    phrase.Add(Environment.NewLine);
                    cell = new PdfPCell(new Phrase(phrase));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    document.Add(table);

                    ShopOrderNoteProvider shopOrderNoteProvider = new ShopOrderNoteProvider();
                    ShopOrderNote customerOrderNote = shopOrderNoteProvider.GetByShopName(cart.SiteOrder.SSOrderID);
                    if (!string.IsNullOrEmpty(customerOrderNote.ShopNote))
                    {
                        phrase = new Phrase();
                        phrase.Add(new Chunk("Customer Note: ", boldFont));
                        phrase.Add(new Chunk(customerOrderNote.ShopNote));
                        phrase.Add(Environment.NewLine);
                        document.Add(phrase);
                    }

                    phrase = new Phrase();
                    phrase.Add(new Chunk("Contents", boldFont));
                    phrase.Add(Environment.NewLine);
                    document.Add(phrase);
                    document.Add(new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))));

                    //Table Contents list
                    table = new PdfPTable(3);
                    //table.SetWidthPercentage(new float[3] { 20.0f, 60.0f, 20.0f }, PageSize.LETTER); //100% total
                    table.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.TotalWidth = 500f;
                    table.LockedWidth = true;
                    widths = new float[] { 100f, 350f, 50f };
                    table.SetWidths(widths);

                    //headers
                    cell = new PdfPCell(new Paragraph(new Chunk("SKU", boldFont)));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(new Chunk("Description", boldFont)));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(new Chunk("QTY", boldFont)));
                    cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    foreach(Item item in cart.Items)
                    {
                        cell = new PdfPCell(new Paragraph(item.LineItemProduct.Sku));
                        cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                        cell.Border = 0;
                        table.AddCell(cell);

                        cell = new PdfPCell(new Paragraph(item.LineItemProduct.Name));
                        cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                        cell.Border = 0;
                        table.AddCell(cell);

                        cell = new PdfPCell(new Paragraph(item.Quantity.ToString()));
                        cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                        cell.Border = 0;
                        table.AddCell(cell);
                    }

                    document.Add(table);

                    //add some blank lines
                    int rowsBelowFirst = 8;
                    int rowsToAdd = rowsBelowFirst - cart.Items.ItemCount();
                    if (rowsToAdd > 0)
                    {
                        for(int i = 0; i < rowsToAdd; i++)
                        {
                            document.Add(new Paragraph(Environment.NewLine));
                        }
                    }

                    //Table for Return Address & Logo (Footer)
                    table = new PdfPTable(2);
                    table.SetWidthPercentage(new float[2] { 300f, 300f }, PageSize.LETTER);
                    table.HorizontalAlignment = Element.ALIGN_CENTER;

                    /*
                    cell = new PdfPCell(HeaderPageLogo);
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    paragraph = new Paragraph(" " + Environment.NewLine);
                    cell = new PdfPCell(new Paragraph(paragraph));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);
                    */

                    paragraph = new Paragraph(returnAddress + Environment.NewLine);
                    cell = new PdfPCell(new Paragraph(paragraph));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    paragraph = new Paragraph(" " + Environment.NewLine);
                    cell = new PdfPCell(new Paragraph(paragraph));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    phrase = new Phrase();
                    phrase.Add(new Chunk("Ship To:", boldFont));
                    phrase.Add(Environment.NewLine);
                    phrase.Add(new Chunk(customerStuff));
                    phrase.Add(Environment.NewLine);
                    cell = new PdfPCell(new Phrase(phrase));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    phrase = new Phrase();
                    phrase.Add(new Chunk("Return/Exchange Policy", boldFont));
                    string returnPolicy = AdminCart.Config.ReturnExchangePolicy.Replace("**WEBSITE**", AdminCart.Config.StatementDescriptor());
                    phrase.Add(new Chunk(returnPolicy));
                    paragraph = new Paragraph(phrase);
                    cell = new PdfPCell(new Paragraph(paragraph));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    cell.PaddingLeft = 25;
                    table.AddCell(cell);

                    //create code128 barcode
                    Barcode128 bar128 = new Barcode128();
                    bar128.Code = cart.SiteOrder.OrderID.ToString();
                    Image img128 = bar128.CreateImageWithBarcode(cb, null, null);
                    img128.ScalePercent(200f);
                    cell = new PdfPCell(img128);
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    cell = new PdfPCell(img128);
                    cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    phrase = new Phrase();
                    phrase.Add(new Chunk("Ground          Order:" + cart.SiteOrder.OrderID.ToString() + "/" + cart.SiteOrder.SSOrderID.ToString()));
                    paragraph = new Paragraph(phrase);
                    cell = new PdfPCell(new Paragraph(paragraph)); cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(" "));
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    cell.Border = 0;
                    table.AddCell(cell);

                    document.Add(table);

                }

                document.Close();
            }
            catch (DocumentException de)
            {
                Console.Error.WriteLine(de.Message);
                document.Close();
            }

            return stream;
        }

    }

}