﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using AdminCart;
using FM2015.Helpers;

namespace FM2015.Models
{
    public class Roles
    {
        /// <summary>
        /// Summary description for roles
        /// </summary>
            #region Private/Public Vars
            private int id;
            public int ID { get { return id; } set { id = value; } }
            private int customerid;
            public int Customerid { get { return customerid; } set { customerid = value; } }
            private string role;
            public string Role { get { return role; } set { role = value; } }
            private string lastname;
            public string Lastname { get { return lastname; } set { lastname = value; } }
            private string firstname;
            public string Firstname { get { return firstname; } set { firstname = value; } }
            #endregion

            public Roles()
            {
                ID = 0;
                Customerid = 0;
                Role = "n/a";
                Lastname = "n/a";
                Firstname = "n/a";
            }

            public Roles(int customerID)
            {
                string sql = @" 
            SELECT * FROM  fm_roles 
            WHERE CustomerID = @CustomerID 
        ";
                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, customerID);
                SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read())
                {
                    ID = Convert.ToInt32(dr["ID"]);
                    Customerid = Convert.ToInt32(dr["Customerid"].ToString());
                    Role = dr["Role"].ToString();
                    Lastname = dr["Lastname"].ToString();
                    Firstname = dr["Firstname"].ToString();
                }
                if (dr != null) { dr.Close(); }
            }

            public int Save(int ID)
            {
                int result = 0;
                SqlParameter[] mySqlParameters = null;
                SqlDataReader dr = null;
                if (ID == 0)
                {
                    string sql = @" 
                        INSERT INTO fm_roles (Customerid, Role, Lastname, Firstname) 
                        VALUES (@Customerid, @Role, @Lastname, @Firstname); 
                        SELECT ID=@@identity;  
                    ";
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, Customerid, Role, Lastname, Firstname);
                    dr = DBUtil.FillDataReader(sql, mySqlParameters);
                    if (dr.Read())
                    {
                        result = Convert.ToInt32(dr["ID"]);
                    }
                    else
                    {
                        result = 99;
                    }
                }
                else
                {
                    string sql = @" 
                        UPDATE fm_roles 
                        SET  
                            Customerid = @Customerid,  
                            Role = @Role,  
                            Lastname = @Lastname,  
                            Firstname = @Firstname  
                        WHERE ID = @ID  
                    ";
                    mySqlParameters = DBUtil.BuildParametersFrom(sql, Customerid, Role, Lastname, Firstname, ID);
                    DBUtil.Exec(sql, mySqlParameters);
                }
                return result;
            }

            /***********************************
            * Static methods
            ************************************/

            /// <summary>
            /// Returns a collection with all roles
            /// </summary>
            public static List<Roles> GetrolesList()
            {
                List<Roles> thelist = new List<Roles>();

                string sql = @" 
                    SELECT * FROM fm_roles 
                ";

                SqlDataReader dr = DBUtil.FillDataReader(sql);

                while (dr.Read())
                {
                    Roles obj = new Roles();

                    obj.ID = Convert.ToInt32(dr["ID"]);
                    obj.Customerid = Convert.ToInt32(dr["Customerid"].ToString());
                    obj.Role = dr["Role"].ToString();
                    obj.Lastname = dr["Lastname"].ToString();
                    obj.Firstname = dr["Firstname"].ToString();

                    thelist.Add(obj);
                }
                if (dr != null) { dr.Close(); }
                return thelist;
            }

            /// <summary>
            /// Returns a className object with the specified ID
            /// </summary>

            public static Roles GetrolesByID(int id)
            {
                Roles obj = new Roles(id);
                return obj;
            }

            /// <summary>
            /// Updates an existing roles
            /// </summary>
            public static bool Updateroles(int id, int customerid, string role)
            {
                Customer c = new Customer(customerid);
                Roles obj = new Roles();

                obj.ID = id;
                obj.Customerid = customerid;
                obj.Role = role;
                obj.Lastname = c.LastName;
                obj.Firstname = c.FirstName;
                int ret = obj.Save(obj.ID);
                return (ret > 0) ? true : false;
            }

            /// <summary>
            /// Creates a new roles
            /// </summary>
            public static int Insertroles(int customerid, string role)
            {
                Customer c = new Customer(customerid);
                Roles obj = new Roles();

                obj.Customerid = customerid;
                obj.Role = role;
                obj.Lastname = c.FirstName;
                obj.Firstname = c.LastName;
                int ret = obj.Save(obj.ID);
                return ret;
            }

            static public string GetRole(int customerID)
            {

                string sql = @"
            SELECT Role
            FROM fm_Roles
            WHERE CustomerID = @id 
         ";

                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, customerID);
                SqlDataReader dr = dr = DBUtil.FillDataReader(sql, mySqlParameters);

                string role = "";
                if (dr.Read()) { role += dr["Role"].ToString(); }
                role = role.ToUpper();
                if (dr != null) { dr.Close(); }
                return role;
            }

        static public List<string> GetUserRoleList(int customerID)
        {
            List<string> theList = new List<string>();

            string sql = @"
            SELECT Role
            FROM fm_Roles
            WHERE CustomerID = @id 
         ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, customerID);
            SqlDataReader dr = dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                string roleStr = dr["Role"].ToString();
                String[] roleList = roleStr.Split(',');
                foreach (string role in roleList)
                {
                    theList.Add(role.ToUpper());
                }
            }
            if (dr != null) { dr.Close(); }
            return theList;
        }

        static public DataView GetFromRoles(int customerid)
            {

                string sql = @"
            SELECT *
            FROM fm_Roles
            WHERE CustomerID = @customerid 
         ";

                SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, customerid);
                DataSet ds = DBUtil.FillDataSet(sql, "RolesTable", mySqlParameters);
                return ds.Tables["RolesTable"].DefaultView;
            }

            static public DataView GetFromRoles()
            {
                string sql = @"
            SELECT *
            FROM fm_Roles
         ";

                DataSet ds = DBUtil.FillDataSet(sql, "RolesTable");
                return ds.Tables["RolesTable"].DefaultView;
            }

        }
    }
