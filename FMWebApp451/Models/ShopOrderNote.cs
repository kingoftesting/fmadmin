﻿
namespace FM2015.Models
{
    public class ShopOrderNote
    {
        #region Private/Public Vars
        public int ShopOrderName { get; set; }
        public string ShopNote { get; set; }
        #endregion

        public ShopOrderNote()
        {
            ShopOrderName = 0;
            ShopNote = string.Empty;
        }
    }
}