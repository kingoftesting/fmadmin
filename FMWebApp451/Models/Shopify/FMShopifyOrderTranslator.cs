﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FMWebApp.Helpers;

public class FMShopifyOrderTranslator
{
    public FMShopifyOrderTranslator()
    {
    }

    public static void CreateShopifyOrder(int fmOrderID)
    {
        if (!AdminCart.Config.SyncShopify)
        {
            return;
        }

        AdminCart.Cart cart = new AdminCart.Cart();
        cart = cart.GetCartFromOrder(fmOrderID, false);

        ShopifyAgent.ShopifyOrder shopOrder = new ShopifyAgent.ShopifyOrder();

        shopOrder.Subtotal_price = cart.TotalProduct + cart.TotalDiscounts;
        shopOrder.Total_price = cart.Total;
        shopOrder.Total_price_usd = cart.Total;
        //shopOrder.Total_discounts = cart.TotalDiscounts;
        shopOrder.Total_tax = cart.TotalTax;
        shopOrder.Taxes_included = false;
        shopOrder.Currency = "USD";

        string jsonCustomerTemplate = @"{""first_name"":""#FIRST#"",""last_name"":""#LAST#"",""first_name"":""#FIRST#"",
            ""email"":""#EMAIL#"",""verified_email"":""true"", ""default_addresses"":#ADDRESS#}";
        string jsonCustomer = "";
        jsonCustomer = jsonCustomerTemplate;
        jsonCustomer = jsonCustomer.Replace("\r", "");
        jsonCustomer = jsonCustomer.Replace("\n", "");
        jsonCustomer = jsonCustomer.Replace(" ", "");

        jsonCustomer = jsonCustomer.Replace("#FIRST#", cart.SiteCustomer.FirstName);
        jsonCustomer = jsonCustomer.Replace("#LAST#", cart.SiteCustomer.LastName);
        jsonCustomer = jsonCustomer.Replace("#EMAIL#", cart.SiteCustomer.Email);
        jsonCustomer = jsonCustomer.Replace("#EMAIL#", cart.SiteCustomer.Email);

        shopOrder.ShopifyBillingAddress.First_name = cart.SiteCustomer.FirstName;
        shopOrder.ShopifyBillingAddress.Last_name = cart.SiteCustomer.LastName;
        shopOrder.ShopifyBillingAddress.Address1 = cart.BillAddress.Street;
        shopOrder.ShopifyBillingAddress.Address2 = cart.BillAddress.Street2;
        shopOrder.ShopifyBillingAddress.City = cart.BillAddress.City;
        shopOrder.ShopifyBillingAddress.Province = cart.BillAddress.State;
        shopOrder.ShopifyBillingAddress.Country = cart.BillAddress.Country;
        shopOrder.ShopifyBillingAddress.Zip = cart.BillAddress.Zip;
        shopOrder.ShopifyBillingAddress.Phone = cart.SiteCustomer.Phone;
        shopOrder.Email = cart.SiteCustomer.Email;

        string jsonAddrTemplate = @"{""first_name"":""#FIRST#"",""last_name"":""#LAST#"",""address1"":""#ADDRESS1#"",""address2"":""#ADDRESS2#"",
            ""city"":""#CITY#"",""province"":""#PROVINCE#"",""country"":""#COUNTRY#"",""phone"":""#PHONE#"",""zip"":""#ZIP#"",""email"":""#EMAIL#""}";
        string jsonBillingAddr = "";
        string jsonShippingAddr = "";
        jsonBillingAddr = jsonAddrTemplate;
        jsonBillingAddr = jsonBillingAddr.Replace("\r", "");
        jsonBillingAddr = jsonBillingAddr.Replace("\n", "");
        jsonBillingAddr = jsonBillingAddr.Replace(" ", "");

        jsonBillingAddr = jsonBillingAddr.Replace("#FIRST#", cart.SiteCustomer.FirstName);
        jsonBillingAddr = jsonBillingAddr.Replace("#LAST#", cart.SiteCustomer.LastName);
        jsonBillingAddr = jsonBillingAddr.Replace("#ADDRESS1#", cart.BillAddress.Street);
        jsonBillingAddr = jsonBillingAddr.Replace("#ADDRESS2#", cart.BillAddress.Street2);
        jsonBillingAddr = jsonBillingAddr.Replace("#CITY#", cart.BillAddress.City);
        jsonBillingAddr = jsonBillingAddr.Replace("#PROVINCE#", cart.BillAddress.State);
        jsonBillingAddr = jsonBillingAddr.Replace("#COUNTRY#", cart.BillAddress.Country);
        jsonBillingAddr = jsonBillingAddr.Replace("#ZIP#", cart.BillAddress.Zip);
        jsonBillingAddr = jsonBillingAddr.Replace("#PHONE#", cart.SiteCustomer.Phone);
        jsonBillingAddr = jsonBillingAddr.Replace("#EMAIL#", cart.SiteCustomer.Email);

        shopOrder.ShopifyShippingAddress.First_name = cart.SiteCustomer.FirstName;
        shopOrder.ShopifyShippingAddress.Last_name = cart.SiteCustomer.LastName;
        shopOrder.ShopifyShippingAddress.Address1 = cart.ShipAddress.Street;
        shopOrder.ShopifyShippingAddress.Address2 = cart.ShipAddress.Street2;
        shopOrder.ShopifyShippingAddress.City = cart.ShipAddress.City;
        shopOrder.ShopifyShippingAddress.Province = cart.ShipAddress.State;
        shopOrder.ShopifyShippingAddress.Country = cart.ShipAddress.Country;
        shopOrder.ShopifyShippingAddress.Zip = cart.ShipAddress.Zip;
        shopOrder.ShopifyShippingAddress.Phone = cart.SiteCustomer.Phone;

        jsonShippingAddr = jsonAddrTemplate;
        jsonShippingAddr = jsonShippingAddr.Replace("\r", "");
        jsonShippingAddr = jsonShippingAddr.Replace("\n", "");
        jsonShippingAddr = jsonShippingAddr.Replace(" ", "");

        jsonShippingAddr = jsonShippingAddr.Replace("#FIRST#", cart.SiteCustomer.FirstName);
        jsonShippingAddr = jsonShippingAddr.Replace("#LAST#", cart.SiteCustomer.LastName);
        jsonShippingAddr = jsonShippingAddr.Replace("#ADDRESS1#", cart.ShipAddress.Street);
        jsonShippingAddr = jsonShippingAddr.Replace("#ADDRESS2#", cart.ShipAddress.Street2);
        jsonShippingAddr = jsonShippingAddr.Replace("#CITY#", cart.ShipAddress.City);
        jsonShippingAddr = jsonShippingAddr.Replace("#PROVINCE#", cart.ShipAddress.State);
        jsonShippingAddr = jsonShippingAddr.Replace("#COUNTRY#", cart.ShipAddress.Country);
        jsonShippingAddr = jsonShippingAddr.Replace("#ZIP#", cart.ShipAddress.Zip);
        jsonShippingAddr = jsonShippingAddr.Replace("#PHONE#", cart.SiteCustomer.Phone);
        jsonShippingAddr = jsonShippingAddr.Replace("#EMAIL#", cart.SiteCustomer.Email);

        string jsonLineItemsTemplate = @"{""product_id"": ""#PRODUCTID#"",""title"": ""#TITLE#"",""sku"": ""#SKU#"",""price"":#PRICE#,""quantity"": #QUANTITY#, ""total_discount"": #LINEITEMDISCOUNT#}";
        string jsonLineItems = "";
        List<ShopifyLineItem> lineItemList = new List<ShopifyLineItem>();
        foreach(AdminCart.OrderDetail od in cart.SiteOrder.details)
        {
            ShopifyLineItem lineItem = new ShopifyLineItem();
            lineItem.Product_id = od.LineItemProduct.PartnerID;
            lineItem.Title = od.LineItemProduct.Name;
            lineItem.Price = od.UnitPrice; // od.UnitPrice - od.Discount; for Shopify, discounts are not at lineitem level
            lineItem.Grams = od.LineItemProduct.Weight;
            lineItem.Quantity = od.Quantity;

            lineItem.Total_discount = od.Discount;
            shopOrder.Total_discounts += od.Discount; //get total LI discounts; send to Shopify as "adjust" to total

            lineItem.Requires_shipping = od.LineItemProduct.IsShippable.ToString();
            lineItemList.Add(lineItem);

            string jsonTmp = jsonLineItemsTemplate;
            jsonTmp = jsonTmp.Replace("\r", "");
            jsonTmp = jsonTmp.Replace("\n", "");
            jsonTmp = jsonTmp.Replace(" ", "");

            jsonTmp = jsonTmp.Replace("#PRODUCTID#", od.LineItemProduct.PartnerID.ToString());
            jsonTmp = jsonTmp.Replace("#TITLE#", od.LineItemProduct.Name);
            jsonTmp = jsonTmp.Replace("#SKU#", od.LineItemProduct.Sku);
            jsonTmp = jsonTmp.Replace("#PRICE#", od.UnitPrice.ToString("F"));
            jsonTmp = jsonTmp.Replace("#QUANTITY#", od.Quantity.ToString());
            jsonTmp = jsonTmp.Replace("#LINEITEMDISCOUNT#", od.Discount.ToString("F"));
            jsonLineItems += jsonTmp + ",";
        }
        shopOrder.LineItems = lineItemList.ToArray();
        jsonLineItems = jsonLineItems.Remove(jsonLineItems.Length - 1); //get rid of the last comma

        string jsonTransactionsTemplate = @"{""authorization"": ""#AUTHORIZATION#"",""kind"": ""#KIND#"",
            ""status"": ""#STATUS#"",""test"": #TEST#,""amount"": #AMOUNT#,
            ""payment_details"": {""avs_result_code"":""#AVSRESULTCODE#"",""credit_card_bin"":""#CCBIN#"",
            ""cvv_result_code"":""#CVVRESULT#"",""credit_card_number"":""#CCNUMBER#"",""credit_card_company"":""#CCTYPE#""} }";
        string jsonTransactions = "";
        List<AdminCart.Transaction> tList = AdminCart.Transaction.GetTransactionList(cart.SiteOrder.OrderID);
        List<ShopifyAgent.ShopifyTransaction> shopTList = new List<ShopifyAgent.ShopifyTransaction>();
        foreach(AdminCart.Transaction t in tList)
        {
            ShopifyAgent.ShopifyTransaction shopT = new ShopifyAgent.ShopifyTransaction();
            if (t.TrxType.ToUpperInvariant() == "S") { shopT.Kind = "Sale"; }
            if (t.TrxType.ToUpperInvariant() == "C") { shopT.Kind = "Refund"; }
            if (t.TrxType.ToUpperInvariant() == "V") { shopT.Kind = "Void"; }

            //shopT.Status = "Success";
            shopT.Status = "success";
            shopT.Test = (ConfigurationManager.AppSettings["IsDevelopment"].ToString().ToUpperInvariant() == "TRUE") ? true : false;
            shopT.Amount = t.TransactionAmount;
            shopTList.Add(shopT);

            string jsonTmp = jsonTransactionsTemplate;
            //prep json string for replacements
            jsonTmp = jsonTmp.Replace("\r", "");
            jsonTmp = jsonTmp.Replace("\n", "");
            jsonTmp = jsonTmp.Replace(" ", "");

            //jsonTmp = jsonTmp.Replace("#STATUS#", "Success");
            jsonTmp = jsonTmp.Replace("#STATUS#", "success");
            string testStr = (shopT.Test) ? "true" : "false";
            jsonTmp = jsonTmp.Replace("#TEST#", testStr);
            jsonTmp = jsonTmp.Replace("#AUTHORIZATION#", t.TransactionID);
            jsonTmp = jsonTmp.Replace("#KIND#", shopT.Kind);
            jsonTmp = jsonTmp.Replace("#AMOUNT#", t.TransactionAmount.ToString("F"));
            jsonTmp = jsonTmp.Replace("#AVSRESULTCODE#", "");
            jsonTmp = jsonTmp.Replace("#CCBIN#", t.CardNo.Substring(0,6));

            jsonTmp = (t.CvsCode == "") ? jsonTmp.Replace("#CVVRESULT#", "") : jsonTmp.Replace("#CVVRESULT#", t.CvsCode);
            jsonTmp = (t.CardNo == "") ? jsonTmp.Replace("#CCNUMBER#", "") : jsonTmp.Replace("#CCNUMBER#", t.CardNo);
            jsonTmp = (t.CardType == "unknown") ? jsonTmp.Replace("#CCTYPE#", "") : jsonTmp.Replace("#CCTYPE#", t.CardType);
            jsonTransactions += jsonTmp + ",";
        }
        shopOrder.Transactions = shopTList.ToArray();
        jsonTransactions = jsonTransactions.Remove(jsonTransactions.Length - 1); //get rid of the last comma


        string jsonShippingTemplate = @"{""code"":""#CODE#"",""price"":""#PRICE#"",""source"":""#SOURCE#"",
            ""title"":""#TITLE#"",""tax_lines"":[]}";
        string jsonShipping = "";
        jsonShipping = jsonShippingTemplate;
        jsonShipping = jsonShipping.Replace("\r", "");
        jsonShipping = jsonShipping.Replace("\n", "");
        jsonShipping = jsonShipping.Replace(" ", "");

        jsonShipping = jsonShipping.Replace("#CODE#", AdminCart.Config.appName + " Shipping");
        jsonShipping = jsonShipping.Replace("#PRICE#", cart.TotalShipping.ToString("F"));
        jsonShipping = jsonShipping.Replace("#TITLE#", AdminCart.Config.appName + " Shipping");
        jsonShipping = jsonShipping.Replace("#SOURCE#", AdminCart.Config.appName);

        string jsonOrder = @"{""order"":{""line_items"":[#LINEITEMS#],""note"":""{ #FMORDERID# }"", ""tax_lines"":[],
            ""email"":""#EMAIL#"",""total_price"":#TOTAL#,""total_discounts"":#DISCOUNTS#,
            ""subtotal_price"":#SUBTOTAL#,""total_tax"":#TOTALTAX#,""currency"":""USD"",""transactions"":[#TRANSACTIONS#],
            ""billing_address"":#BILLINGADDR#, ""shipping_address"":#SHIPPINGADDR#,""shipping_lines"":[#SHIPPINGLINE#] } }"; //,""customer"":{}

        //prep json string for replacements
        jsonOrder = jsonOrder.Replace("\r", "");
        jsonOrder = jsonOrder.Replace("\n", "");
        jsonOrder = jsonOrder.Replace(" ", "");

        jsonOrder = jsonOrder.Replace("#FMORDERID#", AdminCart.Config.appName + ":" + cart.SiteOrder.OrderID.ToString());
        jsonOrder = jsonOrder.Replace("#EMAIL#", cart.SiteCustomer.Email);
        jsonOrder = jsonOrder.Replace("#TOTAL#", cart.Total.ToString("F"));

        //jsonOrder = jsonOrder.Replace("#DISCOUNTS#", cart.SiteOrder.Adjust.ToString("F")); //cart.TotalDiscounts
        shopOrder.Total_discounts += cart.SiteOrder.Adjust; //total discounts = LI discount + Adjust
        jsonOrder = jsonOrder.Replace("#DISCOUNTS#", shopOrder.Total_discounts.ToString("F")); //cart.TotalDiscounts

        jsonOrder = jsonOrder.Replace("#SUBTOTAL#", cart.TotalProduct.ToString("F"));
        jsonOrder = jsonOrder.Replace("#TOTALTAX#", cart.TotalTax.ToString("F"));

        jsonCustomer = jsonCustomer.Replace("#ADDRESS#", jsonBillingAddr); //use billing address as default address
        jsonOrder = jsonOrder.Replace("#CUSTOMER#", jsonCustomer);
        jsonOrder = jsonOrder.Replace("#BILLINGADDR#", jsonBillingAddr);
        jsonOrder = jsonOrder.Replace("#SHIPPINGADDR#", jsonShippingAddr);
        jsonOrder = jsonOrder.Replace("#LINEITEMS#", jsonLineItems);
        jsonOrder = jsonOrder.Replace("#TRANSACTIONS#", jsonTransactions);
        jsonOrder = jsonOrder.Replace("#SHIPPINGLINE#", jsonShipping);

        //string jsonStr = JsonConvert.SerializeObject(shopOrder);
        string jsonStr = jsonOrder;

        string jsonURL = "/admin/orders.json";
        //response holds new order json from Shopify, if order was created OK
        string response = JsonHelpers.POSTtoShopify(jsonURL, jsonStr);

        try
        {
            ShopifyAgent.ShopifyOrderSingle responseOrder = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyOrderSingle>(response);
            cart.SiteOrder.SSOrderID = Convert.ToInt32(responseOrder.order.Name);
            AdminCart.Order.UpDatePartnerID(cart.OrderID, cart.SiteOrder.SSOrderID);
        }
        catch (Exception ex)
        {
            //response wasn't a good Product response
            string msg = "error in CreateShopifyOrder" + Environment.NewLine;
            msg += "jsonURL: " + jsonURL + Environment.NewLine;
            msg += "jsonTemplate: " + jsonStr + Environment.NewLine;
            msg += "response: " + response + Environment.NewLine;
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
        }
    }
}
