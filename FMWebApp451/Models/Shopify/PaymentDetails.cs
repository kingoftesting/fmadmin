﻿// Generated by Xamasoft JSON Class Generator
// http://www.xamasoft.com/json-class-generator

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ShopifyAgent
{

    public class PaymentDetails
    {

        [JsonProperty("avs_result_code")]
        public string AvsResultCode;

        [JsonProperty("credit_card_bin")]
        public string CreditCardBin;

        [JsonProperty("cvv_result_code")]
        public string CvvResultCode;

        [JsonProperty("credit_card_number")]
        public string CreditCardNumber;

        [JsonProperty("credit_card_company")]
        public string CreditCardCompany;
    }

}
