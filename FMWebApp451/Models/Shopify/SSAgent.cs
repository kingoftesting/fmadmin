﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using FM2015.Helpers;

namespace ShopifyAgent
{
    public class SSAgent
    {
        public SSAgent()
        {
        }

        //returns list of new created FM orders, translated from ShopifyOrders
        public static List<int> SyncSS()
        {
            List<int> oList = new List<int>();
            List<int> fmOrderIdList = new List<int>();
            if (AdminCart.Config.SyncSS)
            {
                //Gen filename
                //string filepath = GenFileName();
                oList = DownloadLatestOrderIDs();
                foreach (int id in oList)
                {
                    ShopifyOrder o = new ShopifyOrder(id);
                    FMTranslate(o); //have fm.com translate and save order
                    int orderID = Convert.ToInt32(o.Name);
                    fmOrderIdList.Add(orderID);
                }               
            }
            return fmOrderIdList;
        }

        static public List<int> DownloadLatestOrderIDs()
        {
            List<int> oList = new List<int>();
            //int SSCompanyID = AdminCart.Config.ssCompanyID;

            string sql = @"
                SELECT ShopifyOrderID 
                FROM ShopifyOrders 
                WHERE Translated = 0 
            ";
            //--AND CompanyID = @CompanyID 

            string conn = AdminCart.Config.ConnStrShopify();
            //SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, SSCompanyID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, conn);
            while (dr.Read())
            {
                int id = Convert.ToInt32(dr["shopifyOrderID"].ToString());
                oList.Add(id);
            }
            if (dr != null) { dr.Close(); }

            return oList;
        }

        private static void FMTranslate(ShopifyOrder o)
        {
            ShopifyFMOrderTranslator.MergeOrders(o.Id.ToString(), o.CompanyID);
            MarkOrderTranslated(o);
        }

        public static void MarkOrderTranslated(ShopifyOrder o)
        {
            string conn = AdminCart.Config.ConnStrShopify();
            string sql = @"
                UPDATE ShopifyOrders 
                SET Translated = 1 
                WHERE  id = @ID AND CompanyID = @CompanyID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, o.Id, o.CompanyID);
            DBUtil.Exec(sql, conn, mySqlParameters);
        }
    }
}