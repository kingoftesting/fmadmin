﻿// Generated by Xamasoft JSON Class Generator
// http://www.xamasoft.com/json-class-generator

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ShopifyAgent
{

    public class ShippingAddress
    {

        private string address1;

        [JsonProperty("address1")]
        public string Address1 { get { return address1; } set {
            Console.WriteLine("Saving ShippingAddress.address1");
            address1 = value; } }

        [JsonProperty("address2")]
        public string Address2;

        [JsonProperty("city")]
        public string City;

        [JsonProperty("company")]
        public string Company;

        [JsonProperty("country")]
        public string Country;

        [JsonProperty("first_name")]
        public string FirstName;

        [JsonProperty("last_name")]
        public string LastName;

        [JsonProperty("latitude")]
        public string Latitude;

        [JsonProperty("longitude")]
        public string Longitude;

        [JsonProperty("phone")]
        public string Phone;

        [JsonProperty("province")]
        public string Province;

        [JsonProperty("zip")]
        public string Zip;

        [JsonProperty("name")]
        public string Name;

        [JsonProperty("country_code")]
        public string CountryCode;

        [JsonProperty("province_code")]
        public string ProvinceCode;
    }

}
