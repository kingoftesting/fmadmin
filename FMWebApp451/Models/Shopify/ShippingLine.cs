﻿// Generated by Xamasoft JSON Class Generator
// http://www.xamasoft.com/json-class-generator

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ShopifyAgent
{

    public class ShippingLine
    {
        private string code;

        [JsonProperty("code")]
        public string Code { get { return code; } set {
            Console.WriteLine("Saving ShippingLine.code");
            code = value; } }

        [JsonProperty("price")]
        public string Price;

        [JsonProperty("source")]
        public string Source;

        [JsonProperty("title")]
        public string Title;
    }

}
