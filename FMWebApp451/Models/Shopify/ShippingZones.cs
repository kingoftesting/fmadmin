﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using FMWebApp.Helpers;
using Newtonsoft.Json.Linq;

namespace ShopifyAgent
{
    public class ShippingZones
    {
        [JsonProperty("shipping_zones")]
        public ShopifyShippingZone[] Shipping_Zones;

        public ShippingZones()
        {
            Shipping_Zones = new ShopifyShippingZone[] { };
        }
    }

    public class Province
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("country_id")]
        public long Country_Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("tax")]
        public decimal Tax { get; set; }

        [JsonProperty("tax_name")]
        public string Tax_Name { get; set; }

        [JsonProperty("tax_type")]
        public string Tax_Type { get; set; }

        [JsonProperty("shipping_zone_id")]
        public long Shipping_Zone_Id { get; set; }

        [JsonProperty("tax_percentage")]
        public decimal Tax_Percentage { get; set; }

        public Province()
        {
            Id = 0;
            Country_Id = 0;
            Name = "";
            Tax = 0;
            Tax_Name = "";
            Tax_Type = "";
            Shipping_Zone_Id = 0;
            Tax_Percentage = 0;
        }
    }

    public class Country
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("tax")]
        public decimal Tax { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("tax_name")]
        public string Tax_Name { get; set; }

        [JsonProperty("provinces")]
        public Province[] provinces;

        public Country()
        {
            Id = 0;
            Name = "";
            Tax = 0;
            Code = "";
            Tax_Name = "";
            provinces = new Province[] { };
        }
    }

    public class Weight_Based_Shipping_Rate
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("weight_low")]
        public decimal Weight_Low { get; set; }

        [JsonProperty("weight_high")]
        public decimal Weight_High { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("shipping_zone_id")]
        public long Shipping_Zone_Id { get; set; }

        public Weight_Based_Shipping_Rate()
        {
            Id = 0;
            Weight_Low = 0;
            Weight_High = 0;
            Name = "";
            Price = 0;
            Code = "";
            Shipping_Zone_Id = 0;
        }
    }

    public class Price_Based_Shipping_Rate
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("min_order_subtotal")]
        public decimal Min_Order_Subtotal { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("max_order_subtotal")]
        public decimal? Max_Order_Subtotal { get; set; }

        [JsonProperty("shipping_zone_id")]
        public long Shipping_Zone_Id { get; set; }

        public Price_Based_Shipping_Rate()
        {
            Id = 0;
            Name = "";
            Min_Order_Subtotal = 0;
            Max_Order_Subtotal = 0;
            Price = 0;
            Shipping_Zone_Id = 0;
        }
    }

    public class Carrier_Shipping_Rate_Provider
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("carrier_service_id")]
        public long Carrier_Service_Id { get; set; }

        [JsonProperty("flat_modifier")]
        public decimal Flat_Modifier { get; set; }

        [JsonProperty("precent_modifier")]
        public decimal Precent_Modifier { get; set; }

        [JsonProperty("service_filter")]
        public string Service_Filter { get; set; }

        [JsonProperty("shipping_zone_id")]
        public long Shipping_Zone_Id { get; set; }

        public Carrier_Shipping_Rate_Provider()
        {
            Id = 0;
            Carrier_Service_Id = 0;
            Flat_Modifier = 0;
            Precent_Modifier = 0;
            Service_Filter = "";
            Shipping_Zone_Id = 0;
        }
    }

    public class ShopifyShippingZone
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("countries")]
        public Country[] countries;

        [JsonProperty("weight_based_shipping_rates")]
        public Weight_Based_Shipping_Rate[] weight_based_shipping_rates;

        [JsonProperty("price_based_shipping_rates")]
        public Price_Based_Shipping_Rate[] price_based_shipping_rates;

        [JsonProperty("carrier_shipping_rate_providers")]
        public Carrier_Shipping_Rate_Provider[] carrier_shipping_rate_providers;

        #region Contructor
        public ShopifyShippingZone()
        {
            Id = 0;
            Name = "";
            countries = new Country[] { };
            weight_based_shipping_rates = new Weight_Based_Shipping_Rate[] { };
            price_based_shipping_rates = new Price_Based_Shipping_Rate[] { };
            carrier_shipping_rate_providers = new Carrier_Shipping_Rate_Provider[] { };
        }
        #endregion

        public static List<ShopifyShippingZone> GetShippingZonesViaAPI()
        {
            List<ShopifyShippingZone> theList = new List<ShopifyShippingZone>();
            ShippingZones Shipping_Zones = new ShippingZones();
            //ShopifyShippingZone[] shippingZones = null;

            string uri = "admin/shipping_zones.json";
            string apiResult = FMWebApp.Helpers.JsonHelpers.GETfromShopify(uri);

            string testResult = @"
{
	""shipping_zones"": [{

        ""id"": 67668929,
		""name"": ""Australia"",
		""countries"": [{
			""id"": 66975361,
			""name"": ""Australia"",
			""tax"": 0.0,
			""code"": ""AU"",
			""tax_name"": ""GST"",
			""provinces"": []
    }],
		""weight_based_shipping_rates"": [],
		""price_based_shipping_rates"": [{
			""id"": 15716801,
			""name"": ""Australia Flat Rate Shipping"",
			""min_order_subtotal"": ""0.00"",
			""price"": ""29.99"",
			""max_order_subtotal"": 0.00,
			""shipping_zone_id"": 67668929

        }],
		""carrier_shipping_rate_providers"": []
	}]
}
";
            testResult = testResult.Replace("\n", "").Replace("\r", "").Replace("\t", "").Replace(" ", "");

            try
            {
                Shipping_Zones = JsonConvert.DeserializeObject<ShippingZones>(apiResult);
            }
            catch (Exception ex)
            {
                string msg = "error in GetShippingZonesViaAPI </br>";
                msg += "jsonURL: " + uri + "</br>";
                msg += "response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            if (Shipping_Zones != null)
            {
                foreach (ShopifyShippingZone zone in Shipping_Zones.Shipping_Zones)
                {
                    theList.Add(zone);
                }
            }

            return theList;
        }

    }
}