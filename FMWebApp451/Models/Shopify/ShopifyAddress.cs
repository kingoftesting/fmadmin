﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;
using System.Text;
using System.Data;

using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using FM2015.Helpers;

public class ShopifyCustomerAddress
{
    [JsonProperty("customer_address")]
    public ShopifyAddress Customer_address;

}

/// <summary>
/// Summary description for ShopifyBillingAddress
/// </summary>
public class ShopifyAddress
{
    //Built with ClassHatch Version Build: 4.0.7  9/8/2013.  Template file:ExtendedClass.cs.tem

    #region Fields
    private int shopifyAddressID;
    private int shopifyOrderID;
    private long id;
    private string address1;
    private string address2;
    private string city;
    private string company;
    private string country;
    private string first_name;
    private string last_name;
    private Decimal? latitude;
    private Decimal? longitude;
    private string phone;
    private string province;
    private string zip;
    private string name;
    private string country_code;
    private string province_code;
    private bool isDefault;
    #endregion


    #region Properties

    public int ShopifyAddressID { get { return shopifyAddressID; } set { shopifyAddressID = value; } }
    public int ShopifyOrderID { get { return shopifyOrderID; } set { shopifyOrderID = value; } }

    [JsonProperty("id")]
    public long Id { get { return id; } set { id = value; } }
    [JsonProperty("address1")]
    public string Address1 { get { return address1; } set { address1 = value; } }
    [JsonProperty("address2")]
    public string Address2 { get { return address2; } set { address2 = value; } }
    [JsonProperty("city")]
    public string City { get { return city; } set { city = value; } }
    [JsonProperty("company")]
    public string Company { get { return company; } set { company = value; } }
    [JsonProperty("country")]
    public string Country { get { return country; } set { country = value; } }
    [JsonProperty("first_name")]
    public string First_name { get { return first_name; } set { first_name = value; } }
    [JsonProperty("last_name")]
    public string Last_name { get { return last_name; } set { last_name = value; } }
    [JsonProperty("latitude")]
    public Decimal? Latitude
    {
        get { return latitude; }
        set
        {
            Console.WriteLine("Saving ShopifyBillingAddress.Latitude");
            latitude = value;
        }
    }
    [JsonProperty("longitude")]
    public Decimal? Longitude { get { return longitude; } set { longitude = value; } }
    [JsonProperty("phone")]
    public string Phone { get { return phone; } set { phone = value; } }
    [JsonProperty("province")]
    public string Province { get { return province; } set { province = value; } }
    [JsonProperty("zip")]
    public string Zip { get { return zip; } set { zip = value; } }
    [JsonProperty("name")]
    public string Name { get { return name; } set { name = value; } }
    [JsonProperty("country_code")]
    public string Country_code { get { return country_code; } set { country_code = value; } }
    [JsonProperty("province_code")]
    public string Province_code { get { return province_code; } set { province_code = value; } }
    [JsonProperty("default")]
    public bool IsDefault { get { return isDefault; } set { isDefault = value; } }


    #endregion


    #region Constructors
    public ShopifyAddress()
    {
        //sets reasonable initial values
        shopifyAddressID = 0;
        shopifyOrderID = 0;
        id = 0;
        address1 = "";
        address2 = "";
        city = "";
        company = "";
        country = "";
        first_name = "";
        last_name = "";
        latitude = 0;
        longitude = 0;
        phone = "";
        province = "";
        zip = "";
        name = "";
        country_code = "";
        province_code = "";
        isDefault = false;
    }

    #endregion


    #region Validation
    //Requires Validation class from classhatch.com
    static public bool IsValid(ShopifyShippingAddress obj)
    {
        bool isValid = false;

        if (GetErrors(obj).Count == 0)
        {
            isValid = true;
        }

        return isValid;
    }


    static public List<ValidationError> GetErrors(ShopifyShippingAddress obj)
    {
        List<ValidationError> errorlist = new List<ValidationError>();

        /*
        if (ValidateUtil.IsTooLong(20, obj.FirstName))
        {
            ValidationError e = new ValidationError();
            e.Field = "First Name";
            e.ErrorDescription = "20 characters maximium are allowed";
            errorlist.Add(e);
        };
        */

        return errorlist;
    }
    #endregion


    #region OverRides

    #endregion

}
