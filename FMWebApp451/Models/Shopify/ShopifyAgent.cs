﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using FM2015.Helpers;

namespace ShopifyAgent
{
    public class ShopifyAgent
    {
        private static readonly Object _locker = new Object();

        public ShopifyAgent()
        {
        }

        public static List<int> SyncShopify()
        {
            List<int> oList = new List<int>();
            List<int> ssOrderIDList = new List<int>();

            lock (_locker)
            {
                oList = new List<int>();
                int count = GetNewOrderCount();
                if ((count > 0) && (AdminCart.Config.SyncShopify))
                {
                    //Gen filename
                    string filepath = GenFileName();
                    DownloadLatestOrders(filepath);
                    oList = parseFile(filepath);
                }

                ssOrderIDList = SSAgent.SyncSS();
            }
            
            foreach (int ssOrderID in ssOrderIDList)
            {
                oList.Add(ssOrderID);
            }
            oList = oList.OrderByDescending(x => x).ToList();
            return oList;
        }

        static string GenFileName()
        {
            //string syncFilePath = ConfigurationManager.AppSettings["syncFilePath"].ToString();
            string syncFilePath = AdminCart.Config.ShopifySyncPath(); //@"c:\tmp\slc\shopify\";

            string filePrefix = @"Shopify";
            string fileSuffix = ".json";

            DateTime now = DateTime.Now;

            string majorPart = String.Format("{0}{1}{2}", now.Year.ToString(), now.Month.ToString().PadLeft(2, '0'), now.Day.ToString().PadLeft(2, '0'));
            string minorPart = String.Format("_{0}{1}{2}", now.Hour.ToString().PadLeft(2, '0'), now.Minute.ToString().PadLeft(2, '0'), now.Second.ToString().PadLeft(2, '0'));

            string jsonFile = filePrefix + majorPart + minorPart + fileSuffix;


            string filepath = syncFilePath + jsonFile;

            return filepath;
        }

        static public void DownloadLatestOrders(string filepath)
        {
            string conn = AdminCart.Config.ConnStrShopify();
            string lastLocalID = GetLastLocalID();

            string apipath = "admin/orders.json?since_id=" + lastLocalID + "&limit=249&status=open";
            //string apipath = "admin/orders.json";
            string response = FMWebApp.Helpers.JsonHelpers.GETFilefromShopify(apipath, filepath);
        }

        static public int GetNewOrderCount()
        {
            string lastLocalID = GetLastLocalID();
            string apipath = "admin/orders.json?since_id=" + lastLocalID + "&limit=249&status=open";
            //string apipath = "admin/orders.json";
            string response = FMWebApp.Helpers.JsonHelpers.GETfromShopify(apipath);

            ShopifyOrders responseOrders = new ShopifyOrders();
            try
                {
                    responseOrders = JsonConvert.DeserializeObject<ShopifyOrders>(response);
                }
                catch (Exception ex)
                {
                    //put some exception handling code here
                    string msg = "error in ShopifyAgent:SyncShopify: GetNewOrderCount" + Environment.NewLine;
                    msg += "apipath: " + apipath + Environment.NewLine;
                    msg += "response: " + response + Environment.NewLine;
                    FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                }

            if (responseOrders.Orders == null)
            {
                return 0;
            }
            else
            {
                return responseOrders.Orders.Length;
            }
            
        }

        static private string GetLastLocalID()
        {
            string conn = AdminCart.Config.ConnStrShopify();
            int companyID = AdminCart.Config.CompanyID();
            string sqlMaxID = @"
                SELECT TOP 1 id
                FROM ShopifyOrders
                WHERE name NOT LIKE '#%' AND CompanyID = @CompanyID 
                ORDER BY convert(int,name) DESC";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sqlMaxID, companyID);
            SqlDataReader dr = DBUtil.FillDataReader(sqlMaxID, mySqlParameters, conn);
            string lastLocalID = "0";
            if (dr.HasRows && dr.Read()) { lastLocalID = Convert.ToInt64(dr["id"]).ToString(); }
            if (dr != null) { dr.Close(); }

            //string lastLocalID = DBUtil.GetScalar(sqlMaxID);
            //string lastLocalID = "1";

            return lastLocalID;
        }


        static List<int> parseFile(string filepath)
        {
            string conn = AdminCart.Config.ConnStrShopify();
            List<int> oList = new List<int>();

            //dupecheck
            bool dupefile = true;
            string dupesql = @"
                SELECT * 
                FROM ShopifyFiles 
                WHERE ShopifyFileName='" + filepath + "' ";

            SqlDataReader dupeDR = DBUtil.FillDataReader(dupesql, conn);
            if (dupeDR.HasRows)
            {
                dupefile = true;
            }
            else
            {
                dupefile = false;
            }

            if (!dupefile)
            {
                string filesql = "INSERT ShopifyFiles(ShopifyFileName) VALUES ('" + filepath + "')";
                DBUtil.Exec(filesql, conn);

                //List<ShopifyOrder> Orders = new List<ShopifyOrder>();
                StreamReader re = new StreamReader(filepath);
                JsonTextReader reader = new JsonTextReader(re);
                JsonSerializer se = new JsonSerializer();
                var JsonOrders = (ShopifyOrders)se.Deserialize(reader, typeof(ShopifyOrders));
                //Orders = JsonOrders.Orders.ToList();

                long minID = 0;
                long maxID = 0;

                foreach (ShopifyOrder o in JsonOrders.Orders) //MyOrders.Orders
                {
                    if (minID == 0) minID = o.Id;
                    if (maxID == 0) maxID = o.Id;

                    if (o.Id < minID) minID = o.Id;
                    if (o.Id > maxID) maxID = o.Id;

                    if (!o.Name.Contains("#"))
                    {
                        SaveData(o); //store mirrored Shopify order for translation & record keeping
                        FMTranslate(o); //have fm.com translate and save order via WebAPI
                        int orderID = Convert.ToInt32(o.Name);
                        oList.Add(orderID);
                    }
                }

                //ScheduleBatch(minID, maxID, filepath);

            }
            else
            {
                string mailFrom = AdminCart.Config.MailFrom();
                string mailTo = AdminCart.Config.MailTo;
                string subject = "FM dbErrorLogging";
                string msg = "error in ShopifyAgent: ParseFile: Duplicate file: filePath= " + filepath;
                mvc_Mail.SendMail(mailFrom, mailTo, subject, msg);
            }

            return oList;
        }

        private static void SaveData(ShopifyOrder o)
        {
            //Order
            o.ShopifyOrderID = o.SaveDB();

            //Billing Address
            ShopifyBillingAddress billadd = o.ShopifyBillingAddress;
            if ( (billadd == null) || (string.IsNullOrEmpty(billadd.Address1)) || (string.IsNullOrEmpty(billadd.City)) )
            {
                //if no Billing Address then use Default Address
                billadd.Name = o.ShopifyCustomer.Default_address.Name;
                billadd.First_name = o.ShopifyCustomer.Default_address.First_name;
                billadd.Last_name = o.ShopifyCustomer.Default_address.Last_name;
                billadd.Company = o.ShopifyCustomer.Default_address.Company;
                billadd.Address1 = o.ShopifyCustomer.Default_address.Address1;
                billadd.Address2 = o.ShopifyCustomer.Default_address.Address2;
                billadd.City = o.ShopifyCustomer.Default_address.City;
                billadd.Province = o.ShopifyCustomer.Default_address.Province;
                billadd.Province_code = o.ShopifyCustomer.Default_address.Province_code;
                billadd.Country = o.ShopifyCustomer.Default_address.Country;
                billadd.Country_code = o.ShopifyCustomer.Default_address.Country_code;
                billadd.Zip = o.ShopifyCustomer.Default_address.Zip;
                billadd.Phone = o.ShopifyCustomer.Default_address.Phone;

                string msg = "Shopify Order = " + o.Name;
                string mailFrom = AdminCart.Config.MailFrom();
                string mailTo = AdminCart.Config.MailTo;
                //mvc_Mail.SendMail(mailFrom, mailTo, "Error in ShopifyAgent: SyncShopify: BillAdr is Empty, using Default", msg);
            }
            billadd.ShopifyOrderID = o.ShopifyOrderID;
            billadd.ShopifyBillingAddressID = billadd.SaveDB();

            //Shipping Address
            ShopifyShippingAddress shipadd = o.ShopifyShippingAddress;
            if ((shipadd == null) || (string.IsNullOrEmpty(shipadd.Address1)) || (string.IsNullOrEmpty(shipadd.City)) )
            {
                //if no Shipping Address then use Default Address
                shipadd.Name = o.ShopifyCustomer.Default_address.Name;
                shipadd.First_name = o.ShopifyCustomer.Default_address.First_name;
                shipadd.Last_name = o.ShopifyCustomer.Default_address.Last_name;
                shipadd.Company = o.ShopifyCustomer.Default_address.Company;
                shipadd.Address1 = o.ShopifyCustomer.Default_address.Address1;
                shipadd.Address2 = o.ShopifyCustomer.Default_address.Address2;
                shipadd.City = o.ShopifyCustomer.Default_address.City;
                shipadd.Province = o.ShopifyCustomer.Default_address.Province;
                shipadd.Province_code = o.ShopifyCustomer.Default_address.Province_code;
                shipadd.Country = o.ShopifyCustomer.Default_address.Country;
                shipadd.Country_code = o.ShopifyCustomer.Default_address.Country_code;
                shipadd.Zip = o.ShopifyCustomer.Default_address.Zip;
                shipadd.Phone = o.ShopifyCustomer.Default_address.Phone;

                string msg = "Shopify Order = " + o.Name;
                string mailFrom = AdminCart.Config.MailFrom();
                string mailTo = AdminCart.Config.MailTo;
                //mvc_Mail.SendMail(mailFrom, mailTo, "Error in ShopifyAgent: SyncShopify: ShipAdr is Empty, using Default", msg);
            }
            shipadd.ShopifyOrderID = o.ShopifyOrderID;
            shipadd.ShopifyShippingAddressID = shipadd.SaveDB();

            //Line Items
            foreach (ShopifyLineItem i in o.LineItems)
            {
                i.ShopifyOrderID = o.ShopifyOrderID;

                string friendlyProductName = i.Name;
                if (friendlyProductName.Length > 30) friendlyProductName = friendlyProductName.Substring(0, 30);
                i.ShopifyLineItemID = i.SaveDB();
            }

            //Discount Codes
            foreach (DiscountCode code in o.DiscountCodes)
            {
                ShopifyDiscountCodes shopCode = new ShopifyDiscountCodes();
                shopCode.ShopifyOrderID = o.ShopifyOrderID;
                shopCode.Code = code.Code;
                shopCode.Amount = code.Amount;
                shopCode.Type = code.Type;
                shopCode.ID = shopCode.Save(0); //insert new into DB
            }

            //Shipping Line
            foreach (ShopifyShippingLine line in o.ShippingLines)
            {
                line.ShopifyOrderID = o.ShopifyOrderID;
                line.ShopifyShippingLineID = line.SaveDB();
            }

            //Clientdetails
            ShopifyClientDetail det = o.ClientDetails;
            if (det == null)
            {
                det = new ShopifyClientDetail();
            }
            det.ShopifyOrderID = o.ShopifyOrderID;
            det.ShopifyClientDetailID = det.SaveDB();

            //Customer
            ShopifyCustomer c = o.ShopifyCustomer;
            if ((c == null) || (c.Id == 0))
            {
                string msg = "Shopify Order = " + o.Name;
                string mailFrom = AdminCart.Config.MailFrom();
                string mailTo = AdminCart.Config.MailTo;
                mvc_Mail.SendMail(mailFrom, mailTo, "Error in ShopifyAgent: SyncShopify: ShopifyCustomer is Empty", msg);
                throw new IndexOutOfRangeException("Error in ShopifyAgent: SyncShopify: ShopifyCustomer is Empty: " + msg);
            }
            c.Phone = o.ShopifyCustomer.Default_address.Phone;
            c.ShopifyOrderID = o.ShopifyOrderID;
            c.ShopifyCustomerID = c.SaveDB();

            //Transaction
            string apiResult = DownloadOrderTransactions(o);
            ShopifyTransactions sos = new ShopifyTransactions();
            try
            {
                sos = JsonConvert.DeserializeObject<ShopifyTransactions>(apiResult);
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                string msg = "error in ShopifyAgent: SaveData(): DownLoadTransactions </br>";
                msg += "JSON response: " + apiResult;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            for (int i = 0; i < sos.Transactions.Length; i++)
            {
                sos.Transactions[i].ShopifyOrderID = o.ShopifyOrderID;
                sos.Transactions[i].ShopifyImportDate = o.ShopifyImportDate;
                ShopifyTransaction.InsertShopifyTransaction(sos.Transactions[i]);
                //sos.Transactions[i].Save(0);
            }

        }

        private static void FMTranslate(ShopifyOrder o)
        {
            ShopifyFMOrderTranslator.MergeOrders(o.Id.ToString(), o.CompanyID);
            MarkOrderTranslated(o); 
        }

        static public string DownloadOrderTransactions(ShopifyOrder o)
        {

            string apipath = @"admin/orders/" + o.Id + "/transactions.json";
            string apiResult = FMWebApp.Helpers.JsonHelpers.GETfromShopify(apipath);

            return apiResult;
        }

        static public string DownloadOrderTransaction(long orderId, long transactionId)
        {

            string apipath = @"admin/orders/" + orderId.ToString() + "/transactions/" + transactionId.ToString() + ".json";
            string apiResult = FMWebApp.Helpers.JsonHelpers.GETfromShopify(apipath);

            return apiResult;
        }

        private static void SaveCustomerToFile(Customer c)
        {
            using (FileStream fs = File.Open(ConfigurationManager.AppSettings["syncFilePath"].ToString() + "person.json", FileMode.CreateNew))

            using (StreamWriter sw = new StreamWriter(fs))

            using (JsonWriter jw = new JsonTextWriter(sw))
            {

                jw.Formatting = Formatting.Indented;

                JsonSerializer serializer = new JsonSerializer();

                serializer.Serialize(jw, c);

            }
        }

        public static void MarkOrderTranslated(ShopifyOrder o)
        {
            string conn = AdminCart.Config.ConnStrShopify();
            string sql = @"
                UPDATE ShopifyOrders 
                SET Translated = 1 
                WHERE  id = @ID AND CompanyID = @CompanyID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, o.Id, o.CompanyID);
            DBUtil.Exec(sql, conn, mySqlParameters);
        }
    }
}