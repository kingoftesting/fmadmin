﻿using System;
using System.Text;
using System.Data;

using System.Data.SqlClient;
using System.Collections.Generic;

using Newtonsoft.Json;
using FM2015.Helpers;
using System.Configuration;

/// <summary>
/// Summary description for ShopifyClientDetail
/// </summary>
public class ShopifyClientDetail
{
    //Built with ClassHatch Version Build: 4.0.7  9/8/2013.  Template file:ExtendedClass.cs.tem

    #region Fields
    private int shopifyClientDetailID;
    private int shopifyOrderID;
    private string accept_language;
    private string browser_ip;
    private string session_hash;
    private string user_agent;
    #endregion


    #region Properties

    
    
    
    public int ShopifyClientDetailID { get { return shopifyClientDetailID; } set { shopifyClientDetailID = value; } }
    public int ShopifyOrderID { get { return shopifyOrderID; } set { shopifyOrderID = value; } }

    [JsonProperty("accept_language")]
    public string Accept_language { get { return accept_language; } set {
        Console.WriteLine("Saving ShopifyClientDetail.accept_language");
        accept_language = value; } }

    [JsonProperty("browser_ip")]
    public string Browser_ip { get { return browser_ip; } set { browser_ip = value; } }

    [JsonProperty("session_hash")]
    public string Session_hash { get { return session_hash; } set { session_hash = ValidateUtil.maxString( value,3500); } }

    [JsonProperty("user_agent")]
    public string User_agent { get { return user_agent; } set { user_agent = value; } }
    #endregion


    #region Constructors
    public ShopifyClientDetail()
    {
        //sets reasonable initial values
        shopifyClientDetailID = 0;
        shopifyOrderID = 0;
        accept_language = "";
        browser_ip = "";
        session_hash = "";
        user_agent = "";
    }

    public ShopifyClientDetail(int PrimaryKeyValue)
    {
        //Loads a single record from a table
        string sql = @"
        SELECT *
        FROM ShopifyClientDetails
        WHERE ShopifyClientDetailID = @PrimaryKeyValue";
        SqlParameter[] parms = new SqlParameter[1];
        parms[0] = new SqlParameter("@PrimaryKeyValue", PrimaryKeyValue);

        SqlDataReader dr = DBUtil.FillDataReader(sql, parms);

        while (dr.Read())
        {
            this.shopifyClientDetailID = Convert.ToInt32(dr["shopifyClientDetailID"].ToString());
            this.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            this.accept_language = Convert.ToString(dr["accept_language"].ToString());
            this.browser_ip = Convert.ToString(dr["browser_ip"].ToString());
            this.session_hash = Convert.ToString(dr["session_hash"].ToString());
            this.user_agent = Convert.ToString(dr["user_agent"].ToString());
        }
        dr.Close();
    }

    #endregion


    #region Methods
    //requires DBUtil class from classhatch.com
    public int SaveDB()
    {
        //Insert or Update record on SQL Table
        SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrShopify());

        string sql = "";


        if (ShopifyClientDetailID == 0)
        {
            //Insert if primary key is default value of zero
            sql = @"
            INSERT ShopifyClientDetails(
--ShopifyClientDetailID,
ShopifyOrderID,
accept_language,
browser_ip,
session_hash,
user_agent

             )VALUES(
--@ShopifyClientDetailID,
@ShopifyOrderID,
@accept_language,
@browser_ip,
@session_hash,
@user_agent

            ); select id=scope_identity();";
        }
        else
        {
            //Update db record if primary key is not default value of zero
            sql = @"
            UPDATE ShopifyClientDetails
            SET 
--ShopifyClientDetailID= @ShopifyClientDetailID,
ShopifyOrderID= @ShopifyOrderID,
accept_language= @accept_language,
browser_ip= @browser_ip,
session_hash= @session_hash,
user_agent= @user_agent

            WHERE ShopifyClientDetailID = PrimaryKeyFieldValue; select id=@ShopifyClientDetailID;";
        }
        SqlCommand cmd = new SqlCommand(sql, conn);

        SqlParameter shopifyClientDetailIDParam = new SqlParameter("@shopifyClientDetailID", SqlDbType.Int);
        SqlParameter shopifyOrderIDParam = new SqlParameter("@shopifyOrderID", SqlDbType.Int);
        SqlParameter accept_languageParam = new SqlParameter("@accept_language", SqlDbType.VarChar);
        SqlParameter browser_ipParam = new SqlParameter("@browser_ip", SqlDbType.VarChar);
        SqlParameter session_hashParam = new SqlParameter("@session_hash", SqlDbType.VarChar);
        SqlParameter user_agentParam = new SqlParameter("@user_agent", SqlDbType.VarChar);

        shopifyClientDetailIDParam.Value = shopifyClientDetailID;
        shopifyOrderIDParam.Value = shopifyOrderID;
        accept_languageParam.Value = (object)accept_language ?? DBNull.Value;
        browser_ipParam.Value = (object)browser_ip ?? DBNull.Value;
        session_hashParam.Value = (object)session_hash ?? DBNull.Value;
        user_agentParam.Value = (object)user_agent ?? DBNull.Value;

        cmd.Parameters.Add(shopifyClientDetailIDParam);
        cmd.Parameters.Add(shopifyOrderIDParam);
        cmd.Parameters.Add(accept_languageParam);
        cmd.Parameters.Add(browser_ipParam);
        cmd.Parameters.Add(session_hashParam);
        cmd.Parameters.Add(user_agentParam);

        conn.Open();
        int result = Convert.ToInt32(cmd.ExecuteScalar().ToString());
        conn.Close();
        return result;
    }

    static public List<ShopifyClientDetail> GetShopifyClientDetailList()
    {
        string conn = AdminCart.Config.ConnStrShopify();
        List<ShopifyClientDetail> thelist = new List<ShopifyClientDetail>();

        string sql = "SELECT * FROM ShopifyClientDetails";

        SqlDataReader dr = DBUtil.FillDataReader(sql);

        while (dr.Read())
        {
            ShopifyClientDetail obj = new ShopifyClientDetail();

            obj.shopifyClientDetailID = Convert.ToInt32(dr["shopifyClientDetailID"].ToString());
            obj.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            obj.accept_language = Convert.ToString(dr["accept_language"].ToString());
            obj.browser_ip = Convert.ToString(dr["browser_ip"].ToString());
            obj.session_hash = Convert.ToString(dr["session_hash"].ToString());
            obj.user_agent = Convert.ToString(dr["user_agent"].ToString());

            thelist.Add(obj);
        }
        dr.Close();
        return thelist;

    }

    #endregion


    #region Validation
    //Requires Validation class from classhatch.com
    static public bool IsValid(ShopifyClientDetail obj)
    {
        bool isValid = false;

        if (GetErrors(obj).Count == 0)
        {
            isValid = true;
        }

        return isValid;
    }


    static public List<ValidationError> GetErrors(ShopifyClientDetail obj)
    {
        List<ValidationError> errorlist = new List<ValidationError>();

        /*
        if (ValidateUtil.IsTooLong(20, obj.FirstName))
        {
            ValidationError e = new ValidationError();
            e.Field = "First Name";
            e.ErrorDescription = "20 characters maximium are allowed";
            errorlist.Add(e);
        };
        */

        return errorlist;
    }
    #endregion


    #region OverRides

    #endregion

}
