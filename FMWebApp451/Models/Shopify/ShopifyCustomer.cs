﻿using System;
using System.Text;
using System.Data;

using System.Data.SqlClient;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using FM2015.Helpers;
using FMWebApp.Helpers;
using System.Collections.Specialized;

public class ShopifyCustomers
{
    [JsonProperty("customers")]
    public ShopifyCustomer[] Customers;
}

public class ShopifyCustomerSingle
{
    [JsonProperty("customer")]
    public ShopifyCustomer customer;
}

public class ShopifyCustomerCreate
{
    [JsonProperty("customer")]
    public ShopifyCustomerCreateDetails customerCreate;

    public ShopifyCustomerCreate() 
    { customerCreate = new ShopifyCustomerCreateDetails(); }

    public class ShopifyCustomerCreateDetails
    {
        [JsonProperty("first_name")]
        public string First { get; set; }

        [JsonProperty("last_name")]
        public string Last { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("verified_email")]
        public bool Verified_Email { get; set; }

        [JsonProperty("addresses")]
        public ShopifyCustomerCreateAddressDetails addressDetails;

        public ShopifyCustomerCreateDetails() 
        {
            First = "";
            Last = "";
            Email = "";
            Verified_Email = false;
            addressDetails = new ShopifyCustomerCreateAddressDetails();
        }

        public class ShopifyCustomerCreateAddressDetails
        {
            [JsonProperty("address1")]
            public string Address1 { get; set; }

            [JsonProperty("address2")]
            public string Address2 { get; set; }

            [JsonProperty("city")]
            public string City { get; set; }

            [JsonProperty("province")]
            public string Province { get; set; }

            [JsonProperty("phone")]
            public string Phone { get; set; }

            [JsonProperty("zip")]
            public string Zip { get; set; }

            [JsonProperty("last_name")]
            public string Last { get; set; }

            [JsonProperty("first_name")]
            public string First { get; set; }

            [JsonProperty("country")]
            public string Country { get; set; }

            public ShopifyCustomerCreateAddressDetails() 
            {
                Address1 = "";
                Address2="";
                City = "";
                Province = "";
                Phone = "";
                Zip = "";
                Last = "";
                First = "";
                Country = "";
            }

        }
    }
}

public class ShopifyCustomerCreateVM
{
    public string First { get; set; }
    public string Last { get; set; }
    public string Email { get; set; }
    public bool Verified_Email { get; set; }

    public string Address1 { get; set; }
    public string City { get; set; }
    public string Province { get; set; }
    public string Phone { get; set; }
    public string Zip { get; set; }
    public string Addr_LastName { get; set; }
    public string Addr_FirstName { get; set; }
    public string Country { get; set; }

}



//Built with ClassHatch Version Build: 4.0.7  9/8/2013.  Template file:ExtendedClass.cs.tem

/// <summary>
/// Summary description for ShopifyCustomer
/// </summary>
public class ShopifyCustomer
{
    //Built with ClassHatch Version Build: 4.0.7  9/8/2013.  Template file:ExtendedClass.cs.tem

    #region Fields
    private int shopifyCustomerID;
    private int shopifyOrderID;
    private bool accepts_marketing;
    private DateTime? created_at;
    private string email;
    private string first_name;
    private long id;
    private string last_name;
    private long? last_order_id;
    private string multipass_identifier;
    private string note;
    private int orders_count;
    private string state;
    private Decimal total_spent;
    private DateTime? updated_at;
    private bool verified_email;
    private string phone;
    private string tags;
    private string last_order_name;
    private string image_url;
    private int default_addressID;
    #endregion


    #region Properties

    public int ShopifyCustomerID { get { return shopifyCustomerID; } set { shopifyCustomerID = value; } }
    public int ShopifyOrderID { get { return shopifyOrderID;} set { shopifyOrderID=value; } }

    [JsonProperty("accepts_marketing")]
    public bool Accepts_marketing { get { return accepts_marketing; } set { accepts_marketing = value; } }

    [JsonProperty("created_at")]
    public DateTime? Created_at { get { return created_at; } set { created_at = value; } }

    [JsonProperty("email")]
    public string Email { get { return email; } set { email = value; } }
    
    [JsonProperty("first_name")]
    public string First_name { get { return first_name; } set { first_name = value; } }

    [JsonProperty("id")]
    public long Id { get { return id; } set {
        Console.WriteLine("Saving ShopifyCustomer.id");
        id = value; } }

    [JsonProperty("last_name")]
    public string Last_name { get { return last_name; } set { last_name = value; } }

    [JsonProperty("last_order_id")]
    public long? Last_order_id { get { return last_order_id; } set { last_order_id = value; } }

    [JsonProperty("multipass_identifier")]
    public string Multipass_identifier { get { return multipass_identifier; } set { multipass_identifier = value; } }

    [JsonProperty("note")]
    public string Note { get { return note; } set { note = value; } }

    [JsonProperty("orders_count")]
    public int Orders_count { get { return orders_count; } set { orders_count = value; } }

    [JsonProperty("state")]
    public string State { get { return state; } set { state = value; } }

    [JsonProperty("total_spent")]
    public Decimal Total_spent { get { return total_spent; } set { total_spent = value; } }

    [JsonProperty("updated_at")]
    public DateTime? Updated_at { get { return updated_at; } set { updated_at = value; } }

    [JsonProperty("verified_email")]
    public bool Verified_email { get { return verified_email; } set { verified_email = value; } }

    [JsonProperty("phone")]
    public string Phone { get { return phone; } set { phone = value; } }

    [JsonProperty("tags")]
    public string Tags { get { return tags; } set { tags = value; } }

    [JsonProperty("last_order_name")]
    public string Last_order_name { get { return last_order_name; } set { last_order_name = value; } }

    [JsonProperty("image_url")]
    public string Image_url { get { return image_url; } set { image_url = value; } }

    [JsonProperty("default_addressID")]
    public int Default_addressID { get { return default_addressID; } set { default_addressID = value; } }

    [JsonProperty("default_address")]
    public ShopifyAddress Default_address;

    [JsonProperty("addresses")]
    public ShopifyAddress[] addresses;
    #endregion


    #region Constructors
    public ShopifyCustomer()
    {
        //sets reasonable initial values

        Default_address = new ShopifyAddress();
        addresses = new ShopifyAddress[] { };

        shopifyCustomerID = 0;
        shopifyOrderID = 0;
        accepts_marketing = false;
        created_at = DateTime.Parse("1/1/1900");
        email = "";
        first_name = "";
        id = 0;
        last_name = "";
        last_order_id = 0;
        multipass_identifier = "";
        note = "";
        orders_count = 0;
        state = "";
        total_spent = 0;
        updated_at = DateTime.Parse("1/1/1900");
        verified_email = false;
        phone = "";
        tags = "";
        last_order_name = "";
        image_url = "";
        default_addressID = 0;
    }

    public ShopifyCustomer(int shopifyOrderID)
    {
        //Loads a single record from a table
        string sql = @"
        SELECT *
        FROM ShopifyCustomers
        WHERE ShopifyOrderID = @ShopifyOrderID";
        SqlParameter[] parms = new SqlParameter[1];
        parms[0] = new SqlParameter("@ShopifyOrderID", shopifyOrderID);

        string shopConnStr = AdminCart.Config.ConnStrShopify();
        SqlDataReader dr = DBUtil.FillDataReader(sql, parms, shopConnStr);

        if (dr.Read())
        {
            this.shopifyCustomerID = Convert.ToInt32(dr["shopifyCustomerID"].ToString());
            this.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            this.accepts_marketing = Convert.ToBoolean(dr["accepts_marketing"].ToString());
            this.created_at = Convert.ToDateTime(dr["created_at"].ToString());
            this.email = Convert.ToString(dr["email"].ToString());
            this.first_name = Convert.ToString(dr["first_name"].ToString());
            this.id = Convert.ToInt64(dr["id"].ToString());
            this.last_name = Convert.ToString(dr["last_name"].ToString());
            this.last_order_id = (dr["last_order_id"] == DBNull.Value) ? 0 : Convert.ToInt64(dr["last_order_id"]);
            this.multipass_identifier = Convert.ToString(dr["multipass_identifier"].ToString());
            this.note = Convert.ToString(dr["note"].ToString());
            this.orders_count = Convert.ToInt32(dr["orders_count"].ToString());
            this.state = Convert.ToString(dr["state"].ToString());
            this.total_spent = Convert.ToDecimal(dr["total_spent"].ToString());
            this.updated_at = Convert.ToDateTime(dr["updated_at"].ToString());
            this.verified_email = Convert.ToBoolean(dr["verified_email"].ToString());
            this.phone = Convert.ToString(dr["phone"].ToString());
            this.tags = Convert.ToString(dr["tags"].ToString());
            this.last_order_name = Convert.ToString(dr["last_order_name"].ToString());
            this.image_url = Convert.ToString(dr["image_url"].ToString());
            this.default_addressID = Convert.ToInt32(dr["default_addressID"].ToString());
        }
        dr.Close();
    }

    public ShopifyCustomer(string email)
    {
        List<int> list = ShopifyAgent.ShopifyAgent.SyncShopify(); //sync orders/customers from Shopify

        //Loads a single record from a table
        string sql = @"
        SELECT *
        FROM ShopifyCustomers
        WHERE Email = @Email";
        SqlParameter[] parms = new SqlParameter[1];
        parms[0] = new SqlParameter("@Email", email);

        string shopConnStr = AdminCart.Config.ConnStrShopify();
        SqlDataReader dr = DBUtil.FillDataReader(sql, parms, shopConnStr);

        if (dr.Read())
        {
            this.shopifyCustomerID = Convert.ToInt32(dr["shopifyCustomerID"].ToString());
            this.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            this.accepts_marketing = Convert.ToBoolean(dr["accepts_marketing"].ToString());
            this.created_at = Convert.ToDateTime(dr["created_at"].ToString());
            this.email = Convert.ToString(dr["email"].ToString());
            this.first_name = Convert.ToString(dr["first_name"].ToString());
            this.id = Convert.ToInt64(dr["id"].ToString());
            this.last_name = Convert.ToString(dr["last_name"].ToString());
            this.last_order_id = Convert.ToInt64(dr["last_order_id"].ToString());
            this.multipass_identifier = Convert.ToString(dr["multipass_identifier"].ToString());
            this.note = Convert.ToString(dr["note"].ToString());
            this.orders_count = Convert.ToInt32(dr["orders_count"].ToString());
            this.state = Convert.ToString(dr["state"].ToString());
            this.total_spent = Convert.ToDecimal(dr["total_spent"].ToString());
            this.updated_at = Convert.ToDateTime(dr["updated_at"].ToString());
            this.verified_email = Convert.ToBoolean(dr["verified_email"].ToString());
            this.phone = Convert.ToString(dr["phone"].ToString());
            this.tags = Convert.ToString(dr["tags"].ToString());
            this.last_order_name = Convert.ToString(dr["last_order_name"].ToString());
            this.image_url = Convert.ToString(dr["image_url"].ToString());
            this.default_addressID = Convert.ToInt32(dr["default_addressID"].ToString());
        }
        dr.Close();
    }


    #endregion


    #region Methods
    //requires DBUtil class from classhatch.com
    public int SaveDB()
    {
        //Insert or Update record on SQL Table
        if (created_at == null) { created_at = DateTime.MinValue; }
        if (updated_at == null) { updated_at = DateTime.MinValue; }

        SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrShopify());

        string sql = "";


        if (ShopifyCustomerID == 0)
        {
            //Insert if primary key is default value of zero
            sql = @"
            INSERT ShopifyCustomers(
--ShopifyCustomerID,
shopifyOrderID,
accepts_marketing,
created_at,
email,
first_name,
id,
last_name,
last_order_id,
multipass_identifier,
note,
orders_count,
state,
total_spent,
updated_at,
verified_email,
phone,
tags,
last_order_name,
image_url,
default_addressID

             )VALUES(
--@ShopifyCustomerID,
@shopifyOrderID,
@accepts_marketing,
@created_at,
@email,
@first_name,
@id,
@last_name,
@last_order_id,
@multipass_identifier,
@note,
@orders_count,
@state,
@total_spent,
@updated_at,
@verified_email,
@phone,
@tags,
@last_order_name,
@image_url,
@default_addressID

            ); select id=scope_identity();";
        }
        else
        {
            //Update db record if primary key is not default value of zero
            sql = @"
            UPDATE ShopifyCustomers
            SET 
--ShopifyCustomerID= @ShopifyCustomerID,
shopifyOrderID = @shopifyOrderID,
accepts_marketing= @accepts_marketing,
created_at= @created_at,
email= @email,
first_name= @first_name,
id= @id,
last_name= @last_name,
last_order_id= @last_order_id,
multipass_identifier= @multipass_identifier,
note= @note,
orders_count= @orders_count,
state= @state,
total_spent= @total_spent,
updated_at= @updated_at,
verified_email= @verified_email,
phone = @phone,
tags= @tags,
last_order_name= @last_order_name,
image_url= @image_url,
default_addressID= @default_addressID

            WHERE ShopifyCustomerID = @ShopifyCustomerID; select id=@ShopifyCustomerID;";
        }
        
        SqlCommand cmd = new SqlCommand(sql, conn);

        SqlParameter shopifyCustomerIDParam = new SqlParameter("@shopifyCustomerID", SqlDbType.Int);
        SqlParameter shopifyOrderIDParam = new SqlParameter("@shopifyOrderID", SqlDbType.Int);
        SqlParameter accepts_marketingParam = new SqlParameter("@accepts_marketing", SqlDbType.Bit);
        SqlParameter created_atParam = new SqlParameter("@created_at", SqlDbType.DateTime);
        SqlParameter emailParam = new SqlParameter("@email", SqlDbType.VarChar);
        SqlParameter first_nameParam = new SqlParameter("@first_name", SqlDbType.VarChar);
        SqlParameter idParam = new SqlParameter("@id", SqlDbType.BigInt);
        SqlParameter last_nameParam = new SqlParameter("@last_name", SqlDbType.VarChar);
        SqlParameter last_order_idParam = new SqlParameter("@last_order_id", SqlDbType.BigInt);
        SqlParameter multipass_identifierParam = new SqlParameter("@multipass_identifier", SqlDbType.VarChar);
        SqlParameter noteParam = new SqlParameter("@note", SqlDbType.VarChar);
        SqlParameter orders_countParam = new SqlParameter("@orders_count", SqlDbType.Int);
        SqlParameter stateParam = new SqlParameter("@state", SqlDbType.VarChar);
        SqlParameter total_spentParam = new SqlParameter("@total_spent", SqlDbType.Money);
        SqlParameter updated_atParam = new SqlParameter("@updated_at", SqlDbType.DateTime);
        SqlParameter verified_emailParam = new SqlParameter("@verified_email", SqlDbType.Bit);
        SqlParameter phoneParam = new SqlParameter("@phone", SqlDbType.VarChar);
        SqlParameter tagsParam = new SqlParameter("@tags", SqlDbType.VarChar);
        SqlParameter last_order_nameParam = new SqlParameter("@last_order_name", SqlDbType.VarChar);
        SqlParameter image_urlParam = new SqlParameter("@image_url", SqlDbType.VarChar);
        SqlParameter default_addressIDParam = new SqlParameter("@default_addressID", SqlDbType.Int);

        shopifyCustomerIDParam.Value = shopifyCustomerID;
        shopifyOrderIDParam.Value = shopifyOrderID;
        accepts_marketingParam.Value = (object)accepts_marketing??DBNull.Value;
        created_atParam.Value = created_at;
        emailParam.Value = email;
        first_nameParam.Value = (object)first_name ?? DBNull.Value;
        idParam.Value = id;
        last_nameParam.Value = (object)last_name ?? DBNull.Value;
        last_order_idParam.Value = (object)last_order_id?? DBNull.Value;
        multipass_identifierParam.Value = (object)multipass_identifier ?? DBNull.Value;
        noteParam.Value = (object)note ?? DBNull.Value;
        orders_countParam.Value = (object)orders_count ?? DBNull.Value;
        stateParam.Value = state;
        total_spentParam.Value = (object)total_spent ?? DBNull.Value;
        updated_atParam.Value = (object)updated_at ?? DBNull.Value;
        verified_emailParam.Value = (object)verified_email ?? DBNull.Value;
        phoneParam.Value = (object)phone ?? DBNull.Value;
        tagsParam.Value = (object)tags ?? DBNull.Value;
        last_order_nameParam.Value = (object)last_order_name ?? DBNull.Value;
        image_urlParam.Value = (object)image_url ?? DBNull.Value;
        default_addressIDParam.Value = default_addressID;

        cmd.Parameters.Add(shopifyCustomerIDParam);
        cmd.Parameters.Add(shopifyOrderIDParam);
        cmd.Parameters.Add(accepts_marketingParam);
        cmd.Parameters.Add(created_atParam);
        cmd.Parameters.Add(emailParam);
        cmd.Parameters.Add(first_nameParam);
        cmd.Parameters.Add(idParam);
        cmd.Parameters.Add(last_nameParam);
        cmd.Parameters.Add(last_order_idParam);
        cmd.Parameters.Add(multipass_identifierParam);
        cmd.Parameters.Add(noteParam);
        cmd.Parameters.Add(orders_countParam);
        cmd.Parameters.Add(stateParam);
        cmd.Parameters.Add(total_spentParam);
        cmd.Parameters.Add(updated_atParam);
        cmd.Parameters.Add(verified_emailParam);
        cmd.Parameters.Add(phoneParam);
        cmd.Parameters.Add(tagsParam);
        cmd.Parameters.Add(last_order_nameParam);
        cmd.Parameters.Add(image_urlParam);
        cmd.Parameters.Add(default_addressIDParam);

        conn.Open();
        int result = Convert.ToInt32(cmd.ExecuteScalar().ToString());
        conn.Close();
        return result;
    }

    static public List<ShopifyCustomer> GetShopifyCustomerList()
    {
        List<ShopifyCustomer> thelist = new List<ShopifyCustomer>();

        string sql = "SELECT * FROM ShopifyCustomers";

        SqlDataReader dr = DBUtil.FillDataReader(sql, AdminCart.Config.ConnStrShopify());

        while (dr.Read())
        {
            ShopifyCustomer obj = new ShopifyCustomer();

            obj.shopifyCustomerID = Convert.ToInt32(dr["shopifyCustomerID"].ToString());
            obj.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            obj.accepts_marketing = Convert.ToBoolean(dr["accepts_marketing"].ToString());
            obj.created_at = Convert.ToDateTime(dr["created_at"].ToString());
            obj.email = Convert.ToString(dr["email"].ToString());
            obj.first_name = Convert.ToString(dr["first_name"].ToString());
            obj.id = Convert.ToInt64(dr["id"].ToString());
            obj.last_name = Convert.ToString(dr["last_name"].ToString());
            obj.last_order_id = Convert.ToInt64(dr["last_order_id"].ToString());
            obj.multipass_identifier = Convert.ToString(dr["multipass_identifier"].ToString());
            obj.note = Convert.ToString(dr["note"].ToString());
            obj.orders_count = Convert.ToInt32(dr["orders_count"].ToString());
            obj.state = Convert.ToString(dr["state"].ToString());
            obj.total_spent = Convert.ToDecimal(dr["total_spent"].ToString());
            obj.updated_at = Convert.ToDateTime(dr["updated_at"].ToString());
            obj.verified_email = Convert.ToBoolean(dr["verified_email"].ToString());
            obj.phone = Convert.ToString(dr["phone"].ToString());
            obj.tags = Convert.ToString(dr["tags"].ToString());
            obj.last_order_name = Convert.ToString(dr["last_order_name"].ToString());
            obj.image_url = Convert.ToString(dr["image_url"].ToString());
            obj.default_addressID = Convert.ToInt32(dr["default_addressID"].ToString());

            thelist.Add(obj);
        }
        dr.Close();
        return thelist;

    }

    public static List<ShopifyCustomer> GetShopifyCustomersViaAPIAsync()
    {
        string uri = "admin/customers.json?since_id=0&limit=249&status=any";
        string apiResult = JsonHelpers.GETfromShopify(uri);

        List<ShopifyCustomer> customerList = new List<ShopifyCustomer>();
        try
        {
            ShopifyCustomers sos = JsonConvert.DeserializeObject<ShopifyCustomers>(apiResult);
            for (int i = 0; i < sos.Customers.Length; i++)
            { customerList.Add(sos.Customers[i]); }
        }
        catch
        {
            string msg = "error in GetShopifyCustomersViaAPIAsync </br>";
            msg += "jsonURL: " + uri + "</br>";
            msg += "response: " + apiResult;
        }
        return customerList;

    }

    public static ShopifyCustomer GetShopifyCustomerViaAPI(long id)
    {
        string uri = "admin/customers/" + id.ToString() + ".json";
        ShopifyCustomer customer = new ShopifyCustomer();
        string apiResult = FMWebApp.Helpers.JsonHelpers.GETfromShopify(uri);

        try
        {
            //ShopifyCustomer customer = JsonConvert.DeserializeObject<ShopifyCustomer>(apiResult);
            var buffer = JObject.Parse(apiResult);
            customer = buffer.ToObject<ShopifyCustomerSingle>().customer;
        }
        catch (Exception ex)
        {
            string msg = "error in GetShopifyCustomerViaAPI </br>";
            msg += "jsonURL: " + uri + "</br>";
            msg += "response: " + apiResult;
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
        }

        return customer;

    }

    public static long PutShopifyCustomerViaAPI(ShopifyCustomer shopCustomer)
    {
        long partnerID = -1;
        string uri = "admin/customers/" + shopCustomer.Id.ToString() + ".json";

        /*
        ShopifyOrderSingle singleOrder = new ShopifyOrderSingle();
        singleOrder.order = shopOrder;
        string dataStr = JsonConvert.SerializeObject(singleOrder);
        byte[] dataBuffer = Encoding.ASCII.GetBytes(dataStr);
        */

        String JsonString = @"{""customer"":{""email"":""**email**"", ""note"":""**note**""}}";

        JsonString = JsonString.Replace("**email**", shopCustomer.Email.ToString());
        JsonString = JsonString.Replace("**note**", shopCustomer.Note.ToString());

        string apiResult = JsonHelpers.PUTtoShopify(uri,JsonString);
        try
        {
            ShopifyCustomerSingle responseCustomer = JsonConvert.DeserializeObject<ShopifyCustomerSingle>(apiResult);
            partnerID = responseCustomer.customer.id;
        }
        catch (Exception ex)
        {
            //put some exception handling code here
            string msg = "error in PutShopifyCustomerViaAPI </br>";
            msg += "jsonURL: " + uri + "</br>";
            msg += "jsonTemplate: " + JsonString + "</br>";
            msg += "response: " + apiResult;
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            partnerID = 0;
        }
        return partnerID; //returns id if successful, -1 if not

    }

    public static long PostShopifyCustomerViaAPI(ShopifyCustomer shopCustomer)
    {
        long partnerID = -1;

        //Json syntax checker: http://jsonlint.com/

        string jsonTemplate = JsonMapShopifyCustomer(shopCustomer, "POST");

        string jsonURL = "admin/customers.json";
        //Uri myUri = new Uri(uri);

        string apiResult = JsonHelpers.POSTtoShopify(jsonURL, jsonTemplate);

        try
        {
                ShopifyCustomerSingle responseCustomer = JsonConvert.DeserializeObject<ShopifyCustomerSingle>(apiResult);
                partnerID = responseCustomer.customer.id;
        }
        catch (Exception ex)
        {
            //put some exception handling code here
            string msg = "error in PostShopifyCustomerViaAPI </br>";
            msg += "jsonURL: " + jsonURL + "</br>";
            msg += "jsonTemplate: " + jsonTemplate + "</br>";
            msg += "response: " + apiResult;
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            partnerID = 0;
        }
        return partnerID; //returns id of successful, -1 if not

    }

    public static long PutShopifyCustomerViaAPI(long id, ShopifyCustomer shopCustomer)
    {
        long partnerID = -1;

        //Json syntax checker: http://jsonlint.com/
        string jsonTemplate = JsonMapShopifyCustomer(shopCustomer, "PUT");

        NameValueCollection collection = new NameValueCollection();
        collection.Add("id", shopCustomer.id.ToString());
        collection.Add("email", shopCustomer.Email);
        collection.Add("note", shopCustomer.Note);

        string jsonURL = "admin/customers/" + id.ToString() + ".json";
        //Uri myUri = new Uri(uri);

        string apiResult = JsonHelpers.PUTtoShopify(jsonURL, jsonTemplate);

        try
        {
            ShopifyCustomerSingle responseCustomer = JsonConvert.DeserializeObject<ShopifyCustomerSingle>(apiResult);
            partnerID = responseCustomer.customer.id;
        }
        catch (Exception ex)
        {
            //put some exception handling code here
            string msg = "error in PutShopifyCustomerViaAPI(long id, ShopifyCustomer shopCustomer) </br>";
            msg += "jsonURL: " + jsonURL + "</br>";
            msg += "jsonTemplate: " + jsonTemplate + "</br>";
            msg += "response: " + apiResult;
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            partnerID = 0;
        }
        return partnerID; //returns id of successful, -1 if not

    }

    public static long PostShopifyCustomerAddressViaAPI(ShopifyCustomer shopCustomer)
    {
        long address_id = -1;

        //Json syntax checker: http://jsonlint.com/
        string jsonTemplate = JsonMapShopifyCustomerAddress(shopCustomer);

        long id = shopCustomer.Id;

        string jsonURL = "admin/customers/" + id.ToString() + "/addresses.json";
        //Uri myUri = new Uri(uri);

        string apiResult = JsonHelpers.POSTtoShopify(jsonURL, jsonTemplate);

        try
        {
            ShopifyAddress responseAddress = JsonConvert.DeserializeObject<ShopifyCustomerAddress>(apiResult).Customer_address;
            address_id = responseAddress.Id;
        }
        catch (Exception ex)
        {
            //put some exception handling code here
            string msg = "error in PostShopifyCustomerAddressViaAPI" + Environment.NewLine;
            msg += "jsonURL: " + jsonURL + Environment.NewLine;
            msg += "jsonTemplate: " + jsonTemplate + Environment.NewLine;
            msg += "response: " + apiResult + Environment.NewLine;
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            address_id = 0;
        }
        return address_id; //returns id of successful, -1 if not

    }

    public static bool MakeAddressDefault(long address_id, ShopifyCustomer shopCustomer)
    {
        bool isDefault = false;

        //Json syntax checker: http://jsonlint.com/
        string jsonTemplate = JsonMapShopifyCustomerAddress(shopCustomer);

        long id = shopCustomer.Id;

        string jsonURL = "admin/customers/" + id.ToString() + "/addresses/" + address_id.ToString() + "/default.json";
        //Uri myUri = new Uri(uri);

        string apiResult = JsonHelpers.PUTtoShopify(jsonURL, jsonTemplate);

        try
        {
            ShopifyAddress responseAddress = JsonConvert.DeserializeObject<ShopifyCustomerAddress>(apiResult).Customer_address;
            isDefault = responseAddress.IsDefault;
        }
        catch (Exception ex)
        {
            //put some exception handling code here
            string msg = "error in MakeAddressDefault" + Environment.NewLine;
            msg += "jsonURL: " + jsonURL + Environment.NewLine;
            msg += "jsonTemplate: " + jsonTemplate + Environment.NewLine;
            msg += "response: " + apiResult + Environment.NewLine;
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            address_id = 0;
            isDefault = false;
        }
        return isDefault; //returns id of successful, -1 if not

    }

    public static List<ShopifyAgent.ShopifyOrder> GetOrderList(ShopifyCustomer shopCustomer)
    {
        List<ShopifyAgent.ShopifyOrder> shopOrderList = new List<ShopifyAgent.ShopifyOrder>();

        string jsonURL = "/admin/orders.json?customer_id=" + shopCustomer.Id.ToString();
        //Uri myUri = new Uri(uri);

        string apiResult = JsonHelpers.GETfromShopify(jsonURL);
        ShopifyAgent.ShopifyOrders response = new ShopifyAgent.ShopifyOrders();

        try
        {
            response = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyOrders>(apiResult);
        }
        catch (Exception ex)
        {
            //put some exception handling code here
            string msg = "error in ShopifyCustomer: GetOrderList" + Environment.NewLine;
            msg += "jsonURL: " + jsonURL + Environment.NewLine;
            msg += "response: " + apiResult + Environment.NewLine;
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
        }

        if (response.Orders.Length > 0)
        {
            for(int i=0; i < response.Orders.Length; i++)
            {
                shopOrderList.Add(response.Orders[i]);
            }
        }

        return shopOrderList; //returns list of all orders Shopify has on record for customer
    }


    public static long UpdateShopifyCustomer (AdminCart.Customer customer)
    {
        long customer_id = 0;
        long address_id = 0;
        bool isDefault = false;

        ShopifyCustomer shopCustomer = new ShopifyCustomer();
        shopCustomer.First_name = customer.FirstName;
        shopCustomer.Last_name = customer.LastName;
        shopCustomer.Email = customer.Email;
        shopCustomer.Accepts_marketing = customer.FaceMasterNewsletter;

        AdminCart.Address billAddr = AdminCart.Address.LoadAddress(customer.CustomerID, 1); //get billing address;

        //List<ShopifyAddress> aList = new List<ShopifyAddress>();
        ShopifyAddress a = new ShopifyAddress();
        a.First_name = customer.FirstName;
        a.Last_name = customer.LastName;
        a.Address1 = billAddr.Street;
        a.Address2 = billAddr.Street2;
        a.City = billAddr.City;
        a.Province_code = billAddr.State;
        a.Country_code = billAddr.Country;
        a.Zip = billAddr.Zip;
        a.Phone = customer.Phone;
        //aList.Add(a);
        //shopCustomer.addresses = aList.ToArray();
        shopCustomer.Default_address = a;

        ShopifyCustomer tempCustomer = new ShopifyCustomer(shopCustomer.email); //check if out-of-sync

        //update Shopify
        if ((customer.PartnerID == 0) && (tempCustomer.ShopifyCustomerID == 0))
        { customer_id = PostShopifyCustomerViaAPI(shopCustomer); }
        else if ((customer.PartnerID == 0) && (tempCustomer.ShopifyCustomerID != 0))
        { customer_id = PutShopifyCustomerViaAPI(customer.PartnerID, shopCustomer); }
        else if ((customer.PartnerID != 0) && (tempCustomer.ShopifyCustomerID != 0))
        { customer_id = PutShopifyCustomerViaAPI(customer.PartnerID, shopCustomer); }


        if (customer_id <= 0)
        {
            customer_id = 0;
            string msg = "ShopifyCustomer: UpdateShopifyCustomer: customer_id = 0:" + Environment.NewLine;
            msg += "FM customerID: " + customer.CustomerID.ToString() + Environment.NewLine;
            msg += "FM customer name: " + customer.FirstName + " " + customer.LastName + Environment.NewLine;
            msg += "Email: " + customer.Email + Environment.NewLine;
            mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "", "", "ShopifyCustomer Update Error", msg, true);
        }
        else
        {
            shopCustomer.Id = customer_id; //what's its ID out at Shopify?
            address_id = PostShopifyCustomerAddressViaAPI(shopCustomer);
            if (address_id <= 0)
            {
                address_id = 0;
                string msg = "ShopifyCustomer: UpdateShopifyCustomer: address_id = 0:" + Environment.NewLine;
                msg += "FM customerID: " + customer.CustomerID.ToString() + Environment.NewLine;
                msg += "FM customer name: " + customer.FirstName + " " + customer.LastName + Environment.NewLine;
                msg += "Shopify CustomerID: " + shopCustomer.Id + Environment.NewLine;
                msg += "Email: " + customer.Email + Environment.NewLine;
                mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "", "", "ShopifyCustomer Update Error", msg, false);
            }
            else
            {
                isDefault = MakeAddressDefault(address_id, shopCustomer);
                if (!isDefault)
                {
                    string msg = "ShopifyCustomer: UpdateShopifyCustomer: isDefault = false" + Environment.NewLine; ;
                    msg += "FM customerID: " + customer.CustomerID.ToString() + Environment.NewLine;
                    msg += "FM customer name: " + customer.FirstName + " " + customer.LastName + Environment.NewLine;
                    msg += "Shopify CustomerID: " + shopCustomer.Id + Environment.NewLine;
                    msg += "Shopify AddressID: " + address_id + Environment.NewLine;
                    msg += "Email: " + customer.Email + Environment.NewLine;
                    mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "", "", "ShopifyCustomer Update Error", msg, false);
                }
            }
        }
        
        return customer_id;
    }

    private static string JsonMapShopifyCustomer(ShopifyCustomer shopCustomer, string method)
    {
        string jsonTemplate = @"{""customer"": {#CUSTOMER#}}"; 
        string customerStrPUT = @"""first_name"":""#FIRST#"",""last_name"":""#LAST#"",""email"":""#EMAIL#"", ""accepts_marketing"":""#MKTG#""";
        string customerStrPOST = @"""first_name"":""#FIRST#"",""last_name"":""#LAST#"",""email"":""#EMAIL#"", ""accepts_marketing"":""#MKTG#"", ""verified_email"":false";
        //string addressStr = @" {""address1"":""#STREET#"",""address2"":""#STREET2#"",""city"":""#CITY#"",""province"":""#PROVINCE#"",""phone"":""#PHONE#"",""zip"":""#ZIP#"",""last_name"":""#ADDR_LAST#"",""first_name"":""#ADDR_FIRST#"",""country"":""#COUNTRY#""}";

        string customerStr = (method.ToUpperInvariant() == "POST") ? customerStrPOST : customerStrPUT;

        //customerStr = customerStr.Replace("#VERIFY_EMAIL#", shopCustomer.customerCreate.Verified_Email.ToString().ToLowerInvariant());
        customerStr = customerStr.Replace("#FIRST#", shopCustomer.First_name);
        customerStr = customerStr.Replace("#LAST#", shopCustomer.Last_name);
        customerStr = customerStr.Replace("#EMAIL#", shopCustomer.Email);
        if (shopCustomer.Accepts_marketing)
        {
            customerStr = customerStr.Replace("#MKTG#", "true");
        }
        else
        {
            customerStr = customerStr.Replace("#MKTG#", "false");
        }

        jsonTemplate = jsonTemplate.Replace("#CUSTOMER#", customerStr);

        return jsonTemplate;
    }

    private static string JsonMapShopifyCustomerAddress(ShopifyCustomer shopCustomer)
    {
        string jsonTemplate = @"{""address"":#ADDRARRAY#}";

        string addressStr = @" {""address1"":""#STREET#"",""address2"":""#STREET2#"",""city"":""#CITY#"",""province"":""#PROVINCE#"",""province_code"":""#PROVINCECODE#"",""phone"":""#PHONE#"",""zip"":""#ZIP#"",""last_name"":""#ADDR_LAST#"",""first_name"":""#ADDR_FIRST#"",""name"":""#ADDR_NAME#"",""country"":""#COUNTRY#"",""country_code"":""#COUNTRYCODE#""}";
        string stateName = FM2015.Models.Location.StateNameFromCode(shopCustomer.Default_address.Province_code, shopCustomer.Default_address.Country_code);
        string countryName = FM2015.Models.Location.CountryNameFromCode(shopCustomer.Default_address.Country_code);

        string addr = addressStr;
        addr = addr.Replace("#STREET#", shopCustomer.Default_address.Address1);
        addr = addr.Replace("#STREET2#", shopCustomer.Default_address.Address2);
        addr = addr.Replace("#CITY#", shopCustomer.Default_address.City);
        addr = addr.Replace("#PROVINCE#", stateName);
        addr = addr.Replace("#PROVINCECODE#", shopCustomer.Default_address.Province_code);
        addr = addr.Replace("#COUNTRY#", countryName);
        addr = addr.Replace("#COUNTRYCODE#", shopCustomer.Default_address.Country_code);
        addr = addr.Replace("#ZIP#", shopCustomer.Default_address.Zip);
        addr = addr.Replace("#PHONE#", shopCustomer.Default_address.Phone);
        addr = addr.Replace("#ADDR_LAST#", shopCustomer.Default_address.Last_name);
        addr = addr.Replace("#ADDR_FIRST#", shopCustomer.Default_address.First_name);
        addr = addr.Replace("#ADDR_NAME#", shopCustomer.Default_address.First_name + " " + shopCustomer.Default_address.Last_name);
        jsonTemplate = jsonTemplate.Replace("#ADDRARRAY#", addr);

        return jsonTemplate;
    }
   #endregion


    #region Validation
    //Requires Validation class from classhatch.com
    static public bool IsValid(ShopifyCustomer obj)
    {
        bool isValid = false;

        if (GetErrors(obj).Count == 0)
        {
            isValid = true;
        }

        return isValid;
    }


    static public List<ValidationError> GetErrors(ShopifyCustomer obj)
    {
        List<ValidationError> errorlist = new List<ValidationError>();

        /*
        if (ValidateUtil.IsTooLong(20, obj.FirstName))
        {
            ValidationError e = new ValidationError();
            e.Field = "First Name";
            e.ErrorDescription = "20 characters maximium are allowed";
            errorlist.Add(e);
        };
        */

        return errorlist;
    }
    #endregion


    #region OverRides

    #endregion

}



