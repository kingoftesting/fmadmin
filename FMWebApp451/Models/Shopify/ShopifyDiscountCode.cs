﻿using System;
using System.Data;

using System.Data.SqlClient;
using System.Collections.Generic;

using Newtonsoft.Json;
using FM2015.Helpers;
using ShopifyAgent;
using System.Configuration;

/// <summary>
/// Summary description for ShopifyDiscountCodes
/// </summary>
public class ShopifyDiscountCodes
{
    string shopConnStr = AdminCart.Config.ConnStrShopify();

    #region Private/Public Vars
    private int id;
    public int ID { get { return id; } set { id = value; } }
    private int shopifyOrderID;

    public int ShopifyOrderID { get { return shopifyOrderID; } set { shopifyOrderID = value; } }

    private string code;
    [JsonProperty("code")]
    public string Code { get { return code; } set { code = value; } }

    private string amount;
    [JsonProperty("amount")]
    public string Amount { get { return amount; } set { amount = value; } }

    private string type;
    [JsonProperty("type")]
    public string Type { get { return type; } set { type = value; } }
    #endregion

    public ShopifyDiscountCodes()
    {
        ID = 0;
        ShopifyOrderID = 0;
        Code = "";
        Amount = "";
        Type = "";
    }

    public ShopifyDiscountCodes(int shopifyOrderID)
    {
        string sql = @" 
            SELECT * FROM  ShopifyDiscountCodes 
            WHERE ShopifyOrderID = @ShopifyOrderID 
        ";
        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, shopifyOrderID);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, shopConnStr);
        if (dr.Read())
        {
            ID = Convert.ToInt32(dr["ID"]);
            ShopifyOrderID = Convert.ToInt32(dr["ShopifyOrderID"].ToString());
            Code = dr["Code"].ToString();
            Amount = dr["Amount"].ToString();
            Type = dr["Type"].ToString();
        }
        if (dr != null) { dr.Close(); }
    }

    public int Save(int shopifyOrderID)
    {
        int result = 0;
        SqlParameter[] mySqlParameters = null;
        SqlDataReader dr = null;
        if (shopifyOrderID == 0)
        {
            string sql = @" 
                INSERT INTO ShopifyDiscountCodes 
                (ShopifyOrderID, Code, Amount, Type) 
                VALUES(@ShopifyOrderID, @Code, @Amount, @Type); 
                SELECT ID=@@identity;  
                ";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, ShopifyOrderID, Code, Amount, Type);
            dr = DBUtil.FillDataReader(sql, mySqlParameters, shopConnStr);
            if (dr.Read())
            {
                result = Convert.ToInt32(dr["ID"]);
            }
            else
            {
                result = 99;
            }
        }
        else
        {
            string sql = @" 
                UPDATE ShopifyDiscountCodes 
                SET  
                    Code = @Code,  
                    Amount = @Amount,  
                    Type = @Type  
                WHERE ShopifyOrderID = @ShopifyOrderID  
";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, ShopifyOrderID, Code, Amount, Type, ID);
            DBUtil.Exec(sql, shopConnStr, mySqlParameters);
        }
        return result;
    }

    /***********************************
    * Static methods
    ************************************/

    /// <summary>
    /// Returns a collection with all ShopifyDiscountCodes
    /// </summary>
    public static List<ShopifyDiscountCodes> GetShopifyDiscountCodesList()
    {
        List<ShopifyDiscountCodes> thelist = new List<ShopifyDiscountCodes>();

        string sql = @" 
            SELECT * FROM ShopifyDiscountCodes 
            ";

        SqlDataReader dr = DBUtil.FillDataReader(sql);

        while (dr.Read())
        {
            ShopifyDiscountCodes obj = new ShopifyDiscountCodes();

            obj.ID = Convert.ToInt32(dr["ID"]);
            obj.ShopifyOrderID = Convert.ToInt32(dr["ShopifyOrderID"].ToString());
            obj.Code = dr["Code"].ToString();
            obj.Amount = dr["Amount"].ToString();
            obj.Type = dr["Type"].ToString();

            thelist.Add(obj);
        }
        if (dr != null) { dr.Close(); }
        return thelist;
    }

    /// <summary>
    /// Returns a className object with the specified ID
    /// </summary>

    public static ShopifyDiscountCodes GetShopifyDiscountCodesByID(int id)
    {
        ShopifyDiscountCodes obj = new ShopifyDiscountCodes(id);
        return obj;
    }

    /// <summary>
    /// Updates an existing ShopifyDiscountCodes
    /// </summary>
    public static bool UpdateShopifyDiscountCodes(int id,
    int shopifyorderid, string code, string amount, string type)
    {
        ShopifyDiscountCodes obj = new ShopifyDiscountCodes();

        obj.ID = id;
        obj.ShopifyOrderID = shopifyorderid;
        obj.Code = code;
        obj.Amount = amount;
        obj.Type = type;
        int ret = obj.Save(obj.ID);
        return (ret > 0) ? true : false;
    }

    /// <summary>
    /// Creates a new ShopifyDiscountCodes
    /// </summary>
    public static int InsertShopifyDiscountCodes(
    int shopifyorderid, string code, string amount, string type)
    {
        ShopifyDiscountCodes obj = new ShopifyDiscountCodes();

        obj.ShopifyOrderID = shopifyorderid;
        obj.Code = code;
        obj.Amount = amount;
        obj.Type = type;
        int ret = obj.Save(obj.ID);
        return ret;
    }

}