﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;
using FM2015.Helpers;
using System.Diagnostics;
using Stripe;
using System.Linq;
using FMWebApp451.Services.StripeServices;

public class ShopifyFMOrderTranslator
{
    public ShopifyFMOrderTranslator()
    {
    }

    /// <summary>
    /// Merge new Shopify orders into the FM Database
    /// </summary>
    public static void MergeOrders(string id, int companyID)
    {
        if (!AdminCart.Config.SyncShopify)
        {
            return;
        }

        long longID = Convert.ToInt64(id);
        ShopifyAgent.ShopifyOrder shopOrder = new ShopifyAgent.ShopifyOrder(longID, companyID);

        AdminCart.Order fmOrder = new AdminCart.Order();
        shopOrder.Name = shopOrder.Name.Replace("#", "");
        int VendorOrderID = Convert.ToInt32(shopOrder.Name);

        fmOrder.OrderID = AdminCart.Order.OrderExists(VendorOrderID, companyID); //must have an orderid and order details

        if (fmOrder.OrderID == 0) //create a new order IFF it doesn't altready exist
        {
            //either create a new FM customer or find an existing one
            int fmCustomerID = GetFMCustomerID(shopOrder.ShopifyOrderID);
            if (fmCustomerID <= 0)
            {
                //some exception handling goes here
                string msg = "CustomerID = " + fmCustomerID.ToString() + "FMOrderID = 0, Shopify VendorOrderID = " + shopOrder.Name.ToString();
                mvc_Mail.SendDiagEmail("Error in Shopify Merge: GetFMCustomer", msg);
                throw new IndexOutOfRangeException("Error in Shopify Merge: GetFMCustomer" + msg);
            }

            fmOrder.OrderID = CreateNewFMOrder(shopOrder, fmCustomerID);
            UpdateOrderActions(shopOrder, fmOrder.OrderID); //indicate order was valid
        }
    }

    /// <summary>
    /// Get Shopify customer info based on an Shopify customer ID; check if customer is in FM Database. If yes return the FM customer ID, 
    /// otherwise create a new FM customer and return the new FM customer ID
    /// </summary>
    static public int GetFMCustomerID(int ShopifyOrderId)
    {
        int FMCustomerID = 0;

        ShopifyCustomer ShopCustomer = new ShopifyCustomer(ShopifyOrderId);
        ShopifyBillingAddress ShopBillAddr = new ShopifyBillingAddress(ShopifyOrderId);
        ShopifyShippingAddress ShopShipAddr = new ShopifyShippingAddress(ShopifyOrderId);

        AdminCart.Customer FMCustomer = AdminCart.Customer.FindCustomerByEmail(ShopCustomer.Email);

        /*
        if (FMCustomer.CustomerID == 0) //maybe we can find the customer by name & address
        {
            FMCustomer = AdminCart.Customer.FindCustomer(ShopCustomer.First_name, ShopCustomer.Last_name,
                ShopBillAddr.Address1, ShopBillAddr.City, ShopBillAddr.Zip);
        }
        */

        if (FMCustomer.CustomerID == 0) //couldn't find customer so create a new one
        {
            FMCustomerID = AdminCart.Customer.CreateNewCustomer(ShopCustomer, ShopBillAddr, ShopShipAddr);
        }
        else //found customer by email; do address check for updates
        {
            FMCustomerID = FMCustomer.CustomerID;
            AdminCart.Address billAdr = AdminCart.Address.LoadAddress(FMCustomerID, 1);
            AdminCart.Address shipAdr = AdminCart.Address.LoadAddress(FMCustomerID, 2);

            if ((billAdr.Street != ShopBillAddr.Address1) && (ShopBillAddr.ShopifyBillingAddressID != 0)) //looks like a new billing address
            {
                billAdr.Street = ShopBillAddr.Address1;
                billAdr.Street2 = ShopBillAddr.Address2;
                billAdr.City = ShopBillAddr.City;
                billAdr.State = ShopBillAddr.Province_code;
                billAdr.Country = ShopBillAddr.Country_code;
                billAdr.Zip = ShopBillAddr.Zip;
                billAdr.Save(FMCustomer, 1);

                shipAdr.Street = ShopBillAddr.Address1;
                shipAdr.Street2 = ShopBillAddr.Address2;
                shipAdr.City = ShopBillAddr.City;
                shipAdr.State = ShopBillAddr.Province_code;
                shipAdr.Country = ShopBillAddr.Country_code;
                shipAdr.Zip = ShopBillAddr.Zip;
                shipAdr.Save(FMCustomer, 2);
                string msg = "CustomerID = " + FMCustomerID.ToString() + Environment.NewLine;
                msg += "ShopifyOrderID =" + ShopifyOrderId.ToString() + Environment.NewLine;
                msg += "LastName =" + ShopCustomer.Last_name.ToString() + Environment.NewLine;
                //mvc_Mail.SendDiagEmail("ShopifyFMOrderTranslator Merge: GetFMCustomerID: Bill Adr mismatch: using ShopBillAdr for BillAdr & ShipAdr", msg);
            }

            if ((shipAdr.Street != ShopShipAddr.Address1) && (ShopShipAddr.ShopifyShippingAddressID != 0)) //looks like a new billing address
            {
                shipAdr.Street = ShopShipAddr.Address1;
                shipAdr.Street2 = ShopShipAddr.Address2;
                shipAdr.City = ShopShipAddr.City;
                shipAdr.State = ShopShipAddr.Province_code;
                shipAdr.Country = ShopShipAddr.Country_code;
                shipAdr.Zip = ShopShipAddr.Zip;
                shipAdr.Save(FMCustomer, 2);
                string msg = "CustomerID = " + FMCustomerID.ToString() + Environment.NewLine;
                msg += "ShopifyOrderID =" + ShopifyOrderId.ToString() + Environment.NewLine;
                msg += "LastName =" + ShopCustomer.Last_name.ToString() + Environment.NewLine;
                //mvc_Mail.SendDiagEmail("ShopifyFMOrderTranslator Merge: GetFMCustomerID: Ship Adr mismatch: using ShopShipAdr for ShipAdr", msg);
            }

            if (billAdr.AddressId == 0)
            {
                billAdr.Street = ShopBillAddr.Address1;
                billAdr.Street2 = ShopBillAddr.Address2;
                billAdr.City = ShopBillAddr.City;
                billAdr.State = ShopBillAddr.Province_code;
                billAdr.Country = ShopBillAddr.Country_code;
                billAdr.Zip = ShopBillAddr.Zip;
                billAdr.Save(FMCustomer, 1);
                string msg = "CustomerID = " + FMCustomerID.ToString() + Environment.NewLine;
                msg += "ShopifyOrderID =" + ShopifyOrderId.ToString() + Environment.NewLine;
                msg += "LastName =" + ShopCustomer.Last_name.ToString() + Environment.NewLine;
               // mvc_Mail.SendDiagEmail("ShopifyFMOrderTranslator Merge: GetFMCustomerID: billAdrID = 0: using ShopBillAdr for BillAdr", msg);
            }

            if (shipAdr.AddressId == 0) //use Billing Address
            {
                if (ShopShipAddr.Address1 == null) //use null address as last resort; we'll update at fulfillment 
                {
                    shipAdr.Street = "n/a";
                    shipAdr.Street2 = "n/a";
                    shipAdr.City = "n/a";
                    shipAdr.State = "n/a";
                    shipAdr.Country = "n/a";
                    shipAdr.Zip = "n/a";
                    string msg = "CustomerID = " + FMCustomerID.ToString() + Environment.NewLine;
                    msg += "ShopifyOrderID =" + ShopifyOrderId.ToString() + Environment.NewLine;
                    msg += "LastName =" + ShopCustomer.Last_name.ToString() + Environment.NewLine;
                    //mvc_Mail.SendDiagEmail("ShopifyFMOrderTranslator Merge: GetFMCustomerID: shipAdrID & Shopify ShipAdr = 0: using NULL adr", msg);

                }
                else
                {
                    shipAdr.Street = ShopShipAddr.Address1;
                    shipAdr.Street2 = ShopShipAddr.Address2;
                    shipAdr.City = ShopShipAddr.City;
                    shipAdr.State = ShopShipAddr.Province_code;
                    shipAdr.Country = ShopShipAddr.Country_code;
                    shipAdr.Zip = ShopShipAddr.Zip;
                    string msg = "CustomerID = " + FMCustomerID.ToString() + Environment.NewLine;
                    msg += "ShopifyOrderID =" + ShopifyOrderId.ToString() + Environment.NewLine;
                    msg += "LastName =" + ShopCustomer.Last_name.ToString() + Environment.NewLine;
                    //mvc_Mail.SendDiagEmail("ShopifyFMOrderTranslator Merge: GetFMCustomerID: shipAdrID = 0: using ShopShipAdr for ShipAdr", msg);
                }

                shipAdr.Save(FMCustomer, 2);
            }
        }

        return FMCustomerID;

    }

    /// <summary>
    /// Get HDI customer info based on an HDI customer ID; check if customer is in FM Database. If yes return the FM customer ID, 
    /// otherwise create a new FM customer and return the new FM customer ID
    /// </summary>
    static public int CreateNewFMOrder(ShopifyAgent.ShopifyOrder o, int fmCustomerID)
    {
        int fmOrderID = FindFMOrder(o);
        if (fmOrderID > 0)
        {
            List<AdminCart.OrderDetail> odList = GetOrderDetailList(o.ShopifyOrderID, fmOrderID);
            if (odList.Count == 0)
            {
                //dbErrorLogging.LogError()
                string msg = "CustomerID = " + fmCustomerID.ToString() + "FMOrderID =" + fmOrderID.ToString() + ", Shopify VendorOrderID = " + o.Name.ToString();
                mvc_Mail.SendDiagEmail("FATAL!!**ShopifyFMOrderTranslator Merge: OrderDetailList Missing", msg);
            }
        }

        if (fmOrderID == 0)
        {
            AdminCart.Order fmOrder = new AdminCart.Order();
            fmOrder.CustomerID = fmCustomerID;
            AdminCart.Address billAdr = AdminCart.Address.LoadAddress(fmCustomerID, 1);
            fmOrder.BillingAddressID = billAdr.AddressId;
            AdminCart.Address shipAdr = AdminCart.Address.LoadAddress(fmCustomerID, 2);
            fmOrder.ShippingAddressID = shipAdr.AddressId;

            fmOrder.SourceID = -3; //indicates order reported to warehouse via Shopify website
            fmOrder.SSOrderID = Convert.ToInt32(o.Name); //using old Purple Hippo field to track the Shopify order number
            fmOrder.CompanyID = o.CompanyID; //register who owns the order
            fmOrder.OrderDate = o.Created_at;

            fmOrder.Adjust = o.Total_discounts;
            fmOrder.Total = o.Total_price;
            fmOrder.TotalTax = o.Total_tax;
            if (o.ShippingLines != null)
            { if (o.ShippingLines.Length > 0) { fmOrder.TotalShipping = o.ShippingLines[0].Price; } }
            fmOrder.SubTotal = o.Total_line_items_price;

            fmOrderID = fmOrder.SaveDB();
            fmOrder.OrderID = fmOrderID;

            List<AdminCart.OrderDetail> odList = GetOrderDetailList(o.ShopifyOrderID, fmOrderID);

            foreach (AdminCart.OrderDetail od in odList)
            {
                od.OrderID = fmOrderID;
                od.SaveToOrderDetail();
            }

            List<AdminCart.Transaction> tList = new List<AdminCart.Transaction>(); //get ready to add transactions to the order

            if (o.CompanyID != AdminCart.Config.CompanyID()) //only orders owned AdminCart.Config.CompanyID() get transaction data
            {
                o.Gateway = "external_system"; //fake the system out to handle non-existent transactions              
            }
            else
            {
                tList = GetTransactionList(o.ShopifyOrderID); //assume gateway is Shopify
            }

            if ((tList.Count == 0)) //create a transaction if none exists (like for PayWhirl, etc.)
            {
                AdminCart.Customer c = new AdminCart.Customer(fmOrder.CustomerID);
                AdminCart.Transaction tran = new AdminCart.Transaction();
                string msg = "";
                switch (o.Gateway)
                {
                    case "shopify_payments": //something must have gone wrong downloading from Shopify
                        tran = new AdminCart.Transaction()
                        {
                            OrderID = fmOrderID,
                            TransactionDate = fmOrder.OrderDate,
                            TransactionAmount = fmOrder.Total,
                            TransactionID = "n/a:_shopify_error?_" + fmOrderID.ToString(),
                            TrxType = "S",
                            CardType = "n/a",
                            CardNo = "**** **** **** 0000",
                            ExpDate = "0000",
                            CardHolder = c.FirstName + " " + c.LastName,
                            ResultCode = 0,
                            ResultMsg = "Investigate (Shopify)",
                            CustomerCode = AdminCart.Config.AppCode() + fmOrderID.ToString()
                        };
                        tList.Add(tran);
                        msg = "Shopify OrderID: " + o.ShopifyOrderID.ToString() + Environment.NewLine;
                        msg += "Customer Name: " + o.Name + Environment.NewLine;
                        mvc_Mail.SendDiagEmail("FATAL!!**ShopifyFMOrderTranslator Merge: no transactions (ShopifyError)", msg);
                        break;

                    case "cash": //PayWhirl customer

                        List<StripeInvoice> stripeInvoiceList = new List<StripeInvoice>();

                        StripeServices stripeServices = new StripeServices();
                        StripeCustomer stripeCustomer = stripeServices.FindStripeCustomerByEmail(c.Email);
                        if (stripeCustomer.Id.Contains("cus_"))
                        {
                            //get last Invoice of Stripe customer
                            stripeInvoiceList = stripeServices.GetStripeCustomerInvoices(stripeCustomer.Id, out string erMsg);
                        }

                        if (stripeInvoiceList.Count() != 0)
                        {
                            stripeInvoiceList.OrderByDescending(x => x.Date);
                            StripeInvoice stripeInvoice = stripeInvoiceList.First(); //get the last invoice
                            decimal adjust = Convert.ToDecimal(stripeInvoice.Subtotal - stripeInvoice.Total) / 100; //Stripe use integer cents not dollars
                            fmOrder.Adjust = fmOrder.Adjust + adjust;
                            AdminCart.Order.UpDateAdjust(fmOrder.OrderID, adjust);
                            string promoCode = "n/a";
                            try
                            {
                                promoCode = (stripeInvoice.StripeDiscount.StripeCoupon.Id == null) ? "n/a" : stripeInvoice.StripeDiscount.StripeCoupon.Id;
                            }
                            catch 
                            {; } //do nothing on exception

                            if (adjust > 0) // add an OrderNote if a promoCode was used
                            {
                                OrderNote.Insertordernote(fmOrder.OrderID,
                                    "Shopify-FM order converter 1.0", "ShopifyOrderID: " + o.Name + "; Discount Code= " + promoCode + "; Discount Amount= " + adjust.ToString("C") + "; Discount Type= fixed_amount");
                            }
                            tList = Get24HRStripeTransactions(stripeCustomer.Id, fmOrder);
                        }
                        else
                        {
                            //create bogus transaction that gets flagged & alert via email
                            tran = new AdminCart.Transaction()
                            {
                                OrderID = fmOrderID,
                                TransactionDate = fmOrder.OrderDate,
                                TransactionAmount = fmOrder.Total,
                                TransactionID = "n/a:_paywhirl/stripe_error?_" + fmOrderID.ToString(),
                                TrxType = "S",
                                CardType = "n/a",
                                CardNo = "**** **** **** 0000",
                                ExpDate = "0000",
                                CardHolder = c.FirstName + " " + c.LastName,
                                ResultCode = 0,
                                ResultMsg = "Investigate (PayWhirl/Stripe)",
                                CustomerCode = AdminCart.Config.AppCode() + fmOrderID.ToString()
                            };
                            tList.Add(tran);
                            msg = "Shopify OrderID: " + o.ShopifyOrderID.ToString() + Environment.NewLine;
                            msg += "Customer Name: " + o.Name + Environment.NewLine;
                            mvc_Mail.SendDiagEmail("FATAL!!**ShopifyFMOrderTranslator Merge: no transactions (PayWhirl/Shopify)", msg);
                        }
                        break;

                    case "gift_card":
                        tran = new AdminCart.Transaction()
                        {
                            OrderID = fmOrderID,
                            TransactionDate = fmOrder.OrderDate,
                            TransactionAmount = fmOrder.Total,
                            TransactionID = "ch_gift_card_order_" + fmOrderID.ToString(),
                            TrxType = "S",
                            CardType = "gift card",
                            CardNo = "**** **** **** 0000",
                            ExpDate = "0000",
                            CardHolder = c.FirstName + " " + c.LastName,
                            ResultCode = 0,
                            ResultMsg = "Approved (Shopify)",
                            CustomerCode = AdminCart.Config.AppCode() + fmOrderID.ToString()
                        };
                        tList.Add(tran);
                        msg = "Shopify OrderID: " + o.ShopifyOrderID.ToString() + Environment.NewLine;
                        msg += "Customer Name: " + o.Name + Environment.NewLine;
                        mvc_Mail.SendDiagEmail("FATAL!!**ShopifyFMOrderTranslator Merge: no transactions (gift card)", msg);
                        break;

                    case "external_system":
                        //need to have some transaction info in order for reports to work, etc.
                        tran = new AdminCart.Transaction()
                        {
                            OrderID = fmOrderID,
                            TransactionDate = fmOrder.OrderDate,
                            TransactionAmount = fmOrder.Total,
                            TransactionID = "ch_ss.com_order?_" + fmOrderID.ToString(),
                            TrxType = "S",
                            CardType = "n/a",
                            CardNo = "**** **** **** 0000",
                            ExpDate = "0000",
                            CardHolder = c.FirstName + " " + c.LastName,
                            ResultCode = 0,
                            ResultMsg = "Approved (external: ss.com?)",
                            CustomerCode = AdminCart.Config.AppCode() + fmOrderID.ToString()
                        };
                        tList.Add(tran);
                        break;

                    default:                        
                        c = new AdminCart.Customer(fmOrder.CustomerID);
                        tran = new AdminCart.Transaction()
                        {
                            OrderID = fmOrderID,
                            TransactionDate = fmOrder.OrderDate,
                            TransactionAmount = fmOrder.Total,
                            TransactionID = "n/a:_shopify_error_(no gateway)",
                            TrxType = "S",
                            CardType = "n/a",
                            CardNo = "**** **** **** 0000",
                            ExpDate = "0000",
                            CardHolder = c.FirstName + " " + c.LastName,
                            ResultCode = 0,
                            ResultMsg = "Investigate (Shopify)",
                            CustomerCode = AdminCart.Config.AppCode() + fmOrderID.ToString()
                        };
                        tList.Add(tran);
                        msg += "FM OrderID: " + fmOrderID.ToString() + Environment.NewLine;
                        msg += "Shopify OrderID: " + o.ShopifyOrderID.ToString() + Environment.NewLine;
                        msg += "Customer Name: " + o.Name + Environment.NewLine;
                        msg += "Gateway Name: " + o.Gateway + Environment.NewLine;
                        mvc_Mail.SendDiagEmail("FATAL!!**ShopifyFMOrderTranslator Merge: no transactions", msg);
                        break;
                }
            }

            foreach (AdminCart.Transaction t in tList)
            {
                t.OrderID = fmOrderID;
                t.TransactionDate = fmOrder.OrderDate;
                t.SaveDB();
            }

            OrderNote.Insertordernote(fmOrderID, "Shopify-FM order converter 1.0", "Converted from Shopify order; ShopifyOrderID: " + o.Name + ", CompanyID: " + o.CompanyID.ToString());
            if (!string.IsNullOrEmpty(o.Note))
            {
                OrderNote.Insertordernote(fmOrderID, "Shopify-FM order converter 1.0", "Converted from Shopify order; ShopifyOrderID: " + o.Name + "; OrderNote: " + o.Note);
                FM2015.Models.ShopOrderNoteProvider shopOrderNoteProvider = new FM2015.Models.ShopOrderNoteProvider();
                shopOrderNoteProvider.ClearCache();
            }

            ShopifyDiscountCodes shopCode = new ShopifyDiscountCodes(o.ShopifyOrderID);
            if (!string.IsNullOrEmpty(shopCode.Code))
            {
                string noteMsg = "ShopifyOrderID: " + o.Name + "; ";
                noteMsg += "Discount Code= " + shopCode.Code + "; ";
                noteMsg += "Discount Amount= " + shopCode.Amount + "; ";
                noteMsg += "Discount Type= " + shopCode.Type;
                OrderNote.Insertordernote(fmOrderID, "Shopify-FM order converter 1.0", noteMsg);
            }

            List<ShopifyAgent.ShopOrderRisk> riskList = ShopifyAgent.ShopOrderRisk.GetOrderRiskList(o.Id, o.CompanyID);
            bool sendRiskEmail = false;
            if (riskList.Count() > 0)
            {
                foreach(ShopifyAgent.ShopOrderRisk risk in riskList)
                {
                    if ((risk.Recommendation.ToUpper() == "CANCEL") || (risk.Recommendation.ToUpper() == "INVESTIGATE"))
                    {
                        risk.ShopifyName = Convert.ToInt32(o.Name);
                        if (risk.Cause_cancel == null) { risk.Cause_cancel = ""; }
                        FM2015.Models.RiskProvider riskProvider = new FM2015.Models.RiskProvider();
                        riskProvider.Save(risk);
                        sendRiskEmail = true;
                        break;
                    }
                }
            }
            if (sendRiskEmail)
            {
                string body = "OrderID: " + fmOrderID.ToString() + Environment.NewLine;
                body += "Risk Assessment: " + Environment.NewLine;
                foreach(ShopifyAgent.ShopOrderRisk risk in riskList)
                {
                    body += "score = " + risk.Score.ToString() + Environment.NewLine;
                    body += "recommendation = " + risk.Recommendation + Environment.NewLine;
                    body += "message = " + risk.Message + Environment.NewLine;
                    body += Environment.NewLine;
                }
                mvc_Mail.SendDiagEmail("ShopifyFMOrderTranslator: Order Risk!! OrderID: " + fmOrderID.ToString(), body);
            }
        }
        return fmOrderID;
    }

    static public List<AdminCart.OrderDetail> GetOrderDetailList(int shopifyOrderId, int fmOrderId)
    {
        List<AdminCart.OrderDetail> odList = new List<AdminCart.OrderDetail>();

        //build OrderDetails
        string sql = @" 
        SELECT * 
        FROM ShopifyLineItems  
        WHERE ShopifyOrderId = @shopifyOrderId 
        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, shopifyOrderId);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, AdminCart.Config.ConnStrShopify());

        while (dr.Read())
        {
            AdminCart.OrderDetail od = new AdminCart.OrderDetail();
            //string sku = dr["sku"].ToString();
            long shopifyProductID = 0;
            bool ok = long.TryParse(dr["product_id"].ToString(), out shopifyProductID);
            if (shopifyProductID == 0)
            {
                string body = "Shopify OrderID: " + shopifyOrderId.ToString() + Environment.NewLine;
                body += "Shopify ProductID: " + shopifyProductID.ToString() + Environment.NewLine;
                mvc_Mail.SendDiagEmail("Error in ShopifyFMOrderTranslator: GetOrderDetailsList: ShopifyProductID == 0", body);
            }
            else
            {
                if (AdminCart.Config.appName.ToUpper() == "CRYSTALIFT") // check for a Crystalift SKU Transform
                {
                    decimal shopifyProductPrice = 0;
                    //decimal.TryParse(dr["price"].ToString(), out shopifyProductPrice);
                    AdminCart.Cart cart = new AdminCart.Cart();
                    cart = cart.GetCartFromOrder(fmOrderId, false);
                    List<StripeCharge> stripeChargeList = Utilities.FMHelpers.stripeChargeList(cart.SiteCustomer.Email, "Shopify Product Transform: Get StripeCharge");
                    if (stripeChargeList.Count() != 0) // must be a subscription item if found at Stripe
                    {
                        StripeCharge stripeCharge = stripeChargeList.First();
                        decimal.TryParse(stripeCharge.Amount.ToString(), out shopifyProductPrice);
                        shopifyProductPrice = shopifyProductPrice / 100;
                        cart.SiteOrder.SubTotal = shopifyProductPrice;
                        cart.SiteOrder.Total = cart.SiteOrder.SubTotal - cart.SiteOrder.Adjust + cart.SiteOrder.TotalShipping;
                        cart.UpdateOrder();

                        //perform Crystalift transform logic here: update shopifyProductID with transformed ID
                        long temp = TransformShopSKU.TransformShopProduct(shopifyProductID, shopifyProductPrice); //update with a Transformation method
                        if (temp != 0)
                        {
                            shopifyProductID = temp;
                            long shopifyOrderID = ShopifyAgent.ShopifyOrder.GetShopifyIDFromName(cart.SiteOrder.SSOrderID.ToString(), cart.SiteOrder.CompanyID);
                            ShopifyAgent.ShopifyOrder shopOrder = new ShopifyAgent.ShopifyOrder(shopifyOrderID, AdminCart.Config.CompanyID());
                            ShopifyLineItem sItem = new ShopifyLineItem(shopOrder.ShopifyOrderID);
                            long origID = 0;
                            Int64.TryParse(sItem.Product_id.ToString(), out origID);
                            AdminCart.Product origP = new AdminCart.Product(origID);
                            AdminCart.Product newP = new AdminCart.Product(shopifyProductID);
                            OrderNote note = new OrderNote();
                            note.OrderID = fmOrderId;
                            note.OrderNoteBy = "ShopifyFMOrderTranslator";
                            note.OrderNoteDate = DateTime.Now;
                            note.OrderNoteText = "Shopify Order SKU transformation" + Environment.NewLine;
                            note.OrderNoteText += "OrderID = " + note.OrderID.ToString() + Environment.NewLine;
                            note.OrderNoteText += "Orig SKU = " + origP.Sku + Environment.NewLine;
                            note.OrderNoteText += "Orig Name = " + origP.Name + Environment.NewLine;
                            note.OrderNoteText += "New SKU = " + newP.Sku + Environment.NewLine;
                            note.OrderNoteText += "New Name = " + newP.Name + Environment.NewLine;
                            note.Save(0); //insert new note

                            //mvc_Mail.SendDiagEmail("ShopifyFMOrderTranslator", note.OrderNoteText);
                        }
                    }
                }
            }

            AdminCart.Product p = new AdminCart.Product(shopifyProductID); //potentially translated product
            if (p.ProductID == 0)
            {
                string body = "Shopify OrderID: " + shopifyOrderId.ToString() + Environment.NewLine;
                body += "Shopify ProductID: " + shopifyProductID.ToString() + Environment.NewLine;
                mvc_Mail.SendDiagEmail("Error in ShopifyFMOrderTranslator: GetOrderDetailsList: FM ProductID == 0", body);

            }

            od.OrderDetailID = 0; //not assigned yet
            od.OrderID = fmOrderId; 
            od.LineItemProduct = p;
            od.ProductID = p.ProductID;

            int tmpQTY = 0;
            if (Int32.TryParse(dr["Quantity"].ToString(), out tmpQTY)) { od.Quantity = tmpQTY; }

            od.UnitPrice = p.Price;
            od.LineItemTotal = od.Quantity * od.UnitPrice;
            od.Discount = 0;

            AdminCart.ProductCost pCost = new AdminCart.ProductCost(od.ProductID);
            od.UnitCost = pCost.Cost;

            odList.Add(od);
        }
        if (dr != null) { dr.Close(); }

        return odList;
    }

    static public List<AdminCart.Transaction> GetTransactionList(int shopifyOrderId)
    {
        List<AdminCart.Transaction> tList = new List<AdminCart.Transaction>();

        //build OrderDetails
        string sql = @" 
        SELECT * 
        FROM ShopifyTransactions  
        WHERE ShopifyOrderId = @shopifyOrderId 
        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, shopifyOrderId);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, AdminCart.Config.ConnStrShopify());

        while (dr.Read())
        {
            AdminCart.Transaction t = new AdminCart.Transaction();

            t.TransactionRowID = 0; //not assigned yet
            t.OrderID = 0; //not assigned yet
            t.TransactionDate = DateTime.MinValue;//not assigned yet

            string gateway = (dr["Gateway"] == DBNull.Value) ? "unknown gateway" : dr["gateway"].ToString();

            t.TransactionID = dr["Authorization_key"].ToString();
            if (t.TransactionID.Length < 8)
            {
                ShopifyAgent.ShopifyOrder o = new ShopifyAgent.ShopifyOrder(shopifyOrderId);
                t.TransactionID = "ch_" + gateway + ": Order_" + o.Name.ToString();
            }

            string trxType = dr["Kind"].ToString();
            t.TrxType = "S"; //assume a sale or authorization
            if (trxType == "refund") { t.TrxType = "C"; }
            if (trxType == "void") { t.TrxType = "V"; }

            t.CardType = dr["Credit_card_company"].ToString();
            t.CardNo = t.CardType + ": " + dr["Credit_card_number"].ToString();
            t.ExpDate = "0000"; //value not kept by ShopifyTrnasaction
            t.TransactionAmount = Convert.ToDecimal(dr["amount"].ToString());
            t.ResultCode = (dr["status"].ToString() == "success") ? 0 : -1; //0 = success
            if (t.ResultCode == 0)
            {
                t.ResultMsg = "Approved (" + gateway + "): ";
                if (t.CardNo.Contains("n/a"))
                {
                    t.CardNo = "PayPal: card n/a";
                    t.ResultMsg += "private PayPal acct?";
                }
                t.ResultMsg += (dr["error_code"] == DBNull.Value) ? "no message" : ": " + dr["message"].ToString();
            }
            else
            {
                t.ResultMsg = "Failed (" + gateway + "): ";
                t.ResultMsg += (dr["error_code"] == DBNull.Value) ? "no error message" : dr["error_code"].ToString() + ": " + dr["message"].ToString();
            }
          
            tList.Add(t);
        }
        if (dr != null) { dr.Close(); }

        return tList;
    }

    static public void UpdateOrderActions(ShopifyAgent.ShopifyOrder o, int fmOrderID)
    {
        AdminCart.OrderAction oa = new AdminCart.OrderAction();
        oa.OrderID = fmOrderID; //will be assigned when inserted into DB
        oa.OrderActionBy = "Shopify-FM order translator";
        oa.OrderActionDate = o.ShopifyImportDate;
        oa.OrderActionReason = "Merge Shopify order into FM Database";
        oa.OrderActionType = 2; //completed order, ready to be fullfilled
        oa.SSOrderID = Convert.ToInt32(o.Name); //remember which Shopify order this came from
        int result = oa.SaveDB();
    }

    static public int FindFMOrder(ShopifyAgent.ShopifyOrder o)
    {
        int fmOrderID = 0;
        string sql = @"
        SELECT OrderID FROM Orders  
        WHERE SuzanneSomersOrderID = @SuzanneSomersOrderID AND SourceID = -3 AND CompanyID = @CompanyID 
        ";

        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, o.Name, o.CompanyID);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

        if (dr.Read())
        {
            fmOrderID = Convert.ToInt32(dr["OrderID"]);
        }
        if (dr != null) { dr.Close(); }

        return fmOrderID;
    }

    public void RunShopifyAgentTest()
    {
        Process serverSideProcess = new Process();
        //serverSideProcess.StartInfo.FileName = @"C:\software\facemaster\shopifytest\shopifytest.exe";
        string consoleAppPath = ConfigurationManager.AppSettings["ConsoleAppPath"].ToString();
        serverSideProcess.StartInfo.FileName = consoleAppPath + "shopifytest.exe";
        serverSideProcess.StartInfo.Arguments = "";
        serverSideProcess.EnableRaisingEvents = true;
        try
        {
            ApplicationException ex = new ApplicationException(); //this is just for debugging... getting something listed in the database for later viewing
            throw ex;
        }
        catch (Exception ex)
        {
            //something happened trying to run ShopifyAgent to download orders
            string msg = "about to run RunShopifyAgentTest </br>";
            msg += "FileName: " + serverSideProcess.StartInfo.FileName + "</br>";
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
        }

        try
        {
            //serverSideProcess.StartInfo.UseShellExecute = true;
        }
        catch (Exception ex)
        {
            //something happened trying to run ShopifyAgent to download orders
            string msg = "error in RunShopifyAgentTest after trying to startup process </br>";
            msg += "FileName: " + serverSideProcess.StartInfo.FileName + "</br>";
            FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
        }
    }

    public static List<AdminCart.Transaction> Get24HRStripeTransactions(string stripeCustomerID, AdminCart.Order fmOrder)
    {
        AdminCart.Customer c = new AdminCart.Customer(fmOrder.CustomerID);
        AdminCart.Address a = AdminCart.Address.LoadAddress(fmOrder.CustomerID, 1);
        List<AdminCart.Transaction> tList = new List<AdminCart.Transaction>();

        if (!string.IsNullOrEmpty(stripeCustomerID))
        {
            StripeChargeService chargeService = new StripeChargeService();
            StripeChargeListOptions chargeOptions = new StripeChargeListOptions();
            chargeOptions.CustomerId = stripeCustomerID;
            chargeOptions.Created = new StripeDateFilter();
            chargeOptions.Created.GreaterThan = fmOrder.OrderDate.AddDays(-1);
            List<StripeCharge> stripeChargeList = new List<StripeCharge>();
            try
            {
                stripeChargeList = chargeService.List(chargeOptions).ToList(); // optional StripeChargeListOptions
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "ShopifyFMOrderTranslator: Get 24HRTransactions: Get StripeChargeList:");
            }

            foreach (StripeCharge charge in stripeChargeList)
            {
                AdminCart.Transaction tran = new AdminCart.Transaction();
                tran.TransactionID = charge.Id;
                tran.AuthCode = (tran.TransactionID.Length > 8) ? tran.TransactionID.Substring(0, 8) : "ch_" + fmOrder.OrderID.ToString();
                tran.OrigTranID = "0";
                tran.TransactionAmount = Decimal.Divide(charge.Amount, 100); //Stripe does stuff in cents
                tran.TrxType = "S";
                tran.OrderID = fmOrder.OrderID;
                tran.TransactionDate = charge.Created;
                tran.CardType = charge.Source.Card.Brand;  //**********
                tran.CardHolder = charge.Source.Card.Name;
                tran.CardNo = "**** **** **** " + charge.Source.Card.Last4;
                tran.ExpDate = charge.Source.Card.ExpirationMonth.ToString() + charge.Source.Card.ExpirationYear.ToString().Substring(2, 2);
                tran.ResultCode = (charge.FailureCode == null) ? 0 : -1;
                tran.ResultMsg = (charge.FailureCode == null) ? "Approved (Stripe)" : (charge.FailureCode + ": " + charge.FailureMessage);
                tran.AuthCode = "0";
                tran.Address.Street = (string.IsNullOrEmpty(charge.Source.Card.AddressLine1)) ? a.Street : charge.Source.Card.AddressLine1;
                tran.Address.City = (string.IsNullOrEmpty(charge.Source.Card.AddressCity)) ? a.City : charge.Source.Card.AddressCity;
                tran.Address.State = (string.IsNullOrEmpty(charge.Source.Card.AddressState)) ? a.State : charge.Source.Card.AddressState;
                tran.Address.Country = (string.IsNullOrEmpty(charge.Source.Card.AddressCountry)) ? a.Country : charge.Source.Card.AddressCountry;
                tran.Address.Zip = (string.IsNullOrEmpty(charge.Source.Card.AddressZip)) ? a.Zip : charge.Source.Card.AddressZip;
                tran.Comment2 = "FM" + tran.OrderID;

                tList.Add(tran);
            }
        }
        return tList;
    }
}
