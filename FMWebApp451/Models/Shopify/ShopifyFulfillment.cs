﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FMWebApp.Helpers;

namespace ShopifyAgent
{
    public class ShopifyFulfillments
    {
        [JsonProperty("fulfillments")]
        public ShopifyFulfillment[] fulfillments;
    }


    public class ShopifyFulfillmentSingle
    {
        [JsonProperty("fulfillment")]
        public ShopifyFulfillment fulfillment { get; set; }

        public ShopifyFulfillmentSingle()
        {
            fulfillment = new ShopifyFulfillment();
        }

    }

    public class Receipt
    {
        public string testcase { get; set; }
        public string authorization { get; set; }
    }

    public class ShopifyFulfillment
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("order_id")]
        public long Order_Id { get; set; }

        [JsonProperty("created_at")]
        public DateTime? Created_at { get; set; }

        [JsonProperty("updated_at")]
        public DateTime? Updated_at { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("tracking_company")]
        public string Tracking_company { get; set; }

        [JsonProperty("tracking_numbers")]
        public string[] Tracking_numbers { get; set; }

        [JsonProperty("tracking_urls")]
        public string[] Tracking_urls { get; set; }

        [JsonProperty("variant_inventory_management")]
        public string Variant_inventory_management { get; set; }


        [JsonProperty("receipt")]
        public Receipt receipt;

        [JsonProperty("line_items")]
        public ShopifyLineItem[] LineItems;

        public static void SendShopifyTracking(int SSOrderID, string tracking, string trackingURL, int companyID)
        {
            long id = ShopifyOrder.GetShopifyIDFromName(SSOrderID.ToString(), companyID);

            if (id == 0)
            {
                string msg = "error in SendShopifyTracking: Could not find Shopify Order (id = 0) </br>";
                msg += "SSOrderID: " + SSOrderID.ToString() + "</br>";
                mvc_Mail.SendMail(AdminCart.Config.MailFrom(), AdminCart.Config.MailTo, "", "", "ShopifyCustomer Update ErrorSendShopifyTracking", msg, true);
            }

            string jsonURL = "admin/orders/#id#/fulfillments.json";
            jsonURL = jsonURL.Replace("#id#", id.ToString());

            string jsonPacket = @"{""fulfillment"": {""tracking_number"": ""#tracking#"",""notify_customer"": true, ""status"":""success"", ""service"":""manual""}}";
            jsonPacket = jsonPacket.Replace("#tracking#", tracking);

            string apiResult = JsonHelpers.POSTtoShopify(jsonURL, jsonPacket);

            try
            {
                ShopifyFulfillment responseFulfillment = JsonConvert.DeserializeObject<ShopifyFulfillmentSingle>(apiResult).fulfillment;
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                string msg = "error in SendShopifyTracking </br>";
                msg += "jsonURL: " + jsonURL + "</br>";
                msg += "jsonTemplate: " + jsonPacket + "</br>";
                msg += "response: " + apiResult + "</br>";
                msg += "SSOrderID: " + SSOrderID.ToString() + "</br>";
                msg += "Shopify ID: " + id.ToString() + "</br>";
                msg += "Tracking#: " + tracking + "</br>";
                msg += "Tracking URL: " + trackingURL + "</br>";
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
        }

    }
}