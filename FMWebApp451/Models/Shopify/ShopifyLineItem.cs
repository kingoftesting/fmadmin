﻿using System;
using System.Data;

using System.Data.SqlClient;
using System.Collections.Generic;

using Newtonsoft.Json;
using FM2015.Helpers;
using ShopifyAgent;
using System.Configuration;


/// <summary>
/// Summary description for ShopifyLineItem
/// </summary>
public class ShopifyLineItem
{
    public class ShopifyProperty
    {
        public string name { get; set; }
        public string value { get; set; }

        public ShopifyProperty()
        {
            name = "";
            value = "";
        }
    }
    //Built with ClassHatch Version Build: 4.0.7  9/8/2013.  Template file:ExtendedClass.cs.tem

    #region Fields
    private int shopifyLineItemID;
    private int shopifyOrderID;
    private string fulfillment_service;
    private string fulfillment_status;
    private bool gift_card;
    private Decimal grams;
    private Int64 id;
    private Decimal price;
    private Int64? product_id;
    private int quantity;
    private string requires_shipping;
    private string sku;
    private bool taxable;
    private string title;
    private Int64? variant_id;
    private string variant_title;
    private string vendor;
    private string name;
    private string variant_inventory_management;
    private string product_exists;
    private int fulfillable_quantity;
    private decimal total_discount;
    #endregion


    #region Properties
    //[JsonProperty("shopifyLineItemID")]
    //[JsonProperty("shopifyOrderID")]

    public int ShopifyLineItemID { get { return shopifyLineItemID; } set { shopifyLineItemID = value; } }
    public int ShopifyOrderID { get { return shopifyOrderID; } set { shopifyOrderID = value; } }

    [JsonProperty("fulfillment_service")]
    public string Fulfillment_service { get { return fulfillment_service; } set { fulfillment_service = value; } }

    [JsonProperty("fulfillment_status")]
    public string Fulfillment_status { get { return fulfillment_status; } set { fulfillment_status = value; } }

    [JsonProperty("gift_card")]
    public bool Gift_Card { get { return gift_card; } set { gift_card = value; } }

    [JsonProperty("grams")]
    public Decimal Grams { get { return grams; } set { grams = value; } }

    [JsonProperty("id")]
    public Int64 Id { get { return id; } set { id = value; } }

    [JsonProperty("price")]
    public Decimal Price { get { return price; } set { price = value; } }

    [JsonProperty("product_id")]
    public Int64? Product_id { get { return product_id; } set { product_id = value; } }

    [JsonProperty("quantity")]
    public int Quantity { get { return quantity; } set { quantity = value; } }

    [JsonProperty("requires_shipping")]
    public string Requires_shipping { get { return requires_shipping; } set { requires_shipping = value; } }

    [JsonProperty("sku")]
    public string Sku { get { return sku; } set { sku = value; } }

    [JsonProperty("taxable")]
    public bool Taxable { get { return taxable; } set { taxable = value; } }

    [JsonProperty("title")]
    public string Title { get { return title; } set { title = value; } }

    [JsonProperty("variant_id")]
    public Int64? Variant_id { get { return variant_id; } set { variant_id = value; } }

    [JsonProperty("variant_title")]
    public string Variant_title { get { return variant_title; } set { variant_title = value; } }

    [JsonProperty("vendor")]
    public string Vendor { get { return vendor; } set { vendor = value; } }

    [JsonProperty("name")]
    public string Name { get { return name; } set { name = value; } }

    [JsonProperty("variant_inventory_management")]
    public string Variant_inventory_management { get { return variant_inventory_management; } set { variant_inventory_management = value; } }

    [JsonProperty("product_exists")]
    public string Product_exists { get { return product_exists; } set { product_exists = value; } }

    [JsonProperty("fulfillable_quantity")]
    public int Fulfillable_quantity { get { return fulfillable_quantity; } set { fulfillable_quantity = value; } }

    [JsonProperty("total_discount")]
    public decimal Total_discount { get { return total_discount; } set { total_discount = value; } }

    [JsonProperty("properties")]
    public ShopifyProperty[] Properties;

    [JsonProperty("tax_lines")]
    public TaxLine[] Tax_lines;

    #endregion


    #region Constructors
    public ShopifyLineItem()
    {
        Properties = new ShopifyProperty[] { };
        Tax_lines = new TaxLine[] { };

        //sets reasonable initial values
        shopifyLineItemID = 0;
        shopifyOrderID = 0;
        fulfillment_service = "";
        fulfillment_status = "";
        gift_card = false;
        grams = 0;
        id = 0;
        price = 0;
        product_id = 0;
        quantity = 0;
        requires_shipping = "";
        sku = "";
        taxable = true;
        title = "";
        variant_id = 0;
        variant_title = "";
        vendor = "";
        name = "";
        variant_inventory_management = "";
        product_exists = "";
        fulfillable_quantity = 1;
        total_discount = 0;
    }

    public ShopifyLineItem(int shopifyOrderID)
    {
        //Loads a single record from a table
        string sql = @"
        SELECT *
        FROM ShopifyLineItems
        WHERE ShopifyOrderID = @ShopifyOrderID";
        SqlParameter[] parms = new SqlParameter[1];
        parms[0] = new SqlParameter("@ShopifyOrderID", shopifyOrderID);

        SqlDataReader dr = DBUtil.FillDataReader(sql, parms, AdminCart.Config.ConnStrShopify());

        while (dr.Read())
        {
            this.shopifyLineItemID = Convert.ToInt32(dr["shopifyLineItemID"].ToString());
            this.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            this.fulfillment_service = Convert.ToString(dr["fulfillment_service"].ToString());
            this.fulfillment_status = Convert.ToString(dr["fulfillment_status"].ToString());
            this.gift_card = Convert.ToBoolean(dr["gift_card"].ToString());
            this.grams = Convert.ToDecimal(dr["grams"].ToString());
            this.id = Convert.ToInt64(dr["id"].ToString());
            this.price = Convert.ToDecimal(dr["price"].ToString());
            this.product_id = Convert.ToInt64(dr["product_id"].ToString());
            this.quantity = Convert.ToInt32(dr["quantity"].ToString());
            this.requires_shipping = Convert.ToString(dr["requires_shipping"].ToString());
            this.sku = Convert.ToString(dr["sku"].ToString());
            this.title = Convert.ToString(dr["title"].ToString());
            this.variant_id = Convert.ToInt64(dr["variant_id"].ToString());
            this.variant_title = Convert.ToString(dr["variant_title"].ToString());
            this.vendor = Convert.ToString(dr["vendor"].ToString());
            this.name = Convert.ToString(dr["name"].ToString());
            this.variant_inventory_management = Convert.ToString(dr["variant_inventory_management"].ToString());
            this.product_exists = Convert.ToString(dr["product_exists"].ToString());
        }
        dr.Close();
    }

    #endregion


    #region Methods
    //requires DBUtil class from classhatch.com
    public int SaveDB()
    {
        //Insert or Update record on SQL Table
        SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrShopify());

        string sql = "";


        if (ShopifyLineItemID == 0)
        {
            //Insert if primary key is default value of zero
            sql = @"
            INSERT ShopifyLineItems(
--ShopifyLineItemID,
ShopifyOrderID,
fulfillment_service,
fulfillment_status,
gift_card,
grams,
id,
price,
product_id,
quantity,
requires_shipping,
sku,
title,
variant_id,
variant_title,
vendor,
name,
variant_inventory_management,
product_exists

             )VALUES(
--@ShopifyLineItemID,
@ShopifyOrderID,
@fulfillment_service,
@fulfillment_status,
@gift_card,
@grams,
@id,
@price,
@product_id,
@quantity,
@requires_shipping,
@sku,
@title,
@variant_id,
@variant_title,
@vendor,
@name,
@variant_inventory_management,
@product_exists

            ); select id=scope_identity();";
        }
        else
        {
            //Update db record if primary key is not default value of zero
            sql = @"
            UPDATE ShopifyLineItems
            SET 
--ShopifyLineItemID= @ShopifyLineItemID,
ShopifyOrderID= @ShopifyOrderID,
fulfillment_service= @fulfillment_service,
fulfillment_status= @fulfillment_status,
gift_card= @gift_card,
grams= @grams,
id= @id,
price= @price,
product_id= @product_id,
quantity= @quantity,
requires_shipping= @requires_shipping,
sku= @sku,
title= @title,
variant_id= @variant_id,
variant_title= @variant_title,
vendor= @vendor,
name= @name,
variant_inventory_management= @variant_inventory_management,
product_exists= @product_exists

            WHERE ShopifyLineItemID = PrimaryKeyFieldValue; select id=@ShopifyLineItemID;";
        }


        SqlCommand cmd = new SqlCommand(sql, conn);

        SqlParameter shopifyLineItemIDParam = new SqlParameter("@shopifyLineItemID", SqlDbType.Int);
        SqlParameter shopifyOrderIDParam = new SqlParameter("@shopifyOrderID", SqlDbType.Int);
        SqlParameter fulfillment_serviceParam = new SqlParameter("@fulfillment_service", SqlDbType.VarChar);
        SqlParameter fulfillment_statusParam = new SqlParameter("@fulfillment_status", SqlDbType.VarChar);
        SqlParameter gift_cardParam = new SqlParameter("@gift_card", SqlDbType.Bit);
        SqlParameter gramsParam = new SqlParameter("@grams", SqlDbType.Decimal);
        SqlParameter idParam = new SqlParameter("@id", SqlDbType.BigInt);
        SqlParameter priceParam = new SqlParameter("@price", SqlDbType.Money);
        SqlParameter product_idParam = new SqlParameter("@product_id", SqlDbType.BigInt);
        SqlParameter quantityParam = new SqlParameter("@quantity", SqlDbType.Int);
        SqlParameter requires_shippingParam = new SqlParameter("@requires_shipping", SqlDbType.VarChar);
        SqlParameter skuParam = new SqlParameter("@sku", SqlDbType.VarChar);
        SqlParameter titleParam = new SqlParameter("@title", SqlDbType.VarChar);
        SqlParameter variant_idParam = new SqlParameter("@variant_id", SqlDbType.BigInt);
        SqlParameter variant_titleParam = new SqlParameter("@variant_title", SqlDbType.VarChar);
        SqlParameter vendorParam = new SqlParameter("@vendor", SqlDbType.VarChar);
        SqlParameter nameParam = new SqlParameter("@name", SqlDbType.VarChar);
        SqlParameter variant_inventory_managementParam = new SqlParameter("@variant_inventory_management", SqlDbType.VarChar);
        SqlParameter product_existsParam = new SqlParameter("@product_exists", SqlDbType.VarChar);

        shopifyLineItemIDParam.Value = shopifyLineItemID;
        shopifyOrderIDParam.Value = shopifyOrderID;
        fulfillment_serviceParam.Value = fulfillment_service;
        fulfillment_statusParam.Value = (object)fulfillment_status ?? DBNull.Value;
        gift_cardParam.Value = gift_card;
        gramsParam.Value = grams;
        idParam.Value = id;
        priceParam.Value = price;
        product_idParam.Value = (object)product_id ?? DBNull.Value;
        quantityParam.Value = quantity;
        requires_shippingParam.Value = requires_shipping;
        skuParam.Value = sku;
        titleParam.Value = title;
        variant_idParam.Value = (object)variant_id ?? DBNull.Value;
        variant_titleParam.Value = (object)variant_title ?? DBNull.Value;
        vendorParam.Value = (object)vendor ?? DBNull.Value;
        nameParam.Value = name;
        variant_inventory_managementParam.Value = (object)variant_inventory_management ?? DBNull.Value;
        product_existsParam.Value = product_exists;

        cmd.Parameters.Add(shopifyLineItemIDParam);
        cmd.Parameters.Add(shopifyOrderIDParam);
        cmd.Parameters.Add(fulfillment_serviceParam);
        cmd.Parameters.Add(fulfillment_statusParam);
        cmd.Parameters.Add(gift_cardParam);
        cmd.Parameters.Add(gramsParam);
        cmd.Parameters.Add(idParam);
        cmd.Parameters.Add(priceParam);
        cmd.Parameters.Add(product_idParam);
        cmd.Parameters.Add(quantityParam);
        cmd.Parameters.Add(requires_shippingParam);
        cmd.Parameters.Add(skuParam);
        cmd.Parameters.Add(titleParam);
        cmd.Parameters.Add(variant_idParam);
        cmd.Parameters.Add(variant_titleParam);
        cmd.Parameters.Add(vendorParam);
        cmd.Parameters.Add(nameParam);
        cmd.Parameters.Add(variant_inventory_managementParam);
        cmd.Parameters.Add(product_existsParam);

        conn.Open();
        int result = Convert.ToInt32(cmd.ExecuteScalar().ToString());
        conn.Close();
        return result;
    }

    static public List<ShopifyLineItem> GetShopifyLineItemList()
    {
        string conn = AdminCart.Config.ConnStrShopify();
        List<ShopifyLineItem> thelist = new List<ShopifyLineItem>();

        string sql = "SELECT * FROM ShopifyLineItems";

        SqlDataReader dr = DBUtil.FillDataReader(sql, conn);

        while (dr.Read())
        {
            ShopifyLineItem obj = new ShopifyLineItem();

            obj.shopifyLineItemID = Convert.ToInt32(dr["shopifyLineItemID"].ToString());
            obj.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            obj.fulfillment_service = Convert.ToString(dr["fulfillment_service"].ToString());
            obj.fulfillment_status = Convert.ToString(dr["fulfillment_status"].ToString());
            obj.gift_card = Convert.ToBoolean(dr["gift_card"].ToString());
            obj.grams = Convert.ToDecimal(dr["grams"].ToString());
            obj.id = Convert.ToInt64(dr["id"].ToString());
            obj.price = Convert.ToDecimal(dr["price"].ToString());
            obj.product_id = Convert.ToInt64(dr["product_id"].ToString());
            obj.quantity = Convert.ToInt32(dr["quantity"].ToString());
            obj.requires_shipping = Convert.ToString(dr["requires_shipping"].ToString());
            obj.sku = Convert.ToString(dr["sku"].ToString());
            obj.title = Convert.ToString(dr["title"].ToString());
            obj.variant_id = Convert.ToInt64(dr["variant_id"].ToString());
            obj.variant_title = Convert.ToString(dr["variant_title"].ToString());
            obj.vendor = Convert.ToString(dr["vendor"].ToString());
            obj.name = Convert.ToString(dr["name"].ToString());
            obj.variant_inventory_management = Convert.ToString(dr["variant_inventory_management"].ToString());
            obj.product_exists = Convert.ToString(dr["product_exists"].ToString());

            thelist.Add(obj);
        }
        dr.Close();
        return thelist;

    }


    #endregion


    #region Validation
    //Requires Validation class from classhatch.com
    static public bool IsValid(ShopifyLineItem obj)
    {
        bool isValid = false;

        if (GetErrors(obj).Count == 0)
        {
            isValid = true;
        }

        return isValid;
    }


    static public List<ValidationError> GetErrors(ShopifyLineItem obj)
    {
        List<ValidationError> errorlist = new List<ValidationError>();

        /*
        if (ValidateUtil.IsTooLong(20, obj.FirstName))
        {
            ValidationError e = new ValidationError();
            e.Field = "First Name";
            e.ErrorDescription = "20 characters maximium are allowed";
            errorlist.Add(e);
        };
        */

        return errorlist;
    }
    #endregion


    #region OverRides

    #endregion

}

