﻿using System;
using System.Data;

using System.Data.SqlClient;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;
using FM2015.Helpers;
using FMWebApp.Helpers;

namespace ShopifyAgent
{

    public class ShopifyOrderSingle
    {
        //Built with ClassHatch Version Build: 4.0.7  9/8/2013.  Template file:ExtendedClass.cs.tem

        [JsonProperty("order")]
        public ShopifyOrder order { get; set; }

        public ShopifyOrderSingle()
        {
            order = new ShopifyOrder();
        }
    }

    public class ShopifyNoteAttribute
    {
        //Built with ClassHatch Version Build: 4.0.7  9/8/2013.  Template file:ExtendedClass.cs.tem

        [JsonProperty("note_attributes")]
        public string name { get; set; }
        public string value { get; set; }

        public ShopifyNoteAttribute()
        {
            name = "";
            value = "";
        }
    }

    /// <summary>
    /// Summary description for ShopifyOrder
    /// </summary>
    public class ShopifyOrder
    {
        //Built with ClassHatch Version Build: 4.0.7  9/8/2013.  Template file:ExtendedClass.cs.tem

        #region Fields
        private int shopifyOrderID;
        private DateTime shopifyImportDate;
        private bool isFMTranslated;
        private int companyID;
        private bool buyer_accepts_marketing;
        private string cancel_reason;
        private DateTime? cancelled_at;
        private string cart_token;
        private string checkout_token;
        private DateTime? closed_at;
        private bool confirmed;
        private DateTime created_at;
        private string currency;
        private string email;
        private string financial_status;
        private string fulfillment_status;
        private string gateway;
        private Int64 id;
        private string landing_site;
        private string location_id;
        private string name;
        private string note;
        private int number;
        private string reference;
        private string referring_site;
        private string source;
        private Decimal subtotal_price;
        private bool taxes_included;
        private bool test;
        private string token;
        private Decimal total_discounts;
        private Decimal total_line_items_price;
        private Decimal total_price;
        private Decimal? total_price_usd;
        private Decimal total_tax;
        private Decimal? total_weight;
        private DateTime updated_at;
        private int? user_id;
        private string browser_ip;
        private string landing_site_ref;
        private int order_number;
        private string discount_codes;
        private string note_attributes;
        private string processing_method;
        private string checkout_id;
        #endregion


        #region Properties

        public int ShopifyOrderID { get { return shopifyOrderID; } set { shopifyOrderID = value; } }
        public DateTime ShopifyImportDate { get { return shopifyImportDate; } set { shopifyImportDate = value; } }
        public bool IsFMTranslated { get { return isFMTranslated; } set { isFMTranslated = value; } }
        public int CompanyID { get { return companyID; } set { companyID = value; } }


        [JsonProperty("buyer_accepts_marketing")]
        public bool Buyer_accepts_marketing { get { return buyer_accepts_marketing; } set { buyer_accepts_marketing = value; } }

        [JsonProperty("cancel_reason")]
        public string Cancel_reason { get { return cancel_reason; } set { cancel_reason = value; } }

        [JsonProperty("cancelled_at")]
        public DateTime? Cancelled_at { get { return cancelled_at; } set { cancelled_at = value; } }

        [JsonProperty("cart_token")]
        public string Cart_token { get { return cart_token; } set { cart_token = value; } }

        [JsonProperty("checkout_token")]
        public string Checkout_token { get { return checkout_token; } set { checkout_token = value; } }

        [JsonProperty("closed_at")]
        public DateTime? Closed_at { get { return closed_at; } set { closed_at = value; } }

        [JsonProperty("confirmed")]
        public bool Confirmed { get { return confirmed; } set { confirmed = value; } }

        [JsonProperty("created_at")]
        public DateTime Created_at { get { return created_at; } set { created_at = value; } }

        [JsonProperty("currency")]
        public string Currency { get { return currency; } set { currency = value; } }

        [JsonProperty("email")]
        public string Email { get { return email; } set { email = value; } }

        [JsonProperty("financial_status")]
        public string Financial_status { get { return financial_status; } set { financial_status = value; } }

        [JsonProperty("fulfillment_status")]
        public string Fulfillment_status { get { return fulfillment_status; } set { fulfillment_status = value; } }

        [JsonProperty("gateway")]
        public string Gateway { get { return gateway; } set { gateway = value; } }

        [JsonProperty("id")]
        public Int64 Id { get { return id; } set { id = value; } }

        [JsonProperty("landing_site")]
        public string Landing_site { get { return landing_site; } set { landing_site = value; } }

        [JsonProperty("location_id")]
        public string Location_id { get { return location_id; } set { location_id = value; } }

        [JsonProperty("name")]
        public string Name { get { return name; } set { name = value; } }

        [JsonProperty("note")]
        public string Note { get { return note; } set { note = value; } }

        [JsonProperty("number")]
        public int Number { get { return number; } set { number = value; } }

        [JsonProperty("reference")]
        public string Reference { get { return reference; } set { reference = value; } }

        [JsonProperty("referring_site")]
        public string Referring_site { get { return referring_site; } set { referring_site = value; } }

        [JsonProperty("source")]
        public string Source { get { return source; } set { source = value; } }

        [JsonProperty("subtotal_price")]
        public Decimal Subtotal_price { get { return subtotal_price; } set { subtotal_price = value; } }

        [JsonProperty("taxes_included")]
        public bool Taxes_included { get { return taxes_included; } set { taxes_included = value; } }

        [JsonProperty("test")]
        public bool Test { get { return test; } set { test = value; } }

        [JsonProperty("token")]
        public string Token { get { return token; } set { token = value; } }

        [JsonProperty("total_discounts")]
        public Decimal Total_discounts { get { return total_discounts; } set { total_discounts = value; } }

        [JsonProperty("total_line_items_price")]
        public Decimal Total_line_items_price { get { return total_line_items_price; } set { total_line_items_price = value; } }

        [JsonProperty("total_price")]
        [DataType(DataType.Currency)]
        public Decimal Total_price { get { return total_price; } set { total_price = value; } }

        [JsonProperty("total_price_usd")]
        public Decimal? Total_price_usd { get { return total_price_usd; } set { total_price_usd = value; } }

        [JsonProperty("total_tax")]
        public Decimal Total_tax { get { return total_tax; } set { total_tax = value; } }

        [JsonProperty("total_weight")]
        public Decimal? Total_weight { get { return total_weight; } set { total_weight = value; } }

        [JsonProperty("updated_at")]
        public DateTime Updated_at { get { return updated_at; } set { updated_at = value; } }

        [JsonProperty("user_id")]
        public int? User_id { get { return user_id; } set { user_id = value; } }

        [JsonProperty("browser_ip")]
        public string Browser_ip { get { return browser_ip; } set { browser_ip = value; } }

        [JsonProperty("landing_site_ref")]
        public string Landing_site_ref { get { return landing_site_ref; } set { landing_site_ref = value; } }

        [JsonProperty("order_number")]
        public int Order_number { get { return order_number; } set { order_number = value; } }

        [JsonProperty("processing_method")]
        public string Processing_method { get { return processing_method; } set { processing_method = value; } }

        [JsonProperty("checkout_id")]
        public string Checkout_id { get { return checkout_id; } set { checkout_id = value; } }

        [JsonProperty("line_items")]
        public ShopifyLineItem[] LineItems;

        [JsonProperty("shipping_lines")]
        public ShopifyShippingLine[] ShippingLines;

        [JsonProperty("discount_codes")]
        public DiscountCode[] DiscountCodes;

        [JsonProperty("note_attributes")]
        public ShopifyNoteAttribute[] NoteAttributes;

        [JsonProperty("fulfillments")]
        public ShopifyFulfillment[] Fulfillments;

        [JsonProperty("client_details")]
        public ShopifyClientDetail ClientDetails;

        [JsonProperty("customer")]
        public ShopifyCustomer ShopifyCustomer;

        [JsonProperty("billing_address")]
        public ShopifyBillingAddress ShopifyBillingAddress;

        [JsonProperty("shipping_address")]
        public ShopifyShippingAddress ShopifyShippingAddress;

        [JsonProperty("tax_lines")]
        public TaxLine[] TaxLines;

        [JsonProperty("transactions")]
        public ShopifyTransaction[] Transactions;

        [JsonProperty("refunds")]
        public ShopifyRefund[] Refunds;

        [JsonProperty("payment_details")]
        public ShopifyPaymentDetails payment_details;

        #endregion


        #region Constructors
        public ShopifyOrder()
        {
            //sets reasonable initial values
            shopifyOrderID = 0;
            shopifyImportDate = DateTime.Now;
            isFMTranslated = false;
            companyID = 0; // AdminCart.Config.CompanyID(); //default to same CompanyID as AppName (ie, FM = 1, etc.)
            buyer_accepts_marketing = false;
            cancel_reason = "";
            cancelled_at = DateTime.Parse("1/1/1900");
            cart_token = "";
            checkout_token = "";
            closed_at = DateTime.Parse("1/1/1900");
            confirmed = false;
            created_at = DateTime.Parse("1/1/1900");
            currency = "";
            email = "";
            financial_status = "";
            fulfillment_status = "";
            gateway = "";
            id = 0;
            landing_site = "";
            location_id = "";
            name = "";
            note = "";
            number = 0;
            reference = "";
            referring_site = "";
            source = "";
            subtotal_price = 0;
            taxes_included = false;
            test = false;
            token = "";
            total_discounts = 0;
            total_line_items_price = 0;
            total_price = 0;
            total_price_usd = 0;
            total_tax = 0;
            total_weight = 0;
            updated_at = DateTime.Parse("1/1/1900");
            user_id = 0;
            browser_ip = "";
            landing_site_ref = "";
            order_number = 0;
            discount_codes = "";
            note_attributes = "";
            processing_method = "";
            checkout_id = "";
            LineItems = new ShopifyLineItem[] { };
            ShippingLines = new ShopifyShippingLine[] { };
            DiscountCodes = new DiscountCode[] { };
            NoteAttributes = new ShopifyNoteAttribute[] { };
            Fulfillments = new ShopifyFulfillment[] { };
            ClientDetails = new ShopifyClientDetail();
            ShopifyCustomer = new ShopifyCustomer();
            ShopifyBillingAddress = new ShopifyBillingAddress();
            ShopifyShippingAddress = new ShopifyShippingAddress();
            Transactions = new ShopifyTransaction[] { };
        }

        public ShopifyOrder(int PrimaryKeyValue)
        {
            //Loads a single record from a table
            string sql = @"
        SELECT *
        FROM ShopifyOrders
        WHERE ShopifyOrderID = @PrimaryKeyValue";

            string shopConnStr = AdminCart.Config.ConnStrShopify();
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, PrimaryKeyValue);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, shopConnStr);

            while (dr.Read())
            {
                this.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
                this.shopifyImportDate = Convert.ToDateTime(dr["shopifyImportDate"].ToString());
                this.buyer_accepts_marketing = Convert.ToBoolean(dr["buyer_accepts_marketing"].ToString());
                this.cancel_reason = Convert.ToString(dr["cancel_reason"].ToString());
                this.cancelled_at = (dr["cancelled_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["cancelled_at"].ToString());
                this.cart_token = Convert.ToString(dr["cart_token"].ToString());
                this.checkout_token = Convert.ToString(dr["checkout_token"].ToString());
                this.closed_at = (dr["closed_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["closed_at"].ToString());
                this.confirmed = Convert.ToBoolean(dr["confirmed"].ToString());
                this.created_at = (dr["created_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["created_at"].ToString());
                this.currency = Convert.ToString(dr["currency"].ToString());
                this.email = Convert.ToString(dr["email"].ToString());
                this.financial_status = Convert.ToString(dr["financial_status"].ToString());
                this.fulfillment_status = Convert.ToString(dr["fulfillment_status"].ToString());
                this.gateway = Convert.ToString(dr["gateway"].ToString());
                this.id = Convert.ToInt64(dr["id"].ToString());
                this.landing_site = Convert.ToString(dr["landing_site"].ToString());
                if (this.landing_site.Length > 1024) this.landing_site = this.landing_site.Substring(0, 1023);
                this.location_id = Convert.ToString(dr["location_id"].ToString());
                this.name = Convert.ToString(dr["name"].ToString());
                this.note = Convert.ToString(dr["note"].ToString());
                this.number = Convert.ToInt32(dr["number"].ToString());
                this.reference = Convert.ToString(dr["reference"].ToString());
                this.referring_site = Convert.ToString(dr["referring_site"].ToString());
                if (this.referring_site.Length > 2048) this.referring_site = this.referring_site.Substring(0, 2048);
                this.source = Convert.ToString(dr["source"].ToString());
                this.subtotal_price = Convert.ToDecimal(dr["subtotal_price"].ToString());
                this.taxes_included = Convert.ToBoolean(dr["taxes_included"].ToString());
                this.test = Convert.ToBoolean(dr["test"].ToString());
                this.token = Convert.ToString(dr["token"].ToString());
                this.total_discounts = Convert.ToDecimal(dr["total_discounts"].ToString());
                this.total_line_items_price = Convert.ToDecimal(dr["total_line_items_price"].ToString());
                this.total_price = Convert.ToDecimal(dr["total_price"].ToString());
                this.total_price_usd = Convert.ToDecimal(dr["total_price_usd"].ToString());
                this.total_tax = Convert.ToDecimal(dr["total_tax"].ToString());

                this.total_weight = (dr["total_weight"] == DBNull.Value) ? 0 : Convert.ToDecimal(dr["total_weight"].ToString());
                //this.total_weight = Convert.ToDecimal(dr["total_weight"].ToString());

                this.updated_at = (dr["updated_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["updated_at"].ToString());
                this.user_id = (dr["user_id"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["user_id"].ToString());
                this.browser_ip = Convert.ToString(dr["browser_ip"].ToString());
                this.landing_site_ref = Convert.ToString(dr["landing_site_ref"].ToString());
                this.order_number = Convert.ToInt32(dr["order_number"].ToString());
                this.discount_codes = Convert.ToString(dr["discount_codes"].ToString());
                this.note_attributes = Convert.ToString(dr["note_attributes"].ToString());
                this.processing_method = Convert.ToString(dr["processing_method"].ToString());
                this.checkout_id = (dr["checkout_id"] == DBNull.Value) ? "0" : (dr["checkout_id"].ToString());
                this.isFMTranslated = Convert.ToBoolean(dr["translated"]);
                this.companyID = Convert.ToInt32(dr["companyID"].ToString());
            }
            dr.Close();
            LineItems = new ShopifyLineItem[] { };
            ShippingLines = new ShopifyShippingLine[] { };
            DiscountCodes = new DiscountCode[] { };
            NoteAttributes = new ShopifyNoteAttribute[] { };
            Fulfillments = new ShopifyFulfillment[] { };
            ClientDetails = new ShopifyClientDetail();
            ShopifyCustomer = new ShopifyCustomer();
            ShopifyBillingAddress = new ShopifyBillingAddress();
            ShopifyShippingAddress = new ShopifyShippingAddress();
            Transactions = new ShopifyTransaction[] { };

            ShopifyShippingLine shipLine = new ShopifyShippingLine(this.shopifyOrderID, this.id);
            List<ShopifyShippingLine> shipLineList = new List<ShopifyShippingLine>();
            shipLineList.Add(shipLine);
            ShippingLines = shipLineList.ToArray();
        }

        public ShopifyOrder(long id, int companyID)
        {
            //Loads a single record from a table
            string sql = @"
                SELECT *
                FROM ShopifyOrders
                WHERE ID = @ID AND CompanyID = @CompanyID 
            ";

            string shopConnStr = AdminCart.Config.ConnStrShopify();
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, id, companyID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, shopConnStr);

            while (dr.Read())
            {
                this.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
                this.shopifyImportDate = Convert.ToDateTime(dr["shopifyImportDate"].ToString());
                this.buyer_accepts_marketing = Convert.ToBoolean(dr["buyer_accepts_marketing"].ToString());
                this.cancel_reason = Convert.ToString(dr["cancel_reason"].ToString());
                this.cancelled_at = (dr["cancelled_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["cancelled_at"].ToString());
                this.cart_token = Convert.ToString(dr["cart_token"].ToString());
                this.checkout_token = Convert.ToString(dr["checkout_token"].ToString());
                this.closed_at = (dr["closed_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["closed_at"].ToString());
                this.confirmed = Convert.ToBoolean(dr["confirmed"].ToString());
                this.created_at = (dr["created_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["created_at"].ToString());
                this.currency = Convert.ToString(dr["currency"].ToString());
                this.email = Convert.ToString(dr["email"].ToString());
                this.financial_status = Convert.ToString(dr["financial_status"].ToString());
                this.fulfillment_status = Convert.ToString(dr["fulfillment_status"].ToString());
                this.gateway = Convert.ToString(dr["gateway"].ToString());
                this.id = Convert.ToInt64(dr["id"].ToString());
                this.landing_site = Convert.ToString(dr["landing_site"].ToString());
                if (this.landing_site.Length > 1024) this.landing_site = this.landing_site.Substring(0, 1023);
                this.location_id = Convert.ToString(dr["location_id"].ToString());
                this.name = Convert.ToString(dr["name"].ToString());
                this.note = Convert.ToString(dr["note"].ToString());
                this.number = Convert.ToInt32(dr["number"].ToString());
                this.reference = Convert.ToString(dr["reference"].ToString());
                this.referring_site = Convert.ToString(dr["referring_site"].ToString());
                if (this.referring_site.Length > 2048) this.referring_site = this.referring_site.Substring(0, 2048);
                this.source = Convert.ToString(dr["source"].ToString());
                this.subtotal_price = Convert.ToDecimal(dr["subtotal_price"].ToString());
                this.taxes_included = Convert.ToBoolean(dr["taxes_included"].ToString());
                this.test = Convert.ToBoolean(dr["test"].ToString());
                this.token = Convert.ToString(dr["token"].ToString());
                this.total_discounts = Convert.ToDecimal(dr["total_discounts"].ToString());
                this.total_line_items_price = Convert.ToDecimal(dr["total_line_items_price"].ToString());
                this.total_price = Convert.ToDecimal(dr["total_price"].ToString());
                this.total_price_usd = Convert.ToDecimal(dr["total_price_usd"].ToString());
                this.total_tax = Convert.ToDecimal(dr["total_tax"].ToString());

                this.total_weight = (dr["total_weight"] == DBNull.Value) ? 0 : Convert.ToDecimal(dr["total_weight"].ToString());
                //this.total_weight = Convert.ToDecimal(dr["total_weight"].ToString());

                this.updated_at = (dr["updated_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["updated_at"].ToString());
                this.user_id = (dr["user_id"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["user_id"].ToString());
                this.browser_ip = Convert.ToString(dr["browser_ip"].ToString());
                this.landing_site_ref = Convert.ToString(dr["landing_site_ref"].ToString());
                this.order_number = Convert.ToInt32(dr["order_number"].ToString());
                this.discount_codes = Convert.ToString(dr["discount_codes"].ToString());
                this.note_attributes = Convert.ToString(dr["note_attributes"].ToString());
                this.processing_method = Convert.ToString(dr["processing_method"].ToString());
                this.checkout_id = (dr["checkout_id"] == DBNull.Value) ? "0" : (dr["checkout_id"].ToString());
                this.isFMTranslated = Convert.ToBoolean(dr["translated"]);
                this.companyID = Convert.ToInt32(dr["companyID"].ToString());
            }
            dr.Close();

            LineItems = new ShopifyLineItem[] { };
            ShippingLines = new ShopifyShippingLine[] { };
            DiscountCodes = new DiscountCode[] { };
            NoteAttributes = new ShopifyNoteAttribute[] { };
            Fulfillments = new ShopifyFulfillment[] { };
            ClientDetails = new ShopifyClientDetail();
            ShopifyCustomer = new ShopifyCustomer();
            ShopifyBillingAddress = new ShopifyBillingAddress();
            ShopifyShippingAddress = new ShopifyShippingAddress();
            Transactions = new ShopifyTransaction[] { };

            ShopifyShippingLine shipLine = new ShopifyShippingLine(this.shopifyOrderID, this.id);
            List<ShopifyShippingLine> shipLineList = new List<ShopifyShippingLine>();
            shipLineList.Add(shipLine);
            ShippingLines = shipLineList.ToArray();
        }

        #endregion


        #region Methods
        //requires DBUtil class from classhatch.com
        public int SaveDB()
        {
            //Insert or Update record on SQL Table
            companyID = AdminCart.Config.CompanyID();
            if (total_price_usd == null) { total_price_usd = 0; }
            if (total_weight == null) { total_weight = 0; }
            SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrShopify());
            string sql = "";

            if (ShopifyOrderID == 0)
            {
                //Insert if primary key is default value of zero
                sql = @"
            INSERT ShopifyOrders(
--ShopifyOrderID,
ShopifyImportDate,
buyer_accepts_marketing,
cancel_reason,
cancelled_at,
cart_token,
checkout_token,
closed_at,
confirmed,
created_at,
currency,
email,
financial_status,
fulfillment_status,
gateway,
id,
landing_site,
location_id,
name,
note,
number,
reference,
referring_site,
source,
subtotal_price,
taxes_included,
test,
token,
total_discounts,
total_line_items_price,
total_price,
total_price_usd,
total_tax,
total_weight,
updated_at,
user_id,
browser_ip,
landing_site_ref,
order_number,
discount_codes,
note_attributes,
processing_method,
checkout_id,
translated,
companyID 

             )VALUES(
--@ShopifyOrderID,
@ShopifyImportDate,
@buyer_accepts_marketing,
@cancel_reason,
@cancelled_at,
@cart_token,
@checkout_token,
@closed_at,
@confirmed,
@created_at,
@currency,
@email,
@financial_status,
@fulfillment_status,
@gateway,
@id,
@landing_site,
@location_id,
@name,
@note,
@number,
@reference,
@referring_site,
@source,
@subtotal_price,
@taxes_included,
@test,
@token,
@total_discounts,
@total_line_items_price,
@total_price,
@total_price_usd,
@total_tax,
@total_weight,
@updated_at,
@user_id,
@browser_ip,
@landing_site_ref,
@order_number,
@discount_codes,
@note_attributes,
@processing_method,
@checkout_id,
@translated,
@companyID 

            ); select id=scope_identity();";
            }
            else
            {
                //Update db record if primary key is not default value of zero
                sql = @"
            UPDATE ShopifyOrders
            SET 
--ShopifyOrderID= @ShopifyOrderID,
ShopifyImportDate= @ShopifyImportDate,
buyer_accepts_marketing= @buyer_accepts_marketing,
cancel_reason= @cancel_reason,
cancelled_at= @cancelled_at,
cart_token= @cart_token,
checkout_token= @checkout_token,
closed_at= @closed_at,
confirmed= @confirmed,
created_at= @created_at,
currency= @currency,
email= @email,
financial_status= @financial_status,
fulfillment_status= @fulfillment_status,
gateway= @gateway,
id= @id,
landing_site= @landing_site,
location_id= @location_id,
name= @name,
note= @note,
number= @number,
reference= @reference,
referring_site= @referring_site,
source= @source,
subtotal_price= @subtotal_price,
taxes_included= @taxes_included,
test= @test,
token= @token,
total_discounts= @total_discounts,
total_line_items_price= @total_line_items_price,
total_price= @total_price,
total_price_usd= @total_price_usd,
total_tax= @total_tax,
total_weight= @total_weight,
updated_at= @updated_at,
user_id= @user_id,
browser_ip= @browser_ip,
landing_site_ref= @landing_site_ref,
order_number= @order_number,
discount_codes= @discount_codes,
note_attributes= @note_attributes,
processing_method= @processing_method,
checkout_id= @checkout_id,
translated = @translated,
companyID = @companyID 

            WHERE ShopifyOrderID = PrimaryKeyFieldValue; select id=@ShopifyOrderID;";
            }
            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlParameter shopifyOrderIDParam = new SqlParameter("@shopifyOrderID", SqlDbType.Int);
            SqlParameter shopifyImportDateParam = new SqlParameter("@shopifyImportDate", SqlDbType.DateTime);
            SqlParameter buyer_accepts_marketingParam = new SqlParameter("@buyer_accepts_marketing", SqlDbType.Bit);
            SqlParameter cancel_reasonParam = new SqlParameter("@cancel_reason", SqlDbType.VarChar);
            SqlParameter cancelled_atParam = new SqlParameter("@cancelled_at", SqlDbType.DateTime);
            SqlParameter cart_tokenParam = new SqlParameter("@cart_token", SqlDbType.VarChar);
            SqlParameter checkout_tokenParam = new SqlParameter("@checkout_token", SqlDbType.VarChar);
            SqlParameter closed_atParam = new SqlParameter("@closed_at", SqlDbType.DateTime);
            SqlParameter confirmedParam = new SqlParameter("@confirmed", SqlDbType.Bit);
            SqlParameter created_atParam = new SqlParameter("@created_at", SqlDbType.DateTime);
            SqlParameter currencyParam = new SqlParameter("@currency", SqlDbType.VarChar);
            SqlParameter emailParam = new SqlParameter("@email", SqlDbType.VarChar);
            SqlParameter financial_statusParam = new SqlParameter("@financial_status", SqlDbType.VarChar);
            SqlParameter fulfillment_statusParam = new SqlParameter("@fulfillment_status", SqlDbType.VarChar);
            SqlParameter gatewayParam = new SqlParameter("@gateway", SqlDbType.VarChar);
            SqlParameter idParam = new SqlParameter("@id", SqlDbType.BigInt);
            SqlParameter landing_siteParam = new SqlParameter("@landing_site", SqlDbType.VarChar);
            SqlParameter location_idParam = new SqlParameter("@location_id", SqlDbType.VarChar);
            SqlParameter nameParam = new SqlParameter("@name", SqlDbType.VarChar);
            SqlParameter noteParam = new SqlParameter("@note", SqlDbType.VarChar);
            SqlParameter numberParam = new SqlParameter("@number", SqlDbType.Int);
            SqlParameter referenceParam = new SqlParameter("@reference", SqlDbType.VarChar);
            SqlParameter referring_siteParam = new SqlParameter("@referring_site", SqlDbType.VarChar);
            SqlParameter sourceParam = new SqlParameter("@source", SqlDbType.VarChar);
            SqlParameter subtotal_priceParam = new SqlParameter("@subtotal_price", SqlDbType.Money);
            SqlParameter taxes_includedParam = new SqlParameter("@taxes_included", SqlDbType.Bit);
            SqlParameter testParam = new SqlParameter("@test", SqlDbType.Bit);
            SqlParameter tokenParam = new SqlParameter("@token", SqlDbType.VarChar);
            SqlParameter total_discountsParam = new SqlParameter("@total_discounts", SqlDbType.Money);
            SqlParameter total_line_items_priceParam = new SqlParameter("@total_line_items_price", SqlDbType.Money);
            SqlParameter total_priceParam = new SqlParameter("@total_price", SqlDbType.Money);
            SqlParameter total_price_usdParam = new SqlParameter("@total_price_usd", SqlDbType.Money);
            SqlParameter total_taxParam = new SqlParameter("@total_tax", SqlDbType.Money);
            SqlParameter total_weightParam = new SqlParameter("@total_weight", SqlDbType.Decimal);
            SqlParameter updated_atParam = new SqlParameter("@updated_at", SqlDbType.DateTime);
            SqlParameter user_idParam = new SqlParameter("@user_id", SqlDbType.Int);
            SqlParameter browser_ipParam = new SqlParameter("@browser_ip", SqlDbType.VarChar);
            SqlParameter landing_site_refParam = new SqlParameter("@landing_site_ref", SqlDbType.VarChar);
            SqlParameter order_numberParam = new SqlParameter("@order_number", SqlDbType.Int);
            SqlParameter discount_codesParam = new SqlParameter("@discount_codes", SqlDbType.VarChar);
            SqlParameter note_attributesParam = new SqlParameter("@note_attributes", SqlDbType.VarChar);
            SqlParameter processing_methodParam = new SqlParameter("@processing_method", SqlDbType.VarChar);
            SqlParameter checkout_idParam = new SqlParameter("@checkout_id", SqlDbType.BigInt);
            SqlParameter translatedParam = new SqlParameter("@translated", SqlDbType.Bit);
            SqlParameter companyIDParam = new SqlParameter("@companyID", SqlDbType.Int);

            shopifyOrderIDParam.Value = shopifyOrderID;
            shopifyImportDateParam.Value = shopifyImportDate;
            buyer_accepts_marketingParam.Value = buyer_accepts_marketing;
            cancel_reasonParam.Value = (object)cancel_reason ?? DBNull.Value;
            cancelled_atParam.Value = (object)cancelled_at ?? DBNull.Value;
            cart_tokenParam.Value = (object)cart_token ?? DBNull.Value;
            checkout_tokenParam.Value = (object)checkout_token ?? DBNull.Value;
            closed_atParam.Value = (object)closed_at ?? DBNull.Value;
            confirmedParam.Value = confirmed;
            created_atParam.Value = created_at;
            currencyParam.Value = currency;
            emailParam.Value = email;
            financial_statusParam.Value = (object)financial_status ?? DBNull.Value;
            fulfillment_statusParam.Value = (object)fulfillment_status ?? DBNull.Value;
            gatewayParam.Value = (object)gateway ?? DBNull.Value;
            idParam.Value = id;
            landing_siteParam.Value = (object)landing_site ?? DBNull.Value;
            location_idParam.Value = (object)location_id ?? DBNull.Value;
            nameParam.Value = name;
            if(!string.IsNullOrEmpty(note))
            {
                if(note.Length >= 499)
                {
                    note = note.Substring(0, 490);
                }
            }
            noteParam.Value = (object)note ?? DBNull.Value;
            numberParam.Value = number;
            referenceParam.Value = (object)reference ?? DBNull.Value;
            referring_siteParam.Value = (object)referring_site ?? DBNull.Value;
            sourceParam.Value = source;
            subtotal_priceParam.Value = subtotal_price;
            taxes_includedParam.Value = taxes_included;
            testParam.Value = test;
            tokenParam.Value = token;
            total_discountsParam.Value = total_discounts;
            total_line_items_priceParam.Value = total_line_items_price;
            total_priceParam.Value = total_price;
            total_price_usdParam.Value = total_price_usd;
            total_taxParam.Value = total_tax;
            total_weightParam.Value = total_weight;
            updated_atParam.Value = updated_at;
            user_idParam.Value = (object)user_id ?? DBNull.Value;
            browser_ipParam.Value = (object)browser_ip ?? DBNull.Value;
            landing_site_refParam.Value = (object)landing_site_ref ?? DBNull.Value;
            order_numberParam.Value = order_number;
            discount_codesParam.Value = discount_codes;
            note_attributesParam.Value = (Object)note_attributes ?? DBNull.Value;
            processing_methodParam.Value = processing_method;
            checkout_idParam.Value = (object)checkout_id ?? DBNull.Value;
            translatedParam.Value = isFMTranslated;
            companyIDParam.Value = companyID;

            cmd.Parameters.Add(shopifyOrderIDParam);
            cmd.Parameters.Add(shopifyImportDateParam);
            cmd.Parameters.Add(buyer_accepts_marketingParam);
            cmd.Parameters.Add(cancel_reasonParam);
            cmd.Parameters.Add(cancelled_atParam);
            cmd.Parameters.Add(cart_tokenParam);
            cmd.Parameters.Add(checkout_tokenParam);
            cmd.Parameters.Add(closed_atParam);
            cmd.Parameters.Add(confirmedParam);
            cmd.Parameters.Add(created_atParam);
            cmd.Parameters.Add(currencyParam);
            cmd.Parameters.Add(emailParam);
            cmd.Parameters.Add(financial_statusParam);
            cmd.Parameters.Add(fulfillment_statusParam);
            cmd.Parameters.Add(gatewayParam);
            cmd.Parameters.Add(idParam);
            cmd.Parameters.Add(landing_siteParam);
            cmd.Parameters.Add(location_idParam);
            cmd.Parameters.Add(nameParam);
            cmd.Parameters.Add(noteParam);
            cmd.Parameters.Add(numberParam);
            cmd.Parameters.Add(referenceParam);
            cmd.Parameters.Add(referring_siteParam);
            cmd.Parameters.Add(sourceParam);
            cmd.Parameters.Add(subtotal_priceParam);
            cmd.Parameters.Add(taxes_includedParam);
            cmd.Parameters.Add(testParam);
            cmd.Parameters.Add(tokenParam);
            cmd.Parameters.Add(total_discountsParam);
            cmd.Parameters.Add(total_line_items_priceParam);
            cmd.Parameters.Add(total_priceParam);
            cmd.Parameters.Add(total_price_usdParam);
            cmd.Parameters.Add(total_taxParam);
            cmd.Parameters.Add(total_weightParam);
            cmd.Parameters.Add(updated_atParam);
            cmd.Parameters.Add(user_idParam);
            cmd.Parameters.Add(browser_ipParam);
            cmd.Parameters.Add(landing_site_refParam);
            cmd.Parameters.Add(order_numberParam);
            cmd.Parameters.Add(discount_codesParam);
            cmd.Parameters.Add(note_attributesParam);
            cmd.Parameters.Add(processing_methodParam);
            cmd.Parameters.Add(checkout_idParam);
            cmd.Parameters.Add(translatedParam);
            cmd.Parameters.Add(companyIDParam);

            conn.Open();
            int result = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            conn.Close();
            return result;
        }

        static public List<ShopifyOrder> GetShopifyOrderList()
        {
            string conn = AdminCart.Config.ConnStrShopify();
            List<ShopifyOrder> thelist = new List<ShopifyOrder>();

            string sql = "SELECT * FROM ShopifyOrders";

            SqlDataReader dr = DBUtil.FillDataReader(sql, conn);

            while (dr.Read())
            {
                ShopifyOrder obj = new ShopifyOrder();

                obj.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
                obj.shopifyImportDate = Convert.ToDateTime(dr["shopifyImportDate"].ToString());
                obj.buyer_accepts_marketing = Convert.ToBoolean(dr["buyer_accepts_marketing"].ToString());
                obj.cancel_reason = Convert.ToString(dr["cancel_reason"].ToString());
                obj.cancelled_at = (dr["cancelled_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["cancelled_at"].ToString());
                obj.cart_token = Convert.ToString(dr["cart_token"].ToString());
                obj.checkout_token = Convert.ToString(dr["checkout_token"].ToString());
                obj.closed_at = (dr["closed_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["closed_at"].ToString());
                obj.confirmed = Convert.ToBoolean(dr["confirmed"].ToString());
                obj.created_at = (dr["created_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["created_at"].ToString());
                obj.currency = Convert.ToString(dr["currency"].ToString());
                obj.email = Convert.ToString(dr["email"].ToString());
                obj.financial_status = Convert.ToString(dr["financial_status"].ToString());
                obj.fulfillment_status = Convert.ToString(dr["fulfillment_status"].ToString());
                obj.gateway = Convert.ToString(dr["gateway"].ToString());
                obj.id = Convert.ToInt64(dr["id"].ToString());
                obj.landing_site = Convert.ToString(dr["landing_site"].ToString());
                if (obj.landing_site.Length > 1024) obj.landing_site = obj.landing_site.Substring(0, 1023);
                obj.location_id = Convert.ToString(dr["location_id"].ToString());
                obj.name = Convert.ToString(dr["name"].ToString());
                obj.note = Convert.ToString(dr["note"].ToString());
                obj.number = Convert.ToInt32(dr["number"].ToString());
                obj.reference = Convert.ToString(dr["reference"].ToString());
                obj.referring_site = Convert.ToString(dr["referring_site"].ToString());
                if (obj.referring_site.Length > 2048) obj.referring_site = obj.referring_site.Substring(0, 2048);
                obj.source = Convert.ToString(dr["source"].ToString());
                obj.subtotal_price = Convert.ToDecimal(dr["subtotal_price"].ToString());
                obj.taxes_included = Convert.ToBoolean(dr["taxes_included"].ToString());
                obj.test = Convert.ToBoolean(dr["test"].ToString());
                obj.token = Convert.ToString(dr["token"].ToString());
                obj.total_discounts = Convert.ToDecimal(dr["total_discounts"].ToString());
                obj.total_line_items_price = Convert.ToDecimal(dr["total_line_items_price"].ToString());
                obj.total_price = Convert.ToDecimal(dr["total_price"].ToString());
                obj.total_price_usd = Convert.ToDecimal(dr["total_price_usd"].ToString());
                obj.total_tax = Convert.ToDecimal(dr["total_tax"].ToString());

                obj.total_weight = (dr["total_weight"] == DBNull.Value) ? 0 : Convert.ToDecimal(dr["total_weight"].ToString());
                //obj.total_weight = Convert.ToDecimal(dr["total_weight"].ToString());

                obj.updated_at = (dr["updated_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["updated_at"].ToString());
                obj.user_id = (dr["user_id"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["user_id"].ToString());
                obj.browser_ip = Convert.ToString(dr["browser_ip"].ToString());
                obj.landing_site_ref = Convert.ToString(dr["landing_site_ref"].ToString());
                obj.order_number = Convert.ToInt32(dr["order_number"].ToString());
                obj.discount_codes = Convert.ToString(dr["discount_codes"].ToString());
                obj.note_attributes = Convert.ToString(dr["note_attributes"].ToString());
                obj.processing_method = Convert.ToString(dr["processing_method"].ToString());
                obj.checkout_id = (dr["checkout_id"] == DBNull.Value) ? "0" : (dr["checkout_id"].ToString());
                obj.isFMTranslated = Convert.ToBoolean(dr["translated"]);
                obj.companyID = Convert.ToInt32(dr["companyID"].ToString());

                thelist.Add(obj);
            }
            dr.Close();
            return thelist;

        }

        static public List<ShopifyOrder> GetShopifyOrderNoteList()
        {
            string conn = AdminCart.Config.ConnStrShopify();
            List<ShopifyOrder> thelist = new List<ShopifyOrder>();

            string sql = "SELECT * FROM ShopifyOrders WHERE note NOT LIKE ''";

            SqlDataReader dr = DBUtil.FillDataReader(sql, conn);

            while (dr.Read())
            {
                ShopifyOrder obj = new ShopifyOrder();

                obj.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
                obj.shopifyImportDate = Convert.ToDateTime(dr["shopifyImportDate"].ToString());
                obj.buyer_accepts_marketing = Convert.ToBoolean(dr["buyer_accepts_marketing"].ToString());
                obj.cancel_reason = Convert.ToString(dr["cancel_reason"].ToString());
                obj.cancelled_at = (dr["cancelled_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["cancelled_at"].ToString());
                obj.cart_token = Convert.ToString(dr["cart_token"].ToString());
                obj.checkout_token = Convert.ToString(dr["checkout_token"].ToString());
                obj.closed_at = (dr["closed_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["closed_at"].ToString());
                obj.confirmed = Convert.ToBoolean(dr["confirmed"].ToString());
                obj.created_at = (dr["created_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["created_at"].ToString());
                obj.currency = Convert.ToString(dr["currency"].ToString());
                obj.email = Convert.ToString(dr["email"].ToString());
                obj.financial_status = Convert.ToString(dr["financial_status"].ToString());
                obj.fulfillment_status = Convert.ToString(dr["fulfillment_status"].ToString());
                obj.gateway = Convert.ToString(dr["gateway"].ToString());
                obj.id = Convert.ToInt64(dr["id"].ToString());
                obj.landing_site = Convert.ToString(dr["landing_site"].ToString());
                if (obj.landing_site.Length > 1024) obj.landing_site = obj.landing_site.Substring(0, 1023);
                obj.location_id = Convert.ToString(dr["location_id"].ToString());
                obj.name = Convert.ToString(dr["name"].ToString());
                obj.note = Convert.ToString(dr["note"].ToString());
                obj.number = Convert.ToInt32(dr["number"].ToString());
                obj.reference = Convert.ToString(dr["reference"].ToString());
                obj.referring_site = Convert.ToString(dr["referring_site"].ToString());
                if (obj.referring_site.Length > 2048) obj.referring_site = obj.referring_site.Substring(0, 2048);
                obj.source = Convert.ToString(dr["source"].ToString());
                obj.subtotal_price = Convert.ToDecimal(dr["subtotal_price"].ToString());
                obj.taxes_included = Convert.ToBoolean(dr["taxes_included"].ToString());
                obj.test = Convert.ToBoolean(dr["test"].ToString());
                obj.token = Convert.ToString(dr["token"].ToString());
                obj.total_discounts = Convert.ToDecimal(dr["total_discounts"].ToString());
                obj.total_line_items_price = Convert.ToDecimal(dr["total_line_items_price"].ToString());
                obj.total_price = Convert.ToDecimal(dr["total_price"].ToString());
                obj.total_price_usd = Convert.ToDecimal(dr["total_price_usd"].ToString());
                obj.total_tax = Convert.ToDecimal(dr["total_tax"].ToString());

                obj.total_weight = (dr["total_weight"] == DBNull.Value) ? 0 : Convert.ToDecimal(dr["total_weight"].ToString());
                //obj.total_weight = Convert.ToDecimal(dr["total_weight"].ToString());

                obj.updated_at = (dr["updated_at"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dr["updated_at"].ToString());
                obj.user_id = (dr["user_id"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["user_id"].ToString());
                obj.browser_ip = Convert.ToString(dr["browser_ip"].ToString());
                obj.landing_site_ref = Convert.ToString(dr["landing_site_ref"].ToString());
                obj.order_number = Convert.ToInt32(dr["order_number"].ToString());
                obj.discount_codes = Convert.ToString(dr["discount_codes"].ToString());
                obj.note_attributes = Convert.ToString(dr["note_attributes"].ToString());
                obj.processing_method = Convert.ToString(dr["processing_method"].ToString());
                obj.checkout_id = (dr["checkout_id"] == DBNull.Value) ? "0" : (dr["checkout_id"].ToString());
                obj.isFMTranslated = Convert.ToBoolean(dr["translated"]);
                obj.companyID = Convert.ToInt32(dr["companyID"].ToString());

                thelist.Add(obj);
            }
            dr.Close();
            return thelist;

        }

        public static long GetShopifyIDFromName(string name, int companyID)
        {
            long id = 0;

            int temp = 0;
            bool ok = (Int32.TryParse(name, out temp));
            if (temp > 800000) //exclude legacy SS or HDI orders
            {
                string conn = AdminCart.Config.ConnStrShopify();
                string sql = @" 
                SELECT id 
                FROM ShopifyOrders 
                WHERE name = @name AND companyID = @companyID 
            ";

                SqlParameter[] mySqlParameters = FM2015.Helpers.DBUtil.BuildParametersFrom(sql, name, companyID);
                SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, conn);

                if (dr.HasRows && dr.Read())
                {
                    id = (dr["id"] == DBNull.Value) ? 0 : Convert.ToInt64(dr["id"]);
                }
                else
                {
                    //put some exception handling code here
                    string msg = "error in GetShopifyIDFromName" + Environment.NewLine;
                    msg += "Name: " + name + Environment.NewLine;
                    FM2015.Helpers.dbErrorLogging.LogError(msg);
                }
                if (dr != null) { dr.Close(); }
            }

            return id;
        }

        public static long PutShopifyOrderViaAPI(ShopifyOrder shopOrder)
        {
            long partnerID = -1;
            string uri = "admin/orders/" + shopOrder.Id.ToString() + ".json";
            /*
            ShopifyOrderSingle singleOrder = new ShopifyOrderSingle();
            singleOrder.order = shopOrder;
            string dataStr = JsonConvert.SerializeObject(singleOrder);
            byte[] dataBuffer = Encoding.ASCII.GetBytes(dataStr);
            */

            String JsonString = @"{""order"":{""id"":**id**,""email"":""**email**""}}";

            /*
                        ""note"":""**note**"",
                        ""buyer_accepts_marketing"": ""true""
            */

            JsonString = JsonString.Replace("**id**", shopOrder.Id.ToString());
            JsonString = JsonString.Replace("**email**", shopOrder.Email.ToString());
            JsonString = JsonString.Replace("**note**", shopOrder.Note.ToString());
            JsonString = JsonString.Replace("**buyer_accepts_marketing**", shopOrder.buyer_accepts_marketing.ToString());

            string apiResult = JsonHelpers.PUTtoShopify(uri, JsonString);

            try
            {
                ShopifyOrder order = JsonConvert.DeserializeObject<ShopifyOrder>(apiResult);
                partnerID = order.Id;
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                string msg = "error in PutShopifyOrderViaAPI" + Environment.NewLine;
                msg += "jsonURL: " + uri + Environment.NewLine;
                msg += "jsonString: " + JsonString + Environment.NewLine;
                msg += "response: " + apiResult + Environment.NewLine;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
            return partnerID; //returns id of successful, -1 if not

        }

        public static void Cancel(long shopID)
        {
            string uri = "admin/orders/" + shopID.ToString() + "/cancel.json";
            String JsonString = @"{}";

            string apiResult = JsonHelpers.POSTtoShopify(uri, JsonString);

            try
            {
                ShopifyOrder order = JsonConvert.DeserializeObject<ShopifyOrder>(apiResult);
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                string msg = "error in Cancel Shopify Order" + Environment.NewLine;
                msg += "jsonURL: " + uri + Environment.NewLine;
                msg += "jsonString: " + JsonString + Environment.NewLine;
                msg += "response: " + apiResult + Environment.NewLine;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
        }

        #endregion


        #region Validation
        //Requires Validation class from classhatch.com
        static public bool IsValid(ShopifyOrder obj)
        {
            bool isValid = false;

            if (GetErrors(obj).Count == 0)
            {
                isValid = true;
            }

            return isValid;
        }


        static public List<ValidationError> GetErrors(ShopifyOrder obj)
        {
            List<ValidationError> errorlist = new List<ValidationError>();

            /*
            if (ValidateUtil.IsTooLong(20, obj.FirstName))
            {
                ValidationError e = new ValidationError();
                e.Field = "First Name";
                e.ErrorDescription = "20 characters maximium are allowed";
                errorlist.Add(e);
            };
            */

            return errorlist;
        }
        #endregion


        #region OverRides

        #endregion

    }

}