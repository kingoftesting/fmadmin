﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using FM2015.Helpers;
using FMWebApp.Helpers;

namespace ShopifyAgent
{
    
    public class ShopifyOrderRisks
    {
        [JsonProperty("risks")]
        public ShopOrderRisk[] Risks;
    }

     public class ShopifyOrderRisk
    {
         [JsonProperty("risk")]
         public ShopOrderRisk Risk;
    }

    public class ShopOrderRisk
    {
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("order_id")]
        public long Order_id { get; set; }

        [JsonProperty("score")]
        public decimal? Score { get; set; }

        [JsonProperty("cause_cancel")]
        public string Cause_cancel { get; set; }

        [JsonProperty("display")]
        public bool Display { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("recommendation")]
        public string Recommendation { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        public int RiskId { get; set; }
        public int ShopifyName { get; set; }

        public ShopOrderRisk()
        {
            RiskId = 0;
            Id = 0;
            Order_id = 0;
            ShopifyName = 0;
            Score = 0;
            Cause_cancel = "";
            Display = false;
            Message = "";
            Recommendation = "";
            Source = "";
        }

        public static List<ShopOrderRisk> GetOrderRiskList(long id, int companyID)
        {
            List<ShopOrderRisk> riskList = new List<ShopOrderRisk>();
            if (companyID == AdminCart.Config.CompanyID())
            {
                ShopifyOrderRisks risks = new ShopifyOrderRisks();
                string jsonURL = "admin/orders/" + id.ToString() + "/risks.json";
                string apiResult = JsonHelpers.GETfromShopify(jsonURL);
                try
                {
                    risks = JsonConvert.DeserializeObject<ShopifyOrderRisks>(apiResult);
                }
                catch (Exception ex)
                {
                    //put some exception handling code here
                    string msg = "error in GetOrderRisk" + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "response: " + apiResult + Environment.NewLine;
                    FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                }

                if (risks.Risks != null)
                {
                    for (int i = 0; i < risks.Risks.Length; i++)
                    {
                        if (risks.Risks[i].Score == null)
                        {
                            risks.Risks[i].Score = 0;
                        }
                        if (risks.Risks[i].Id == null)
                        {
                            risks.Risks[i].Id = 0;
                        }
                        riskList.Add(risks.Risks[i]);
                    }
                }
            }

            return riskList;
        }

    }
}