﻿using System;
using Newtonsoft.Json;

namespace ShopifyAgent
{
   
    public class ShopifyPaymentDetails
    {
        [JsonProperty("avs_result_code")]
        public string Avs_result_code { get; set; }

        [JsonProperty("credit_card_bin")]
        public string Credit_card_bin { get; set; }

        [JsonProperty("cvv_result_code")]
        public string Cvv_result_code { get; set; }

        [JsonProperty("credit_card_number")]
        public string Credit_card_number { get; set; }

        [JsonProperty("credit_card_company")]
        public string Credit_card_company { get; set; }
    }
}