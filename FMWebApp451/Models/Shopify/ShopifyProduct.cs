﻿using System;
using Newtonsoft.Json;
using FMWebApp.Helpers;

namespace ShopifyAgent
{
    public class ShopifyProducts
    {
        [JsonProperty("products")]
        public ShopifyProduct[] products;
    }  
    
    public class ShopifyProductSingle
    {
        [JsonProperty("product")]
        public ShopifyProduct product { get; set; }

        public ShopifyProductSingle()
        {
            product = new ShopifyProduct();
        }

    }

    public class ProductVariantSingle
    {
        [JsonProperty("variant")]
        public ProductVariant variant { get; set; }

        public ProductVariantSingle()
        {
            variant = new ProductVariant();
        }

    }

    public class ProductVariant
    {

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("compare_at_price")]
        public decimal? Compare_at_price { get; set; }

        [JsonProperty("created_at")]
        public DateTime? Created_at { get; set; }

        [JsonProperty("fulfillment_service")]
        public string Fulfillment_service { get; set; }

        [JsonProperty("grams")]
        public double Grams { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("inventory_management")]
        public string Inventory_management { get; set; }

        [JsonProperty("inventory_policy")]
        public string Inventory_policy { get; set; }

        [JsonProperty("option1")]
        public string Option1 { get; set; }

        [JsonProperty("option2")]
        public string Option2 { get; set; }

        [JsonProperty("option3")]
        public string Option3 { get; set; }

        [JsonProperty("position")]
        public string Position { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("product_id")]
        public long? Product_id { get; set; }

        [JsonProperty("requires_shipping")]
        public bool Requires_shipping { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("taxable")]
        public bool Taxable { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("updated_at")]
        public DateTime? Updated_at { get; set; }

        [JsonProperty("inventory_quantity")]
        public int Inventory_quantity { get; set; }

        [JsonProperty("old_inventory_quantity")]
        public int Old_inventory_quantity { get; set; }

        [JsonProperty("image_id")]
        public long? Image_id { get; set; }

        [JsonProperty("weight")]
        public double Weight { get; set; }

        [JsonProperty("weight_unit")]
        public string Weight_unit { get; set; }

        public ProductVariant()
        {
            Barcode = "";
            Compare_at_price = 0;
            //Created_at = Convert.ToDateTime("1/1/1900");
            Fulfillment_service = "manual";
            Grams = 0;
            //Id = 0;
            //Inventory_management = "";
            //Inventory_policy = "";
            //Option1 = "";
            //Option2 = "";
            //Option3 = "";
            //Position = 1;
            Price = 0;
            Product_id = 0;
            Requires_shipping = true;
            Sku = "";
            Taxable = true;
            //Updated_at = Convert.ToDateTime("1/1/1900");
            //Inventory_quantity = 0;
            //Image_id = 0;
            Weight = 0;
            Weight_unit = "lb";
        }

        public static void UpDateInventory(long variantID, int newQty, int oldQty)
        {
            /*
            PUT /admin/variants/#{id}.json
                {
                  "variant": {
                    "id": 808950810,
                    "inventory_quantity": 100,
                    "old_inventory_quantity": 10
                  }
                }
             */

            string uri = "admin/variants/" + variantID.ToString() + ".json";
            String JsonString = @"{""variant"": {
                        ""id"": 808950810,
                        ""inventory_quantity"": **NEWQTY**
                      }
                }";
            //""old_inventory_quantity"": **OLDQTY**

            JsonString = JsonString.Replace(" ", "").Replace("\n", "").Replace("\r", "");
            JsonString = JsonString.Replace("**NEWQTY**", newQty.ToString()).Replace("**OLDQTY**", oldQty.ToString());

            string apiResult = JsonHelpers.PUTtoShopify(uri, JsonString);
            ProductVariantSingle variant = new ProductVariantSingle();
            try
            {
                variant = JsonConvert.DeserializeObject<ProductVariantSingle>(apiResult);
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                string msg = "error in updating variant inventory" + Environment.NewLine;
                msg += "jsonURL: " + uri + Environment.NewLine;
                msg += "jsonString: " + JsonString + Environment.NewLine;
                msg += "response: " + apiResult + Environment.NewLine;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }
        }

        public static ProductVariant GetInventory(long variantID)
        {
            /*
            PUT /admin/variants/#{id}.json
                {
                  "variant": {
                    "id": 808950810,
                    "inventory_quantity": 100,
                    "old_inventory_quantity": 10
                  }
                }
             */

            string uri = "admin/variants/" + variantID.ToString() + ".json";

            string apiResult = JsonHelpers.GETfromShopify(uri);
            ProductVariantSingle variant = new ProductVariantSingle();
            try
            {
                variant = JsonConvert.DeserializeObject<ProductVariantSingle>(apiResult);
            }
            catch (Exception ex)
            {
                //put some exception handling code here
                string msg = "error in updating variant inventory" + Environment.NewLine;
                msg += "jsonURL: " + uri + Environment.NewLine;
                //msg += "jsonString: " + JsonString + Environment.NewLine;
                msg += "response: " + apiResult + Environment.NewLine;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            return variant.variant;
        }
}

    public class ProductOption
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonProperty("product_id")]
        public long? Product_id { get; set; }

        public ProductOption() 
        {
            //Id = 0;
            Name = "";
            Position = 1;
            //Product_id = 0;
        }

    }

    public class ProductImage
    {
        [JsonProperty("created_at")]
        public DateTime? Created_at { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonProperty("product_id")]
        public long? Product_id { get; set; }

        [JsonProperty("updated_at")]
        public DateTime? Updated_at { get; set; }

        [JsonProperty("src")]
        public string Src { get; set; }

        public ProductImage() 
        {
            //Created_at = Convert.ToDateTime("1/1/1900");
            //Id = 0;
            Position = 1;
            //Product_id = 0;
            //Updated_at = Convert.ToDateTime("1/1/1900");
            Src = "";
        }

    }
       
    public class ShopifyProduct
    {
        [JsonProperty("body_html")]
        public string Body_html { get; set; }

        [JsonProperty("created_at")]
        public DateTime? Created_at { get; set; }

        [JsonProperty("handle")]
        public string Handle { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("product_type")]
        public string Product_type { get; set; }

        [JsonProperty("published_at")]
        public DateTime? Published_at { get; set; }

        [JsonProperty("published_scope")]
        public string Published_scope { get; set; }

        [JsonProperty("template_suffix")]
        public string Template_suffix { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("updated_at")]
        public DateTime? Updated_at { get; set; }

        [JsonProperty("vendor")]
        public string Vendor { get; set; }

        [JsonProperty("tags")]
        public string Tags { get; set; }

        [JsonProperty("variants")]
        public ProductVariant[] variants;

        [JsonProperty("options")]
        public ProductOption[] options;

        [JsonProperty("images")]
        public ProductImage[] images;

        [JsonProperty("image")]
        public ProductImage image;

        public ShopifyProduct()
        {
            Body_html = "";
            //Created_at = Convert.ToDateTime("1/1/1900");
            Handle = "";
            //Id = 0;
            Product_type = "";
            //Published_at = Convert.ToDateTime("1/1/1900");
            //Published_scope = "";
            //Template_suffix = "";
            Title = "";
            //Updated_at = Convert.ToDateTime("1/1/1900");
            Vendor = "FaceMaster";
            Tags = "";

            variants = new ProductVariant[] {};
            options = new ProductOption[] {};
            images = new ProductImage[] {};
            image = new ProductImage();
        }
    }
}