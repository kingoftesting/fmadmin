﻿using System;
using Newtonsoft.Json;

namespace ShopifyAgent
{
    public class ShopifyRefunds
    {
        [JsonProperty("refunds")]
        public ShopifyRefund[] Refunds;
    }

    public class ShopifyRefundLineItem
    {
        [JsonProperty("id")]
        public Int64 Id { get; set; }

        [JsonProperty("line_item_id")]
        public Int64 Line_item_id { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("line_item")]
        public ShopifyLineItem Line_item;
    }

    public class ShopifyRefund
    {
        public int ShopifyTransactionID { get; set; }
        public int ShopifyOrderID { get; set; }
        public DateTime ShopifyImportDate { get; set; }

        [JsonProperty("created_at")]
        public DateTime? Created_at { get; set; }

        [JsonProperty("id")]
        public Int64 Id { get; set; }

        [JsonProperty("note")]
        public string note { get; set; }

        [JsonProperty("order_id")]
        public Int64 Order_id { get; set; }

        [JsonProperty("restock")]
        public bool? Restock { get; set; }

        [JsonProperty("user_id")]
        public Int64? User_id { get; set; }

        [JsonProperty("refund_line_items")]
        public ShopifyRefundLineItem[] Refund_line_items;

        [JsonProperty("transactions")]
        public ShopifyTransaction[] Transactions;

        public ShopifyRefund()
        {
            ShopifyTransactionID = 0;
            ShopifyOrderID = 0;
            ShopifyImportDate = DateTime.MinValue;
            Created_at = DateTime.MinValue;
            Id = 0;
            note = string.Empty;
            Order_id = 0;
            Restock = false;
            User_id = 0;
            Refund_line_items = new ShopifyRefundLineItem[] { };
            Transactions = new ShopifyTransaction[] { };
        }

    }
}