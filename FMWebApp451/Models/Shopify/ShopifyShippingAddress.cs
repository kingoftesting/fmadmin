﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;
using System.Text;
using System.Data;

using System.Data.SqlClient;
using System.Collections.Generic;
using FM2015.Helpers;
using System.Configuration;

/// <summary>
/// Summary description for ShopifyShippingAddress
/// </summary>
public class ShopifyShippingAddress
{
    //Built with ClassHatch Version Build: 4.0.7  9/8/2013.  Template file:ExtendedClass.cs.tem

    #region Fields
    private int shopifyShippingAddressID;
    private int shopifyOrderID;
    private string address1;
    private string address2;
    private string city;
    private string company;
    private string country;
    private string first_name;
    private string last_name;
    private Decimal? latitude;
    private Decimal? longitude;
    private string phone;
    private string province;
    private string zip;
    private string name;
    private string country_code;
    private string province_code;
    #endregion


    #region Properties
    //[JsonProperty("shopifyShippingAddressID")]
    public int ShopifyShippingAddressID { get { return shopifyShippingAddressID; } set { shopifyShippingAddressID = value; } }
    public int ShopifyOrderID { get { return shopifyOrderID; } set { shopifyOrderID = value; } }

    [JsonProperty("address1")]
    public string Address1 { get { return address1; } set { address1 = value; } }
    [JsonProperty("address2")]
    public string Address2 { get { return address2; } set { address2 = value; } }
    [JsonProperty("city")]
    public string City { get { return city; } set { city = value; } }
    [JsonProperty("company")]
    public string Company { get { return company; } set { company = value; } }
    [JsonProperty("country")]
    public string Country { get { return country; } set { country = value; } }
    [JsonProperty("first_name")]
    public string First_name { get { return first_name; } set { first_name = value; } }
    [JsonProperty("last_name")]
    public string Last_name { get { return last_name; } set { last_name = value; } }
    [JsonProperty("latitude")]
    public Decimal? Latitude { get { return latitude; } set { latitude = value; } }
    [JsonProperty("longitude")]
    public Decimal? Longitude { get { return longitude; } set { longitude = value; } }
    [JsonProperty("phone")]
    public string Phone { get { return phone; } set { phone = value; } }
    [JsonProperty("province")]
    public string Province { get { return province; } set { province = value; } }
    [JsonProperty("zip")]
    public string Zip { get { return zip; } set { zip = value; } }
    [JsonProperty("name")]
    public string Name { get { return name; } set { name = value; } }
    [JsonProperty("country_code")]
    public string Country_code { get { return country_code; } set { country_code = value; } }
    [JsonProperty("province_code")]
    public string Province_code { get { return province_code; } set { province_code = value; } }
   
    
    #endregion


    #region Constructors
    public ShopifyShippingAddress()
    {
        //sets reasonable initial values
        shopifyShippingAddressID = 0;
        shopifyOrderID = 0;
        address1 = "";
        address2 = "";
        city = "";
        company = "";
        country = "";
        first_name = "";
        last_name = "";
        latitude = 0;
        longitude = 0;
        phone = "";
        province = "";
        zip = "";
        name = "";
        country_code = "";
        province_code = "";
    }

    public ShopifyShippingAddress(int shopifyOrderID)
    {
        //Loads a single record from a table
        string sql = @"
        SELECT *
        FROM ShopifyShippingAddresses
        WHERE ShopifyOrderID = @ShopifyOrderID";
        SqlParameter[] parms = new SqlParameter[1];
        parms[0] = new SqlParameter("@ShopifyOrderID", shopifyOrderID);

        string shopConnStr = AdminCart.Config.ConnStrShopify();
        SqlDataReader dr = DBUtil.FillDataReader(sql, parms, shopConnStr);

        while (dr.Read())
        {
            this.shopifyShippingAddressID = Convert.ToInt32(dr["shopifyShippingAddressID"].ToString());
            this.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            this.address1 = Convert.ToString(dr["address1"].ToString());
            this.address2 = Convert.ToString(dr["address2"].ToString());
            this.city = Convert.ToString(dr["city"].ToString());
            this.company = Convert.ToString(dr["company"].ToString());
            this.country = Convert.ToString(dr["country"].ToString());
            this.first_name = Convert.ToString(dr["first_name"].ToString());
            this.last_name = Convert.ToString(dr["last_name"].ToString());
            this.latitude = (dr["latitude"] == DBNull.Value) ? 0 : Convert.ToDecimal(dr["latitude"].ToString());
            this.longitude = (dr["longitude"] == DBNull.Value) ? 0 : Convert.ToDecimal(dr["longitude"].ToString());
            this.phone = Convert.ToString(dr["phone"].ToString());
            this.province = Convert.ToString(dr["province"].ToString());
            this.zip = Convert.ToString(dr["zip"].ToString());
            this.name = Convert.ToString(dr["name"].ToString());
            this.country_code = Convert.ToString(dr["country_code"].ToString());
            this.province_code = Convert.ToString(dr["province_code"].ToString());
        }
        dr.Close();
    }

    #endregion


    #region Methods
    //requires DBUtil class from classhatch.com
    public int SaveDB()
    {
        //Insert or Update record on SQL Table
        SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrShopify());

        string sql = "";


        if (ShopifyShippingAddressID == 0)
        {
            //Insert if primary key is default value of zero
            sql = @"
            INSERT ShopifyShippingAddresses(
--ShopifyShippingAddressID,
ShopifyOrderID,
address1,
address2,
city,
company,
country,
first_name,
last_name,
latitude,
longitude,
phone,
province,
zip,
name,
country_code,
province_code

             )VALUES(
--@ShopifyShippingAddressID,
@ShopifyOrderID,
@address1,
@address2,
@city,
@company,
@country,
@first_name,
@last_name,
@latitude,
@longitude,
@phone,
@province,
@zip,
@name,
@country_code,
@province_code

            ); select id=scope_identity();";
        }
        else
        {
            //Update db record if primary key is not default value of zero
            sql = @"
            UPDATE ShopifyShippingAddresses
            SET 
--ShopifyShippingAddressID= @ShopifyShippingAddressID,
ShopifyOrderID= @ShopifyOrderID,
address1= @address1,
address2= @address2,
city= @city,
company= @company,
country= @country,
first_name= @first_name,
last_name= @last_name,
latitude= @latitude,
longitude= @longitude,
phone= @phone,
province= @province,
zip= @zip,
name= @name,
country_code= @country_code,
province_code= @province_code

            WHERE ShopifyShippingAddressID = PrimaryKeyFieldValue; select id=@ShopifyShippingAddressID;";
        }
        SqlCommand cmd = new SqlCommand(sql, conn);

        SqlParameter shopifyShippingAddressIDParam = new SqlParameter("@shopifyShippingAddressID", SqlDbType.Int);
        SqlParameter shopifyOrderIDParam = new SqlParameter("@shopifyOrderID", SqlDbType.Int);
        SqlParameter address1Param = new SqlParameter("@address1", SqlDbType.VarChar);
        SqlParameter address2Param = new SqlParameter("@address2", SqlDbType.VarChar);
        SqlParameter cityParam = new SqlParameter("@city", SqlDbType.VarChar);
        SqlParameter companyParam = new SqlParameter("@company", SqlDbType.VarChar);
        SqlParameter countryParam = new SqlParameter("@country", SqlDbType.VarChar);
        SqlParameter first_nameParam = new SqlParameter("@first_name", SqlDbType.VarChar);
        SqlParameter last_nameParam = new SqlParameter("@last_name", SqlDbType.VarChar);
        SqlParameter latitudeParam = new SqlParameter("@latitude", SqlDbType.Decimal);
        SqlParameter longitudeParam = new SqlParameter("@longitude", SqlDbType.Decimal);
        SqlParameter phoneParam = new SqlParameter("@phone", SqlDbType.VarChar);
        SqlParameter provinceParam = new SqlParameter("@province", SqlDbType.VarChar);
        SqlParameter zipParam = new SqlParameter("@zip", SqlDbType.VarChar);
        SqlParameter nameParam = new SqlParameter("@name", SqlDbType.VarChar);
        SqlParameter country_codeParam = new SqlParameter("@country_code", SqlDbType.VarChar);
        SqlParameter province_codeParam = new SqlParameter("@province_code", SqlDbType.VarChar);

        shopifyShippingAddressIDParam.Value = shopifyShippingAddressID;
        shopifyOrderIDParam.Value = shopifyOrderID;
        address1Param.Value = (object)address1 ?? DBNull.Value;
        address2Param.Value = (object)address2 ?? DBNull.Value;
        cityParam.Value = (object)city ?? DBNull.Value;
        companyParam.Value = (object)company ?? DBNull.Value;
        countryParam.Value = (object)country ?? DBNull.Value;
        first_nameParam.Value = (object)first_name ?? DBNull.Value;
        last_nameParam.Value = (object)last_name ?? DBNull.Value;
        latitudeParam.Value = (object)latitude ?? DBNull.Value;
        longitudeParam.Value = (object)longitude ?? DBNull.Value;
        phoneParam.Value = (object)phone ?? DBNull.Value;
        provinceParam.Value = (object)province ?? DBNull.Value;
        zipParam.Value = (object)zip ?? DBNull.Value;
        nameParam.Value = (object)name ?? DBNull.Value;
        country_codeParam.Value = (object)country_code ?? DBNull.Value;
        province_codeParam.Value = (object) province_code ?? DBNull.Value; 

        cmd.Parameters.Add(shopifyShippingAddressIDParam);
        cmd.Parameters.Add(shopifyOrderIDParam);
        cmd.Parameters.Add(address1Param);
        cmd.Parameters.Add(address2Param);
        cmd.Parameters.Add(cityParam);
        cmd.Parameters.Add(companyParam);
        cmd.Parameters.Add(countryParam);
        cmd.Parameters.Add(first_nameParam);
        cmd.Parameters.Add(last_nameParam);
        cmd.Parameters.Add(latitudeParam);
        cmd.Parameters.Add(longitudeParam);
        cmd.Parameters.Add(phoneParam);
        cmd.Parameters.Add(provinceParam);
        cmd.Parameters.Add(zipParam);
        cmd.Parameters.Add(nameParam);
        cmd.Parameters.Add(country_codeParam);
        cmd.Parameters.Add(province_codeParam);

        conn.Open();
        int result = Convert.ToInt32(cmd.ExecuteScalar().ToString());
        conn.Close();
        return result;
    }

    static public List<ShopifyShippingAddress> GetShopifyShippingAddressList()
    {
        string conn = AdminCart.Config.ConnStrShopify();
        List<ShopifyShippingAddress> thelist = new List<ShopifyShippingAddress>();

        string sql = "SELECT * FROM ShopifyShippingAddresses";

        SqlDataReader dr = DBUtil.FillDataReader(sql, conn);

        while (dr.Read())
        {
            ShopifyShippingAddress obj = new ShopifyShippingAddress();

            obj.shopifyShippingAddressID = Convert.ToInt32(dr["shopifyShippingAddressID"].ToString());
            obj.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            obj.address1 = Convert.ToString(dr["address1"].ToString());
            obj.address2 = Convert.ToString(dr["address2"].ToString());
            obj.city = Convert.ToString(dr["city"].ToString());
            obj.company = Convert.ToString(dr["company"].ToString());
            obj.country = Convert.ToString(dr["country"].ToString());
            obj.first_name = Convert.ToString(dr["first_name"].ToString());
            obj.last_name = Convert.ToString(dr["last_name"].ToString());
            obj.latitude = (dr["latitude"] == DBNull.Value) ? 0 : Convert.ToDecimal(dr["latitude"].ToString());
            obj.longitude = (dr["longitude"] == DBNull.Value) ? 0 : Convert.ToDecimal(dr["longitude"].ToString());
            obj.phone = Convert.ToString(dr["phone"].ToString());
            obj.province = Convert.ToString(dr["province"].ToString());
            obj.zip = Convert.ToString(dr["zip"].ToString());
            obj.name = Convert.ToString(dr["name"].ToString());
            obj.country_code = Convert.ToString(dr["country_code"].ToString());
            obj.province_code = Convert.ToString(dr["province_code"].ToString());

            thelist.Add(obj);
        }
        dr.Close();
        return thelist;

    }

    #endregion


    #region Validation
    //Requires Validation class from classhatch.com
    static public bool IsValid(ShopifyShippingAddress obj)
    {
        bool isValid = false;

        if (GetErrors(obj).Count == 0)
        {
            isValid = true;
        }

        return isValid;
    }


    static public List<ValidationError> GetErrors(ShopifyShippingAddress obj)
    {
        List<ValidationError> errorlist = new List<ValidationError>();

        /*
        if (ValidateUtil.IsTooLong(20, obj.FirstName))
        {
            ValidationError e = new ValidationError();
            e.Field = "First Name";
            e.ErrorDescription = "20 characters maximium are allowed";
            errorlist.Add(e);
        };
        */

        return errorlist;
    }
    #endregion


    #region OverRides

    #endregion

}
