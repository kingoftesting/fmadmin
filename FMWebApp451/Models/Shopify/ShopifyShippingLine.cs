﻿using System;
using System.Text;
using System.Data;
using System.Configuration;

using System.Data.SqlClient;
using System.Collections.Generic;

using Newtonsoft.Json;
using FM2015.Helpers;

/// <summary>
/// Summary description for ShopifyShippingLine
/// </summary>
public class ShopifyShippingLine
{
    //Built with ClassHatch Version Build: 4.0.7  9/8/2013.  Template file:ExtendedClass.cs.tem

    #region Fields
    private int shopifyShippingLineID;
    private int shopifyOrderID;
    private string code;
    private Decimal price;
    private string source;
    private string title;
    #endregion


    #region Properties
    public int ShopifyShippingLineID { get { return shopifyShippingLineID; } set { shopifyShippingLineID = value; } }
    public int ShopifyOrderID { get { return shopifyOrderID; } set { shopifyOrderID = value; } }


    [JsonProperty("code")]
    public string Code { get { return code; } set { code = value; } }


    [JsonProperty("price")]
    public Decimal Price { get { return price; } set { price = value; } }


    [JsonProperty("source")]
    public string Source { get { return source; } set { source = value; } }

    [JsonProperty("title")]
    public string Title { get { return title; } set { title = value; } }
    #endregion


    #region Constructors
    public ShopifyShippingLine()
    {
        //sets reasonable initial values
        shopifyShippingLineID = 0;
        shopifyOrderID = 0;
        code = "";
        price = 0;
        source = "";
        title = "";
    }

    public ShopifyShippingLine(int shopifyShippingLineID)
    {
        //Loads a single record from a table
        string sql = @"
        SELECT *
        FROM ShopifyShippingLines
        WHERE ShopifyShippingLineID = @ShopifyShippingLineID";
        SqlParameter[] parms = new SqlParameter[1];
        parms[0] = new SqlParameter("@PrimaryKeyValue", shopifyShippingLineID);

        string shopConnStr = AdminCart.Config.ConnStrShopify();
        SqlDataReader dr = DBUtil.FillDataReader(sql, parms, shopConnStr);

        while (dr.Read())
        {
            this.shopifyShippingLineID = Convert.ToInt32(dr["shopifyShippingLineID"].ToString());
            this.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            this.code = Convert.ToString(dr["code"].ToString());
            this.price = Convert.ToDecimal(dr["price"].ToString());
            this.source = Convert.ToString(dr["source"].ToString());
            this.title = Convert.ToString(dr["title"].ToString());
        }
        dr.Close();
    }

    public ShopifyShippingLine(int shopifyOrderID, long shopId)
    {
        //Loads a single record from a table
        string sql = @"
        SELECT * 
        FROM ShopifyShippingLines 
        WHERE ShopifyOrderID = @ShopifyOrderID";
        SqlParameter[] parms = new SqlParameter[1];
        parms[0] = new SqlParameter("@ShopifyOrderID", shopifyOrderID);

        string shopConnStr = AdminCart.Config.ConnStrShopify();
        SqlDataReader dr = DBUtil.FillDataReader(sql, parms, shopConnStr);

        while (dr.Read())
        {
            this.shopifyShippingLineID = Convert.ToInt32(dr["shopifyShippingLineID"].ToString());
            this.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            this.code = Convert.ToString(dr["code"].ToString());
            this.price = Convert.ToDecimal(dr["price"].ToString());
            this.source = Convert.ToString(dr["source"].ToString());
            this.title = Convert.ToString(dr["title"].ToString());
        }
        dr.Close();
    }

    #endregion


    #region Methods
    //requires DBUtil class from classhatch.com
    public int SaveDB()
    {
        //Insert or Update record on SQL Table
        SqlConnection conn = new SqlConnection(AdminCart.Config.ConnStrShopify());

        string sql = "";


        if (ShopifyShippingLineID == 0)
        {
            //Insert if primary key is default value of zero
            sql = @"
            INSERT ShopifyShippingLines(
--ShopifyShippingLineID,
ShopifyOrderID,
code,
price,
source,
title

             )VALUES(
--@ShopifyShippingLineID,
@ShopifyOrderID,
@code,
@price,
@source,
@title

            ); select id=scope_identity();";
        }
        else
        {
            //Update db record if primary key is not default value of zero
            sql = @"
            UPDATE ShopifyShippingLines
            SET 
--ShopifyShippingLineID= @ShopifyShippingLineID,
ShopifyOrderID= @ShopifyOrderID,
code= @code,
price= @price,
source= @source,
title= @title

            WHERE ShopifyShippingLineID = PrimaryKeyFieldValue; select id=@ShopifyShippingLineID;";
        }
        
        
        SqlCommand cmd = new SqlCommand(sql, conn);

        SqlParameter shopifyShippingLineIDParam = new SqlParameter("@shopifyShippingLineID", SqlDbType.Int);
        SqlParameter shopifyOrderIDParam = new SqlParameter("@shopifyOrderID", SqlDbType.Int);
        SqlParameter codeParam = new SqlParameter("@code", SqlDbType.VarChar);
        SqlParameter priceParam = new SqlParameter("@price", SqlDbType.Money);
        SqlParameter sourceParam = new SqlParameter("@source", SqlDbType.VarChar);
        SqlParameter titleParam = new SqlParameter("@title", SqlDbType.VarChar);

        shopifyShippingLineIDParam.Value = shopifyShippingLineID;
        shopifyOrderIDParam.Value = shopifyOrderID;
        codeParam.Value = code;
        priceParam.Value = price;
        sourceParam.Value = (object)source ?? DBNull.Value;
        titleParam.Value = title;

        cmd.Parameters.Add(shopifyShippingLineIDParam);
        cmd.Parameters.Add(shopifyOrderIDParam);
        cmd.Parameters.Add(codeParam);
        cmd.Parameters.Add(priceParam);
        cmd.Parameters.Add(sourceParam);
        cmd.Parameters.Add(titleParam);

        conn.Open();
        int result = Convert.ToInt32(cmd.ExecuteScalar().ToString());
        conn.Close();
        return result;
    }

    static public List<ShopifyShippingLine> GetShopifyShippingLineList()
    {
        string conn = AdminCart.Config.ConnStrShopify();
        List<ShopifyShippingLine> thelist = new List<ShopifyShippingLine>();

        string sql = "SELECT * FROM ShopifyShippingLines";

        SqlDataReader dr = DBUtil.FillDataReader(sql, conn);

        while (dr.Read())
        {
            ShopifyShippingLine obj = new ShopifyShippingLine();

            obj.shopifyShippingLineID = Convert.ToInt32(dr["shopifyShippingLineID"].ToString());
            obj.shopifyOrderID = Convert.ToInt32(dr["shopifyOrderID"].ToString());
            obj.code = Convert.ToString(dr["code"].ToString());
            obj.price = Convert.ToDecimal(dr["price"].ToString());
            obj.source = Convert.ToString(dr["source"].ToString());
            obj.title = Convert.ToString(dr["title"].ToString());

            thelist.Add(obj);
        }
        dr.Close();
        return thelist;

    }

    
    #endregion


    #region Validation
    //Requires Validation class from classhatch.com
   


   
    #endregion


    #region OverRides

    #endregion

}


