﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using FM2015.Helpers;
using Newtonsoft.Json;


namespace ShopifyAgent
{
    public class ShopifyTransactions
    {
        [JsonProperty("transactions")]
        public ShopifyTransaction[] Transactions;
    }

    public class ShopifyTransactionSingle
    {
        [JsonProperty("transaction")]
        public ShopifyTransaction transaction { get; set; }

        public ShopifyTransactionSingle()
        {
            transaction = new ShopifyTransaction();
        }
    }

    public class ShopifyReceipt
    {
        [JsonProperty("testcase")]
        public bool Testcase { get; set; }

        [JsonProperty("authorization")]
        public string Authorization { get; set; }
    }

    public class ShopifyPayment_Details
    {
        [JsonProperty("avs_result_code")]
        public string Avs_result_code { get; set; }

        [JsonProperty("credit_card_bin")]
        public string Credit_card_bin { get; set; }

        [JsonProperty("cvv_result_code")]
        public string Cvv_result_code { get; set; }

        [JsonProperty("credit_card_number")]
        public string Credit_card_number { get; set; }

        [JsonProperty("credit_card_company")]
        public string Credit_card_company { get; set; }

        public ShopifyPayment_Details()
        {
            Avs_result_code = "n/a";
            Credit_card_bin = "n/a";
            Credit_card_number = "n/a";
            Credit_card_company = "n/a";
        }
    }

    public class ShopifyTransaction
    {
        public int ShopifyTransactionID { get; set; }
        public int ShopifyOrderID { get; set; }
        public DateTime ShopifyImportDate { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("authorization")]
        public string Authorization_key { get; set; }

        [JsonProperty("created_at")]
        public DateTime? Created_at { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("gateway")]
        public string Gateway { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("location_id")]
        public string Location_id { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("order_id")]
        public long Order_id { get; set; }

        [JsonProperty("parent_id")]
        public string Parent_id { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("test")]
        public bool Test { get; set; }

        [JsonProperty("user_id")]
        public string User_id { get; set; }

        [JsonProperty("device_id")]
        public string Device_id { get; set; }

        [JsonProperty("receipt")]
        ShopifyReceipt Receipt;

        [JsonProperty("error_code")]
        public string Error_code { get; set; }

        [JsonProperty("source_name")]
        public string Source_name { get; set; }

        [JsonProperty("payment_details")]
        public ShopifyPayment_Details Payment_Details;

        public ShopifyTransaction()
        {
            ShopifyTransactionID = 0;
            ShopifyOrderID = 0;
            ShopifyImportDate = Convert.ToDateTime("1/1/1900");
            Amount = 0;
            Authorization_key = "n/a";
            Created_at = Convert.ToDateTime("1/1/1900");
            Currency = "n/a";
            Gateway = "n/a";
            Id = 0;
            Kind = "n/a";
            Location_id = "0";
            Message = "n/a";
            Order_id = 0;
            Parent_id = "0";
            Status = "n/a";
            Test = false;
            User_id = "0";
            Device_id = "0";
            Error_code = "n/a";
            Source_name = "n/a";

            Receipt = new ShopifyReceipt();
            Payment_Details = new ShopifyPayment_Details();

        }

/***********************************
* Static methods
************************************/

        /// <summary>
        /// Returns a collection with all ShopifyTransactions
        /// </summary>
        public static List<ShopifyTransaction> GetShopifyTransactionList(int ShopifyOrderID)
        {
            string conn = AdminCart.Config.ConnStrShopify();
            List<ShopifyTransaction> thelist = new List<ShopifyTransaction>();

            string sql = @" 
            SELECT * FROM ShopifyTransactions 
            WHERE ShopifyOrderID = @ShopifyOrderID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, ShopifyOrderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, conn);

            while (dr.Read())
            {
                ShopifyTransaction obj = new ShopifyTransaction();

                obj.ShopifyTransactionID = Convert.ToInt32(dr["ShopifyTransactionID"].ToString());
                obj.ShopifyOrderID = Convert.ToInt32(dr["ShopifyOrderID"].ToString());
                obj.ShopifyImportDate = Convert.ToDateTime(dr["ShopifyImportDate"].ToString());
                obj.Amount = Convert.ToDecimal(dr["Amount"].ToString());
                obj.Authorization_key = dr["Authorization_key"].ToString();
                obj.Created_at = Convert.ToDateTime(dr["Created_at"].ToString());
                obj.Currency = dr["Currency"].ToString();
                obj.Gateway = dr["Gateway"].ToString();
                obj.Id = (dr["Id"] == DBNull.Value) ? 0 : Convert.ToInt64(dr["Id"]);
                obj.Kind = dr["Kind"].ToString();
                obj.Location_id = (dr["Location_id"] == DBNull.Value) ? "null" : dr["Location_id"].ToString();
                obj.Message = dr["Message"].ToString();
                obj.Order_id = (dr["Order_id"] == DBNull.Value) ? 0 : Convert.ToInt64(dr["Order_id"]);
                obj.Parent_id = (dr["Parent_id"] == DBNull.Value) ? "null" : dr["Parent_id"].ToString();
                obj.Status = dr["Status"].ToString();
                obj.Test = Convert.ToBoolean(dr["Test"].ToString());
                obj.User_id = (dr["User_id"] == DBNull.Value) ? "null" : dr["User_id"].ToString();
                obj.Device_id = (dr["device_id"] == DBNull.Value) ? "null" : dr["Device_id"].ToString();
                obj.Receipt.Testcase = Convert.ToBoolean(dr["Testcase"].ToString());
                obj.Receipt.Authorization = dr["Authorization"].ToString();
                obj.Error_code = dr["Error_code"].ToString();
                obj.Source_name = dr["Source_name"].ToString();
                obj.Payment_Details.Avs_result_code = dr["Avs_result_code"].ToString();
                obj.Payment_Details.Credit_card_bin = dr["Credit_card_bin"].ToString();
                obj.Payment_Details.Cvv_result_code = dr["Cvv_result_code"].ToString();
                obj.Payment_Details.Credit_card_number = dr["Credit_card_number"].ToString();
                obj.Payment_Details.Credit_card_company = dr["Credit_card_company"].ToString();

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a className object with the specified ID
        /// </summary>

        public static ShopifyTransaction GetShopifyTransactionByID(int id)
        {
            string conn = AdminCart.Config.ConnStrShopify();
            ShopifyTransaction obj = new ShopifyTransaction();

            string sql = @" 
            SELECT * FROM  ShopifyTransactions 
            WHERE ShopifyTransactionID = @ID 
            ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, id);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, conn);

            if (dr.Read())
            {
                obj.ShopifyTransactionID = Convert.ToInt32(dr["ShopifyTransactionID"].ToString());
                obj.ShopifyOrderID = (dr["ShopifyOrderID"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["ShopifyOrderID"].ToString());
                obj.ShopifyImportDate = Convert.ToDateTime(dr["ShopifyImportDate"].ToString());
                obj.Amount = Convert.ToDecimal(dr["Amount"].ToString());
                obj.Authorization_key = dr["Authorization_key"].ToString();
                obj.Created_at = Convert.ToDateTime(dr["Created_at"].ToString());
                obj.Currency = dr["Currency"].ToString();
                obj.Gateway = dr["Gateway"].ToString();
                obj.Id = (dr["Id"] == DBNull.Value) ? 0 : Convert.ToInt64(dr["Id"]);
                obj.Kind = dr["Kind"].ToString();
                obj.Location_id = (dr["Location_id"] == DBNull.Value) ? "null" : dr["Location_id"].ToString();
                obj.Message = dr["Message"].ToString();
                obj.Order_id = (dr["Order_id"] == DBNull.Value) ? 0 : Convert.ToInt64(dr["Order_id"]);
                obj.Parent_id = (dr["Parent_id"] == DBNull.Value) ? "null" : dr["Parent_id"].ToString();
                obj.Status = dr["Status"].ToString();
                obj.Test = Convert.ToBoolean(dr["Test"].ToString());
                obj.User_id = (dr["User_id"] == DBNull.Value) ? "null" : dr["User_id"].ToString();
                obj.Device_id = (dr["Device_id"] == DBNull.Value) ? "null" : dr["Device_id"].ToString();
                obj.Receipt.Testcase = Convert.ToBoolean(dr["Testcase"].ToString());
                obj.Receipt.Authorization = dr["Authorization"].ToString();
                obj.Error_code = dr["Error_code"].ToString();
                obj.Source_name = dr["Source_name"].ToString();
                obj.Payment_Details.Avs_result_code = dr["Avs_result_code"].ToString();
                obj.Payment_Details.Credit_card_bin = dr["Credit_card_bin"].ToString();
                obj.Payment_Details.Cvv_result_code = dr["Cvv_result_code"].ToString();
                obj.Payment_Details.Credit_card_number = dr["Credit_card_number"].ToString();
                obj.Payment_Details.Credit_card_company = dr["Credit_card_company"].ToString();
            }
            if (dr != null) { dr.Close(); }

            return obj;
        }

        /// <summary>
        /// Updates an existing ShopifyTransactions
        /// </summary>
        public static bool UpdateShopifyTransaction(ShopifyTransaction tran)
        {
            if (tran.Created_at == null) { tran.Created_at = DateTime.MinValue; }
            string conn = AdminCart.Config.ConnStrShopify();
            SqlParameter[] mySqlParameters = null;
            string sql = @" 
                UPDATE ShopifyTransactions 
                SET  
                    ShopifyOrderID = @ShopifyOrderID,  
                    ShopifyImportDate = @ShopifyImportDate,  
                    Amount = @Amount,  
                    Authorization_key = @Authorization_key,  
                    Created_at = @Created_at,  
                    Currency = @Currency,  
                    Gateway = @Gateway,  
                    Id = @Id,  
                    Kind = @Kind,  
                    Location_id = @Location_id,  
                    Message = @Message,  
                    Order_id = @Order_id,  
                    Parent_id = @Parent_id,  
                    Status = @Status,  
                    Test = @Test,  
                    User_id = @User_id,  
                    Device_id = @Device_id,  
                    Testcase = @Testcase,  
                    Authorization = @Authorization,  
                    Error_code = @Error_code,  
                    Source_name = @Source_name,  
                    Avs_result_code = @Avs_result_code,  
                    Credit_card_bin = @Credit_card_bin,  
                    Cvv_result_code = @Cvv_result_code,  
                    Credit_card_number = @Credit_card_number,  
                    Credit_card_company = @Credit_card_company  
                WHERE ShopifyTransactionID = @ID  
                ";

            mySqlParameters = DBUtil.BuildParametersFrom(sql, tran.ShopifyTransactionID,
                tran.ShopifyOrderID, tran.ShopifyImportDate, tran.Amount, tran.Authorization_key, tran.Created_at, tran.Currency,
                tran.Gateway, tran.Id, tran.Kind, tran.Location_id, tran.Message, tran.Order_id, tran.Parent_id, tran.Status,
                tran.Test, tran.User_id, tran.Device_id, tran.Receipt.Testcase, tran.Receipt.Authorization, tran.Error_code,
                tran.Source_name, tran.Payment_Details.Avs_result_code, tran.Payment_Details.Credit_card_bin,
                tran.Payment_Details.Cvv_result_code, tran.Payment_Details.Credit_card_number, tran.Payment_Details.Credit_card_company,
                tran.ShopifyTransactionID);

            DBUtil.Exec(sql, conn, mySqlParameters);
            return true;
        }

        /// <summary>
        /// Creates a new ShopifyTransactions
        /// </summary>
        public static void InsertShopifyTransaction(ShopifyTransaction tran)
        {
            string conn = AdminCart.Config.ConnStrShopify();
            SqlParameter[] mySqlParameters = null;

            string sql = @" 
                SET NOCOUNT ON 
                IF NOT EXISTS (Select Id From ShopifyTransactions Where Id = @ID)
                    INSERT INTO ShopifyTransactions 
                    (ShopifyOrderID, ShopifyImportDate, Amount, Authorization_key, Created_at, Currency, 
                    Gateway, Id, Kind, Location_id, [Message], Order_id, Parent_id, [Status], 
                    Test, User_id, Device_id, Testcase, [Authorization], Error_code, 
                    Source_name, Avs_result_code, Credit_card_bin, 
                    Cvv_result_code, Credit_card_number, Credit_card_company) 
                    VALUES
                    (@ShopifyOrderID, @ShopifyImportDate, @Amount, @Authorization_Key, @Created_at, @Currency, 
                    @Gateway, @ID2, @Kind, @Location_id, @Message, @Order_id, @Parent_id, @Status, 
                    @Test, @User_id, @Device_id, @Testcase, @Authorization, @Error_code, 
                    @Source_name, @Avs_result_code, @Credit_card_bin, 
                    @Cvv_result_code, @Credit_card_number, @Credit_card_company);   
            ";

            tran.Authorization_key = (string.IsNullOrEmpty(tran.Authorization_key)) ? "0" : tran.Authorization_key;
            tran.Location_id = (string.IsNullOrEmpty(tran.Location_id)) ? "0" : tran.Location_id;
            tran.Message = (string.IsNullOrEmpty(tran.Message)) ? "n/a" : tran.Message;
            tran.Parent_id = (string.IsNullOrEmpty(tran.Parent_id)) ? "0" : tran.Parent_id;
            tran.User_id = (string.IsNullOrEmpty(tran.User_id)) ? "0" : tran.User_id;
            tran.Device_id = (string.IsNullOrEmpty(tran.Device_id)) ? "0" : tran.Device_id;
            tran.Source_name = (string.IsNullOrEmpty(tran.Source_name)) ? "0" : tran.Source_name;
            tran.Receipt.Authorization = (string.IsNullOrEmpty(tran.Receipt.Authorization)) ? "0" : tran.Receipt.Authorization;
            tran.Error_code = (string.IsNullOrEmpty(tran.Error_code)) ? "0" : tran.Error_code;
            tran.Payment_Details.Avs_result_code = (string.IsNullOrEmpty(tran.Payment_Details.Avs_result_code)) ? "n/a" : tran.Payment_Details.Avs_result_code;
            tran.Payment_Details.Cvv_result_code = (string.IsNullOrEmpty(tran.Payment_Details.Cvv_result_code)) ? "n/a" : tran.Payment_Details.Cvv_result_code;
            tran.Payment_Details.Credit_card_bin = (string.IsNullOrEmpty(tran.Payment_Details.Credit_card_bin)) ? "n/a" : tran.Payment_Details.Cvv_result_code;

            mySqlParameters = DBUtil.BuildParametersFrom(sql, tran.Id, 
                tran.ShopifyOrderID, tran.ShopifyImportDate, tran.Amount, tran.Authorization_key, tran.Created_at, tran.Currency,
                tran.Gateway, tran.Id, tran.Kind, tran.Location_id, tran.Message, tran.Order_id, tran.Parent_id, tran.Status,
                tran.Test, tran.User_id, tran.Device_id, tran.Receipt.Testcase, tran.Receipt.Authorization, tran.Error_code,
                tran.Source_name, tran.Payment_Details.Avs_result_code, tran.Payment_Details.Credit_card_bin,
                tran.Payment_Details.Cvv_result_code, tran.Payment_Details.Credit_card_number, tran.Payment_Details.Credit_card_company); 

            DBUtil.Exec(sql, conn, mySqlParameters);
        }

        public static ShopifyTransaction Refund(string shopifyName, decimal amount)
        {
            ShopifyTransaction shopTrans = new ShopifyTransaction();
            
            long ShopifyOrderID = ShopifyOrder.GetShopifyIDFromName(shopifyName, AdminCart.Config.CompanyID());
            ShopifyOrder o = new ShopifyOrder(ShopifyOrderID, AdminCart.Config.CompanyID());
            if (o.ShopifyOrderID > 0)
            {
                string jsonURL = "";
                string jsonString = "";
                string apiResult = "";
                jsonURL = "admin/orders/" + ShopifyOrderID.ToString() + "/transactions.json";
                jsonString = @"{""transaction"":{""amount"":""#AMOUNT#"",""kind"":""refund""}}";
                jsonString = jsonString.Replace("#AMOUNT#", amount.ToString("F"));
                apiResult = FMWebApp.Helpers.JsonHelpers.POSTtoShopify(jsonURL, jsonString);
                try
                {
                    shopTrans = JsonConvert.DeserializeObject<ShopifyTransactionSingle>(apiResult).transaction;
                }
                catch (Exception ex)
                {
                    //put some exception handling code here
                    string msg = "error in ShopifyTransaction: Refund (shopify_payments)" + Environment.NewLine;
                    msg += "jsonURL: " + jsonURL + Environment.NewLine;
                    msg += "jsonString: " + jsonString + Environment.NewLine;
                    msg += "response: " + apiResult + Environment.NewLine;
                    msg += "name: " + shopifyName + Environment.NewLine;
                    msg += "amount: " + amount.ToString("F") + Environment.NewLine;
                    FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                }
            }            
            return shopTrans;
        }

        private static string RefundCalc(ShopifyOrder o)
        {
            string apiResult = "";

            if (o.ShopifyOrderID > 0)
            {
                string jsonURL = "";
                string jsonString = "";

                jsonURL = "admin/orders/" + o.Id.ToString() + "/refunds/calculate.json";
                jsonString = @"
                                {
                                  ""refund"": {
                                    ""shipping"": {
                                        ""full_refund"": false
                                    },
                                    ""refund_line_items"": [
                                      {
                                        ""line_item_id"": 5625139713,
                                        ""quantity"": 1
                                      }
                                    ]
                                  }
                                }
                            ";

                jsonString = jsonString.Replace(" ", "").Replace("\n", "").Replace("\r", "");
                apiResult = FMWebApp.Helpers.JsonHelpers.POSTtoShopify(jsonURL, jsonString);
                
            }

            return apiResult;
        }

    }
}

