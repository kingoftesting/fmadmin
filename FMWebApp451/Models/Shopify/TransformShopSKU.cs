﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using FM2015.Helpers;


/// <summary>
/// Summary description for TransformShopSKU
/// </summary>
public class TransformShopSKU
{
    #region Private/Public Vars
    private int id;
    public int ID { get { return id; } set { id = value; } }
    private long shopProductID;
    public long ShopProductID { get { return shopProductID; } set { shopProductID = value; } }
    private string shopProductSKU;
    public string ShopProductSKU { get { return shopProductSKU; } set { shopProductSKU = value; } }
    private string shopProductName;
    public string ShopProductName { get { return shopProductName; } set { shopProductName = value; } }
    private decimal shopProductPrice;
    public decimal ShopProductPrice { get { return shopProductPrice; } set { shopProductPrice = value; } }
    private long transformShopProductID;
    public long TransformShopProductID { get { return transformShopProductID; } set { transformShopProductID = value; } }
    private long transformShopProductID2;
    public long TransformShopProductID2 { get { return transformShopProductID2; } set { transformShopProductID2 = value; } }

    #endregion

    public TransformShopSKU()
    {
        id = 0;
        shopProductID = 0;
        shopProductSKU = "n/a";
        shopProductName = "n/a";
        shopProductPrice = 0;
        transformShopProductID = 0;
        transformShopProductID2 = 0;
    }

    public TransformShopSKU(long sProductID)
    {
        id = 0;
        shopProductID = 0;
        shopProductSKU = "n/a";
        shopProductName = "n/a";
        shopProductPrice = 0;
        transformShopProductID = 0;
        transformShopProductID2 = 0;

        string sql = @" 
            SELECT * FROM  TransformShopSKU 
            WHERE ShopProductID = @ShopProductID 
        ";
        SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, sProductID);
        SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
        if (dr.Read())
        {
            id = Convert.ToInt32(dr["ID"]);

            long temp = 0;

            if (Int64.TryParse(dr["ShopProductID"].ToString(), out temp)) { ShopProductID = temp; }
            else { ShopProductID = 0; }

             
            shopProductSKU = dr["ShopProductSKU"].ToString();
            shopProductName = dr["ShopProductName"].ToString();
            shopProductPrice = Convert.ToDecimal(dr["ShopProdcutPrice"].ToString());

            if (Int64.TryParse(dr["TransformShopProductID"].ToString(), out temp)) { TransformShopProductID = temp; }
            else { transformShopProductID = 0; }

            if (Int64.TryParse(dr["TransformShopProductID2"].ToString(), out temp)) { TransformShopProductID2 = temp; }
            else { transformShopProductID2 = 0; }

        }
        if (dr != null) { dr.Close(); }
    }

    public int Save(int ID)
    {
        int result = 0;
        SqlParameter[] mySqlParameters = null;
        SqlDataReader dr = null;
        if (ID == 0)
        {
            string sql = @" 
                INSERT INTO TransformShopSKU (ShopProductID, ShopProductSKU, ShopProductName, ShopProdcutPrice, TransformShopProductID, TransformShopProductID2) 
                VALUES (@ShopProductID, @ShopProductSKU, @ShopProductName, @ShopProdcutPrice, @TransformShopProductID, @TransformShopProductID2); 
                SELECT ID=@@identity;  
            ";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, shopProductID, shopProductSKU, shopProductName, shopProductPrice, transformShopProductID, transformShopProductID2);
            dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                result = Convert.ToInt32(dr["ID"]);
            }
            else
            {
                result = 99;
            }
        }
        else
        {
            string sql = @" 
                UPDATE TransformShopSKU 
                SET  
                    ShopProductID = @ShopProductID,  
                    ShopProductSKU = @ShopProductSKU,  
                    ShopProductName = @ShopProductName,  
                    ShopProdcutPrice = @ShopProdcutPrice,
                    TransformShopProductID = @TransformShopProductID,  
                    TransformShopProductID2 = @TransformShopProductID2  
                WHERE ID = @ID  
                ";
            mySqlParameters = DBUtil.BuildParametersFrom(sql, shopProductID, shopProductSKU, shopProductName, shopProductPrice, transformShopProductID, transformShopProductID2, id);
            DBUtil.Exec(sql, mySqlParameters);
        }
        return result;
    }

    /***********************************
    * Static methods
    ************************************/

    /// <summary>
    /// Returns a collection with all TransformShopSKU
    /// </summary>
    public static List<TransformShopSKU> GetTransformShopSKUList()
    {
        List<TransformShopSKU> thelist = new List<TransformShopSKU>();

        string sql = @" 
            SELECT * FROM TransformShopSKU 
            ORDER BY ID ASC 
        ";

        SqlDataReader dr = DBUtil.FillDataReader(sql);

        while (dr.Read())
        {
            TransformShopSKU obj = new TransformShopSKU();

            obj.ID = Convert.ToInt32(dr["ID"]);

            long temp = 0;

            if (Int64.TryParse(dr["ShopProductID"].ToString(), out temp)) { obj.ShopProductID = temp; }
            else { obj.ShopProductID = 0; }

            obj.ShopProductSKU = dr["ShopProductSKU"].ToString();
            obj.ShopProductName = dr["ShopProductName"].ToString();
            obj.ShopProductPrice = Convert.ToDecimal(dr["ShopProductPrice"].ToString());

            if (Int64.TryParse(dr["TransformShopProductID"].ToString(), out temp)) { obj.TransformShopProductID = temp; }
            else { obj.TransformShopProductID = 0; }

            if (Int64.TryParse(dr["TransformShopProductID2"].ToString(), out temp)) { obj.TransformShopProductID2 = temp; }
            else { obj.TransformShopProductID2 = 0; }

            thelist.Add(obj);
        }
        if (dr != null) { dr.Close(); }
        return thelist;
    }

    /// <summary>
    /// Returns a className object with the specified ID
    /// </summary>

    public static TransformShopSKU GetTransformShopSKUByID(long shopProductID)
    {
        TransformShopSKU obj = new TransformShopSKU(shopProductID);
        return obj;
    }

    /// <summary>
    /// Updates an existing TransformShopSKU
    /// </summary>
    public static bool UpdateTransformShopSKU(int id,
    long shopproductid, string shopproductsku, string shopproductname, decimal shopprodcutprice, long transformShopProductID, long transformShopProductID2)
    {
        TransformShopSKU obj = new TransformShopSKU();

        obj.ID = id;
        obj.ShopProductID = shopproductid;
        obj.ShopProductSKU = shopproductsku;
        obj.ShopProductName = shopproductname;
        obj.ShopProductPrice = shopprodcutprice;
        obj.TransformShopProductID = transformShopProductID;
        obj.TransformShopProductID2 = transformShopProductID2;
        int ret = obj.Save(obj.ID);
        return (ret > 0) ? true : false;
    }

    /// <summary>
    /// Creates a new TransformShopSKU
    /// </summary>
    public static int InsertTransformShopSKU(
    long shopProductId, string shopProductSku, string shopProductName, decimal shopProductPrice, long transformShopProductID, long transformShopProductID2)
    {
        TransformShopSKU obj = new TransformShopSKU();

        obj.ShopProductID = shopProductId;
        obj.ShopProductSKU = shopProductSku;
        obj.ShopProductName = shopProductName;
        obj.ShopProductPrice = shopProductPrice;
        obj.TransformShopProductID = transformShopProductID;
        obj.TransformShopProductID2 = transformShopProductID2;
        int ret = obj.Save(obj.ID);
        return ret;
    }

    public static long TransformShopProduct(long shopProductID, decimal shopProductPrice)
    {
        TransformShopSKU t = new TransformShopSKU(shopProductID);
        long Id = shopProductID; // send back original ProductID if no transform to be done...
        if (t.ShopProductID > 0) //if there's a Transform entry for shopProductID, then...
        {
            if (shopProductPrice > t.ShopProductPrice) //decide which new productID to send back...
            {
                Id = t.TransformShopProductID; 
            }
            else
            {
                Id = t.TransformShopProductID2; 
            }
        }
        else
        {
            Id = 0;
        }
        return Id;       
    }

}