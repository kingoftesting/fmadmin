﻿using System;
using System.Linq;
using System.Collections.Generic;
using AdminCart;

namespace FM2015.Models
{
    public class StaleCustomers
    {
        public StaleCustomers()
        { }

        public static List<OrdersWithDetail> GetList(List<OrdersWithDetail> odList)
        {
            DateTime startDate = DateTime.Now.AddMonths(Config.stale_startMonthsBack);
            DateTime fmEndDate = startDate.AddMonths(Config.stale_fmPurchaseWindow);
            DateTime noFMDate = startDate.AddMonths(Config.stale_startAccyPurchaseWindow);
            DateTime noPurchaseDate = DateTime.Now.AddMonths(Config.stale_endAccyPurchaseWindow);

            List<OrdersWithDetail> targetList = new List<OrdersWithDetail>();
            if (odList.Count == 0)
            {
                throw new ArgumentOutOfRangeException("DisplayStaleCustomers: couldn't find any customers that purchased a device");
            }

            //get list of FM purchases older then fmEndDate
            List<OrdersWithDetail> fmList = FindDeviceOrders(odList, fmEndDate);

            //get list of customers that purchased accys between fmEndDate & noPurchaseDate
            List<OrdersWithDetail> notRecentAccyList = FindNonRecentAccyOrders(odList, fmEndDate, noPurchaseDate);

            //get list of customers that purchased recently (we want to exclude these!)
            List<OrdersWithDetail> recentAccyList = FindRecentAccyOrders(odList, noPurchaseDate);

            //get list of customers that are common to fm purchase list and not recent accy list
            List<OrdersWithDetail> fmAccyList = FindCommonInLists(fmList, notRecentAccyList);

            //find customers that purchased FM a ways back, then purchased accy, but not recently: target them for a promo?
            targetList = FindNotCommonInLists(notRecentAccyList, recentAccyList);

            targetList = MergeLists(fmList, targetList);

            //sort by customerId
            targetList = targetList.OrderBy(x => x.OrderDate).ToList();
            return targetList;
        }

        public static List<OrdersWithDetail> FindDeviceOrders(List<OrdersWithDetail> odList, DateTime fmEndDate)
        {
            //get list of FM purchases older then fmEnDate
            List<OrdersWithDetail> fmList = (from od in odList
                                             where od.IsFMSystem == true
                                                && od.OrderDate < fmEndDate
                                             select od).ToList();
            if (fmList.Count == 0)
            {
                throw new ArgumentOutOfRangeException("DisplayStaleCustomers: couldn't find any customers that purchased a device greater than 12mos ago");
            }
            return fmList;
        }

        public static List<OrdersWithDetail> FindNonRecentAccyOrders(List<OrdersWithDetail> odList, DateTime fmEndDate, DateTime noPurchaseDate)
        {
            //get list of customers that purchased accys between fmEndDate & noPurchaseDate
            List<OrdersWithDetail> noFMList = (from od in odList
                                               where od.IsFMSystem == false
                                                && od.OrderDate > fmEndDate
                                                && od.OrderDate < noPurchaseDate
                                               select od).ToList();
            if (noFMList.Count == 0)
            {
                throw new ArgumentOutOfRangeException("DisplayStaleCustomers: couldn't find any customers that purchased an accessory between dates: fmEndDate: " + fmEndDate.ToString() + "; noPurchaseDate: " + noPurchaseDate.ToString());
            }
            return noFMList;
        }

        public static List<OrdersWithDetail> FindRecentAccyOrders(List<OrdersWithDetail> odList, DateTime noPurchaseDate)
        {
            //get list of customers that purchased recently (we want to exclude these!)
            List<OrdersWithDetail> recentPurchaseList = (from od in odList
                                                         where od.IsFMSystem == false
                                                            && od.OrderDate >= noPurchaseDate
                                                         select od).ToList();

            if (recentPurchaseList.Count == 0)
            {
                throw new ArgumentOutOfRangeException("DisplayStaleCustomers: couldn't find any customers that purchased an accessory between noPurchaseDate and today");
            }
            return recentPurchaseList;
        }

        public static List<OrdersWithDetail> FindCommonInLists(List<OrdersWithDetail> list1, List<OrdersWithDetail> list2)
        {
            //fills theList with all elements in list1 that are also in list2
            List<OrdersWithDetail> theList = list1.Where(p => list2.Any(p2 => p2.CustomerId == p.CustomerId)).Distinct().ToList();
            theList = RemoveDuplicates(theList);
            if (theList.Count == 0)
            {
                throw new ArgumentOutOfRangeException("DisplayStaleCustomers: couldn't find any common customers between lists");
            }
            return theList;
        }

        public static List<OrdersWithDetail> FindNotCommonInLists(List<OrdersWithDetail> list1, List<OrdersWithDetail> list2)
        {
            //fills theList with all elements in list1 that are not in list2
            List<OrdersWithDetail> theList = list1.Where(p => !list2.Any(p2 => p2.CustomerId == p.CustomerId)).Distinct().ToList();

            theList = RemoveDuplicates(theList);
            if (theList.Count == 0)
            {
                throw new ArgumentNullException("DisplayStaleCustomers: all customers are included in both lists");
            }
            return theList;
        }

        public static List<OrdersWithDetail> MergeLists(List<OrdersWithDetail> list1, List<OrdersWithDetail> list2)
        {
            //fills theList with all elements in list1 that are not in list2
            List<OrdersWithDetail> theList = list1.Concat(list2).ToList();

            theList = RemoveDuplicates(theList);
            if (theList.Count == 0)
            {
                throw new ArgumentNullException("DisplayStaleCustomers: MergeLists: no customers result after merge");
            }

            return theList;
        }

        public static List<OrdersWithDetail> RemoveDuplicates(List<OrdersWithDetail> theList)
        {

            List<OrdersWithDetail> DistinctItems = new List<OrdersWithDetail>();
            DistinctItems = theList.GroupBy(x => x.CustomerId).Select(y => y.First()).ToList();

            return DistinctItems;
        }
    }
}