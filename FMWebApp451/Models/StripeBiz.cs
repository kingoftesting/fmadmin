﻿using AdminCart;
using FM2015.Models;
using FMWebApp451.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stripe
{
    public class StripeBiz_obsolete
    {
        public List<StripeCustomer> GetStripeCustomerList(string param, out string errMsg)
        {
            errMsg = string.Empty;
            StripeCustomerListOptions stripeCustomerListOptions = new StripeCustomerListOptions();

            string email = ParseCustomerListParam(param);
            if (!string.IsNullOrEmpty(email))
            {
                stripeCustomerListOptions = new StripeCustomerListOptions()
                {
                    Email = email,
                    Limit = 10,
                };
            }

            IEnumerable<StripeCustomer> stripeCustomerList = new List<StripeCustomer>();
            var customerService = new StripeCustomerService();

            try
            {
                stripeCustomerList = customerService.List(stripeCustomerListOptions); // optional StripeCustomerListOptions
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "GetCustomerList: customerService.List(): Stripe Error: param = " + param);
            }
            return stripeCustomerList.ToList();
        }

        public StripeCustomer GetStripeCustomer(string id, out string errMsg)
        {
            errMsg = string.Empty;
            StripeCustomer stripeCustomer = new StripeCustomer();
            StripeCustomerService customerService = new StripeCustomerService();

            try
            {
                stripeCustomer = customerService.Get(id);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "GetStripeCustomer: customerService.Get: Stripe Error: StripeCustomerId = " + id);
            }
            return stripeCustomer;
        }

        public List<StripeSubscription> GetCustomerSubscriptionList(string id, out string errMsg)
        {
            errMsg = string.Empty;
            IEnumerable<StripeSubscription> stripeCustomerSubscriptionList = new List<StripeSubscription>();

            StripeSubscriptionService subscriptionService = new StripeSubscriptionService(); //get list of all subscriptions for customer
            StripeSubscriptionListOptions stripeSubscriptionListOptions = new StripeSubscriptionListOptions()
            {
                CustomerId = id,
            };
            try
            {
                stripeCustomerSubscriptionList = subscriptionService.List(stripeSubscriptionListOptions);
            }
            catch (StripeException ex)
            {
                errMsg= Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: GetCustomerSubscriptionList: SubscriptionService: Stripe Error: StripeCustomerId = " + id);
            }
            return stripeCustomerSubscriptionList.ToList();
        }

        public StripeProduct GetStripeProduct(string productId, out string errMsg)
        {
            errMsg = string.Empty;
            StripeProduct stripeProduct = new StripeProduct();
            try
            {
                var productService = new StripeProductService();
                stripeProduct = productService.Get(productId);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: GetStripeProduct: productService.Get: Stripe Error: StripeProductId = " + productId);
            }
            return stripeProduct;
        }

        public List<StripeCard> GetStripeCustomerCards(string id, out string errMsg)
        {
            errMsg = string.Empty;
            IEnumerable<StripeCard> stripeCustomerCardList = new List<StripeCard>();
            try
            {
                StripeCardService cardService = new StripeCardService(); //get list of all credit cards for customer
                stripeCustomerCardList = cardService.List(id);

            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: GetStripeCustomerCards: cardService.List: Stripe Error: CustomerId = " + id);
            }
            return stripeCustomerCardList.ToList();
        }

        public List<StripeCharge> GetStripeCustomerCharges(string id, out string errMsg)
        {
            errMsg = string.Empty;
            IEnumerable<StripeCharge> stripeCustomerChargeList = new List<StripeCharge>();
            StripeChargeListOptions chargeListOptions = new StripeChargeListOptions
            {
                CustomerId = id
            };
            try
            {
                StripeChargeService chargeService = new StripeChargeService();
                stripeCustomerChargeList = chargeService.List(chargeListOptions); // optional StripeChargeListOptions
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: GetStripeCustomerCharges: chargeService.List: Stripe Error: CustomerId = " + id);
            }
            return stripeCustomerChargeList.ToList();
        }

        public List<StripeInvoice> GetStripeCustomerInvoices(string id, out string errMsg)
        {
            errMsg = string.Empty;
            IEnumerable<StripeInvoice> stripeCustomerInvoiceList = new List<StripeInvoice>();
            StripeInvoiceListOptions stripeInvoiceListOptions = new StripeInvoiceListOptions
            {
                CustomerId = id
            };
            try
            {
                StripeInvoiceService invoiceService = new StripeInvoiceService();
                stripeCustomerInvoiceList = invoiceService.List(stripeInvoiceListOptions); // optional StripeInvoiceListOptions
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: GetStripeCustomerInvoices: chargeInvoice.List: Stripe Error: CustomerId = " + id);
            }
            return stripeCustomerInvoiceList.ToList();
        }

        public static StripeCustomer GetStripeCustomerFromPW(string email)
        {
            StripeCustomer stripeCustomer = new StripeCustomer();

            //get PayWhirl subscriber
            PayWhirl.PayWhirlSubscriber pwSubScriber = new PayWhirl.PayWhirlSubscriber();
            try
            {
                //pwSubScriber = PayWhirl.PayWhirlSubscriber.FindSubscriberByEmail(c.Email);
                pwSubScriber = PayWhirl.PayWhirlBiz.FindSubscriberByEmail(email);
            }
            catch (Exception ex)
            {
                string msg = "Customer Email: " + email + Environment.NewLine;
                msg += "Ex Message: " + ex.Message + Environment.NewLine;
                mvc_Mail.SendDiagEmail("GetStripeCustomerFromPW: No PayWhirl Subscriber found", msg);
            }

            //get the customer from Stripe, based on PayWhirl subscriber
            var customerService = new StripeCustomerService();

            try
            {
                stripeCustomer = customerService.Get(pwSubScriber.Stripe_id);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "ShopifyFMOrderTranslator: Get Transaction: Get StripeCustomer: ", "Stripe Error in Translator");
            }

            return stripeCustomer;
        }

        public static List<StripeInvoice> GetStripeInvoiceList(string stripeCustomerId)
        {
            List<StripeInvoice> stripeInvoiceList = new List<StripeInvoice>();

            ///get last Invoice of Stripe customer
            var invoiceService = new StripeInvoiceService();
            var invoiceListOptions = new StripeInvoiceListOptions();
            invoiceListOptions.CustomerId = stripeCustomerId;
            //invoiceListOptions.Date = new StripeDateFilter { GreaterThanOrEqual = DateTime.UtcNow.Date }; // today's invoices
            //List<StripeInvoice> stripeInvoiceList = new List<StripeInvoice>();
            try
            {
                stripeInvoiceList = invoiceService.List(invoiceListOptions).ToList(); // optional StripeInvoiceListOptions
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "GetStripeInvoiceList: Get Invoice List Error", "StripeBiz Error");
            }

            return stripeInvoiceList;
        }

        public bool CreateStripeSubscription(string stripeCustomerID, string stripePlanID, string StripeCouponID, string subscriptionErrMsg, out string errMsg)
        {
            bool success = false;
            errMsg = string.Empty;
            StripeCouponID = (StripeCouponID.ToUpper() == "N/A") ? string.Empty : StripeCouponID;

            var subCreateOptions = new StripeSubscriptionCreateOptions()
            {
                CustomerId = stripeCustomerID,
                Items = new List<StripeSubscriptionItemOption>()
                {
                    new StripeSubscriptionItemOption()
                    {
                        Quantity = 1,
                        PlanId = stripePlanID,
                    },
                },
                CouponId = StripeCouponID,
            };

            var subscriptionService = new StripeSubscriptionService();
            try
            {
                StripeSubscription stripeSubscription = subscriptionService.Create(stripeCustomerID, subCreateOptions, null);
                //StripeSubscription stripeSubscription = subscriptionService.Create(subCreateOptions, null);
            }
            catch (StripeException ex)
            {
                errMsg = "failed: " + Utilities.FMHelpers.ProcessStripeException(ex, true, "", subscriptionErrMsg);
                success = false;
            }

            return success;
        }

        private string ParseCustomerListParam(string param)
        {
            string email = string.Empty;
            if (param != null)
            {
                if (param.Contains("@"))
                {
                    email = param;
                }
                else if (int.TryParse(param, out int customerID))
                {
                    Customer c = new Customer(customerID);
                    email = c.Email;
                }
                else if ((param.ToUpper().Contains("CUS_")) || (param.ToUpper().Contains("SUB_")))
                {
                    List<Subscriptions> subscriptions = Subscriptions.GetSubscribersList();
                    Subscriptions subscriber = (from s in subscriptions where (s.StripeCusID.ToUpper() == param.ToUpper() || s.StripeSubID.ToUpper() == param.ToUpper()) select s).FirstOrDefault();
                    if (subscriber != null)
                    {
                        Order o = new Order(subscriber.OrderID);
                        Customer c = new Customer(o.CustomerID);
                        email = c.Email;
                    }
                }
            }
            return email;
        }

        public StripeCustomer FindStripeCustomerByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public StripeCard CardService_Create(string customerId, StripeCardCreateOptions myCard)
        {
            throw new NotImplementedException();
        }

        public StripeCustomer CustomerService_Get(string customerId)
        {
            throw new NotImplementedException();
        }

        public StripeCustomer CustomerService_Update(string customerId, StripeCustomerUpdateOptions myCustomer)
        {
            throw new NotImplementedException();
        }
    }
}