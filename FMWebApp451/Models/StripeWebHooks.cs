﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Stripe;

namespace FM2015.Models
{
    public class StripeWebHooks
    {
        //https://blogs.msdn.microsoft.com/webdev/2016/12/14/introducing-microsoft-asp-net-webhooks-preview-2/

        public static void ProcessWebHook(StripeEvent stripeEvent, string postValue)
        {
            bool isTest = false; //assume live mode
            if ((stripeEvent.LiveMode == null) || (stripeEvent.LiveMode == false) || (stripeEvent.Id == "evt_00000000000000"))
            { isTest = true; }
            string testStr = (isTest) ? "TEST" : string.Empty;

            List<int> list = ShopifyAgent.ShopifyAgent.SyncShopify(); //sync orders from Shopify

            AdminCart.Cart cart = new AdminCart.Cart();
            bool isMultiPay = false;
            bool isSubscription = false;

            StripeCharge stripeCharge = new StripeCharge();
            StripeCard stripeCard = new StripeCard();
            StripeSubscription stripeSubscription = new StripeSubscription();
            StripeInvoice stripeInvoice = new StripeInvoice();
            Subscriptions newSubUpdate = new Subscriptions();

            switch (stripeEvent.Type)
            {
                // all of the types available are listed in StripeEvents
                case StripeEvents.CustomerSubscriptionUpdated:

                    try
                    {
                        stripeSubscription = Stripe.Mapper<StripeSubscription>.MapFromJson(stripeEvent.Data.Object.ToString());
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, "Subscription.MapFrom():" + testStr + "SubscriptionUpdated");
                        if (isTest)
                        {
                            //return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: Subscription Parsed");
                        }
                        //return Request.CreateResponse(HttpStatusCode.NotFound, "Stripe " + testStr + " WebHook: StripeSubscription Not Parsed Correctly");
                    }
                    newSubUpdate = new Subscriptions(stripeSubscription.Id, stripeSubscription.CustomerId);
                    if (newSubUpdate.ID == 0)
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "WebHook: Subscription/Customer pair not found in Subscriptions Table");
                        if (isTest)
                        {
                            //return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: Subscriptions table error");
                        }
                        //return Request.CreateResponse(HttpStatusCode.NotFound, "Stripe" + testStr + "WebHook: Subscription/Customer pair not found in Subscriptions Table");
                    }
                    newSubUpdate.StripeStatus = stripeSubscription.Status;
                    if (!isTest)
                    {
                        newSubUpdate.Save(newSubUpdate.ID);
                    }
                    if ((stripeSubscription.Status == "past_due") || (stripeSubscription.Status == "unpaid"))
                    {
                        decimal amountDue = Convert.ToDecimal(stripeSubscription.StripePlan.Amount) / 100;
                        // AdminCart.Order order = new AdminCart.Order(90954);
                        AdminCart.Order order = new AdminCart.Order(newSubUpdate.OrderID);
                        AdminCart.Cart pastDueCart = new AdminCart.Cart();
                        pastDueCart = pastDueCart.GetCartFromOrder(newSubUpdate.OrderID, false);
                        if (mvc_Mail.Send2DeclineEmail(order, amountDue, pastDueCart.Tran.CardNo, DateTime.Now, false))
                        {
                            adminCER.CER cer = new adminCER.CER(pastDueCart);
                            cer.EventDescription = "Send Dunning Letter" + Environment.NewLine;
                            cer.EventDescription += HttpContext.Current.Session["EmailDunningCopy"].ToString();
                            cer.ActionExplaination = "Sent Dunning Letter";

                            int result = cer.Add();
                        }
                    }
                    //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Successfull Subscription Status Update");
                    break;

                case StripeEvents.ChargeRefunded:
                    try
                    {
                        stripeCharge = Stripe.Mapper<StripeCharge>.MapFromJson(stripeEvent.Data.Object.ToString());
                        //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: ChargeRefunded:unimplemented");
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "Charge.MapFrom(): Refund");
                        if (isTest)
                        {
                            //return Request.CreateResponse(HttpStatusCode.OK, "Test Modein Live Mode: StripeStripeCharge.MapFrom(): Refund");
                        }
                        //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeCharge:Refund Not Parsed Correctly");
                    }
                    break;

                case StripeEvents.CustomerSubscriptionDeleted:
                    try
                    {
                        stripeSubscription = Stripe.Mapper<StripeSubscription>.MapFromJson(stripeEvent.Data.Object.ToString());
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, "Subscription.MapFrom():" + testStr + "    ");
                        if (isTest)
                        {
                            //return Request.CreateResponse(HttpStatusCode.OK, "Test Modein Live Mode: StripeStripeCharge.MapFrom(): subscription deleted");
                        }
                        //return Request.CreateResponse(HttpStatusCode.NotFound, "Stripe " + testStr + " WebHook: StripeSubscription Not Parsed Correctly");
                    }
                    newSubUpdate = new Subscriptions(stripeSubscription.Id, stripeSubscription.CustomerId);
                    if (newSubUpdate.ID == 0)
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "Subscription_Deleted: Multi-Pay Subscription Not Found");
                        //return Request.CreateResponse(HttpStatusCode.NotFound, "Stripe" + testStr + "WebHook: StripeSubscription Not Parsed Correctly");
                    }
                    newSubUpdate.StripeStatus = stripeSubscription.Status;
                    if (!isTest)
                    {
                        newSubUpdate.Save(newSubUpdate.ID);
                    }

                    //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: Subscription Deleted");
                    break;

                case StripeEvents.ChargeSucceeded:
                    try
                    {
                        stripeCharge = Stripe.Mapper<StripeCharge>.MapFromJson(stripeEvent.Data.Object.ToString());
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "ChargeSucceeded: Failed StripeCharge.MapFrom())");
                        if (isTest)
                        {
                            //return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: ChargeSucceeded: Failed StripeCharge.MapFrom()");
                        }
                        //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeCharge:Succeeded: StripeCharge Not Parsed Correctly");
                    }

                    StripeInvoiceService invoiceService = new StripeInvoiceService();
                    if (stripeCharge.InvoiceId != null)
                    {
                        try
                        {
                            stripeInvoice = invoiceService.Get(stripeCharge.InvoiceId);
                        }
                        catch (StripeException ex)
                        {
                            FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "ChargeSucceeded: Failed InvoiceService.GET");
                            if (isTest)
                            {
                                //return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: ChargeSucceeded: Failed InvoiceService.GET");
                            }
                            //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeCharge:Succeeded Failed InvoiceService.GET");
                        }
                    }

                    string subscriptionID = "";
                    try
                    {
                        subscriptionID = stripeInvoice.SubscriptionId;
                    }
                    catch
                    {; } //ignore exception if not a subscription

                    if (!string.IsNullOrEmpty(subscriptionID))
                    {
                        Subscriptions sub = new Subscriptions(subscriptionID, stripeInvoice.CustomerId);
                        if (sub.ID == 0)
                        {
                            FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "Stripe WebHook: ChargeSucceeded: Subscription; failed to find subscription entry");
                            break;
                        }
                        else
                        {
                            //get transaction info from Stripe, store it where the order can find it (Transactions)
                            cart = cart.GetCartFromOrder(sub.OrderID, false);
                            foreach (AdminCart.Item item in cart.Items)
                            {
                                isSubscription = item.LineItemProduct.IsSubscription;
                                isMultiPay = item.LineItemProduct.IsMultiPay;
                                if (isMultiPay)
                                {
                                    break;
                                }
                            }

                            if (isMultiPay)
                            {
                                List<AdminCart.Transaction> tList = AdminCart.Transaction.GetTransactionList(sub.OrderID);
                                if (tList.Count > 0)
                                {
                                    // check if transaction has already been recorded
                                    List<AdminCart.Transaction> tranList = (from t in tList
                                                                            where t.TransactionID == stripeCharge.Id
                                                                            select t).ToList();

                                    if (tranList.Count() == 0) // if not then record it
                                    {
                                        List<AdminCart.Transaction> tFromStripeList = ShopifyFMOrderTranslator.Get24HRStripeTransactions(stripeCharge.CustomerId, cart.SiteOrder);
                                        if (tFromStripeList.Count() > 0)
                                        {
                                            tFromStripeList.OrderByDescending(x => x.TransactionDate);
                                            AdminCart.Transaction tran = tFromStripeList.First();
                                            if (!isTest) //save the transaction if live
                                            {
                                                tran.SaveDB();
                                            }

                                        }
                                        else
                                        {
                                            FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "Stripe WebHook: ChargeSucceeded: Failed to Find StripeTransaction: OrderID =" + cart.SiteOrder.OrderID.ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "Stripe WebHook: ChargeSucceeded: Failed to Find Transaction: OrderID =" + cart.SiteOrder.OrderID.ToString());
                                }
                            } //end subscription charge processing
                        }

                    } //not a multi-pay charge
                    //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: ChargeSucceeded");
                    break;

                case StripeEvents.CustomerSubscriptionCreated:

                    try
                    {
                        stripeSubscription = Stripe.Mapper<StripeSubscription>.MapFromJson(stripeEvent.Data.Object.ToString());
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "SubscriptionCreated: StripeSubscription.MapFrom())");
                        if (isTest)
                        {
                            //return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: SubscriptionCreated: StripeSubscription.MapFrom()");
                        }
                        //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeSubscription Not Parsed Correctly");
                    }
                    Subscriptions newSub = new Subscriptions();
                    newSub.StripeSubID = stripeSubscription.Id;
                    newSub.StripeCusID = stripeSubscription.CustomerId;
                    newSub.StripePlanID = stripeSubscription.StripePlan.Id;
                    try
                    {
                        newSub.StripePromoCode = stripeSubscription.StripeDiscount.StripeCoupon.Id;
                    }
                    catch
                    {; } //if no coupon then ignore

                    StripeCustomer stripeCustomer = new StripeCustomer();
                    try
                    {
                        var customerService = new StripeCustomerService();
                        stripeCustomer = customerService.Get(newSub.StripeCusID);
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "SubscriptionCreated: customerService.Get())");
                        if (isTest)
                        {
                            //return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: subscription created: customerService.Get()");
                        }
                        //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeCustomer Not Parsed Correctly");
                    }
                    AdminCart.Customer c = AdminCart.Customer.FindCustomerByEmail(stripeCustomer.Email);
                    if (c.CustomerID == 0)
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "Find-Customer-By-Email failed: Email=" + stripeCustomer.Email);
                        if (isTest)
                        {
                            //return Request.CreateResponse(HttpStatusCode.OK, "Stripe WebHook: Test Mode in Live Mode: subscription created: Customer Not Found");
                        }
                        //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Customer Not Found");
                    }
                    List<AdminCart.Order> oList = AdminCart.Order.FindAllOrders(c.CustomerID, false);
                    if (oList.Count == 0)
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "Find-All-Orders Failed: customerID=" + c.CustomerID);
                        //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Order Not Found");
                    }

                    AdminCart.Order o = oList.OrderByDescending(x => x.OrderDate).ToList().First();
                    try
                    {
                        DateTime dtSubStart = (DateTime)stripeSubscription.Start;
                        dtSubStart = dtSubStart.AddHours(-8).AddMinutes(-1); //account for time conversion at Stripe
                        if (o.OrderDate < dtSubStart) //this is here to catch repeat subscribers; an older purchase may get used by mistake
                        {
                            string subStart = (stripeSubscription.Start == null) ? string.Empty : stripeSubscription.Start.ToString();
                            string msg = testStr + "Find-All-Orders failed: Not Created by Shopify yet? Email=" + stripeCustomer.Email + "OrderDate: " + o.OrderDate.ToString() + "Sub.Start: " + subStart;
                            FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, msg);
                            //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Order Not Found: Not Created by Shopify yet?");
                        }
                    }
                    catch
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "Find-All-Orders failed: Subscription.Start == null? Email=" + stripeCustomer.Email);
                        //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Order Not Found: Subscription.Start == null?");
                    }
                    cart = new AdminCart.Cart();
                    cart = cart.GetCartFromOrder(o.OrderID, false);
                    foreach (AdminCart.Item item in cart.Items)
                    {
                        isSubscription = item.LineItemProduct.IsSubscription;
                        isMultiPay = item.LineItemProduct.IsMultiPay;
                        if (isSubscription)
                        {
                            break;
                        }
                    }
                    if (!isSubscription)
                    {
                        FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "Subscription Item in Order Not Found: OrderID=" + o.OrderID);
                        if (isTest)
                        {
                            //return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: subscription created: Subscription Item in Order Not Found");
                        }
                        //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Subscription Order Not Found");
                    }
                    else //if (isMultiPay): allow all subscriptions, not just multipay
                    {
                        newSub.OrderID = o.OrderID;
                        newSub.StripeSubDate = o.OrderDate;
                        newSub.StripeStatus = "active";
                        int id = 0;
                        if (!isTest)
                        {
                            id = newSub.Save(0); // insert new subscription if not a test event
                        }

                        if (id == 0)
                        {
                            FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "Insert to Subscribe Table Not Successful");
                            if (isTest)
                            {
                                //return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: subscription created: Insert to Subscribe Table Not Successful");
                            }
                            //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: Subscription Table Not Updated");
                        }
                    }
                    //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Stripe WebHook: Successfull Subscription Update");
                    break;

                case StripeEvents.InvoicePaymentSucceeded:
                    try
                    {
                        stripeInvoice = Stripe.Mapper<StripeInvoice>.MapFromJson(stripeEvent.Data.Object.ToString());
                    }
                    catch (StripeException ex)
                    {
                        FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "InvoicePaymentSucceeded: Failed StripeInvoice.MapFrom())");
                        if (isTest)
                        {
                            //return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: InvoicePaymentSucceeded: Failed StripeInvoice.MapFrom()");
                        }
                        //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: StripeInvoice:Succeeded: StripeInvoice Not Parsed Correctly");
                    }

                    invoiceService = new StripeInvoiceService();
                    if (stripeInvoice.Id != null)
                    {
                        try
                        {
                            stripeInvoice = invoiceService.Get(stripeInvoice.Id);
                        }
                        catch (StripeException ex)
                        {
                            FM2015.Helpers.Helpers.ProcessStripeException(ex, testStr + "InvoicePaymentSucceeded: Failed InvoiceService.GET");
                            if (isTest)
                            {
                                //return Request.CreateResponse(HttpStatusCode.OK, "Test Mode in Live Mode: InvoicePaymentSucceeded: Failed InvoiceService.GET");
                            }
                            //return Request.CreateResponse(HttpStatusCode.NotFound, testStr + "Stripe WebHook: InvoicePaymentSucceeded Failed InvoiceService.GET");
                        }
                    }



                    subscriptionID = "";
                    try
                    {
                        subscriptionID = stripeInvoice.SubscriptionId;
                    }
                    catch
                    {; } //ignore exception if not a subscription

                    if (!string.IsNullOrEmpty(subscriptionID))
                    {
                        Subscriptions sub = new Subscriptions(subscriptionID, stripeInvoice.CustomerId);
                        if (sub.ID == 0)
                        {
                            FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "Stripe WebHook: InvoicePaymentSucceeded: Subscription; failed to find subscription entry");
                            break;
                        }
                        else
                        {
                            //get transaction info from Stripe, store it where the order can find it (Transactions)
                            cart = cart.GetCartFromOrder(sub.OrderID, false);
                            foreach (AdminCart.Item item in cart.Items)
                            {
                                isSubscription = item.LineItemProduct.IsSubscription;
                                isMultiPay = item.LineItemProduct.IsMultiPay;
                                if (isMultiPay)
                                {
                                    break;
                                }
                            }

                            if (isMultiPay)
                            {
                                List<AdminCart.Transaction> tList = AdminCart.Transaction.GetTransactionList(sub.OrderID);
                                if ((tList.Count() >= 3) && (cart.SiteOrder.SourceID > 0))
                                {
                                    int installements = PayWhirl.PayWhirlPlan.GetPlanInstallments(stripeInvoice.CustomerId, stripeInvoice.SubscriptionId);
                                    string msg = "OrderID = " + cart.SiteOrder.OrderID.ToString() + Environment.NewLine;
                                    msg += "Installments = " + installements.ToString() + Environment.NewLine;
                                    msg += "Transaction Count = " + tList.Count().ToString();
                                    FM2015.Helpers.Helpers.ProcessUpdateEmail(stripeEvent, postValue, testStr + "ACTION ALERT!!: InvoicePayment: Succeeded: Check for Subscription Completion: " + msg);
                                }

                            } //end invoice payment succeeded processing
                        }
                    }
                    break;

                default:
                    //ProcessUpdateEmail(stripeEvent, value.ToString(), testStr + "Unidentified StripeEvent");
                    break;
            }

            //return Request.CreateResponse(HttpStatusCode.OK, "Stripe WebHook Processed OK");
            return;
        }
    }

}