﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using FM2015.Helpers;
using FMWebApp451.Respositories;

namespace FM2015.Models
{
    public class SubscriptionPlans
    {
        #region Private/Public Vars
        private int id;
        public int Id { get { return id; } set { id = value; } }
        private string planId;
        public string PlanId { get { return planId; } set { planId = value; } }
        private string name;
        public string Name { get { return name; } set { name = value; } }
        private decimal amount;
        public decimal Amount { get { return amount; } set { amount = value; } }
        private DateTime created;
        public DateTime Created { get { return created; } set { created = value; } }
        private string interval;
        public string Interval { get { return interval; } set { interval = value; } }
        private int interval_count;
        public int Interval_Count { get { return interval_count; } set { interval_count = value; } }
        private int? installments;
        public int? Installments { get { return installments; } set { installments = value; } }
        #endregion

        public SubscriptionPlans()
        {
            id = 0;
            planId = "0";
            name = "n/a";
            amount = 0;
            created = Convert.ToDateTime("1/1/1900");
            interval = "";
            interval_count = 0;
            installments = null;
        }

        public SubscriptionPlans(string planId)
        {
            SubscriptionPlanProvider subscriptionPlanProvider = new SubscriptionPlanProvider();
            SubscriptionPlans subPlan = subscriptionPlanProvider.GetByPlanId(planId);

            id = subPlan.Id;
            planId = subPlan.PlanId;
            name = subPlan.Name;
            amount = subPlan.Amount;
            created = subPlan.Created;
            interval = subPlan.Interval;
            interval_count = subPlan.Interval_Count;
            installments = subPlan.Installments;
        }
    }
}