﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Stripe;
using FM2015.Helpers;
using AdminCart;

namespace FM2015.Models
{
    public class Subscriptions
    {

        #region Private/Public Vars
        private int id;
        public int ID { get { return id; } set { id = value; } }
        private int orderID;
        public int OrderID { get { return orderID; } set { orderID = value; } }
        private string stripeSubID;
        public string StripeSubID { get { return stripeSubID; } set { stripeSubID = value; } }
        private string stripeCusID;
        public string StripeCusID { get { return stripeCusID; } set { stripeCusID = value; } }
        private string stripePlanID;
        public string StripePlanID { get { return stripePlanID; } set { stripePlanID = value; } }
        private DateTime stripeSubDate;
        public DateTime StripeSubDate { get { return stripeSubDate; } set { stripeSubDate = value; } }
        private string stripeStatus;
        public string StripeStatus { get { return stripeStatus; } set { stripeStatus = value; } }
        private string stripePromoCode;
        public string StripePromoCode { get { return stripePromoCode; } set { stripePromoCode = value; } }
        #endregion

        public Subscriptions()
        {
            id = 0;
            orderID = 0;
            stripeSubID = "n/a";
            stripeCusID = "n/a";
            stripePlanID = "n/a";
            stripeSubDate = Convert.ToDateTime("1/1/1900");
            stripeStatus = "n/a";
            stripePromoCode = "";
        }

        public Subscriptions(int sub_id)
        {
            id = 0;
            orderID = 0;
            stripeSubID = "n/a";
            stripeCusID = "n/a";
            stripePlanID = "n/a";
            stripeSubDate = Convert.ToDateTime("1/1/1900");
            stripeStatus = "n/a";
            stripePromoCode = "";

            string sql = @" 
                SELECT * FROM  Subscribers 
                WHERE ID = @ID 
            ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, sub_id);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                id = Convert.ToInt32(dr["ID"]);
                orderID = Convert.ToInt32(dr["OrderID"].ToString());
                stripeSubID = dr["StripeSubID"].ToString();
                stripeCusID = dr["StripeCusID"].ToString();
                stripePlanID = dr["StripePlanID"].ToString();
                stripeSubDate = Convert.ToDateTime(dr["StripeSubDate"].ToString());
                stripeStatus = dr["StripeStatus"].ToString();
                stripePromoCode = dr["StripePromoCode"].ToString();
            }
            if (dr != null) { dr.Close(); }
        }

        public Subscriptions(string subOrderID)
        {
            id = 0;
            orderID = 0;
            stripeSubID = "n/a";
            stripeCusID = "n/a";
            stripePlanID = "n/a";
            stripeSubDate = Convert.ToDateTime("1/1/1900");
            stripeStatus = "n/a";
            stripePromoCode = "";

            string sql = @" 
                SELECT * FROM  Subscribers 
                WHERE OrderID = @OrderID 
                ORDER BY OrderID ASC 
            ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, subOrderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                id = Convert.ToInt32(dr["ID"]);
                orderID = Convert.ToInt32(dr["OrderID"].ToString());
                stripeSubID = dr["StripeSubID"].ToString();
                stripeCusID = dr["StripeCusID"].ToString();
                stripePlanID = dr["StripePlanID"].ToString();
                stripeSubDate = Convert.ToDateTime(dr["StripeSubDate"].ToString());
                stripeStatus = dr["StripeStatus"].ToString();
                stripePromoCode = dr["StripePromoCode"].ToString();
            }
            if (dr != null) { dr.Close(); }
        }

        public Subscriptions(string stripeSubscriptionID, string stripeCustomerID)
        {
            id = 0;
            orderID = 0;
            stripeSubID = "n/a";
            stripeCusID = "n/a";
            stripePlanID = "n/a";
            stripeSubDate = Convert.ToDateTime("1/1/1900");
            stripeStatus = "n/a";
            stripePromoCode = "";

            string sql = @" 
                SELECT * FROM  Subscribers 
                WHERE StripeSubID = @StripeSubID AND StripeCusID = @StripeCusID  
            ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, stripeSubscriptionID, stripeCustomerID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                id = Convert.ToInt32(dr["ID"]);
                orderID = Convert.ToInt32(dr["OrderID"].ToString());
                stripeSubID = dr["StripeSubID"].ToString();
                stripeCusID = dr["StripeCusID"].ToString();
                stripePlanID = dr["StripePlanID"].ToString();
                stripeSubDate = Convert.ToDateTime(dr["StripeSubDate"].ToString());
                stripeStatus = dr["StripeStatus"].ToString();
                stripePromoCode = dr["StripePromoCode"].ToString();
            }
            if (dr != null) { dr.Close(); }
        }

        public int Save(int ID)
        {
            CacheHelper.Clear(Config.cachekey_SubscriptionsList);
            int result = 0;
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (ID == 0)
            {
                string sql = @" 
                INSERT INTO Subscribers 
                    (OrderID, StripeSubID, StripeCusID, StripePlanID, StripeSubDate, StripeStatus, StripePromoCode) 
                VALUES
                    (@OrderID, @StripeSubID, @StripeCusID, @StripePlanID, @StripeSubDate, @StripeStatus, @StripePromoCode); 
                SELECT ID=@@identity;  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID, StripeSubID, StripeCusID, StripePlanID, StripeSubDate, StripeStatus, StripePromoCode); 
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ID"]);
                }
                else
                {
                    result = 99;
                }
            }
            else
            {
                string sql = @" 
                UPDATE Subscribers 
                SET  
                    OrderID = @OrderID,  
                    StripeSubID = @StripeSubID,  
                    StripeCusID = @StripeCusID,  
                    StripePlanID = @StripePlanID,  
                    StripeSubDate = @StripeSubDate,  
                    StripeStatus = @StripeStatus,   
                    StripePromoCode = @StripePromoCode 
                WHERE ID = @ID  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql,
                    orderID, stripeSubID, stripeCusID, stripePlanID, stripeSubDate, stripeStatus, StripePromoCode, id); 
                DBUtil.Exec(sql, mySqlParameters);
            }
            return result;
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Subscribers
        /// </summary>
        public static List<Subscriptions> GetSubscribersList()
        {
            List<Subscriptions> thelist = new List<Subscriptions>();

            string sql = @" 
                SELECT * FROM Subscribers 
            ";

            SqlDataReader dr = DBUtil.FillDataReader(sql);

            while (dr.Read())
            {
                Subscriptions obj = new Subscriptions();

                obj.ID = Convert.ToInt32(dr["ID"]);
                obj.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                obj.StripeSubID = dr["StripeSubID"].ToString();
                obj.StripeCusID = dr["StripeCusID"].ToString();
                obj.StripePlanID = dr["StripePlanID"].ToString();
                obj.StripeSubDate = Convert.ToDateTime(dr["StripeSubDate"].ToString());
                obj.StripeStatus = dr["StripeStatus"].ToString();
                obj.StripePromoCode = dr["StripePromoCode"].ToString();

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a collection with all Subscribers
        /// </summary>
        public static List<Subscriptions> GetSubscribersList(string stripeCusID)
        {
            List<Subscriptions> thelist = new List<Subscriptions>();

            string sql = @" 
                SELECT * FROM Subscribers 
                WHERE StripeCusID = @StripeCusID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, stripeCusID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                Subscriptions obj = new Subscriptions();

                obj.ID = Convert.ToInt32(dr["ID"]);
                obj.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                obj.StripeSubID = dr["StripeSubID"].ToString();
                obj.StripeCusID = dr["StripeCusID"].ToString();
                obj.StripePlanID = dr["StripePlanID"].ToString();
                obj.StripeSubDate = Convert.ToDateTime(dr["StripeSubDate"].ToString());
                obj.StripeStatus = dr["StripeStatus"].ToString();
                obj.StripePromoCode = dr["StripePromoCode"].ToString();

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a className object with the specified ID
        /// </summary>

        public static Subscriptions GetSubscribersBySubID(string subscriberID)
        {
            Subscriptions obj = new Subscriptions();

            string sql = @" 
                SELECT * FROM  Subscribers 
                WHERE SubscriberID = @SubScriberID AND OrderID <> -1 
                ORDER BY OrderID DESC 
            ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, subscriberID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                obj.id = Convert.ToInt32(dr["ID"]);
                obj.orderID = Convert.ToInt32(dr["OrderID"].ToString());
                obj.stripeSubID = dr["StripeSubID"].ToString();
                obj.stripeCusID = dr["StripeCusID"].ToString();
                obj.stripePlanID = dr["StripePlanID"].ToString();
                obj.stripeSubDate = Convert.ToDateTime(dr["StripeSubDate"].ToString());
                obj.stripeStatus = dr["StripeStatus"].ToString();
                obj.StripePromoCode = dr["StripePromoCode"].ToString();
            }
            if (dr != null) { dr.Close(); }
            return obj;
        }

        /// <summary>
        /// Updates an existing Subscribers
        /// </summary>
        public static bool UpdateSubscribers(int id,
        int orderid, string stripesubid, string stripecusid, string stripeplanid, DateTime stripesubdate, string stripestatus, string stripePromoCode)
        {
            Subscriptions obj = new Subscriptions();

            obj.ID = id;
            obj.OrderID = orderid;
            obj.StripeSubID = stripesubid;
            obj.StripeCusID = stripecusid;
            obj.StripePlanID = stripeplanid;
            obj.StripeSubDate = stripesubdate;
            obj.StripeStatus = stripestatus;
            obj.StripePromoCode = stripePromoCode;
            int ret = obj.Save(obj.ID);
            return (ret > 0) ? true : false;
        }

        /// <summary>
        /// Creates a new Subscribers
        /// </summary>
        public static int InsertSubscribers(
        int orderid, string stripesubid, string stripecusid, string stripeplanid, DateTime stripesubdate, string stripestatus, string stripePromoCode)
        {
            Subscriptions obj = new Subscriptions();

            obj.OrderID = orderid;
            obj.StripeSubID = stripesubid;
            obj.StripeCusID = stripecusid;
            obj.StripePlanID = stripeplanid;
            obj.StripeSubDate = stripesubdate;
            obj.StripeStatus = stripestatus;
            obj.StripePromoCode = stripePromoCode;
            int ret = obj.Save(obj.ID);
            return ret;
        }

        public static int Stripe_UnSubscribe(string stripeCustomerID, string stripeSubscriptionID)
        {
            var subscriptionService = new StripeSubscriptionService();
            string ErrMsg = "";

            Subscriptions sub = new Subscriptions(stripeSubscriptionID, stripeCustomerID);
            if (sub.ID == 0)
            {
                ErrMsg = "Subscription Delete: subscription can't be found; StripeCustomerID = " + stripeCustomerID + "; " + "StripeSubscriptionID = " + stripeSubscriptionID;
                HttpContext.Current.Session["Stripe_UnSubscribe_Msg"] = ErrMsg;
                return -1;
            }

            Order o = new Order(sub.OrderID);
            if (o.OrderID == 0)
            {
                ErrMsg = "Subscription Delete: order can't be found; StripeCustomerID = " + stripeCustomerID + "; " + "StripeSubscriptionID = " + stripeSubscriptionID + "; " + "OrderID = " + sub.OrderID.ToString();
                HttpContext.Current.Session["Stripe_UnSubscribe_Msg"] = ErrMsg;
                return -1;
            }

            Cart cart = new Cart();
            cart = cart.GetCartFromOrder(o.OrderID);

            try
            {
                //subscriptionService.Cancel(stripeCustomerID, stripeSubscriptionID); // optional cancelAtPeriodEnd flag
                subscriptionService.Cancel(stripeSubscriptionID); // optional cancelAtPeriodEnd flag

                adminCER.CER cer = new adminCER.CER(cart);
                cer.EventDescription = "Subscription Delete" + Environment.NewLine;
                cer.EventDescription += "Subscription ID: " + sub.ID.ToString() + Environment.NewLine;
                cer.EventDescription += "Subscription Plan: " + sub.StripePlanID + Environment.NewLine;
                cer.EventDescription += "Stripe Customer ID: " + stripeCustomerID + Environment.NewLine;
                cer.EventDescription += "Stripe Subscriber ID: " + stripeSubscriptionID + Environment.NewLine;
                cer.EventDescription += "OrderID:" + o.OrderID.ToString() + Environment.NewLine;
                cer.EventDescription += "LastName:" + cart.SiteCustomer.LastName + Environment.NewLine;
                cer.EventDescription += "Unsubscribed By: " + cer.ReceivedBy + Environment.NewLine;
                cer.ActionExplaination = "Unsubscribe Subscription";
                int result = cer.Add();
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Subscription: Unsubscribe: Stripe Error");
                ErrMsg = "Subscription Delete: SubscriptionService.Cancel Failed; StripeCustomerID = " + stripeCustomerID + "; " + "StripeSubscriptionID = " + stripeSubscriptionID + "; " + "OrderID = " + sub.OrderID.ToString();
                HttpContext.Current.Session["Stripe_UnSubscribe_Msg"] = ErrMsg;
                return -1;
            }

            ErrMsg = "";
            HttpContext.Current.Session["Stripe_UnSubscribe_Msg"] = ErrMsg;
            return 0;
        }

        public static string SubscriberStatus(string email)
        {
            PayWhirl.PayWhirlSubscriber pwSubScriber = new PayWhirl.PayWhirlSubscriber();
            try
            {
                //pwSubScriber = PayWhirl.PayWhirlSubscriber.FindSubscriberByEmail(email);
                pwSubScriber = PayWhirl.PayWhirlBiz.FindSubscriberByEmail(email);
            }
            catch
            {
                return ""; // return w/ indicating customer is not a subscriber
            }

            List<Subscriptions> subList = Subscriptions.GetSubscribersList(pwSubScriber.Stripe_id);
            foreach(Subscriptions sub in subList)
            {
                if (sub.stripeStatus == "unpaid") { return "unpaid"; }
                if (sub.stripeStatus == "past_due") { return "past_due"; }
            }

            return "";
        }

        public static StripeInvoice GetLastInvoice(string stripeCustomerId)
        {
            StripeInvoice invoice = new StripeInvoice();
            var invoiceService = new StripeInvoiceService();
            StripeInvoiceListOptions stripeInvoiceListOptions = new StripeInvoiceListOptions();
            stripeInvoiceListOptions.CustomerId = stripeCustomerId;
            try
            {
                IEnumerable<StripeInvoice> response = invoiceService.List(stripeInvoiceListOptions); // optional StripeInvoiceListOptions
                invoice = (from i in response select i).OrderByDescending(i => i.Date).FirstOrDefault();
            }
            catch //ignore Stripe exceptions here
            {; }

            return invoice;
        }

        public static decimal GetAccountBalance(string stripeCustomerId)
        {
            decimal accountBalance = 0;
            StripeCustomer stripeCustomer = new StripeCustomer();

            //get the customer from Stripe
            var customerService = new StripeCustomerService();

            try
            {
                stripeCustomer = customerService.Get(stripeCustomerId);
            }
            catch (StripeException ex)
            {
                HttpContext.Current.Session["subErrorMessage"] = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Cart: Get StripeCustomer: Stripe Error");
                return -1; // return w/ error
            }

            accountBalance = Convert.ToDecimal(stripeCustomer.AccountBalance) / 100;
            return accountBalance;
        }

        public static StripeCustomer GetStripeCustomer(string stripeCustomerId)
        {
            StripeCustomer stripeCustomer = new StripeCustomer();

            //get the customer from Stripe
            var customerService = new StripeCustomerService();

            try
            {
                stripeCustomer = customerService.Get(stripeCustomerId);
            }
            catch (StripeException ex)
            {
                HttpContext.Current.Session["subErrorMessage"] = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Cart: Get StripeCustomer: Stripe Error");
                return stripeCustomer; // return w/ error
            }

            return stripeCustomer;
        }

        public static StripeCustomer GetStripeCustomerFromEmail(string email)
        {
            StripeCustomer stripeCustomer = new StripeCustomer();

            PayWhirl.PayWhirlSubscriber pwSubScriber = new PayWhirl.PayWhirlSubscriber();
            try
            {
                //pwSubScriber = PayWhirl.PayWhirlSubscriber.FindSubscriberByEmail(email);
                pwSubScriber = PayWhirl.PayWhirlBiz.FindSubscriberByEmail(email);
            }
            catch (Exception ex)
            {
                //some exception handling stuff here
                HttpContext.Current.Session["subErrorMessage"] = "GetStripeCustomerFromEmail: Error Finding PayWhirl Customer: " + ex.Message;
                return stripeCustomer; // return w/ error
            }

            //get the customer from Stripe
            var customerService = new StripeCustomerService();

            try
            {
                stripeCustomer = customerService.Get(pwSubScriber.Stripe_id);
            }
            catch (StripeException ex)
            {
                HttpContext.Current.Session["subErrorMessage"] = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "GetStripeCustomerFromEmail: Get StripeCustomer: Stripe Error");
                return stripeCustomer; // return w/ error
            }

            return stripeCustomer;
        }
    }
}
