﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using FM2015.Helpers;

namespace FM2015.Models
{
    public class UPSShipments
    {
        #region Private/Public Vars
        private int id;
        public int ID { get { return id; } set { id = value; } }
        private int orderID;
        public int OrderID { get { return orderID; } set { orderID = value; } }
        private decimal weight;
        public decimal Weight { get { return weight; } set { weight = value; } }
        private string shipDate;
        public string ShipDate { get { return shipDate; } set { shipDate = value; } }
        private string trackingNumber;
        public string TrackingNumber { get { return trackingNumber; } set { trackingNumber = value; } }
        private decimal uPSCharge;
        public decimal UPSCharge { get { return uPSCharge; } set { uPSCharge = value; } }
        private DateTime upLoadDate;
        public DateTime UpLoadDate { get { return upLoadDate; } set { upLoadDate = value; } }
        private string upLoadBy;
        public string UpLoadBy { get { return upLoadBy; } set { upLoadBy = value; } }
        private int sSOrderID;
        public int SSOrderID { get { return sSOrderID; } set { sSOrderID = value; } }
        #endregion

        public UPSShipments()
        {
            ID = 0;
            OrderID = 0;
            Weight = 0;
            ShipDate = "n/a";
            TrackingNumber = "";
            UPSCharge = 0;
            UpLoadDate = Convert.ToDateTime("1/1/1900");
            UpLoadBy = "n/a";
            SSOrderID = 0;
        }

        public UPSShipments(int FMOrderID)
        {
            //initialize
            ID = 0;
            OrderID = 0;
            Weight = 0;
            ShipDate = "n/a";
            TrackingNumber = "";
            UPSCharge = 0;
            UpLoadDate = Convert.ToDateTime("1/1/1900");
            UpLoadBy = "n/a";
            SSOrderID = 0;

            string sql = @" 
                SELECT * FROM  UPSShipments 
                WHERE OrderID = @OrderID 
            ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, FMOrderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);
            if (dr.Read())
            {
                ID = Convert.ToInt32(dr["UPSShipmentID"]);
                if (dr["OrderID"] != DBNull.Value) { OrderID = Convert.ToInt32(dr["OrderID"].ToString()); }
                if (dr["Weight"] != DBNull.Value) { Weight = Convert.ToDecimal(dr["Weight"].ToString()); }
                if (dr["ShipDate"] != DBNull.Value) { ShipDate = dr["ShipDate"].ToString(); }
                if (dr["TrackingNumber"] != DBNull.Value) { TrackingNumber = dr["TrackingNumber"].ToString(); }
                if (dr["UPSCharge"] != DBNull.Value) { UPSCharge = Convert.ToDecimal(dr["UPSCharge"].ToString()); }
                if (dr["UpLoadDate"] != DBNull.Value) { UpLoadDate = Convert.ToDateTime(dr["UpLoadDate"].ToString()); }
                if (dr["UpLoadBy"] != DBNull.Value) { UpLoadBy = dr["UpLoadBy"].ToString(); }
                if (dr["SSOrderID"] != DBNull.Value) { SSOrderID = Convert.ToInt32(dr["SSOrderID"].ToString()); }
            }
            if (dr != null) { dr.Close(); }
        }

        public int Save(int ID)
        {
            int result = 0;
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (ID == 0)
            {
                string sql = @" 
                    INSERT INTO UPSShipments 
                    (OrderID, Weight, ShipDate, TrackingNumber, UPSCharge, UpLoadDate, UpLoadBy, SSOrderID) 
                    VALUES(@OrderID, @Weight, @ShipDate, @TrackingNumber, @UPSCharge, @UpLoadDate, @UpLoadBy, @SSOrderID); 
                    SELECT ID=@@identity;  
                ";

                mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID, Weight, ShipDate, TrackingNumber, UPSCharge, UpLoadDate, UpLoadBy, SSOrderID);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ID"]);
                }
                else
                {
                    result = 99;
                }
            }
            else
            {
                string sql = @" 
                UPDATE UPSShipments 
                SET  
                    OrderID = @OrderID,  
                    Weight = @Weight,  
                    ShipDate = @ShipDate,  
                    TrackingNumber = @TrackingNumber,  
                    UPSCharge = @UPSCharge,  
                    UpLoadDate = @UpLoadDate,  
                    UpLoadBy = @UpLoadBy,  
                    SSOrderID = @SSOrderID  
                WHERE ID = @ID  
";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, OrderID, Weight, ShipDate, TrackingNumber, UPSCharge, UpLoadDate, UpLoadBy, SSOrderID, ID);
                DBUtil.Exec(sql, mySqlParameters);
            }
            return result;
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all UPSShipments
        /// </summary>
        public static List<UPSShipments> GetUPSShipmentsList()
        {
            List<UPSShipments> thelist = new List<UPSShipments>();

            string sql = @" 
                SELECT * FROM UPSShipments 
            ";

            SqlDataReader dr = DBUtil.FillDataReader(sql);

            while (dr.Read())
            {
                UPSShipments obj = new UPSShipments();

                obj.ID = Convert.ToInt32(dr["ID"]);
                obj.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                obj.Weight = Convert.ToDecimal(dr["Weight"].ToString());
                obj.ShipDate = dr["ShipDate"].ToString();
                obj.TrackingNumber = dr["TrackingNumber"].ToString();
                obj.UPSCharge = Convert.ToDecimal(dr["UPSCharge"].ToString());
                obj.UpLoadDate = Convert.ToDateTime(dr["UpLoadDate"].ToString());
                obj.UpLoadBy = dr["UpLoadBy"].ToString();
                obj.SSOrderID = Convert.ToInt32(dr["SSOrderID"].ToString());

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        public static List<UPSShipments> GetUPSShipmentsListByID(int orderID)
        {
            List<UPSShipments> thelist = new List<UPSShipments>();

            string sql = @" 
                SELECT * FROM UPSShipments 
                WHERE OrderID = @OrderID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, orderID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                UPSShipments obj = new UPSShipments();

                obj.ID = Convert.ToInt32(dr["UPSShipmentID"]);
                obj.OrderID = Convert.ToInt32(dr["OrderID"].ToString());
                obj.Weight = Convert.ToDecimal(dr["Weight"].ToString());
                obj.ShipDate = dr["ShipDate"].ToString();
                obj.TrackingNumber = dr["TrackingNumber"].ToString();
                obj.UPSCharge = Convert.ToDecimal(dr["UPSCharge"].ToString());
                obj.UpLoadDate = Convert.ToDateTime(dr["UpLoadDate"].ToString());
                obj.UpLoadBy = dr["UpLoadBy"].ToString();
                obj.SSOrderID = Convert.ToInt32(dr["SSOrderID"].ToString());

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        /// <summary>
        /// Returns a className object with the specified ID
        /// </summary>

        public static UPSShipments GetUPSShipmentsByID(int orderID)
        {
            UPSShipments obj = new UPSShipments(orderID);
            return obj;
        }

        /// <summary>
        /// Updates an existing UPSShipments
        /// </summary>
        public static bool UpdateUPSShipments(int id,
        int orderid, decimal weight, string shipdate, string trackingnumber, decimal upscharge, DateTime uploaddate, string uploadby, int ssorderid)
        {
            UPSShipments obj = new UPSShipments();

            obj.ID = id;
            obj.OrderID = orderid;
            obj.Weight = weight;
            obj.ShipDate = shipdate;
            obj.TrackingNumber = trackingnumber;
            obj.UPSCharge = upscharge;
            obj.UpLoadDate = uploaddate;
            obj.UpLoadBy = uploadby;
            obj.SSOrderID = ssorderid;
            int ret = obj.Save(obj.ID);
            return (ret > 0) ? true : false;
        }

        /// <summary>
        /// Creates a new UPSShipments
        /// </summary>
        public static int InsertUPSShipments(
        int orderid, decimal weight, string shipdate, string trackingnumber, decimal upscharge, DateTime uploaddate, string uploadby, int ssorderid)
        {
            UPSShipments obj = new UPSShipments();

            obj.OrderID = orderid;
            obj.Weight = weight;
            obj.ShipDate = shipdate;
            obj.TrackingNumber = trackingnumber;
            obj.UPSCharge = upscharge;
            obj.UpLoadDate = uploaddate;
            obj.UpLoadBy = uploadby;
            obj.SSOrderID = ssorderid;
            int ret = obj.Save(obj.ID);
            return ret;
        }
    }
}