﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using FM2015.Helpers;
using FMWebApp451.Interfaces;

namespace FM2015.Models
{
    public class User
    {
        #region private Properties
        #endregion

        #region public Properties
        public int UserID { get; set; } //use for Authentication
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserRoles { get; set; } //use for Authorization
        #endregion

        public User()
        {
            UserID = 0;
            FirstName = "";
            LastName = "";
            Email = "";
            Password = "";
            UserRoles = "";
        }

        public User(int userID)
        {
            AdminCart.Customer c = new AdminCart.Customer(userID);
            UserID = c.CustomerID;
            FirstName = c.FirstName;
            LastName = c.LastName;
            Email = c.Email;
            Password = c.Password;
            UserRoles = User.GetRoles(userID);
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all roles
        /// </summary>
        public static List<Roles> GetrolesList()
        {
            List<Roles> thelist = new List<Roles>();

            string sql = @" 
            SELECT * FROM fm_roles 
        ";

            SqlDataReader dr = DBUtil.FillDataReader(sql);

            while (dr.Read())
            {
                Roles obj = new Roles();

                obj.ID = Convert.ToInt32(dr["ID"]);
                obj.Customerid = Convert.ToInt32(dr["Customerid"].ToString());
                obj.Role = dr["Role"].ToString();
                obj.Lastname = dr["Lastname"].ToString();
                obj.Firstname = dr["Firstname"].ToString();

                thelist.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return thelist;
        }

        public static string GetRoles(int userID)
        {
            string obj = "";

            string sql = @" 
            SELECT Role FROM fm_roles WHERE CustomerID = @CustomerID 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, userID);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                obj = dr["Role"].ToString();
            }
            if (dr != null) { dr.Close(); }
            return obj;
        }

        public static bool IsAuthenticated()
        {
            bool isAuth = false;
            if (HttpContext.Current.Session != null)
            {
                try
                {
                    if ((User)HttpContext.Current.Session["_currentUser"] != null)
                    {
                        User user = (User)HttpContext.Current.Session["_currentUser"];
                        if (user.UserID > 0) { isAuth = true; }
                    }
                }
                catch
                {
                    return false;
                }
            }

            return isAuth;
        }

        public static string GetUserName()
        {
            User user = new User();
            if (HttpContext.Current.Session != null)
            {
                try
                {
                    if ((User)HttpContext.Current.Session["_currentUser"] != null)
                    {
                        user = (User)HttpContext.Current.Session["_currentUser"];
                    }
                }
                catch
                {
                    return string.Empty;
                }
            }
            return user.FirstName + " " + user.LastName;
        }

        public static int GetUserID()
        {
            User user = new User();
            if (HttpContext.Current.Session != null)
            {
                try
                {
                    user = (User)HttpContext.Current.Session["_currentUser"];
                    return user.UserID;
                }
                catch
                {
                    return 0;
                }
            }
            return 0;
        }


        public static bool HasRole(string userRole)
        {
            User user = new User();
            if (HttpContext.Current.Session != null)
            {
                try
                {
                    user = (User)HttpContext.Current.Session["_currentUser"];
                    string roles = GetRoles(user.UserID).ToLowerInvariant();
                    return (roles.Contains(userRole.ToLowerInvariant()));
                }
                catch
                {
                    return false;
                }

            }
            return false;
        }

        public static bool HasRole()
        {
            User user = new User();
            if (HttpContext.Current.Session != null)
            {
                try
                {
                    user = (User)HttpContext.Current.Session["_currentUser"];
                    string roles = GetRoles(user.UserID);
                    return (roles.Length > 0);
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }
    }
}