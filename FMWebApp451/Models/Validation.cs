﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FM2015.Models
{
    public class Validation
    {
        #region private Properties
        #endregion

        #region public Properties
        public int ValidationId { get; set; } //use for Authentication
        public DateTime Date { get; set; }
        public string By { get; set; }
        public bool Pass { get; set; }
        public long SerialNum { get; set; }
        public string Comments { get; set; }
        #endregion

        public Validation()
        {
            ValidationId = 0;
            Date = DateTime.MinValue;
            By = "";
            Pass = false;
            SerialNum = 0;
            Comments = "n/a";
        }
    }
}