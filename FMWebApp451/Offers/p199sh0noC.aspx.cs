using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class p199sh0noC : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Create META Description
        HtmlMeta metaDESC = new HtmlMeta();
        metaDESC.Name = "DESCRIPTION";
        metaDESC.Content = "Suzanne Somers' FaceMaster Facial Toning System - give yourself a great face makeover with FaceMaster";

        //Create META Page-Topic
        HtmlMeta metaPageTopic = new HtmlMeta();
        metaPageTopic.Name = "PAGE-TOPIC";
        metaPageTopic.Content = "Suzanne Somers FaceMaster";

        //Create META Keywords
        HtmlMeta metaKeywords = new HtmlMeta();
        metaKeywords.Name = "KEYWORDS";
        metaKeywords.Content = "Suzanne somers, facemaster, facemaster, beauty, beauty-makeover, beauty-make-over, face-makeover, face-make-over, beauty secrets, finger wands, fingerwands, FaceMaster Treatment";

        /*
        HtmlLink lnkCanonical = new HtmlLink();
        lnkCanonical.Href = "~/beauty-makeover/";
        lnkCanonical.Attributes.Add("rel", "canonical");
        */

        //Add META controls to HtmlHead
        HtmlHead head = (HtmlHead)Page.Header;
        head.Controls.Add(metaDESC);
        head.Controls.Add(metaPageTopic);
        head.Controls.Add(metaKeywords);
        //head.Controls.Add(lnkCanonical);



        if (!Page.IsPostBack)
        {
            InitURLs(); 

            string s = "";
            if (Request["Panel"] != null) { s = Request["Panel"].ToString(); }
            if (!string.IsNullOrEmpty(s) && s == "Content")
            {
                pnlStart.Visible = false;
                pnlContent.Visible = true;
            }
            else
            {
                pnlStart.Visible = false; //always display the content panel
                pnlContent.Visible = true;
            }
        }
        else
        {
        }
    }

    private void InitURLs()
    {
        lnkIntroFMP9.NavigateUrl = AbsoluteWebRoot + "beauty-MakeOver/Default.aspx?Panel=Content#order";
        lnkIntroFMP9.ImageUrl = AbsoluteWebRoot + "images/adwords/OrderButton1.jpg";

        //imgTT2.ImageUrl = AbsoluteWebRoot + "images/adwords/ToneTighten.jpg";
        //imgTT2.AlternateText = "Tone and Tighten Your Face Without Surgery";
    }


    #region URL handling

    private static string _RelativeWebRoot;
    /// <summary>
    /// Gets the relative root of the website.
    /// </summary>
    /// <value>A string that ends with a '/'.</value>
    public static string RelativeWebRoot
    {
        get
        {
            if (_RelativeWebRoot == null)
                _RelativeWebRoot = VirtualPathUtility.ToAbsolute("~/");

            return _RelativeWebRoot;
        }
    }

    //private static Uri _AbsoluteWebRoot;

    /// <summary>
    /// Gets the absolute root of the website.
    /// </summary>
    /// <value>A string that ends with a '/'.</value>
    public static Uri AbsoluteWebRoot
    {
        get
        {
            //if (_AbsoluteWebRoot == null)
            //{
            HttpContext context = HttpContext.Current;
            if (context == null)
                throw new System.Net.WebException("The current HttpContext is null");

            if (context.Items["absoluteurl"] == null)
                context.Items["absoluteurl"] = new Uri(context.Request.Url.GetLeftPart(UriPartial.Authority) + RelativeWebRoot);

            return context.Items["absoluteurl"] as Uri;
            //_AbsoluteWebRoot = new Uri(context.Request.Url.GetLeftPart(UriPartial.Authority) + RelativeWebRoot);// new Uri(context.Request.Url.Scheme + "://" + context.Request.Url.Authority + RelativeWebRoot);
            //}
            //return _AbsoluteWebRoot;
        }
    }

    /// <summary>
    /// Converts a relative URL to an absolute one.
    /// </summary>
    public static Uri ConvertToAbsolute(Uri relativeUri)
    {
        return ConvertToAbsolute(relativeUri.ToString()); ;
    }

    /// <summary>
    /// Converts a relative URL to an absolute one.
    /// </summary>
    public static Uri ConvertToAbsolute(string relativeUri)
    {
        if (String.IsNullOrEmpty(relativeUri))
            throw new ArgumentNullException("relativeUri");

        string absolute = AbsoluteWebRoot.ToString();
        int index = absolute.LastIndexOf(RelativeWebRoot.ToString());

        return new Uri(absolute.Substring(0, index) + relativeUri);
    }

    /// Retrieves the subdomain from the specified URL.
    /// </summary>
    /// <param name="url">The URL from which to retrieve the subdomain.</param>
    /// <returns>The subdomain if it exist, otherwise null.</returns>
    public static string GetSubDomain(Uri url)
    {
        if (url.HostNameType == UriHostNameType.Dns)
        {
            string host = url.Host;
            if (host.Split('.').Length > 2)
            {
                int lastIndex = host.LastIndexOf(".");
                int index = host.LastIndexOf(".", lastIndex - 1);
                return host.Substring(0, index);
            }
        }

        return null;
    }

    #endregion
}
