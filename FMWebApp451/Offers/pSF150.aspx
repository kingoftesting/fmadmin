<%@ Page Language="C#" MasterPageFile="AdWords.master" Theme="AdWords" AutoEventWireup="true" Inherits="pSF150" Title="Tighten, Lift, and Tone for a Younger Looking Face!" Codebehind="pSF150.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server"> 

<!-- *********************** START MAIN PANEL *********************** -->
<asp:Panel ID="pnlStart" Width="650px" Visible="true" runat="server">

<!-- *********************** PAGE CONTENT HERE *********************** -->
<div style="width: 100%; background-color:#ffffff; margin:0;">
<div style="width:650px; margin:0 auto 0 auto;">
<table id="Table_01" class="MainTable" width="650px" cellspacing="0">
    <tr>
        <td valign="top" style="width:100%; height:29px; background-color:White; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:7px 0 0 0;">
            <div style="text-align:center;">
            <strong> 
            &nbsp &nbsp &nbsp &nbsp
            &nbsp &nbsp &nbsp &nbsp
            <a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content#tt">Tone and Tighten</a>
            &nbsp &nbsp &nbsp &nbsp
            <a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content#ss">Suzanne Somers</a>
            &nbsp &nbsp &nbsp &nbsp
            <a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content#t">Technology</a>
            &nbsp &nbsp &nbsp &nbsp
            <a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content#test">Testimonials</a>
            &nbsp &nbsp &nbsp &nbsp
            <a href="<%=AbsoluteWebRoot %>p199sh0noC.aspx?Panel=Content#faqs">Q&amp;A</a>
            &nbsp &nbsp &nbsp &nbsp
            &nbsp &nbsp &nbsp &nbsp
            </strong> 
            </div>
       </td>
    </tr> 
  
    <tr>
        <td valign="top" style="padding: 0 0 0 0px;">
            <a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content#tt"><img src="<%=AbsoluteWebRoot %>images/adwords/ToneTighten.jpg" alt="Tone and Tighten Your Face Without Surgery" width="675" height="256" style="border:0" /> </a>
        </td>
    </tr>
    
    <tr>
        <td valign="middle" style="height:160px; padding: 0px 0 0 0px; border-top:#ffd0ec solid 2px; border-bottom:#ffd0ec solid 2px; background-color:#f9f7ea;">            
            <h1 style="font-family: Georgia, 'Times New Roman', Times, serif; font-size: 19px; line-height: 27px; color: #333; font-weight: normal; margin: 0; padding: 0px 0 0 20px;">
            <br /><strong>Suzanne Somers&rsquo; #1 beauty makeover secret</strong> isn&rsquo;t a lotion, a day at the spa, or painful visit 
            to the doctor&rsquo;s office. The secret behind her amazingly youthful beauty comes straight out 
            of a dermatologist&rsquo;s office and into your home&hellip;</h1>
        </td>
    </tr>
    <tr>
		<td>
		    <div>
		    <asp:Image ID="imgFMP" ImageUrl="~/images/products/platinum.jpg" Width="300px" AlternateText="FaceMaster Platinum" runat="server" style="float:right; margin-left: 5px; margin-right: 5px;"/>
            <h2 style="text-align:left; font-family:News Gothic MT, Arial, Helvetica, sans-serif; font-size:32px; font-weight:normal; line-height:38px;"> 
             Introducing<br />
            <strong><em><a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content#order">FaceMaster<sup style="font-size:smaller">&reg;</sup> Platinum</a></em></strong>
            </h2>
            <ul style="margin-left:0; padding-left:10px; font-family:News Gothic MT, Arial, Helvetica, sans-serif; font-size:16px; font-weight:normal; line-height:30px;">
                <li>Reduce Fine Lines and Wrinkles</li>
                <li>Feel Younger, Look Better!</li>
                <li>Technology Used By Professional Spas and Clinics, Now Available To You In Your Own Home</li>
            </ul>
            </div>
		</td>
	</tr>
	<tr>
		<td align="left">
           <br /><br />
           <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="675" height="72" id="MiddleSloganBar" align="middle">
            <param name="allowScriptAccess" value="sameDomain" />
            <param name="movie" value="../Flash/MiddleSloganBar.swf" />
            <param name="quality" value="high" />
            <param name="bgcolor" value="#ffffff" />
            <embed src="~/Flash/MiddleSloganBar.swf" quality="high" bgcolor="#ffffff" width="675" height="72" name="MiddleSloganBar" align="middle" allowscriptaccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
            </object>
        </td>
	</tr>
  
  <tr>
    <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: 19px;  padding:10px 20px; color: #444;">
      
      <h2 style="text-align:center; font-family:News Gothic MT, Arial, Helvetica, sans-serif; font-size:32px; font-weight:bold; line-height:38px; color: #444;"> 
      Introducing the        
      <br />
      <em>
      <a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content#order">FaceMaster<sup style="font-size:smaller">&reg;</sup> Platinum</a></em>
      <br />
      <em><a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content#order">Facial Toning System</a></em> </h2>
      
      <p> <strong> 
      Tighten, lift, and tone your face</strong> right at home and 
      <strong><a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content#tt">look as young as you feel!</a></strong>
      This incredibly easy facial toning system tightens sagging skin with 
      advanced technology usually found only at your dermatologist&rsquo;s office, or in professional spa&rsquo;s and clinic&rsquo;s.</p>
      
      <p> Invasive, expensive surgeries often leave you with a taut, pulled-back, expressionless face. 
      But <strong>the 
      <em><a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content#order">FaceMaster</a></em> 
      tones your facial muscles to reduce sagging and the look of wrinkles</strong>, taking years off of your appearance
      with a complete beauty makeover. 
      Within two-to-three weeks, people will start wondering what you&rsquo;ve been doing to look so good!</p>
      
      <p><em><strong><a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content">Keep Reading&hellip;</a></strong></em></p>

      <p style="font-family:Georgia, Times New Roman, Times, serif; font-size:18px; line-height: 27px; color: #333; border-top:#ffd0ec solid 2px; border-bottom:#ffd0ec solid 2px; padding:10px; background-color:#f9f7ea;">
      <strong><em>As Suzanne puts it&hellip;</em><br />
        </strong>&ldquo;I have been using the 
        <em><a href="<%=AbsoluteWebRoot %>pSS150.aspx?Panel=Content"><strong>FaceMaster</strong></a></em> 
        for 15 years and it is still the most important beauty makeover product in my skincare regime. I use it every day of the year, 
        and on the occasional day that I don&rsquo;t, people always tell me I look tired. I really feel it&rsquo;s 
        kept my face from sagging. Using the same technology found in expenses spas and clinics, a FaceMaster beauty makeover 
        keeps you looking tight and taut!"
       </p>
      
      <p align="center"><asp:HyperLink ID="lnkIntroFMP9" runat="server" Text="" /></p>
      </td>
  </tr>
</table>
</div>
</div>

</asp:Panel>
<!-- *********************** END MAIN PANEL *********************** -->


<!-- *********************** START CONTENT PANEL *********************** -->
<asp:Panel ID="pnlContent" Width="850px" runat="server">

<div id="nav">
<strong>
    &nbsp &nbsp &nbsp &nbsp
    &nbsp &nbsp &nbsp &nbsp
    &nbsp &nbsp &nbsp &nbsp
    <a href="#tt">Tone and Tighten</a>
    &nbsp &nbsp &nbsp &nbsp
    <a href="#t">Technology</a>
    &nbsp &nbsp &nbsp &nbsp
    <a href="#ss">Suzanne Somers</a>
    &nbsp &nbsp &nbsp &nbsp
    <a href="#test">Testimonials</a>
    &nbsp &nbsp &nbsp &nbsp
    <a href="#faqs">FAQs</a>
    &nbsp &nbsp &nbsp &nbsp
    &nbsp &nbsp &nbsp &nbsp
</strong> 
</div>

<!--//////////////////////////////////////////////this is the FREE LP banner version2/////////////////////////////////////////////-->
<div style="width: 100%; background-color:#ffffff; margin:0;">
<div style="width:650px; margin:0 auto 0 auto;">
  <table width="650px" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
      <tr>
        <td style="width:650px" ><img src="<%=AbsoluteWebRoot %>images/adwords/FreeShippingMinPurchase50.jpg" alt="Save $29.95 while supplies last!" /></td>
      </tr>
    </tbody>
  </table>
  </div>
</div><!--//////////////////////////////////////////////this is the FREE Shipping LP banner version2/////////////////////////////////////////////-->

     
<div id="container"><a href="#order"><img src="<%=AbsoluteWebRoot %>images/adwords/ToneTighten7.jpg" alt="Save $29.95 while supplies last!" /></a>
<hr />
    <span style="margin-left:0px; color:#eb0184;"><strong>FREE GIFT PACK ($150 Value!):</strong></span> 
    order now and receive FREE one additonal bottle of Soothing Conductive Serum, 
    Shipping, one additional year Extended Warranty, Travel Bag and Portable Mirror
<hr />

<table border="0" cellspacing="0" cellpadding="0" id="orderGrid">
  <tr id="labels">
    <td class="leftCol">Product </td>
    <td>Value</td>
    <td class="red">SAVE</td>
    <td><strong>Sale Price</strong></td>
    <td>Add to Cart</td>
  </tr>
  <tr class="divideRow">
      <td class="leftCol"><strong><em>FaceMaster Platinum Facial Toning System</em></strong><br />
          <span class="smll">Includes: The <em>FaceMaster, </em>Ergonomic Hand Wands, Finger Wands, Soothing Conductive Serum, Collagen Enhancing Serum with Peptides, 100 Disposable Foam Caps, Instructional DVD, Instruction Manual, Quick Start Guide, and 9-Volt Battery.</span>
          <span class="smll"><br /><span style="color:#eb0184;"><strong>FREE GIFT PACK</strong></span> 
            order now and receive FREE: one additonal bottle of Soothing Conductive Serum, 
            Shipping, one additional year Extended Warranty, Travel Bag and Portable Mirror!</span>
      </td>
      <td><div class="priceCut">$299.99</div></td>
    <td  class="red"><strong>$150.00</strong></td>
    <td><strong>$149.99</strong></td>
    <td>
        <asp:HyperLink ID="HyperLink1" NavigateUrl="http://www.facemaster.com/ViewCart.aspx?promo=yes&sset=18&Action=AddMult&count=5&pID0=14&qty0=1&pID1=107&qty1=1&pID2=97&qty2=1&pID3=106&qty3=1&pID4=108&qty4=1" ImageUrl="~/images/buynow_pink.jpg" runat="server">Add To Cart</asp:HyperLink>
    </td>
  </tr>
</table>

<br /><br />
<h1 style="text-align:center; font-family: Georgia, 'Times New Roman', Times, serif; font-size: 24px; line-height: 27px; color: #333; font-weight: normal; margin: 0; padding: 0px 0 0 20px;">
<a href="#order">Try the FaceMaster For a Great Beauty MakeOver!</a>
<br /><br />
<a href="#order">Reduce Lines and Wrinkles. Look Beautiful and Amazing!</a>
</h1>
 
<div id="colWrapper">
 
  <p style="margin-top: 25px;">
  <a name="tt" id="tt"></a>You can finally <strong><a href="#order">tighten sagging skin and tone your face</a></strong> 
  without succumbing to painful, costly surgery! Originally developed for actress Suzanne Somers, the <strong><em>
  <a href="#order">FaceMaster</a></em><sup style="font-size:12px;">&reg;</sup> <em><a href="#order">Platinum Facial Toning System</a></em></strong> 
  gives your facial muscles a gentle &#8220;workout&#8221; using micro current stimulation.</p>
  
  <div class="floatright" style="padding-left:10px;">
    <object width="380" height="285">
      <param name="movie" value="http://www.youtube.com/v/52--ZoH5ie8?fs=1&amp;hl=en_US" />
      <param name="allowFullScreen" value="true" />
      <param name="allowscriptaccess" value="always" />
      <embed src="http://www.youtube.com/v/52--ZoH5ie8?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="380" height="285"></embed>
    </object>
  </div>
  
  <p>As the micro currents tone and strengthen your facial muscles, <strong>your skin lifts, wrinkles fade, 
  and fine lines smooth out</strong>. 
  Until the <strong><em>FaceMaster</em></strong> was developed, the only way to get the amazing results 
  from this technology was at high-end spas and dermatologist offices&#8212;with a hefty price tag of more 
  than $150 per treatment. </p>
  
  <p>But the team at <strong><em><a href="#order">FaceMaster</a></em></strong> changed all that&#8230; </p>
  
  <h2><a name="t" id="t"></a>The first portable, affordable, facial toning system</h2>
  
  <p>The <strong><em>FaceMaster</em></strong> team wanted to take the technology found in the large $12,000 machines 
  and harness it in a portable, compact device you could use anywhere. While no easy task, 
  the <strong><em>FaceMaster</em></strong> technology team got the job done.</p>
  
  <p>Now you can <strong><a href="#order">tighten sagging skin and reduce the appearance of wrinkles right at home</a>
  </strong>&#8230;or anywhere you go. All you do is use the &#8220;magic wands&#8221; to send tiny micro currents into 
  your facial muscles, prompting them to contract. This induces a tightening effect; much like exercise has on all the 
  muscles throughout our bodies. It&#8217;s the sagging muscles that create wrinkles. The micro currents plump up the 
  muscles and smooth out the wrinkles.</p>
  
  <p>The end result is a <strong>tighter, smoother, younger-looking face</strong>&#8212;much like you&#8217;d get with 
  costly surgery. But instead of that too-tight, pulled-back, &#8220;my face doesn&#8217;t move anymore&#8221; effect, 
  the <strong><em><a href="#order">FaceMaster</a></em><a href="#order"> tightens, tones, and smoothes</a></strong> for 
  a <strong>naturally youthful appearance</strong>. </p>
  
  <div class="floatright" style="padding-left:10px;"><a name="ss" id="ss"></a><br />
      <object width="380" height="285">
        <param name="movie" value="http://www.youtube.com/v/7r6rFuy0OhE?fs=1&amp;hl=en_US" />
        <param name="allowFullScreen" value="true" />
        <param name="allowscriptaccess" value="always" />
        <embed src="http://www.youtube.com/v/7r6rFuy0OhE?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="380" height="285"></embed>
      </object>
  </div>
  
  <h2 align="center">&#8220;Using the same technology found in expenses spas and clinics, a FaceMaster beauty makeover keeps you looking tight and taut!&#8221;  </h2>
  
  <p>The team at <strong><em><a href="#order">FaceMaster</a></em></strong> didn&#8217;t just tap the best technology 
  talent; they also worked with one of the most famous beautiful women of our time, Suzanne Somers. In fact, she was 
  the inspiration for the <strong><em>FaceMaster</em></strong> and fully credits it with her amazingly youthful, 
  smooth, stunning face.</p>
  
  <p>As Suzanne says, you can use the <strong><em><a href="#order">FaceMaster</a></em></strong> every day, or as 
  little as 3 times a week. If you follow the complete routine, it takes about 18 minutes. And just like going to the 
  gym, the more you do it, the sooner you&#8217;ll see the fantastic results!  </p>
  
  <h2 align="center"> <a name="t" id="A1"></a>Incredible results, plus a great value!<br />
    And you save $150.00 while supplies last&#8230;</h2>
    
  <p>You can <strong><a href="#order">order your <em>FaceMaster</em> right now</a></strong> and pay only $149.99! 
  Quantities at this price are limited, so act fast! Your <strong><em>FaceMaster</em></strong> comes with a 
  <strong>100% 30-day guarantee</strong>, so if you&#8217;re not pleased with your results, you can return it 
  for a full refund.</p>
  
 <div id="quote"> 
 <p>&#8220;I would never recommend going &#8216;under the knife&#8217; for a more youthful appearance&#8230;all 
 surgery carries great risks, and oftentimes looks unnatural. But looking youthful and beautiful is a critical part of 
 your overall health. When you look good, you feel good and you&#8217;re far more likely to take 
 good care of yourself. </p>
 
 <p>I recommend the <em><strong><a href="#order">FaceMaster&#8212;it&#8217;s safe, effective,</a></strong></em> 
and the science behind it stands up to the closest scrutiny. It&#8217;s a trustworthy product that&#8217;s going to 
deliver results you&#8217;ll see and love.&#8221; 
<br /><br />
Peter G. Hanson, MD
<br />Best Selling Author of "The Joy of Stress" 
&nbsp;</p>
 
 </div>
 
 <br />
  <h2 align="center">Typically, you (and everyone that looks at you!) can see a difference in just the first week!</h2>

  <div style="text-align:center"><a href="#order"><img src="<%=AbsoluteWebRoot %>images/adwords/beforeafter.jpg" alt="FaceMaster: Before and After" /></a></div>
 
  <p>
  Brenda, pictured above, has the 
  <a href="#order">FaceMaster</a>
  on only one side of her face in a 20 minute treatment.
  Compare her eyebrows and you'll see that the one on the right is higher than the other. 
  <a href="#order">FaceMaster</a>
  tones the muscles
  underneath the skin. By contracting the muscles, it makes them more taut and creates an uplifting effect. The
  eye on the 
  <a href="#order">FaceMaster</a>
   side looks more open and bright. Notice how the crow's feet are softened on the treated side.
  Notice how the cheek looks higher and firmer. And notice how the corner of the mouth is turning up on the treated side,
  while on the untreated side it is frowning. By stimulating the muscle from the corner of the nose to the corner of the
  lip, 
  <a href="#order">FaceMaster</a>
  brings back the natural smile!
  </p>

 
  <div style="text-align:center"><a href="#order"><img src="<%=AbsoluteWebRoot %>images/products/platinum.jpg" alt="FaceMaster" /></a></div>
  
  <div id="inset">
    
  <h4 align="center" class="clearBothod"><span style="font-size:24px;">Order Your <em>FaceMaster Platinum Facial Toning System</em> Today</span><br />
  Save $150.00&mdash;while supplies last!</h4>

  <h2> Your <strong><em>FaceMaster</em></strong> comes right to your door
    with<br />
    everything you need to start
    looking younger
    right away:</h2>
    <ol>
        <li class="ans"><a href="<%=AbsoluteWebRoot %>ViewCart.aspx?promo=yes&sset=18&Action=AddMult&count=5&pID0=14&qty0=1&pID1=107&qty1=1&pID2=97&qty2=1&pID3=106&qty3=1&pID4=108&qty4=1"><img src="<%=AbsoluteWebRoot %>images/adwords/fmkit.jpg" alt="FaceMaster" border="0" class="floatright" style="margin-right:25px;"/></a>
        <strong class="reg">The <em style="font-weight:bold">FaceMaster Platinum Facial Toning System</em></strong></li>
        <li class="ans"><strong class="reg">Ergonomic Hand Wands</strong></li>
        <li class="ans"><strong class="reg">NEW Finger Wands&#8212;for one-handed operation</strong></li>
        <li class="ans"><strong class="reg">Soothing Conductive Serum (2 fl. oz.)</strong></li>
        <li class="ans"><strong class="reg">Anti-Aging Serum with Glycopeptides (1.0 fl. oz.)</strong></li>
        <li class="ans"><strong class="reg">100 Disposable Foam Caps</strong></li>
        <li class="ans"><strong class="reg">Instructional DVD</strong></li>
        <li class="ans"><strong class="reg">Instruction Manual</strong></li>
        <li class="ans"><strong class="reg">Quick Start Guide</strong></li>
        <li class="ans"><strong class="reg">9-Volt Battery</strong></li>
        <li class="ans"><strong class="reg">FREE Shipping (US only)</strong></li>
        <li class="ans"><strong class="reg">FREE One additional year Extended Warranty</strong></li>
        <li class="ans"><strong class="reg">FREE GIFT: Soothing Conductive Serum (2 fl. oz.)</strong></li>
        <li class="ans"><strong class="reg">FREE GIFT: Travel Bag</strong></li>
        <li class="ans"><strong class="reg">FREE GIFT: Portable Mirror</strong></li>

    </ol>
    </div>

  <br />
  <h2 align="center">The new &amp; improved FaceMaster Platinum</h2>

  <p>
    With a 
    <a href="#order">desire to look their very best</a>,
    women from every corner of the world have made it a personal mission to find
    the most effective solutions to the challenge of looking younger without breaking the bank. The medical and technical
    team behind the revolutionary creation of
    <strong><em>FaceMaster</em></strong>
    have completed years of research on how
    <strong><em>FaceMaster</em></strong>,
    with 
    <a href="#order">advanced "Wave Technology"</a>, 
    can deliver 
    <a href="#order">affordable and sustainable results</a> 
    without invasive procedures.
    The table below shows how
   <strong><em>FaceMaster</em></strong>,
    when compared to many popular treatments, is truly the best and most 
    <a href="#order">comprehensive solution to looking years younger</a>.
  </p>
  
  <div align="center">
  <img src="<%=AbsoluteWebRoot %>images/adwords/whyfm2.jpg" alt="Comparison Chart" border="0" style="padding:0; margin:10px 0 10px 0;"/>
  </div>
  
  <div id="testimonials">
  <h3><a name="test" id="A2"></a><em>Testimonials</em></h3>
  <p id="2" class="q">
  AMAZING! My face has been 
  <a href="#order">rejuvenated!</a> 
  A workout for my face that really helps plump up and diminish fine lines. 
  I also have used it after Botox (which I am now resistant too!!) and it would make the Botox injections last longer. 
  So now it's just me and my
  <a href="#order">FaceMaster!</a>
  LOVE IT! Thanks Suzanne for all you do to help women everyday! 
  <br />- Gina, Los Angeles 
  </p>
  
  <p id="3" class="q">
  As a baby boomer getting close to the big 50, I have been very concerned about what I could do to 
  <a href="#order">look younger and feel better about myself</a>. 
  The 
  <a href="#order">FaceMaster</a>
  has been a huge breakthrough for me! I experimented and did only 
  one side of my face for a week without touching the other side. The results astonished me! My face looked firmer, 
  my jawline tighter, and my eyes looked rested, yet alert. Even my 16 year old son noticed something was different! 
  This has been one of the best things I have ever done for myself. It is not another half empty, useless jar of face 
  cream sitting in my drawer. It is the way I get 
  <a href="#order">REAL results</a> 
  every time I use it!!!! Thanks Suzanne!!!  
  <br />- Beth, Las Vegas 
  </p>
  
  <p id="6" class="q">
  I absolutely L O V E my 
  <a href="#order">FaceMaster</a> 
  and cannot imagine life without it. I receive so many compliments on my 
  appearance and people think that I am actually 20, yes, 
  <a href="#order">twenty years younger than I am</a>. 
  I especially love the 
  effect it produces around my eyes. Even if I had only minimal sleep, like 4 - 5 hcours, my 
  <a href="#order">FaceMaster</a>
  gets that 
  circulation going, makes me feel totally refreshed and my eyes, eyebrows and general eye area looks fabulous. Then 
  I get 
  <a href="#order">constant compliments</a> 
  on my beautiful, big, ice-blue eyes.....and don't I just love that! Like I said, I cannot 
  live without it....the 
  <a href="#order">FaceMaster</a>
  is AWESOME!!! Thanks Suzanne....you're also fabulous and I love you girl!!! 
  <br />- Jutta, Delaware Ohio
  </p>
  
  </div>

  <br />
  <div id="faq">
  <h3><a name="faqs" id="faqs"></a><em>Frequently Asked Questions</em></h3>
  <p class="q"><strong>Q. </strong>What is <strong><em>FaceMaster</em></strong> Platinum &quot;Wave&quot; Technology?</p>
  
  <p><span class="ans"><strong><em>A. </em></strong></span><strong><em>FaceMaster</em></strong> <strong><em>Platinum</em></strong> is the third generation <strong><em>FaceMaster</em></strong> and utilizes new &quot;Wave&quot; Technology&#8212;a soothing, yet stimulating electrical output from either the <strong><em>FaceMaster</em></strong> Hand Wands or the <strong><em>FaceMaster</em></strong> Finger Wands. Using a bi-phase, symmetrical alternating current, the <strong><em>FaceMaster</em></strong> <strong><em>Platinum</em></strong> delivers an <strong><a href="#order">affordable and sustainable result</a></strong>.</p>
  
  <p class="q"><strong>Q.</strong> Is this electricity safe?</p>
  
  <p><strong class="ans"><em>A. </em></strong>Yes. Your <strong><em>FaceMaster</em></strong> has 
  <strong><a href="#order">passed a rigorous, physician-supervised, independently reviewed, clinical study</a></strong> 
  to determine both its safety and cosmetic effectiveness.</p>
  
  
  <p class="q"><strong>Q.</strong> How often do you recommend using the <strong><em>FaceMaster</em></strong> and 
  how long does a treatment take?</p>
  
  <p><strong class="ans"><em>A. </em></strong>The <strong>recommended usage is every other day</strong>. This gives 
  the muscles time to rest and change. <strong><a href="#order">The entire treatment takes 18 minutes</a></strong> 
  after you've gained some experience with using the product; initially the treatment will take a little longer. 
  You can also use the feathering technique to reduce fine lines&#8212;which takes about 12 minutes every day if you 
  desire.</p>
  
  
  <p class="q"><strong>Q. </strong>How does electricity in the micro current technology help my face?</p>
  
  <p><strong class="ans"><em>A. </em></strong>For over three decades, doctors and spas have recognized the great 
  cosmetic potential of small doses of electricity applied to the skin. This superficial stimulation can be felt 
  as a mild tingling, tapping, or &#8220;twitch.&#8221; The use of mild electrical stimulation has been shown to 
  have the <strong><a href="#order">beneficial effect of toning the skin</a></strong> and making it <strong>appear 
  smoother and less wrinkled</strong>.</p>
  
  
  <p class="q"><strong>Q.</strong> Are there any potential safety issues that I should be aware of?</p>
  
  <p><strong class="ans"><em>A. </em></strong>In addition to the indicated and contra-indicated uses listed on the 
  label, any person with cardiac arrhythmias, pacemakers, implanted defibrillators, any other implanted metallic or 
  electronic devices, or other cardiac conditions should not use the <strong><em>FaceMaster</em></strong> unless they 
  have the express permission from their doctor. <br /></p>
 
</div>
 
 
<a name="order"></a>
<!--//////////////////////////////////////////////this is the FREE LP banner version2/////////////////////////////////////////////-->
<div style="width: 100%; background-color:#ffffff; margin:10px 0;">
<div style="width:650px; margin:0 auto 0 auto;">
  <table width="650px" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
      <tr>
        <td width="650"><img src="<%=AbsoluteWebRoot %>images/adwords/freeshipping.jpg" alt="Save $150.00 while supplies last!" /></td>
      </tr>
    </tbody>
  </table>
  </div>
</div><!--//////////////////////////////////////////////this is the FREE Shipping LP banner version2/////////////////////////////////////////////-->
 
 
<h4 align="center" class="clearBothod"><span style="font-size:24px;">Order Your <em>FaceMaster Platinum Facial Toning System</em> Today</span><br />
  Save $150.00&mdash;while supplies last!</h4>
<!--<p><img src="images/0110_hydropulseLP_check.jpg" width="38" height="37" class="check" /><strong class="pink" style="font-size: 16px;">YES!</strong> I want a safe, easy, and effective solution to my nasal and sinus congestion. Please send me the <strong><em>Hydro Pulse Nasal/Sinus Irrigation System</em></strong>&mdash;Dr. Lark&rsquo;s top recommended nasal and sinus irrigation system&mdash;<em>right away</em>! </p>-->
 
<table border="0" cellspacing="0" cellpadding="0" id="orderGrid1">
  <tr id="labels1">
    <td class="leftCol">Product </td>
    <td>Value</td>
    <td class="red">SAVE</td>
    <td><strong>Sale Price</strong></td>
    <td>Add to Cart</td>
  </tr>
  <tr class="divideRow">
      <td class="leftCol"><strong><em>FaceMaster Platinum Facial Toning System</em></strong><br />
          <span class="smll">Includes: The <em>FaceMaster, </em>Ergonomic Hand Wands, Finger Wands, Soothing Conductive Serum, Anti-Aging Serum with Glycopeptides, 100 Disposable Foam Caps, Instructional DVD, Instruction Manual, Quick Start Guide, and 9-Volt Battery.</span> 
          <span class="smll"><br /><span style="color:#eb0184;"><strong>FREE GIFT PACK</strong></span> 
            order now and receive FREE: one additonal bottle of Soothing Conductive Serum, 
            FREE Shipping, one additional year Extended Warranty, Travel Bag and Portable Mirror!</span></td>
      <td><div class="priceCut">$299.99</div></td>
    <td  class="red"><strong>$150.00</strong></td>
    <td><strong>$149.99</strong></td>
    <td>
        <asp:HyperLink ID="lnkBuyNow" NavigateUrl="http://www.facemaster.com/ViewCart.aspx?promo=yes&sset=18&Action=AddMult&count=5&pID0=14&qty0=1&pID1=107&qty1=1&pID2=97&qty2=1&pID3=106&qty3=1&pID4=108&qty4=1" ImageUrl="~/images/buynow_pink.jpg" runat="server">Add To Cart</asp:HyperLink>
    </td>
  </tr>
</table>
 
 
 
 
<div id="guarantee">
  <h3>100% 30-Day Guarantee</h3>
  <p>Your <strong><em>FaceMaster</em></strong> comes with a <strong>100% 30-day guarantee</strong>, so if you&#8217;re not pleased with your results, you can return it within 30 days for a full refund. </p>
</div>
</div>
 
 
 
 
<div id="orderByPhone">You must place your order on this page to qualify for this special offer. Ordering online is safe and secure.</div>
</div>

 
 
 
 
<script type="text/javascript"> 
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>

</asp:Panel>
<!-- *********************** END CONTENT PANEL *********************** -->


</asp:Content>


