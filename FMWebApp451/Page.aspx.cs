using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Page : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int PageID = Convert.ToInt32(Request["PageID"].ToString());
        //PageTitle.Text = DBUtil.GetScalar("SELECT PageTitle FROM Pages WHERE PageID=" + PageID.ToString());

        //instead of trying to customize the OperatingInstructions page
        //just send them to VideoTips
        if (PageID == 3) { Response.Redirect("videotips.aspx"); }

        BodyText.Text = Biz.GetPageText(PageID);
        LeftHTML.Text = Biz.GetPageText(PageID, 5);

    }
}
