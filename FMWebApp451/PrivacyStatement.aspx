<%@ Page Language="C#" MasterPageFile="~/MasterPage3_Cart.master" Theme="Platinum" AutoEventWireup="true" Inherits="PrivacyStatement" Title="FaceMaster - Privacy Policy" Codebehind="PrivacyStatement.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <table width="600px">
    <tr>
        <td width="200px">
            <br /><br /><br /><br />
            <asp:Literal ID="LeftHTML" runat="server" />
        </td>
        <td>
            <br />
            <asp:Literal ID="BodyText" runat="server" />
        </td>
    </tr>
    </table>
</asp:Content>

