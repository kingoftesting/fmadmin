﻿using FMWebApp451.Interfaces;
using AdminCart;

namespace FM2015.Models
{
    public class AddressProvider : IAddress
    {
        public Address GetByCustomerId(int customerId, int type)
        {
            Address a = Address.LoadAddress(customerId, type);
            return a;
        }
    }
}