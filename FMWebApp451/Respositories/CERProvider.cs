﻿using FMWebApp451.Interfaces;
using AdminCart;

namespace FM2015.Models
{
    public class CERProvider : ICER
    {
        public adminCER.CER GetByCustomer(Customer c)
        {
            adminCER.CER cer = new adminCER.CER(c);
            return cer;
        }

        public int Add(adminCER.CER cer)
        {
            return cer.Add();
        }
    }

}