﻿using System;
using System.Collections.Generic;
using FMWebApp451.Interfaces;
using AdminCart;
using FM2015.Helpers;
using System.Linq;
using System.Data.SqlClient;

namespace FM2015.Models
{
    public class CustomerProvider : ICustomer
    {
        public Customer GetById(int id)
        {
            //Customer c = new Customer(id);
            List<Customer> theList = GetAll();
            Customer customer = (from c in theList where c.CustomerID == id select c).FirstOrDefault();
            return customer;
        }

        public List<Customer> GetAll()
        {
            List<Customer> customerList = new List<Customer>();
            CacheHelper.Get(AdminCart.Config.cachekey_CustomerList, out customerList);
            if ((customerList == null) || (customerList.Count == 0))
            {
                customerList = new List<Customer>();

                string sql = @"
                SELECT * 
                FROM Customers 
                ";

                SqlDataReader dr = DBUtil.FillDataReader(sql);
                while (dr.Read())
                {
                    Customer c = new Customer();
                    c.CustomerID = Int32.Parse(dr["CustomerID"].ToString());
                    c.FirstName = dr["FirstName"].ToString();
                    c.LastName = dr["LastName"].ToString();

                    c.Email = dr["Email"].ToString();
                    c.Password = dr["Password"].ToString();
                    c.Phone = dr["phone"].ToString();

                    c.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                    c.LastUpdateDate = DateTime.Parse(dr["LastUpdateDate"].ToString());
                    c.LastLoginDate = DateTime.Parse(dr["LastLoginDate"].ToString());

                    c.SiteID = Int32.Parse(dr["SiteID"].ToString());
                    c.ReferrerDomain = dr["ReferrerDomain"].ToString();
                    c.InBoundQuery = dr["inBoundQuery"].ToString();

                    c.SuzanneSomersCustomerID = Convert.ToInt32(dr["suzannesomerscustomerid"].ToString());

                    if (dr["FaceMasterNewsletter"].ToString() == "True") c.FaceMasterNewsletter = true; else c.FaceMasterNewsletter = false;
                    if (dr["SuzanneSomersNewsletter"].ToString() == "True") c.SuzanneSomersNewsletter = true; else c.SuzanneSomersNewsletter = false;

                    customerList.Add(c);
                }

                if (customerList.Count > 0)
                {
                    CacheHelper.Add<List<Customer>>(customerList, Config.cachekey_CustomerList, Config.cacheDuration_CustomerList);
                }
            }
            return customerList;
        }

        public bool Save(Customer customer)
        {
            CacheHelper.Clear(Config.cachekey_ProductList);
            customer.SaveCustomerToDB();
            return true;
        }
    }
}