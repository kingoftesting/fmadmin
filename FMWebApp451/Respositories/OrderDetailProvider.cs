﻿using System;
using System.Collections.Generic;
using FMWebApp451.Interfaces;
using AdminCart;
using FM2015.Helpers;
using System.Linq;

namespace FM2015.Models
{
    public class OrderDetailProvider : IOrderDetail
    {
        public List<OrderDetail> GetById(int Id)
        {
            //return OrderDetail.GetOrderDetails(startDate, endDate);
            List<OrderDetail> theList = GetAll();
            theList = (from od in theList where (od.OrderDetailID == Id) select od).ToList();
            if (theList == null)
            {
                theList = new List<AdminCart.OrderDetail>();
            }
            return theList;
        }

        public List<OrderDetail> GetByDate(DateTime startDate, DateTime endDate)
        {
            //List<OrderDetail> theList = GetAll();
            //theList = (from od in theList where (od.OrderDate <= endDate && od.OrderDate >= startDate) select od).ToList();
            List<OrderDetail> theList = OrderDetail.GetOrderDetails(startDate, endDate);
            if (theList == null)
            {
                theList = new List<AdminCart.OrderDetail>();
            }
            return theList;
        }

        public List<OrderDetail> GetAll()
        {
            List<OrderDetail> orderDetailList = OrderDetail.GetOrderDetails();
            return orderDetailList;
        }

        public bool Save(OrderDetail od)
        {
            //CacheHelper.Clear(Config.cachekey_OrderList);
            od.SaveToOrderDetail();
            return true;
        }
    }
}