﻿using System;
using System.Collections.Generic;
using FMWebApp451.Interfaces;
using AdminCart;
using FM2015.Helpers;
using System.Linq;
using System.Data.SqlClient;

namespace FM2015.Models
{
    public class OrderDetailRestoreProvider : IOrderDetail
    {
        public List<OrderDetail> GetById(int Id)
        {
            //return OrderDetail.GetOrderDetails(startDate, endDate);
            List<OrderDetail> theList = GetAll();
            theList = (from od in theList where (od.OrderDetailID == Id) select od).ToList();
            if (theList == null)
            {
                theList = new List<AdminCart.OrderDetail>();
            }
            return theList;
        }

        public List<OrderDetail> GetByDate(DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public List<OrderDetail> GetAll()
        {
            List<OrderDetail> orderDetailList = GetOrderDetails();
            return orderDetailList;
        }

        public bool Save(OrderDetail od)
        {
            throw new NotImplementedException();
        }

        private List<OrderDetail> GetOrderDetails()
        {
            List<OrderDetail> details = new List<OrderDetail>();

            string sql = @"
            SELECT * FROM OrderDetails";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql);
            string shopConnStr = AdminCart.Config.ConnStrSiteV2Restore();
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters, shopConnStr);

            while (dr.Read())
            {
                OrderDetail od = new OrderDetail
                {
                    OrderDetailID = Convert.ToInt32(dr["OrderDetailID"].ToString()),
                    OrderID = Convert.ToInt32(dr["OrderID"].ToString()),
                    ProductID = Convert.ToInt32(dr["ProductID"].ToString()),
                    Quantity = Convert.ToInt32(dr["Quantity"].ToString()),
                    UnitPrice = Convert.ToDecimal(dr["Price"].ToString())
                };

                if (dr["UnitCost"] == DBNull.Value) { od.UnitCost = 0; }
                else { od.UnitCost = Convert.ToDecimal(dr["UnitCost"].ToString()); }

                od.Discount = Convert.ToDecimal(dr["Discount"].ToString());

                od.LineItemProduct = new Product(od.ProductID);
                od.LineItemTotal = od.Quantity * (od.UnitPrice - od.Discount);

                details.Add(od);
            }
            dr.Close();
            return details;
        }
    }
}