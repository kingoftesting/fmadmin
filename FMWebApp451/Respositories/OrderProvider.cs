﻿using System;
using System.Collections.Generic;
using FMWebApp451.Interfaces;
using AdminCart;
using FM2015.Helpers;
using System.Linq;

namespace FM2015.Models
{
    public class OrderProvider : IOrder
    {
        public List<Order> GetByDate(DateTime startDate, DateTime endDate)
        {
            List<Order> theList = GetAll();
            theList = (from o in theList where (o.OrderDate <= endDate && o.OrderDate >= startDate) select o).ToList();
            if (theList == null)
            {
                theList = new List<AdminCart.Order>();
            }
            return theList;
        }

        public List<Order> GetAll()
        {
            List<Order> orderList = new List<Order>();
            CacheHelper.Get(AdminCart.Config.cachekey_OrderList, out orderList);
            if ((orderList == null) || (orderList.Count == 0))
            {
                orderList = Order.FindAllOrders(Convert.ToDateTime("1/1/2005"), DateTime.Now);
            }
            if (orderList.Count > 0)
            {
                CacheHelper.Add<List<Order>>(orderList, Config.cachekey_OrderList, Config.cacheDuration_OrderList);
            }
            return orderList;
        }

        public bool Save(Order o)
        {
            //CacheHelper.Clear(Config.cachekey_OrderList);
            o.SaveDB();
            return true;
        }
    }
}