﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FMWebApp451.Interfaces;
using FM2015.Helpers;

namespace FM2015.Models
{
    public class OrdersWithDetailProvider : IOrdersWithDetailProvider
    {
        public List<OrdersWithDetail> GetMany(DateTime startDate)
        {
            CheckDate(startDate); //exception if startDate is not right for some reason
            List<OrdersWithDetail> theList = new List<OrdersWithDetail>();

            string sql = @"
                SELECT top 20000 o.orderid, o.orderdate, o.customerid, p.IsFMSystem, c.FirstName, c.LastName, c.Email  
                FROM orders o, orderdetails od, products p, orderActions oa, customers c 
                WHERE o.orderid = od.orderid AND o.orderid = oa.orderid AND od.productid = p.productid AND o.customerid = c.customerid 
                    AND o.orderdate between @startDate AND getDate() 
	                AND oa.OrderActionType = (select top 1 orderactionType from orderactions where orderid = o.orderid AND orderactiontype = 2 order by orderactionId desc) 
                ORDER BY o.orderid ASC 
            ";
            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                OrdersWithDetail obj = new OrdersWithDetail();
                obj.OrdersWithDetailsId = Convert.ToInt32(dr["OrderId"]);
                obj.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                obj.CustomerId = Convert.ToInt32(dr["CustomerId"]);
                //obj.ProductId = Convert.ToInt32(dr["ProductId"]);
                obj.IsFMSystem = Convert.ToBoolean(dr["IsFMSystem"]);
                obj.FirstName = dr["FirstName"].ToString();
                obj.LastName = dr["Lastname"].ToString();
                obj.Email = dr["Email"].ToString();
                theList.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return theList;
        }

        public static void CheckDate(DateTime date)
        {
            if (date > DateTime.Now)
            {
                throw new ArgumentOutOfRangeException("OrdersWithDetailProvider: startDate is out-of-range: " + date.ToString());
            }
        }
    }
}