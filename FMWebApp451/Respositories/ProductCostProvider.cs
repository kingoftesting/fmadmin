﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Newtonsoft.Json;
using FMWebApp.Helpers;
using FM2015.Helpers;
using FMWebApp451.Interfaces;
using AdminCart;

namespace FM2015.Models
{
    public class ProductCostProvider : IProductCostProvider
    {
        #region Methods

        public ProductCost GetById(int productCostId)
        {
            List<ProductCost> productCostList = GetAll();
            ProductCost productCost = new ProductCost();
            if (productCostList.Any(p => p.ProductCostID == productCostId))
            {
                productCost = (from p in productCostList where p.ProductID == productCostId select p).FirstOrDefault();
            }
            return productCost;
        }

        public ProductCost GetByProduct(Product product)
        {
            List<ProductCost> productCostList = GetAll();
            ProductCost productCost = new ProductCost();
            if (productCostList.Any(p => p.ProductID == product.ProductID))
            {
                productCost = (from p in productCostList where p.ProductID == product.ProductID select p).FirstOrDefault();
            }
            return productCost;
        }


        public int Save(ProductCost productCost)
        {
            return productCost.Save(productCost.ProductCostID);
        }
        #endregion

        public List<ProductCost> GetAll()
        {
            return ProductCost.GetAll();
        }
    }
}