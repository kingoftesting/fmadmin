using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Newtonsoft.Json;
using FMWebApp.Helpers;
using FM2015.Helpers;
using FMWebApp451.Interfaces;
using AdminCart;

namespace FM2015.Models
{
    public class ProductProvider : IProductProvider
    {

        #region Methods

        public Product GetById(int productId)
        {
            List<Product> productList = GetAll();
            Product product = new Product();
            if (productList.Any(p => p.ProductID == productId))
            {
                product = (from p in productList where p.ProductID == productId select p).FirstOrDefault();
            }
            return product;         
        }

        public Product GetBySku(string sku)
        {
            List<Product> productList = GetAll();
            Product product = new Product();
            if (productList.Any(p => p.Sku == sku))
            {
                product = (from p in productList where p.Sku == sku select p).FirstOrDefault();
            }
            return product;
        }

        public Product GetByShopifyProductId(long shopifyProductID)
        {
            List<Product> productList = GetAll();
            Product product = new Product();
            if (productList.Any(p => p.PartnerID == shopifyProductID))
            {
                product = (from p in productList where p.PartnerID == shopifyProductID select p).FirstOrDefault();
            }
            return product;
        }


        public int Save(Product product)
        {
            return product.Save(product.ProductID);
        }

        public List<Product> GetAll()
        {
            return Product.GetProducts();
        }

        public List<Product> GetAll(bool customerOnly)
        {
            List<Product> productList = Product.GetProducts();
            if (customerOnly)
            {
                productList = (from p in productList where (p.CSOnly == !customerOnly) && (p.Enabled == true) select p).ToList();
            }
            else
            {
                productList = (from p in productList where (p.CSOnly == customerOnly) && (p.Enabled == true) select p).ToList();
            }

            //string sql = @"
            //SELECT *  
            //FROM Products 
            //";
            //if (customerOnly) { sql += "WHERE enabled = 1 AND csonly = 0 "; }
            //else { sql += "WHERE enabled = 1 "; }

            //sql += "ORDER BY Sort ASC";

            return productList;
        }
        #endregion

        public static void PostFMProductToShopify(Product product)
        {
            ShopifyAgent.ShopifyProductSingle shopProduct = new ShopifyAgent.ShopifyProductSingle();
            shopProduct.product.Body_html = product.ShortDescription;
            shopProduct.product.Title = product.Name;
            shopProduct.product.Vendor = "FaceMaster";
            shopProduct.product.Product_type = product.PartnerProductType;

            List<ShopifyAgent.ProductVariant> vList = new List<ShopifyAgent.ProductVariant>();
            ShopifyAgent.ProductVariant variant = new ShopifyAgent.ProductVariant();
            variant.Sku = product.Sku;
            variant.Fulfillment_service = "manual";
            variant.Grams = Convert.ToDouble(product.Weight * Convert.ToDecimal(453.59)); //pounds to grams
            variant.Weight = Convert.ToDouble(product.Weight);
            variant.Weight_unit = "lb";
            variant.Price = product.SalePrice;
            variant.Requires_shipping = product.IsShippable;
            variant.Taxable = true;
            vList.Add(variant);
            shopProduct.product.variants = vList.ToArray();

            List<ShopifyAgent.ProductImage> imgList = new List<ShopifyAgent.ProductImage>();
            ShopifyAgent.ProductImage image = new ShopifyAgent.ProductImage();
            image.Src = "http://www.facemaster.com/images/products/" + product.ThumbnailImage;
            imgList.Add(image);
            shopProduct.product.images = imgList.ToArray();

            shopProduct.product.image.Src = "http://www.facemaster.com/images/products/" + product.ThumbnailImage;

            //Debug... using a static string
            string jsonStr = @"{""product"":{""title"":""#TITLE#"",""body_html"":""#BODY#"",""vendor"":""FaceMaster"",""product_type"":""FaceMaster"",""images"":[{""src"":""#IMAGE-SRC#""}],""variants"":[{""title"":""#TITLE#"",""price"":""#PRICE#"",""sku"":""#SKU#"",""weight"":""#WEIGHT#""}]}}";
            jsonStr = jsonStr.Replace("#TITLE#", product.Name);
            jsonStr = jsonStr.Replace("#BODY#", product.ShortDescription);
            jsonStr = jsonStr.Replace("#IMAGE-SRC#", "http://www.facemaster.com/images/products/" + product.ThumbnailImage);
            jsonStr = jsonStr.Replace("#PRICE#", product.SalePrice.ToString("C"));
            jsonStr = jsonStr.Replace("#SKU#", product.Sku);
            jsonStr = jsonStr.Replace("#WEIGHT#", product.Weight.ToString());
            jsonStr = jsonStr.Replace("\r", ""); //get rid of new line stuff; tends to blow the API processor up
            jsonStr = jsonStr.Replace("\n", "");

            //string jsonStr = JsonConvert.SerializeObject(shopProduct);
            string jsonURL = "admin/products.json";
            //response holds new product json from Shopify, if new product was created OK
            string response = JsonHelpers.POSTtoShopify(jsonURL, jsonStr);

            try
            {
                ShopifyAgent.ShopifyProductSingle responseProduct = JsonConvert.DeserializeObject<ShopifyAgent.ShopifyProductSingle>(response);
            }
            catch
            {
                //response wasn't a good Product response
            }


        }

        private static void SendExceptionEmail(string msg, Exception ex)
        {
            dbErrorLogging.LogError(msg, ex);
            //string mailFrom = ConfigurationManager.AppSettings["MailFrom"].ToString();
            //string mailTo = ConfigurationManager.AppSettings["MailTo"].ToString(); 
            //Mail.SendMail(mailFrom, mailTo, "Product Exception", msg + "sysMsg: " + ex.Message + ex.StackTrace + ex.TargetSite + ex.ToString());
            //throw ex;
        }

    }
}
