﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMWebApp451.Interfaces;
using FM2015.Models;

namespace FMWebApp451.Respositories
{
    public class RegistrationProvider : IRegistration
    {
        public Registration GetById(int id)
        {
            List<Registration> theList = GetAll();
            Registration registration = new Registration();
            if (theList.Any(r => r.RegistrationId == id))
            {
                registration = (from r in theList where r.RegistrationId == id select r).FirstOrDefault();
            }
            return registration;
        }

        public int Save(Registration registration)
        {
            if (registration.RegistrationId == 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<Registration> GetAll()
        {
            List<Registration> theList = new List<Registration>();

            return theList;
        }
    }
}