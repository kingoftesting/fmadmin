﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using AdminCart;
using FM2015.Helpers;
using FMWebApp451.Interfaces;
using ShopifyAgent;

namespace FM2015.Models
{
    public class RiskProvider : IRiskProvider
    {
        
        public ShopOrderRisk GetByShopifyName(int ShopifyName)
        {
            long shopOrderId = ShopifyAgent.ShopifyOrder.GetShopifyIDFromName(ShopifyName.ToString(), Config.CompanyID());
            List<ShopOrderRisk> theList = GetAll();
            ShopOrderRisk risk = new ShopOrderRisk();
            if (theList.Any(r => r.Order_id == shopOrderId))
            {
                risk = (from r in theList where (r.Order_id == shopOrderId) select r).FirstOrDefault();
            }
            
            return risk;
        }

        public int Save(ShopOrderRisk risk)
        {
            CacheHelper.Clear(Config.cachekey_RiskList);
            int result = 0;
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (risk.RiskId == 0)
            {
                //INSERT
                string sql = @"
                INSERT INTO Risks 
                ([Id], [OrderId], [ShopifyName], [Message], [Recommendation], [Score], [Source], [Display], [CauseCancel]) 
                VALUES(@Id, @OrderId, @ShopifyName, @Message, @Recommendation, @Score, @Source, @Display, @CauseCancel); 
                SELECT ID=@@identity; 
            ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, risk.Id, risk.Order_id, risk.ShopifyName, risk.Message, risk.Recommendation, risk.Score,
                    risk.Source, risk.Display, risk.Cause_cancel);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ID"]);
                }
                else
                {
                    result = 99;
                }
            }
            else
            {
                //UPDATE
                string sql = @" 
                UPDATE Risks   
                SET  
                    [Id] = @Id, 
                    [OrderId] = @OrderId, 
                    [ShopifyName] = @ShopifyName,
                    [Message] = @Message, 
                    [Recommendation] = @Recommendation, 
                    [Score] = @Score, 
                    [Source] = @Source, 
                    [Display] = @Display, 
                    [CauseCancel] = @CauseCancel  
                WHERE [RiskId] = @RiskId  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, risk.Id, risk.Order_id, risk.ShopifyName, risk.Message, risk.Recommendation, risk.Score,
                    risk.Source, risk.Display, risk.Cause_cancel, risk.RiskId);
                DBUtil.Exec(sql, mySqlParameters);
            }

            return result;
        }

        public List<ShopOrderRisk> GetAll()
        {
            List<ShopOrderRisk> theList = new List<ShopOrderRisk>();
            CacheHelper.Get(AdminCart.Config.cachekey_RiskList, out theList);
            if ((theList == null) || (theList.Count == 0))
            {
                theList = new List<ShopOrderRisk>();
                string sql = @"
                SELECT * 
                FROM Risks
                ORDER BY OrderId DESC 
            ";
                //SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, param);
                SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql);

                while (dr.Read())
                {
                    ShopOrderRisk obj = new ShopOrderRisk();
                    obj.RiskId = Convert.ToInt32(dr["RiskId"]);
                    obj.Id = Convert.ToInt64(dr["Id"]);
                    obj.Order_id = Convert.ToInt64(dr["OrderId"]);
                    obj.ShopifyName = Convert.ToInt32(dr["ShopifyName"]);
                    obj.Message = dr["Message"].ToString();
                    obj.Recommendation = dr["Recommendation"].ToString();
                    obj.Score = Convert.ToDecimal(dr["Score"]);
                    obj.Source = dr["Source"].ToString();
                    obj.Display = Convert.ToBoolean(dr["Display"]);
                    obj.Cause_cancel = (dr["CauseCancel"]).ToString();

                    theList.Add(obj);
                }
                if (dr != null) { dr.Close(); }

            }
            if (theList.Count > 0)
            {
                CacheHelper.Add<List<ShopOrderRisk>>(theList, Config.cachekey_RiskList, Config.cacheDuration_RiskList);
            }

            return theList;
        }
    }
}