﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdminCart;
using FM2015.Helpers;
using FMWebApp451.Interfaces;
using ShopifyAgent;

namespace FM2015.Models
{
    public class ShopOrderNoteProvider : IShopOrderNote
    {
        public ShopOrderNote GetByShopName(int shopName)
        {
            List<ShopOrderNote> theList = GetAll();
            ShopOrderNote shopNote = new ShopOrderNote();
            if (theList.Any(s => s.ShopOrderName == shopName))
            {
                shopNote = (from s in theList where (s.ShopOrderName == shopName) select s).FirstOrDefault();
            }
            return shopNote;
        }

        public List<ShopOrderNote> GetAll()
        {
            List<ShopOrderNote> theList = new List<ShopOrderNote>();
            CacheHelper.Get(Config.cachekey_ShopOrderNoteList, out theList);
            if ((theList == null) || (theList.Count == 0))
            {
                theList = new List<ShopOrderNote>(); // reinitialize because CacheHelper just set to NULL
                List<ShopifyOrder> shopList = ShopifyAgent.ShopifyOrder.GetShopifyOrderNoteList();
                foreach (ShopifyOrder shop in shopList)
                {
                    if (shop.Note.Substring(0,1) != "{")  //filter out all customer service order notes (ie, { FM97756 })
                    {
                        ShopOrderNote shopNote = new ShopOrderNote()
                        {
                            ShopOrderName = Convert.ToInt32(shop.Name),
                            ShopNote = shop.Note,
                        };
                        theList.Add(shopNote);
                    }
                }
                if (theList.Count > 0)
                {
                    CacheHelper.Add<List<ShopOrderNote>>(theList, Config.cachekey_ShopOrderNoteList, Config.cacheDuration_ShopOrderNoteList);
                }
            }
            return theList;
        }

        public void ClearCache()
        {
            CacheHelper.Clear(Config.cachekey_ShopOrderNoteList);
        }
    }
}