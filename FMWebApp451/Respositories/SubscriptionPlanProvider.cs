﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FM2015.Models;
using FMWebApp451.Interfaces;
using AdminCart;
using FM2015.Helpers;

namespace FMWebApp451.Respositories
{
    public class SubscriptionPlanProvider : ISubscriptionPlans
    {
        public SubscriptionPlans GetByPlanId(string planId)
        {
            List<SubscriptionPlans> theList = GetAll();
            SubscriptionPlans subPlan = (from sp in theList where (sp.PlanId.ToLower() == planId.ToLower()) select sp).FirstOrDefault();
            if (subPlan == null)
            {
                subPlan = new SubscriptionPlans();
            }
            return subPlan;

        }

        public List<SubscriptionPlans> GetAll()
        {
            List<SubscriptionPlans> subPlanList = new List<SubscriptionPlans>();
            CacheHelper.Get(Config.cachekey_SubscriptionPlanList, out subPlanList);
            if ((subPlanList == null) || (subPlanList.Count == 0))
            {
                subPlanList = new List<SubscriptionPlans>();
                string sql = @" 
                SELECT * FROM  SubscriptionPlans 
            ";
                //SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, planId);
                SqlDataReader dr = DBUtil.FillDataReader(sql);
                while (dr.Read())
                {
                    SubscriptionPlans subPlan = new SubscriptionPlans();
                    subPlan.Id = Convert.ToInt32(dr["ID"]);
                    subPlan.PlanId = dr["PlanId"].ToString();
                    subPlan.Name = dr["Name"].ToString();
                    subPlan.Amount = Convert.ToDecimal(dr["Amount"]);
                    subPlan.Created = Convert.ToDateTime(dr["Created"].ToString());
                    subPlan.Interval = dr["Interval"].ToString();
                    subPlan.Interval_Count = Convert.ToInt32(dr["IntervalCount"].ToString());
                    if (dr["Installments"] == DBNull.Value)
                    {
                        subPlan.Installments = null;
                    }
                    else
                    {
                        subPlan.Installments = Convert.ToInt32(dr["Installments"].ToString());
                    }
                    subPlanList.Add(subPlan);
                }
                if (dr != null) { dr.Close(); }
            }
            if (subPlanList.Count > 0)
            {
                CacheHelper.Add<List<SubscriptionPlans>>(subPlanList, Config.cachekey_SubscriptionPlanList, Config.cacheDuration_SubscriptionPlanList);
            }
            return subPlanList;
        }
    }
}