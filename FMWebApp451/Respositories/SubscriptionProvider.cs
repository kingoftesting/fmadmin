﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using FM2015.Models;
using FMWebApp451.Interfaces;
using AdminCart;
using FM2015.Helpers;

namespace FMWebApp451.Respositories
{
    public class SubscriptionProvider : ISubscriptions
    {
        public List<Subscriptions> GetByStripeCustomerId(string stripeCustomerId)
        {
            List<Subscriptions> theList = GetAll();
            theList = (from s in theList where s.StripeCusID == stripeCustomerId select s).ToList();
            if (theList == null)
            {
                theList = new List<Subscriptions>();
            }

            return theList;
        }

        public List<Subscriptions> GetAll()
        {
            List<Subscriptions> theList = new List<Subscriptions>();
            CacheHelper.Get(Config.cachekey_SubscriptionsList, out theList);
            if ((theList == null) || (theList.Count == 0))
            {
                theList = Subscriptions.GetSubscribersList();
            }
            if (theList.Count > 0)
            {
                CacheHelper.Add<List<Subscriptions>>(theList, Config.cachekey_SubscriptionsList, Config.cacheDuration_SubscriptionsList);
            }
            return theList;
        }

        public bool Update(Subscriptions sub)
        {
            if (sub.ID != 0)
            {
                sub.Save(sub.ID);
            }

            return true;
        }

        public bool Insert(Subscriptions sub)
        {
            sub.Save(0);
            return true;
        }
    }
}