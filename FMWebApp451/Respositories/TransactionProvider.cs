﻿using System;
using System.Collections.Generic;
using FMWebApp451.Interfaces;
using AdminCart;

namespace FM2015.Models
{
    public class TransactionProvider : ITransaction
    {
        public List<Transaction> GetAll(DateTime startDate, DateTime endDate)
        {
            List<Transaction> tList = Transaction.GetTransactionList(startDate.ToString(), endDate.ToString());
            return tList;
        }
    }
}