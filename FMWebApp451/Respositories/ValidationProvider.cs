﻿using System.Collections.Generic;
using System.Linq;
using FMWebApp451.Interfaces;
using FM2015.Models;
using System.Data.SqlClient;
using System;
using FM2015.Helpers;

namespace FMWebApp451.Respositories
{
    public class ValidationProvider : IValidationProvider
    {
        public Validation GetById(int valId)
        {
            List<Validation> validationList = GetAll();
            Validation validation = new Validation();
            if (validationList.Any(v => v.ValidationId == valId))
            {
                validation = (from v in validationList where v.ValidationId == valId select v).FirstOrDefault();
            }

            return validation;
        }

        public int Save(Validation validation)
        {
            int result = 0;
            SqlParameter[] mySqlParameters = null;
            SqlDataReader dr = null;
            if (validation.ValidationId == 0)
            {
                //INSERT
                string sql = @"
                INSERT INTO Validations 
                ([Date], [By], [SerialNum], [Pass], [Comments]) 
                VALUES(@Date, @By, @SerialNum, @Pass, @Comments); 
                SELECT ID=@@identity; 
            ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql, validation.Date, validation.By, validation.SerialNum, validation.Pass, validation.Comments);
                dr = DBUtil.FillDataReader(sql, mySqlParameters);
                if (dr.Read())
                {
                    result = Convert.ToInt32(dr["ID"]);
                }
                else
                {
                    result = 99;
                }
            }
            else
            {
                //UPDATE
                string sql = @" 
                UPDATE Validations   
                SET  
                    [Date] = @Date,  
                    [By] = @By,  
                    [SerialNum] = @SerialNum,  
                    [Pass] = @Pass,
                    [Comments] = @Comments  
                WHERE [ValidationID] = @ID  
                ";
                mySqlParameters = DBUtil.BuildParametersFrom(sql,
                    validation.Date, validation.By, validation.SerialNum, validation.Pass, validation.Comments, validation.ValidationId );
                DBUtil.Exec(sql, mySqlParameters);
            }
            return result;
        }

        public List<Validation> GetAll()
        {
            List<Validation> theList = new List<Validation>();

            string sql = @"
                SELECT * 
                FROM Validations
                ORDER BY ValidationId ASC 
            ";
            //SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, param);
            SqlDataReader dr = FM2015.Helpers.DBUtil.FillDataReader(sql);

            while (dr.Read())
            {
                Validation obj = new Validation();
                obj.ValidationId = Convert.ToInt32(dr["ValidationId"]);
                obj.Date = Convert.ToDateTime(dr["Date"]);
                obj.By = dr["By"].ToString();
                obj.Pass = Convert.ToBoolean(dr["Pass"]);
                obj.SerialNum = Convert.ToInt64(dr["SerialNum"].ToString());
                obj.Comments = dr["Comments"].ToString();
                theList.Add(obj);
            }
            if (dr != null) { dr.Close(); }
            return theList;
        }
    }
}