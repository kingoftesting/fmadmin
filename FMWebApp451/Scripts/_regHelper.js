﻿
$(document).ready(function () {
    if ($('#ddlShipState').val() != '') {
        $('#ddlShipState').show();
        $('#txtShipState').hide();
    }
    else {
        $('#ddlShipState').hide();
        $('#txtShipState').show();
    }
    if ($('#ddlBillState').val() != '') {
        $('#ddlBillState').show();
        $('#txtBillState').hide();
    }
    else {
        $('#ddlBillState').hide();
        $('#txtBillState').show();
    }

});

    $(function () {
        $('#ddlBillCountry').change(function () {
            $.getJSON('/Customers/ddlStateList/' + $('#ddlBillCountry').val(), function (data) {
                var items = '';
                $.each(data, function (i, state) {
                    items += "<option value='" + state.Value + "'>" + state.Text + "</option>";
                });
                if (items == '') {
                    $('#ddlBillState').hide();
                    $('#txtBillState').show();
                    $('#txtBillState').val('');
                }
                else {
                    $('#txtBillState').hide();
                    $('#ddlBillState').show();
                    $('#ddlBillState').html(items);
                }
            });
        });
    });

    $(function () {
        $('#ddlShipCountry').change(function () {
            $.getJSON('/Customers/ddlStateList/' + $('#ddlShipCountry').val(), function (data) {
                var items = '';
                $.each(data, function (i, state) {
                    items += "<option value='" + state.Value + "'>" + state.Text + "</option>";
                });
                if (items == '') {
                    $('#ddlShipState').hide();
                    $('#txtShipState').show();
                    $('#txtShipState').val('State/Province');
                }
                else {
                    $('#txtShipState').hide();
                    $('#ddlShipState').show();
                    $('#ddlShipState').html(items);
                }
            });
        });
    });

$(function () {
    $('#chkShipBox').change(function () {
        if ($('#chkShipBox').is(':checked')) {
            //$('#ShipAddress_Street').val($('#BillAddress_Street').val);
            //$('#ShipAddress_Street2').val($('#BillAddress_Street2').val);
            //$('#ShipAddress_City').val($('#BillAddress_City2').val);
            //$('#ShipAddress_Zip').val($('#BillAddress_Zip2').val);
            ($('#frmShipAddress')).Hide
        }
        else {
            ($('#frmShipAddress')).Show
        }
    });
});

function ToggleShipVis() {
    if ((document.getElementById(event.target.id)).checked) {
        //alert("checked; ID of element: " + event.target.id + "BillAddress.Street: " + document.getElementById('BillAddress_Street').value);
        document.getElementById('ShipAddress_Street').value = document.getElementById('BillAddress_Street').value;
        //alert("checked; ID of element: " + event.target.id + "ShipAddress.Street: " + document.getElementById('ShipAddress_Street').value);
        document.getElementById('ShipAddress_Street2').value = document.getElementById('BillAddress_Street2').value;
        document.getElementById('ShipAddress_City').value = document.getElementById('BillAddress_City').value;
        document.getElementById('ShipAddress_Zip').value = document.getElementById('BillAddress_Zip').value;
        document.getElementById('frmShipAddress').style.display = "none";
    }
    else {
        //alert("unchecked; ID of element: " + event.target.id);
        document.getElementById('frmShipAddress').style.display = "block";
    }
};

function SetStateVals() {
    //alert('SetStateVals: txtShipState=' + $('#txtShipState').val())
    if ($('#txtShipState').val() != '') {
        //alert('SetStateVals: #txtShipState not empty, set #hdnShipState')
        $('#hdnShipState').val($('#txtShipState').val());
        //alert('SetStateVals: #hdnShipState= ' + $('#hdnShipState').val())

    }
    else {
        $('#hdnShipState').val('');
        //alert('SetStateVals: #hdnShipState set to empty')

    }
    return false;
};


