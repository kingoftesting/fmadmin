using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FM2015.Helpers;

public partial class SearchResult : System.Web.UI.Page
{
    private string SearchTerm;
    private bool SearchSuccess = false;
    private int resultcount = 0;


    Certnet.Cart cart;

    protected void Page_Load(object sender, EventArgs e)
    {
        cart = new Certnet.Cart();
        cart.Load(Session["cart"]);

        if (Request["SearchBox"] != null)
        {
            if (Request["SearchBox"].ToString() != "Search...")
            {
                SearchTerm = Request["SearchBox"];
                SearchTerm = SearchTerm.Replace("'", "''");
                try
                {
                    if (String.Compare(SearchTerm.Substring(0, 9), "Search...") == 0)
                    { SearchTerm = SearchTerm.Remove(0, 9); }
                }
                catch { }

                lblsrch.Text = SearchTerm;

                bodytext.Text = "";

                SearchPages();
                SearchFAQs();
                SearchTestimonials();

                if (SearchSuccess)
                {
                    String sql = "INSERT Searches VALUES('" + SearchTerm + "', " + cart.SiteCustomer.CustomerID.ToString() + ", GETDATE(), '1')";
                    DBUtil.Exec(sql);
                }
                else
                {
                    bodytext.Text = "Your search for <strong>";
                    bodytext.Text += SearchTerm;
                    bodytext.Text += "</strong> produced no results."; //biz.GetPageText(PageID);
                    bodytext.Text += "<br /><br /><strong>Suggestion</strong>: check your spelling and/or shorten your search term <br />(for example: <strong>where do i buy batteries</strong> --> <strong>batteries</strong>)";
                    bodytext.Text += "<br /><br />If you were searching for:</a>";
                    bodytext.Text += "<br /><strong>FaceMaster products (serums, refills, etc.)</strong>:&nbsp; <a href='http://www.facemaster.com/products.aspx'>FaceMaster Products</a>";
                    bodytext.Text += "<br /><strong>FaceMaster instructions or videos</strong>:&nbsp; <a href='http://www.facemaster.com/operatinginstructions.aspx'>Operating Instructions</a>";
                    bodytext.Text += "<br /><br />If we can assist you in your search please send us an email:&nbsp; <a href='http://www.facemaster.com/ContactUs.aspx'>Contact Us</a>";
                    bodytext.Text += "";


                    String sql = "INSERT Searches VALUES('" + SearchTerm + "', " + cart.SiteCustomer.CustomerID.ToString() + ", GETDATE(), '0')";
                    DBUtil.Exec(sql);
                }
            }
        }

    }

    private void SearchPages()
    {
        string sql = "SELECT PageID, PageLink, PageTitle FROM Pages where publicdisplay=1 and pageid in( " +
                "select pageid from paragraphs where paragraphtext like '%" + SearchTerm + "%') ";
        SqlDataReader dr = DBUtil.FillDataReader(sql);

        if (dr.HasRows)
        {
            bodytext.Text += "Website Pages: <br /><br />";
            resultcount = 0;
            while (dr.Read())
            {
                    resultcount++;
                    string pagelink = dr["PageLink"].ToString();
                    if (pagelink == "") { pagelink = "Page.aspx?PageID=" + dr["PageID"]; }
                    bodytext.Text += resultcount.ToString() + ". <a href=" + pagelink + ">" + dr["PageTitle"].ToString() + "</a><br><br>";
            }
            dr.Close();
            SearchSuccess = true;
         }
    }

    private void SearchFAQs()
    {
        String sql = "SELECT DISTINCT FaqID, Category, Question, Answer ";
        sql += " FROM Faqs";
        sql += " WHERE (Category LIKE '%" + SearchTerm + "%'";
        sql += " OR Question LIKE '%" + SearchTerm + "%'";
        sql += " OR Answer LIKE '%" + SearchTerm + "%')";
        sql += "AND Enabled=1";

        SqlDataReader dr = DBUtil.FillDataReader(sql);

        if (dr.HasRows)
        {
            resultcount = 0; 
            bodytext.Text += "<br />FAQ results: <br /><br />";
            while (dr.Read())
            {

                resultcount++;
                bodytext.Text += resultcount.ToString() + ". <a href=faq.aspx?Category=" + dr["Category"].ToString() + ">FAQs</a>";
                bodytext.Text += " Category: " + dr["Category"].ToString() + "<br />";
                bodytext.Text += " <strong>Question:</strong> " + dr["Question"].ToString() + "<br />";
                bodytext.Text += " <strong>Answer:</strong> " + dr["Answer"].ToString() + "<br />";
                bodytext.Text += "<br /><br />";
            }
            dr.Close();
            SearchSuccess = true;
        }
    }


    private void SearchTestimonials()
    {
    }


}
