﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMWebApp451.Interfaces;
using PayWhirl;

namespace FMWebApp451.Services.PayWhirlServices
{
    public class PayWhirlService : IPayWhirlService
    {
        public PayWhirlSubscriber FindSubscriberByEmail(string email)
        {
            PayWhirlSubscriber p = PayWhirlBiz.FindSubscriberByEmail(email);
            return p;
        }
    }
}