﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminCart;
using FM2015.Models;
using FMWebApp451.Interfaces;
using Stripe;

namespace FMWebApp451.Services.StripeServices
{
    public class StripeServices : IStripeService
    {
        private readonly IPayWhirlService paywhirlService;

        public StripeServices()
        { }

        public StripeServices(IPayWhirlService paywhirlService)
        {
            this.paywhirlService = paywhirlService;
        }

        //******* CUSTOMER STUFF
        public List<StripeCustomer> GetStripeCustomerList(string param, out string errMsg)
        {
            errMsg = string.Empty;
            StripeCustomerListOptions stripeCustomerListOptions = new StripeCustomerListOptions();

            string email = ParseCustomerListParam(param);
            if (!string.IsNullOrEmpty(email))
            {
                stripeCustomerListOptions = new StripeCustomerListOptions()
                {
                    Email = email,
                    Limit = 10,
                };
            }

            IEnumerable<StripeCustomer> stripeCustomerList = new List<StripeCustomer>();
            var customerService = new StripeCustomerService();

            try
            {
                stripeCustomerList = customerService.List(stripeCustomerListOptions); // optional StripeCustomerListOptions
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "GetCustomerList: customerService.List(): Stripe Error: param = " + param);
            }
            return stripeCustomerList.ToList();
        }

        public StripeCustomer GetStripeCustomer(string id, out string errMsg)
        {
            errMsg = string.Empty;
            StripeCustomer stripeCustomer = new StripeCustomer();
            StripeCustomerService customerService = new StripeCustomerService();

            try
            {
                stripeCustomer = customerService.Get(id);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "GetStripeCustomer: customerService.Get: Stripe Error: StripeCustomerId = " + id);
            }
            return stripeCustomer;
        }

        public StripeCustomer CreateStripeCustomer(StripeCustomerCreateOptions stripeCustomerCreateOptions, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeCustomer UpdateStripeCustomer(string stripeCustomerId, StripeCustomerUpdateOptions stripeCustomerUpdateOptions, out string errMsg)
        {
            errMsg = string.Empty;
            StripeCustomer stripeCustomer = new StripeCustomer();
            try
            {
                StripeCustomerService customerService = new StripeCustomerService();
                stripeCustomer = customerService.Update(stripeCustomerId, stripeCustomerUpdateOptions);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "UpdateStripeCustomer: customerService.Update: Stripe Error: StripeCustomerId = " + stripeCustomerId);
            }
            return stripeCustomer;
        }


        //******* SUBSCRIPTION STUFF
        public List<StripeSubscription> GetCustomerSubscriptionList(string id, out string errMsg)
        {
            errMsg = string.Empty;
            IEnumerable<StripeSubscription> stripeCustomerSubscriptionList = new List<StripeSubscription>();

            StripeSubscriptionService subscriptionService = new StripeSubscriptionService(); //get list of all subscriptions for customer
            StripeSubscriptionListOptions stripeSubscriptionListOptions = new StripeSubscriptionListOptions()
            {
                CustomerId = id,
            };
            try
            {
                stripeCustomerSubscriptionList = subscriptionService.List(stripeSubscriptionListOptions);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: GetCustomerSubscriptionList: SubscriptionService: Stripe Error: StripeCustomerId = " + id);
            }
            return stripeCustomerSubscriptionList.ToList();
        }


        //******* PRODUCT & PLAN STUFF
        public StripeProduct GetStripeProduct(string productId, out string errMsg)
        {
            errMsg = string.Empty;
            StripeProduct stripeProduct = new StripeProduct();
            try
            {
                var productService = new StripeProductService();
                stripeProduct = productService.Get(productId);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: GetStripeProduct: productService.Get: Stripe Error: StripeProductId = " + productId);
            }
            return stripeProduct;
        }

        public List<StripePlan> GetStripePlanList(out string errMsg)
        {
            errMsg = string.Empty;
            IEnumerable<StripePlan> stripePlanList = new List<StripePlan>();
            try
            {
                StripePlanService planService = new StripePlanService();
                StripeProductService productService = new StripeProductService();
                stripePlanList = planService.List(new StripePlanListOptions { Limit = 50 }); // optional StripeListOptions
                foreach (StripePlan stripePlan in stripePlanList)
                {
                    StripeProduct stripeProduct = productService.Get(stripePlan.ProductId);
                    stripePlan.Product = stripeProduct;
                }
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeServices: GetStripePlanList: planService.List: Stripe Error");
            }

            return stripePlanList.ToList();
        }

        public StripePlan GetStripePlanById(string planId, out string errMsg)
        {
            errMsg = string.Empty;
            List<StripePlan> stripePlans = GetStripePlanList(out errMsg);
            StripePlan plan = (from p in stripePlans where p.Id == planId select p).FirstOrDefault();
            if (plan == null)
            {
                plan = new StripePlan();
            }
            return plan;
        }

        public StripePlan GetStripePlanByName(string planName, out string errMsg)
        {
            errMsg = string.Empty;
            List<StripePlan> stripePlans = GetStripePlanList(out errMsg);
            StripePlan plan = (from p in stripePlans where p.Product.Name == planName select p).FirstOrDefault();
            if (plan == null)
            {
                plan = new StripePlan();
            }
            return plan;
        }

        public StripePlan CreateStripePlan(StripePlanCreateOptions planOptions, out string errMsg)
        {
            errMsg = string.Empty;
            StripePlan plan = new StripePlan();
            try
            {
                StripePlanService planService = new StripePlanService();
                plan = planService.Create(planOptions);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeService: CreateStripePlan: Stripe Error: plan Name = " + plan.Product.Name);
            }
            return plan;
        }

        public StripePlan UpdateStripePlan(string planId, StripePlanUpdateOptions planOptions, out string errMsg)
        {
            errMsg = string.Empty;
            StripePlan plan = new StripePlan();
            try
            {
                StripePlanService planService = new StripePlanService();
                plan = planService.Update(planId, planOptions);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeService: UpDateStripePlan: Stripe Error: plan name = " + plan.Product.Name);
            }
            return plan;
        }

        public bool DeleteStripePlan(string planId, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {
                StripePlanService planService = new StripePlanService();
                planService.Delete(planId);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeService: DeleteStripePlan: Stripe Error: planId = " + planId);
                return false;
            }
            return true;
        }


        //******* TOKEN STUFF
        public StripeToken CreateStripeToken(StripeTokenCreateOptions tokenOptions, out string errMsg)
        {
            errMsg = string.Empty;
            StripeToken stripeToken = new StripeToken();
            try
            {
                StripeTokenService tokenService = new StripeTokenService();
                stripeToken = tokenService.Create(tokenOptions);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeService: CreateStripeToken: Stripe Error");
            }
            return stripeToken;
        }

        public StripeToken GetStripeToken(string tokenId, out string errMsg)
        {
            errMsg = string.Empty;
            StripeToken stripeToken = new StripeToken();
            try
            {
                StripeTokenService tokenService = new StripeTokenService();
                stripeToken = tokenService.Get(tokenId);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeService: GetStripeToken: Stripe Error: tokenId = " + tokenId);
            }
            return stripeToken;
        }


        //******* CREDIT CARD STUFF
        public List<StripeCard> GetStripeCustomerCards(string id, out string errMsg)
        {
            errMsg = string.Empty;
            IEnumerable<StripeCard> stripeCustomerCardList = new List<StripeCard>();
            try
            {
                StripeCardService cardService = new StripeCardService(); //get list of all credit cards for customer
                stripeCustomerCardList = cardService.List(id);

            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: GetStripeCustomerCards: cardService.List: Stripe Error: CustomerId = " + id);
            }
            return stripeCustomerCardList.ToList();
        }

        public StripeCard GetStripeCustomerCard(string stripeCustomerId, string stripeCardId, out string errMsg)
        {
            errMsg = string.Empty;
            List<StripeCard> stripeCards = GetStripeCustomerCards(stripeCustomerId, out errMsg);
            StripeCard stripeCard = (from c in stripeCards where c.Id == stripeCardId select c).FirstOrDefault();
            if (stripeCard == null)
            {
                stripeCard = new StripeCard();
            }

            return stripeCard;
        }

        public StripeCard CreateStripeCard(string stripeCustomerId, StripeCardCreateOptions cardCreateOptions, out string errMsg)
        {
            errMsg = string.Empty;
            StripeCard stripeCard = new StripeCard();
            try
            {
                StripeCardService cardService = new StripeCardService();
                stripeCard = cardService.Create(stripeCustomerId, cardCreateOptions);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: CreateStripeCard: cardService.Create: Stripe Error: StripeCustomerId = " + stripeCustomerId);
            }
            return stripeCard;
        }

        public StripeCard UpdateStripeCard(string stripeCustomerId, string stripeCardId, StripeCardUpdateOptions cardUpdateOptions, out string errMsg)
        {
            errMsg = string.Empty;
            StripeCard stripeCard = new StripeCard();
            try
            {
                StripeCardService cardService = new StripeCardService();
                stripeCard = cardService.Update(stripeCustomerId, stripeCardId, cardUpdateOptions);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: UpdateStripeCard: cardService.Update: Stripe Error: StripeCustomerId = " + stripeCustomerId + "; CardId = " + stripeCardId);
            }
            return stripeCard;
        }

        public bool DeleteStripeCard(string stripeCustomerId, string stripeCardId, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {
                StripeCardService cardService = new StripeCardService();
                StripeDeleted card = cardService.Delete(stripeCustomerId, stripeCardId);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeService: UpdateStripeCard: cardService.Update: Stripe Error: StripeCustomerId = " + stripeCustomerId + "; CardId = " + stripeCardId);
                return false;
            }
            return true;
        }


        //******* CHARGE STUFF
        public List<StripeCharge> GetStripeCustomerCharges(string id, out string errMsg)
        {
            errMsg = string.Empty;
            IEnumerable<StripeCharge> stripeCustomerChargeList = new List<StripeCharge>();
            StripeChargeListOptions chargeListOptions = new StripeChargeListOptions
            {
                CustomerId = id
            };
            try
            {
                StripeChargeService chargeService = new StripeChargeService();
                stripeCustomerChargeList = chargeService.List(chargeListOptions); // optional StripeChargeListOptions
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: GetStripeCustomerCharges: chargeService.List: Stripe Error: CustomerId = " + id);
            }
            return stripeCustomerChargeList.ToList();
        }

        public StripeCharge GetStripeCustomerCharge(string stripeChargeId, out string errMsg)
        {
            throw new NotImplementedException();
        }

        public StripeCharge CreateStripeCharge(StripeChargeCreateOptions stripeChargeCreateOptions, out string errMsg)
        {
            errMsg = string.Empty;
            StripeCharge stripeCharge = new StripeCharge();
            try
            {
                StripeChargeService chargeService = new StripeChargeService();
                stripeCharge = chargeService.Create(stripeChargeCreateOptions);
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeService: CreateStripeCharge: Stripe Error: Source Id = " + stripeChargeCreateOptions.SourceTokenOrExistingSourceId);
            }
            return stripeCharge;
        }

        public StripeCharge UpdateStripeCharge(string stripeChargeId, StripeChargeUpdateOptions stripeChargeUpdateOptions, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {

            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeService: UpdateStripeCharge: : Stripe Error: Charge ID = " + stripeChargeId);
            }
            throw new NotImplementedException();
        }


        //******* INVOICE STUFF
        public List<StripeInvoice> GetStripeCustomerInvoices(string id, out string errMsg)
        {
            errMsg = string.Empty;
            IEnumerable<StripeInvoice> stripeCustomerInvoiceList = new List<StripeInvoice>();
            StripeInvoiceListOptions stripeInvoiceListOptions = new StripeInvoiceListOptions
            {
                CustomerId = id
            };
            try
            {
                StripeInvoiceService invoiceService = new StripeInvoiceService();
                stripeCustomerInvoiceList = invoiceService.List(stripeInvoiceListOptions); // optional StripeInvoiceListOptions
            }
            catch (StripeException ex)
            {
                errMsg = Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeBiz: GetStripeCustomerInvoices: chargeInvoice.List: Stripe Error: CustomerId = " + id);
            }
            return stripeCustomerInvoiceList.ToList();
        }




        public StripeCustomer GetStripeCustomerFromPW(string email)
        {
            StripeCustomer stripeCustomer = new StripeCustomer();

            //get PayWhirl subscriber
            PayWhirl.PayWhirlSubscriber pwSubScriber = new PayWhirl.PayWhirlSubscriber();
            try
            {
                //pwSubScriber = PayWhirl.PayWhirlSubscriber.FindSubscriberByEmail(c.Email);
                pwSubScriber = PayWhirl.PayWhirlBiz.FindSubscriberByEmail(email);
            }
            catch (Exception ex)
            {
                string msg = "Customer Email: " + email + Environment.NewLine;
                msg += "Ex Message: " + ex.Message + Environment.NewLine;
                mvc_Mail.SendDiagEmail("GetStripeCustomerFromPW: No PayWhirl Subscriber found", msg);
            }

            //get the customer from Stripe, based on PayWhirl subscriber
            var customerService = new StripeCustomerService();

            try
            {
                stripeCustomer = customerService.Get(pwSubScriber.Stripe_id);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "ShopifyFMOrderTranslator: Get Transaction: Get StripeCustomer: ", "Stripe Error in Translator");
            }

            return stripeCustomer;
        }

        public List<StripeInvoice> GetStripeInvoiceList(string stripeCustomerId)
        {
            List<StripeInvoice> stripeInvoiceList = new List<StripeInvoice>();

            ///get last Invoice of Stripe customer
            var invoiceService = new StripeInvoiceService();
            var invoiceListOptions = new StripeInvoiceListOptions
            {
                CustomerId = stripeCustomerId
            };
            //invoiceListOptions.Date = new StripeDateFilter { GreaterThanOrEqual = DateTime.UtcNow.Date }; // today's invoices
            //List<StripeInvoice> stripeInvoiceList = new List<StripeInvoice>();
            try
            {
                stripeInvoiceList = invoiceService.List(invoiceListOptions).ToList(); // optional StripeInvoiceListOptions
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "GetStripeInvoiceList: Get Invoice List Error", "StripeBiz Error");
            }

            return stripeInvoiceList;
        }

        public StripeSubscription CreateStripeSubscription(string stripeCustomerID, string stripePlanID, string StripeCouponID, string subscriptionErrMsg, out string errMsg)
        {
            errMsg = string.Empty;
            StripeCouponID = (StripeCouponID.ToUpper() == "N/A") ? string.Empty : StripeCouponID;

            var subCreateOptions = new StripeSubscriptionCreateOptions()
            {
                CustomerId = stripeCustomerID,
                Items = new List<StripeSubscriptionItemOption>()
                {
                    new StripeSubscriptionItemOption()
                    {
                        Quantity = 1,
                        PlanId = stripePlanID,
                    },
                },
                CouponId = StripeCouponID,
            };

            StripeSubscription stripeSubscription = new StripeSubscription();
            var subscriptionService = new StripeSubscriptionService();
            try
            {
                stripeSubscription = subscriptionService.Create(stripeCustomerID, subCreateOptions, null);
                //StripeSubscription stripeSubscription = subscriptionService.Create(subCreateOptions, null);
            }
            catch (StripeException ex)
            {
                errMsg = "failed: " + Utilities.FMHelpers.ProcessStripeException(ex, true, "", subscriptionErrMsg);
            }

            return stripeSubscription;
        }

        public bool DeleteStripeSubscription(string stripeCustomerID, string stripeSubscriptionID, string subscriptionErrMsg, out string errMsg)
        {
            bool success = false;
            errMsg = string.Empty;

            try
            {
                StripeSubscriptionService subscriptionService = new StripeSubscriptionService();
                StripeSubscription stripeSubscription = subscriptionService.Cancel(stripeSubscriptionID);
            }
            catch (StripeException ex)
            {
                errMsg = "Subscription Delete: FAILED: " + Utilities.FMHelpers.ProcessStripeException(ex, true, "", subscriptionErrMsg);
                success = false;
            }

            return success;
        }

        

        private string ParseCustomerListParam(string param)
        {
            string email = string.Empty;
            if (param != null)
            {
                if (param.Contains("@"))
                {
                    email = param;
                }
                else if (int.TryParse(param, out int customerID))
                {
                    Customer c = new Customer(customerID);
                    email = c.Email;
                }
                else if ((param.ToUpper().Contains("CUS_")) || (param.ToUpper().Contains("SUB_")))
                {
                    List<Subscriptions> subscriptions = Subscriptions.GetSubscribersList();
                    Subscriptions subscriber = (from s in subscriptions where (s.StripeCusID.ToUpper() == param.ToUpper() || s.StripeSubID.ToUpper() == param.ToUpper()) select s).FirstOrDefault();
                    if (subscriber != null)
                    {
                        Order o = new Order(subscriber.OrderID);
                        Customer c = new Customer(o.CustomerID);
                        email = c.Email;
                    }
                }
            }
            return email;
        }

        public StripeCustomer FindStripeCustomerByEmail(string email)
        {
            StripeCustomer stripeCustomer = new StripeCustomer();
            PayWhirl.PayWhirlSubscriber pwSubScriber = paywhirlService.FindSubscriberByEmail(email);            
            if (pwSubScriber.Id != 0) //wasn't located in the PayWhirl world
            {
                //get the customer from Stripe
                StripeServices stripeServices = new StripeServices();
                stripeCustomer = stripeServices.GetStripeCustomer(pwSubScriber.Stripe_id, out string errMsg);
            }            
            return stripeCustomer;
        }

        public StripeCard CardService_Create(string customerId, StripeCardCreateOptions myCard)
        {
            var cardService = new StripeCardService();
            StripeCard card = new StripeCard();
            try
            {
                card = cardService.Create(customerId, myCard);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Add Card: Stripe Error");
                return card;
            }

            return card;
        }

        public StripeCustomer CustomerService_Get(string customerId)
        {
            var customerService = new StripeCustomerService();
            StripeCustomer stripeCustomer = new StripeCustomer();
            try
            {
                //stripeCustomer = customerService.Get(ccvm.StripeCustomerID);
                stripeCustomer = customerService.Get(customerId);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Add Card: Stripe Error");
            }

            return stripeCustomer;
        }

        public StripeCustomer CustomerService_Update(string customerId, StripeCustomerUpdateOptions myCard)
        {
            var customerService = new StripeCustomerService();
            StripeCustomer stripeCustomer = new StripeCustomer();
            try
            {
                //stripeCustomer = customerService.Get(ccvm.StripeCustomerID);
                stripeCustomer = customerService.Update(customerId, myCard);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "Add Card: Stripe Error");
            }

            return stripeCustomer;
        }

    }
}