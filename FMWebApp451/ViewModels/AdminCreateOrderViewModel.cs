﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AdminCart;

namespace FM2015.ViewModels
{
    public class AdminCreateOrderViewModel
    {
        private int _customerId;
        private decimal _total;
        private decimal _adjust;
        private decimal _totalTax;
        private decimal _totalShipping;
        private string _transactionId;
        private string _cardType;
        private string _cardLast4;
        private string _cardExp;
        private DateTime _transactionDate;
        private DateTime _orderDate;
        private string _productIDQuantityPairs;
        private int _orderId;

        //Order stuff
        [Display(Name = "Customer ID")]
        public int CustomerId { get => _customerId; set => _customerId = value; }

        [Display(Name = "Order ID")]
        public int OrderId { get => _orderId; set => _orderId = value; }

        [Required]
        [Display(Name = "Order Date/Time")]
        public DateTime OrderDate { get => _orderDate; set => _orderDate = value; }

        [Required]
        [Display(Name = "Order Total")]
        public decimal Total { get => _total; set => _total = value; }

        [Required]
        [Display(Name = "Order Adjust")]
        public decimal Adjust { get => _adjust; set => _adjust = value; }

        [Required]
        [Display(Name = "Total Tax")]
        public decimal TotalTax { get => _totalTax; set => _totalTax = value; }

        [Required]
        [Display(Name = "Total Shipping")]
        public decimal TotalShipping { get => _totalShipping; set => _totalShipping = value; }

        //OrdrDetail stuff
        [Required]
        [Display(Name = "ProductID/Quanity pairs")]
        // productID1,Quantity1;productID2,Quantity2; etc.
        public string ProductIDQuantityPairs { get => _productIDQuantityPairs; set => _productIDQuantityPairs = value; }

        //Transaction stuff
        [Required]
        [Display(Name = "Transaction ID")]
        public string TransactionId { get => _transactionId; set => _transactionId = value; }

        [Required]
        [Display(Name = "Transaction Date/Time")] //can be different then Order date
        public DateTime TransactionDate { get => _transactionDate; set => _transactionDate = value; }

        [Required]
        [Display(Name = "Card Type")]
        public string CardType { get => _cardType; set => _cardType = value; }

        [Required]
        [Display(Name = "Card Last4")]
        public string CardLast4 { get => _cardLast4; set => _cardLast4 = value; }

        [Required]
        [Display(Name = "Card Exp (mm/yy)")]
        public string CardExp { get => _cardExp; set => _cardExp = value; }

        public AdminCreateOrderViewModel()
        {
        }

    }
}