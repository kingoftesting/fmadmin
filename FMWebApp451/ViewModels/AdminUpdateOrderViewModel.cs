﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AdminCart;

namespace FM2015.ViewModels
{
    public class AdminUpdateOrderViewModel
    {
        private int _orderId;
        private int _customerId;
        private DateTime _orderDate;
        private decimal _total;
        private decimal _adjust;
        private decimal _totalTax;
        private decimal _totalShipping;

        //Order stuff
        [Display(Name = "Customer ID")]
        public int CustomerId { get => _customerId; set => _customerId = value; }

        [Display(Name = "Order ID")]
        public int OrderId { get => _orderId; set => _orderId = value; }

        [Required]
        [Display(Name = "Order Date/Time")]
        public DateTime OrderDate { get => _orderDate; set => _orderDate = value; }

        [Required]
        [Display(Name = "Order Total")]
        public decimal Total { get => _total; set => _total = value; }

        [Required]
        [Display(Name = "Order Adjust")]
        public decimal Adjust { get => _adjust; set => _adjust = value; }

        [Required]
        [Display(Name = "Order Coupon Amount")]
        public decimal Coupon { get => _coupon; set => _coupon = value; }

        [Required]
        [Display(Name = "Total Tax")]
        public decimal TotalTax { get => _totalTax; set => _totalTax = value; }

        [Required]
        [Display(Name = "Total Shipping")]
        public decimal TotalShipping { get => _totalShipping; set => _totalShipping = value; }

        public List<OrderDetail> odList;
        public List<OrderAction> oaList;
        public List<Transaction> tranList;
        private decimal _coupon;

        public AdminUpdateOrderViewModel()
        {
            odList = new List<OrderDetail>();
            oaList = new List<OrderAction>();
            tranList = new List<Transaction>();
        }

    }
}