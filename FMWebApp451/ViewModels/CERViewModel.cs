﻿
using System.Collections.Generic;
using System.Web.Mvc;
using FM2015.Models.mvcCER;
using CER;
using System.ComponentModel.DataAnnotations;

namespace FM2015.ViewModels
{
    public class CERCreateViewModel
    {
        public int CustomerID { get; set; }
        public int OrderID { get; set; }

        [DataType(DataType.MultilineText)]
        public string EventDescription { get; set; }

        public string ActionExplaination { get; set; }

        public CERCreateViewModel()
        {
            CustomerID = 0;
            OrderID = 0;
            EventDescription = "";
            ActionExplaination = "";
        }
    }

    public class CERViewModel
    {
        #region Properties
        public adminCER.CER cer { get; set; }
        public AdminCart.Cart cart { get; set; }
        public CER.Defect defectAnalysis { get; set; }
        public CER.Returns returnAnalysis { get; set; }
        public List<CERMessages> cerMessageList;
        public List<OrderNote> orderNoteList;
        public List<adminCER.CER> relatedCERList;
        public List<FM2015.Models.mvcCER.CERHistory> cerHistoryList;
        public SelectList ddlSymptomOptions { get; set; }
        public SelectList ddlDefectOptions { get; set; }
        public SelectList ddlDefectCatOptions { get; set; }

        #endregion

        #region Constructors
        public CERViewModel()
        {
            cer = new adminCER.CER();
            cart = new AdminCart.Cart();
            defectAnalysis = new CER.Defect();
            returnAnalysis = new CER.Returns();
            cerMessageList = new List<CERMessages>();
            orderNoteList = new List<OrderNote>();
            relatedCERList = new List<adminCER.CER>();
            cerHistoryList = new List<FM2015.Models.mvcCER.CERHistory>();

            ddlSymptomOptions = SymptomOpts.GetSymptomOptions();
            ddlDefectOptions = DefectOpts.GetDefectOptions();
            ddlDefectCatOptions = DefectCatOpts.GetDefectCatOptions();

        }
        #endregion
    }
}