﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FM2015.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace FMWebApp451.ViewModels
{
    public class ClubRevenueByOrder
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public decimal TransactionAmount { get; set; }

        [DisplayName("Order Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime OrderDate { get; set; }

        [DisplayName("Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDate { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndDate { get; set; }

        [Display(Name = "GrossRev")]
        public decimal GrossRevenue { get; set; }

        [Display(Name = "Discount%")]
        public decimal PromoCodePercent { get; set; }

        [Display(Name = "Discount")]
        public decimal PromoCodeDiscount { get; set; }

        [Display(Name = "PromoCode")]
        public string PromoCode { get; set; }

        [Display(Name = "Revenue")]
        public decimal Revenue { get; set; }

        [Display(Name = "Payment")]
        public decimal Payment { get; set; }

        [Display(Name = "PaymentsExp")]
        public int ttlPaymentCount { get; set; }

        [Display(Name = "PaymentsMade")]
        public int PaymentsMade { get; set; }

        [Display(Name = "Collected")]
        public decimal Collected { get; set; }

        [Display(Name = "ToBeCollected")]
        public decimal ToBeCollected { get; set; }

        [Display(Name = "UnCollectable")]
        public decimal UnCollectable { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        public ClubRevenueByOrder()
        {
            Id = 0;
            OrderId = 0;
            TransactionAmount = 0;
            StartDate = Convert.ToDateTime("1/1/2015");
            EndDate = DateTime.Now;
            GrossRevenue = 0;
            PromoCodePercent = 0;
            PromoCodeDiscount = 0;
            PromoCode = "";
            Revenue = 0;
            Payment = 0;
            ttlPaymentCount = 0;
            PaymentsMade = 0;
            Collected = 0;
            ToBeCollected = 0;
            UnCollectable = 0;
            Status = "";
        }

        public static List<ClubRevenueByOrder> GetList(DateTime startDate, DateTime endDate)
        {
            List<ClubRevenueByOrder> theList = new List<ClubRevenueByOrder>();
            
            string sql = @" 
                SELECT row#=row_number() over (order by o.orderdate asc), o.orderid, t.transactionAmount, o.orderdate,  
	                GrossRevenue = p.price, 
                    PromoCodePercent = o.adjust / p.price, 
                    PromoCodeDiscount = o.adjust,  
	                PromoCode = s.StripePromoCode, 
                    Revenue = p.price - o.adjust, 
	                Payment = o.total, 
                    TotalPaymentCount = 1, 
	                PaymentsMade = 1, 
	                Collected = (t.transactionAmount), 
                    ToBeCollected = 0,  
	                CASE 
		                WHEN (s.StripeStatus = 'past_due') OR (s.StripeStatus = 'unpaid') THEN t.transactionAmount
		                ELSE 0 
	                END as unCollectable, 
                    [Status] = s.StripeStatus 
                FROM orders o, orderdetails od, subscribers s, transactions t, products p 
                WHERE o.orderid = od.orderid AND o.orderid = s.orderid AND o.orderid = t.orderid AND  
	                od.productid = p.productid AND 
	                o.orderdate BETWEEN @startDate AND @endDate AND 
	                t.transactiondate BETWEEN @startDate1 AND @endDate1 AND 
                    p.IsSubscription = 1 AND 
	                p.IsMultiPay = 0 AND 
	                t.TrxType = 'S' AND 
	                t.ResultCode = 0 AND 
	                o.orderid <> -1 
                GROUP BY o.orderid, o.adjust, o.total, o.totalTax, o.totalShipping, o.orderdate, 
                        t.orderid, t.transactionAmount, p.price, 
		                s.StripeStatus, s.StripePlanID, s.StripePromoCode, s.StripeStatus 
                ORDER BY o.orderid ASC 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.HasRows && dr.Read())
            {
                ClubRevenueByOrder obj = new ClubRevenueByOrder();
                obj.Id = Convert.ToInt32(dr["row#"]);
                obj.OrderId = Convert.ToInt32(dr["OrderId"].ToString());
                obj.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                obj.TransactionAmount = Convert.ToDecimal(dr["TransactionAmount"].ToString());
                obj.GrossRevenue = Convert.ToDecimal(dr["GrossRevenue"].ToString());
                obj.PromoCodePercent = Convert.ToDecimal(dr["PromoCodePercent"].ToString());
                obj.PromoCodeDiscount = Convert.ToDecimal(dr["PromoCodeDiscount"].ToString());
                obj.PromoCode = (dr["PromoCode"] == DBNull.Value) ? "" : dr["PromoCode"].ToString();
                obj.Revenue = Convert.ToDecimal(dr["Revenue"].ToString());
                obj.Payment = Convert.ToDecimal(dr["Payment"].ToString());
                obj.ttlPaymentCount = Convert.ToInt32(dr["TotalPaymentCount"].ToString());
                obj.PaymentsMade = Convert.ToInt32(dr["PaymentsMade"].ToString());
                obj.Collected = Convert.ToDecimal(dr["Collected"].ToString());
                obj.ToBeCollected = Convert.ToDecimal(dr["ToBeCollected"].ToString());
                obj.UnCollectable = Convert.ToDecimal(dr["UnCollectable"].ToString());
                obj.Status = dr["Status"].ToString();
                theList.Add(obj);
            }
            if (dr != null) { dr.Close(); }

            return theList;
        }
    }
}