﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FM2015.Models;
using System.ComponentModel.DataAnnotations;
using AdminCart;
using PayWhirl;
using Stripe;
using System;
using FMWebApp451.Services.StripeServices;

namespace FM2015.ViewModels
{
    public class CustomerViewModel
    {
        public enum AddressType
        {
            BillingAddressType = 1,
            ShippingAddressType = 2
        }

        [Key]
        public int CustomerViewModelKey { get; set; }

        public Customer Customer { get; set; }
        public Address BillAddress { get; set; }
        public Address ShipAddress { get; set; }
        public List<Order> OrderList;
        public PayWhirlSubscriber payWhirlCustomer;
        public List<Subscriptions> CustomerSubscriptionList;
        public IEnumerable<StripeSubscription> stripeCustomerSubscriptionList;
        public IEnumerable<DelayedPlanChanges> stripeCustomerDelayedSubscriptionList;
        public List<adminCER.CER> cerList;
        public List<string> roleList;
        public IEnumerable<StripeCard> stripeCustomerCardList;

        public string SelectBillCountryCode { get; set; }
        public SelectList BillCountrySelectList { get; set; }
        public string SelectShipCountryCode { get; set; }
        public SelectList ShipCountrySelectList { get; set; }


        public string SelectBillStateCode { get; set; }
        public SelectList BillStateSelectList { get; set; }
        public string SelectShipStateCode { get; set; }
        public SelectList ShipStateSelectList { get; set; }

        public CustomerViewModel()
        {
            Customer = new Customer();
            BillAddress = new Address();
            ShipAddress = new Address();
            OrderList = new List<Order>();
            payWhirlCustomer = new PayWhirlSubscriber();
            CustomerSubscriptionList = new List<Subscriptions>();
            stripeCustomerSubscriptionList = new List<StripeSubscription>();
            stripeCustomerDelayedSubscriptionList = new List<DelayedPlanChanges>();
            cerList = new List<adminCER.CER>();
            roleList = new List<string>();
            stripeCustomerCardList = new List<StripeCard>();

            BillCountrySelectList = Location.GetCountrySelectList("US");
            BillStateSelectList = Location.GetStateSelectList(Location.GetCountryCodeIDFromSelectList(BillCountrySelectList));
            ShipCountrySelectList = Location.GetCountrySelectList("US");
            ShipStateSelectList = Location.GetStateSelectList(Location.GetCountryCodeIDFromSelectList(BillCountrySelectList));
        }

        public CustomerViewModel(Customer customer)
        {
            Customer = customer;
            string errMsg = string.Empty;

            OrderList = Order.FindAllOrders(Customer.CustomerID, false);

            try
            {
                //payWhirlCustomer = PayWhirlSubscriber.FindSubscriberByEmail(customer.Email);
                payWhirlCustomer = PayWhirlBiz.FindSubscriberByEmail(customer.Email);
            }
            catch (Exception ex)
            {
                string msg = "CustomerViewModel Find PayWhirl Subscriber: ";
                msg += "customerID= " + customer.CustomerID + "CustomerEmail= " + customer.Email;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            CustomerSubscriptionList = Subscriptions.GetSubscribersList();
            CustomerSubscriptionList = (from s in CustomerSubscriptionList where s.StripeCusID == payWhirlCustomer.Stripe_id select s).ToList();
            if (CustomerSubscriptionList == null)
            {
                CustomerSubscriptionList = new List<Subscriptions>();
            }

            if (!string.IsNullOrEmpty(payWhirlCustomer.Stripe_id))
            {
                StripeServices stripeServices = new StripeServices();
                stripeCustomerSubscriptionList = stripeServices.GetCustomerSubscriptionList(payWhirlCustomer.Stripe_id, out errMsg);
                stripeCustomerCardList = stripeServices.GetStripeCustomerCards(payWhirlCustomer.Stripe_id, out errMsg);
            }

            stripeCustomerDelayedSubscriptionList = FM2015.Models.DelayedPlanChanges.GetDelayedPlanChangesList(Customer.CustomerID);

            cerList = adminCER.CER.FindCERList("", "", "", "", customer.Email, "", "");
            roleList = Models.Roles.GetUserRoleList(Customer.CustomerID);

            BillAddress = Address.GetAddressList(customer.LastName, 1).Find(x => x.CustomerID == customer.CustomerID);
            BillCountrySelectList = Location.GetCountrySelectList(BillAddress.Country);
            SelectBillCountryCode = BillAddress.Country;
            BillStateSelectList = Location.GetStateSelectList(Location.GetCountryCodeIDFromSelectList(BillCountrySelectList));
            SelectBillStateCode = BillAddress.State;

            ShipAddress = Address.GetAddressList(customer.LastName, 2).Find(x => x.CustomerID == customer.CustomerID);
            if (ShipAddress == null)
            {
                ShipAddress = BillAddress; //if no shipping address then must have been using billing address instead
                ShipCountrySelectList = Location.GetCountrySelectList(BillAddress.Country);
                SelectShipCountryCode = BillAddress.Country;
                SelectShipStateCode = BillAddress.State;
            }
            else
            {
                ShipCountrySelectList = Location.GetCountrySelectList(ShipAddress.Country);
                SelectShipCountryCode = ShipAddress.Country;
                SelectShipStateCode = ShipAddress.State;
            }

            ShipStateSelectList = Location.GetStateSelectList(Location.GetCountryCodeIDFromSelectList(ShipCountrySelectList));
        }

        public static List<CustomerViewModel> GetCvmList(string searchCriteria, string searchItem)
        {
            List<CustomerViewModel> cvmList = new List<CustomerViewModel>();

            return cvmList;
        }
    }
}