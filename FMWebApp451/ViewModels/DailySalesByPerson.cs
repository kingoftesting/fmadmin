﻿using System;
using System.Collections.Generic;
using FM2015.Models;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FM2015.ViewModels
{
    public class DailySalesByPerson
    {
        #region properties

        [Display(Name = "Gross Revenue")]
        [DataType(DataType.Currency)]
        public decimal TotalGrossRevenue { get; set; }

        [Display(Name = "Net Revenue")]
        [DataType(DataType.Currency)]
        public decimal TotalWebRevenue { get; set; }

        [Display(Name = "Phone Revenue")]
        [DataType(DataType.Currency)]
        public decimal TotalPhoneRevenue { get; set; }

        public int SalesTeamNum { get; set; }

        public List<DayOfWeekSales> sales;

        public string SelectMonth { get; set; }
        public SelectList MonthSelectList { get; set; }

        public string SelectYear { get; set; }
        public SelectList YearSelectList { get; set; }

        #endregion

        #region Constructors
        public DailySalesByPerson()
        {
            TotalGrossRevenue = 0;
            TotalWebRevenue = 0;
            TotalPhoneRevenue = 0;
            sales = new List<DayOfWeekSales>();

            SelectMonth = DateTime.Now.Month.ToString();
            SelectYear = DateTime.Now.Year.ToString();
            MonthSelectList = Helpers.Helpers.SetMonthSelectList();
            YearSelectList = Helpers.Helpers.SetYearSelectList();

        }
        #endregion
    }
}