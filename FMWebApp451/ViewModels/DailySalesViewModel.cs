﻿using FM2015.Models;

namespace FM2015.ViewModels
{
    public class SalesViewModel
    {
        public Sales SalesPrimary { get; set; }
        public Sales SalesCompare { get; set; }

        public SalesViewModel()
        {
            SalesPrimary = new Sales();
            SalesCompare = new Sales();
        }
    }
}