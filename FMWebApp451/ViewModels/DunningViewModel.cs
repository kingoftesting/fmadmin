﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.Mvc;

namespace FM2015.ViewModels
{
    public class DunningViewModel
    {
        [Required]
        public int OrderId { get; set; }

        public string Name { get; set; }
        public string EMail { get; set; }

        [Required]
        [AllowHtml]
        [DisplayName("Dunning Email")]
        [DataType(DataType.MultilineText)]
        public string DunningBody { get; set; }

        public DunningViewModel()
        {
            OrderId = 0;
            Name = string.Empty;
            EMail = string.Empty;
            DunningBody = string.Empty;
        }

        public DunningViewModel(int orderId)
        {
            OrderId = orderId;
            DunningBody = string.Empty;
            AdminCart.Order o = new AdminCart.Order(orderId);
            AdminCart.Customer c = new AdminCart.Customer(o.CustomerID);
            Name = c.FirstName + " " + c.LastName;
            EMail = c.Email;
        }
    }
}