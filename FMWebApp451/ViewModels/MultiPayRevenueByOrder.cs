﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FM2015.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using FMWebApp451.Interfaces;

namespace FMWebApp451.ViewModels
{
    public class MultiPayRevenueByOrder : IMultiPayRevenueByOrder
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public decimal TransactionAmount { get; set; }
        public decimal RefundAmount { get; set; }

        [DisplayName("Order Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime OrderDate { get; set; }


        [DisplayName("Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDate { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndDate { get; set; }

        [Display(Name = "GrossRev")]
        public decimal GrossRevenue { get; set; }

        [Display(Name = "Discount%")]
        public decimal PromoCodePercent { get; set; }

        [Display(Name = "Discount")]
        public decimal PromoCodeDiscount { get; set; }

        [Display(Name = "PromoCode")]
        public string PromoCode { get; set; }

        [Display(Name = "Revenue")]
        public decimal Revenue { get; set; }

        [Display(Name = "Payment")]
        public decimal Payment { get; set; }

        [Display(Name = "PaymentsExp")]
        public int ttlPaymentCount { get; set; }

        [Display(Name = "PaymentsMade")]
        public int PaymentsMade { get; set; }

        [Display(Name = "Collected")]
        public decimal Collected { get; set; }

        [Display(Name = "Cancelled")]
        public decimal Cancelled { get; set; }

        [Display(Name = "ToBeCollected")]
        public decimal ToBeCollected { get; set; }

        [Display(Name = "UnCollectable")]
        public decimal UnCollectable { get; set; }

        [Display(Name = "WrittenOff")]
        public decimal WrittenOff { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        public MultiPayRevenueByOrder()
        {
            Id = 0;
            OrderId = 0;
            TransactionAmount = 0;
            RefundAmount = 0;
            OrderDate = Convert.ToDateTime("1/1/2015");
            StartDate = Convert.ToDateTime("1/1/2015");
            EndDate = DateTime.Now;
            GrossRevenue = 0;
            PromoCodePercent = 0;
            PromoCodeDiscount = 0;
            PromoCode = "";
            Revenue = 0;
            Payment = 0;
            ttlPaymentCount = 0;
            PaymentsMade = 0;
            Collected = 0;
            Cancelled = 0;
            ToBeCollected = 0;
            UnCollectable = 0;
            WrittenOff = 0;
            Status = "";
        }

        public List<MultiPayRevenueByOrder> GetAll (DateTime startDate, DateTime endDate)
        {
            return GetList(startDate, endDate);
        }

        //STATIC METHODS
        //public static List<MultiPayRevenueByOrder> GetList(DateTime startDate, DateTime endDate)
        //{
        //    List<MultiPayRevenueByOrder> theList = new List<MultiPayRevenueByOrder>();

        //    string sql = @" 
        //        SELECT row#=row_number() over (order by tmp.orderdate asc), tmp.orderid, tmp.orderdate,  --t.transactionAmount, 
	       //         GrossRevenue = tmp.OnePayPrice, 
	       //         CASE 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-4-easy-payments' THEN (4 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'facemaster---4-payments-of-7499' THEN (4 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'facemaster-4-easy-payments-rf' THEN (4 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-rf-4-pay-4999' THEN (4 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-3-easy-payments' THEN (3 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'facemaster-black-friday-3-pay-rf' THEN (3 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'facemaster-14999-3-pay' THEN (3 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'crystalift-3-easy-payments' THEN (3 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'crystalift-4-easy-payments' THEN (4 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'crystalift-14999-3-pay' THEN (3 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'crystalift-4-pay-40month' THEN (4 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'sonulase-3-pay' THEN (3 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'sonulase-4-pay' THEN (4 * tmp.adjust) / tmp.OnePayPrice 
		      //          WHEN tmp.StripePlanID = 'sonulase-17999-3-pay' THEN (3 * tmp.adjust) / tmp.OnePayPrice 
        //                WHEN tmp.StripePlanID = 'sonulase-4-payments-4999' THEN (4 * tmp.adjust) / tmp.OnePayPrice 
	       //         END as PromoCodePercent, 
	       //         CASE 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-4-easy-payments' THEN (4 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster---4-payments-of-7499' THEN (4 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster-4-easy-payments-rf' THEN (4 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-rf-4-pay-4999' THEN (4 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-3-easy-payments' THEN (3 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster-black-friday-3-pay-rf' THEN (3 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster-14999-3-pay' THEN (3 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'crystalift-3-easy-payments' THEN (3 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'crystalift-4-easy-payments' THEN (4 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'crystalift-14999-3-pay' THEN (3 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'crystalift-4-pay-40month' THEN (4 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'sonulase-3-pay' THEN (3 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'sonulase-4-pay' THEN (4 * tmp.adjust) 
		      //          WHEN tmp.StripePlanID = 'sonulase-17999-3-pay' THEN (3 * tmp.adjust) 
        //                WHEN tmp.StripePlanID = 'sonulase-4-payments-4999' THEN (4* tmp.adjust) 
        //            END as PromoCodeDiscount,  
	       //         PromoCode = tmp.StripePromoCode, 
	       //         CASE 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-4-easy-payments' THEN tmp.OnePayPrice - (4 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster---4-payments-of-7499' THEN tmp.OnePayPrice - (4 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-3-easy-payments' THEN tmp.OnePayPrice - (3 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster-4-easy-payments-rf' THEN tmp.OnePayPrice - (4 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-rf-4-pay-4999' THEN tmp.OnePayPrice - (4 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster-black-friday-3-pay-rf' THEN tmp.OnePayPrice - (3 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'facemaster-14999-3-pay' THEN tmp.OnePayPrice - (3 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'crystalift-4-easy-payments' THEN tmp.OnePayPrice - (4 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'crystalift-3-easy-payments' THEN tmp.OnePayPrice - (3 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'crystalift-14999-3-pay' THEN tmp.OnePayPrice - (3 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'crystalift-4-pay-40month' THEN tmp.OnePayPrice - (4 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'sonulase-4-pay' THEN tmp.OnePayPrice - (4 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'sonulase-3-pay' THEN tmp.OnePayPrice - (3 * tmp.Adjust) 
		      //          WHEN tmp.StripePlanID = 'sonulase-17999-3-pay' THEN tmp.OnePayPrice - (3 * tmp.Adjust)  
        //                WHEN tmp.StripePlanID = 'sonulase-4-payments-4999' THEN tmp.OnePayPrice - (4 * tmp.Adjust) 
	       //         END as Revenue, 
	       //         Payment = tmp.Payment, 
	       //         CASE 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-4-easy-payments' THEN 4 
		      //          WHEN tmp.StripePlanID = 'facemaster---4-payments-of-7499' THEN 4 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-3-easy-payments' THEN 3 
		      //          WHEN tmp.StripePlanID = 'facemaster-4-easy-payments-rf' THEN 4 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-rf-4-pay-4999' THEN 4 
		      //          WHEN tmp.StripePlanID = 'facemaster-black-friday-3-pay-rf' THEN 3 
		      //          WHEN tmp.StripePlanID = 'facemaster-14999-3-pay' THEN 3 
		      //          WHEN tmp.StripePlanID = 'crystalift-4-easy-payments' THEN 4 
		      //          WHEN tmp.StripePlanID = 'crystalift-3-easy-payments' THEN 3 
		      //          WHEN tmp.StripePlanID = 'crystalift-14999-3-pay' THEN 3 
		      //          WHEN tmp.StripePlanID = 'crystalift-4-pay-40month' THEN 4 
		      //          WHEN tmp.StripePlanID = 'sonulase-4-pay' THEN 4 
		      //          WHEN tmp.StripePlanID = 'sonulase-3-pay' THEN 3 
		      //          WHEN tmp.StripePlanID = 'sonulase-17999-3-pay' THEN 3  
        //                WHEN tmp.StripePlanID = 'sonulase-4-payments-4999' THEN 4  
	       //         END as TotalPaymentCount, 
	       //         PaymentsMade = tmp.TotalPaymentsMade, 
	       //         Collected = tmp.TotalPaymentAmount, 
        //            CASE 
        //                WHEN (tmp.StripeStatus = 'canceled') THEN 
        //                    CASE 
		      //                  WHEN tmp.StripePlanID = 'facemaster-system-4-easy-payments'  THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'facemaster---4-payments-of-7499'  THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'facemaster-system-3-easy-payments' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'facemaster-4-easy-payments-rf'  THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'facemaster-system-rf-4-pay-4999'  THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'facemaster-black-friday-3-pay-rf' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'facemaster-14999-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'crystalift-4-easy-payments' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'crystalift-3-easy-payments' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'crystalift-14999-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'crystalift-4-pay-40month' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'sonulase-4-pay' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'sonulase-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'sonulase-17999-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount  
        //                        WHEN tmp.StripePlanID = 'sonulase-4-payments-4999' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
        //                    END 
        //                ELSE 0 
	       //         END as Cancelled,  
	       //         CASE 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-4-easy-payments' THEN (4 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'facemaster---4-payments-of-7499' THEN (4 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-3-easy-payments' THEN (3 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'facemaster-4-easy-payments-rf' THEN (4 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-rf-4-pay-4999' THEN (4 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'facemaster-black-friday-3-pay-rf' THEN (3 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'facemaster-14999-3-pay' THEN (3 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'crystalift-4-easy-payments' THEN (4 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'crystalift-3-easy-payments' THEN (3 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'crystalift-14999-3-pay' THEN (3 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'crystalift-4-pay-40month' THEN (4 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'sonulase-4-pay' THEN (4 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'sonulase-3-pay' THEN (3 - tmp.TotalPaymentsMade) * tmp.Payment 
		      //          WHEN tmp.StripePlanID = 'sonulase-17999-3-pay' THEN (3 - tmp.TotalPaymentsMade) * tmp.Payment  
        //                WHEN tmp.StripePlanID = 'sonulase-4-payments-4999' THEN (4 - tmp.TotalPaymentsMade) * tmp.Payment  
	       //         END as ToBeCollected,  
	       //         CASE 
		      //          WHEN (tmp.StripeStatus = 'past_due') OR (tmp.StripeStatus = 'unpaid') THEN 
			     //           CASE 
				    //            WHEN tmp.StripePlanID = 'facemaster-system-4-easy-payments' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'facemaster---4-payments-of-7499' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'facemaster-system-3-easy-payments' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'facemaster-4-easy-payments-rf' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'facemaster-system-rf-4-pay-4999' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'facemaster-black-friday-3-pay-rf' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'facemaster-14999-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'crystalift-4-easy-payments' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'crystalift-3-easy-payments' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'crystalift-14999-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'crystalift-4-pay-40month' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'sonulase-4-pay' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'sonulase-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'sonulase-17999-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
        //                        WHEN tmp.StripePlanID = 'sonulase-4-payments-4999' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount   
			     //           END 
		      //          ELSE 0 
	       //         END as unCollectable, 
	       //         CASE 
		      //          WHEN (tmp.StripeStatus = 'writtenoff') THEN 
			     //           CASE 
				    //            WHEN tmp.StripePlanID = 'facemaster-system-4-easy-payments' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'facemaster---4-payments-of-7499' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'facemaster-system-3-easy-payments' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'facemaster-4-easy-payments-rf' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'facemaster-system-rf-4-pay-4999' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'facemaster-black-friday-3-pay-rf' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'facemaster-14999-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'crystalift-4-easy-payments' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'crystalift-3-easy-payments' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'crystalift-14999-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'crystalift-4-pay-40month' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'sonulase-4-pay' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
				    //            WHEN tmp.StripePlanID = 'sonulase-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
		      //                  WHEN tmp.StripePlanID = 'sonulase-17999-3-pay' THEN (3 * tmp.Payment) - tmp.TotalPaymentAmount 
        //                        WHEN tmp.StripePlanID = 'sonulase-4-payments-4999' THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount   
			     //           END 
		      //          ELSE 0 
	       //         END as WrittenOff, 
        //            CASE 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-3-easy-payments' and 3 <= tmp.TotalPaymentsMade THEN 'completed'		
		      //          WHEN tmp.StripePlanID = 'facemaster---4-payments-of-7499' and 4 <= tmp.totalPaymentsMade THEN 'completed' 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-4-easy-payments' and 4 <= tmp.totalPaymentsMade THEN 'completed' 
		      //          WHEN tmp.StripePlanID = 'facemaster-black-friday-3-pay-rf' and 3 <= tmp.TotalPaymentsMade THEN 'completed'		
		      //          WHEN tmp.StripePlanID = 'facemaster-4-easy-payments-rf' and 4 <= tmp.totalPaymentsMade THEN 'completed' 
		      //          WHEN tmp.StripePlanID = 'facemaster-system-rf-4-pay-4999' and 4 <= tmp.totalPaymentsMade THEN 'completed' 
		      //          WHEN tmp.StripePlanID = 'facemaster-14999-3-pay' and 3 <= tmp.totalPaymentsMade THEN 'completed' 
		      //          WHEN tmp.StripePlanID = 'crystalift-3-easy-payments' and 3 <= tmp.totalPaymentsMade THEN 'completed' 		
		      //          WHEN tmp.StripePlanID = 'crystalift-4-easy-payments' and 4 <= tmp.totalPaymentsMade THEN 'completed' 
		      //          WHEN tmp.StripePlanID = 'crystalift-14999-3-pay' and 3 <= tmp.totalPaymentsMade THEN 'completed' 
		      //          WHEN tmp.StripePlanID = 'crystalift-4-pay-40month' and 4 <= tmp.totalPaymentsMade THEN 'completed' 
		      //          WHEN tmp.StripePlanID = 'sonulase-3-pay' and 3 <= tmp.totalPaymentsMade THEN 'completed' 		
		      //          WHEN tmp.StripePlanID = 'sonulase-4-pay' and 4 <= tmp.totalPaymentsMade THEN 'completed' 
		      //          WHEN tmp.StripePlanID = 'sonulase-17999-3-pay' and 3 <= tmp.totalPaymentsMade THEN 'completed'  
        //                WHEN tmp.StripePlanID = 'sonulase-4-payments-4999' and 4 <= tmp.totalPaymentsMade THEN 'completed' 
		      //          ELSE tmp.StripeStatus 
        //            END as [Status] 
        //        FROM ( 
		      //          Select o.orderid, o.orderDate, p.OnePayPrice, 
			     //           Payment = p.Price - o.Adjust, 
			     //           o.Adjust, 
			     //           TotalPaymentAmount = SUM(t.TransactionAmount), 
			     //           TotalPaymentsMade = COUNT(t.TransactionId), 
			     //           s.StripePlanID, s.StripeStatus, s.StripePromoCode 
		      //          FROM orders o, orderdetails od, transactions t, products p, Subscribers s 
		      //          WHERE o.orderid = od.orderid AND o.orderid = t.orderid AND od.productid = p.productid AND o.orderid = s.orderid AND 
			     //           o.orderid = s.orderid AND 
			     //           o.orderdate BETWEEN @startDate AND @endDate AND 
			     //           p.IsMultiPay = 1 AND 
			     //           t.TrxType = 'S' AND 
			     //           t.ResultCode = 0 AND 
			     //           o.orderid <> -1  
		      //          GROUP BY o.orderid, o.orderDate, o.Adjust, p.OnePayPrice, p.price, s.StripePlanID, s.StripeStatus, s.StripePromoCode 
	       //         ) as tmp 
        //        GROUP BY tmp.orderid, tmp.orderDate, tmp.Adjust, tmp.OnePayPrice, tmp.Payment, tmp.TotalPaymentAmount, tmp.TotalPaymentsMade, 
		      //          tmp.StripePlanID, tmp.StripePromoCode, tmp.StripeStatus 
        //        ORDER BY tmp.orderid ASC 
        //    ";

        //    SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
        //    SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

        //    while (dr.HasRows && dr.Read())
        //    {
        //        MultiPayRevenueByOrder obj = new MultiPayRevenueByOrder();
        //        obj.Id = Convert.ToInt32(dr["row#"]);
        //        obj.OrderId = Convert.ToInt32(dr["OrderId"].ToString());
        //        obj.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
        //        //obj.TransactionAmount = Convert.ToDecimal(dr["TransactionAmount"].ToString());
        //        obj.GrossRevenue = Convert.ToDecimal(dr["GrossRevenue"].ToString());
        //        obj.PromoCodePercent = Convert.ToDecimal(dr["PromoCodePercent"].ToString());
        //        obj.PromoCodeDiscount = Convert.ToDecimal(dr["PromoCodeDiscount"].ToString());
        //        obj.PromoCode = (dr["PromoCode"] == DBNull.Value) ? "" : dr["PromoCode"].ToString();
        //        obj.Revenue = Convert.ToDecimal(dr["Revenue"].ToString());
        //        obj.Payment = Convert.ToDecimal(dr["Payment"].ToString());
        //        obj.ttlPaymentCount = Convert.ToInt32(dr["TotalPaymentCount"].ToString());
        //        obj.PaymentsMade = Convert.ToInt32(dr["PaymentsMade"].ToString());
        //        obj.Collected = Convert.ToDecimal(dr["Collected"].ToString());
        //        obj.Cancelled = Convert.ToDecimal(dr["Cancelled"].ToString());
        //        obj.ToBeCollected = Convert.ToDecimal(dr["ToBeCollected"].ToString());
        //        obj.UnCollectable = Convert.ToDecimal(dr["UnCollectable"].ToString());
        //        obj.WrittenOff = Convert.ToDecimal(dr["WrittenOff"].ToString());
        //        obj.Status = dr["Status"].ToString();
        //        theList.Add(obj);
        //    }
        //    if (dr != null) { dr.Close(); }

        //    return theList;
        //}

        public static List<MultiPayRevenueByOrder> GetList(DateTime startDate, DateTime endDate)
        {
            List<MultiPayRevenueByOrder> theList = new List<MultiPayRevenueByOrder>();

            string sql = @" 
                SELECT row#=row_number() over (order by tmp.orderdate asc), tmp.orderid, tmp.orderdate,  --t.transactionAmount, 
	                GrossRevenue = tmp.OnePayPrice, 
                    PromoCodePercent = (tmp.Installments * tmp.adjust) / tmp.OnePayPrice, 
                    PromoCodeDiscount = (tmp.Installments * tmp.adjust),   
	                PromoCode = tmp.StripePromoCode, 
                    Revenue = tmp.OnePayPrice - (tmp.Installments * tmp.Adjust), 
	                Payment = tmp.Payment, 
                    TotalPaymentCount = tmp.Installments,  
	                PaymentsMade = tmp.TotalPaymentsMade, 
	                Collected = tmp.TotalPaymentAmount, 
                    CASE 
                        WHEN (tmp.StripeStatus = 'canceled') THEN (tmp.Installments * tmp.Payment) - tmp.TotalPaymentAmount  
                        ELSE 0 
	                END as Cancelled, 
                    --ToBeCollected =  (tmp.Installments - tmp.TotalPaymentsMade) * tmp.Payment, 
                    CASE 
                        WHEN (tmp.StripeStatus = 'canceled') THEN 0  
                        ELSE (tmp.Installments - tmp.TotalPaymentsMade) * tmp.Payment 
	                END as ToBeCollected,
	                CASE 
		                WHEN (tmp.StripeStatus = 'past_due') OR (tmp.StripeStatus = 'unpaid') THEN (tmp.Installments * tmp.Payment) - tmp.TotalPaymentAmount 
		                ELSE 0 
	                END as unCollectable, 
	                CASE 
		                WHEN (tmp.StripeStatus = 'writtenoff') THEN (4 * tmp.Payment) - tmp.TotalPaymentAmount 
		                ELSE 0 
	                END as WrittenOff, 
                    CASE 
		                WHEN tmp.Installments <= tmp.TotalPaymentsMade THEN 'completed'  
		                ELSE tmp.StripeStatus 
                    END as [Status] 
                FROM ( 
		                Select o.orderid, o.orderDate, p.OnePayPrice, 
			                Payment = p.Price - o.Adjust, 
			                o.Adjust, 
			                TotalPaymentAmount = SUM(t.TransactionAmount), 
			                TotalPaymentsMade = COUNT(t.TransactionId), 
			                s.StripePlanID, s.StripeStatus, s.StripePromoCode, sp.Installments  
		                FROM orders o, orderdetails od, transactions t, products p, Subscribers s, subscriptionPlans sp 
		                WHERE o.orderid = od.orderid AND o.orderid = t.orderid AND od.productid = p.productid AND o.orderid = s.orderid AND 
			                o.orderid = s.orderid AND 
                            s.StripePlanId = sp.PlanId AND 
			                o.orderdate BETWEEN @startDate AND @endDate AND 
			                p.IsMultiPay = 1 AND 
			                t.TrxType = 'S' AND 
			                t.ResultCode = 0 AND 
			                o.orderid <> -1  
		                GROUP BY o.orderid, o.orderDate, o.Adjust, p.OnePayPrice, p.price, s.StripePlanID, s.StripeStatus, s.StripePromoCode, 
							sp.PlanId, sp.Installments  
	                ) as tmp 
                GROUP BY tmp.orderid, tmp.orderDate, tmp.Adjust, tmp.OnePayPrice, tmp.Payment, tmp.TotalPaymentAmount, tmp.TotalPaymentsMade, 
		                tmp.StripePlanID, tmp.StripePromoCode, tmp.StripeStatus, tmp.Installments  
                ORDER BY tmp.orderid ASC  
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.HasRows && dr.Read())
            {
                MultiPayRevenueByOrder obj = new MultiPayRevenueByOrder();
                obj.Id = Convert.ToInt32(dr["row#"]);
                obj.OrderId = Convert.ToInt32(dr["OrderId"].ToString());
                obj.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                //obj.TransactionAmount = Convert.ToDecimal(dr["TransactionAmount"].ToString());
                obj.GrossRevenue = Convert.ToDecimal(dr["GrossRevenue"].ToString());
                obj.PromoCodePercent = Convert.ToDecimal(dr["PromoCodePercent"].ToString());
                obj.PromoCodeDiscount = Convert.ToDecimal(dr["PromoCodeDiscount"].ToString());
                obj.PromoCode = (dr["PromoCode"] == DBNull.Value) ? "" : dr["PromoCode"].ToString();
                obj.Revenue = Convert.ToDecimal(dr["Revenue"].ToString());
                obj.Payment = Convert.ToDecimal(dr["Payment"].ToString());
                obj.ttlPaymentCount = Convert.ToInt32(dr["TotalPaymentCount"].ToString());
                obj.PaymentsMade = Convert.ToInt32(dr["PaymentsMade"].ToString());
                obj.Collected = Convert.ToDecimal(dr["Collected"].ToString());
                obj.Cancelled = Convert.ToDecimal(dr["Cancelled"].ToString());
                obj.ToBeCollected = Convert.ToDecimal(dr["ToBeCollected"].ToString());
                obj.UnCollectable = Convert.ToDecimal(dr["UnCollectable"].ToString());
                obj.WrittenOff = Convert.ToDecimal(dr["WrittenOff"].ToString());
                obj.Status = dr["Status"].ToString();
                theList.Add(obj);
            }
            if (dr != null) { dr.Close(); }

            return theList;
        }
    }

    public class MultiPayTransactionsByOrder
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public decimal TransactionAmount { get; set; }

        [DisplayName("Order Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime OrderDate { get; set; }

        [DisplayName("Transaction Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime TransactionDate { get; set; }


        [DisplayName("Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDate { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndDate { get; set; }

        public MultiPayTransactionsByOrder()
        {
            Id = 0;
            OrderId = 0;
            TransactionAmount = 0;
            OrderDate = Convert.ToDateTime("1/1/2015");
            TransactionDate = Convert.ToDateTime("1/1/2015");
            StartDate = Convert.ToDateTime("1/1/2015");
            EndDate = DateTime.Now;
            
        }

        public static List<MultiPayTransactionsByOrder> GetList(DateTime startDate, DateTime endDate)
        {
            List<MultiPayTransactionsByOrder> theList = new List<MultiPayTransactionsByOrder>();

            string sql = @" 
                SELECT row#=row_number() over (order by o.orderdate asc), o.orderID, o.orderDate, t.transactionAmount, t.transactionDate 
                FROM Orders o, Transactions t, subscribers s, orderdetails od, products p  
                WHERE o.orderid = t.orderid AND o.orderid = s.orderid AND o.orderid = od.orderid AND od.productID = p.productID 
	                AND p.IsMultiPay = 1 
	                AND t.ResultCode = 0 
	                AND t.Trxtype = 'S' 
	                AND o.OrderDate between @startDate and @endDate 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.HasRows && dr.Read())
            {
                MultiPayTransactionsByOrder obj = new MultiPayTransactionsByOrder();
                obj.Id = Convert.ToInt32(dr["row#"]);
                obj.OrderId = Convert.ToInt32(dr["OrderId"].ToString());
                obj.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                obj.TransactionDate = Convert.ToDateTime(dr["TransactionDate"]);
                obj.TransactionAmount = Convert.ToDecimal(dr["TransactionAmount"].ToString());
                theList.Add(obj);
            }
            if (dr != null) { dr.Close(); }

            return theList;
        }
    }

    public class MultiPayRefundsByOrder
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public decimal TransactionAmount { get; set; }

        [DisplayName("Order Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime OrderDate { get; set; }

        [DisplayName("Transaction Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime TransactionDate { get; set; }


        [DisplayName("Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDate { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndDate { get; set; }

        public MultiPayRefundsByOrder()
        {
            Id = 0;
            OrderId = 0;
            TransactionAmount = 0;
            OrderDate = Convert.ToDateTime("1/1/2015");
            TransactionDate = Convert.ToDateTime("1/1/2015");
            StartDate = Convert.ToDateTime("1/1/2015");
            EndDate = DateTime.Now;

        }


        public static List<MultiPayRefundsByOrder> GetList(DateTime startDate, DateTime endDate)
        {
            List<MultiPayRefundsByOrder> theList = new List<MultiPayRefundsByOrder>();

            string sql = @" 
                SELECT row#=row_number() over (order by o.orderdate asc), o.orderID, o.orderDate, t.transactionAmount, t.transactionDate 
                FROM Orders o, Transactions t, subscribers s, orderdetails od, products p  
                WHERE o.orderid = t.orderid AND o.orderid = s.orderid AND o.orderid = od.orderid AND od.productID = p.productID 
	                AND p.IsMultiPay = 1 
	                AND t.ResultCode = 0 
	                AND t.Trxtype = 'C' 
	                AND o.OrderDate between @startDate and @endDate 
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.HasRows && dr.Read())
            {
                MultiPayRefundsByOrder obj = new MultiPayRefundsByOrder();
                obj.Id = Convert.ToInt32(dr["row#"]);
                obj.OrderId = Convert.ToInt32(dr["OrderId"].ToString());
                obj.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                obj.TransactionDate = Convert.ToDateTime(dr["TransactionDate"]);
                obj.TransactionAmount = Convert.ToDecimal(dr["TransactionAmount"].ToString());
                theList.Add(obj);
            }
            if (dr != null) { dr.Close(); }

            return theList;
        }
    }
}