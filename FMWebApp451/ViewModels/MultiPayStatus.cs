﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FM2015.Helpers;
using System.ComponentModel.DataAnnotations;

namespace FMWebApp451.ViewModels
{
    public class MultiPayStatus
    {
        public int Id { get; set; }
        public int OrderId { get; set; }

        [Display(Name = "StartDate")]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
               ApplyFormatInEditMode = true)]
        public DateTime OrderDate { get; set; }

        [Display(Name = "Plan")]
        public string StripePlan { get; set; }
        public string LastName { get; set; }

        [Display(Name = "Payments_Due")]
        public int Payments_Total { get; set; }
        public int Payments_Made { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}",
               ApplyFormatInEditMode = true)]
        [Display(Name = "EndDate")]
        public DateTime LastPaymentDate { get; set; }

        [Display(Name = "Staus")]
        public string StripeStatus { get; set; }

        [Display(Name = "Promo")]
        public string StripePromoCode { get; set; }
        public string IsDeadBeat { get; set; }

        public MultiPayStatus()
        {
            Id = 0;
            OrderId = 0;
            OrderDate = DateTime.MinValue;
            StripePlan = "";
            LastName = "";
            Payments_Total = 0;
            Payments_Made = 0;
            LastPaymentDate = DateTime.MinValue;
            StripeStatus = "";
            StripePromoCode = "";
            IsDeadBeat = "no";
        }

        public static List<MultiPayStatus> GetStatusList()
        {
            List<MultiPayStatus> mpStatusList = new List<MultiPayStatus>();

            string sql = @" 
                SELECT row#=row_number() over (order by o.orderdate asc),
	                o.orderid, orderdate = CONVERT(VARCHAR(11),o.orderdate,106), 
                    CASE
		                WHEN s.StripePlanID = 'facemaster-system-3-easy-payments' THEN 'FM-3-Pay'
		                WHEN s.StripePlanID = 'facemaster-system-4-easy-payments' THEN 'FM-4-Pay'
		                WHEN s.StripePlanID = 'facemaster-14999-3-pay' THEN 'FM-3-Pay150'
		                WHEN s.StripePlanID = 'faceMaster-platinum-club' THEN 'Plat-Club'
		                WHEN s.StripePlanID = 'faceMaster-gold-club' THEN 'Gold-Club'
                        WHEN s.StripePlanID = '99-crystalift-with-club-membership' THEN 'CL-99-Club'
                        WHEN s.StripePlanID = 'crystalift-club-membership' THEN 'CL-Refill-Club'
                        WHEN s.StripePlanID = 'crystalift-4-easy-payments' THEN 'CL-4-Pay'
                        WHEN s.StripePlanID = 'crystalift-3-easy-payments' THEN 'CL-3-Pay'
                        WHEN s.StripePlanID = 'crystalift-14999-3-pay' THEN 'CL-3-Pay150'
                        WHEN s.StripePlanID = 'crystalift-4-pay-40month' THEN 'CL-4-Pay160'
                        WHEN s.StripePlanID = 'sonulase-4-pay' THEN 'SU-4-Pay'
                        WHEN s.StripePlanID = 'sonulase-3-pay' THEN 'SU-3-Pay'
                        WHEN s.StripePlanID = 'sonulase-17999-3-pay' THEN 'SU-3-Pay180'
                        WHEN s.StripePlanID = 'sonulase-4-payments-4999' THEN 'SU-4-Pay4999'
                        ELSE 'unknown'
	                END as [StripePlanID], 
                    s.StripePromoCode, 
                    c.lastname, 
	                CASE
		                WHEN s.StripePlanID = 'facemaster-system-3-easy-payments' THEN 3
		                WHEN s.StripePlanID = 'facemaster-system-4-easy-payments' THEN 4
		                WHEN s.StripePlanID = 'facemaster-14999-3-pay' THEN 3
                        WHEN s.StripePlanID = 'crystalift-4-easy-payments' THEN 4
                        WHEN s.StripePlanID = 'crystalift-3-easy-payments' THEN 3
                        WHEN s.StripePlanID = 'crystalift-14999-3-pay' THEN 3
                        WHEN s.StripePlanID = 'crystalift-4-pay-40month' THEN 4
                        WHEN s.StripePlanID = 'sonulase-4-pay' THEN 4
                        WHEN s.StripePlanID = 'sonulase-3-pay' THEN 3
                        WHEN s.StripePlanID = 'sonulase-17999-3-pay' THEN 3
                        WHEN s.StripePlanID = 'sonulase-4-payments-4999' THEN 4
                        ELSE 1
	                END as [Payments_Total],
	                COUNT(t.trxtype) as [Payments_Made],
	                CASE
		                WHEN s.StripePlanID = 'facemaster-system-3-easy-payments' THEN CONVERT(VARCHAR(11),DATEADD (mm , 3 , o.orderdate ),106)
		                WHEN s.StripePlanID = 'facemaster-system-4-easy-payments' THEN CONVERT(VARCHAR(11),DATEADD (mm , 4 , o.orderdate ),106) 
		                WHEN s.StripePlanID = 'facemaster-14999-3-pay' THEN CONVERT(VARCHAR(11),DATEADD (mm , 3 , o.orderdate ),106)
                        WHEN s.StripePlanID = 'crystalift-4-easy-payments' THEN CONVERT(VARCHAR(11),DATEADD (mm , 4 , o.orderdate ),106)
                        WHEN s.StripePlanID = 'crystalift-3-easy-payments' THEN CONVERT(VARCHAR(11),DATEADD (mm , 3 , o.orderdate ),106)
                        WHEN s.StripePlanID = 'crystalift-14999-3-pay' THEN CONVERT(VARCHAR(11),DATEADD (mm , 3 , o.orderdate ),106)
                        WHEN s.StripePlanID = 'crystalift-4-pay-40month' THEN CONVERT(VARCHAR(11),DATEADD (mm , 4 , o.orderdate ),106)
                        WHEN s.StripePlanID = 'sonulase-4-pay' THEN CONVERT(VARCHAR(11),DATEADD (mm , 4 , o.orderdate ),106)
                        WHEN s.StripePlanID = 'sonulase-3-pay' THEN CONVERT(VARCHAR(11),DATEADD (mm , 3 , o.orderdate ),106)
                        WHEN s.StripePlanID = 'sonulase-17999-3-pay' THEN CONVERT(VARCHAR(11),DATEADD (mm , 3 , o.orderdate ),106)
                        WHEN s.StripePlanID = 'sonulase-4-payments-4999' THEN CONVERT(VARCHAR(11),DATEADD (mm , 4 , o.orderdate ),106)
                        ELSE CONVERT(VARCHAR(11),DATEADD (mm , 120 , o.orderdate ),106) 
	                END as [last_Payment_Date],
                    CASE
		                WHEN s.StripePlanID = 'facemaster-system-3-easy-payments' and 3 = COUNT(t.trxtype) THEN 'completed'		
		                WHEN s.StripePlanID = 'facemaster-system-4-easy-payments' and 4 = COUNT(t.trxtype) THEN 'completed'
                        WHEN s.StripePlanID = 'facemaster-14999-3-pay' and 3 = COUNT(t.trxtype) THEN 'completed'                       
                        WHEN s.StripePlanID = 'crystalift-4-easy-payments' and 4 = COUNT(t.trxtype) THEN 'completed'	
                        WHEN s.StripePlanID = 'crystalift-3-easy-payments' and 3 = COUNT(t.trxtype) THEN 'completed'
                        WHEN s.StripePlanID = 'crystalift-14999-3-pay' and 3 = COUNT(t.trxtype) THEN 'completed'	
                        WHEN s.StripePlanID = 'crystalift-4-pay-40month' and 4 = COUNT(t.trxtype) THEN 'completed'	
                        WHEN s.StripePlanID = 'sonulase-4-pay' and 4 = COUNT(t.trxtype) THEN 'completed'	
                        WHEN s.StripePlanID = 'sonulase-3-pay' and 3 = COUNT(t.trxtype) THEN 'completed'	
                        WHEN s.StripePlanID = 'sonulase-17999-3-pay' and 3 = COUNT(t.trxtype) THEN 'completed'
                        WHEN s.StripePlanID = 'sonulase-4-payments-4999' and 4 = COUNT(t.trxtype) THEN 'completed'	
		                ELSE s.StripeStatus
                    END as [StripeStatus],
	                CASE
		                WHEN (s.StripeStatus = 'past_due' or s.StripeStatus = 'unpaid') and s.StripePlanID = 'facemaster-system-3-easy-payments' and 3 <> COUNT(t.trxtype) THEN '!!YES!!'		
		                WHEN (s.StripeStatus = 'past_due' or s.StripeStatus = 'unpaid') and s.StripePlanID = 'facemaster-system-4-easy-payments' and 4 <> COUNT(t.trxtype) THEN '!!YES!!'
		                WHEN (s.StripeStatus = 'past_due' or s.StripeStatus = 'unpaid') and s.StripePlanID = 'facemaster-14999-3-pay' and 3 <> COUNT(t.trxtype) THEN '!!YES!!'
		                WHEN (s.StripeStatus = 'past_due' or s.StripeStatus = 'unpaid') and s.StripePlanID = 'crystalift-3-easy-payments' and 3 <> COUNT(t.trxtype) THEN '!!YES!!'		
		                WHEN (s.StripeStatus = 'past_due' or s.StripeStatus = 'unpaid') and s.StripePlanID = 'crystalift-4-easy-payments' and 4 <> COUNT(t.trxtype) THEN '!!YES!!'
                        WHEN (s.StripeStatus = 'past_due' or s.StripeStatus = 'unpaid') and s.StripePlanID = 'crystalift-14999-3-pay' and 3 <> COUNT(t.trxtype) THEN '!!YES!!'
                        WHEN (s.StripeStatus = 'past_due' or s.StripeStatus = 'unpaid') and s.StripePlanID = 'crystalift-4-pay-40month' and 4 <> COUNT(t.trxtype) THEN '!!YES!!'
		                WHEN (s.StripeStatus = 'past_due' or s.StripeStatus = 'unpaid') and s.StripePlanID = 'sonulase-3-pay' and 3 <> COUNT(t.trxtype) THEN '!!YES!!'		
		                WHEN (s.StripeStatus = 'past_due' or s.StripeStatus = 'unpaid') and s.StripePlanID = 'sonulase-4-pay' and 4 <> COUNT(t.trxtype) THEN '!!YES!!'
		                WHEN (s.StripeStatus = 'past_due' or s.StripeStatus = 'unpaid') and s.StripePlanID = 'sonulase-17999-3-pay' and 3 <> COUNT(t.trxtype) THEN '!!YES!!'
                        WHEN (s.StripeStatus = 'past_due' or s.StripeStatus = 'unpaid') and s.StripePlanID = 'sonulase-4-payments-4999' and 4 <> COUNT(t.trxtype) THEN '!!YES!!'		
		                ELSE 'no'		
	                END as IsDeadBeat
                FROM orders o, customers c, transactions t, subscribers s
                WHERE
	                o.orderid = t.orderid AND
	                o.orderid = s.orderid AND
	                o.customerid = c.customerid AND
                    c.LastName NOT LIKE '%mohme%' AND
	                t.ResultCode = 0 AND
	                t.trxType = 'S' AND
	                o.orderid > 0 AND
	                o.orderdate BETWEEN DATEADD (mm , -6 , getdate() ) and getdate()
                GROUP BY o.orderid, o.orderdate, s.StripePlanID, c.lastname, c.email, s.StripeStatus, s.StripePromoCode 
                ORDER BY o.orderdate ASC 
            ";
            //SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, IDfield);
            SqlDataReader dr = DBUtil.FillDataReader(sql);
            while (dr.Read())
            {
                MultiPayStatus mpStat = new MultiPayStatus();
                mpStat.Id = Convert.ToInt32(dr["row#"]);
                mpStat.OrderId = Convert.ToInt32(dr["OrderId"].ToString());
                mpStat.OrderDate = Convert.ToDateTime(dr["OrderDate"].ToString());
                mpStat.StripePlan = dr["StripePlanID"].ToString();
                mpStat.LastName = dr["LastName"].ToString();
                mpStat.Payments_Total = Convert.ToInt32(dr["Payments_Total"].ToString());
                mpStat.Payments_Made = Convert.ToInt32(dr["Payments_Made"].ToString());
                mpStat.LastPaymentDate = Convert.ToDateTime(dr["Last_Payment_Date"].ToString());
                mpStat.StripeStatus = dr["StripeStatus"].ToString();
                mpStat.StripePromoCode = dr["StripePromoCode"].ToString();
                mpStat.IsDeadBeat = dr["IsDeadBeat"].ToString();
                mpStatusList.Add(mpStat);
            }
            if (dr != null) { dr.Close(); }
            return mpStatusList;
        }

    }
}