﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FM2015.Helpers;
using FMWebApp451.Interfaces;


namespace FMWebApp451.ViewModels
{
    public class MultiPayTransactionsByDate : IMultiPayTransactionsByDate
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal TransactionAmount { get; set; }

        public MultiPayTransactionsByDate()
        {
            Id = 0;
            OrderId = 0;
            OrderDate = DateTime.MinValue;
            TransactionAmount = 0;
        }

        public List<MultiPayTransactionsByDate> GetAll(DateTime startDate, DateTime endDate)
        {
            return GetList(startDate, endDate);
        }

        public static List<MultiPayTransactionsByDate> GetList(DateTime startDate, DateTime endDate)
        {
            List<MultiPayTransactionsByDate> theList = new List<MultiPayTransactionsByDate>();

            string sql = @" 
                SELECT row#=row_number() over (order by o.orderid asc), o.orderid, o.orderdate, 
                                t.transactionAmount
                FROM orders o, transactions t, orderdetails od, products p 
                WHERE o.orderid = t.orderid AND o.orderid = od.orderid AND od.productid = p.productid AND
                    p.IsMultiPay = 1 AND
	                t.transactionDate BETWEEN @startDate AND @endDate AND
	                t.TrxType = 'S' AND
	                t.ResultCode = 0 AND
	                o.orderid <> -1
                ORDER BY o.orderid ASC  
            ";

            SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, startDate, endDate);
            SqlDataReader dr = DBUtil.FillDataReader(sql, mySqlParameters);

            while (dr.Read())
            {
                MultiPayTransactionsByDate obj = new MultiPayTransactionsByDate();
                obj.Id = Convert.ToInt32(dr["row#"]);
                obj.OrderId = Convert.ToInt32(dr["OrderId"].ToString());
                obj.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                obj.TransactionAmount = Convert.ToDecimal(dr["TransactionAmount"].ToString());
                theList.Add(obj);
            }
            if (dr != null) { dr.Close(); }

            return theList;
        }
    }
}