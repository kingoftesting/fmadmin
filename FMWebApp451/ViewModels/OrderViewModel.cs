﻿using System.Collections.Generic;
using FM2015.Models;
using AdminCart;
using PayWhirl;
using Stripe;
using FMWebApp.ViewModels.Stripe;

namespace FM2015.ViewModels
{
    public class OrderViewModel
    {

        public Cart cart { get; set; }
        public string SourceID { get; set; }
        public string CompanyID { get; set; }
        public string SiteSpecial { get; set; }
        public bool HoldingForSubscription { get; set; }
        public List<ShopifyAgent.ShopOrderRisk> RiskList { get; set; }
        public List<Order> orderList { get; set; }
        public List<Cart> cartList { get; set; }
        public Subscriptions subscription { get; set; }
        public IEnumerable<StripeInvoice> invoiceList { get; set; }
        public List<DelayedPlanChanges> delayedSubscriptionList;
        public List<adminCER.CER> cerList;


        public OrderViewModel()
        {
            cart = new Cart();
            SourceID = "";
            CompanyID = "";
            SiteSpecial = "";
            HoldingForSubscription = false;
            RiskList = new List<ShopifyAgent.ShopOrderRisk>();
            orderList = new List<Order>();
            cartList = new List<Cart>();
            subscription = new Subscriptions();
            invoiceList = new List<StripeInvoice>();
            delayedSubscriptionList = new List<DelayedPlanChanges>();
            cerList = new List<adminCER.CER>();
        }           
    }
}