﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayWhirl;
using Stripe;

namespace FMWebApp.ViewModels.PayWhirl
{
    public class PayWhirlCustomerViewModel
    {
        public PayWhirlSubscriber payWhirlCustomer;
        public IEnumerable<StripeSubscription> stripeCustomerSubscriptionList;

        public PayWhirlCustomerViewModel()
        {
            payWhirlCustomer = new PayWhirlSubscriber();
            stripeCustomerSubscriptionList = new List<StripeSubscription>();
        }

    }
}