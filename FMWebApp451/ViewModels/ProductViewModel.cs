﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AdminCart;

namespace FM2015.ViewModels
{
    public class ProductViewModel
    {
        [Required]
        public Product Product { get; set; }

        [Required]
        public ProductCost ProductCost { get; set; }

        [Required]
        public List<SelectListItem> Categories { set; get; }
        public int SelectedCategory { set; get; }

        public ProductViewModel()
        {
            Product = new Product();
            ProductCost = new ProductCost();
            Categories = new List<SelectListItem>();
        }
    }
}