﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using FM2015.Helpers;
using System.ComponentModel.DataAnnotations;

namespace FMWebApp451.ViewModels
{
    public class PromoCodeMismatches
    {
        public int Id { get; set; }
        public int OrderId { get; set; }

        public int SourceId { get; set; }

        [Display(Name = "Total")]
        [DisplayFormat(DataFormatString = "{0:C}",
               ApplyFormatInEditMode = true)]
        public Decimal Total { get; set; }

        [Display(Name = "Adjust")]
        [DisplayFormat(DataFormatString = "{0:C}",
               ApplyFormatInEditMode = true)]
        public Decimal Adjust { get; set; }

        [Display(Name = "Tax")]
        [DisplayFormat(DataFormatString = "{0:C}",
               ApplyFormatInEditMode = true)]
        public Decimal Tax { get; set; }

        [Display(Name = "Shipping")]
        [DisplayFormat(DataFormatString = "{0:C}",
               ApplyFormatInEditMode = true)]
        public Decimal Shipping { get; set; }

        [Display(Name = "SubTotal")]
        [DisplayFormat(DataFormatString = "{0:C}",
               ApplyFormatInEditMode = true)]
        public Decimal SubTotal { get; set; }

        [Display(Name = "ItemTotal")]
        [DisplayFormat(DataFormatString = "{0:C}",
               ApplyFormatInEditMode = true)]
        public Decimal ItemTotal { get; set; }

        public PromoCodeMismatches()
        {
            Id = 0;
            OrderId = 0;
            SourceId = 0;
            Total = 0;
            Adjust = 0;
            Tax = 0;
            Shipping = 0;
            SubTotal = 0;
            ItemTotal = 0;
            
        }

        public static List<PromoCodeMismatches> GetMismatchList()
        {
            List<PromoCodeMismatches> mismatchList = new List<PromoCodeMismatches>();

            string sql = @" 
                SELECT row#=row_number() over (order by orderid asc),
                    SourceID, OrderID, Total, Adjust, TotalTax, TotalShipping, SubTotal, LineItemTotal
                FROM (
		            SELECT o.SourceID, o.orderid, o.Total, o.Adjust, o.TotalTax, o.TotalShipping,
			            CASE
				            WHEN sum(od.Discount) > 0
					        THEN (o.Total-o.TotalTax-o.TotalShipping)
					        ELSE (o.Total+o.Adjust-o.TotalTax-o.TotalShipping)	
			            END as SubTotal,
			            LineItemTotal = sum(od.Quantity * (od.Price - od.Discount))
		            FROM orders o, orderdetails od, transactions t
		            WHERE o.orderid = od.orderid AND o.orderid = t.orderid AND
			            t.TrxType = 'S' AND 
			            t.ResultCode = 0 AND
			            o.OrderDate BETWEEN '7/1/15' and getDate()
		            GROUP BY o.orderid, o.Total, o.Adjust, o.TotalTax, o.TotalShipping, o.SourceID
                ) as tmp
                WHERE SubTotal <> LineItemTotal
                ORDER BY OrderID asc 
            ";
            //SqlParameter[] mySqlParameters = DBUtil.BuildParametersFrom(sql, IDfield);
            SqlDataReader dr = DBUtil.FillDataReader(sql);
            while (dr.Read())
            {
                PromoCodeMismatches misMatch = new PromoCodeMismatches();
                misMatch.Id = (dr["row#"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["row#"]);
                misMatch.OrderId = Convert.ToInt32(dr["OrderId"].ToString());
                misMatch.SourceId = Convert.ToInt32(dr["SourceId"].ToString());
                misMatch.Total = Convert.ToDecimal(dr["Total"].ToString());
                misMatch.Adjust = Convert.ToDecimal(dr["Adjust"].ToString());
                misMatch.Tax = Convert.ToDecimal(dr["TotalTax"].ToString());
                misMatch.Shipping = Convert.ToDecimal(dr["TotalShipping"].ToString());
                misMatch.SubTotal = Convert.ToDecimal(dr["SubTotal"].ToString());
                misMatch.ItemTotal = Convert.ToDecimal(dr["LineItemTotal"].ToString());

                mismatchList.Add(misMatch);
            }
            if (dr != null) { dr.Close(); }
            return mismatchList;
        }

    }
}