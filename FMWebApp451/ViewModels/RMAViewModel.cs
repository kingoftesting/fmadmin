﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace FM2015.ViewModels
{
    public class RMAViewModel
    {
        //[Required]
        //public Product Product { get; set; }

        //[Required]
        //public List<SelectListItem> Categories { set; get; }
        //public int SelectedCategory { set; get; }

        //public ProductViewModel()
        //{
        //    Product = new Product();
        //    Categories = new List<SelectListItem>();
        //}

        [Required]
        public int OrderId { get; set; }

        public string Name { get; set; }
        public string EMail { get; set; }

        [Required]
        [DisplayName("RMA Email")]
        [DataType(DataType.MultilineText)]
        public string RMABody { get; set; }

        [Required]
        [DisplayName("Customer Email")]
        [DataType(DataType.MultilineText)]
        public string ActionBody { get; set; }

        public RMAViewModel()
        {
            OrderId = 0;
            Name = string.Empty;
            EMail = string.Empty;
            RMABody = string.Empty;
            ActionBody = string.Empty;
        }

        public RMAViewModel(int orderId)
        {
            OrderId = orderId;
            RMABody = string.Empty;
            ActionBody = string.Empty;
            AdminCart.Order o = new AdminCart.Order(orderId);
            AdminCart.Customer c = new AdminCart.Customer(o.CustomerID);
            Name = c.FirstName + " " + c.LastName;
            EMail = c.Email;

        }
    }
}