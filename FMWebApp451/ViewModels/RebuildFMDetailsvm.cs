﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminCart;
using ShopifyAgent;

namespace FM2015.ViewModels
{
    public class RebuildFMDetailsvm
    {
        public AdminCart.Order FMOrder;
        public ShopifyOrder ShopifyOrder;
        public List<OrderDetail> FMOrderDetails;
        public List<ShopifyLineItem> ShopifyLineItems;

        public RebuildFMDetailsvm()
        {
            FMOrder = new AdminCart.Order();
            ShopifyOrder = new ShopifyOrder();
            FMOrderDetails = new List<OrderDetail>();
            ShopifyLineItems = new List<ShopifyLineItem>();
        }
    }

    public class RebuildFMDetailsReport
    {
        public List<int> MatchedOrders;
        public List<RebuildFMDetailsErrors> UnMatchedOrders;

        public RebuildFMDetailsReport()
        {
            MatchedOrders = new List<int>();
            UnMatchedOrders = new List<RebuildFMDetailsErrors>();
        }
    }

    public class RebuildFMDetailsErrors
    {
        private string _errorMsg;
        private int _errorCode;
        private int _orderId;

        public int OrderId { get => _orderId; set => _orderId = value; }
        public string ErrorMsg { get => _errorMsg; set => _errorMsg = value; }
        public int ErrorCode { get => _errorCode; set => _errorCode = value; }
    }
}