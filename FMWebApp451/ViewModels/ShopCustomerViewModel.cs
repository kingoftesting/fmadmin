﻿using System.Collections.Generic;
using System.Web.Mvc;
using FM2015.Models;
using System.ComponentModel.DataAnnotations;
using AdminCart;
using PayWhirl;
using Stripe;
using System;

namespace FM2015.ViewModels
{
    public class ShopCustomerViewModel
    {
        public enum AddressType
        {
            BillingAddressType = 1,
            ShippingAddressType = 2
        }

        [Key]
        public int CustomerViewModelKey { get; set; }

        public Customer Customer { get; set; }
        public Address BillAddress { get; set; }
        public Address ShipAddress { get; set; }
        public PayWhirlSubscriber payWhirlCustomer;
        public IEnumerable<StripeSubscription> stripeCustomerSubscriptionList;

        public string SelectBillCountryCode { get; set; }
        public SelectList BillCountrySelectList { get; set; }
        public string SelectShipCountryCode { get; set; }
        public SelectList ShipCountrySelectList { get; set; }

        public string SelectBillStateCode { get; set; }
        public SelectList BillStateSelectList { get; set; }
        public string SelectShipStateCode { get; set; }
        public SelectList ShipStateSelectList { get; set; }

        public ShopCustomerViewModel()
        {
            Customer = new Customer();
            BillAddress = new Address();
            ShipAddress = new Address();
            payWhirlCustomer = new PayWhirlSubscriber();
            stripeCustomerSubscriptionList = new List<StripeSubscription>();

            BillCountrySelectList = Location.GetCountrySelectList("US");
            BillStateSelectList = Location.GetStateSelectList(Location.GetCountryCodeIDFromSelectList(BillCountrySelectList));
            ShipCountrySelectList = Location.GetCountrySelectList("US");
            ShipStateSelectList = Location.GetStateSelectList(Location.GetCountryCodeIDFromSelectList(BillCountrySelectList));
        }

        public ShopCustomerViewModel(Customer customer)
        {
            Customer = customer;

            try
            {
                //payWhirlCustomer = PayWhirlSubscriber.FindSubscriberByEmail(customer.Email);
                payWhirlCustomer = PayWhirlBiz.FindSubscriberByEmail(customer.Email);
            }
            catch (Exception ex)
            {
                string msg = "CustomerViewModel Find PayWhirl Subscriber: ";
                msg += "customerID= " + customer.CustomerID + "CustomerEmail= " + customer.Email;
                FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
            }

            if (!string.IsNullOrEmpty(payWhirlCustomer.Stripe_id))
            {
                var subscriptionService = new Stripe.StripeSubscriptionService(); //get list of all subscriptions for customer
                StripeSubscriptionListOptions stripeSubscriptionListOptions = new StripeSubscriptionListOptions()
                {
                    CustomerId = payWhirlCustomer.Stripe_id,
                };
                try
                {
                    //stripeCustomerSubscriptionList = subscriptionService.List(payWhirlCustomer.Stripe_id);
                    stripeCustomerSubscriptionList = subscriptionService.List(stripeSubscriptionListOptions);
                }
                catch (StripeException ex)
                {
                    int response = (int)ex.HttpStatusCode;
                    StripeError err = ex.StripeError;
                    string errType = err.ErrorType;
                    string errCode = (string.IsNullOrEmpty(err.Code)) ? "" : err.Code;
                    string errError = (string.IsNullOrEmpty(err.Error)) ? "" : err.Error;
                    string errMessage = (string.IsNullOrEmpty(err.Message)) ? "" : err.Message;
                    string msg = "CustomerViewModel Get Subscription List: Stripe Error: errorType= " + errType + "; errorCode= ";
                    msg += errCode + "; errorError=" + errError + "; errorMEssage= " + errMessage;
                    FM2015.Helpers.dbErrorLogging.LogError(msg, ex);
                }
            }



            BillAddress = Address.GetAddressList(customer.LastName, 1).Find(x => x.CustomerID == customer.CustomerID);
            BillCountrySelectList = Location.GetCountrySelectList(BillAddress.Country);
            BillStateSelectList = Location.GetStateSelectList(Location.GetCountryCodeIDFromSelectList(BillCountrySelectList));

            ShipAddress = Address.GetAddressList(customer.LastName, 2).Find(x => x.CustomerID == customer.CustomerID);
            if (ShipAddress == null) { ShipCountrySelectList = Location.GetCountrySelectList(BillAddress.Country); }
            else { ShipCountrySelectList = Location.GetCountrySelectList(ShipAddress.Country); }

            ShipStateSelectList = Location.GetStateSelectList(Location.GetCountryCodeIDFromSelectList(ShipCountrySelectList));
        }

        public static List<CustomerViewModel> GetCvmList(string searchCriteria, string searchItem)
        {
            List<CustomerViewModel> cvmList = new List<CustomerViewModel>();

            return cvmList;
        }
    }
}