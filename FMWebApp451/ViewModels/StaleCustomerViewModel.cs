﻿using System;
using System.Collections.Generic;
using FMWebApp451.Interfaces;
using FM2015.Models;
using AdminCart;

namespace FM2015.ViewModels
{
    public class StaleCustomerViewModel
    {

        public int customerId;
        public DateTime OrderDate;
        public int OrderId;
        public string firstName;
        public string lastName;
        public string email;

        public StaleCustomerViewModel()
        {
            this.customerId = 0;
            this.OrderDate = DateTime.MinValue;
            this.OrderId = 0;
            this.firstName = "";
            this.lastName = "";
            this.email = "";
        }
    }
}