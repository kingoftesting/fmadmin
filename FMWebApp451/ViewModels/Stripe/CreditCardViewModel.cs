﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using FM2015.Models;
using AdminCart;

namespace FMWebApp451.ViewModels.Stripe
{
    public class CreditCardViewModel
    {
        [Key]
        public int CreditCardViewModelKey { get; set; }

        public Customer customer { get; set; }
        public string StripeCustomerID { get; set; }

        [Required]
        public string CardNumber { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string ExpirationMonth { get; set; }

        [Required]
        public string ExpirationYear { get; set; }

        [Required]
        public string Cvc { get; set; }

        public CreditCardViewModel()
        {
            CreditCardViewModelKey = 0;
            StripeCustomerID = "";
            CardNumber = "";
            Name = "";
            ExpirationMonth = "";
            ExpirationMonth = "";
            Cvc = "";
        }

        public CreditCardViewModel(int customerID)
        {
            CreditCardViewModelKey = customerID;
            StripeCustomerID = "";
            CardNumber = "";
            Name = "";
            ExpirationMonth = "";
            ExpirationMonth = "";
            Cvc = "";
        }


    }
}