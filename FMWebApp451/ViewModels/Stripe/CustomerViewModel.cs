﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Stripe;
using FM2015.Models;

namespace FMWebApp.ViewModels.Stripe
{
    public class StripeCustomerViewModel
    {
        public int customerId;
        public int orderId;
        public AdminCart.Customer customer;
        public string currSubscriptionId;
        public Subscriptions currSubscription;
        public string stripeCustomerId;
        public StripeCustomer stripeCustomer;
        public string currStripeSubscriptionId;
        public StripeSubscription currStripeSubscription;
        public string currStripePlanId;
        public string newPromoCode;
        public DateTime trialPeriodEndDate;
        public IEnumerable<StripeCard> stripeCustomerCardList;
        public IEnumerable<StripeSubscription> stripeCustomerSubscriptionList;
        public IEnumerable<StripeCharge> stripeCustomerChargeList;
        public IEnumerable<StripePlan> stripeCustomerPlanList;
        public IEnumerable<StripeInvoice> stripeCustomerInvoiceList;
        public IEnumerable<StripeCoupon> stripeCustomerCouponList;


        public StripeCustomerViewModel()
        {
            customerId = 0;
            orderId = 0;
            customer = new AdminCart.Customer();
            currSubscription = new Subscriptions();
            stripeCustomerId = "";
            stripeCustomer = new StripeCustomer();
            currStripeSubscriptionId = "";
            currStripeSubscription = new StripeSubscription();
            currStripePlanId = "";
            newPromoCode = "";
            trialPeriodEndDate = DateTime.MinValue;
            stripeCustomerCardList = new List<StripeCard>();
            stripeCustomerSubscriptionList = new List<StripeSubscription>();
            stripeCustomerChargeList = new List<StripeCharge>();
            stripeCustomerPlanList = new List<StripePlan>();
            stripeCustomerInvoiceList = new List<StripeInvoice>();
            stripeCustomerCouponList = new List<StripeCoupon>();
        }

        public StripeCustomerViewModel(string stripeCustomerID, string stripeSubscriptionID)
        {
            //initialize everything
            customerId = 0;
            orderId = 0;
            customer = new AdminCart.Customer();
            currSubscription = new Subscriptions();
            stripeCustomerId = "";
            stripeCustomer = new StripeCustomer();
            currStripeSubscriptionId = "";
            currStripeSubscription = new StripeSubscription();
            currStripePlanId = "";
            newPromoCode = "";
            trialPeriodEndDate = DateTime.MinValue;
            stripeCustomerCardList = new List<StripeCard>();
            stripeCustomerSubscriptionList = new List<StripeSubscription>();
            stripeCustomerChargeList = new List<StripeCharge>();
            stripeCustomerPlanList = new List<StripePlan>();
            stripeCustomerInvoiceList = new List<StripeInvoice>();
            stripeCustomerCouponList = new List<StripeCoupon>();

            //now fill-in with actual values, if any
            currSubscription = new Subscriptions(stripeSubscriptionID, stripeCustomerID);
            currSubscriptionId = currSubscription.ID.ToString();
            orderId = currSubscription.OrderID;
            AdminCart.Order order = new AdminCart.Order(currSubscription.OrderID);
            customer = new AdminCart.Customer(order.CustomerID);
            customerId = customer.CustomerID;

            stripeCustomerId = stripeCustomerID;
            stripeCustomer = new StripeCustomer();
            var customerService = new StripeCustomerService();
            try
            {
                stripeCustomer = customerService.Get(stripeCustomerID);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeCustomerViewModel: Subscriber Lookup Error: Stripe customer can't be found");
            }

            currStripeSubscriptionId = stripeSubscriptionID;
            currStripeSubscription = new StripeSubscription();
            var subscriptionService = new StripeSubscriptionService();
            try
            {
                currStripeSubscription = subscriptionService.Get(stripeSubscriptionID);
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeCustomerViewModel: Subscriber Lookup Error: original subscription can't be found");
            }

            if (currStripeSubscription.StripePlan != null)
            {
                currStripePlanId = currStripeSubscription.StripePlan.Id;
            }

            trialPeriodEndDate = (currStripeSubscription.TrialEnd == null) ? DateTime.MinValue : (DateTime)currStripeSubscription.TrialEnd;

            stripeCustomerCardList = new List<StripeCard>();
            var cardService = new StripeCardService();
            try
            {
                stripeCustomerCardList = cardService.List(stripeCustomerID); // optional StripeListOptions and isRecipient
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeCustomerViewModel: Subscriber Lookup Error: card list can't be found");
            }

            StripeSubscriptionListOptions stripeSubscriptionListOptions = new StripeSubscriptionListOptions()
            {
                CustomerId = stripeCustomerID,
            };
            try
            {
                //stripeCustomerSubscriptionList = subscriptionService.List(stripeCustomerID); // optional StripeListOptions
                stripeCustomerSubscriptionList = subscriptionService.List(stripeSubscriptionListOptions); // optional StripeListOptions
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeCustomerViewModel: Subscriber Lookup Error: card list can't be found");
            }

            stripeCustomerPlanList = new List<StripePlan>();
            StripePlanListOptions stripePlanListOptions = new StripePlanListOptions();
            stripePlanListOptions.Limit = 50;
            var planService = new StripePlanService();
            try
            {
                stripeCustomerPlanList = planService.List(stripePlanListOptions); // optional StripeListOptions
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeCustomerViewModel: Subscriber Lookup Error: plan list can't be found");
            }

            stripeCustomerChargeList = new List<StripeCharge>();
            StripeChargeListOptions stripeChargeListOptions = new StripeChargeListOptions();
            stripeChargeListOptions.CustomerId = stripeCustomerID;
            var chargeService = new StripeChargeService();
            try
            {
                stripeCustomerChargeList = chargeService.List(stripeChargeListOptions); // optional StripeChargeListOptions
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeCustomerViewModel: Subscriber Lookup Error: charge list can't be found");
            }

            stripeCustomerCouponList = new List<StripeCoupon>();
            stripePlanListOptions.Limit = 50;
            var couponService = new StripeCouponService();
            try
            {
                stripeCustomerCouponList = couponService.List();  // optional StripeListOptions
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeCustomerViewModel: Subscriber Lookup Error: Coupon list can't be found");
            }

            stripeCustomerInvoiceList = new List<StripeInvoice>();
            StripeInvoiceListOptions stripeInvoiceListOptions = new StripeInvoiceListOptions();
            stripeInvoiceListOptions.CustomerId = stripeCustomerID;
            var invoiceService = new StripeInvoiceService();
            try
            {
                stripeCustomerInvoiceList = invoiceService.List(stripeInvoiceListOptions); // optional StripeInvoiceListOptions
            }
            catch (StripeException ex)
            {
                Utilities.FMHelpers.ProcessStripeException(ex, true, "", "StripeCustomerViewModel: Subscriber Lookup Error: invoice list can't be found");
            }
        }

    }
}